﻿using Mse.PDFSharp;
using Mse.PDFSharp.Fonts;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Mse.PDFSharp
{
    public class SegoeWpFontResolver : IFontResolver
    {
        public string DefaultFontName => throw new NotImplementedException();

        static class FamilyNames
        {
            public const string SegoeWPLight = "segoe wp light";
            public const string SegoeWPSemilight = "segoe wp semilight";
            public const string SegoeWP = "segoe wp";
            public const string SegoeWPSemibold = "segoe wp semibold";
            public const string SegoeWPBold = "segoe wp bold";
            public const string SegoeWPBlack = "segoe wp black";
        }
        
        static class FaceNames
        {
            /// Used in the first parameter of the FontResolverInfo constructor.
            public const string SegoeWPLight = "SegoeWPLight";
            public const string SegoeWPSemilight = "SegoeWPSemilight";
            public const string SegoeWP = "SegoeWP";
            public const string SegoeWPSemibold = "SegoeWPSemibold";
            public const string SegoeWPBold = "SegoeWPBold";
            public const string SegoeWPBlack = "SegoeWPBlack";
        }
                
        public FontResolverInfo ResolveTypeface(string familyName, bool isBold, bool isItalic)
        {            
            string lowerFamilyName = familyName.ToLowerInvariant();
            // Looking for a Segoe WP font?
            if (lowerFamilyName.StartsWith("segoe wp"))
            {
                // Bold simulation is not recommended, but used here for demonstration.
                bool simulateBold = false;

                // Since Segoe WP typefaces do not contain any italic font
                // always simulate italic if it is requested.
                bool simulateItalic = isItalic;

                string faceName;

                // In this sample family names are case sensitive. You can relax this in your own implementation
                // and make them case insensitive.
                switch (lowerFamilyName)
                {
                    case FamilyNames.SegoeWPLight:
                        // Just for demonstration use 'Semilight' if bold is requested.
                        if (isBold)
                            goto case FamilyNames.SegoeWPSemilight;
                        faceName = FaceNames.SegoeWPLight;
                        break;

                    case FamilyNames.SegoeWPSemilight:
                        // Demonstrate bold simulation.
                        if (isBold)
                            simulateBold = true;
                        faceName = FaceNames.SegoeWPSemilight;
                        break;

                    case FamilyNames.SegoeWP:
                        // Use font 'Bold' if bold is requested.
                        if (isBold)
                            goto UseSegoeWPBold;
                        faceName = FaceNames.SegoeWP;
                        break;

                    case FamilyNames.SegoeWPSemibold:
                        // Do not care about bold for semibold.
                        faceName = FaceNames.SegoeWPSemibold;
                        break;

                    case FamilyNames.SegoeWPBold:
                        // Just for demonstration use font 'Black' if bold is requested.
                        if (isBold)
                            goto case FamilyNames.SegoeWPBlack;
                        UseSegoeWPBold:
                        faceName = FaceNames.SegoeWPBold;
                        break;

                    case FamilyNames.SegoeWPBlack:
                        // Do not care about bold for black.
                        faceName = FaceNames.SegoeWPBlack;
                        break;

                    default:
                        Debug.Assert(false, "Unknown Segoe WP font: " + lowerFamilyName);
                        goto case FamilyNames.SegoeWP;  // Alternatively throw an exception in this case.
                }

                // Tell the caller the effective typeface name and whether bold  or italic should be simulated.
                return new FontResolverInfo(faceName, simulateBold, simulateItalic);
            }

            // Return null means that the typeface cannot be resolved and PDFsharp stops working.
            // Alternatively forward call to PlatformFontResolver.
            return PlatformFontResolver.ResolveTypeface(familyName, isBold, isItalic);
        }
        
        public byte[] GetFont(string faceName)
        {
            // Note: PDFsharp never calls GetFont twice with the same face name.

            // Return the bytes of a font.
            switch (faceName)
            {
                case FaceNames.SegoeWPLight:
                    return FontDataHelper.SegoeWPLight;

                case FaceNames.SegoeWPSemilight:
                    return FontDataHelper.SegoeWPSemilight;

                case FaceNames.SegoeWP:
                    return FontDataHelper.SegoeWP;

                case FaceNames.SegoeWPSemibold:
                    return FontDataHelper.SegoeWPSemibold;

                case FaceNames.SegoeWPBold:
                    return FontDataHelper.SegoeWPBold;

                case FaceNames.SegoeWPBlack:
                    return FontDataHelper.SegoeWPBlack;
            }
            // PDFsharp never calls GetFont with a face name that was not returned by ResolveTypeface.
            throw new ArgumentException(String.Format("Invalid typeface name '{0}'", faceName));
        }
    }
}
