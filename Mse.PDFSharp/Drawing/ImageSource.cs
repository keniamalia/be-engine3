﻿/*Reza*/
using Mse.PDFSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;


namespace MigraDocCore.DocumentObjectModel.MigraDoc.DocumentObjectModel.Shapes
{
    public abstract class ImageSource
    {
        public static ImageSource ImageSourceImpl { get; set; }

        public interface IImageSource
        {
            int Width { get; }
            int Height { get; }
            string Name { get; }
            bool IsJpeg { get; }
            void SaveAsJpeg(MemoryStream ms);
            void SaveAsBmp(MemoryStream ms);
        }

        protected abstract IImageSource FromFileImpl(string path, bool isJpeg = true, int? quality = 75);
        protected abstract IImageSource FromBinaryImpl(string name, Func<byte[]> imageSource, bool isJpeg = true, int? quality = 75);
        protected abstract IImageSource FromStreamImpl(string name, Func<Stream> imageStream, bool isJpeg = true, int? quality = 75);
        
        private class NetFrameworkImageSourceImpl : IImageSource
        {
            private Func<Image> _getImage;
            public int Width => Image.Width;
            public int Height => Image.Height;
            public string Name { get; }

            private Image _image;
            private readonly int? _quality;
            private bool _IsJpeg;

            private Image Image
            {
                get
                {
                    if (_image == null)
                    {
                        _image = _getImage.Invoke();
                    }
                    return _image;
                }
            }

            public bool IsJpeg => _IsJpeg;

            public NetFrameworkImageSourceImpl(string name, Func<Image> getImage, int quality, bool isJpeg)
            {
                _IsJpeg = isJpeg;
                Name = name;
                _getImage = getImage;
                _quality = quality;
            }

            public void SaveAsJpeg(MemoryStream ms)
            {
                try
                {
                    //ini memaksakan png jd white background
                    using (var b = new Bitmap(Image.Width, Image.Height))
                    {
                        b.SetResolution(Image.HorizontalResolution, Image.VerticalResolution);

                        using (var g = Graphics.FromImage(b))
                        {
                            try
                            {
                                g.Clear(ColorTranslator.FromHtml("#ffffff"));
                                g.DrawImageUnscaled(Image, 0, 0);
                            }
                            catch (Exception es)
                            {
                                Debug.WriteLine("" + es.Message);
                            }
                        }

                        b.Save(ms, ImageFormat.Jpeg);
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine("" + e.Message + " " + e.StackTrace);
                }
            }

            public void Dispose()
            {
                Image.Dispose();
            }

            public void SaveAsBmp(MemoryStream ms)
            {
                throw new NotImplementedException();
            }
        }

        public static IImageSource FromFile(string path, bool isJpeg = true, int? quality = 75)
        {
            return ImageSourceImpl.FromFileImpl(path, isJpeg, quality);
        }

        public static IImageSource FromBinary(string name, Func<byte[]> imageSource, bool isJpeg = true, int? quality = 75)
        {
            return ImageSourceImpl.FromBinaryImpl(name, imageSource, isJpeg, quality);
        }

        public static IImageSource FromStream(string name, Func<Stream> imageStream, bool isJpeg = true, int? quality = 75)
        {
//#if NETSTANDARD2_0
            Debug.WriteLine("name:" + name);
            Debug.WriteLine("imageStream:"+ imageStream);

            Stream value = imageStream.Invoke();
            Bitmap bmpImage = (Bitmap)Bitmap.FromStream(value);

            Debug.WriteLine("bmpImage:"+ bmpImage.Width);
            Debug.WriteLine("bmpImage:" + bmpImage.Height);
            
            //set langsung
            return new NetFrameworkImageSourceImpl(name, () =>
            {
                return bmpImage;
            }, (int)quality, isJpeg);
//#endif
#if NET
           return ImageSourceImpl.FromStreamImpl(name, imageStream, isJpeg, quality);
#endif
        }
    }
}
