﻿using MigraDocCore.DocumentObjectModel.MigraDoc.DocumentObjectModel.Shapes;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static MigraDocCore.DocumentObjectModel.MigraDoc.DocumentObjectModel.Shapes.ImageSource;

namespace Mse.PDFSharp
{
    public class ImageSharpImageSource : ImageSource
    {
        protected override IImageSource FromBinaryImpl(string name, Func<byte[]> imageSource, bool isJpeg = true, int? quality = 75)
        {
            return new NetFrameworkImageSourceImpl(name, () =>
            {
                using (var ms = new MemoryStream(imageSource.Invoke()))
                    return Image.FromStream(ms);
            }, (int)quality);

        }

        protected override IImageSource FromFileImpl(string path, bool isJpeg = true, int? quality = 75)
        {
            return new NetFrameworkImageSourceImpl(path, () =>
            {
                return Image.FromFile(path);
            }, (int)quality);
        }

        protected override IImageSource FromStreamImpl(string name, Func<Stream> imageStream, bool isJpeg = true, int? quality = 75)
        {
            return new NetFrameworkImageSourceImpl(name, () =>
            {
                using (var stream = imageStream.Invoke())
                {
                    return Image.FromStream(imageStream.Invoke());
                }
            }, (int)quality);
        }

        private class ImageSharpImageSourceImpl : IImageSource
        {
            private Image _image;
            private Image Image
            {
                get
                {
                    if (_image == null)
                    {
                        _image = _getImage.Invoke();
                    }
                    return _image;
                }
            }
            private Func<Image> _getImage;
            private readonly int _quality;

            public int Width => Image.Width;
            public int Height => Image.Height;
            public string Name { get; }

            public bool IsJpeg => throw new NotImplementedException();

            public ImageSharpImageSourceImpl(string name, Func<Image> getImage, int quality)
            {
                Name = name;
                _getImage = getImage;
                _quality = quality;
            }

            public void SaveAsJpeg(MemoryStream ms)
            {
                /*
                Image.AutoOrient();
                Image.SaveAsJpeg(ms, new JpegEncoderOptions() { Quality = _quality });
                */
            }

            public void Dispose()
            {
                Image.Dispose();
            }

            public void SaveAsBmp(MemoryStream ms)
            {

            }
        }

        private class NetFrameworkImageSourceImpl : IImageSource
        {
            private Func<Image> _getImage;
            public int Width => Image.Width;
            public int Height => Image.Height;
            public string Name { get; }

            private Image _image;
            private readonly int? _quality;

            private Image Image
            {
                get
                {
                    if (_image == null)
                    {
                        _image = _getImage.Invoke();
                    }
                    return _image;
                }
            }

            public bool IsJpeg => throw new NotImplementedException();

            public NetFrameworkImageSourceImpl(string name, Func<Image> getImage, int quality)
            {
                Name = name;
                _getImage = getImage;
                _quality = quality;
            }

            public void SaveAsJpeg(MemoryStream ms)
            {
                var paras = new EncoderParameters();
                ImageCodecInfo jpgEncoder = ImageCodecInfo.GetImageDecoders().Where(x => x.FormatID == ImageFormat.Jpeg.Guid).First();
                var param = new EncoderParameters().Param[0];
                paras.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, (long)_quality);
                Image.Save(ms, jpgEncoder, paras);
            }

            public void Dispose()
            {
                Image.Dispose();
            }

            public void SaveAsBmp(MemoryStream ms)
            {
                throw new NotImplementedException();
            }
        }

    }
}
