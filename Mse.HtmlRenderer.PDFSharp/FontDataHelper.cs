﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Mse.HtmlRenderer.PDFSharp
{
    public static class FontDataHelper
    {
        /// <summary>
        /// Gets the Segoe WP Light font.
        /// </summary>
        public static byte[] SegoeWPLight
        {
            get { return LoadFontData("Mse.HtmlRenderer.PDFSharp.Fonts.SegoeWP-Light.ttf"); }
        }

        /// <summary>
        /// Gets the Segoe WP Semilight font.
        /// </summary>
        public static byte[] SegoeWPSemilight
        {
            get { return LoadFontData("Mse.HtmlRenderer.PDFSharp.Fonts.SegoeWP-Semilight.ttf"); }
        }

        /// <summary>
        /// Gets the Segoe WP font.
        /// </summary>
        public static byte[] SegoeWP
        {
            get { return LoadFontData("Mse.HtmlRenderer.PDFSharp.Fonts.SegoeWP.ttf"); }
        }

        /// <summary>
        /// Gets the Segoe WP Semibold font.
        /// </summary>
        public static byte[] SegoeWPSemibold
        {
            get { return LoadFontData("Mse.HtmlRenderer.PDFSharp.Fonts.SegoeWP-Semibold.ttf"); }
        }

        /// <summary>
        /// Gets the Segoe WP Bold font.
        /// </summary>
        public static byte[] SegoeWPBold
        {
            get { return LoadFontData("Mse.HtmlRenderer.PDFSharp.Fonts.SegoeWP-Bold.ttf"); }
        }

        /// <summary>
        /// Gets the Segoe WP Black font.
        /// </summary>
        public static byte[] SegoeWPBlack
        {
            get { return LoadFontData("Mse.HtmlRenderer.PDFSharp.Fonts.SegoeWP-Black.ttf"); }
        }

        /// <summary>
        /// Returns the specified font from an embedded resource.
        /// </summary>
        static byte[] LoadFontData(string name)
        {
            Assembly assembly = typeof(FontDataHelper).Assembly;
            var datas=typeof(FontDataHelper).Assembly.GetManifestResourceNames();

            /*
            using (Stream stream = new FileStream(@"D:\SegoeWP-Light.ttf", FileMode.Open) )
            {
                if (stream == null)
                    throw new ArgumentException("No resource with name " + name);

                var count = (int)stream.Length;
                var data = new byte[count];
                stream.Read(data, 0, count);
                return data;
            }
            */


            string fileName = @"D:\SegoeWP-Light.ttf";
            byte[] buffer = File.ReadAllBytes(fileName);

            if (buffer == null)
                throw new ArgumentException("No resource with name " + name);
            else
                return buffer;

        }
    }
}
