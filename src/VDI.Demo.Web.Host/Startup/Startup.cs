using System;
using System.Linq;
using Abp.AspNetCore;
using Abp.AspNetZeroCore.Web.Authentication.JwtBearer;
using Abp.Castle.Logging.Log4Net;
using Abp.Dependency;
using Abp.Extensions;
using Abp.Hangfire;
using Abp.Timing;
using Castle.Facilities.Logging;
using Hangfire;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using VDI.Demo.Authorization;
using VDI.Demo.Authorization.Roles;
using VDI.Demo.Authorization.Users;
using VDI.Demo.Configuration;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.Identity;
using VDI.Demo.Install;
using VDI.Demo.MultiTenancy;
using VDI.Demo.Web.Authentication.JwtBearer;
using PaulMiami.AspNetCore.Mvc.Recaptcha;
using Swashbuckle.AspNetCore.Swagger;
using VDI.Demo.Web.IdentityServer;
using Microsoft.EntityFrameworkCore;
using VDI.Demo.Debugging;
using VDI.Demo.Web.Startup.BackgroundTask;
#if FEATURE_SIGNALR
using Abp.Owin;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin.Cors;
using Owin;
using Owin.Security.AesDataProtectorProvider;
using Abp.Web.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Abp.AspNetZeroCore.Web.Owin;
#endif

namespace VDI.Demo.Web.Startup
{
    public class Startup
    {
        private const string DefaultCorsPolicyName = "localhost";

        private readonly IConfigurationRoot _appConfiguration;

        public Startup(IHostingEnvironment env)
        {
            _appConfiguration = env.GetAppConfiguration();
        }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DemoDbContext>(options =>
                           options.UseSqlServer(_appConfiguration.GetConnectionString(DemoConsts.ConnectionStringName)));

            services.AddDbContext<NewCommDbContext>(options =>
                        options.UseSqlServer(_appConfiguration.GetConnectionString(DemoConsts.ConnectionStringNewCommDbContext)));

            services.AddDbContext<PropertySystemDbContext>(options =>
                        options.UseSqlServer(_appConfiguration.GetConnectionString(DemoConsts.ConnectionStringPropertySystemDbContext)));

            services.AddDbContext<PersonalsNewDbContext>(options =>
                        options.UseSqlServer(_appConfiguration.GetConnectionString(DemoConsts.ConnectionStringPersonalsNewDbContext)));

            services.AddDbContext<AccountingDbContext>(options =>
                        options.UseSqlServer(_appConfiguration.GetConnectionString(DemoConsts.ConnectionStringAccountingDbContext)));

            services.AddDbContext<TAXDbContext>(options =>
                        options.UseSqlServer(_appConfiguration.GetConnectionString(DemoConsts.ConnectionStringTAXDbContext)));

            services.AddDbContext<LKLaunchingSystemDbContext>(options =>
                        options.UseSqlServer(_appConfiguration.GetConnectionString(DemoConsts.ConnectionStringLKLaunchingSystemDbContext)));

            services.AddDbContext<PPOnlineDbContext>(options =>
                        options.UseSqlServer(_appConfiguration.GetConnectionString(DemoConsts.ConnectionStringPPOnline)));

            services.AddDbContext<OracleStageDbContext>(options =>
                        options.UseSqlServer(_appConfiguration.GetConnectionString(DemoConsts.ConnectionStringOracleStageDbContext)));


            //MVC
            services.AddMvc(options =>
            {
                options.Filters.Add(new CorsAuthorizationFilterFactory(DefaultCorsPolicyName));
            });
            services.AddDistributedMemoryCache();//untuk session
            services.AddSession();//untuk session

            //Configure CORS for angular2 UI
            services.AddCors(options =>
            {
                options.AddPolicy(DefaultCorsPolicyName, builder =>
                {
                    //App:CorsOrigins in appsettings.json can contain more than one address with splitted by comma.
                    builder
                        //.WithOrigins(_appConfiguration["App:CorsOrigins"].Split(",", StringSplitOptions.RemoveEmptyEntries).Select(o => o.RemovePostFix("/")).ToArray())
                        .AllowAnyOrigin() //TODO: Will be replaced by above when Microsoft releases microsoft.aspnetcore.cors 2.0 - https://github.com/aspnet/CORS/pull/94
                        .AllowAnyHeader()
                        .AllowAnyMethod();
                });
            });

            services.AddHangfire(config =>
            {
                config.UseSqlServerStorage(_appConfiguration.GetConnectionString(DemoConsts.ConnectionStringName));
            });


            IdentityRegistrar.Register(services);
            AuthConfigurer.Configure(services, _appConfiguration);
            
            //Identity server
            if (bool.Parse(_appConfiguration["IdentityServer:IsEnabled"]))
            {
                IdentityServerRegistrar.Register(services, _appConfiguration);
            }

            //Swagger - Enable this line and the related lines in Configure method to enable swagger UI
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new Info { Title = "Engine 3 API", Version = "v1" });
                options.DocInclusionPredicate((docName, description) => true);
            });

            //Recaptcha
            services.AddRecaptcha(new RecaptchaOptions
            {
                SiteKey = _appConfiguration["Recaptcha:SiteKey"],
                SecretKey = _appConfiguration["Recaptcha:SecretKey"]
            });

            //Hangfire(Enable to use Hangfire instead of default job manager)
            services.AddHangfire(config =>
            {
                config.UseSqlServerStorage(_appConfiguration.GetConnectionString("Default"));
            });

            //Configure Abp and Dependency Injection
            return services.AddAbp<DemoWebHostModule>(options =>
            {
                //Configure Log4Net logging
                options.IocManager.IocContainer.AddFacility<LoggingFacility>(
                    f => f.UseAbpLog4Net().WithConfig("log4net.config")
                );
            });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            //Initializes ABP framework.
            app.UseAbp(options =>
            {
                options.UseAbpRequestLocalization = false; //used below: UseAbpRequestLocalization
            });

            app.UseCors(DefaultCorsPolicyName); //Enable CORS!

            app.UseAuthentication();
            app.UseHangfireDashboard();
            app.UseJwtTokenMiddleware();
            
            if (bool.Parse(_appConfiguration["IdentityServer:IsEnabled"]))
            {
                app.UseJwtTokenMiddleware("IdentityBearer");
                app.UseIdentityServer();
            }

            app.UseStaticFiles();

            if (DatabaseCheckHelper.Exist(_appConfiguration["ConnectionStrings:Default"]))
            {
                app.UseAbpRequestLocalization();
            }

#if FEATURE_SIGNALR
            //Integrate to OWIN
            app.UseAppBuilder(ConfigureOwinServices);
#endif

            //Hangfire dashboard &server(Enable to use Hangfire instead of default job manager)
            app.UseHangfireDashboard("/hangfire", new DashboardOptions
            {
                Authorization = new[] { new AbpHangfireAuthorizationFilter(AppPermissions.Pages_Administration_HangfireDashboard) }
            });
            app.UseHangfireServer();
            
            string cron = DebugHelper.IsDebug ? Cron.Minutely() : Cron.Daily();
            var localTimeZone = "SE Asia Standard Time";
            RecurringJob.AddOrUpdate("online-booking-reminder", () => OnlineBookingTask.Run(), Cron.Minutely(), TimeZoneInfo.FindSystemTimeZoneById(localTimeZone));
            RecurringJob.AddOrUpdate("pppu-expired-template", () => LegalDocumentMappingTemplateTask.Run(), Cron.Daily(1), TimeZoneInfo.FindSystemTimeZoneById(localTimeZone));
            RecurringJob.AddOrUpdate("payment-send-email-PrintOR", () => PaymentTask.Run(), "0 10 * * *", TimeZoneInfo.FindSystemTimeZoneById(localTimeZone));
            RecurringJob.AddOrUpdate("PSAS-create-penalty-schedule", () => PSASPenaltyTask.Run(), "0 23 * * *", TimeZoneInfo.FindSystemTimeZoneById(localTimeZone));
            RecurringJob.AddOrUpdate("PSAS-create-SP-period", () => PSASSPTask.Run(), "0 00 * * *", TimeZoneInfo.FindSystemTimeZoneById(localTimeZone));
            RecurringJob.AddOrUpdate("online-booking-reminder-payment", () => ReminderPaymentOBTask.Run(), Cron.Hourly(), TimeZoneInfo.FindSystemTimeZoneById(localTimeZone));

            app.UseSession();//untuk session
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "defaultWithArea",
                    template: "{area}/{controller=Home}/{action=Index}/{id?}");

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            // Enable middleware to serve generated Swagger as a JSON endpoint
            app.UseSwagger();
            // Enable middleware to serve swagger-ui assets (HTML, JS, CSS etc.)
            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint(_appConfiguration["App:VirtualDirectory"] + "/swagger/v1/swagger.json", "Engine 3 API V1");
            }); //URL: /swagger
        }

#if FEATURE_SIGNALR
        private static void ConfigureOwinServices(IAppBuilder app)
        {
            GlobalHost.DependencyResolver.Register(typeof(IAssemblyLocator), () => new SignalRAssemblyLocator());
            app.Properties["host.AppName"] = "Demo";

            app.UseAbp();
            app.UseAesDataProtectorProvider();

            app.Map("/signalr", map =>
            {
                map.UseCors(CorsOptions.AllowAll);

                var hubConfiguration = new HubConfiguration
                {
                    EnableJSONP = true
                };

                map.RunSignalR(hubConfiguration);
            });
        }
#endif
    }
}
