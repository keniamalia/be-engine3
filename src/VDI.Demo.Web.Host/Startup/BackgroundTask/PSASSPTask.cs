﻿using Abp.Dependency;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VDI.Demo.PSAS.SuratPeringatans;

namespace VDI.Demo.Web.Startup.BackgroundTask
{
    public class PSASSPTask
    {
        public static void Run()
        {
            var PSASSuratPeringatanAppService = IocManager.Instance.Resolve<IPSASSuratPeringatanAppService>();
            PSASSuratPeringatanAppService.CreateSuratPeringatanPenaltyScheduler();
            PSASSuratPeringatanAppService.SendEmailSPScheduler();
        }
    }
}
