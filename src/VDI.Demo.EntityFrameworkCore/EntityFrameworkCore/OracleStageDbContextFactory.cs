﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.Configuration;
using VDI.Demo.Web;

namespace VDI.Demo.EntityFrameworkCore
{
    public class OracleStageDbContextFactory : IDesignTimeDbContextFactory<OracleStageDbContext>
    {
        public OracleStageDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<OracleStageDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder(), addUserSecrets: true);

            OracleStageDbContextConfigurer.Configure(builder, configuration.GetConnectionString(DemoConsts.ConnectionStringOracleStageDbContext));

            return new OracleStageDbContext(builder.Options);
        }
    }
}
