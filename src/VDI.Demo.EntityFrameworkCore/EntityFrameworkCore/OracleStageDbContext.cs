﻿using Abp.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using VDI.Demo.OracleStageDB;
using VDI.Demo.PropertySystemDB;
using VDI.Demo.PropertySystemDB.LippoMaster;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;
using VDI.Demo.PropertySystemDB.OnlineBooking.PPOnline;
using VDI.Demo.PropertySystemDB.OnlineBooking.ProjectInfo;
using VDI.Demo.PropertySystemDB.OnlineBooking.PropertySystem;
using VDI.Demo.PropertySystemDB.Pricing;
using VDI.Demo.TAXDB;

namespace VDI.Demo.EntityFrameworkCore
{
    public class OracleStageDbContext : AbpDbContext
    {
        public virtual DbSet<LK_BUSINESS_UNIT> LK_BUSINESS_UNIT { get; set; }

        public virtual DbSet<LK_COA> LK_COA { get; set; }

        public virtual DbSet<LK_GL_INTERFACE> LK_GL_INTERFACE { get; set; }

        public OracleStageDbContext(DbContextOptions<OracleStageDbContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<LK_BUSINESS_UNIT>()
                .HasKey(c => new { c.BIZ_UNIT_ID });

            modelBuilder.Entity<LK_COA>()
                .HasKey(c => new { c.NATURE_ACCOUNT, c.REF_CODE});

            modelBuilder.Entity<LK_GL_INTERFACE>()
                .HasKey(c => new { c.INPUT_TIME });

            //modelBuilder.Entity<FP_TR_FPHeader>()
            //    .HasMany(e => e.FP_TR_FPDetail)
            //    .WithOne(e => e.FP_TR_FPHeader)
            //    .HasForeignKey(e => new { e.entityCode, e.coCode, e.FPCode });
            //.HasConstraintName("ForeignKey_FP_TR_FPHeader_FP_TR_FPDetail");

            //modelBuilder.Entity<MS_Account>()
            //            .HasIndex(b => b.accCode)
            //            .IsUnique()
            //            .HasName("accCodeUnique");



            ////No Delete On Action
            //modelBuilder.Entity<MS_BankBranch>()
            //    .HasOne(t => t.LK_BankLevel)
            //    .WithMany(w => w.MS_BankBranch)
            //    .HasForeignKey(d => d.bankBranchTypeID)
            //    .OnDelete(DeleteBehavior.Restrict);

            base.OnModelCreating(modelBuilder);
            //Database.SetInitializer<PropertySystemDbContext>(null);
        }
    }
}
