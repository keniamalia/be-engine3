﻿using Abp.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.PPOnlineDB;

namespace VDI.Demo.EntityFrameworkCore
{
    public class PPOnlineDbContext : AbpDbContext
    {
        public virtual DbSet<LK_OrderStatus> LK_OrderStatus { get; set; }

        public virtual DbSet<TR_PPOrder> TR_PPOrder { get; set; }

        public virtual DbSet<TR_PPOrderPPNo> TR_PPOrderPPNo { get; set; }

        public virtual DbSet<MS_BatchEntry> MS_BatchEntry { get; set; }

        public virtual DbSet<LK_PPStatus> LK_PPStatus { get; set; }

        public virtual DbSet<TR_PriorityPass> TR_PriorityPass { get; set; }

        public virtual DbSet<TR_PriorityPassRefund> TR_PriorityPassRefund { get; set; }

        public virtual DbSet<LK_RefundReason> LK_RefundReason { get; set; }

        public virtual DbSet<MS_Projects> MS_Projects { get; set; }

        public virtual DbSet<LK_TermRef> LK_TermRef { get; set; }

        public virtual DbSet<LK_Bank> LK_Bank { get; set; }

        public virtual DbSet<TR_Payment> TR_Payment { get; set; }

        public virtual DbSet<TR_PaymentDetailEDC> TR_PaymentDetailEDC { get; set; }

        public virtual DbSet<LK_CategoryType> LK_CategoryType { get; set; }

        public virtual DbSet<LK_CategoryTypePreferred> LK_CategoryTypePreferred { get; set; }


        public PPOnlineDbContext(DbContextOptions<PPOnlineDbContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
        }

    }
}
