﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.Configuration;
using VDI.Demo.Web;

namespace VDI.Demo.EntityFrameworkCore
{
    public class LKLaunchingSystemDbContextFactory : IDesignTimeDbContextFactory<LKLaunchingSystemDbContext>
    {
        public LKLaunchingSystemDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<LKLaunchingSystemDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder(), addUserSecrets: true);

            LKLaunchingSystemDbContextConfigurer.Configure(builder, configuration.GetConnectionString(DemoConsts.ConnectionStringLKLaunchingSystemDbContext));

            return new LKLaunchingSystemDbContext(builder.Options);
        }
    }
}
