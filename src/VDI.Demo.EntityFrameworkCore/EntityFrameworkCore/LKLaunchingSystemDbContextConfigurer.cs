﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;

namespace VDI.Demo.EntityFrameworkCore
{
    public static class LKLaunchingSystemDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<LKLaunchingSystemDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<LKLaunchingSystemDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
