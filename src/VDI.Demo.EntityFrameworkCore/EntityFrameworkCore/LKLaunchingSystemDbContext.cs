﻿using Abp.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using VDI.Demo.LKLaunchingSystemDB;
using VDI.Demo.PropertySystemDB;
using VDI.Demo.PropertySystemDB.LippoMaster;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;
using VDI.Demo.PropertySystemDB.OnlineBooking.PPOnline;
using VDI.Demo.PropertySystemDB.OnlineBooking.ProjectInfo;
using VDI.Demo.PropertySystemDB.OnlineBooking.PropertySystem;
using VDI.Demo.PropertySystemDB.Pricing;
using VDI.Demo.TAXDB;

namespace VDI.Demo.EntityFrameworkCore
{
    public class LKLaunchingSystemDbContext : AbpDbContext
    {
        

        public virtual DbSet<MS_BatchEntry> MS_BatchEntry { get; set; }

        public virtual DbSet<TR_PriorityPass> TR_PriorityPass { get; set; }

        public LKLaunchingSystemDbContext(DbContextOptions<LKLaunchingSystemDbContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            //modelBuilder.Entity<msBatchPajakStock>()
            //    .HasKey(c => new { c.BatchID, c.CoCode, c.FPBranchCode, c.FPYear, c.FPNo });

            //modelBuilder.Entity<FP_TR_FPHeader>()
            //    .HasMany(e => e.FP_TR_FPDetail)
            //    .WithOne(e => e.FP_TR_FPHeader)
            //    .HasForeignKey(e => new { e.entityCode, e.coCode, e.FPCode });
            //.HasConstraintName("ForeignKey_FP_TR_FPHeader_FP_TR_FPDetail");

            //modelBuilder.Entity<MS_Account>()
            //            .HasIndex(b => b.accCode)
            //            .IsUnique()
            //            .HasName("accCodeUnique");



            ////No Delete On Action
            //modelBuilder.Entity<MS_BankBranch>()
            //    .HasOne(t => t.LK_BankLevel)
            //    .WithMany(w => w.MS_BankBranch)
            //    .HasForeignKey(d => d.bankBranchTypeID)
            //    .OnDelete(DeleteBehavior.Restrict);

            base.OnModelCreating(modelBuilder);
            //Database.SetInitializer<PropertySystemDbContext>(null);
        }
    }
}
