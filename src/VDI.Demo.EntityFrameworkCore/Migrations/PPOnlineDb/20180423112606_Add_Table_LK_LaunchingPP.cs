﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PPOnlineDb
{
    public partial class Add_Table_LK_LaunchingPP : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "LK_PPStatus",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    PPStatus = table.Column<string>(maxLength: 1, nullable: false),
                    PPStatusName = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LK_PPStatus", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MS_BatchEntry",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    batchCode = table.Column<string>(maxLength: 3, nullable: false),
                    batchMaxNum = table.Column<int>(nullable: true),
                    batchSeq = table.Column<int>(nullable: false),
                    batchStartNum = table.Column<int>(nullable: false),
                    clusterCode = table.Column<string>(maxLength: 10, nullable: true),
                    clusterName = table.Column<string>(maxLength: 100, nullable: true),
                    floorSize = table.Column<string>(maxLength: 50, nullable: true),
                    floorSizeMath = table.Column<decimal>(nullable: true),
                    isBookingFee = table.Column<bool>(nullable: true),
                    isConvertOnly = table.Column<bool>(nullable: true),
                    isRunOut = table.Column<bool>(nullable: false),
                    isSellOnly = table.Column<int>(nullable: true),
                    isTopupOnly = table.Column<bool>(nullable: false),
                    maxToken = table.Column<int>(nullable: false),
                    maxTopupFromOldBatch = table.Column<int>(nullable: false),
                    minToken = table.Column<int>(nullable: false),
                    priorityPassPrice = table.Column<decimal>(type: "money", nullable: true),
                    projectCode = table.Column<string>(maxLength: 10, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MS_BatchEntry", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TR_PriorityPass",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    KPA = table.Column<string>(maxLength: 50, nullable: false),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    PPNo = table.Column<string>(maxLength: 6, nullable: false),
                    PPStatusID = table.Column<int>(nullable: false),
                    bank = table.Column<string>(maxLength: 20, nullable: true),
                    batchID = table.Column<int>(nullable: false),
                    batchSeq = table.Column<int>(nullable: false),
                    buyDate = table.Column<DateTime>(nullable: false),
                    cardNo = table.Column<string>(maxLength: 30, nullable: true),
                    dealingTime = table.Column<DateTime>(nullable: true),
                    idCard = table.Column<string>(maxLength: 200, nullable: true),
                    idSetting = table.Column<int>(nullable: true),
                    inputFrom = table.Column<string>(maxLength: 50, nullable: true),
                    kamar = table.Column<string>(maxLength: 50, nullable: true),
                    lantai = table.Column<string>(maxLength: 50, nullable: true),
                    memberCode = table.Column<string>(maxLength: 12, nullable: false),
                    oldPPNo = table.Column<string>(maxLength: 6, nullable: false),
                    paymentType = table.Column<string>(maxLength: 20, nullable: true),
                    psCode = table.Column<string>(maxLength: 8, nullable: false),
                    regTime = table.Column<DateTime>(nullable: true),
                    remarks = table.Column<string>(maxLength: 300, nullable: true),
                    scmCode = table.Column<string>(maxLength: 3, nullable: false),
                    tableNo = table.Column<int>(nullable: true),
                    token = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TR_PriorityPass", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TR_PriorityPass_LK_PPStatus_PPStatusID",
                        column: x => x.PPStatusID,
                        principalTable: "LK_PPStatus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TR_PriorityPass_MS_BatchEntry_batchID",
                        column: x => x.batchID,
                        principalTable: "MS_BatchEntry",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TR_PriorityPass_PPStatusID",
                table: "TR_PriorityPass",
                column: "PPStatusID");

            migrationBuilder.CreateIndex(
                name: "IX_TR_PriorityPass_batchID",
                table: "TR_PriorityPass",
                column: "batchID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TR_PriorityPass");

            migrationBuilder.DropTable(
                name: "LK_PPStatus");

            migrationBuilder.DropTable(
                name: "MS_BatchEntry");
        }
    }
}
