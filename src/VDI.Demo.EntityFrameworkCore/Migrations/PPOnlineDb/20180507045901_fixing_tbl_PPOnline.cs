﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PPOnlineDb
{
    public partial class fixing_tbl_PPOnline : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "orderRef",
                table: "TR_PPOrderPPNo");

            migrationBuilder.DropColumn(
                name: "orderRef",
                table: "TR_PPOrder");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "orderRef",
                table: "TR_PPOrderPPNo",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "orderRef",
                table: "TR_PPOrder",
                nullable: false,
                defaultValue: 0);
        }
    }
}
