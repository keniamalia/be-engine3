﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PPOnlineDb
{
    public partial class add_tbl_ppOnline_trpayment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TR_Payment",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    PPOrderID = table.Column<int>(nullable: false),
                    bankAccName = table.Column<string>(maxLength: 50, nullable: false),
                    bankAccNo = table.Column<string>(maxLength: 50, nullable: false),
                    bankBranch = table.Column<string>(maxLength: 50, nullable: false),
                    bankID = table.Column<int>(nullable: false),
                    clearDate = table.Column<DateTime>(nullable: true),
                    docFile = table.Column<string>(maxLength: 200, nullable: true),
                    paymentAmt = table.Column<float>(nullable: false),
                    paymentDate = table.Column<DateTime>(nullable: false),
                    paymentType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TR_Payment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TR_Payment_TR_PPOrder_PPOrderID",
                        column: x => x.PPOrderID,
                        principalTable: "TR_PPOrder",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TR_Payment_LK_Bank_bankID",
                        column: x => x.bankID,
                        principalTable: "LK_Bank",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TR_PaymentDetailEDC",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    MID = table.Column<string>(maxLength: 50, nullable: true),
                    TID = table.Column<string>(maxLength: 50, nullable: true),
                    approvalCode = table.Column<string>(maxLength: 50, nullable: true),
                    paymentID = table.Column<int>(nullable: false),
                    remarks = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TR_PaymentDetailEDC", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TR_PaymentDetailEDC_TR_Payment_paymentID",
                        column: x => x.paymentID,
                        principalTable: "TR_Payment",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TR_Payment_PPOrderID",
                table: "TR_Payment",
                column: "PPOrderID");

            migrationBuilder.CreateIndex(
                name: "IX_TR_Payment_bankID",
                table: "TR_Payment",
                column: "bankID");

            migrationBuilder.CreateIndex(
                name: "IX_TR_PaymentDetailEDC_paymentID",
                table: "TR_PaymentDetailEDC",
                column: "paymentID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TR_PaymentDetailEDC");

            migrationBuilder.DropTable(
                name: "TR_Payment");
        }
    }
}
