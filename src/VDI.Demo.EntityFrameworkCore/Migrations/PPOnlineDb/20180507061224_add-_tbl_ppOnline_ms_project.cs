﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PPOnlineDb
{
    public partial class add_tbl_ppOnline_ms_project : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "termRef",
                table: "TR_PPOrder",
                newName: "termRefID");

            migrationBuilder.AddColumn<int>(
                name: "projectID",
                table: "TR_PPOrder",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "LK_Bank",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    bankName = table.Column<string>(maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LK_Bank", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LK_TermRef",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    termName = table.Column<string>(maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LK_TermRef", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MS_Project",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    banner = table.Column<string>(maxLength: 200, nullable: true),
                    clusterCode = table.Column<string>(maxLength: 5, nullable: false),
                    contactEmail = table.Column<string>(maxLength: 500, nullable: true),
                    endDate = table.Column<DateTime>(nullable: true),
                    logo = table.Column<string>(maxLength: 200, nullable: true),
                    ppAmt = table.Column<float>(nullable: false),
                    projectCode = table.Column<string>(maxLength: 5, nullable: false),
                    projectDesc = table.Column<string>(maxLength: 500, nullable: false),
                    projectName = table.Column<string>(maxLength: 50, nullable: false),
                    siteLink = table.Column<string>(maxLength: 100, nullable: true),
                    sortNo = table.Column<short>(nullable: false),
                    startDate = table.Column<DateTime>(nullable: true),
                    termAndCondition = table.Column<string>(maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MS_Project", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TR_PPOrder_projectID",
                table: "TR_PPOrder",
                column: "projectID");

            migrationBuilder.CreateIndex(
                name: "IX_TR_PPOrder_termRefID",
                table: "TR_PPOrder",
                column: "termRefID");

            migrationBuilder.AddForeignKey(
                name: "FK_TR_PPOrder_MS_Project_projectID",
                table: "TR_PPOrder",
                column: "projectID",
                principalTable: "MS_Project",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TR_PPOrder_LK_TermRef_termRefID",
                table: "TR_PPOrder",
                column: "termRefID",
                principalTable: "LK_TermRef",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TR_PPOrder_MS_Project_projectID",
                table: "TR_PPOrder");

            migrationBuilder.DropForeignKey(
                name: "FK_TR_PPOrder_LK_TermRef_termRefID",
                table: "TR_PPOrder");

            migrationBuilder.DropTable(
                name: "LK_Bank");

            migrationBuilder.DropTable(
                name: "LK_TermRef");

            migrationBuilder.DropTable(
                name: "MS_Project");

            migrationBuilder.DropIndex(
                name: "IX_TR_PPOrder_projectID",
                table: "TR_PPOrder");

            migrationBuilder.DropIndex(
                name: "IX_TR_PPOrder_termRefID",
                table: "TR_PPOrder");

            migrationBuilder.DropColumn(
                name: "projectID",
                table: "TR_PPOrder");

            migrationBuilder.RenameColumn(
                name: "termRefID",
                table: "TR_PPOrder",
                newName: "termRef");
        }
    }
}
