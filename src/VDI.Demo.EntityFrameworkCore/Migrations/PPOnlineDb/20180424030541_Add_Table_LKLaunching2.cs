﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PPOnlineDb
{
    public partial class Add_Table_LKLaunching2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "LK_RefundReason",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    refundReason = table.Column<string>(maxLength: 1, nullable: false),
                    refundReasonName = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LK_RefundReason", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TR_PriorityPassRefund",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    namaBank = table.Column<string>(maxLength: 50, nullable: false),
                    namaRek = table.Column<string>(maxLength: 50, nullable: false),
                    noRek = table.Column<string>(maxLength: 50, nullable: false),
                    priorityPassID = table.Column<int>(nullable: false),
                    refundReasonID = table.Column<int>(nullable: false),
                    refundReasonOthers = table.Column<string>(maxLength: 200, nullable: false),
                    refundRef = table.Column<int>(nullable: false),
                    refundTime = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TR_PriorityPassRefund", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TR_PriorityPassRefund_TR_PriorityPass_priorityPassID",
                        column: x => x.priorityPassID,
                        principalTable: "TR_PriorityPass",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TR_PriorityPassRefund_LK_RefundReason_refundReasonID",
                        column: x => x.refundReasonID,
                        principalTable: "LK_RefundReason",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TR_PriorityPassRefund_priorityPassID",
                table: "TR_PriorityPassRefund",
                column: "priorityPassID");

            migrationBuilder.CreateIndex(
                name: "IX_TR_PriorityPassRefund_refundReasonID",
                table: "TR_PriorityPassRefund",
                column: "refundReasonID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TR_PriorityPassRefund");

            migrationBuilder.DropTable(
                name: "LK_RefundReason");
        }
    }
}
