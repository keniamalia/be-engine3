﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PPOnlineDb
{
    public partial class Add_Table_TR_PPOrder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TR_PPOrder",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    noted = table.Column<string>(maxLength: 500, nullable: false),
                    orderCode = table.Column<string>(maxLength: 20, nullable: false),
                    orderRef = table.Column<int>(nullable: false),
                    orderStatusID = table.Column<int>(nullable: false),
                    preferredType = table.Column<int>(nullable: false),
                    qty = table.Column<int>(nullable: false),
                    refferalPsCode = table.Column<string>(maxLength: 8, nullable: true),
                    termRef = table.Column<int>(nullable: false),
                    userRef = table.Column<int>(nullable: false),
                    userReinputBy = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TR_PPOrder", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TR_PPOrder_LK_OrderStatus_orderStatusID",
                        column: x => x.orderStatusID,
                        principalTable: "LK_OrderStatus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TR_PPOrder_orderStatusID",
                table: "TR_PPOrder",
                column: "orderStatusID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TR_PPOrder");
        }
    }
}
