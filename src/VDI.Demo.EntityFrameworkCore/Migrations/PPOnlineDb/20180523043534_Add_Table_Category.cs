﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PPOnlineDb
{
    public partial class Add_Table_Category : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "categoryTypeID",
                table: "MS_Project",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "LK_CategoryType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    categoryType = table.Column<int>(nullable: false),
                    categoryTypeName = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LK_CategoryType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LK_CategoryTypePreferred",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    categoryTypeID = table.Column<int>(nullable: false),
                    preferredName = table.Column<string>(maxLength: 30, nullable: false),
                    preferredType = table.Column<int>(nullable: false),
                    projectID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LK_CategoryTypePreferred", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LK_CategoryTypePreferred_LK_CategoryType_categoryTypeID",
                        column: x => x.categoryTypeID,
                        principalTable: "LK_CategoryType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LK_CategoryTypePreferred_MS_Project_projectID",
                        column: x => x.projectID,
                        principalTable: "MS_Project",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MS_Project_categoryTypeID",
                table: "MS_Project",
                column: "categoryTypeID");

            migrationBuilder.CreateIndex(
                name: "IX_LK_CategoryTypePreferred_categoryTypeID",
                table: "LK_CategoryTypePreferred",
                column: "categoryTypeID");

            migrationBuilder.CreateIndex(
                name: "IX_LK_CategoryTypePreferred_projectID",
                table: "LK_CategoryTypePreferred",
                column: "projectID");

            migrationBuilder.AddForeignKey(
                name: "FK_MS_Project_LK_CategoryType_categoryTypeID",
                table: "MS_Project",
                column: "categoryTypeID",
                principalTable: "LK_CategoryType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MS_Project_LK_CategoryType_categoryTypeID",
                table: "MS_Project");

            migrationBuilder.DropTable(
                name: "LK_CategoryTypePreferred");

            migrationBuilder.DropTable(
                name: "LK_CategoryType");

            migrationBuilder.DropIndex(
                name: "IX_MS_Project_categoryTypeID",
                table: "MS_Project");

            migrationBuilder.DropColumn(
                name: "categoryTypeID",
                table: "MS_Project");
        }
    }
}
