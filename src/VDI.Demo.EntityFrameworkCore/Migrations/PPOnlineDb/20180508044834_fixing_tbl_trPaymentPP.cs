﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PPOnlineDb
{
    public partial class fixing_tbl_trPaymentPP : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "paymentAmt",
                table: "TR_Payment",
                nullable: false,
                oldClrType: typeof(float));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<float>(
                name: "paymentAmt",
                table: "TR_Payment",
                nullable: false,
                oldClrType: typeof(double));
        }
    }
}
