﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PPOnlineDb
{
    public partial class fixing_trpporderppno : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "paymentTypeName",
                table: "TR_PPOrderPPNo",
                newName: "PPNo");

            migrationBuilder.RenameColumn(
                name: "paymentTypeName",
                table: "LK_OrderStatus",
                newName: "orderStatusName");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "PPNo",
                table: "TR_PPOrderPPNo",
                newName: "paymentTypeName");

            migrationBuilder.RenameColumn(
                name: "orderStatusName",
                table: "LK_OrderStatus",
                newName: "paymentTypeName");
        }
    }
}
