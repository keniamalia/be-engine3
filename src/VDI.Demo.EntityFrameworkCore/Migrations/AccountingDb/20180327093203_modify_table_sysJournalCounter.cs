﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.AccountingDb
{
    public partial class modify_table_sysJournalCounter : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_SYS_JournalCounter",
                table: "SYS_JournalCounter");

            migrationBuilder.AlterColumn<string>(
                name: "journalCode",
                table: "SYS_JournalCounter",
                maxLength: 30,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 30);

            migrationBuilder.AddPrimaryKey(
                name: "PK_SYS_JournalCounter",
                table: "SYS_JournalCounter",
                column: "accCode");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_SYS_JournalCounter",
                table: "SYS_JournalCounter");

            migrationBuilder.AlterColumn<string>(
                name: "journalCode",
                table: "SYS_JournalCounter",
                maxLength: 30,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 30,
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_SYS_JournalCounter",
                table: "SYS_JournalCounter",
                columns: new[] { "accCode", "journalCode" });
        }
    }
}
