﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PropertySystemDb
{
    public partial class Update_TR_BookingDocument_For_UploadSignature2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "tandaTerimaBy",
                table: "TR_BookingDocument");

            migrationBuilder.DropColumn(
                name: "uploadCity",
                table: "TR_BookingDocument");

            migrationBuilder.RenameColumn(
                name: "uploadFile",
                table: "TR_BookingDocument",
                newName: "signedDocumentFile");

            migrationBuilder.RenameColumn(
                name: "uploadDate",
                table: "TR_BookingDocument",
                newName: "signedDocumentDate");

            migrationBuilder.AddColumn<DateTime>(
                name: "signatureDate",
                table: "TR_BookingDocument",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "signatureDate",
                table: "TR_BookingDocument");

            migrationBuilder.RenameColumn(
                name: "signedDocumentFile",
                table: "TR_BookingDocument",
                newName: "uploadFile");

            migrationBuilder.RenameColumn(
                name: "signedDocumentDate",
                table: "TR_BookingDocument",
                newName: "uploadDate");

            migrationBuilder.AddColumn<string>(
                name: "tandaTerimaBy",
                table: "TR_BookingDocument",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "uploadCity",
                table: "TR_BookingDocument",
                maxLength: 70,
                nullable: true);
        }
    }
}
