﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;

namespace VDI.Demo.Migrations.PropertySystemDb.Seed
{
    public class EntityCreator
    {
        private readonly PropertySystemDbContext _context;

        public EntityCreator(PropertySystemDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateSeed();
        }

        private void CreateSeed()
        {
            List<MS_Entity> InitialEntity = new List<MS_Entity>();
            List<String> listEntity = new List<string>()
            {
                "1,Lippo Karawaci"
            };
            foreach (var item in listEntity)
            {
                var EntitySeparate = item.Split(',');

                var EntityPush = new MS_Entity()
                {
                    entityCode = EntitySeparate[0],
                    entityName = EntitySeparate[1]
                };

                InitialEntity.Add(EntityPush);
            }

            foreach (var Entity in InitialEntity)
            {
                AddIfNotExists(Entity);
            }
        }

        private void AddIfNotExists(MS_Entity Entity)
        {
            if (_context.MS_Entity.Any(l => l.entityCode == Entity.entityCode && l.entityName == Entity.entityName))
            {
                return;
            }

            _context.MS_Entity.Add(Entity);

            _context.SaveChanges();
        }
    }
}
