﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.PropertySystemDB.LippoMaster;
using VDI.Demo.PropertySystemDB.OnlineBooking.PPOnline;

namespace VDI.Demo.Migrations.PropertySystemDb.Seed
{
    public class SalesEventCreator
    {
        private readonly PropertySystemDbContext _context;

        public SalesEventCreator(PropertySystemDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateSeed();
        }

        private void CreateSeed()
        {
            List<MS_SalesEvent> InitialSalesEvent = new List<MS_SalesEvent>();
            List<String> listSalesEvent = new List<string>()
            {
                "00001,Daily,40",
                "00002,Launching,35",
                "00003,Reserved By Mobile Booking,30",
                "00004,Internal Launching,0",
                "00005,Grand Briefing,5",
                "00006,Grand Preview,10",
                "00007,Pasca Grand Preview,15",
                "00008,Grand Breaking,20",
                "00009,Pasca Grand Breaking,25",
                "00010,Private Viewing,45",
                "00011,Customer Gathering,50",
                "00012,Online Booking,60"
            };
            foreach (var item in listSalesEvent)
            {
                var SalesEventSeparate = item.Split(',');

                var SalesEventPush = new MS_SalesEvent()
                {
                    EntityID = 1,
                    eventCode = SalesEventSeparate[0],
                    eventName = SalesEventSeparate[1],
                    sortNo = Convert.ToInt16(SalesEventSeparate[2])
                };

                InitialSalesEvent.Add(SalesEventPush);
            }

            foreach (var SalesEvent in InitialSalesEvent)
            {
                AddIfNotExists(SalesEvent);
            }
        }

        private void AddIfNotExists(MS_SalesEvent SalesEvent)
        {
            if (_context.MS_SalesEvent.Any(l => l.eventCode == SalesEvent.eventCode
                                           && l.eventName == SalesEvent.eventName && l.sortNo == SalesEvent.sortNo 
                                           && l.EntityID == SalesEvent.EntityID))
            {
                return;
            }

            _context.MS_SalesEvent.Add(SalesEvent);

            _context.SaveChanges();
        }
    }
}
