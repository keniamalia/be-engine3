﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.PropertySystemDB.LippoMaster;

namespace VDI.Demo.Migrations.PropertySystemDb.Seed
{
    public class LetterTypeCreator
    {
        private readonly PropertySystemDbContext _context;

        public LetterTypeCreator(PropertySystemDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateSeed();
        }

        private void CreateSeed()
        {
            List<LK_LetterType> InitialLetterType = new List<LK_LetterType>();
            List<String> listLetterType = new List<string>()
            {
                "DUD,Daftar Unit Dibatalkan,21",
                "SP1,Surat Peringatan 1,14",
                "SP2,Surat Peringatan 2,14",
                "SPP,Surat Peringatan Pembatalan,14"
            };
            foreach (var item in listLetterType)
            {
                var LetterTypeSeparate = item.Split(',');

                var LetterTypePush = new LK_LetterType()
                {
                    letterType = LetterTypeSeparate[0],
                    letterDesc = LetterTypeSeparate[1],
                    duration = Convert.ToInt32(LetterTypeSeparate[2])
                };

                InitialLetterType.Add(LetterTypePush);
            }

            foreach (var LetterType in InitialLetterType)
            {
                AddIfNotExists(LetterType);
            }
        }

        private void AddIfNotExists(LK_LetterType LetterType)
        {
            if (_context.LK_LetterType.Any(l => l.letterType == LetterType.letterType))
            {
                return;
            }

            _context.LK_LetterType.Add(LetterType);

            _context.SaveChanges();
        }
    }
}
