﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.PropertySystemDB.LippoMaster;

namespace VDI.Demo.Migrations.PropertySystemDb.Seed
{
    public class DocumentCreator
    {
        private readonly PropertySystemDbContext _context;

        public DocumentCreator(PropertySystemDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateSeed();
        }

        private void CreateSeed()
        {
            List<MS_DocumentPS> initialDocument = new List<MS_DocumentPS>();
            List<String> listDocument = new List<string>()
            {
                "PPPU,PENEGASAN DAN PERSETUJUAN PEMESANAN UNIT,1",
            };
            foreach (var item in listDocument)
            {
                var documentSeparate = item.Split(',');

                var documentPush = new MS_DocumentPS()
                {
                    docCode = documentSeparate[0],
                    docName = documentSeparate[1],
                    entityID = Int32.Parse(documentSeparate[2])
                };

                initialDocument.Add(documentPush);
            }

            foreach (var document in initialDocument)
            {
                AddIfNotExists(document);
            }
        }

        private void AddIfNotExists(MS_DocumentPS document)
        {
            if (_context.MS_DocumentPS.Any(l => l.docCode == document.docCode))
            {
                return;
            }

            _context.MS_DocumentPS.Add(document);

            _context.SaveChanges();
        }
    }
}
