﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.PropertySystemDB.LippoMaster;

namespace VDI.Demo.Migrations.PropertySystemDb.Seed
{
    public class FPStatusCreator
    {
        private readonly PropertySystemDbContext _context;

        public FPStatusCreator(PropertySystemDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateSeed();
        }

        private void CreateSeed()
        {
            List<MS_FPStatus> InitialFPStatus = new List<MS_FPStatus>();
            List<String> listFPStatus = new List<string>()
            {
                "1,Done",
                "2,Kehabisan FP",
                "3,Tidak TAX"
            };
            foreach (var item in listFPStatus)
            {
                var FPStatusSeparate = item.Split(',');

                var FPStatusPush = new MS_FPStatus()
                {
                    statusCode = FPStatusSeparate[0],
                    statusDesc = FPStatusSeparate[1]
                };

                InitialFPStatus.Add(FPStatusPush);
            }

            foreach (var FPStatus in InitialFPStatus)
            {
                AddIfNotExists(FPStatus);
            }
        }

        private void AddIfNotExists(MS_FPStatus FPStatus)
        {
            if (_context.MS_FPStatus.Any(l => l.statusCode == FPStatus.statusCode))
            {
                return;
            }

            _context.MS_FPStatus.Add(FPStatus);

            _context.SaveChanges();
        }
    }
}
