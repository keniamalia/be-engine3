﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.PropertySystemDB.LippoMaster;
using VDI.Demo.PropertySystemDB.OnlineBooking.PPOnline;

namespace VDI.Demo.Migrations.PropertySystemDb.Seed
{
    public class TransfromCreator
    {
        private readonly PropertySystemDbContext _context;

        public TransfromCreator(PropertySystemDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateSeed();
        }

        private void CreateSeed()
        {
            List<MS_TransFrom> InitialTransfrom = new List<MS_TransFrom>();
            List<String> listTransfrom = new List<string>()
            {
                string.Empty + ",00001,Kantor Pemasaran,Kantor Pemasaran",
                "00001,00002,Kantor Pemasaran - Lippo Karawaci,Lippo Karawaci",
                "00001,00003,Kantor Pemasaran - Depok,Depok",
                "00001,00004,Kantor Pemasaran - Medan,Medan",
                "00001,00005,Kantor Pemasaran - Waru,Waru",
                string.Empty + ",00006,Exhibition,Exhibition",
                "00006,00007,Exhibition - Grand Preview Kemang,Grand Preview Kemang",
                "00001,00008,Kantor Pemasaran - Kemang,Kemang",
                string.Empty + ",00009,Others,Others",
                "00001,00010,Kantor Pemasaran - Bellanova,Bellanova",
                "00001,00011,Kantor Pemasaran - Malang,Malang",
                "00001,00012,Kantor Pemasaran - The St Moritz,Plaza Semanggi",
                "00006,00013,Private Viewing - The St Moritz,Private Viewing",
                "00001,00014,Kantor Pemasaran - Lampung,Lampung",
                "00001,00015,Kantor Pemasaran - The St Moritz,St Moritz Puri",
                string.Empty + ",00016,Kantor Pemasaran - Lippo Cikarang,Lippo Cikarang",
                "00001,00017,Kantor Pemasaran - GTC Makasar,GTC Makasar",
                "00001,00018,Kantor Pemasaran - GMTD Makasar,GMTD Makasar",
                "00001,00019,Shophouses Metropolis Town Square,Shophouses Metropolis Town Square",
                "00001,00020,Kantor Pemasaran - Fifth Avenue Makassar,Fifth Avenue Makassar",
                "00006,00021,St. Moritz Makassar,St. Moritz Makassar",
                "00006,00022,Grand Preview Holland Village,Grand Preview Holland Village",
                "00006,00023,Kantor Pemasaran - Monaco Bay,Monaco Bay Residences",
                "00006,00024,Kantor Pemasaran - Holland Village Manado,Grand Preview HV Manado",
                "00006,00025,Rancang Komunika Mandiri Makasar,Rancang Komunika Mandiri Makasar",
                string.Empty + ",00026,Online Booking,Online Booking"
            };

            foreach (var item in listTransfrom)
            {
                var Transfrom = item.Split(',');

                var TransfromPush = new MS_TransFrom()
                {
                    entityID = 1,
                    parentTransCode = Transfrom[0],
                    transCode = Transfrom[1],
                    transDesc = Transfrom[2],
                    transName = Transfrom[3]
                };

                InitialTransfrom.Add(TransfromPush);
            }

            foreach (var Transfrom in InitialTransfrom)
            {
                AddIfNotExists(Transfrom);
            }
        }

        private void AddIfNotExists(MS_TransFrom Transfrom)
        {
            if (_context.MS_Transfrom.Any(l => l.parentTransCode == Transfrom.parentTransCode
                                          && l.transCode == Transfrom.transCode &&
                                          l.transDesc == Transfrom.transDesc &&
                                          l.transName == Transfrom.transName && 
                                          l.entityID == Transfrom.entityID))
            {
                return;
            }

            _context.MS_Transfrom.Add(Transfrom);

            _context.SaveChanges();
        }
    }
}
