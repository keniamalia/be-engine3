﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.PropertySystemDB.Pricing;

namespace VDI.Demo.Migrations.PropertySystemDb.Seed
{
    public class FormulaDPCreator
    {
        private readonly PropertySystemDbContext _context;

        public FormulaDPCreator(PropertySystemDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateSeed();
        }

        private void CreateSeed()
        {
            List<LK_FormulaDP> InitialFormulaDP = new List<LK_FormulaDP>();
            List<String> listFormulaDP = new List<string>()
            {
                "Selling Price,1",
                "Selling Price - Booking Fee,2"
            };
            foreach (var item in listFormulaDP)
            {
                var FormulaDPSeparate = item.Split(',');

                var FormulaDPPush = new LK_FormulaDP()
                {
                    formulaDPDesc = FormulaDPSeparate[0],
                    formulaDPType = FormulaDPSeparate[1]
                };

                InitialFormulaDP.Add(FormulaDPPush);
            }

            foreach (var FormulaDP in InitialFormulaDP)
            {
                AddIfNotExists(FormulaDP);
            }
        }

        private void AddIfNotExists(LK_FormulaDP FormulaDP)
        {
            if (_context.LK_FormulaDP.Any(l => l.formulaDPDesc == FormulaDP.formulaDPDesc && l.formulaDPType == FormulaDP.formulaDPType))
            {
                return;
            }

            _context.LK_FormulaDP.Add(FormulaDP);

            _context.SaveChanges();
        }
    }
}
