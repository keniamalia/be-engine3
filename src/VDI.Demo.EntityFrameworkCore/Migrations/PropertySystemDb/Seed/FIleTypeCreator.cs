﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.PropertySystemDB.OnlineBooking.ProjectInfo;
using VDI.Demo.PropertySystemDB.OnlineBooking.PropertySystem;

namespace VDI.Demo.Migrations.PropertySystemDb.Seed
{
    public class FIleTypeCreator
    {
        private readonly PropertySystemDbContext _context;

        public FIleTypeCreator(PropertySystemDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateSeed();
        }

        private void CreateSeed()
        {
            List<LK_FileType> InitialFileType = new List<LK_FileType>();
            List<String> listFileType = new List<string>()
            {
                "Image",
                "Video"
            };
            foreach (var item in listFileType)
            {
                var FileTypeSeparate = item.Split(',');

                var FileTypePush = new LK_FileType()
                {
                    name = FileTypeSeparate[0]
                };

                InitialFileType.Add(FileTypePush);
            }

            foreach (var FileType in InitialFileType)
            {
                AddIfNotExists(FileType);
            }
        }

        private void AddIfNotExists(LK_FileType FileType)
        {
            if (_context.LK_FileType.Any(l => l.name == FileType.name))
            {
                return;
            }

            _context.LK_FileType.Add(FileType);

            _context.SaveChanges();
        }
    }
}
