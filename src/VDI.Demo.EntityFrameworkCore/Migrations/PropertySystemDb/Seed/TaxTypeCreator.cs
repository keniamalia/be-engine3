﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.PropertySystemDB.LippoMaster;
using VDI.Demo.PropertySystemDB.OnlineBooking.PPOnline;

namespace VDI.Demo.Migrations.PropertySystemDb.Seed
{
    public class TaxTypeCreator
    {
        private readonly PropertySystemDbContext _context;

        public TaxTypeCreator(PropertySystemDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateSeed();
        }

        private void CreateSeed()
        {
            List<MS_TaxType> InitialTaxType = new List<MS_TaxType>();
            List<String> listTaxType = new List<string>()
            {
                "PPh22,PPh22"
            };

            foreach (var item in listTaxType)
            {
                var TaxType = item.Split(',');

                var TaxTypePush = new MS_TaxType()
                {
                    taxTypeCode = TaxType[0],
                    taxTypeDesc = TaxType[1]
                };

                InitialTaxType.Add(TaxTypePush);
            }

            foreach (var TaxType in InitialTaxType)
            {
                AddIfNotExists(TaxType);
            }
        }

        private void AddIfNotExists(MS_TaxType TaxType)
        {
            if (_context.MS_TaxType.Any(l => l.taxTypeCode == TaxType.taxTypeCode
                                        && l.taxTypeDesc == TaxType.taxTypeDesc))
            {
                return;
            }

            _context.MS_TaxType.Add(TaxType);

            _context.SaveChanges();
        }
    }
}
