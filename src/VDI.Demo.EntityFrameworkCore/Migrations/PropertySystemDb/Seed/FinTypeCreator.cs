﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.PropertySystemDB.Pricing;

namespace VDI.Demo.Migrations.PropertySystemDb.Seed
{
    public class FinTypeCreator
    {
        private readonly PropertySystemDbContext _context;

        public FinTypeCreator(PropertySystemDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateSeed();
        }

        private void CreateSeed()
        {
            List<LK_FinType> InitialFinType = new List<LK_FinType>();
            List<String> listFinType = new List<string>()
            {
                "1,A,KPR,0,0,A,1,1,1",
                "1,A-1,KPR (Long Term),0,0,-,1,1,1",
                "1,A-2,KPA Indent,0,0,-,1,1,1",
                "1,A06,KPR DP di cicil 6x,0,0,-,1,1,1",
                "1,A07,KPR DP di cicil 7x,0,0,-,1,1,1",
                "1,A08,KPR DP di cicil 8x,0,0,-,1,1,1",
                "1,A09,KPR DP di cicil 9x,0,0,-,1,1,1",
                "1,A10,KPR DP di cicil 10x,0,0,-,1,1,1",
                "1,A11,KPR DP di cicil 11x,0,0,-,1,1,1",
                "1,A12,KPR DP di cicil 12x,0,0,-,1,1,1",
                "1,A12A,KPA DP dicicil 12x,0,0,-,1,1,1",
                "1,A13,KPR DP di cicil 13x,0,0,-,1,1,1",
                "1,A14,KPR DP di cicil 14x,0,0,-,1,1,1",
                "1,A15,KPR DP di cicil 15x,0,0,-,1,1,1",
                "1,A16,KPR DP di cicil 16x,0,0,-,1,1,1",
                "1,A17,KPR DP di cicil 17x,0,0,-,1,1,1",
                "1,A18,KPR DP di cicil 18x,0,0,-,1,1,1",
                "18,A18B,KPR DP di cicil 18x Tanpa Bunga,0,0,A18,1,1,1",
                "1,A19,KPR DP di cicil 19x,0,0,-,1,1,1",
                "1,A20,KPR DP di cicil 20x,0,0,-,1,1,1",
                "1,A21,KPR DP di cicil 21x,0,0,-,1,1,1",
                "1,A22,KPR DP di cicil 22x,0,0,-,1,1,1",
                "1,A23,KPR DP di cicil 23x,0,0,-,1,1,1",
                "1,A24,KPR DP di cicil 24x,0,0,-,1,1,1",
                "24,A24B,KPR DP di cicil 24x Tanpa Bunga,0,0,A24,1,1,1",
                "1,A25,KPR DP di cicil 25x,0,0,-,1,1,1",
                "1,A26,KPR DP di cicil 26x,0,0,-,1,1,1",
                "1,A27,KPR DP di cicil 27x,0,0,-,1,1,1",
                "1,A28,KPR DP di cicil 28x,0,0,-,1,1,1",
                "1,A29,KPR DP di cicil 29x,0,0,-,1,1,1",
                "1,A30,KPR DP di cicil 30x,0,0,-,1,1,1",
                "1,A31,KPR DP di cicil 31x,0,0,-,1,1,1",
                "1,A32,KPR DP di cicil 32x,0,0,-,1,1,1",
                "1,A33,KPR DP di cicil 33x,0,0,-,1,1,1",
                "1,A34,KPR DP di cicil 34x,0,0,-,1,1,1",
                "1,A35,KPR DP di cicil 35x,0,0,-,1,1,1",
                "1,A36,KPR DP di cicil 36x,0,0,-,1,1,1",
                "30,A60C,KPA Spesial 60x,0,0,A,1,1,1",
                "1,AS,KPR Standard,0,0,A,1,1,1",
                "1,B01,Self Fin (Cash),0,0,B,1.1,1,1",
                "1,B01B,Self Fin (Cash),1,0,-,1.1,1,1",
                "1,B03,Others Self Fin (Cash),0,0,Z,1.1,1,1",
                "1,B06,Cash DP di cicil 6x,0,0,-,1.1,1,1",
                "1,B07,Cash DP di cicil 7x,0,0,-,1.1,1,1",
                "1,B08,Cash DP di cicil 8x,0,0,-,1.1,1,1",
                "1,B09,Cash DP di cicil 9x,0,0,-,1.1,1,1",
                "1,B10,Cash DP di cicil 10x,0,0,-,1.1,1,1",
                "1,B11,Cash DP di cicil 11x,0,0,-,1.1,1,1",
                "1,B12,Cash DP di cicil 12x,0,0,-,1,1,1",
                "1,B13,Cash DP di cicil 13x,0,0,-,1,1,1",
                "1,B14,Cash DP di cicil 14x,0,0,V,1,1,1",
                "1,B15,Cash DP di cicil 15x,0,0,-,1,1,1",
                "1,B16,Cash DP di cicil 16x,0,0,-,1,1,1",
                "1,B17,Cash DP di cicil 17x,0,0,-,1,1,1",
                "1,B18,Cash DP di cicil 18x,0,0,-,1,1,1",
                "1,B19,Cash DP di cicil 19x,0,0,-,1,1,1",
                "1,B20,Cash DP di cicil 20x,0,0,-,1,1,1",
                "1,B21,Cash DP di cicil 21x,0,0,-,1,1,1",
                "1,B22,Cash DP di cicil 22x,0,0,-,1,1,1",
                "1,B23,Cash DP di cicil 23x,0,0,-,1,1,1",
                "1,B24,Cash DP di cicil 24x,0,0,-,1,1,1",
                "1,B25,Cash DP di cicil 25x,0,0,-,0.9,1,1",
                "1,B26,Cash DP di cicil 26x,0,0,-,0.9,1,1",
                "1,B27,Cash DP di cicil 27x,0,0,-,0.9,1,1",
                "1,B28,Cash DP di cicil 28x,0,0,-,0.9,1,1",
                "1,B29,Cash DP di cicil 29x,0,0,-,0.9,1,1",
                "1,B30,Cash DP di cicil 30x,0,0,-,0.9,1,1",
                "1,B31,Cash DP di cicil 31x,0,0,-,0.9,1,1",
                "1,B32,Cash DP di cicil 32x,0,0,-,0.9,1,1",
                "1,B33,Cash DP di cicil 33x,0,0,-,0.9,1,1",
                "1,B34,Cash DP di cicil 34x,0,0,-,0.9,1,1",
                "1,B35,Cash DP di cicil 35x,0,0,-,0.9,1,1",
                "1,B36,Cash DP di cicil 36x,0,0,-,0.9,1,1",
                "1,C01,Fin 1x,0,0,-,1.1,1,1",
                "1,C01B,Fin 1x At Need,0,0,-,1,1,1",
                "2,C02,Fin 2x,0,0,-,1.1,1,1",
                "3,C03,Fin 3x,0,0,P,1.1,1,1",
                "4,C04,Fin 4x,0,0,-,1.1,1,1",
                "5,C05,Fin 5x,0,0,-,1.1,1,1",
                "6,C06,Fin 6x,0,0,E,1.1,1,1",
                "7,C07,Fin 7x,0,0,N,1.1,1,1",
                "8,C08,Fin 8x,0,0,G,1.1,1,1",
                "9,C09,Fin 9x,0,0,-,1.1,1,1",
                "10,C10,Fin 10x,0,0,I,1.1,1,1",
                "11,C11,Fin 11x,0,0,W,1.1,1,1",
                "12,C12,Fin 12x,0,0,-,1,1,1",
                "120,C120,Fin 120x,0,0,-,1,1,1",
                "12,C12C,Fin 12x,0,1,C,1,1,1",
                "12,C12F,Fin 12 Factoring,0,0,-,1,1,1",
                "13,C13,Fin 13x,0,0,-,1,1,1",
                "14,C14,Fin 14x,0,0,-,1,1,1",
                "15,C15,Fin 15x,0,0,H,1,1,1",
                "16,C16,Fin 16x,0,0,-,1,1,1",
                "17,C17,Fin 17x,0,0,-,1,1,1",
                "18,C18,Fin 18x,0,0,D,1,1,1",
                "18,C18A,Fin 18x Tanpa Bunga,0,0,C18,1,1,1",
                "19,C19,Fin 19x,0,0,-,1,1,1",
                "20,C20,Fin 20x,0,0,-,1,1,1",
                "18,C20A,Special Installment 20x,0,0,-,0.9,1,1",
                "21,C21,Fin 21x,0,0,-,1,1,1",
                "22,C22,Fin 22x,0,0,-,1,1,1",
                "23,C23,Fin 23x,0,0,-,1,1,1",
                "24,C24,Fin 24x,0,0,F,1,1,1",
                "18,C24A,Fin 24x Tanpa Bunga,0,0,C24,1,1,1",
                "24,C24B,Special Installment 24X,0,0,-,0.9,1,1",
                "24,C24F,Fin 24 Factoring,0,0,-,1,1,1",
                "24,C24S,Fin 24x Standard,0,0,C24,1,1,1",
                "25,C25,Fin 25x,0,0,-,0.9,1,1",
                "23,C25A,Special Installment 25x,0,0,-,1,1,1",
                "26,C26,Fin 26x,0,0,-,0.9,1,1",
                "27,C27,Fin 27x,0,0,-,0.9,1,1",
                "28,C28,Fin 28x,0,0,-,0.9,1,1",
                "29,C29,Fin 29x,0,0,-,0.9,1,1",
                "30,C30,Fin 30x,0,0,-,0.9,1,1",
                "32,C32,Fin 32x,0,0,-,0.9,1,1",
                "34,C34,Fin 34x,0,0,-,0.9,1,1",
                "35,C35,Fin 35x,0,0,-,0.9,1,1",
                "36,C36,Fin 36x,0,0,J,0.9,1,1",
                "36,C36A,Special Installment 36X,0,0,J,0.9,1,1",
                "36,C36F,Fin 36 Factoring,0,0,-,0.9,1,1",
                "36,C36M,Special Installment 36X Mortgage,0,0,J,1,1,1",
                "36,C36MF,Spesial Installment 36X Mortgage Factoring,0,0,-,1,1,1",
                "36,C36S,Fin 36x Standard,0,0,C36,0.9,1,1",
                "38,C38,Fin 38x,0,0,X,0.9,1,1",
                "39,C39,Fin 39x,0,0,-,0.9,1,1",
                "40,C40,Fin 40x,0,0,Q,0.9,1,1",
                "42,C42,Fin 42x,0,0,-,0.9,1,1",
                "45,C45,Fin 45x,0,0,K,0.9,1,1",
                "46,C46,Fin 46x,0,0,-,0.9,1,1",
                "48,C48,Fin 48x,0,0,O,0.9,1,1",
                "48,C48F,Fin 48 Factoring,0,0,-,0.9,1,1",
                "36,C48S,Fin 48x Standard,0,0,C48,0.9,1,1",
                "49,C49,Fin 49x,0,0,-,0.9,1,1",
                "50,C50,Fin 50x,0,0,L,0.9,1,1",
                "55,C55,Fin 55x,0,0,R,0.9,1,1",
                "57,C57,Fin 57x,0,0,Y,0.9,1,1",
                "60,C60,Fin 60x,0,0,M,0.9,1,1",
                "70,C70,Fin 70x,0,0,-,0.9,1,1",
                "2,F02,Fin 2x,0,0,T,0.9,1,1",
                "1,F24,Fin 24x,0,0,-,0.9,1,1",
                "30,F30,Fin 30x,0,0,S,0.9,1,1",
                "56,F56,Fin 56x,0,0,U,0.9,1,1"
            };
            foreach (var item in listFinType)
            {
                var FinTypeSeparate = item.Split(',');

                var FinTypePush = new LK_FinType()
                {
                    finTimes = Convert.ToInt16(FinTypeSeparate[0]),
                    finTypeCode = FinTypeSeparate[1],
                    finTypeDesc = FinTypeSeparate[2],
                    isCashStd = FinTypeSeparate[3] == "1" ? true : false,
                    isCommStd = FinTypeSeparate[4] == "1" ? true : false,
                    oldFinTypeCode = FinTypeSeparate[5],
                    pctComm = Convert.ToDouble(FinTypeSeparate[6]),
                    pctCommLC = Convert.ToDouble(FinTypeSeparate[7]),
                    pctCommTB = Convert.ToDouble(FinTypeSeparate[8])
                };

                InitialFinType.Add(FinTypePush);
            }

            foreach (var FinType in InitialFinType)
            {
                AddIfNotExists(FinType);
            }
        }

        private void AddIfNotExists(LK_FinType FinType)
        {
            if (_context.LK_FinType.Any(l => l.finTypeCode == FinType.finTypeCode))
            {
                return;
            }

            _context.LK_FinType.Add(FinType);

            _context.SaveChanges();
        }
    }
}
