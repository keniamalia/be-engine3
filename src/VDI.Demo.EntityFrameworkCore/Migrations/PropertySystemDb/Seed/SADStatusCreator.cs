﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.PropertySystemDB.LippoMaster;

namespace VDI.Demo.Migrations.PropertySystemDb.Seed
{
    public class SADStatusCreator
    {
        private readonly PropertySystemDbContext _context;

        public SADStatusCreator(PropertySystemDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateSeed();
        }

        private void CreateSeed()
        {
            List<LK_SADStatus> InitialSADStatus = new List<LK_SADStatus>();
            List<String> listSADStatus = new List<string>()
            {
                "-,-"
            };
            foreach (var item in listSADStatus)
            {
                var SADStatusSeparate = item.Split(',');

                var SADStatusPush = new LK_SADStatus()
                {
                    statusCode = SADStatusSeparate[0],
                    statusDesc = SADStatusSeparate[1]
                };

                InitialSADStatus.Add(SADStatusPush);
            }

            foreach (var SADStatus in InitialSADStatus)
            {
                AddIfNotExists(SADStatus);
            }
        }

        private void AddIfNotExists(LK_SADStatus SADStatus)
        {
            if (_context.LK_SADStatus.Any(l => l.statusCode == SADStatus.statusCode))
            {
                return;
            }

            _context.LK_SADStatus.Add(SADStatus);

            _context.SaveChanges();
        }
    }
}
