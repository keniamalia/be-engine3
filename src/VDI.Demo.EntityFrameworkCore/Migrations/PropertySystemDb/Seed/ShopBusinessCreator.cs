﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.PropertySystemDB.LippoMaster;
using VDI.Demo.PropertySystemDB.OnlineBooking.PPOnline;

namespace VDI.Demo.Migrations.PropertySystemDb.Seed
{
    public class ShopBusinessCreator
    {
        private readonly PropertySystemDbContext _context;

        public ShopBusinessCreator(PropertySystemDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateSeed();
        }

        private void CreateSeed()
        {
            List<MS_ShopBusiness> InitialShopBusiness = new List<MS_ShopBusiness>();
            List<String> listShopBusiness = new List<string>()
            {
                "001|Not Defined|0",
                "002|APOTIK|0",
                "003|KIDS & TOYS|0",
                "004|BEAUTY & COSMETICS|0",
                "005|BIG TENANT|0",
                "006|BOOK STORE|0",
                "007|CASSETTE STORE|0",
                "008|MUSIC, ELECTRONICS & PHOTOGRAPHY|0",
                "009|FASHION|0",
                "010|FASHION-BAG|0",
                "011|FASHION-COSMETIC|0",
                "012|FASHION-SHOES|0",
                "013|FASHION-WATCH|0",
                "014|FOOD & DRINK|0",
                "015|GIFT & ACCESSORIES|0",
                "016|HEALTH|0",
                "017|HOME APPLIANCES|0",
                "018|TELECOMMUNICATION|0",
                "019|JEWELLERY|0",
                "020|MISCELLANOUS|0",
                "021|MUSIC|0",
                "022|OFFICE|0",
                "023|OPTIK|0",
                "024|PET SHOP|0",
                "025|PHOTO STUDIO|0",
                "026|PLAY STATION|0",
                "027|SPORTS|0",
                "028|TOUR & TRAVEL|0",
                "029|TOYS|0",
                "030|WARTEL|0"
            };
            foreach (var item in listShopBusiness)
            {
                var ShopBusiness = item.Split('|');

                var ShopBusinessPush = new MS_ShopBusiness()
                {
                    entityID = 1,
                    shopBusinessCode = ShopBusiness[0],
                    shopBusinessName = ShopBusiness[1],
                    sort = Convert.ToInt16(ShopBusiness[2])
                };

                InitialShopBusiness.Add(ShopBusinessPush);
            }

            foreach (var ShopBusiness in InitialShopBusiness)
            {
                AddIfNotExists(ShopBusiness);
            }
        }

        private void AddIfNotExists(MS_ShopBusiness ShopBusiness)
        {
            if (_context.MS_ShopBusiness.Any(l => l.shopBusinessCode == ShopBusiness.shopBusinessCode))
            {
                return;
            }

            _context.MS_ShopBusiness.Add(ShopBusiness);

            _context.SaveChanges();
        }
    }
}
