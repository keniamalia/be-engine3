﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.PropertySystemDB.LippoMaster;

namespace VDI.Demo.Migrations.PropertySystemDb.Seed
{
    public class LetterStatusCreator
    {
        private readonly PropertySystemDbContext _context;

        public LetterStatusCreator(PropertySystemDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateSeed();
        }

        private void CreateSeed()
        {
            List<LK_LetterStatus> InitialLetterStatus = new List<LK_LetterStatus>();
            List<String> listLetterStatus = new List<string>()
            {
                "C,Cancelled",
                "E,No Email",
                "O,OK",
                "U,Unknown"
            };
            foreach (var item in listLetterStatus)
            {
                var LetterStatusSeparate = item.Split(',');

                var LetterStatusPush = new LK_LetterStatus()
                {
                    entityID = 1,
                    letterStatusCode = LetterStatusSeparate[0],
                    letterStatusDesc = LetterStatusSeparate[1]
                };

                InitialLetterStatus.Add(LetterStatusPush);
            }

            foreach (var LetterStatus in InitialLetterStatus)
            {
                AddIfNotExists(LetterStatus);
            }
        }

        private void AddIfNotExists(LK_LetterStatus LetterStatus)
        {
            if (_context.LK_LetterStatus.Any(l => l.letterStatusCode == LetterStatus.letterStatusCode))
            {
                return;
            }

            _context.LK_LetterStatus.Add(LetterStatus);

            _context.SaveChanges();
        }
    }
}
