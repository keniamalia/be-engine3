﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.PropertySystemDB.Pricing;

namespace VDI.Demo.Migrations.PropertySystemDb.Seed
{
    public class DPCalcCreator
    {
        private readonly PropertySystemDbContext _context;

        public DPCalcCreator(PropertySystemDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateSeed();
        }

        private void CreateSeed()
        {
            List<LK_DPCalc> InitialDPCalc = new List<LK_DPCalc>();
            List<String> listDPCalc = new List<string>()
            {
                "Net Price,1",
                "Net Net Price,2"
            };
            foreach (var item in listDPCalc)
            {
                var DPCalcSeparate = item.Split(',');

                var DPCalcPush = new LK_DPCalc()
                {
                    DPCalcDesc = DPCalcSeparate[0],
                    DPCalcType = DPCalcSeparate[1]
                };

                InitialDPCalc.Add(DPCalcPush);
            }

            foreach (var DPCalc in InitialDPCalc)
            {
                AddIfNotExists(DPCalc);
            }
        }

        private void AddIfNotExists(LK_DPCalc DPCalc)
        {
            if (_context.LK_DPCalc.Any(l => l.DPCalcDesc == DPCalc.DPCalcDesc && l.DPCalcType == DPCalc.DPCalcType))
            {
                return;
            }

            _context.LK_DPCalc.Add(DPCalc);

            _context.SaveChanges();
        }
    }
}
