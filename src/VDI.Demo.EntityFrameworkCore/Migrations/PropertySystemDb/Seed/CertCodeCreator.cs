﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;

namespace VDI.Demo.Migrations.PropertySystemDb.Seed
{
    public class CertCodeCreator
    {
        private readonly PropertySystemDbContext _context;

        public CertCodeCreator(PropertySystemDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateSeed();
        }

        private void CreateSeed()
        {
            List<LK_CertCode> InitialCertCode = new List<LK_CertCode>();
            List<String> listCertCode = new List<string>()
            {
                "C,Sertifikat",
                "N,Tidak Ber Sertifikat"
            };
            foreach (var item in listCertCode)
            {
                var CertCodeSeparate = item.Split(',');

                var CertCodePush = new LK_CertCode()
                {
                    certCode = CertCodeSeparate[0],
                    certDesc = CertCodeSeparate[1]
                };

                InitialCertCode.Add(CertCodePush);
            }

            foreach (var CertCode in InitialCertCode)
            {
                AddIfNotExists(CertCode);
            }
        }

        private void AddIfNotExists(LK_CertCode CertCode)
        {
            if (_context.LK_CertCode.Any(l => l.certCode == CertCode.certCode))
            {
                return;
            }

            _context.LK_CertCode.Add(CertCode);

            _context.SaveChanges();
        }
    }
}
