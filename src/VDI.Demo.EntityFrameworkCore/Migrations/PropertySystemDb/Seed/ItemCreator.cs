﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;

namespace VDI.Demo.Migrations.PropertySystemDb.Seed
{
    public class ItemCreator
    {
        private readonly PropertySystemDbContext _context;

        public ItemCreator(PropertySystemDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateSeed();
        }

        private void CreateSeed()
        {
            List<LK_Item> InitialItem = new List<LK_Item>();
            List<String> listItem = new List<string>()
            {
                "0,01,Tanah,0,Land,1",
                "0,02,Bangunan,0,Build,2",
                "1,03,Bangunan Ext Tipe 1,0,EXT,3",
                "1,04,Bangunan Ext Tipe 2,0,EXT2,4",
                "0,05,Bangunan Renovasi,0,Renov,5",
                "0,06,Tanah Ext Tipe 1,0,Land E1,6",
                "0,07,Garden,0,Garden,7",
                "1,CH,Renovasi Chic,8,Chic,16",
                "1,DM,Renovasi Diamond,6,Diamond,15",
                "1,GO,Renovasi Gold,2,Gold,9",
                "1,PL,Renovasi Platinum,3,Platinum,10",
                "1,RF,Ready To Fit,7,RTF,13",
                "1,SI,Renovasi Silver,0,Silver,8",
                "1,ST,Renovasi Standard,1,Standard,14",
                "1,TI,Renovasi Titanium,4,Titanium,11",
                "1,UL,Renovasi Ultima,5,Ultima,12"
            };
            foreach (var item in listItem)
            {
                var ItemSeparate = item.Split(',');

                var ItemPush = new LK_Item()
                {
                    isOption = ItemSeparate[0] == "1" ? true : false,
                    itemCode = ItemSeparate[1],
                    itemName = ItemSeparate[2],
                    optionSort = Convert.ToInt32(ItemSeparate[3]),
                    shortName = ItemSeparate[4],
                    sortNo = Convert.ToInt32(ItemSeparate[5])
                };

                InitialItem.Add(ItemPush);
            }

            foreach (var Item in InitialItem)
            {
                AddIfNotExists(Item);
            }
        }

        private void AddIfNotExists(LK_Item Item)
        {
            if (_context.LK_Item.Any(l => l.itemCode == Item.itemCode))
            {
                return;
            }

            _context.LK_Item.Add(Item);

            _context.SaveChanges();
        }
    }
}
