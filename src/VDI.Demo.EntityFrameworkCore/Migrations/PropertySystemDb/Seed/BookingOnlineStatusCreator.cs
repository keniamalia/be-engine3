﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.PropertySystemDB.OnlineBooking.PropertySystem;

namespace VDI.Demo.Migrations.PropertySystemDb.Seed
{
    public class BookingOnlineStatusCreator
    {
        private readonly PropertySystemDbContext _context;

        public BookingOnlineStatusCreator(PropertySystemDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateSeed();
        }

        private void CreateSeed()
        {
            List<LK_BookingOnlineStatus> InitialBookingOnlineStatus = new List<LK_BookingOnlineStatus>();
            List<String> listBookingOnlineStatus = new List<string>()
            {
                "1,Outstanding",
                "2,Fully Paid",
                "3,Expired",
                "4,Cancelled"
            };
            foreach (var item in listBookingOnlineStatus)
            {
                var BookingOnlineStatusSeparate = item.Split(',');

                var BookingOnlineStatusPush = new LK_BookingOnlineStatus()
                {
                    statusType = BookingOnlineStatusSeparate[0],
                    statusTypeName = BookingOnlineStatusSeparate[1]
                };

                InitialBookingOnlineStatus.Add(BookingOnlineStatusPush);
            }

            foreach (var BookingOnlineStatus in InitialBookingOnlineStatus)
            {
                AddIfNotExists(BookingOnlineStatus);
            }
        }

        private void AddIfNotExists(LK_BookingOnlineStatus BookingOnlineStatus)
        {
            if (_context.LK_BookingOnlineStatus.Any(l => l.statusType == BookingOnlineStatus.statusType && l.statusTypeName == BookingOnlineStatus.statusTypeName))
            {
                return;
            }

            _context.LK_BookingOnlineStatus.Add(BookingOnlineStatus);

            _context.SaveChanges();
        }
    }
}
