﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;

namespace VDI.Demo.Migrations.PropertySystemDb.Seed
{
    public class FacingCreator
    {
        private readonly PropertySystemDbContext _context;

        public FacingCreator(PropertySystemDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateSeed();
        }

        private void CreateSeed()
        {
            List<LK_Facing> InitialFacing = new List<LK_Facing>();
            List<String> listFacing = new List<string>()
            {
                "-,Not Defined",
                "B,West",
                "BD,South West",
                "BL,North West",
                "BLD,North West South West",
                "BTL,North West North East",
                "EN,East North",
                "ENW,East North West",
                "ES,East South",
                "ESW,East South West",
                "N,Not Defined",
                "S,South",
                "T,East",
                "TBD,South East South West",
                "TG,South East",
                "TL,North East",
                "TTG,North East South East",
                "U,North"
            };
            foreach (var item in listFacing)
            {
                var FacingSeparate = item.Split(',');

                var FacingPush = new LK_Facing()
                {
                    facingCode = FacingSeparate[0],
                    facingName = FacingSeparate[1]
                };

                InitialFacing.Add(FacingPush);
            }

            foreach (var Facing in InitialFacing)
            {
                AddIfNotExists(Facing);
            }
        }

        private void AddIfNotExists(LK_Facing Facing)
        {
            if (_context.LK_Facing.Any(l => l.facingCode == Facing.facingCode && l.facingName == Facing.facingName))
            {
                return;
            }

            _context.LK_Facing.Add(Facing);

            _context.SaveChanges();
        }
    }
}
