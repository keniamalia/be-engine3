﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.PropertySystemDB.OnlineBooking.PPOnline;

namespace VDI.Demo.Migrations.PropertySystemDb.Seed
{
    public class PaymentTypeCreator
    {
        private readonly PropertySystemDbContext _context;

        public PaymentTypeCreator(PropertySystemDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateSeed();
        }

        private void CreateSeed()
        {
            List<LK_PaymentType> InitialPaymentType = new List<LK_PaymentType>();
            List<String> listPaymentType = new List<string>()
            {
                "Cash,10,1",
                "Debit Card,20,2",
                "Credit Card,30,3",
                "Bank Transfer,40,4",
                "Autodebet,50,5",
                "Autodebet Credit Card,60,6",
                "Cheque,70,7",
                "Giro,80,8",
                "Bukti Potong PPH,90,9",
                "BCA KLIKPAY,100,10",
                "CIMB CLICKS,110,11",
                "PERMATA,120,12",
                "MatahariMall.com,130,13",
                "Employee 45,140,14",
                "MANDIRI CLICKPAY,150,15",
                "MOBEY,160,16"

            };
            foreach (var item in listPaymentType)
            {
                var PaymentTypeSeparate = item.Split(',');

                var PaymentTypePush = new LK_PaymentType()
                {
                    paymentTypeName = PaymentTypeSeparate[0],
                    sortNo = Convert.ToInt16(PaymentTypeSeparate[1]),
                    paymentType = Convert.ToInt32(PaymentTypeSeparate[2])
                };

                InitialPaymentType.Add(PaymentTypePush);
            }

            foreach (var PaymentType in InitialPaymentType)
            {
                AddIfNotExists(PaymentType);
            }
        }

        private void AddIfNotExists(LK_PaymentType PaymentType)
        {
            if (_context.LK_PaymentType.Any(l => l.paymentType == PaymentType.paymentType 
            && l.paymentTypeName == PaymentType.paymentTypeName && l.sortNo == PaymentType.sortNo))
            {
                return;
            }

            _context.LK_PaymentType.Add(PaymentType);

            _context.SaveChanges();
        }
    }
}
