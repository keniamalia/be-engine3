﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.PropertySystemDB.LippoMaster;
using VDI.Demo.PropertySystemDB.OnlineBooking.PPOnline;

namespace VDI.Demo.Migrations.PropertySystemDb.Seed
{
    public class SumberDanaCreator
    {
        private readonly PropertySystemDbContext _context;

        public SumberDanaCreator(PropertySystemDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateSeed();
        }

        private void CreateSeed()
        {
            List<MS_SumberDana> InitialSumberDana = new List<MS_SumberDana>();
            List<String> listSumberDana = new List<string>()
            {
                "001,Gaji,0",
                "002,Usaha/Bisnis,0",
                "003,Dana Pensiun,0",
                "004,Donasi,0",
                "005,Orang Tua,0",
                "006,Hibah/Warisan,0"
            };
            foreach (var item in listSumberDana)
            {
                var SumberDana = item.Split(',');

                var SumberDanaPush = new MS_SumberDana()
                {
                    entityID = 1,
                    sumberDanaCode = SumberDana[0],
                    sumberDanaName = SumberDana[1],
                    sort = Convert.ToInt16(SumberDana[2])
                };

                InitialSumberDana.Add(SumberDanaPush);
            }

            foreach (var SumberDana in InitialSumberDana)
            {
                AddIfNotExists(SumberDana);
            }
        }

        private void AddIfNotExists(MS_SumberDana SumberDana)
        {
            if (_context.MS_SumberDana.Any(l => l.sumberDanaCode == SumberDana.sumberDanaCode))
            {
                return;
            }

            _context.MS_SumberDana.Add(SumberDana);

            _context.SaveChanges();
        }
    }
}
