﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.PropertySystemDB.LippoMaster;

namespace VDI.Demo.Migrations.PropertySystemDb.Seed
{
    public class TujuanTransaksiCreator
    {
        private readonly PropertySystemDbContext _context;

        public TujuanTransaksiCreator(PropertySystemDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateSeed();
        }

        private void CreateSeed()
        {
            List<MS_TujuanTransaksi> InitialTujuanTransaksi = new List<MS_TujuanTransaksi>();
            List<String> listTujuanTransaksi = new List<string>()
            {
                "0,001,Tempat Tinggal",
                "0,002,Investasi",
                "0,003,Kantor"
            };
            foreach (var item in listTujuanTransaksi)
            {
                var TujuanTransaksiSeparate = item.Split(',');

                var TujuanTransaksiPush = new MS_TujuanTransaksi()
                {
                    entityID = 1,
                    sort = Convert.ToInt16(TujuanTransaksiSeparate[0]),
                    tujuanTransaksiCode = TujuanTransaksiSeparate[1],
                    tujuanTransaksiName = TujuanTransaksiSeparate[2]
                };

                InitialTujuanTransaksi.Add(TujuanTransaksiPush);
            }

            foreach (var TujuanTransaksi in InitialTujuanTransaksi)
            {
                AddIfNotExists(TujuanTransaksi);
            }
        }

        private void AddIfNotExists(MS_TujuanTransaksi TujuanTransaksi)
        {
            if (_context.MS_TujuanTransaksi.Any(l => l.tujuanTransaksiCode == TujuanTransaksi.tujuanTransaksiCode))
            {
                return;
            }

            _context.MS_TujuanTransaksi.Add(TujuanTransaksi);

            _context.SaveChanges();
        }
    }
}
