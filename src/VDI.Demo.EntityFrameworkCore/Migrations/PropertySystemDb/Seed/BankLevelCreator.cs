﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;

namespace VDI.Demo.Migrations.PropertySystemDb.Seed
{
    public class BankLevelCreator
    {
        private readonly PropertySystemDbContext _context;

        public BankLevelCreator(PropertySystemDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateSeed();
        }

        private void CreateSeed()
        {
            List<LK_BankLevel> InitialBankLevel = new List<LK_BankLevel>();
            List<String> listBankLevel = new List<string>()
            {
                "B,Bank,false,true",
                "MF,Multi Finance,false,true",
                "KCU,Kantor Cabang Utama,true,false"
            };
            foreach (var item in listBankLevel)
            {
                var BankLevelSeparate = item.Split(',');

                var BankLevelPush = new LK_BankLevel()
                {
                    bankLevelCode = BankLevelSeparate[0],
                    bankLevelName = BankLevelSeparate[1],
                    isBankBranchType = Convert.ToBoolean(BankLevelSeparate[2]),
                    isBankType = Convert.ToBoolean(BankLevelSeparate[3])
                };

                InitialBankLevel.Add(BankLevelPush);
            }

            foreach (var BankLevel in InitialBankLevel)
            {
                AddIfNotExists(BankLevel);
            }
        }

        private void AddIfNotExists(LK_BankLevel BankLevel)
        {
            if (_context.LK_BankLevel.Any(l => l.bankLevelCode == BankLevel.bankLevelCode && l.bankLevelName == BankLevel.bankLevelName))
            {
                return;
            }

            _context.LK_BankLevel.Add(BankLevel);

            _context.SaveChanges();
        }
    }
}
