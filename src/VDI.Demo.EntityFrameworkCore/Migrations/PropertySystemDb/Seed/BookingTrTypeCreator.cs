﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.PropertySystemDB.LippoMaster;
using VDI.Demo.PropertySystemDB.OnlineBooking.PPOnline;

namespace VDI.Demo.Migrations.PropertySystemDb.Seed
{
    public class BookingTrTypeCreator
    {
        private readonly PropertySystemDbContext _context;

        public BookingTrTypeCreator(PropertySystemDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateSeed();
        }

        private void CreateSeed()
        {
            List<LK_BookingTrType> InitialBookingTrType = new List<LK_BookingTrType>();
            List<String> listBookingTrType = new List<string>
            {
                "AD,Adjustment",
                "BK,Booking"
            };

            foreach (var item in listBookingTrType)
            {
                var BookingTrType = item.Split(',');

                var BookingTrTypePush = new LK_BookingTrType()
                {
                    bookingTrType = BookingTrType[0],
                    ket = BookingTrType[1],
                };

                InitialBookingTrType.Add(BookingTrTypePush);
            }

            foreach (var BookingTrType in InitialBookingTrType)
            {
                AddIfNotExists(BookingTrType);
            }
        }

        private void AddIfNotExists(LK_BookingTrType BookingTrType)
        {
            if (_context.LK_BookingTrType.Any(l => l.bookingTrType == BookingTrType.bookingTrType
                                              && l.ket == BookingTrType.ket))
            {
                return;
            }

            _context.LK_BookingTrType.Add(BookingTrType);

            _context.SaveChanges();
        }
    }
}
