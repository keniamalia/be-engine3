﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.PropertySystemDB.LippoMaster;

namespace VDI.Demo.Migrations.PropertySystemDb.Seed
{
    public class PromotionCreator
    {
        private readonly PropertySystemDbContext _context;

        public PromotionCreator(PropertySystemDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateSeed();
        }

        private void CreateSeed()
        {
            List<LK_Promotion> InitialPromotion = new List<LK_Promotion>();
            List<String> listPromotion = new List<string>()
            {
                "001,Koran",
                "002,Majalah",
                "003,TV / Radio",
                "004,Billboard",
                "005,Pagar Grafis",
                "006,Lisan",
                "007,Undangan",
                "101,Others"
            };
            foreach (var item in listPromotion)
            {
                var PromotionSeparate = item.Split(',');

                var PromotionPush = new LK_Promotion()
                {
                    promotionCode = PromotionSeparate[0],
                    promotionDesc = PromotionSeparate[1],
                };

                InitialPromotion.Add(PromotionPush);
            }

            foreach (var Promotion in InitialPromotion)
            {
                AddIfNotExists(Promotion);
            }
        }

        private void AddIfNotExists(LK_Promotion Promotion)
        {
            if (_context.LK_Promotion.Any(l => l.promotionCode == Promotion.promotionCode && l.promotionDesc == Promotion.promotionDesc))
            {
                return;
            }

            _context.LK_Promotion.Add(Promotion);

            _context.SaveChanges();
        }
    }
}
