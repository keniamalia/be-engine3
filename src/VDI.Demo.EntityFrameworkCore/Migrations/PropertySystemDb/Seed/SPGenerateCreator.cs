﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.PropertySystemDB.LippoMaster;

namespace VDI.Demo.Migrations.PropertySystemDb.Seed
{
    public class SPGenerateCreator
    {
        private readonly PropertySystemDbContext _context;

        public SPGenerateCreator(PropertySystemDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateSeed();
        }

        private void CreateSeed()
        {
            List<LK_SPGenerate> InitialSPGenerate = new List<LK_SPGenerate>();
            List<String> listSPGenerate = new List<string>()
            {
                "Days Due",
                "SP1",
                "SP2",
                "SPP",
                "DUD"
            };
            foreach (var item in listSPGenerate)
            {
                var SPGenerateSeparate = item.Split(',');

                var SPGeneratePush = new LK_SPGenerate()
                {
                    SPGenerateDesc = SPGenerateSeparate[0]
                };

                InitialSPGenerate.Add(SPGeneratePush);
            }

            foreach (var SPGenerate in InitialSPGenerate)
            {
                AddIfNotExists(SPGenerate);
            }
        }

        private void AddIfNotExists(LK_SPGenerate SPGenerate)
        {
            if (_context.LK_SPGenerate.Any(l => l.SPGenerateDesc == SPGenerate.SPGenerateDesc))
            {
                return;
            }

            _context.LK_SPGenerate.Add(SPGenerate);

            _context.SaveChanges();
        }
    }
}
