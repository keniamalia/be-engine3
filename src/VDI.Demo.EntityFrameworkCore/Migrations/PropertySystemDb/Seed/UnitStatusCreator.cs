﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;

namespace VDI.Demo.Migrations.PropertySystemDb.Seed
{
    public class UnitStatusCreator
    {
        private readonly PropertySystemDbContext _context;

        public UnitStatusCreator(PropertySystemDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateSeed();
        }

        private void CreateSeed()
        {
            List<LK_UnitStatus> InitialUnitStatus = new List<LK_UnitStatus>();
            List<String> listUnitStatus = new List<string>()
            {
                "A,Available",
                "D,Deleted Kavlings",
                "K,Hold Single Unit",
                "L,Launching",
                "N,Not For Sale",
                "P,Pending Available",
                "R,Rental",
                "S,Sold",
                "U,Used Unit",
                "V,Reserved",
                "X,Available Hold",
                "Z,Book In Process"
            };
            foreach (var item in listUnitStatus)
            {
                var UnitStatusSeparate = item.Split(',');

                var UnitStatusPush = new LK_UnitStatus()
                {
                    unitStatusCode = UnitStatusSeparate[0],
                    unitStatusName = UnitStatusSeparate[1]
                };

                InitialUnitStatus.Add(UnitStatusPush);
            }

            foreach (var UnitStatus in InitialUnitStatus)
            {
                AddIfNotExists(UnitStatus);
            }
        }

        private void AddIfNotExists(LK_UnitStatus UnitStatus)
        {
            if (_context.LK_UnitStatus.Any(l => l.unitStatusCode == UnitStatus.unitStatusCode))
            {
                return;
            }

            _context.LK_UnitStatus.Add(UnitStatus);

            _context.SaveChanges();
        }
    }
}
