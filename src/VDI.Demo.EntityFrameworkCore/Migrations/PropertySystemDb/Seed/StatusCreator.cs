﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.PropertySystemDB.LippoMaster;

namespace VDI.Demo.Migrations.PropertySystemDb.Seed
{
    public class StatusCreator
    {
        private readonly PropertySystemDbContext _context;

        public StatusCreator(PropertySystemDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateSeed();
        }

        private void CreateSeed()
        {
            List<LK_Status> InitialStatus = new List<LK_Status>();
            List<String> listStatus = new List<string>()
            {
                "1,1,0,0,C,Clear",
                "0,0,1,1,D,Delete",
                "1,0,1,1,J,Rejected",
                "0,0,0,1,R,Received",
                "0,0,0,1,T,Returned"
            };
            foreach (var item in listStatus)
            {
                var StatusSeparate = item.Split(',');

                var StatusPush = new LK_Status()
                {
                    entityID = 1,
                    isClearingGroup = StatusSeparate[0] == "1" ? true : false,
                    isOR = StatusSeparate[1] == "1" ? true : false,
                    isOthersGroup = StatusSeparate[2] == "1" ? true : false,
                    isTTBGGroup = StatusSeparate[3] == "1" ? true : false,
                    statusCode = StatusSeparate[4],
                    statusDesc = StatusSeparate[5]
                };

                InitialStatus.Add(StatusPush);
            }

            foreach (var Status in InitialStatus)
            {
                AddIfNotExists(Status);
            }
        }

        private void AddIfNotExists(LK_Status Status)
        {
            if (_context.LK_Status.Any(l => l.statusCode == Status.statusCode))
            {
                return;
            }

            _context.LK_Status.Add(Status);

            _context.SaveChanges();
        }
    }
}
