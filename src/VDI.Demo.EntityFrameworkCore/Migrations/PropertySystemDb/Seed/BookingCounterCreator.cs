﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.PropertySystemDB.OnlineBooking.PropertySystem;

namespace VDI.Demo.Migrations.PropertySystemDb.Seed
{
    public class BookingCounterCreator
    {
        private readonly PropertySystemDbContext _context;

        public BookingCounterCreator(PropertySystemDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateSeed();
        }

        private void CreateSeed()
        {
            List<SYS_BookingCounter> InitialBookingCounter = new List<SYS_BookingCounter>();
            List<String> listBookingCounter = new List<string>()
            {
                "0,50000,N/A",
                "0,50000,MSD",
                "0,50017,MBS",
                "0,50052,MCP",
                "0,50402,SDU",
                "0,50000,LK2",
                "0,50000,N/A",
                "0,50000,MSD",
                "0,50043,MBS",
                "0,50098,MCP",
                "0,50723,SDU",
                "0,50008,LK2"

            };
            foreach (var item in listBookingCounter)
            {
                var BookingCounterSeparate = item.Split(',');

                var BookingCounterPush = new SYS_BookingCounter()
                {
                    entityID = 1,
                    BASTNo = Convert.ToInt32(BookingCounterSeparate[0]),
                    bookNo = Convert.ToInt32(BookingCounterSeparate[1]),
                    coCode = BookingCounterSeparate[2]
                };

                InitialBookingCounter.Add(BookingCounterPush);
            }

            foreach (var BookingCounter in InitialBookingCounter)
            {
                AddIfNotExists(BookingCounter);
            }
        }

        private void AddIfNotExists(SYS_BookingCounter BookingCounter)
        {
            if (_context.SYS_BookingCounter.Any(l => l.coCode == BookingCounter.coCode))
            {
                return;
            }

            _context.SYS_BookingCounter.Add(BookingCounter);

            _context.SaveChanges();
        }
    }
}
