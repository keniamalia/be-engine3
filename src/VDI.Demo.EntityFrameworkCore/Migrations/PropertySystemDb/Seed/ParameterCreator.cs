﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.PropertySystemDB.LippoMaster;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;

namespace VDI.Demo.Migrations.PropertySystemDb.Seed
{
    public class ParameterCreator
    {
        private readonly PropertySystemDbContext _context;

        public ParameterCreator(PropertySystemDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateSeed();
        }

        private void CreateSeed()
        {
            List<MS_Parameter> InitialParameter = new List<MS_Parameter>();
            List<String> listParameter = new List<string>()
            {
                "TOSCH,Toleransi perbedaan amount schedule dan selling price pada menu schedule,100,money",
                "WOSCH,Nominal Write Off untuk setiap schedule,100,money",
                "WOBOK,Nominal Write Off untuk satu booking,10000,money",
                "EXCPN,Untuk Write Off exclude Alloc PEN,PEN,Varchar",
                "EXCDP,Untuk New Schedule exclude alloc DP,DP,Varchar",
                "EXCDP,Untuk New Schedule exclude alloc PEN,PEN,Varchar",
                "DRCOW,Dropdown ChangeOwnerType,Hibah/Waris,Varchar",
                "DRCOW,Dropdown ChangeOwnerType,Alih Hak Jual Beli,Varchar",
                "BNPA,Bank Name Insert Payment untuk Upload Auto Debet,NOBU,Varchar",
                "PFP,PayFor Insert Payment untuk VA Bulk dan Upload Auto Debet,PMT,Varchar",
                "PTP,PayType Insert Payment untuk VA Bulk,VRT,Varchar",
                "PTPA,PayType Insert Payment untuk Upload Auto Debet,ADB,Varchar",
                "OTPVA,OthersType Insert Payment untuk VA Bulk dan Upload Auto Debet,PMT,Varchar",
                "100,Penalty Base Rate Description,Percent,Varchar",
                "1000,Penalty Base Rate Description,Permill,Varchar",
                "PSAS,Department PSAS untuk kuasa direksi Setting SP Period,PSAS,varchar",
                "STF,Department PSAS Position Staff untuk kuasa direksi Setting SP Period,Staff,varchar",
                "MGR,Department PSAS Position Manager untuk kuasa direksi Setting SP Period,Manager,varchar",
                "INSPN,Untuk Kebutuhan Get Penalty,INS,Varchar",
                "PTPW,Pay Type Payment For Waive Penalty,ADJ,Varchar",
                "OTPW,Others Type Payment For Waive Penalty,AD5,Varchar",
                "BSGRP,Mapping Bisnis Group,LK,Varchar",
                "BSGRP,Mapping Bisnis Group,LC,Varchar"
            };
            foreach (var item in listParameter)
            {
                var parameterSeparate = item.Split(',');

                var parameterPush = new MS_Parameter()
                {
                    code = parameterSeparate[0],
                    description = parameterSeparate[1],
                    value = parameterSeparate[2],
                    dataType = parameterSeparate[3],
                    isActive = true

                };

                InitialParameter.Add(parameterPush);
            }

            foreach (var parameter in InitialParameter)
            {
                AddIfNotExists(parameter);
            }
        }

        private void AddIfNotExists(MS_Parameter parameter)
        {
            if (_context.MS_Parameter.Any(l => l.code == parameter.code && l.value == parameter.value))
            {
                return;
            }

            _context.MS_Parameter.Add(parameter);

            _context.SaveChanges();
        }
    }
}
