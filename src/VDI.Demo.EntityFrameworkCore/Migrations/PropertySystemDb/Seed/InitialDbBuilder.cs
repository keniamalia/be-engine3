﻿using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.EntityFrameworkCore;

namespace VDI.Demo.Migrations.PropertySystemDb.Seed
{
    public class InitialDbBuilder
    {
        private readonly PropertySystemDbContext _context;

        public InitialDbBuilder(PropertySystemDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            new BookingOnlineStatusCreator(_context).Create();

            new PaymentTypeCreator(_context).Create();

            new PromotionCreator(_context).Create();

            new BookingCounterCreator(_context).Create();

            new BookingTrTypeCreator(_context).Create();

            new SalesEventCreator(_context).Create();

            new ShopBusinessCreator(_context).Create();

            new SumberDanaCreator(_context).Create();

            new TaxTypeCreator(_context).Create();

            new TransfromCreator(_context).Create();

            new TujuanTransaksiCreator(_context).Create();

            new FIleTypeCreator(_context).Create();
            
            ////////////////////////////////////////////////

            new DepartmentCreator(_context).Create();

            new ParameterCreator(_context).Create();

            new BankLevelCreator(_context).Create();

            new DPCalcCreator(_context).Create();

            new FacingCreator(_context).Create();

            new FinTypeCreator(_context).Create();

            new FormulaDPCreator(_context).Create();

            new ItemCreator(_context).Create();

            new RentalStatusCreator(_context).Create();

            new SADStatusCreator(_context).Create();

            new StatusCreator(_context).Create();

            new UnitStatusCreator(_context).Create();

            new CertCodeCreator(_context).Create();

            new EntityCreator(_context).Create();

            new FPStatusCreator(_context).Create();

            new LetterTypeCreator(_context).Create();

            new SPGenerateCreator(_context).Create();

            new LetterStatusCreator(_context).Create();

            _context.SaveChanges();
        }
    }
}
