﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PropertySystemDb
{
    public partial class remove_unused_table_and_added_pic_field_MsBank : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MP_BankBranch_JKB");

            migrationBuilder.DropTable(
                name: "MP_CompanyProject");

            migrationBuilder.DropTable(
                name: "MP_OfficerProject");

            migrationBuilder.DropTable(
                name: "MS_ProjectProduct");

            migrationBuilder.DropTable(
                name: "MS_JenisKantorBank");

            migrationBuilder.AlterColumn<string>(
                name: "phone",
                table: "MS_Bank",
                maxLength: 20,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 20);

            migrationBuilder.AddColumn<string>(
                name: "PICName",
                table: "MS_Bank",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PICPosition",
                table: "MS_Bank",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "email",
                table: "MS_Bank",
                maxLength: 50,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PICName",
                table: "MS_Bank");

            migrationBuilder.DropColumn(
                name: "PICPosition",
                table: "MS_Bank");

            migrationBuilder.DropColumn(
                name: "email",
                table: "MS_Bank");

            migrationBuilder.AlterColumn<string>(
                name: "phone",
                table: "MS_Bank",
                maxLength: 20,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 20,
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "MP_CompanyProject",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    companyID = table.Column<int>(nullable: false),
                    mainStatus = table.Column<bool>(nullable: false),
                    projectID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MP_CompanyProject", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MP_CompanyProject_MS_Company_companyID",
                        column: x => x.companyID,
                        principalTable: "MS_Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MP_CompanyProject_MS_Project_projectID",
                        column: x => x.projectID,
                        principalTable: "MS_Project",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MP_OfficerProject",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    email = table.Column<string>(nullable: true),
                    officerID = table.Column<int>(nullable: false),
                    phoneNo = table.Column<string>(nullable: true),
                    projectID = table.Column<int>(nullable: false),
                    whatsappNo = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MP_OfficerProject", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MP_OfficerProject_MS_Officer_officerID",
                        column: x => x.officerID,
                        principalTable: "MS_Officer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MP_OfficerProject_MS_Project_projectID",
                        column: x => x.projectID,
                        principalTable: "MS_Project",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MS_JenisKantorBank",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    JKBCode = table.Column<string>(maxLength: 5, nullable: false),
                    JKBName = table.Column<string>(maxLength: 100, nullable: false),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MS_JenisKantorBank", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MS_ProjectProduct",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    productID = table.Column<int>(nullable: false),
                    projectID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MS_ProjectProduct", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MS_ProjectProduct_MS_Product_productID",
                        column: x => x.productID,
                        principalTable: "MS_Product",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MS_ProjectProduct_MS_Project_projectID",
                        column: x => x.projectID,
                        principalTable: "MS_Project",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MP_BankBranch_JKB",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    JKBID = table.Column<int>(nullable: false),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    bankBranchID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MP_BankBranch_JKB", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MP_BankBranch_JKB_MS_JenisKantorBank_JKBID",
                        column: x => x.JKBID,
                        principalTable: "MS_JenisKantorBank",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MP_BankBranch_JKB_MS_BankBranch_bankBranchID",
                        column: x => x.bankBranchID,
                        principalTable: "MS_BankBranch",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MP_BankBranch_JKB_JKBID",
                table: "MP_BankBranch_JKB",
                column: "JKBID");

            migrationBuilder.CreateIndex(
                name: "IX_MP_BankBranch_JKB_bankBranchID",
                table: "MP_BankBranch_JKB",
                column: "bankBranchID");

            migrationBuilder.CreateIndex(
                name: "IX_MP_CompanyProject_companyID",
                table: "MP_CompanyProject",
                column: "companyID");

            migrationBuilder.CreateIndex(
                name: "IX_MP_CompanyProject_projectID",
                table: "MP_CompanyProject",
                column: "projectID");

            migrationBuilder.CreateIndex(
                name: "IX_MP_OfficerProject_officerID",
                table: "MP_OfficerProject",
                column: "officerID");

            migrationBuilder.CreateIndex(
                name: "IX_MP_OfficerProject_projectID",
                table: "MP_OfficerProject",
                column: "projectID");

            migrationBuilder.CreateIndex(
                name: "JKBCodeUnique",
                table: "MS_JenisKantorBank",
                column: "JKBCode",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_MS_ProjectProduct_productID",
                table: "MS_ProjectProduct",
                column: "productID");

            migrationBuilder.CreateIndex(
                name: "IX_MS_ProjectProduct_projectID",
                table: "MS_ProjectProduct",
                column: "projectID");
        }
    }
}
