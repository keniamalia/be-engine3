﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PropertySystemDb
{
    public partial class add_table_tr_payment_auto_debet : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TR_PaymentAutoDebet",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ADBKey = table.Column<string>(maxLength: 100, nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    accountNo = table.Column<string>(maxLength: 11, nullable: false),
                    amount = table.Column<decimal>(type: "money", nullable: false),
                    bookingHeaderID = table.Column<int>(nullable: true),
                    curr = table.Column<string>(maxLength: 3, nullable: false),
                    custName = table.Column<string>(maxLength: 20, nullable: false),
                    transDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TR_PaymentAutoDebet", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TR_PaymentAutoDebet_TR_BookingHeader_bookingHeaderID",
                        column: x => x.bookingHeaderID,
                        principalTable: "TR_BookingHeader",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TR_PaymentAutoDebet_bookingHeaderID",
                table: "TR_PaymentAutoDebet",
                column: "bookingHeaderID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TR_PaymentAutoDebet");
        }
    }
}
