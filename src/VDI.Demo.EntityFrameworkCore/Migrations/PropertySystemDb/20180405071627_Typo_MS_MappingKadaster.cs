﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PropertySystemDb
{
    public partial class Typo_MS_MappingKadaster : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "isGeneratedBySistem",
                table: "MS_MappingKadaster",
                newName: "isGeneratedBySystem");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "isGeneratedBySystem",
                table: "MS_MappingKadaster",
                newName: "isGeneratedBySistem");
        }
    }
}
