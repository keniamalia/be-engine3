﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PropertySystemDb
{
    public partial class update_table_MsClusterPenalty : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ratePerDay",
                table: "MS_ClusterPenalty",
                newName: "penaltyRate");

            migrationBuilder.RenameColumn(
                name: "penaltyDays",
                table: "MS_ClusterPenalty",
                newName: "penaltyBaseRate");

            migrationBuilder.AddColumn<int>(
                name: "autoWavePeriode",
                table: "MS_ClusterPenalty",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "bookingPeriodeEnd",
                table: "MS_ClusterPenalty",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "bookingPeriodeStart",
                table: "MS_ClusterPenalty",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "isAutoWave",
                table: "MS_ClusterPenalty",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "autoWavePeriode",
                table: "MS_ClusterPenalty");

            migrationBuilder.DropColumn(
                name: "bookingPeriodeEnd",
                table: "MS_ClusterPenalty");

            migrationBuilder.DropColumn(
                name: "bookingPeriodeStart",
                table: "MS_ClusterPenalty");

            migrationBuilder.DropColumn(
                name: "isAutoWave",
                table: "MS_ClusterPenalty");

            migrationBuilder.RenameColumn(
                name: "penaltyRate",
                table: "MS_ClusterPenalty",
                newName: "ratePerDay");

            migrationBuilder.RenameColumn(
                name: "penaltyBaseRate",
                table: "MS_ClusterPenalty",
                newName: "penaltyDays");
        }
    }
}
