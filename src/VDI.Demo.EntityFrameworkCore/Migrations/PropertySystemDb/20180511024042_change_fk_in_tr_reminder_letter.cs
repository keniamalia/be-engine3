﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PropertySystemDb
{
    public partial class change_fk_in_tr_reminder_letter : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TR_ReminderLetter_LK_LetterType_letterTypeID",
                table: "TR_ReminderLetter");

            migrationBuilder.RenameColumn(
                name: "letterTypeID",
                table: "TR_ReminderLetter",
                newName: "SPPeriodID");

            migrationBuilder.RenameIndex(
                name: "IX_TR_ReminderLetter_letterTypeID",
                table: "TR_ReminderLetter",
                newName: "IX_TR_ReminderLetter_SPPeriodID");

            migrationBuilder.AddForeignKey(
                name: "FK_TR_ReminderLetter_MS_SPPeriod_SPPeriodID",
                table: "TR_ReminderLetter",
                column: "SPPeriodID",
                principalTable: "MS_SPPeriod",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TR_ReminderLetter_MS_SPPeriod_SPPeriodID",
                table: "TR_ReminderLetter");

            migrationBuilder.RenameColumn(
                name: "SPPeriodID",
                table: "TR_ReminderLetter",
                newName: "letterTypeID");

            migrationBuilder.RenameIndex(
                name: "IX_TR_ReminderLetter_SPPeriodID",
                table: "TR_ReminderLetter",
                newName: "IX_TR_ReminderLetter_letterTypeID");

            migrationBuilder.AddForeignKey(
                name: "FK_TR_ReminderLetter_LK_LetterType_letterTypeID",
                table: "TR_ReminderLetter",
                column: "letterTypeID",
                principalTable: "LK_LetterType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
