﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PropertySystemDb
{
    public partial class Add_table_Inventory_Payment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "LK_Status",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    entityID = table.Column<int>(nullable: false),
                    isClearingGroup = table.Column<bool>(nullable: false),
                    isOR = table.Column<bool>(nullable: false),
                    isOthersGroup = table.Column<bool>(nullable: false),
                    isTTBGGroup = table.Column<bool>(nullable: false),
                    statusCode = table.Column<string>(maxLength: 1, nullable: false),
                    statusDesc = table.Column<string>(maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LK_Status", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TR_InventoryHeader",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    TTBGDate = table.Column<DateTime>(nullable: false),
                    accID = table.Column<int>(nullable: false),
                    bookingHeaderID = table.Column<int>(nullable: false),
                    entityID = table.Column<int>(nullable: false),
                    ket = table.Column<string>(maxLength: 200, nullable: false),
                    transNo = table.Column<string>(maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TR_InventoryHeader", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TR_InventoryHeader_MS_Account_accID",
                        column: x => x.accID,
                        principalTable: "MS_Account",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TR_InventoryHeader_TR_BookingHeader_bookingHeaderID",
                        column: x => x.bookingHeaderID,
                        principalTable: "TR_BookingHeader",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TR_InventoryDetail",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    amount = table.Column<decimal>(type: "money", nullable: false),
                    bankID = table.Column<int>(nullable: false),
                    chequeNo = table.Column<string>(maxLength: 30, nullable: false),
                    clearDate = table.Column<DateTime>(nullable: true),
                    dueDate = table.Column<DateTime>(nullable: false),
                    entityID = table.Column<int>(nullable: false),
                    inventoryHeaderID = table.Column<int>(nullable: false),
                    ket = table.Column<string>(maxLength: 200, nullable: false),
                    payForID = table.Column<int>(nullable: false),
                    payNo = table.Column<int>(nullable: false),
                    payTypeID = table.Column<int>(nullable: false),
                    statusID = table.Column<int>(nullable: false),
                    transNoPayment = table.Column<string>(maxLength: 18, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TR_InventoryDetail", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TR_InventoryDetail_MS_Bank_bankID",
                        column: x => x.bankID,
                        principalTable: "MS_Bank",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TR_InventoryDetail_TR_InventoryHeader_inventoryHeaderID",
                        column: x => x.inventoryHeaderID,
                        principalTable: "TR_InventoryHeader",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TR_InventoryDetail_LK_PayFor_payForID",
                        column: x => x.payForID,
                        principalTable: "LK_PayFor",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TR_InventoryDetail_LK_PayType_payTypeID",
                        column: x => x.payTypeID,
                        principalTable: "LK_PayType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TR_InventoryDetail_LK_Status_statusID",
                        column: x => x.statusID,
                        principalTable: "LK_Status",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TR_InventoryDetail_bankID",
                table: "TR_InventoryDetail",
                column: "bankID");

            migrationBuilder.CreateIndex(
                name: "IX_TR_InventoryDetail_inventoryHeaderID",
                table: "TR_InventoryDetail",
                column: "inventoryHeaderID");

            migrationBuilder.CreateIndex(
                name: "IX_TR_InventoryDetail_payForID",
                table: "TR_InventoryDetail",
                column: "payForID");

            migrationBuilder.CreateIndex(
                name: "IX_TR_InventoryDetail_payTypeID",
                table: "TR_InventoryDetail",
                column: "payTypeID");

            migrationBuilder.CreateIndex(
                name: "IX_TR_InventoryDetail_statusID",
                table: "TR_InventoryDetail",
                column: "statusID");

            migrationBuilder.CreateIndex(
                name: "IX_TR_InventoryHeader_accID",
                table: "TR_InventoryHeader",
                column: "accID");

            migrationBuilder.CreateIndex(
                name: "IX_TR_InventoryHeader_bookingHeaderID",
                table: "TR_InventoryHeader",
                column: "bookingHeaderID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TR_InventoryDetail");

            migrationBuilder.DropTable(
                name: "TR_InventoryHeader");

            migrationBuilder.DropTable(
                name: "LK_Status");
        }
    }
}
