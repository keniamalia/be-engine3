﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PropertySystemDb
{
    public partial class Update_TR_BookingDocument_For_UploadSignature : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "uploadCity",
                table: "TR_BookingDocument",
                maxLength: 70,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "uploadDate",
                table: "TR_BookingDocument",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "uploadFile",
                table: "TR_BookingDocument",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "uploadCity",
                table: "TR_BookingDocument");

            migrationBuilder.DropColumn(
                name: "uploadDate",
                table: "TR_BookingDocument");

            migrationBuilder.DropColumn(
                name: "uploadFile",
                table: "TR_BookingDocument");
        }
    }
}
