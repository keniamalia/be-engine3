﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PropertySystemDb
{
    public partial class Rename_Field_ProjectOLBooking : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "isRequiredRP",
                table: "MS_ProjectOLBooking",
                newName: "isRequiredPP");

            migrationBuilder.RenameColumn(
                name: "cluesterName",
                table: "MS_ProjectOLBooking",
                newName: "clusterName");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "isRequiredPP",
                table: "MS_ProjectOLBooking",
                newName: "isRequiredRP");

            migrationBuilder.RenameColumn(
                name: "clusterName",
                table: "MS_ProjectOLBooking",
                newName: "cluesterName");
        }
    }
}
