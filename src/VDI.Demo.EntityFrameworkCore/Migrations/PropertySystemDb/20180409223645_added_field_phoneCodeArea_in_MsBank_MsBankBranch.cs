﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PropertySystemDb
{
    public partial class added_field_phoneCodeArea_in_MsBank_MsBankBranch : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "phoneCodeArea",
                table: "MS_BankBranch",
                maxLength: 20,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "phoneCodeArea",
                table: "MS_Bank",
                maxLength: 20,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "phoneCodeArea",
                table: "MS_BankBranch");

            migrationBuilder.DropColumn(
                name: "phoneCodeArea",
                table: "MS_Bank");
        }
    }
}
