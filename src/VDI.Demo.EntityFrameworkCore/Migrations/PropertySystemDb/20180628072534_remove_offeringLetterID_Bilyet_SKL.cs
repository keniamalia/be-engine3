﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PropertySystemDb
{
    public partial class remove_offeringLetterID_Bilyet_SKL : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Bilyet_TR_OfferingLetter_offeringLetterID",
                table: "Bilyet");

            migrationBuilder.DropForeignKey(
                name: "FK_TR_SKL_TR_OfferingLetter_offeringLetterID",
                table: "TR_SKL");

            migrationBuilder.DropIndex(
                name: "IX_TR_SKL_offeringLetterID",
                table: "TR_SKL");

            migrationBuilder.DropIndex(
                name: "IX_Bilyet_offeringLetterID",
                table: "Bilyet");

            migrationBuilder.DropColumn(
                name: "offeringLetterID",
                table: "TR_SKL");

            migrationBuilder.DropColumn(
                name: "offeringLetterID",
                table: "Bilyet");

            migrationBuilder.AddColumn<string>(
                name: "key",
                table: "TR_SKL",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "key",
                table: "Bilyet",
                maxLength: 50,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "key",
                table: "TR_SKL");

            migrationBuilder.DropColumn(
                name: "key",
                table: "Bilyet");

            migrationBuilder.AddColumn<int>(
                name: "offeringLetterID",
                table: "TR_SKL",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "offeringLetterID",
                table: "Bilyet",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_TR_SKL_offeringLetterID",
                table: "TR_SKL",
                column: "offeringLetterID");

            migrationBuilder.CreateIndex(
                name: "IX_Bilyet_offeringLetterID",
                table: "Bilyet",
                column: "offeringLetterID");

            migrationBuilder.AddForeignKey(
                name: "FK_Bilyet_TR_OfferingLetter_offeringLetterID",
                table: "Bilyet",
                column: "offeringLetterID",
                principalTable: "TR_OfferingLetter",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TR_SKL_TR_OfferingLetter_offeringLetterID",
                table: "TR_SKL",
                column: "offeringLetterID",
                principalTable: "TR_OfferingLetter",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
