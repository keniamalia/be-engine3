﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PropertySystemDb
{
    public partial class change_typo_coloumn_name_in_MS_MappingKadaster : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "isGEneratedBySistem",
                table: "MS_MappingKadaster",
                newName: "isGeneratedBySistem");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "isGeneratedBySistem",
                table: "MS_MappingKadaster",
                newName: "isGEneratedBySistem");
        }
    }
}
