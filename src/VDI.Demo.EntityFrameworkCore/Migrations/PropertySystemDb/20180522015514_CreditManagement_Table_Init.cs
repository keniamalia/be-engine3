﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PropertySystemDb
{
    public partial class CreditManagement_Table_Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MS_AgreementType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    agreementTypeName = table.Column<string>(maxLength: 200, nullable: false),
                    isActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MS_AgreementType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MS_FeeType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    feeTypeName = table.Column<string>(maxLength: 100, nullable: false),
                    isActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MS_FeeType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MS_Retention",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    isActive = table.Column<bool>(nullable: false),
                    retentionCode = table.Column<string>(maxLength: 8, nullable: false),
                    retentionName = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MS_Retention", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TR_CreditAgreement",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    agreementDate = table.Column<DateTime>(nullable: false),
                    agreementDocsName = table.Column<string>(maxLength: 200, nullable: false),
                    agreementDocsNo = table.Column<string>(maxLength: 50, nullable: false),
                    agreementDocsUrl = table.Column<string>(nullable: false),
                    agreementEnd = table.Column<DateTime>(nullable: false),
                    agreementRate = table.Column<decimal>(nullable: false),
                    agreementStart = table.Column<DateTime>(nullable: false),
                    agreementTypeID = table.Column<int>(nullable: false),
                    bankID = table.Column<int>(nullable: false),
                    isActive = table.Column<bool>(nullable: false),
                    projectID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TR_CreditAgreement", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TR_CreditAgreement_MS_AgreementType_agreementTypeID",
                        column: x => x.agreementTypeID,
                        principalTable: "MS_AgreementType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TR_CreditAgreement_MS_Bank_bankID",
                        column: x => x.bankID,
                        principalTable: "MS_Bank",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TR_CreditAgreement_MS_Project_projectID",
                        column: x => x.projectID,
                        principalTable: "MS_Project",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TR_AgreementDetail",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    addendumDocsName = table.Column<string>(maxLength: 200, nullable: true),
                    addendumDocsUrl = table.Column<string>(nullable: true),
                    addendumNo = table.Column<string>(maxLength: 35, nullable: false),
                    adminFeeType = table.Column<int>(nullable: false),
                    adminFeeValue = table.Column<decimal>(nullable: false),
                    bankRate = table.Column<decimal>(nullable: false),
                    clusterID = table.Column<int>(nullable: false),
                    creditAgreementID = table.Column<int>(nullable: false),
                    isActive = table.Column<bool>(nullable: false),
                    programEnd = table.Column<DateTime>(nullable: false),
                    programName = table.Column<string>(maxLength: 100, nullable: false),
                    programNo = table.Column<int>(nullable: false),
                    programStart = table.Column<DateTime>(nullable: false),
                    provisiFeeType = table.Column<int>(nullable: false),
                    provisiValue = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TR_AgreementDetail", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TR_AgreementDetail_MS_Cluster_clusterID",
                        column: x => x.clusterID,
                        principalTable: "MS_Cluster",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TR_AgreementDetail_TR_CreditAgreement_creditAgreementID",
                        column: x => x.creditAgreementID,
                        principalTable: "TR_CreditAgreement",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TR_BankDetail",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    address = table.Column<string>(maxLength: 200, nullable: false),
                    creditAgreementID = table.Column<int>(nullable: false),
                    email = table.Column<string>(maxLength: 50, nullable: false),
                    isPriority = table.Column<bool>(nullable: false),
                    isShowOnOB = table.Column<bool>(nullable: false),
                    isShowOnPPOnline = table.Column<bool>(nullable: false),
                    officePhone = table.Column<string>(maxLength: 20, nullable: false),
                    phone = table.Column<string>(maxLength: 20, nullable: false),
                    picName = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TR_BankDetail", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TR_BankDetail_TR_CreditAgreement_creditAgreementID",
                        column: x => x.creditAgreementID,
                        principalTable: "TR_CreditAgreement",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TR_ProgramDetail",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    agreementDetailID = table.Column<int>(nullable: false),
                    bankRate = table.Column<decimal>(nullable: false),
                    fix = table.Column<int>(nullable: false),
                    tenor = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TR_ProgramDetail", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TR_ProgramDetail_TR_AgreementDetail_agreementDetailID",
                        column: x => x.agreementDetailID,
                        principalTable: "TR_AgreementDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TR_RetentionDetail",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    agreementDetailID = table.Column<int>(nullable: false),
                    percentage = table.Column<decimal>(nullable: false),
                    retentionID = table.Column<int>(nullable: false),
                    sortNo = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TR_RetentionDetail", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TR_RetentionDetail_TR_AgreementDetail_agreementDetailID",
                        column: x => x.agreementDetailID,
                        principalTable: "TR_AgreementDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TR_RetentionDetail_MS_Retention_retentionID",
                        column: x => x.retentionID,
                        principalTable: "MS_Retention",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TR_SubsidyDetail",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    agreementDetailID = table.Column<int>(nullable: false),
                    isCalculateOnSI = table.Column<bool>(nullable: false),
                    remarks = table.Column<string>(maxLength: 100, nullable: true),
                    subsidyType = table.Column<int>(nullable: false),
                    subsidyValue = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TR_SubsidyDetail", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TR_SubsidyDetail_TR_AgreementDetail_agreementDetailID",
                        column: x => x.agreementDetailID,
                        principalTable: "TR_AgreementDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TR_AgreementDetail_clusterID",
                table: "TR_AgreementDetail",
                column: "clusterID");

            migrationBuilder.CreateIndex(
                name: "IX_TR_AgreementDetail_creditAgreementID",
                table: "TR_AgreementDetail",
                column: "creditAgreementID");

            migrationBuilder.CreateIndex(
                name: "IX_TR_BankDetail_creditAgreementID",
                table: "TR_BankDetail",
                column: "creditAgreementID");

            migrationBuilder.CreateIndex(
                name: "IX_TR_CreditAgreement_agreementTypeID",
                table: "TR_CreditAgreement",
                column: "agreementTypeID");

            migrationBuilder.CreateIndex(
                name: "IX_TR_CreditAgreement_bankID",
                table: "TR_CreditAgreement",
                column: "bankID");

            migrationBuilder.CreateIndex(
                name: "IX_TR_CreditAgreement_projectID",
                table: "TR_CreditAgreement",
                column: "projectID");

            migrationBuilder.CreateIndex(
                name: "IX_TR_ProgramDetail_agreementDetailID",
                table: "TR_ProgramDetail",
                column: "agreementDetailID");

            migrationBuilder.CreateIndex(
                name: "IX_TR_RetentionDetail_agreementDetailID",
                table: "TR_RetentionDetail",
                column: "agreementDetailID");

            migrationBuilder.CreateIndex(
                name: "IX_TR_RetentionDetail_retentionID",
                table: "TR_RetentionDetail",
                column: "retentionID");

            migrationBuilder.CreateIndex(
                name: "IX_TR_SubsidyDetail_agreementDetailID",
                table: "TR_SubsidyDetail",
                column: "agreementDetailID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MS_FeeType");

            migrationBuilder.DropTable(
                name: "TR_BankDetail");

            migrationBuilder.DropTable(
                name: "TR_ProgramDetail");

            migrationBuilder.DropTable(
                name: "TR_RetentionDetail");

            migrationBuilder.DropTable(
                name: "TR_SubsidyDetail");

            migrationBuilder.DropTable(
                name: "MS_Retention");

            migrationBuilder.DropTable(
                name: "TR_AgreementDetail");

            migrationBuilder.DropTable(
                name: "TR_CreditAgreement");

            migrationBuilder.DropTable(
                name: "MS_AgreementType");
        }
    }
}
