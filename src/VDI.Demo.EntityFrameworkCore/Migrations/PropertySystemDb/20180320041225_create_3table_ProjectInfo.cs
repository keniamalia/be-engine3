﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PropertySystemDb
{
    public partial class create_3table_ProjectInfo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MS_ProjectOLBooking",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    activeFrom = table.Column<DateTime>(nullable: true),
                    activeTo = table.Column<DateTime>(nullable: true),
                    cluesterName = table.Column<string>(maxLength: 100, nullable: true),
                    clusterID = table.Column<int>(nullable: false),
                    graceOverPeriod = table.Column<string>(maxLength: 200, nullable: true),
                    handOverPeriod = table.Column<string>(maxLength: 200, nullable: true),
                    imgLogo = table.Column<string>(maxLength: 1000, nullable: true),
                    isActive = table.Column<bool>(nullable: true),
                    isRequiredRP = table.Column<bool>(nullable: true),
                    projectDesc = table.Column<string>(maxLength: 1000, nullable: true),
                    projectID = table.Column<int>(nullable: false),
                    projectInfoID = table.Column<int>(nullable: true),
                    projectName = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MS_ProjectOLBooking", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MS_ProjectOLBooking_MS_Cluster_clusterID",
                        column: x => x.clusterID,
                        principalTable: "MS_Cluster",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MS_ProjectOLBooking_MS_Project_projectID",
                        column: x => x.projectID,
                        principalTable: "MS_Project",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MS_ProjectOLBooking_MS_ProjectInfo_projectInfoID",
                        column: x => x.projectInfoID,
                        principalTable: "MS_ProjectInfo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MS_ProjectSocialMedia",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    isActive = table.Column<bool>(nullable: true),
                    socialMediaIcon = table.Column<string>(maxLength: 100, nullable: false),
                    socialMediaName = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MS_ProjectSocialMedia", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TR_ProjectSocialMedia",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    isActive = table.Column<bool>(nullable: true),
                    projectInfoID = table.Column<int>(nullable: false),
                    socialMediaLink = table.Column<string>(maxLength: 100, nullable: false),
                    sosialMediaID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TR_ProjectSocialMedia", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TR_ProjectSocialMedia_MS_ProjectInfo_projectInfoID",
                        column: x => x.projectInfoID,
                        principalTable: "MS_ProjectInfo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TR_ProjectSocialMedia_MS_ProjectSocialMedia_sosialMediaID",
                        column: x => x.sosialMediaID,
                        principalTable: "MS_ProjectSocialMedia",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MS_ProjectOLBooking_clusterID",
                table: "MS_ProjectOLBooking",
                column: "clusterID");

            migrationBuilder.CreateIndex(
                name: "IX_MS_ProjectOLBooking_projectID",
                table: "MS_ProjectOLBooking",
                column: "projectID");

            migrationBuilder.CreateIndex(
                name: "IX_MS_ProjectOLBooking_projectInfoID",
                table: "MS_ProjectOLBooking",
                column: "projectInfoID");

            migrationBuilder.CreateIndex(
                name: "IX_TR_ProjectSocialMedia_projectInfoID",
                table: "TR_ProjectSocialMedia",
                column: "projectInfoID");

            migrationBuilder.CreateIndex(
                name: "IX_TR_ProjectSocialMedia_sosialMediaID",
                table: "TR_ProjectSocialMedia",
                column: "sosialMediaID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MS_ProjectOLBooking");

            migrationBuilder.DropTable(
                name: "TR_ProjectSocialMedia");

            migrationBuilder.DropTable(
                name: "MS_ProjectSocialMedia");
        }
    }
}
