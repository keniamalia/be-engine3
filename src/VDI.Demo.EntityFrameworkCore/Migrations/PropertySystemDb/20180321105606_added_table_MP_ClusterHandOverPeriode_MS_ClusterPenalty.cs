﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PropertySystemDb
{
    public partial class added_table_MP_ClusterHandOverPeriode_MS_ClusterPenalty : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "dueDateRemarks",
                table: "MS_Cluster",
                maxLength: 500,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 500);

            migrationBuilder.CreateTable(
                name: "MP_ClusterHandOverPeriode",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    clusterID = table.Column<int>(nullable: false),
                    entityID = table.Column<int>(nullable: false),
                    graceDue = table.Column<string>(nullable: true),
                    handOverDue = table.Column<string>(nullable: true),
                    handOverEnd = table.Column<DateTime>(nullable: false),
                    handOverStart = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MP_ClusterHandOverPeriode", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MP_ClusterHandOverPeriode_MS_Cluster_clusterID",
                        column: x => x.clusterID,
                        principalTable: "MS_Cluster",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MS_ClusterPenalty",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    clusterID = table.Column<int>(nullable: false),
                    entityID = table.Column<int>(nullable: false),
                    penaltyEnd = table.Column<DateTime>(nullable: false),
                    penaltyStart = table.Column<DateTime>(nullable: false),
                    ratePerDay = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MS_ClusterPenalty", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MS_ClusterPenalty_MS_Cluster_clusterID",
                        column: x => x.clusterID,
                        principalTable: "MS_Cluster",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MP_ClusterHandOverPeriode_clusterID",
                table: "MP_ClusterHandOverPeriode",
                column: "clusterID");

            migrationBuilder.CreateIndex(
                name: "IX_MS_ClusterPenalty_clusterID",
                table: "MS_ClusterPenalty",
                column: "clusterID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MP_ClusterHandOverPeriode");

            migrationBuilder.DropTable(
                name: "MS_ClusterPenalty");

            migrationBuilder.AlterColumn<string>(
                name: "dueDateRemarks",
                table: "MS_Cluster",
                maxLength: 500,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 500,
                oldNullable: true);
        }
    }
}
