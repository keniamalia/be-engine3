﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PropertySystemDb
{
    public partial class add_field_in_ms_clusterPenalty_and_ms_spPeriod : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "accountID",
                table: "MS_SPPeriod",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "penaltyDocName",
                table: "MS_ClusterPenalty",
                maxLength: 100,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "penaltyDocUrl",
                table: "MS_ClusterPenalty",
                maxLength: 200,
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_MS_SPPeriod_accountID",
                table: "MS_SPPeriod",
                column: "accountID");

            migrationBuilder.AddForeignKey(
                name: "FK_MS_SPPeriod_MS_Account_accountID",
                table: "MS_SPPeriod",
                column: "accountID",
                principalTable: "MS_Account",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MS_SPPeriod_MS_Account_accountID",
                table: "MS_SPPeriod");

            migrationBuilder.DropIndex(
                name: "IX_MS_SPPeriod_accountID",
                table: "MS_SPPeriod");

            migrationBuilder.DropColumn(
                name: "accountID",
                table: "MS_SPPeriod");

            migrationBuilder.DropColumn(
                name: "penaltyDocName",
                table: "MS_ClusterPenalty");

            migrationBuilder.DropColumn(
                name: "penaltyDocUrl",
                table: "MS_ClusterPenalty");
        }
    }
}
