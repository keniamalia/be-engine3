﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PropertySystemDb
{
    public partial class delete_icollection_trPaymentDetail_in_msAccount : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TR_PaymentDetail_MS_Account_MS_AccountId",
                table: "TR_PaymentDetail");

            migrationBuilder.DropForeignKey(
                name: "FK_TR_PaymentDetailAlloc_MS_Account_MS_AccountId",
                table: "TR_PaymentDetailAlloc");

            migrationBuilder.DropIndex(
                name: "IX_TR_PaymentDetailAlloc_MS_AccountId",
                table: "TR_PaymentDetailAlloc");

            migrationBuilder.DropIndex(
                name: "IX_TR_PaymentDetail_MS_AccountId",
                table: "TR_PaymentDetail");

            migrationBuilder.DropColumn(
                name: "MS_AccountId",
                table: "TR_PaymentDetailAlloc");

            migrationBuilder.DropColumn(
                name: "MS_AccountId",
                table: "TR_PaymentDetail");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "MS_AccountId",
                table: "TR_PaymentDetailAlloc",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MS_AccountId",
                table: "TR_PaymentDetail",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TR_PaymentDetailAlloc_MS_AccountId",
                table: "TR_PaymentDetailAlloc",
                column: "MS_AccountId");

            migrationBuilder.CreateIndex(
                name: "IX_TR_PaymentDetail_MS_AccountId",
                table: "TR_PaymentDetail",
                column: "MS_AccountId");

            migrationBuilder.AddForeignKey(
                name: "FK_TR_PaymentDetail_MS_Account_MS_AccountId",
                table: "TR_PaymentDetail",
                column: "MS_AccountId",
                principalTable: "MS_Account",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TR_PaymentDetailAlloc_MS_Account_MS_AccountId",
                table: "TR_PaymentDetailAlloc",
                column: "MS_AccountId",
                principalTable: "MS_Account",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
