﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PropertySystemDb
{
    public partial class Add_Table_MS_MappingKadaster_and_Change_Table_DocNoCounter_to_SYS_DocNoCounter : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DocNo_Counter_MS_Company_coID",
                table: "DocNo_Counter");

            migrationBuilder.DropForeignKey(
                name: "FK_DocNo_Counter_MS_Document_docID",
                table: "DocNo_Counter");

            migrationBuilder.DropForeignKey(
                name: "FK_DocNo_Counter_MS_Project_projectID",
                table: "DocNo_Counter");

            migrationBuilder.DropPrimaryKey(
                name: "PK_DocNo_Counter",
                table: "DocNo_Counter");

            migrationBuilder.RenameTable(
                name: "DocNo_Counter",
                newName: "SYS_DocNoCounter");

            migrationBuilder.RenameIndex(
                name: "IX_DocNo_Counter_projectID",
                table: "SYS_DocNoCounter",
                newName: "IX_SYS_DocNoCounter_projectID");

            migrationBuilder.RenameIndex(
                name: "IX_DocNo_Counter_docID",
                table: "SYS_DocNoCounter",
                newName: "IX_SYS_DocNoCounter_docID");

            migrationBuilder.RenameIndex(
                name: "IX_DocNo_Counter_coID",
                table: "SYS_DocNoCounter",
                newName: "IX_SYS_DocNoCounter_coID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_SYS_DocNoCounter",
                table: "SYS_DocNoCounter",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "MS_MappingKadaster",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    categoryID = table.Column<int>(nullable: false),
                    isGEneratedBySistem = table.Column<bool>(nullable: false),
                    kadasterCode = table.Column<string>(nullable: true),
                    kadasterLocation = table.Column<string>(nullable: false),
                    projectID = table.Column<int>(nullable: false),
                    unitID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MS_MappingKadaster", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MS_MappingKadaster_MS_Project_projectID",
                        column: x => x.projectID,
                        principalTable: "MS_Project",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MS_MappingKadaster_projectID",
                table: "MS_MappingKadaster",
                column: "projectID");

            migrationBuilder.AddForeignKey(
                name: "FK_SYS_DocNoCounter_MS_Company_coID",
                table: "SYS_DocNoCounter",
                column: "coID",
                principalTable: "MS_Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SYS_DocNoCounter_MS_Document_docID",
                table: "SYS_DocNoCounter",
                column: "docID",
                principalTable: "MS_Document",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SYS_DocNoCounter_MS_Project_projectID",
                table: "SYS_DocNoCounter",
                column: "projectID",
                principalTable: "MS_Project",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SYS_DocNoCounter_MS_Company_coID",
                table: "SYS_DocNoCounter");

            migrationBuilder.DropForeignKey(
                name: "FK_SYS_DocNoCounter_MS_Document_docID",
                table: "SYS_DocNoCounter");

            migrationBuilder.DropForeignKey(
                name: "FK_SYS_DocNoCounter_MS_Project_projectID",
                table: "SYS_DocNoCounter");

            migrationBuilder.DropTable(
                name: "MS_MappingKadaster");

            migrationBuilder.DropPrimaryKey(
                name: "PK_SYS_DocNoCounter",
                table: "SYS_DocNoCounter");

            migrationBuilder.RenameTable(
                name: "SYS_DocNoCounter",
                newName: "DocNo_Counter");

            migrationBuilder.RenameIndex(
                name: "IX_SYS_DocNoCounter_projectID",
                table: "DocNo_Counter",
                newName: "IX_DocNo_Counter_projectID");

            migrationBuilder.RenameIndex(
                name: "IX_SYS_DocNoCounter_docID",
                table: "DocNo_Counter",
                newName: "IX_DocNo_Counter_docID");

            migrationBuilder.RenameIndex(
                name: "IX_SYS_DocNoCounter_coID",
                table: "DocNo_Counter",
                newName: "IX_DocNo_Counter_coID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_DocNo_Counter",
                table: "DocNo_Counter",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_DocNo_Counter_MS_Company_coID",
                table: "DocNo_Counter",
                column: "coID",
                principalTable: "MS_Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_DocNo_Counter_MS_Document_docID",
                table: "DocNo_Counter",
                column: "docID",
                principalTable: "MS_Document",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_DocNo_Counter_MS_Project_projectID",
                table: "DocNo_Counter",
                column: "projectID",
                principalTable: "MS_Project",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
