﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PropertySystemDb
{
    public partial class add_2table_unitvaAndpaymentbulkVA : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MS_UnitVirtualAccount",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    VA_BankAccNo = table.Column<string>(maxLength: 100, nullable: false),
                    VA_BankName = table.Column<string>(maxLength: 100, nullable: false),
                    unitID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MS_UnitVirtualAccount", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MS_UnitVirtualAccount_MS_Unit_unitID",
                        column: x => x.unitID,
                        principalTable: "MS_Unit",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TR_PaymentBulkVA",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BulkVAKey = table.Column<string>(maxLength: 150, nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    JamTransaksi = table.Column<string>(maxLength: 50, nullable: true),
                    JenisTransaksi = table.Column<string>(maxLength: 200, nullable: true),
                    Keterangan = table.Column<string>(maxLength: 500, nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    NamaVA = table.Column<string>(maxLength: 150, nullable: true),
                    NilaiTransaksi = table.Column<decimal>(type: "money", nullable: true),
                    NomorReferensi = table.Column<string>(maxLength: 50, nullable: true),
                    TanggalMutasi = table.Column<DateTime>(nullable: true),
                    TanggalTransaksi = table.Column<DateTime>(nullable: true),
                    VirtualAccount = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TR_PaymentBulkVA", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MS_UnitVirtualAccount_unitID",
                table: "MS_UnitVirtualAccount",
                column: "unitID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MS_UnitVirtualAccount");

            migrationBuilder.DropTable(
                name: "TR_PaymentBulkVA");
        }
    }
}
