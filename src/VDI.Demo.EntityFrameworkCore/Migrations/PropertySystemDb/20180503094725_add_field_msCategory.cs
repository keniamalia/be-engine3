﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PropertySystemDb
{
    public partial class add_field_msCategory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "diagramaticType",
                table: "MS_Category",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "kadasterType",
                table: "MS_Category",
                maxLength: 50,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "diagramaticType",
                table: "MS_Category");

            migrationBuilder.DropColumn(
                name: "kadasterType",
                table: "MS_Category");
        }
    }
}
