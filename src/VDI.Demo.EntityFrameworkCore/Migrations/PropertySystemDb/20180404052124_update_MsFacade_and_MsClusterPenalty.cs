﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PropertySystemDb
{
    public partial class update_MsFacade_and_MsClusterPenalty : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "penaltyEnd",
                table: "MS_ClusterPenalty");

            migrationBuilder.DropColumn(
                name: "penaltyStart",
                table: "MS_ClusterPenalty");

            migrationBuilder.AddColumn<int>(
                name: "clusterID",
                table: "MS_Facade",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "detailID",
                table: "MS_Facade",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "projectID",
                table: "MS_Facade",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "penaltyDays",
                table: "MS_ClusterPenalty",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_MS_Facade_projectID",
                table: "MS_Facade",
                column: "projectID");

            migrationBuilder.AddForeignKey(
                name: "FK_MS_Facade_MS_Project_projectID",
                table: "MS_Facade",
                column: "projectID",
                principalTable: "MS_Project",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MS_Facade_MS_Project_projectID",
                table: "MS_Facade");

            migrationBuilder.DropIndex(
                name: "IX_MS_Facade_projectID",
                table: "MS_Facade");

            migrationBuilder.DropColumn(
                name: "clusterID",
                table: "MS_Facade");

            migrationBuilder.DropColumn(
                name: "detailID",
                table: "MS_Facade");

            migrationBuilder.DropColumn(
                name: "projectID",
                table: "MS_Facade");

            migrationBuilder.DropColumn(
                name: "penaltyDays",
                table: "MS_ClusterPenalty");

            migrationBuilder.AddColumn<DateTime>(
                name: "penaltyEnd",
                table: "MS_ClusterPenalty",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "penaltyStart",
                table: "MS_ClusterPenalty",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
