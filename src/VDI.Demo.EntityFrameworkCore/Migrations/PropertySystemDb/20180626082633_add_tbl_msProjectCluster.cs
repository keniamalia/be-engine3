﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PropertySystemDb
{
    public partial class add_tbl_msProjectCluster : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MS_ProjectCluster",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    clusterID = table.Column<int>(nullable: false),
                    projectInfoID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MS_ProjectCluster", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MS_ProjectCluster_MS_Cluster_clusterID",
                        column: x => x.clusterID,
                        principalTable: "MS_Cluster",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MS_ProjectCluster_MS_ProjectInfo_projectInfoID",
                        column: x => x.projectInfoID,
                        principalTable: "MS_ProjectInfo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MS_ProjectCluster_clusterID",
                table: "MS_ProjectCluster",
                column: "clusterID");

            migrationBuilder.CreateIndex(
                name: "IX_MS_ProjectCluster_projectInfoID",
                table: "MS_ProjectCluster",
                column: "projectInfoID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MS_ProjectCluster");
        }
    }
}
