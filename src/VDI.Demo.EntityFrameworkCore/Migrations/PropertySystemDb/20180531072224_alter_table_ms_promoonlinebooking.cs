﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PropertySystemDb
{
    public partial class alter_table_ms_promoonlinebooking : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "promoFile",
                table: "MS_PromoOnlineBooking",
                newName: "promoFileWeb");

            migrationBuilder.AddColumn<DateTime>(
                name: "periodEnd",
                table: "MS_PromoOnlineBooking",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "periodStart",
                table: "MS_PromoOnlineBooking",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "promoFileApp",
                table: "MS_PromoOnlineBooking",
                maxLength: 200,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "sortNo",
                table: "MS_PromoOnlineBooking",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "periodEnd",
                table: "MS_PromoOnlineBooking");

            migrationBuilder.DropColumn(
                name: "periodStart",
                table: "MS_PromoOnlineBooking");

            migrationBuilder.DropColumn(
                name: "promoFileApp",
                table: "MS_PromoOnlineBooking");

            migrationBuilder.DropColumn(
                name: "sortNo",
                table: "MS_PromoOnlineBooking");

            migrationBuilder.RenameColumn(
                name: "promoFileWeb",
                table: "MS_PromoOnlineBooking",
                newName: "promoFile");
        }
    }
}
