﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PropertySystemDb
{
    public partial class add_2_tbhistory_alter_1_tbhistory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte>(
                name: "historyNo",
                table: "TR_DPHistory",
                nullable: false,
                defaultValue: (byte)0);

            migrationBuilder.CreateTable(
                name: "TR_BookingDetailHistory",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BFAmount = table.Column<decimal>(type: "money", nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    adjArea = table.Column<double>(nullable: false),
                    adjPrice = table.Column<decimal>(type: "money", nullable: false),
                    amount = table.Column<decimal>(type: "money", nullable: false),
                    amountComm = table.Column<decimal>(type: "money", nullable: false),
                    amountMKT = table.Column<decimal>(type: "money", nullable: false),
                    area = table.Column<double>(nullable: false),
                    bookNo = table.Column<int>(nullable: false),
                    bookingHeaderID = table.Column<int>(nullable: false),
                    bookingTrTypeID = table.Column<int>(nullable: false),
                    coCode = table.Column<string>(maxLength: 5, nullable: false),
                    combineCode = table.Column<string>(maxLength: 1, nullable: false),
                    entityID = table.Column<int>(nullable: false),
                    finTypeID = table.Column<int>(nullable: false),
                    historyNo = table.Column<byte>(nullable: false),
                    itemID = table.Column<int>(nullable: false),
                    netNetPrice = table.Column<decimal>(type: "money", nullable: false),
                    netPrice = table.Column<decimal>(type: "money", nullable: false),
                    netPriceCash = table.Column<decimal>(type: "money", nullable: false),
                    netPriceComm = table.Column<decimal>(type: "money", nullable: false),
                    netPriceMKT = table.Column<decimal>(type: "money", nullable: false),
                    pctDisc = table.Column<double>(nullable: false),
                    pctTax = table.Column<double>(nullable: false),
                    refNo = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TR_BookingDetailHistory", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TR_PaymentDetailAllocHistory",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    entityID = table.Column<int>(nullable: false),
                    historyNo = table.Column<byte>(nullable: false),
                    netAmt = table.Column<decimal>(type: "money", nullable: false),
                    paymentDetailID = table.Column<int>(nullable: false),
                    schedNo = table.Column<short>(nullable: false),
                    vatAmt = table.Column<decimal>(type: "money", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TR_PaymentDetailAllocHistory", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TR_BookingDetailHistory");

            migrationBuilder.DropTable(
                name: "TR_PaymentDetailAllocHistory");

            migrationBuilder.DropColumn(
                name: "historyNo",
                table: "TR_DPHistory");
        }
    }
}
