﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PropertySystemDb
{
    public partial class change_field_table_tr_reminder_letter : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "letterType",
                table: "TR_ReminderLetter");

            migrationBuilder.AddColumn<int>(
                name: "letterTypeID",
                table: "TR_ReminderLetter",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_TR_ReminderLetter_letterTypeID",
                table: "TR_ReminderLetter",
                column: "letterTypeID");

            migrationBuilder.AddForeignKey(
                name: "FK_TR_ReminderLetter_LK_LetterType_letterTypeID",
                table: "TR_ReminderLetter",
                column: "letterTypeID",
                principalTable: "LK_LetterType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TR_ReminderLetter_LK_LetterType_letterTypeID",
                table: "TR_ReminderLetter");

            migrationBuilder.DropIndex(
                name: "IX_TR_ReminderLetter_letterTypeID",
                table: "TR_ReminderLetter");

            migrationBuilder.DropColumn(
                name: "letterTypeID",
                table: "TR_ReminderLetter");

            migrationBuilder.AddColumn<string>(
                name: "letterType",
                table: "TR_ReminderLetter",
                maxLength: 3,
                nullable: false,
                defaultValue: "");
        }
    }
}
