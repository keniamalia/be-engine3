﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PropertySystemDb
{
    public partial class add_table_ms_user_account_bank_in_credit_management : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MS_UserAccountBank",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    bankBranchID = table.Column<int>(nullable: false),
                    bankDetailID = table.Column<int>(nullable: false),
                    clusterID = table.Column<int>(nullable: false),
                    isActive = table.Column<bool>(nullable: false),
                    link = table.Column<string>(maxLength: 200, nullable: false),
                    officePhone = table.Column<string>(maxLength: 20, nullable: false),
                    phoneNo = table.Column<string>(maxLength: 20, nullable: false),
                    picName = table.Column<string>(maxLength: 30, nullable: false),
                    position = table.Column<string>(maxLength: 20, nullable: true),
                    status = table.Column<bool>(nullable: false),
                    userID = table.Column<int>(nullable: false),
                    verified = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MS_UserAccountBank", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MS_UserAccountBank_MS_BankBranch_bankBranchID",
                        column: x => x.bankBranchID,
                        principalTable: "MS_BankBranch",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MS_UserAccountBank_TR_BankDetail_bankDetailID",
                        column: x => x.bankDetailID,
                        principalTable: "TR_BankDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MS_UserAccountBank_MS_Cluster_clusterID",
                        column: x => x.clusterID,
                        principalTable: "MS_Cluster",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MS_UserAccountBank_bankBranchID",
                table: "MS_UserAccountBank",
                column: "bankBranchID");

            migrationBuilder.CreateIndex(
                name: "IX_MS_UserAccountBank_bankDetailID",
                table: "MS_UserAccountBank",
                column: "bankDetailID");

            migrationBuilder.CreateIndex(
                name: "IX_MS_UserAccountBank_clusterID",
                table: "MS_UserAccountBank",
                column: "clusterID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MS_UserAccountBank");
        }
    }
}
