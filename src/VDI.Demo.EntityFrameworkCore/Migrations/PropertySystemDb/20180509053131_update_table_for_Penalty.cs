﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PropertySystemDb
{
    public partial class update_table_for_Penalty : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "createdBy",
                table: "TR_PenaltySchedule");

            migrationBuilder.DropColumn(
                name: "createdOn",
                table: "TR_PenaltySchedule");

            migrationBuilder.RenameColumn(
                name: "bookingPeriodeStart",
                table: "MS_ClusterPenalty",
                newName: "bookingPeriodStart");

            migrationBuilder.RenameColumn(
                name: "bookingPeriodeEnd",
                table: "MS_ClusterPenalty",
                newName: "bookingPeriodEnd");

            migrationBuilder.RenameColumn(
                name: "autoWavePeriode",
                table: "MS_ClusterPenalty",
                newName: "autoWavePeriod");

            migrationBuilder.AlterColumn<DateTime>(
                name: "receiveDate",
                table: "TR_ReminderLetter",
                nullable: true,
                oldClrType: typeof(DateTime));

            migrationBuilder.AlterColumn<DateTime>(
                name: "printDate",
                table: "TR_ReminderLetter",
                nullable: true,
                oldClrType: typeof(DateTime));

            migrationBuilder.AlterColumn<DateTime>(
                name: "mailDate",
                table: "TR_ReminderLetter",
                nullable: true,
                oldClrType: typeof(DateTime));

            migrationBuilder.AlterColumn<DateTime>(
                name: "clearDate",
                table: "TR_ReminderLetter",
                nullable: true,
                oldClrType: typeof(DateTime));

            migrationBuilder.AlterColumn<decimal>(
                name: "penaltyAmount",
                table: "TR_PenaltySchedule",
                type: "money",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "money",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "penaltyAging",
                table: "TR_PenaltySchedule",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "ScheduleNetAmount",
                table: "TR_PenaltySchedule",
                type: "money",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "money",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ScheduleAllocCode",
                table: "TR_PenaltySchedule",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "penaltyFreq",
                table: "MS_ClusterPenalty",
                maxLength: 8,
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "penaltyFreq",
                table: "MS_ClusterPenalty");

            migrationBuilder.RenameColumn(
                name: "bookingPeriodStart",
                table: "MS_ClusterPenalty",
                newName: "bookingPeriodeStart");

            migrationBuilder.RenameColumn(
                name: "bookingPeriodEnd",
                table: "MS_ClusterPenalty",
                newName: "bookingPeriodeEnd");

            migrationBuilder.RenameColumn(
                name: "autoWavePeriod",
                table: "MS_ClusterPenalty",
                newName: "autoWavePeriode");

            migrationBuilder.AlterColumn<DateTime>(
                name: "receiveDate",
                table: "TR_ReminderLetter",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "printDate",
                table: "TR_ReminderLetter",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "mailDate",
                table: "TR_ReminderLetter",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "clearDate",
                table: "TR_ReminderLetter",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "penaltyAmount",
                table: "TR_PenaltySchedule",
                type: "money",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "money");

            migrationBuilder.AlterColumn<int>(
                name: "penaltyAging",
                table: "TR_PenaltySchedule",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<decimal>(
                name: "ScheduleNetAmount",
                table: "TR_PenaltySchedule",
                type: "money",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "money");

            migrationBuilder.AlterColumn<string>(
                name: "ScheduleAllocCode",
                table: "TR_PenaltySchedule",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50);

            migrationBuilder.AddColumn<string>(
                name: "createdBy",
                table: "TR_PenaltySchedule",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "createdOn",
                table: "TR_PenaltySchedule",
                nullable: true);
        }
    }
}
