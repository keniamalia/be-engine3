﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PropertySystemDb
{
    public partial class alter_table_trLogMobey_void : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "isVoid",
                table: "TR_LogMobey",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "voidTime",
                table: "TR_LogMobey",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "isVoid",
                table: "TR_LogMobey");

            migrationBuilder.DropColumn(
                name: "voidTime",
                table: "TR_LogMobey");
        }
    }
}
