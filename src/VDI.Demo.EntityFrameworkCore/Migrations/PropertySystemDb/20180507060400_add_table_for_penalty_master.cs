﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PropertySystemDb
{
    public partial class add_table_for_penalty_master : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "LK_LetterType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    duration = table.Column<int>(nullable: false),
                    letterDesc = table.Column<string>(maxLength: 30, nullable: false),
                    letterType = table.Column<string>(maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LK_LetterType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LK_SPGenerate",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    SPGenerateDesc = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LK_SPGenerate", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MS_SPPeriod",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    SPGenerateID = table.Column<int>(nullable: false),
                    clusterID = table.Column<int>(nullable: false),
                    daysDue = table.Column<int>(nullable: false),
                    entityID = table.Column<int>(nullable: false),
                    letterTypeID = table.Column<int>(nullable: false),
                    templateName = table.Column<string>(maxLength: 100, nullable: false),
                    templateUrl = table.Column<string>(maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MS_SPPeriod", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MS_SPPeriod_LK_SPGenerate_SPGenerateID",
                        column: x => x.SPGenerateID,
                        principalTable: "LK_SPGenerate",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MS_SPPeriod_MS_Cluster_clusterID",
                        column: x => x.clusterID,
                        principalTable: "MS_Cluster",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MS_SPPeriod_LK_LetterType_letterTypeID",
                        column: x => x.letterTypeID,
                        principalTable: "LK_LetterType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MS_SPPeriod_SPGenerateID",
                table: "MS_SPPeriod",
                column: "SPGenerateID");

            migrationBuilder.CreateIndex(
                name: "IX_MS_SPPeriod_clusterID",
                table: "MS_SPPeriod",
                column: "clusterID");

            migrationBuilder.CreateIndex(
                name: "IX_MS_SPPeriod_letterTypeID",
                table: "MS_SPPeriod",
                column: "letterTypeID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MS_SPPeriod");

            migrationBuilder.DropTable(
                name: "LK_SPGenerate");

            migrationBuilder.DropTable(
                name: "LK_LetterType");
        }
    }
}
