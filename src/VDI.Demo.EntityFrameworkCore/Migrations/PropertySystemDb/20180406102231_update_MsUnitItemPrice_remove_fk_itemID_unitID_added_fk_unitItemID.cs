﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PropertySystemDb
{
    public partial class update_MsUnitItemPrice_remove_fk_itemID_unitID_added_fk_unitItemID : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MS_UnitItemPrice_LK_Item_itemID",
                table: "MS_UnitItemPrice");

            migrationBuilder.DropForeignKey(
                name: "FK_MS_UnitItemPrice_MS_Unit_unitID",
                table: "MS_UnitItemPrice");

            migrationBuilder.DropIndex(
                name: "IX_MS_UnitItemPrice_itemID",
                table: "MS_UnitItemPrice");

            migrationBuilder.DropColumn(
                name: "itemID",
                table: "MS_UnitItemPrice");

            migrationBuilder.RenameColumn(
                name: "unitID",
                table: "MS_UnitItemPrice",
                newName: "unitItemID");

            migrationBuilder.RenameIndex(
                name: "IX_MS_UnitItemPrice_unitID",
                table: "MS_UnitItemPrice",
                newName: "IX_MS_UnitItemPrice_unitItemID");

            migrationBuilder.AddForeignKey(
                name: "FK_MS_UnitItemPrice_MS_UnitItem_unitItemID",
                table: "MS_UnitItemPrice",
                column: "unitItemID",
                principalTable: "MS_UnitItem",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MS_UnitItemPrice_MS_UnitItem_unitItemID",
                table: "MS_UnitItemPrice");

            migrationBuilder.RenameColumn(
                name: "unitItemID",
                table: "MS_UnitItemPrice",
                newName: "unitID");

            migrationBuilder.RenameIndex(
                name: "IX_MS_UnitItemPrice_unitItemID",
                table: "MS_UnitItemPrice",
                newName: "IX_MS_UnitItemPrice_unitID");

            migrationBuilder.AddColumn<int>(
                name: "itemID",
                table: "MS_UnitItemPrice",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_MS_UnitItemPrice_itemID",
                table: "MS_UnitItemPrice",
                column: "itemID");

            migrationBuilder.AddForeignKey(
                name: "FK_MS_UnitItemPrice_LK_Item_itemID",
                table: "MS_UnitItemPrice",
                column: "itemID",
                principalTable: "LK_Item",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MS_UnitItemPrice_MS_Unit_unitID",
                table: "MS_UnitItemPrice",
                column: "unitID",
                principalTable: "MS_Unit",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
