﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PropertySystemDb
{
    public partial class fixing_2_table_sys_lippo_master : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "rolesName",
                table: "SYS_RolesProject");

            migrationBuilder.DropColumn(
                name: "rolesName",
                table: "SYS_RolesAccount");

            migrationBuilder.AddColumn<int>(
                name: "rolesID",
                table: "SYS_RolesProject",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "rolesID",
                table: "SYS_RolesAccount",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "rolesID",
                table: "SYS_RolesProject");

            migrationBuilder.DropColumn(
                name: "rolesID",
                table: "SYS_RolesAccount");

            migrationBuilder.AddColumn<string>(
                name: "rolesName",
                table: "SYS_RolesProject",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "rolesName",
                table: "SYS_RolesAccount",
                nullable: true);
        }
    }
}
