﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PropertySystemDb
{
    public partial class remove_field_launchingdisc_MsUnitItem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "launchingDisc",
                table: "MS_UnitItem");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "launchingDisc",
                table: "MS_UnitItem",
                nullable: false,
                defaultValue: 0.0);
        }
    }
}
