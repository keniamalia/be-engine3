﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PropertySystemDb
{
    public partial class Add_Table_Collectability_Update_Field_In_OfferingLetter_To_Money : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "collectability",
                table: "TR_SLIKCheckingDetail ");

            migrationBuilder.DropColumn(
                name: "profondApproved",
                table: "TR_OfferingLetter");

            migrationBuilder.DropColumn(
                name: "profondRequest",
                table: "TR_OfferingLetter");

            migrationBuilder.AddColumn<int>(
                name: "collectabilityID",
                table: "TR_SLIKCheckingDetail ",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<decimal>(
                name: "plafondApproved",
                table: "TR_OfferingLetter",
                type: "money",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "plafondRequest",
                table: "TR_OfferingLetter",
                type: "money",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.CreateTable(
                name: "MS_Collectability",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    description = table.Column<string>(maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MS_Collectability", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MS_TenorParameter",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    number = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MS_TenorParameter", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TR_SLIKCheckingDetail _collectabilityID",
                table: "TR_SLIKCheckingDetail ",
                column: "collectabilityID");

            migrationBuilder.CreateIndex(
                name: "IX_TR_OfferingLetter_tenor",
                table: "TR_OfferingLetter",
                column: "tenor");

            migrationBuilder.AddForeignKey(
                name: "FK_TR_OfferingLetter_MS_TenorParameter_tenor",
                table: "TR_OfferingLetter",
                column: "tenor",
                principalTable: "MS_TenorParameter",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TR_SLIKCheckingDetail _MS_Collectability_collectabilityID",
                table: "TR_SLIKCheckingDetail ",
                column: "collectabilityID",
                principalTable: "MS_Collectability",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TR_OfferingLetter_MS_TenorParameter_tenor",
                table: "TR_OfferingLetter");

            migrationBuilder.DropForeignKey(
                name: "FK_TR_SLIKCheckingDetail _MS_Collectability_collectabilityID",
                table: "TR_SLIKCheckingDetail ");

            migrationBuilder.DropTable(
                name: "MS_Collectability");

            migrationBuilder.DropTable(
                name: "MS_TenorParameter");

            migrationBuilder.DropIndex(
                name: "IX_TR_SLIKCheckingDetail _collectabilityID",
                table: "TR_SLIKCheckingDetail ");

            migrationBuilder.DropIndex(
                name: "IX_TR_OfferingLetter_tenor",
                table: "TR_OfferingLetter");

            migrationBuilder.DropColumn(
                name: "collectabilityID",
                table: "TR_SLIKCheckingDetail ");

            migrationBuilder.DropColumn(
                name: "plafondApproved",
                table: "TR_OfferingLetter");

            migrationBuilder.DropColumn(
                name: "plafondRequest",
                table: "TR_OfferingLetter");

            migrationBuilder.AddColumn<string>(
                name: "collectability",
                table: "TR_SLIKCheckingDetail ",
                maxLength: 10,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "profondApproved",
                table: "TR_OfferingLetter",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "profondRequest",
                table: "TR_OfferingLetter",
                nullable: false,
                defaultValue: 0);
        }
    }
}
