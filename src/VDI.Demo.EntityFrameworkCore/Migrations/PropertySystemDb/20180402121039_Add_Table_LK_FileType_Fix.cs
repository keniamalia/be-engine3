﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PropertySystemDb
{
    public partial class Add_Table_LK_FileType_Fix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "fileTypeID",
                table: "TR_ProjectImageGallery",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "LK_FileType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LK_FileType", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TR_ProjectImageGallery_fileTypeID",
                table: "TR_ProjectImageGallery",
                column: "fileTypeID");

            migrationBuilder.AddForeignKey(
                name: "FK_TR_ProjectImageGallery_LK_FileType_fileTypeID",
                table: "TR_ProjectImageGallery",
                column: "fileTypeID",
                principalTable: "LK_FileType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TR_ProjectImageGallery_LK_FileType_fileTypeID",
                table: "TR_ProjectImageGallery");

            migrationBuilder.DropTable(
                name: "LK_FileType");

            migrationBuilder.DropIndex(
                name: "IX_TR_ProjectImageGallery_fileTypeID",
                table: "TR_ProjectImageGallery");

            migrationBuilder.DropColumn(
                name: "fileTypeID",
                table: "TR_ProjectImageGallery");
        }
    }
}
