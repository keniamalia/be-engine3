﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PropertySystemDb
{
    public partial class Update_Table_BankPortal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Akad_MS_Bank_akadBank",
                table: "Akad");

            migrationBuilder.DropForeignKey(
                name: "FK_Akad_TR_BookingHeader_bookingID",
                table: "Akad");

            migrationBuilder.DropForeignKey(
                name: "FK_Bilyet_TR_BookingHeader_bookingID",
                table: "Bilyet");

            migrationBuilder.DropForeignKey(
                name: "FK_TR_CN_TR_BookingHeader_bookingID",
                table: "TR_CN");

            migrationBuilder.DropForeignKey(
                name: "FK_TR_OfferingLetter_TR_BankDetail_bankDetailID",
                table: "TR_OfferingLetter");

            migrationBuilder.DropForeignKey(
                name: "FK_TR_OfferingLetter_TR_BookingHeader_bookingID",
                table: "TR_OfferingLetter");

            migrationBuilder.DropForeignKey(
                name: "FK_TR_SI_TR_BookingHeader_bookingID",
                table: "TR_SI");

            migrationBuilder.DropForeignKey(
                name: "FK_TR_SKL_TR_BookingHeader_bookingID",
                table: "TR_SKL");

            migrationBuilder.DropIndex(
                name: "IX_TR_OfferingLetter_bankDetailID",
                table: "TR_OfferingLetter");

            migrationBuilder.DropIndex(
                name: "IX_Akad_akadBank",
                table: "Akad");

            migrationBuilder.DropColumn(
                name: "expiredTime",
                table: "TR_SLIKCheckingDetail ");

            migrationBuilder.DropColumn(
                name: "bookingID",
                table: "TR_SLIKChecking");

            migrationBuilder.DropColumn(
                name: "bankDetailID",
                table: "TR_OfferingLetter");

            migrationBuilder.DropColumn(
                name: "akadBank",
                table: "Akad");

            migrationBuilder.RenameColumn(
                name: "bookingID",
                table: "TR_SKL",
                newName: "offeringLetterID");

            migrationBuilder.RenameIndex(
                name: "IX_TR_SKL_bookingID",
                table: "TR_SKL",
                newName: "IX_TR_SKL_offeringLetterID");

            migrationBuilder.RenameColumn(
                name: "bookingID",
                table: "TR_SI",
                newName: "offeringLetterID");

            migrationBuilder.RenameIndex(
                name: "IX_TR_SI_bookingID",
                table: "TR_SI",
                newName: "IX_TR_SI_offeringLetterID");

            migrationBuilder.RenameColumn(
                name: "bookingID",
                table: "TR_OfferingLetter",
                newName: "slikCheckingDetailID");

            migrationBuilder.RenameIndex(
                name: "IX_TR_OfferingLetter_bookingID",
                table: "TR_OfferingLetter",
                newName: "IX_TR_OfferingLetter_slikCheckingDetailID");

            migrationBuilder.RenameColumn(
                name: "bookingID",
                table: "TR_CN",
                newName: "offeringLetterID");

            migrationBuilder.RenameIndex(
                name: "IX_TR_CN_bookingID",
                table: "TR_CN",
                newName: "IX_TR_CN_offeringLetterID");

            migrationBuilder.RenameColumn(
                name: "bilyetNO",
                table: "Bilyet",
                newName: "bilyetNo");

            migrationBuilder.RenameColumn(
                name: "bookingID",
                table: "Bilyet",
                newName: "offeringLetterID");

            migrationBuilder.RenameIndex(
                name: "IX_Bilyet_bookingID",
                table: "Bilyet",
                newName: "IX_Bilyet_offeringLetterID");

            migrationBuilder.RenameColumn(
                name: "bookingID",
                table: "Akad",
                newName: "slikCheckingDetailID");

            migrationBuilder.RenameIndex(
                name: "IX_Akad_bookingID",
                table: "Akad",
                newName: "IX_Akad_slikCheckingDetailID");

            migrationBuilder.AlterColumn<string>(
                name: "userType",
                table: "TR_UpdateRetention",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "remarks",
                table: "TR_SLIKCheckingDetail ",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50);

            migrationBuilder.AddColumn<DateTime>(
                name: "expiredDate",
                table: "TR_SLIKCheckingDetail ",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "key",
                table: "TR_SLIKChecking",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "psCode",
                table: "TR_SLIKChecking",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<string>(
                name: "userType",
                table: "TR_OfferingLetter",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "remark",
                table: "TR_OfferingLetter",
                maxLength: 150,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 150);

            migrationBuilder.AlterColumn<decimal>(
                name: "plafondApproved",
                table: "TR_OfferingLetter",
                type: "money",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "money");

            migrationBuilder.AlterColumn<decimal>(
                name: "bankRate",
                table: "TR_OfferingLetter",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "bankBranch",
                table: "TR_OfferingLetter",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.CreateTable(
                name: "MS_GenerateSetting",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    description = table.Column<string>(maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MS_GenerateSetting", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TR_SKL_generateSetting",
                table: "TR_SKL",
                column: "generateSetting");

            migrationBuilder.AddForeignKey(
                name: "FK_Akad_TR_SLIKCheckingDetail _slikCheckingDetailID",
                table: "Akad",
                column: "slikCheckingDetailID",
                principalTable: "TR_SLIKCheckingDetail ",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Bilyet_TR_OfferingLetter_offeringLetterID",
                table: "Bilyet",
                column: "offeringLetterID",
                principalTable: "TR_OfferingLetter",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TR_CN_TR_OfferingLetter_offeringLetterID",
                table: "TR_CN",
                column: "offeringLetterID",
                principalTable: "TR_OfferingLetter",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TR_OfferingLetter_TR_SLIKCheckingDetail _slikCheckingDetailID",
                table: "TR_OfferingLetter",
                column: "slikCheckingDetailID",
                principalTable: "TR_SLIKCheckingDetail ",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TR_SI_TR_OfferingLetter_offeringLetterID",
                table: "TR_SI",
                column: "offeringLetterID",
                principalTable: "TR_OfferingLetter",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TR_SKL_MS_GenerateSetting_generateSetting",
                table: "TR_SKL",
                column: "generateSetting",
                principalTable: "MS_GenerateSetting",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TR_SKL_TR_OfferingLetter_offeringLetterID",
                table: "TR_SKL",
                column: "offeringLetterID",
                principalTable: "TR_OfferingLetter",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Akad_TR_SLIKCheckingDetail _slikCheckingDetailID",
                table: "Akad");

            migrationBuilder.DropForeignKey(
                name: "FK_Bilyet_TR_OfferingLetter_offeringLetterID",
                table: "Bilyet");

            migrationBuilder.DropForeignKey(
                name: "FK_TR_CN_TR_OfferingLetter_offeringLetterID",
                table: "TR_CN");

            migrationBuilder.DropForeignKey(
                name: "FK_TR_OfferingLetter_TR_SLIKCheckingDetail _slikCheckingDetailID",
                table: "TR_OfferingLetter");

            migrationBuilder.DropForeignKey(
                name: "FK_TR_SI_TR_OfferingLetter_offeringLetterID",
                table: "TR_SI");

            migrationBuilder.DropForeignKey(
                name: "FK_TR_SKL_MS_GenerateSetting_generateSetting",
                table: "TR_SKL");

            migrationBuilder.DropForeignKey(
                name: "FK_TR_SKL_TR_OfferingLetter_offeringLetterID",
                table: "TR_SKL");

            migrationBuilder.DropTable(
                name: "MS_GenerateSetting");

            migrationBuilder.DropIndex(
                name: "IX_TR_SKL_generateSetting",
                table: "TR_SKL");

            migrationBuilder.DropColumn(
                name: "expiredDate",
                table: "TR_SLIKCheckingDetail ");

            migrationBuilder.DropColumn(
                name: "key",
                table: "TR_SLIKChecking");

            migrationBuilder.DropColumn(
                name: "psCode",
                table: "TR_SLIKChecking");

            migrationBuilder.RenameColumn(
                name: "offeringLetterID",
                table: "TR_SKL",
                newName: "bookingID");

            migrationBuilder.RenameIndex(
                name: "IX_TR_SKL_offeringLetterID",
                table: "TR_SKL",
                newName: "IX_TR_SKL_bookingID");

            migrationBuilder.RenameColumn(
                name: "offeringLetterID",
                table: "TR_SI",
                newName: "bookingID");

            migrationBuilder.RenameIndex(
                name: "IX_TR_SI_offeringLetterID",
                table: "TR_SI",
                newName: "IX_TR_SI_bookingID");

            migrationBuilder.RenameColumn(
                name: "slikCheckingDetailID",
                table: "TR_OfferingLetter",
                newName: "bookingID");

            migrationBuilder.RenameIndex(
                name: "IX_TR_OfferingLetter_slikCheckingDetailID",
                table: "TR_OfferingLetter",
                newName: "IX_TR_OfferingLetter_bookingID");

            migrationBuilder.RenameColumn(
                name: "offeringLetterID",
                table: "TR_CN",
                newName: "bookingID");

            migrationBuilder.RenameIndex(
                name: "IX_TR_CN_offeringLetterID",
                table: "TR_CN",
                newName: "IX_TR_CN_bookingID");

            migrationBuilder.RenameColumn(
                name: "bilyetNo",
                table: "Bilyet",
                newName: "bilyetNO");

            migrationBuilder.RenameColumn(
                name: "offeringLetterID",
                table: "Bilyet",
                newName: "bookingID");

            migrationBuilder.RenameIndex(
                name: "IX_Bilyet_offeringLetterID",
                table: "Bilyet",
                newName: "IX_Bilyet_bookingID");

            migrationBuilder.RenameColumn(
                name: "slikCheckingDetailID",
                table: "Akad",
                newName: "bookingID");

            migrationBuilder.RenameIndex(
                name: "IX_Akad_slikCheckingDetailID",
                table: "Akad",
                newName: "IX_Akad_bookingID");

            migrationBuilder.AlterColumn<string>(
                name: "userType",
                table: "TR_UpdateRetention",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "remarks",
                table: "TR_SLIKCheckingDetail ",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "expiredTime",
                table: "TR_SLIKCheckingDetail ",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "bookingID",
                table: "TR_SLIKChecking",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<string>(
                name: "userType",
                table: "TR_OfferingLetter",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "remark",
                table: "TR_OfferingLetter",
                maxLength: 150,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 150,
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "plafondApproved",
                table: "TR_OfferingLetter",
                type: "money",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "money",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "bankRate",
                table: "TR_OfferingLetter",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "bankBranch",
                table: "TR_OfferingLetter",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "bankDetailID",
                table: "TR_OfferingLetter",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "akadBank",
                table: "Akad",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_TR_OfferingLetter_bankDetailID",
                table: "TR_OfferingLetter",
                column: "bankDetailID");

            migrationBuilder.CreateIndex(
                name: "IX_Akad_akadBank",
                table: "Akad",
                column: "akadBank");

            migrationBuilder.AddForeignKey(
                name: "FK_Akad_MS_Bank_akadBank",
                table: "Akad",
                column: "akadBank",
                principalTable: "MS_Bank",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Akad_TR_BookingHeader_bookingID",
                table: "Akad",
                column: "bookingID",
                principalTable: "TR_BookingHeader",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Bilyet_TR_BookingHeader_bookingID",
                table: "Bilyet",
                column: "bookingID",
                principalTable: "TR_BookingHeader",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TR_CN_TR_BookingHeader_bookingID",
                table: "TR_CN",
                column: "bookingID",
                principalTable: "TR_BookingHeader",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TR_OfferingLetter_TR_BankDetail_bankDetailID",
                table: "TR_OfferingLetter",
                column: "bankDetailID",
                principalTable: "TR_BankDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TR_OfferingLetter_TR_BookingHeader_bookingID",
                table: "TR_OfferingLetter",
                column: "bookingID",
                principalTable: "TR_BookingHeader",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TR_SI_TR_BookingHeader_bookingID",
                table: "TR_SI",
                column: "bookingID",
                principalTable: "TR_BookingHeader",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TR_SKL_TR_BookingHeader_bookingID",
                table: "TR_SKL",
                column: "bookingID",
                principalTable: "TR_BookingHeader",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
