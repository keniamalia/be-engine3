﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PropertySystemDb
{
    public partial class add_table_tr_paymentChangeUnknown : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TR_PaymentChangeUnknown",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    accountID = table.Column<int>(nullable: false),
                    changeDate = table.Column<DateTime>(nullable: false),
                    changeNo = table.Column<string>(maxLength: 20, nullable: false),
                    entityID = table.Column<int>(nullable: false),
                    reason = table.Column<string>(maxLength: 300, nullable: false),
                    transNo = table.Column<string>(maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TR_PaymentChangeUnknown", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TR_PaymentChangeUnknown_MS_Account_accountID",
                        column: x => x.accountID,
                        principalTable: "MS_Account",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TR_PaymentChangeUnknown_accountID",
                table: "TR_PaymentChangeUnknown",
                column: "accountID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TR_PaymentChangeUnknown");
        }
    }
}
