﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PropertySystemDb
{
    public partial class add_tb_msparameter_changedatatype_historyno : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<short>(
                name: "historyNo",
                table: "TR_PaymentDetailAllocHistory",
                nullable: false,
                oldClrType: typeof(byte));

            migrationBuilder.AlterColumn<short>(
                name: "historyNo",
                table: "TR_DPHistory",
                nullable: false,
                oldClrType: typeof(byte));

            migrationBuilder.AlterColumn<short>(
                name: "historyNo",
                table: "TR_BookingHeaderHistory",
                nullable: false,
                oldClrType: typeof(byte));

            migrationBuilder.AlterColumn<short>(
                name: "historyNo",
                table: "TR_BookingDetailHistory",
                nullable: false,
                oldClrType: typeof(byte));

            migrationBuilder.AlterColumn<short>(
                name: "historyNo",
                table: "TR_BookingDetailAddDiscHistory",
                nullable: false,
                oldClrType: typeof(byte));

            migrationBuilder.CreateTable(
                name: "MS_Parameter",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    code = table.Column<string>(maxLength: 5, nullable: false),
                    dataType = table.Column<string>(maxLength: 30, nullable: false),
                    description = table.Column<string>(maxLength: 50, nullable: false),
                    isActive = table.Column<bool>(nullable: false),
                    value = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MS_Parameter", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MS_Parameter");

            migrationBuilder.AlterColumn<byte>(
                name: "historyNo",
                table: "TR_PaymentDetailAllocHistory",
                nullable: false,
                oldClrType: typeof(short));

            migrationBuilder.AlterColumn<byte>(
                name: "historyNo",
                table: "TR_DPHistory",
                nullable: false,
                oldClrType: typeof(short));

            migrationBuilder.AlterColumn<byte>(
                name: "historyNo",
                table: "TR_BookingHeaderHistory",
                nullable: false,
                oldClrType: typeof(short));

            migrationBuilder.AlterColumn<byte>(
                name: "historyNo",
                table: "TR_BookingDetailHistory",
                nullable: false,
                oldClrType: typeof(short));

            migrationBuilder.AlterColumn<byte>(
                name: "historyNo",
                table: "TR_BookingDetailAddDiscHistory",
                nullable: false,
                oldClrType: typeof(short));
        }
    }
}
