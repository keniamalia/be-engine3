﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PropertySystemDb
{
    public partial class update_table_MsSPPeriod_Add_KuasaDir : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "kuasaDir1",
                table: "MS_SPPeriod",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "kuasaDir2",
                table: "MS_SPPeriod",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "kuasaDir1",
                table: "MS_SPPeriod");

            migrationBuilder.DropColumn(
                name: "kuasaDir2",
                table: "MS_SPPeriod");
        }
    }
}
