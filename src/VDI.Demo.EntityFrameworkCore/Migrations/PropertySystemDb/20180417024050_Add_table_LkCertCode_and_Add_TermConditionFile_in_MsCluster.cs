﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PropertySystemDb
{
    public partial class Add_table_LkCertCode_and_Add_TermConditionFile_in_MsCluster : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "termConditionFile",
                table: "MS_Cluster",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "LK_CertCode",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    certCode = table.Column<string>(maxLength: 1, nullable: false),
                    certDesc = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LK_CertCode", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "certCodeUnique",
                table: "LK_CertCode",
                column: "certCode",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LK_CertCode");

            migrationBuilder.DropColumn(
                name: "termConditionFile",
                table: "MS_Cluster");
        }
    }
}
