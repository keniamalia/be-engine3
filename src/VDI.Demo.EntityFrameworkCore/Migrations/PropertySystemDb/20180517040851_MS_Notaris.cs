﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PropertySystemDb
{
    public partial class MS_Notaris : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MS_Notaris",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    alamat = table.Column<string>(maxLength: 250, nullable: true),
                    businessGroup = table.Column<string>(maxLength: 50, nullable: true),
                    clusterID = table.Column<int>(nullable: false),
                    email = table.Column<string>(maxLength: 100, nullable: true),
                    kawasan = table.Column<string>(maxLength: 150, nullable: true),
                    namaNotaris = table.Column<string>(maxLength: 100, nullable: true),
                    phoneNo = table.Column<string>(maxLength: 100, nullable: true),
                    projectID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MS_Notaris", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MS_Notaris_MS_Cluster_clusterID",
                        column: x => x.clusterID,
                        principalTable: "MS_Cluster",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MS_Notaris_MS_Project_projectID",
                        column: x => x.projectID,
                        principalTable: "MS_Project",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MS_Notaris_clusterID",
                table: "MS_Notaris",
                column: "clusterID");

            migrationBuilder.CreateIndex(
                name: "IX_MS_Notaris_projectID",
                table: "MS_Notaris",
                column: "projectID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MS_Notaris");
        }
    }
}
