﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PropertySystemDb
{
    public partial class Add_Internal_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Akad",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    akadBank = table.Column<int>(nullable: false),
                    akadDate = table.Column<DateTime>(nullable: false),
                    akadDocumentName = table.Column<string>(maxLength: 200, nullable: true),
                    akadDocumentUrl = table.Column<string>(maxLength: 350, nullable: true),
                    akadNo = table.Column<string>(maxLength: 15, nullable: false),
                    bookingID = table.Column<int>(nullable: false),
                    isActive = table.Column<bool>(nullable: false),
                    userType = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Akad", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Akad_MS_Bank_akadBank",
                        column: x => x.akadBank,
                        principalTable: "MS_Bank",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Akad_TR_BookingHeader_bookingID",
                        column: x => x.bookingID,
                        principalTable: "TR_BookingHeader",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TR_CN",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CNDocName = table.Column<string>(maxLength: 200, nullable: false),
                    CNDocUrl = table.Column<string>(maxLength: 350, nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    bookingID = table.Column<int>(nullable: false),
                    generateTo = table.Column<string>(maxLength: 20, nullable: false),
                    isActive = table.Column<bool>(nullable: false),
                    message = table.Column<string>(maxLength: 100, nullable: true),
                    statusGenerate = table.Column<string>(maxLength: 10, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TR_CN", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TR_CN_TR_BookingHeader_bookingID",
                        column: x => x.bookingID,
                        principalTable: "TR_BookingHeader",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TR_OfferingLetter",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    bankBranch = table.Column<int>(nullable: false),
                    bankDetailID = table.Column<int>(nullable: false),
                    bankRate = table.Column<int>(nullable: false),
                    bookingID = table.Column<int>(nullable: false),
                    dpPercentage = table.Column<int>(nullable: false),
                    isActive = table.Column<bool>(nullable: false),
                    offeringLetterDoc = table.Column<string>(maxLength: 200, nullable: false),
                    offeringLetterUrl = table.Column<string>(maxLength: 350, nullable: false),
                    profondApproved = table.Column<int>(nullable: false),
                    profondRequest = table.Column<int>(nullable: false),
                    remark = table.Column<string>(maxLength: 150, nullable: false),
                    tenor = table.Column<int>(nullable: false),
                    userType = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TR_OfferingLetter", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TR_OfferingLetter_TR_BankDetail_bankDetailID",
                        column: x => x.bankDetailID,
                        principalTable: "TR_BankDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TR_OfferingLetter_TR_BookingHeader_bookingID",
                        column: x => x.bookingID,
                        principalTable: "TR_BookingHeader",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TR_SI",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    SIDocName = table.Column<string>(maxLength: 200, nullable: false),
                    SIDocUrl = table.Column<string>(maxLength: 350, nullable: false),
                    bookingID = table.Column<int>(nullable: false),
                    generateTo = table.Column<string>(maxLength: 20, nullable: false),
                    isActive = table.Column<bool>(nullable: false),
                    message = table.Column<string>(maxLength: 100, nullable: true),
                    statusGenerate = table.Column<string>(maxLength: 10, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TR_SI", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TR_SI_TR_BookingHeader_bookingID",
                        column: x => x.bookingID,
                        principalTable: "TR_BookingHeader",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TR_SKL",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    SKLDocName = table.Column<string>(maxLength: 200, nullable: false),
                    SKLDocUrl = table.Column<string>(maxLength: 350, nullable: false),
                    bookingID = table.Column<int>(nullable: false),
                    dateGenerate = table.Column<DateTime>(nullable: false),
                    generateSetting = table.Column<int>(nullable: false),
                    isActive = table.Column<bool>(nullable: false),
                    message = table.Column<string>(maxLength: 100, nullable: false),
                    statusGenerate = table.Column<string>(maxLength: 10, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TR_SKL", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TR_SKL_TR_BookingHeader_bookingID",
                        column: x => x.bookingID,
                        principalTable: "TR_BookingHeader",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TR_SLIKChecking",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    UserType = table.Column<string>(nullable: false),
                    bookingID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TR_SLIKChecking", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TR_UpdateRetention",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    buildingType = table.Column<string>(maxLength: 8, nullable: false),
                    clusterID = table.Column<int>(nullable: false),
                    docName = table.Column<string>(maxLength: 200, nullable: false),
                    docUrl = table.Column<string>(maxLength: 350, nullable: false),
                    finishDate = table.Column<DateTime>(nullable: false),
                    isActive = table.Column<bool>(nullable: false),
                    retentionDetailID = table.Column<int>(nullable: false),
                    status = table.Column<string>(maxLength: 100, nullable: false),
                    unitCode = table.Column<string>(maxLength: 8, nullable: true),
                    unitNumber = table.Column<string>(maxLength: 8, nullable: true),
                    userType = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TR_UpdateRetention", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TR_UpdateRetention_MS_Cluster_clusterID",
                        column: x => x.clusterID,
                        principalTable: "MS_Cluster",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TR_UpdateRetention_TR_RetentionDetail_retentionDetailID",
                        column: x => x.retentionDetailID,
                        principalTable: "TR_RetentionDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TR_SKLDetailManual",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    isActive = table.Column<bool>(nullable: false),
                    paymentDate = table.Column<DateTime>(nullable: false),
                    sklID = table.Column<int>(nullable: false),
                    totalPayment = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TR_SKLDetailManual", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TR_SKLDetailManual_TR_SKL_sklID",
                        column: x => x.sklID,
                        principalTable: "TR_SKL",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TR_SLIKCheckingDetail ",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    bankDetailID = table.Column<int>(nullable: false),
                    collectability = table.Column<string>(maxLength: 10, nullable: false),
                    expiredTime = table.Column<DateTime>(nullable: false),
                    isActive = table.Column<bool>(nullable: false),
                    isChange = table.Column<bool>(nullable: false),
                    remarks = table.Column<string>(maxLength: 50, nullable: false),
                    slikCheckingDate = table.Column<DateTime>(nullable: false),
                    slikCheckingID = table.Column<int>(nullable: false),
                    slikStatus = table.Column<bool>(nullable: true),
                    userType = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TR_SLIKCheckingDetail ", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TR_SLIKCheckingDetail _TR_BankDetail_bankDetailID",
                        column: x => x.bankDetailID,
                        principalTable: "TR_BankDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TR_SLIKCheckingDetail _TR_SLIKChecking_slikCheckingID",
                        column: x => x.slikCheckingID,
                        principalTable: "TR_SLIKChecking",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Bilyet",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    bilyetDocumentName = table.Column<string>(maxLength: 200, nullable: false),
                    bilyetDocumentUrl = table.Column<string>(maxLength: 350, nullable: false),
                    bilyetNO = table.Column<string>(maxLength: 15, nullable: false),
                    bookingID = table.Column<int>(nullable: false),
                    dueDate = table.Column<DateTime>(nullable: false),
                    isActive = table.Column<bool>(nullable: false),
                    updateRetentionID = table.Column<int>(nullable: false),
                    userTypeCreation = table.Column<string>(nullable: false),
                    userTypeModification = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bilyet", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Bilyet_TR_BookingHeader_bookingID",
                        column: x => x.bookingID,
                        principalTable: "TR_BookingHeader",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Bilyet_TR_UpdateRetention_updateRetentionID",
                        column: x => x.updateRetentionID,
                        principalTable: "TR_UpdateRetention",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TR_SLIKCheckingHistory",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    isActive = table.Column<bool>(nullable: false),
                    newSlikCheckingDetailID = table.Column<int>(nullable: false),
                    slikCheckingDetailID = table.Column<int>(nullable: false),
                    userType = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TR_SLIKCheckingHistory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TR_SLIKCheckingHistory_TR_SLIKCheckingDetail _slikCheckingDetailID",
                        column: x => x.slikCheckingDetailID,
                        principalTable: "TR_SLIKCheckingDetail ",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Akad_akadBank",
                table: "Akad",
                column: "akadBank");

            migrationBuilder.CreateIndex(
                name: "IX_Akad_bookingID",
                table: "Akad",
                column: "bookingID");

            migrationBuilder.CreateIndex(
                name: "IX_Bilyet_bookingID",
                table: "Bilyet",
                column: "bookingID");

            migrationBuilder.CreateIndex(
                name: "IX_Bilyet_updateRetentionID",
                table: "Bilyet",
                column: "updateRetentionID");

            migrationBuilder.CreateIndex(
                name: "IX_TR_CN_bookingID",
                table: "TR_CN",
                column: "bookingID");

            migrationBuilder.CreateIndex(
                name: "IX_TR_OfferingLetter_bankDetailID",
                table: "TR_OfferingLetter",
                column: "bankDetailID");

            migrationBuilder.CreateIndex(
                name: "IX_TR_OfferingLetter_bookingID",
                table: "TR_OfferingLetter",
                column: "bookingID");

            migrationBuilder.CreateIndex(
                name: "IX_TR_SI_bookingID",
                table: "TR_SI",
                column: "bookingID");

            migrationBuilder.CreateIndex(
                name: "IX_TR_SKL_bookingID",
                table: "TR_SKL",
                column: "bookingID");

            migrationBuilder.CreateIndex(
                name: "IX_TR_SKLDetailManual_sklID",
                table: "TR_SKLDetailManual",
                column: "sklID");

            migrationBuilder.CreateIndex(
                name: "IX_TR_SLIKCheckingDetail _bankDetailID",
                table: "TR_SLIKCheckingDetail ",
                column: "bankDetailID");

            migrationBuilder.CreateIndex(
                name: "IX_TR_SLIKCheckingDetail _slikCheckingID",
                table: "TR_SLIKCheckingDetail ",
                column: "slikCheckingID");

            migrationBuilder.CreateIndex(
                name: "IX_TR_SLIKCheckingHistory_slikCheckingDetailID",
                table: "TR_SLIKCheckingHistory",
                column: "slikCheckingDetailID");

            migrationBuilder.CreateIndex(
                name: "IX_TR_UpdateRetention_clusterID",
                table: "TR_UpdateRetention",
                column: "clusterID");

            migrationBuilder.CreateIndex(
                name: "IX_TR_UpdateRetention_retentionDetailID",
                table: "TR_UpdateRetention",
                column: "retentionDetailID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Akad");

            migrationBuilder.DropTable(
                name: "Bilyet");

            migrationBuilder.DropTable(
                name: "TR_CN");

            migrationBuilder.DropTable(
                name: "TR_OfferingLetter");

            migrationBuilder.DropTable(
                name: "TR_SI");

            migrationBuilder.DropTable(
                name: "TR_SKLDetailManual");

            migrationBuilder.DropTable(
                name: "TR_SLIKCheckingHistory");

            migrationBuilder.DropTable(
                name: "TR_UpdateRetention");

            migrationBuilder.DropTable(
                name: "TR_SKL");

            migrationBuilder.DropTable(
                name: "TR_SLIKCheckingDetail ");

            migrationBuilder.DropTable(
                name: "TR_SLIKChecking");
        }
    }
}
