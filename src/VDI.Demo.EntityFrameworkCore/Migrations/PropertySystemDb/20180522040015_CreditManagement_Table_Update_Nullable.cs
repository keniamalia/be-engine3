﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PropertySystemDb
{
    public partial class CreditManagement_Table_Update_Nullable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TR_RetentionDetail_MS_Retention_retentionID",
                table: "TR_RetentionDetail");

            migrationBuilder.AlterColumn<decimal>(
                name: "subsidyValue",
                table: "TR_SubsidyDetail",
                nullable: true,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<int>(
                name: "subsidyType",
                table: "TR_SubsidyDetail",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "sortNo",
                table: "TR_RetentionDetail",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "retentionID",
                table: "TR_RetentionDetail",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<decimal>(
                name: "percentage",
                table: "TR_RetentionDetail",
                nullable: true,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<int>(
                name: "tenor",
                table: "TR_ProgramDetail",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "fix",
                table: "TR_ProgramDetail",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<decimal>(
                name: "bankRate",
                table: "TR_ProgramDetail",
                nullable: true,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "provisiValue",
                table: "TR_AgreementDetail",
                nullable: true,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<int>(
                name: "provisiFeeType",
                table: "TR_AgreementDetail",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<decimal>(
                name: "adminFeeValue",
                table: "TR_AgreementDetail",
                nullable: true,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<int>(
                name: "adminFeeType",
                table: "TR_AgreementDetail",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "addendumNo",
                table: "TR_AgreementDetail",
                maxLength: 35,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 35);

            migrationBuilder.AddForeignKey(
                name: "FK_TR_RetentionDetail_MS_Retention_retentionID",
                table: "TR_RetentionDetail",
                column: "retentionID",
                principalTable: "MS_Retention",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TR_RetentionDetail_MS_Retention_retentionID",
                table: "TR_RetentionDetail");

            migrationBuilder.AlterColumn<decimal>(
                name: "subsidyValue",
                table: "TR_SubsidyDetail",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "subsidyType",
                table: "TR_SubsidyDetail",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "sortNo",
                table: "TR_RetentionDetail",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "retentionID",
                table: "TR_RetentionDetail",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "percentage",
                table: "TR_RetentionDetail",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "tenor",
                table: "TR_ProgramDetail",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "fix",
                table: "TR_ProgramDetail",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "bankRate",
                table: "TR_ProgramDetail",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "provisiValue",
                table: "TR_AgreementDetail",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "provisiFeeType",
                table: "TR_AgreementDetail",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "adminFeeValue",
                table: "TR_AgreementDetail",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "adminFeeType",
                table: "TR_AgreementDetail",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "addendumNo",
                table: "TR_AgreementDetail",
                maxLength: 35,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 35,
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_TR_RetentionDetail_MS_Retention_retentionID",
                table: "TR_RetentionDetail",
                column: "retentionID",
                principalTable: "MS_Retention",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
