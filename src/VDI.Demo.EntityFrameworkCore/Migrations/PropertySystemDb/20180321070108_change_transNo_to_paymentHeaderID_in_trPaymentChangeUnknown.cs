﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PropertySystemDb
{
    public partial class change_transNo_to_paymentHeaderID_in_trPaymentChangeUnknown : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TR_PaymentChangeUnknown_MS_Account_accountID",
                table: "TR_PaymentChangeUnknown");

            migrationBuilder.DropColumn(
                name: "transNo",
                table: "TR_PaymentChangeUnknown");

            migrationBuilder.RenameColumn(
                name: "accountID",
                table: "TR_PaymentChangeUnknown",
                newName: "paymentHeaderID");

            migrationBuilder.RenameIndex(
                name: "IX_TR_PaymentChangeUnknown_accountID",
                table: "TR_PaymentChangeUnknown",
                newName: "IX_TR_PaymentChangeUnknown_paymentHeaderID");

            migrationBuilder.AddColumn<int>(
                name: "MS_AccountId",
                table: "TR_PaymentChangeUnknown",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TR_PaymentChangeUnknown_MS_AccountId",
                table: "TR_PaymentChangeUnknown",
                column: "MS_AccountId");

            migrationBuilder.AddForeignKey(
                name: "FK_TR_PaymentChangeUnknown_MS_Account_MS_AccountId",
                table: "TR_PaymentChangeUnknown",
                column: "MS_AccountId",
                principalTable: "MS_Account",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TR_PaymentChangeUnknown_TR_PaymentHeader_paymentHeaderID",
                table: "TR_PaymentChangeUnknown",
                column: "paymentHeaderID",
                principalTable: "TR_PaymentHeader",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TR_PaymentChangeUnknown_MS_Account_MS_AccountId",
                table: "TR_PaymentChangeUnknown");

            migrationBuilder.DropForeignKey(
                name: "FK_TR_PaymentChangeUnknown_TR_PaymentHeader_paymentHeaderID",
                table: "TR_PaymentChangeUnknown");

            migrationBuilder.DropIndex(
                name: "IX_TR_PaymentChangeUnknown_MS_AccountId",
                table: "TR_PaymentChangeUnknown");

            migrationBuilder.DropColumn(
                name: "MS_AccountId",
                table: "TR_PaymentChangeUnknown");

            migrationBuilder.RenameColumn(
                name: "paymentHeaderID",
                table: "TR_PaymentChangeUnknown",
                newName: "accountID");

            migrationBuilder.RenameIndex(
                name: "IX_TR_PaymentChangeUnknown_paymentHeaderID",
                table: "TR_PaymentChangeUnknown",
                newName: "IX_TR_PaymentChangeUnknown_accountID");

            migrationBuilder.AddColumn<string>(
                name: "transNo",
                table: "TR_PaymentChangeUnknown",
                maxLength: 20,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddForeignKey(
                name: "FK_TR_PaymentChangeUnknown_MS_Account_accountID",
                table: "TR_PaymentChangeUnknown",
                column: "accountID",
                principalTable: "MS_Account",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
