﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace VDI.Demo.Migrations.PropertySystemDb
{
    public partial class delete_foreign_accountID : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TR_PaymentChangeUnknown_MS_Account_MS_AccountId",
                table: "TR_PaymentChangeUnknown");

            migrationBuilder.DropIndex(
                name: "IX_TR_PaymentChangeUnknown_MS_AccountId",
                table: "TR_PaymentChangeUnknown");

            migrationBuilder.DropColumn(
                name: "MS_AccountId",
                table: "TR_PaymentChangeUnknown");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "MS_AccountId",
                table: "TR_PaymentChangeUnknown",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TR_PaymentChangeUnknown_MS_AccountId",
                table: "TR_PaymentChangeUnknown",
                column: "MS_AccountId");

            migrationBuilder.AddForeignKey(
                name: "FK_TR_PaymentChangeUnknown_MS_Account_MS_AccountId",
                table: "TR_PaymentChangeUnknown",
                column: "MS_AccountId",
                principalTable: "MS_Account",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
