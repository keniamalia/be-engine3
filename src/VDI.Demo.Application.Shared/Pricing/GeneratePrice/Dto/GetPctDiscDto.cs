﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Pricing.GeneratePrice.Dto
{
    public class GetPctDiscDto
    {
        public int discountID { get; set; }
        public string discountCode { get; set; }
        public string discountName { get; set; }
        public double pctDisc { get; set; }
    }
}
