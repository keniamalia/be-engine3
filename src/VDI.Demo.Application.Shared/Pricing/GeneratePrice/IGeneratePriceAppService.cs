﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.Dto;
using VDI.Demo.Pricing.GeneratePrice.Dto;
using VDI.Demo.Pricing.MS_Terms.Dto;

namespace VDI.Demo.Pricing.GeneratePrice
{
    public interface IGeneratePriceAppService : IApplicationService
    {
        List<GetGeneratePriceListTermListDto> GetGeneratePriceListTermByTermID(int termID);
        void CreatePriceTaskList(CreatePriceTaskListInputDto input);
        List<GetPriceTaskListDto> GetPriceTaskList(int projectID);
        List<GetMsDiscountDto> GetDiscountDropdownOnlySalesDisc();
        GetPctDiscDto GetPctDiscByProjectClusterCategoryProduct(GetMsUnitByProjectClusterCategoryProduct input);

        // upload gross price
        FileDto GenerateExcelUploadGrossPrice(GetMsUnitByProjectClusterCategoryProduct input);
        FileDto ExportToExcelUploadGrossPrice(ExportToExcelUploadGrossPriceListDto input);

        // upload price list
        FileDto GenerateExcelUploadPriceList(GetMsUnitByProjectIdClusterIdDto input);
        FileDto ExportToExcelUploadPriceList(List<ExportToExcelUploadPriceListDto> input);
    }
}
