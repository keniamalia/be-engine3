﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Pricing.MS_Terms.Dto
{
    public class GetTermAddDiscByTermID
    {
        public int termID { get; set; }
        public int discountID { get; set; }
        public double addDiscPct { get; set; }
        public string discountName { get; set; }
    }
}
