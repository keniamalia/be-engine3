﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.MasterPlan.Project.Setting_PeriodSPs.Dto;

namespace VDI.Demo.MasterPlan.Project.Setting_PeriodSPs
{
    public interface ISettingPeriodSPAppService : IApplicationService
    {
        List<GetDropdownLKSPGenerateListDto> GetDropdownLKSPGenerate();
        List<GetDropdownKuasaDireksiDto> GetDropdownKuasaDireksiManagerPenalty();
        List<GetDropdownKuasaDireksiDto> GetDropdownKuasaDireksiStaffPenalty();
        List<GetDropdownLKLetterTypeListDto> GetAvailableDropdownLKLetterType(bool flag);
        List<GetAllMsSPPeriodByClusterListDto> GetAllMsSPPeriodByCluster(int clusterID);
        List<GetAllMsSPPeriodByClusterListDto> GetAllSettingDefaultSP();
        void CreateMsSPPeriod(CreateOrUpdateMsSPPeriodInputDto input);
        void UpdateMsSPPeriod(CreateOrUpdateMsSPPeriodInputDto input);
        void DeleteMsSPPeriod(int letterTypeID);
    }
}
