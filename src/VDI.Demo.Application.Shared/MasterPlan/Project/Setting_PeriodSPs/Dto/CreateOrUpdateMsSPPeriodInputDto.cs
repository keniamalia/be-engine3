﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.MasterPlan.Project.Setting_PeriodSPs.Dto
{
    public class CreateOrUpdateMsSPPeriodInputDto
    {
        public int clusterID { get; set; }
        public int Id { get; set; }
        public string templateName { get; set; }
        public string templateStatus { get; set; }
        public int daysDue { get; set; }
        public int SPGenerateID { get; set; }
        public int letterTypeID { get; set; }
        public int kuasaDir1 { get; set; }
        public int kuasaDir2 { get; set; }
        public int accountID { get; set; }
    }
}
