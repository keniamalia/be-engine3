﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.MasterPlan.Project.Setting_PeriodSPs.Dto
{
    public class GetDropdownLKLetterTypeListDto
    {
        public int Id { get; set; }
        public string letterType { get; set; }
        public string letterDesc { get; set; }
        public int duration { get; set; }
    }
}
