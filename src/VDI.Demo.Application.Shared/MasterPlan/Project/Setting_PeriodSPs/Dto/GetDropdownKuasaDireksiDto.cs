﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.MasterPlan.Project.Setting_PeriodSPs.Dto
{
    public class GetDropdownKuasaDireksiDto
    {
        public int? Id{ get; set; }
        public string officerName { get; set; }
        public bool isDefault { get; set; }
    }
}
