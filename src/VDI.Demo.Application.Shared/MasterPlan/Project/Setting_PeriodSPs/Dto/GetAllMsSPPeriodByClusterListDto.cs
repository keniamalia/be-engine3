﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.MasterPlan.Project.Setting_PeriodSPs.Dto
{
    public class GetAllMsSPPeriodByClusterListDto
    {
        public int Id { get; set; }
        public int clusterID { get; set; }
        public string clusterName { get; set; }
        public string templateName { get; set; }
        public string templateUrl { get; set; }
        public int daysDue { get; set; }
        public int SPGenerateID { get; set; }
        public string SPGenerateDesc { get; set; }
        public int letterTypeID { get; set; }
        public string letterType { get; set; }
        public int kuasaDir1 { get; set; }
        public int kuasaDir2 { get; set; }
        public int accountID { get; set; }
        public string accountCode { get; set; }
    }
}
