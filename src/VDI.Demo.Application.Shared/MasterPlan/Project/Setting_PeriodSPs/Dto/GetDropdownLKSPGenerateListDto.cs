﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.MasterPlan.Project.Setting_PeriodSPs.Dto
{
    public class GetDropdownLKSPGenerateListDto
    {
        public int Id { get; set; }
        public string SPGenerateDesc { get; set; }
    }
}
