﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.MasterPlan.Project.MS_Projects.Dto
{
    public class GetListBusinessGroupResultDto
    {
        public string businessGroupCode { get; set; }

        public string businessGroupName { get; set; }
    }
}
