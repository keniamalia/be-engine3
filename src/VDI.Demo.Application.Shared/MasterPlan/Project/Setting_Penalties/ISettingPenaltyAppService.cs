﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.MasterPlan.Project.Setting_Penalties.Dto;

namespace VDI.Demo.MasterPlan.Project.Setting_Penalties
{
    public interface ISettingPenaltyAppService : IApplicationService
    {
        List<GetDataMSClusterPenaltyListDto> GetDataMSClusterPenalty(int clusterID);
        GetDataMSClusterPenaltyByIDListDto GetDataMSClusterPenaltyByID(int clusterPenaltyID);
        void CreateOrUpdateMSClusterPenalty(GetDataMSClusterPenaltyByIDListDto input);
    }
}
