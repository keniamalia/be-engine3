﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.MasterPlan.Project.Setting_Penalties.Dto
{
    public class GetDataMSClusterPenaltyListDto
    {
        public int clusterPenaltyID { get; set; }
        public int projectID { get; set; }
        public string project { get; set; }
        public int clusterID { get; set; }
        public string cluster { get; set; }
        public DateTime bookingPeriodStart { get; set; }
        public DateTime bookingPeriodEnd { get; set; }
        public string penaltyRate { get; set; }
        public bool isAutoWaive { get; set; }
        public int autoWaivePeriod { get; set; }
        public string penaltyDocUrl { get; set; }
        public string penaltyDocName { get; set; }
    }
}
