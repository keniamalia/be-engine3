﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.MasterPlan.Project.Setting_Penalties.Dto
{
    public class GetDataMSClusterPenaltyByIDListDto
    {
        public int entityID { get; set; }
        public int clusterPenaltyID { get; set; } //0 = create
        public int projectID { get; set; }
        public string project { get; set; }
        public int clusterID { get; set; }
        public string cluster { get; set; }
        public DateTime bookingPeriodStart { get; set; }
        public DateTime bookingPeriodEnd { get; set; }
        public string penaltyBaseRate { get; set; }
        public string penaltyFreq { get; set; }
        public double penaltyRate { get; set; }
        public bool isAutoWaive { get; set; }
        public int autoWaivePeriod { get; set; }
        public string templateName { get; set; }
        public string templateStatus { get; set; }
        public string penaltyDocUrl { get; set; }
        public string penaltyDocName { get; set; }
    }
}
