﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.MasterPlan.Project.MS_Banks.Dto
{
    public class GetBankListDto
    {
        public int Id { get; set; }
        public string bankName { get; set; }
        public string bankCode { get; set; }
    }
}
