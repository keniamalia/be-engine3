﻿using System;
namespace VDI.Demo.MasterPlan.Unit.MS_Facades.Dto
{
    public class GetMsFacadeListDto
    {
        public int Id { get; set; }
        public string facadeCode { get; set; }
        public string facadeName { get; set; }
        public int projectID { get; set; }
        public int clusterID { get; set; }
        public int detailID { get; set; }
        public string detailCode { get; set; }
        public string detailName { get; set; }
    }
}
