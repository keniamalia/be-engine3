﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.MasterPlan.Unit.MS_Facades.Dto
{
    public class GetUnitDetailByProjectClusterListDto
    {
        public int detailID { get; set; }
        public string detailName { get; set; }
        public string detailCode { get; set; }
    }
}
