﻿using System;
namespace VDI.Demo.MasterPlan.Unit.MS_Facades.Dto
{
    public class UpdateMsFacadeInput
    {
        public int Id { get; set; }
        public string facadeCode { get; set; }
        public string facadeName { get; set; }
        public int projectID { get; set; }
        public int clusterID { get; set; }
        public int detailID { get; set; }
    }
}
