﻿using System;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using VDI.Demo.MasterPlan.Unit.MS_Facades.Dto;

namespace VDI.Demo.MasterPlan.Unit.MS_Facades
{
    public interface IMsFacadeAppService : IApplicationService
    {
        ListResultDto<GetUnitDetailByProjectClusterListDto> GetUnitDetailByProjectCluster(int projectID, int clusterID);
        ListResultDto<GetMsFacadeListDto> GetAllMsFacadeByProject(int projectID);
        void CreateMsFacade(CreateMsFacadeInput input);
        void DeleteMsFacade(int Id);
    }
}
