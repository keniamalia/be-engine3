﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.MasterPlan.Unit.MS_Units.Dto
{
    public class GetAreaMsUnitItemByUnitCodeUnitNoProjectClusterInputDto
    {
        public string unitCode { get; set; }
        public string unitNo { get; set; }
        public int projectID { get; set; }
        public int clusterID { get; set; }
    }
}
