﻿using System;
namespace VDI.Demo.MasterPlan.Unit.MS_Units.Dto
{
    public class GetUnitByProjectClusterCategoryInput
    {
        public int projectID { get; set; }
        public int clusterID { get; set; }
        public int categoryID { get; set; }
    }
}
