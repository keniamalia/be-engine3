﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.MasterPlan.Unit.MS_Clusters.Dto
{
    public class GetAvailableMsProjectDropdownForClusterDto
    {
        public int projectID { get; set; }
        public string projectName { get; set; }
        public string projectCode { get; set; }
    }
}
