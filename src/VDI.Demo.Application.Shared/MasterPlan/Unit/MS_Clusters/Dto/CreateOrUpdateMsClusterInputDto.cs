﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.MasterPlan.Unit.MS_Clusters.Dto
{
    public class CreateOrUpdateMsClusterInputDto
    {
        public int? Id { get; set; }
        public int projectID { get; set; }
        public string projectName { get; set; }
        public string clusterCode { get; set; }
        public string clusterName { get; set; }
        public string website { get; set; }
        public string termConditionFile { get; set; }
        public string termConditionFileStatus { get; set; }

        public List<CreateOrUpdateHandOverPeriodeDto> CreateOrUpdateHandOverPeriodeDto { get; set; }
    }

    public class CreateOrUpdateHandOverPeriodeDto
    {
        public DateTime handOverStart { get; set; }
        public DateTime handOverEnd { get; set; }
        public string handOverDue { get; set; }
        public string graceDue { get; set; }
    }
}
