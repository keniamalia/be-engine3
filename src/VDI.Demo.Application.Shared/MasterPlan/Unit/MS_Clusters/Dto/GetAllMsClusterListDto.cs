﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.MasterPlan.Unit.MS_Clusters.Dto
{
    public class GetAllMsClusterListDto
    {
        public int Id { get; set; }
        public string clusterCode { get; set; }
        public string clusterName { get; set; }
    }
}
