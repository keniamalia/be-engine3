﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Payment.InputInventoryPayment.Dto
{
    public class GetDropdownLkStatusListDto
    {
        public int Id { get; set; }
        public string statusCode { get; set; }
        public string statusName { get; set; }
    }
}
