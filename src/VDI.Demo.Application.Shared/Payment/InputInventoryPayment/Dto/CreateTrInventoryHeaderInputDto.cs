﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Payment.InputInventoryPayment.Dto
{
    public class CreateTrInventoryHeaderInputDto
    {
        public int entityID { get; set; }
        public int accID { get; set; }
        public int bookingHeaderID { get; set; }
        public string bookCode { get; set; }
        public string transNo { get; set; }
        public string ket { get; set; }
        public DateTime TTBGDate { get; set; }
    }
}
