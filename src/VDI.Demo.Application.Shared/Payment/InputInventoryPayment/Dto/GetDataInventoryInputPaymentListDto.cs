﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Payment.InputInventoryPayment.Dto
{
    public class GetDataInventoryInputPaymentListDto
    {
        public int inventoryHeaderID { get; set; }
        public string transNo { get; set; }
        public DateTime tanggalTransNo { get; set; }
        public List<GetInventory> dataInventory { get; set; }
    }

    public class GetInventory
    {
        public int payNo { get; set; }
        public int payTypeID { get; set; }
        public string payTypeCode { get; set; }
        public string payTypeName { get; set; }
        public int bankID { get; set; }
        public string bankCode { get; set; }
        public string bankName { get; set; }
        public string chequeNo { get; set; }
        public decimal amount { get; set; }
        public string keterangan { get; set; }
        public DateTime dueDate { get; set; }
    }
}
