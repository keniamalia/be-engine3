﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Payment.InputInventoryPayment.Dto
{
    public class UpdateTrInventoryDetailInputDto
    {
        public int inventoryHeaderID { get; set; }
        public int payNoInventory { get; set; }
        public DateTime? clearDate { get; set; }
        public string status { get; set; }
        public string transNo { get; set; }
    }
}
