﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Payment.InputInventoryPayment.Dto
{
    public class GetDataInventoryInputPaymentInputDto
    {
        public int accID { get; set; }
        public int bookingHeaderID { get; set; }
        public int payForID { get; set; }
    }
}
