﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Payment.InputInventoryPayment.Dto
{
    public class CreateUniversalInputInventoryDto
    {
        public CreateTrInventoryHeaderInputDto inventoryHeader { get; set; }
        public List<CreateTrInventoryDetailInputDto> dataInventoryDetail { get; set; }
    }
}
