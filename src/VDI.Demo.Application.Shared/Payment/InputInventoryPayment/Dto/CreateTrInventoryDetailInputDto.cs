﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Payment.InputInventoryPayment.Dto
{
    public class CreateTrInventoryDetailInputDto
    {
        public int entityID { get; set; }
        public int inventoryHeaderID { get; set; }
        public int payNo { get; set; }
        public int payForID { get; set; }
        public int payTypeID { get; set; }
        public int bankID { get; set; }
        public int statusID { get; set; }
        public string chequeNo { get; set; }
        public string ket { get; set; }
        public DateTime dueDate { get; set; }
        public DateTime? clearDate { get; set; }
        public string transNoPayment { get; set; }
        public decimal amount { get; set; }

        //for get data
        public string bankName { get; set; }
        public string statusCode { get; set; }
        public string statusDesc { get; set; }
        public string payTypeCode { get; set; }
        public string payTypeDesc { get; set; }
    }
}
