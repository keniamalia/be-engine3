﻿using Abp.Application.Services;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.Payment.InputInventoryPayment.Dto;
using VDI.Demo.Payment.InputPayment.Dto;
using VDI.Demo.PSAS.Schedule.Dto;

namespace VDI.Demo.Payment.InputInventoryPayment
{
    public interface IInputInventoryPaymentAppService : IApplicationService
    {
        List<GetDropdownLkStatusListDto> GetDropdownLkStatus();

        void CreateUniversalInputInventory(CreateUniversalInputInventoryDto input);

        JObject GenerateTransNoTTBG(GenerateTransNoInputDto input);

        List<GetDataTransNoListDto> GetDataTransNoInventory(string filter, int accountID);

        CreateUniversalInputInventoryDto GetDataInventoryByTransNo(string transNoTTBG);

        List<GetDataInventoryInputPaymentListDto> GetDataInventoryInputPayment(GetDataInventoryInputPaymentInputDto input);

        void UpdateTrInventoryDetail(UpdateTrInventoryDetailInputDto input);
    }
}
