﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Payment.ChangeUnknownPayment.Dto
{
    public class CreateTrPaymentChangeUnknownInputDto
    {
        public int entityID { get; set; }
        public int paymentHeaderID { get; set; }
        public string changeNo { get; set; }
        public DateTime changeDate { get; set; }
        public string reason { get; set; }
    }
}
