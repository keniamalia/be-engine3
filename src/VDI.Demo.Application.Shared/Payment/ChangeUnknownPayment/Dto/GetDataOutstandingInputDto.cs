﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Payment.ChangeUnknownPayment.Dto
{
    public class GetDataOutstandingInputDto
    {
        public int bookingHeaderID { get; set; }
        public int payForID { get; set; }
        public List<int> itemID { get; set; }
    }
}
