﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Payment.ChangeUnknownPayment.Dto
{
    public class UpdateBookingScheduleInputDto
    {
        public int bookingDetailScheduleID { get; set; }
        public decimal netOut { get; set; }
        public decimal vatOut { get; set; }
    }
}
