﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Payment.ChangeUnknownPayment.Dto
{
    public class ChangeUnknownInputDto
    {
        public List<decimal> paymentAmount{ get; set; }
        public string bookCode { get; set; }
    }
}
