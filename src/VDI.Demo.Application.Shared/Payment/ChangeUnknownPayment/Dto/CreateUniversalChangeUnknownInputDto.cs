﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Payment.ChangeUnknownPayment.Dto
{
    public class CreateUniversalChangeUnknownInputDto
    {
        //paymentHeader
        public int accountID { get; set; }
        public int? bookingHeaderID { get; set; }
        public int? payForID { get; set; }
        public DateTime? clearDate { get; set; }
        public string description { get; set; }
        public DateTime paymentDate { get; set; }
        public string controlNo { get; set; }
        public string PPNo { get; set; }
        public double pctTax { get; set; }
        public string unitCode { get; set; }
        public string unitNo { get; set; }
        public string address { get; set; }
        public string name { get; set; }
        public string psCode { get; set; }
        public string NPWP { get; set; }
        public List<int> itemID { get; set; }
        public decimal amountTotal { get; set; }
        public decimal maxAmount { get; set; }
        public DateTime changeDate { get; set; }
        public string reason { get; set; }

        //paymentDetail
        public List<PaymentDetail> dataPaymentDetail { get; set; }

    }

    public class PaymentDetail
    {
        public string chequeNo { get; set; }
        public int payNo { get; set; }
        public string othersTypeCode { get; set; }
        public DateTime dueDate { get; set; }
        public int payTypeID { get; set; }
        public string bankName { get; set; }
        public string ket { get; set; }
        public decimal totalAmountDetail { get; set; }
    }
}
