﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.Payment.ChangeUnknownPayment.Dto;

namespace VDI.Demo.Payment.ChangeUnknownPayment
{
    public interface IChangeUnknownPaymentAppService : IApplicationService
    {
        void CreateUniversalChangeUnknown(CreateUniversalChangeUnknownInputDto input);
        decimal GetDataOutstandingSchedule(GetDataOutstandingInputDto input);
        void CreateTrPaymentChangeUnknown(CreateTrPaymentChangeUnknownInputDto input);
    }
}
