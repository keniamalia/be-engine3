﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Payment.PaymentLK_OthersType.Dto
{
    public class DropdownLkOthersTypeDto
    {
        public int Id { get; set; }
        public string othersType { get; set; }
        public string othersTypeCode { get; set; }
        public string othersTypeName { get; set; }
    }
}
