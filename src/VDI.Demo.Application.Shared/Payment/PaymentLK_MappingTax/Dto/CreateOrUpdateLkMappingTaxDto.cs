﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Payment.PaymentLK_MappingTax.Dto
{
    public class CreateOrUpdateLkMappingTaxDto
    {
        public int? Id { get; set; }
        public int payForID { get; set; }
        public int payTypeID { get; set; }
        public int othersTypeID { get; set; }
        public string payForName { get; set; }
        public string payTypeName { get; set; }
        public string othersTypeName { get; set; }
        public string payForCode { get; set; }
        public string payTypeCode { get; set; }
        public string othersTypeCode { get; set; }
        public bool isVAT { get; set; }
        public bool isActive { get; set; }
    }
}
