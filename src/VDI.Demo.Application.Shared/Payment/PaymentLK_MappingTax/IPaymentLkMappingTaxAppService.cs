﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.Payment.PaymentLK_MappingTax.Dto;

namespace VDI.Demo.Payment.PaymentLK_MappingTax
{
    public interface IPaymentLkMappingTaxAppService : IApplicationService
    {
        List<CreateOrUpdateLkMappingTaxDto> GetAllLkMappingTax();
        void UpdateLkMappingTax(CreateOrUpdateLkMappingTaxDto input);

        void CreateLkMappingTax(CreateOrUpdateLkMappingTaxDto input);
    }
}
