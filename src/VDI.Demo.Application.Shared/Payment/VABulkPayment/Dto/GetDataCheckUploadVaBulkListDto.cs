﻿using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.Payment.BulkPayment.Dto;

namespace VDI.Demo.Payment.VABulkPayment.Dto
{
    public class GetDataCheckUploadVaBulkListDto
    {
            public string virtualAccount { get; set; }
            public int bookingHeaderID { get; set; }
            public string bookCode { get; set; }
            public string psCode { get; set; }
            public string name { get; set; }
            public string NPWP { get; set; }
            public string address { get; set; }
            public string unitCode { get; set; }
            public string unitNo { get; set; }
            public int unitID { get; set; }
            public string payForCode { get; set; }
            public string payTypeCode { get; set; }
            public string othersTypeCode { get; set; }
            public int payForID { get; set; }
            public int payTypeID { get; set; }
            public int othersTypeID { get; set; }
            public double pctTax { get; set; }
            public string message { get; set; }
            public List<GetDataSchedule> dataSchedule { get; set; }
    }
}
