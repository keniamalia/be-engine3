﻿using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.Payment.BulkPayment.Dto;

namespace VDI.Demo.Payment.VABulkPayment.Dto
{
    public class CreateUniversalVaBulkPaymentInputDto
    {
        public int accID { get; set; }
        public int? bookingHeaderID { get; set; }
        public int payForID { get; set; }
        public int unitID { get; set; }
        public string virtualAccount { get; set; }
        public string nama { get; set; }
        public decimal nilaiTransaksi { get; set; }
        public string jenisTransaksi { get; set; }
        public DateTime tanggalTransaksi { get; set; }
        public string keterangan { get; set; }
        public string jamTransaksi { get; set; }
        public string noRef { get; set; }
        public string NPWP { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string psCode { get; set; }
        public string unitCode { get; set; }
        public string unitNo { get; set; }
        public DateTime tanggalMutasi { get; set; }
        public double pctTax { get; set; }
        public List<DataAlloc> dataScheduleList { get; set; }
        public List<DataPayment> dataForPayment { get; set; }
    }
}
