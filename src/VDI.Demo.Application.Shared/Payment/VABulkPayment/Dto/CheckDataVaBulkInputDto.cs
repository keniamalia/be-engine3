﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Payment.VABulkPayment.Dto
{
    public class CheckDataVaBulkInputDto
    {
        public int accID { get; set; }
        public int userID { get; set; }
        public string vaNumber { get; set; }
        public string TglTransaksi { get; set; }
        public string refNo { get; set; }
    }
}
