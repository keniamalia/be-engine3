﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.Payment.VABulkPayment.Dto;

namespace VDI.Demo.Payment.VABulkPayment
{
    public interface IVaBulkPaymentAppService : IApplicationService
    {
        List<GetDataCheckUploadVaBulkListDto> CheckDataUploadVaBulk(List<CheckDataVaBulkInputDto> input);
        void CreateUniversalBulkPayment(List<CreateUniversalVaBulkPaymentInputDto> input);
    }
}
