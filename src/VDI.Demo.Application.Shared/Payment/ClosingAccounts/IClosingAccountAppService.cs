﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.Payment.ClosingAccounts.Dto;

namespace VDI.Demo.Payment.ClosingAccounts
{
    public interface IClosingAccountAppService : IApplicationService
    {
        ListResultDto<GetAllMsAccountClosingListDto> GetAllMsAccountClosing(DateTime cutOffDate);
        void ClosingAccountUniversal(ClosingAccountInputDto input);
        void SendEmailClosingAccount(List<GetDataClosingEmailListDto> input);
    }
}
