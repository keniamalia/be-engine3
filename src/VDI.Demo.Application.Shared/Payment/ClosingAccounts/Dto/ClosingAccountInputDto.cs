﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Payment.ClosingAccounts.Dto
{
    public class ClosingAccountInputDto
    {
        public int userID { get; set; }
        public DateTime transactionCutOffDate { get; set; }
        public bool isCheckAll { get; set; }
        public List<AccountDto> accountData { get; set; }
    }

    public class AccountDto
    {
        public int accountID { get; set; }
        public string accountCode { get; set; }
    }
}
