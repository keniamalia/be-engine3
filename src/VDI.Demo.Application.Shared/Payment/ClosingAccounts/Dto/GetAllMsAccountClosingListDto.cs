﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Payment.ClosingAccounts.Dto
{
    public class GetAllMsAccountClosingListDto
    {
        public int accountID { get; set; }
        public string accountCode { get; set; }
        public string companyCode { get; set; }
        public string accountName { get; set; }
        public DateTime closeDate { get; set; }
        public string closeBy { get; set; }
        public string userName { get; set; }
        public bool? isSystemClosing { get; set; }
    }
}
