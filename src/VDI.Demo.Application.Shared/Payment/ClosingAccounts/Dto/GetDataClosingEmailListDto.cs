﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Payment.ClosingAccounts.Dto
{
    public class GetDataClosingEmailListDto
    {
        public string accountingDate { get; set; }
        public string groupID { get; set; }
        public string accCode { get; set; }
    }
}
