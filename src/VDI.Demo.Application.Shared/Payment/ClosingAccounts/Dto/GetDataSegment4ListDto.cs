﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Payment.ClosingAccounts.Dto
{
    public class GetDataSegment4ListDto
    {
        public string division_ID { get; set; }
        public string interCompany { get; set; }
        public string coaCodeFin { get; set; }
    }
}
