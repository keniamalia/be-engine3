﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Payment.ClosingAccounts.Dto
{
    public class GetDataSegment4InputDto
    {
        public string coaCodeFin { get; set; }
        public int flag { get; set; } // 1 = BNK, SBS ; 2 = CD ; 3 = DEP, DPR, DPS, ESW ; 4 = else
        public string natureAccount { get; set; }
    }
}
