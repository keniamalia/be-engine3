﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Payment.OfficialReceiptPayment.Dto
{
    public class GetDataPaymentSearchingInputDto
    {
        public int projectID { get; set; }
        public int clusterID { get; set; }
        public int unitID { get; set; }
        public string psCode { get; set; }
        public DateTime? paymentStart { get; set; }
        public DateTime? paymentEnd { get; set; }
    }
}
