﻿using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.Files.Dto;

namespace VDI.Demo.Payment.OfficialReceiptPayment.Dto
{
    public class GetDataPaymentListDto
    {
        public int projectID { get; set; }
        public int accountID { get; set; }
        public string accountNo { get; set; }
        public string companyAddress { get; set; }
        public string email { get; set; }
        public string transNo { get; set; }
        public string unitCode { get; set; }
        public string unitNo { get; set; }
        public string project { get; set; }
        public string cluster { get; set; }
        public string custName { get; set; }
        public DateTime paymentDate { get; set; }
        public string payFor { get; set; }
        public List<LinkPathListDto> document { get; set; }
    }
}
