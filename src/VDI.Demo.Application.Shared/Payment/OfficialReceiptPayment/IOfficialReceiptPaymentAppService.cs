﻿using Abp.Application.Services;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.Payment.OfficialReceiptPayment.Dto;

namespace VDI.Demo.Payment.OfficialReceiptPayment
{
    public interface IOfficialReceiptPaymentAppService : IApplicationService
    {
        List<GetDataPaymentListDto> GetDataPaymentSearchingOfficialReceipt(GetDataPaymentSearchingInputDto input);
        JObject OfficialReceiptPrint(List<string> input);
    }
}
