﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Payment.BulkPayment.Dto
{
    public class GetDataJournalListDto
    {
        public int accID { get; set; }
        public string accCode { get; set; }
        public string journalCode { get; set; }
    }
}
