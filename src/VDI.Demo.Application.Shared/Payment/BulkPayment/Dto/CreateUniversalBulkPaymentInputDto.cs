﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Payment.BulkPayment.Dto
{
    public class CreateUniversalBulkPaymentInputDto
    {
        public int accID { get; set; }
        public int? bookingHeaderID { get; set; }
        public int payForID { get; set; }
        public string bookCode { get; set; }
        public string psCode { get; set; }
        public string name { get; set; }
        public string bankName { get; set; }
        public string keterangan { get; set; }
        public int unitID { get; set; }
        public string unitCode { get; set; }
        public string unitNo { get; set; }
        public string address { get; set; }
        public string NPWP { get; set; }
        public string journalCode { get; set; }
        public int runningJournal { get; set; }
        public DateTime? paymentDate { get; set; }
        public DateTime clearDate { get; set; }
        public double pctTax { get; set; }
        public int payTypeID { get; set; }
        public int othersTypeID { get; set; }
        public string othersTypeCode { get; set; }
        public decimal amount { get; set; }//total bayar
        public int flag { get; set; } // 1 = Bulk, VA Bulk; 2 = auto debet(untuk keterangan), 3 = create payment penalty
        public List<DataAlloc> dataScheduleList { get; set; }
        public List<DataPayment> dataForPayment { get; set; }
    }

    public class DataPayment
    {
        public decimal amount { get; set; }//bayar berapa
        public int payTypeID { get; set; }
        public int othersTypeID { get; set; }
        public string othersTypeCode { get; set; }
        public List<DataAlloc> dataAllocList { get; set; }
    }

    public class DataAlloc
    {
        public int allocID { get; set; }
        public short schedNo{ get; set; }
        public decimal amount { get; set; }//amount setelah dikurangi
        public decimal amountPerSchedNo { get; set; }//jumlah amount yang harus dibayar
    }
}
