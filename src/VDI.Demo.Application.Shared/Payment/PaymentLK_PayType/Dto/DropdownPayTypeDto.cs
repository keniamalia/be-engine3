﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Payment.PaymentLK_PayType.Dto
{
    public class DropdownPayTypeDto
    {
        public int? Id { get; set; }
        public string payType { get; set; }
        public string payTypeCode { get; set; }
        public string payTypeName { get; set; }
    }
}
