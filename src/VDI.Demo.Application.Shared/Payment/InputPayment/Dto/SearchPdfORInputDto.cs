﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Payment.InputPayment.Dto
{
    public class SearchPdfORInputDto
    {
        public string transNo { get; set; }
        public string unitCode { get; set; }
        public string unitNo { get; set; }
        public string fileName { get; set; }
        public int flag { get; set; } //1 = search pdf single payment, 2 = search pdf official receipt
    }
}
