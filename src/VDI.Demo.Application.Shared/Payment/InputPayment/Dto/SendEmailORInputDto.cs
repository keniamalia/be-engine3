﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Payment.InputPayment.Dto
{
    public class SendEmailORInputDto
    {
        public int projectID { get; set; }
        public int accID { get; set; }
        public string custName { get; set; }
        public string email { get; set; }
        public string unitCode { get; set; }
        public string unitNo { get; set; }
        public string transNo { get; set; }
        public DateTime paymentDate { get; set; }
        public string fileName { get; set; }
        public int flag { get; set; } //1 = search pdf single payment, 2 = search pdf official receipt
    }
}
