﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Payment.InputPayment.Dto
{
    public class GetDataPriorityPassNoListDto
    {
        public string priorityPassNo { get; set; }
        public string psCode { get; set; }
        public string clientName { get; set; }
        public decimal? amount { get; set; }
    }
}
