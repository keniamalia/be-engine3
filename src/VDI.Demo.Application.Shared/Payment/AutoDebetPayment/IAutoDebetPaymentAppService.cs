﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.Payment.AutoDebetPayment.Dto;

namespace VDI.Demo.Payment.AutoDebetPayment
{
    public interface IAutoDebetPaymentAppService : IApplicationService
    {
        List<GetDataCheckUploadAutoDebetListDto> GetDataCheckUploadAutoDebet(List<GetDataCheckUploadAutoDebetInputDto> input);
        void CreateUniversalAutoDebet(List<CreateUniversalAutoDebetInputDto> input);
    }
}
