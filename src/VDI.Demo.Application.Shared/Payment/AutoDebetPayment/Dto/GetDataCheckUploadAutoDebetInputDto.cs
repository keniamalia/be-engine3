﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Payment.AutoDebetPayment.Dto
{
    public class GetDataCheckUploadAutoDebetInputDto
    {
        public int entityID { get; set; }
        public string accNo { get; set; }
        public string bookCode { get; set; }
        public string uniqueNo { get; set; }
    }
}
