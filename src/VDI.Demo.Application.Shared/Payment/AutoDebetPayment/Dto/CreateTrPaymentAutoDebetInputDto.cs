﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Payment.AutoDebetPayment.Dto
{
    public class CreateTrPaymentAutoDebetInputDto
    {
        public string ADBKey { get; set; }
        public string accountNo { get; set; }
        public int? bookingHeaderID { get; set; }
        public string custName { get; set; }
        public string curr { get; set; }
        public DateTime transDate { get; set; }
        public decimal amount { get; set; }
    }
}
