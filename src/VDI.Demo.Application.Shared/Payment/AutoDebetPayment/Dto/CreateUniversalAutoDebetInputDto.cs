﻿using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.Payment.BulkPayment.Dto;

namespace VDI.Demo.Payment.AutoDebetPayment.Dto
{
    public class CreateUniversalAutoDebetInputDto
    {
        public int accID { get; set; }
        public int? bookingHeaderID { get; set; }
        public int payForID { get; set; }
        public int unitID { get; set; }
        public string ADBKey { get; set; }
        public string custName { get; set; }
        public decimal amount { get; set; }
        public string curr { get; set; }
        public DateTime transDate { get; set; }
        public string accountNo { get; set; }
        public string NPWP { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string psCode { get; set; }
        public string unitCode { get; set; }
        public string unitNo { get; set; }
        public string bankName { get; set; }
        public double pctTax { get; set; }
        public List<DataAlloc> dataScheduleList { get; set; }
        public List<DataPayment> dataForPayment { get; set; }
    }
}
