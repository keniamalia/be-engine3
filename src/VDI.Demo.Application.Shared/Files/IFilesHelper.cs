﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.Files.Dto;

namespace VDI.Demo.Files
{
    public interface IFilesHelper: IApplicationService
    {
        string MoveFiles(string filename, string oldPath, string newPath);
        string MoveFilesLegalDoc(string filename, string oldPath, string newPath, int? orderNumber);
        string CopyKPFile(string filename, string oldPath, string newPath, int? orderNumber);
        string ConvertIdToCode(int? Id);
        string ConvertDocIdToDocCode(int Id);
        List<LinkPathListDto> GetBase64FileByPhysicalPath(string physicalPath);
        List<LinkPathListDto> GetBase64FileByPhysicalPathFilter(string physicalPath, string fileName);
    }
}
