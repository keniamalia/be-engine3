﻿using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.OnlineBooking.Transaction.Dto;

namespace VDI.Demo.OnlineBooking.PaymentMidtrans.Dto
{
    public class KonfirmasiPesananDto
    {
        public string orderCode { get; set; }
        public string BookCode { get; set; }
        public string imageProject { get; set; }
        public string kodePelanggan { get; set; }
        public string tanggalBooking { get; set; }
        public string psName { get; set; }
        public string noIdentitas { get; set; }
        public string birthDate { get; set; }
        public string noNPWP { get; set; }
        public string noHpPembeli { get; set; }
        public string email { get; set; }
        public List<ListunitDto> listUnit { get; set; }
        public string hargaJual { get; set; }
        public string namaBank { get; set; }
        public string caraPembayaran { get; set; }
        public string noRekening { get; set; }
        public string tujuanTransaksi { get; set; }
        public string sumberDanaPembelian { get; set; }
        public string lblHandOver { get; set; }
        public string lblGracePeriod { get; set; }
        public string rekComName { get; set; }
        public List<listBankDto> listBank { get; set; }
        public string namaBankVA { get; set; }
        public string noAccVA { get; set; }
        public string bfAmount { get; set; }
        public decimal BF { get; set; }
        public decimal SP { get; set; }
        public string noDealCloser { get; set; }
        public string noHp { get; set; }
        public string namaDealCloser { get; set; }
        public List<listIlustrasiPembayaran> ilustrasiPembayaran { get; set; }
        public List<DPNoDtos> listDPNo { get; set; }
        public List<listCicilans> listCicilan { get; set; }
        public List<ListPenulasanDto> listPelunasan2 { get; set; }
        public List<ListPenulasanDto> listPelunasan1 { get; set; }
        public string imageLippo { get; set; }
        public int unitID { get; set; }
        public int renovID { get; set; }
        public int termID { get; set; }
        public int bookingHeaderID { get; set; }
        public int termBookingHeaderID { get; set; }
    }
    public class listBankDto
    {
        public string bankName { get; set; }
        public string noVA { get; set; }
    }
    public class ListunitDto
    {
        public string UnitCode { get; set; }
        public string UnitNo { get; set; }
        public string luas { get; set; }
        public string tipe { get; set; }
        public string renovation { get; set; }
        public string cluster { get; set; }
        public string category { get; set; }
        public int item { get; set; }
    }
    public class listIlustrasiPembayaran
    {
        public int termID { get; set; }
        public string termName { get; set; }
        public decimal bookingFee { get; set; }
        public decimal sellingPrice { get; set; }
        public string totalBayar { get; set; }
        public DateTime tglJatuhTempo { get; set; }
        public decimal totalDP { get; set; }
        public decimal leftSellingPrice { get; set; }
        public DateTime tglJatuhTempoPelunasan { get; set; }
        public decimal pelunasan { get; set; }
        public List<listCicilanDto> listCicilan { get; set; }
        public int orderHeaderID { get; set; }
    }
    
    public class listCicilanDto
    {
        public int cicilanNo { get; set; }
        public decimal amount { get; set; }
    }
    public class listCicilans
    {
        public int cicilanNo { get; set; }
        public DateTime tglJatuhTempo { get; set; }
        public List<listCicilanAmount> listCicilanAmount { get; set; }
    }
    public class listAllAmount
    {
        public decimal amount { get; set; }
        public int termID { get; set; }
        public DateTime tglJatuhTempo { get; set; }
    }
    public class listFinTimesDto
    {
        public int finStartDue { get; set; }
        public int termID { get; set; }
        public short finTimes { get; set; }
    }
   
    public class listCicilanAmount
    {
        public decimal amount { get; set; }
    }
    public class ListPenulasanDto
    {
        public int termID { get; set; }
        public short finStartDue { get; set; }
        public DateTime? tglJatuhTempo { get; set; }
        public decimal amount { get; set; }
    }
    public class SellingPriceDto
    {
        public int bookingHeaderID { get; set; }
        public double pctDisc { get; set; }
        public double pctTax { get; set; }
        public decimal price { get; set; }
    }
    public class listAmountDto
    {
        public decimal amount { get; set; }
        public int termID { get; set; }
        public DateTime tglJatuhTempo { get; set; }
    }
    public class DPNoDto
    {
        public int termID { get; set; }
        public List<DPNoDtos> listDPNos { get; set; }
        public int countDP { get; set; }
    }
    public class DPNoDtos
    {
        public short DPNo { get; set; }
        public DateTime tglJatuhTempo { get; set; }
        public int termID { get; set; }
        public double DPPct { get; set; }
        public List<listAmountDto> listAmount { get; set; }
    }
}
