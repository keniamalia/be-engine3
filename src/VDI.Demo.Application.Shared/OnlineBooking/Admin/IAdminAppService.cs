﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VDI.Demo.OnlineBooking.Admin.Dto;
using VDI.Demo.OnlineBooking.Transaction.Dto;

namespace VDI.Demo.OnlineBooking.Admin
{
    public interface IAdminAppService : IApplicationService
    {
        #region project information
        Task CreateOrUpdateProjectInfo(CreateOrUpdateProjectInfoDto input);
        ListResultDto<GetListAllProjectResultDto> GetListAllProject();
        GetProjectInformationResultDto GetProjectInformationDetail(int EntityId);
        #endregion

        #region Tower/Cluster
        GetDetailTowerResultDto GetTowerDetail(int projectId);
        GetTowerDetailEditResultDto GetTowerDetailEdit(int towerId);
        Task UpdateTowerDetailAsync(UpdateTowerDetailInputDto input);
        #endregion

        #region Key Feature
        ResultMessageDto CreateKeyFeature(CreateKeyFeatureInputDto input);
        ListResultDto<GetKeyFeaturesResultDto> GetKeyFeatures(int ProjectId);
        EditKeyFeatureResultDto EditKeyFeature(int keyFeatureId);
        Task UpdateKeyFeatures(UpdateKeyFeaturesInputDto input);
        #endregion

        #region SitePlan
        //Task CreateProjectSitePlan(CreateSitePlanInputDto input);
        GetDetailSitePlanResultDto GetDetailSitePlan(int projectId);
        JObject CreateOrUpdateSitePlan(CreateOrUpdateDetailSitePlanInputDto input);
        #endregion

        #region project location
        ResultMessageDto CreateProjectLocation(CreateProjectLocationInputDto input);
        GetDetailProjectLocationResultDto GetDetailProjectLocation(int id);
        JObject UpdateDetailProjectLocation(UpdateDetailProjectLocationInputDto input);
        #endregion

        #region project social media
        //ResultMessageDto CreateSocialMediaProject(CreateSocialMediaProjectDto input);
        ListResultDto<GetListSocialMediaProjectResultDto> GetListSocialMediaProject(int projectId);
        ResultMessageDto CreateOrUpdateSocialMediaProject(CreateOrUpdateSocialMediaProjectInputDto input);
        ResultMessageDto DeleteSocialMediaProject(int projectId, int sosmedId);
        #endregion

        #region sosial media
        //string InsertSocialMedia(AddSocialMediaInputDto input);
        ListResultDto<GetListSocialMediaResultDto> GetListSocialMedia();
        EditSocialMediaResultDto EditSocialMedia(int Id);
        JObject UpdateSocialMedia(UpdateSocialMediaInputDto input);
        ResultMessageDto DeleteSocialMedia(int Id);
        #endregion

        #region project gallery
        Task CreateGalery(CreateGalleryInputDto input);
        //ResultMessageDto AddProjectImageInput(AddProjectImageInputDto input);
        ListResultDto<GetListProjectGalleryResultDto> GetGalleryProjectList(int projectId);
        GetDetailProjectImageResultDto GetDetailProjectImage(int Id);
        Task UpdateProjectImage(UpdateProjectImageInputDto input);
        ResultMessageDto DeleteProjectImage(int Id);
        #endregion

        #region manage project
        string CreateManageProject(CreateManageProjectDto input);
        ListResultDto<GetManageProjectListResultDto> GetManageProjectList();
        GetDetailProjectResultDto GetDetailProject(int projectID);
        JObject UpdateManageProject(UpdateManageProjectInputDto input);
        #endregion

        #region manage main page
        JObject CreateMainPage(CreateMainPageDto input);
        //Task InsertImageMainPage(AddImageMainPageAdminInputDto input);
        ListResultDto<GetListMainPageImagesResultDto> GetListImageMainPage(int projectId);
        JObject UpdateMainPage(UpdateMainPageDto input);
        #endregion
    }
}
