﻿using System;
namespace VDI.Demo.OnlineBooking.Admin.Dto
{
    public class GetMainPageListResultDto
    {
        public string imageURL { get; set; }
        public string imageAlt { get; set; }
        public bool imageStatus { get; set; }
    }
}
