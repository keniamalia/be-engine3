﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.OnlineBooking.Admin.Dto
{
    public class CreateKeyFeatureInputDto
    {
        public int ProjectId { get; set; }
        public string keyFeatures { get; set; }
        public int status { get; set; }
    }
}
