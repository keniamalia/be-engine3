﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.OnlineBooking.Admin.Dto
{
    public class AddProjectInputDto
    {
        //public int projectID { get; set; }
        public string projectName { get; set; }
        public bool projectStatus { get; set; }
        public DateTime? activeFrom { get; set; }
        public DateTime? activeTo { get; set; }
    }
}
