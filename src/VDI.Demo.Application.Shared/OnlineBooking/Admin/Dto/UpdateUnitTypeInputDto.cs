﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.OnlineBooking.Admin.Dto
{
    public class UpdateUnitTypeInputDto
    {
        public int detailID { get; set; }

        public string detailImage { get; set; }

        public string imageStatus { get; set; }
    }
}
