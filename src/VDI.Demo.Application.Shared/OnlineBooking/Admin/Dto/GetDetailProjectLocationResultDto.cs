﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.OnlineBooking.Admin.Dto
{
    public class GetDetailProjectLocationResultDto
    {
        public int id { get; set; }
        public double latitude { get; set; }

        public double longitude { get; set; }

        public string projectAddress { get; set; }

        public string locationImageURL { get; set; }

        public string projectDesc { get; set; }
    }
}
