﻿using System;
namespace VDI.Demo.OnlineBooking.Admin.Dto
{
    public class UpdateMainPageDto
    {
        public int MainPageId { get; set; }
        public string ImageURL { get; set; }
        public string AltName { get; set;  }
        public int FileTypeId { get; set;  }
        public string imageStatus { get; set; }
    }
}
