﻿using System;
namespace VDI.Demo.OnlineBooking.Admin.Dto
{
    public class GetListSocialMediaResultDto
    {
        public int socialMediaID { get; set; }
        public string socialMediaName { get; set; }
        

        public string socialMediaIcon { get; set; }

        public bool? isActive { get; set; }
    }
}
