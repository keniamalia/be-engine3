﻿using System;
namespace VDI.Demo.OnlineBooking.Admin.Dto
{
    public class CreateOrUpdateSocialMediaProjectInputDto
    {
        public int projectID { get; set; }
        public int sosialMediaID { get; set; }
        public string socialMediaLink { get; set; }
        public int socialMediaLinkID { get; set; }
        public bool? isActive { get; set; }
        public DateTime creationTime { get; set; }
    }
}
