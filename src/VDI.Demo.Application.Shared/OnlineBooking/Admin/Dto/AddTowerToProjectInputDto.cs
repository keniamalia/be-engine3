﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.OnlineBooking.Admin.Dto
{
    class AddTowerToProjectInputDto
    {
        public int ProjectId { get; set; }

        public int clusterID { get; set; }

        public string cluesterName { get; set; }

        public bool? isActive { get; set; }

        public DateTime? activeFrom { get; set; }

        public DateTime? activeTo { get; set; }
    }
}
