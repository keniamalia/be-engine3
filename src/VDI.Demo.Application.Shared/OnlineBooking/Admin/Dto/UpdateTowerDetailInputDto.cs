﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.OnlineBooking.Admin.Dto
{
    public class UpdateTowerDetailInputDto
    {
        public int towerId { get; set; }

        public bool? isActive { get; set; }

        public string activeFrom { get; set; }

        public string activeTo { get; set; }

        public bool? isRequiredPP { get; set; }
    }
}
