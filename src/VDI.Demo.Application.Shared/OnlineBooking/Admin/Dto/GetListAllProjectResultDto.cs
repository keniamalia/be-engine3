﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.OnlineBooking.Admin.Dto
{
    public class GetListAllProjectResultDto
    {
        public int projectId { get; set; }

        public string projectCode { get; set; }

        public string projectName { get; set; }
    }
}
