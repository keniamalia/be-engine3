﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.OnlineBooking.Admin.Dto
{
    public class GetProjectInformationResultDto
    {
        public int projectId { get; set; }
        //desc
            //MS_ProjectInfo
        public string projectDesc { get; set; }

        //property detail
            //MS_ProjectInfo
        public string projectDeveloper { get; set; }

        public string projectWebsite { get; set; }

        //marketing office
            //MS_ProjectInfo
        public string projectMarketingOffice { get; set; }

        public string projectMarketingPhone { get; set; }
    }
}
