﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.OnlineBooking.Admin.Dto
{
    class UpdateProjectDescInputDto
    {
        public int projectID { get; set; }
        public string projectDesc { get; set; }
    }
}
