using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.OnlineBooking.Admin.Dto
{
    public class GetListProjectGalleryResultDto
    {
        public int ProjectInfoID { get; set; }
        public int GalleryId { get; set; }
        public string ImageURL { get; set; }
        public string ImageAlt { get; set; }
        public bool ImageStatus { get; set; }
        public int FileTypeId { get; set; }
    }
}
