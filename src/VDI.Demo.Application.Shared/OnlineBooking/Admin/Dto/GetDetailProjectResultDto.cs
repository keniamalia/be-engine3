﻿using System;
namespace VDI.Demo.OnlineBooking.Admin.Dto
{
    public class GetDetailProjectResultDto
    {
        public int ProjectID { get; set; }

        public int id { get; set; }

        public string projectCode { get; set; }

        public string ProjectName { get; set; }

        public bool? ProjectStatus { get; set; }

        public DateTime? ActiveFrom { get; set; }

        public DateTime? ActiveTo { get; set; }
    }
}
