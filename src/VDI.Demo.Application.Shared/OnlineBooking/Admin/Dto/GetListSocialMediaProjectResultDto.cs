﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.OnlineBooking.Admin.Dto
{
    public class GetListSocialMediaProjectResultDto
    {
        public int projectId { get; set; }
        public int socialMediaId { get; set; }
        public string socialMediaName { get; set; }
        public int socialMediaLinkID { get; set; }
        public string socialMediaLink { get; set; }
        public bool? isActive { get; set; }
    }
}
