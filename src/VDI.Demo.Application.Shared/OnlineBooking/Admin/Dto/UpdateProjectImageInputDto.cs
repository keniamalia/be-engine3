﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.OnlineBooking.Admin.Dto
{
    public class UpdateProjectImageInputDto
    {
        public int Id { get; set; }

        public string imageURL { get; set; }

        public string imageAlt { get; set; }

        public bool imageStatus { get; set; }

    }
}
