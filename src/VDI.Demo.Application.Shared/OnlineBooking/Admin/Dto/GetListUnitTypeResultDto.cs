﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.OnlineBooking.Admin.Dto
{
    public class GetListUnitTypeResultDto
    {
        public int projectID { get; set; }
        public int detailID { get; set; }
        public string clusterName { get; set; }
        public string detailImage { get; set; }
        public string detailCode { get; set; }
    }
}
