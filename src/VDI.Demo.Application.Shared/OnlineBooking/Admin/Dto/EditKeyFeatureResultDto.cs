﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.OnlineBooking.Admin.Dto
{
    public class EditKeyFeatureResultDto
    {
        public int id { get; set; }
        public string keyFeatures { get; set; }
        public int keyFeaturesCollectionID { get; set; }
        public int status { get; set; }
    }
}
