﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.OnlineBooking.Admin.Dto
{
    public class UpdateKeyFeaturesInputDto
    {
        public int Id { get; set; }
        public string keyFeatures { get; set; }
        public int status { get; set; }
    }
}
