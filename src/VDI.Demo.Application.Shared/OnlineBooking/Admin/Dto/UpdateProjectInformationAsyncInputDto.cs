﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.OnlineBooking.Admin.Dto
{
    public class UpdateProjectInformationAsyncInputDto
    {
        public int projectId { get; set; }

        public string projectDesc { get; set; }

        public string projectDeveloper { get; set; }

        public string projectWebsite { get; set; }

        public string projectMarketingOffice { get; set; }

        public string projectMarketingPhone { get; set; }
    }
}
