﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.OnlineBooking.Admin.Dto
{
    public class UpdateImageGalleryInputDto
    {
        public string imageUrl { get; set; }
        public string imageAlt { get; set; }
        public string imageStatus { get; set; }
        public int galleryID { get; set; }
    }
}
