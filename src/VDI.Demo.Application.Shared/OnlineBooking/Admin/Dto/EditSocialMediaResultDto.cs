﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.OnlineBooking.Admin.Dto
{
    public class EditSocialMediaResultDto
    {
        public int Id { get; set; }
        public string socialMediaName { get; set; }
        public string socialMediaIcon { get; set; }
        public bool? isActive { get; set; }
    }
}
