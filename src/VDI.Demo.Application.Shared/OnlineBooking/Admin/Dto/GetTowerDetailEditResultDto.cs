﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.OnlineBooking.Admin.Dto
{
    public class GetTowerDetailEditResultDto
    {
        public int towerId { get; set; }
        public string clusterCode { get; set; }
        public string clusterName { get; set; }
        public DateTime? activeFrom { get; set; }
        public DateTime? activeTo { get; set; }
        public bool? isRequiredRP { get; set; }
        public bool? isActive { get; set; }
    }
}
