﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.OnlineBooking.Admin.Dto
{
    public class GetDetailTowerResultDto
    {
        public int projectId { get; set; }

        public int towerId { get; set; }

        public string clusterCode { get; set; }

        public int clusterID { get; set; }

        public string clusterName { get; set; }

        public bool? isActive { get; set; }

        public DateTime? activeFrom { get; set; }

        public DateTime? activeTo { get; set; }

        public bool? isRequiredPP { get; set; }
    }
}
