﻿using System;
namespace VDI.Demo.OnlineBooking.Admin.Dto
{
    public class GetManageProjectListResultDto
    {
        public int projectID { get; set; }
        public int Id { get; set; }
        public string imgLogo { get; set; }
        public string projectName { get; set; }
        public bool? isActive { get; set; }
        public DateTime? activeFrom { get; set; }
        public DateTime? activeTo { get; set; }
    }
}
