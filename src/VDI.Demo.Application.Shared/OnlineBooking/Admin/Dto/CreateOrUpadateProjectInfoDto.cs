﻿using System;
namespace VDI.Demo.OnlineBooking.Admin.Dto
{
    public class CreateOrUpdateProjectInfoDto
    {
        public int projectID { get; set; }
        public string projectDesc { get; set; }
        //public int keyFeaturesCollectionID { get; set; }
        public string projectDeveloper { get; set; }
        public string projectWebsite { get; set; }
        public string projectMarketingOffice { get; set; }
        public string projectMarketingPhone { get; set; }
    }
}
