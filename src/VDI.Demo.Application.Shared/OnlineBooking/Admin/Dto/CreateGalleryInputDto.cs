﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.OnlineBooking.Admin.Dto
{
    public class CreateGalleryInputDto
    {
        public int ProjectId { get; set; }
        public string ImageURL { get; set; }
        public string ImageAlt { get; set; }
    }
}
