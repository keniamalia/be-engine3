using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.OnlineBooking.Admin.Dto
{
    public class GetListMainPageImagesResultDto
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public string ImageURL { get; set; }
        public string ImageAlt { get; set; }
        public bool ImageStatus { get; set; }
        public int FileTypeId { get; set; }
    }
}