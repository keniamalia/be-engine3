﻿using System;
namespace VDI.Demo.OnlineBooking.Admin.Dto
{
    public class AddSocialMediaInputDto
    {
        public string socialMediaName
        {
            get;
            set;
        }

        public string socialMediaIcon
        {
            get;
            set;
        }

        public bool isActive { get; set; }
    }
}
