﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.OnlineBooking.Admin.Dto
{
    public class GetDetailSitePlanResultDto
    {
        public int projectId { get; set; }

        public string sitePlansImageUrl { get; set; }

        public string sitePlansLegend { get; set; }
    }
}
