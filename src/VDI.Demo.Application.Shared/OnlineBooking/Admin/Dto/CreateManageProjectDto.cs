﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.OnlineBooking.Admin.Dto
{
    public class CreateManageProjectDto
    {
        public int projectID { get; set; }
        public string projectImageLogo { get; set; }
        public string projectName { get; set; }
        public bool projectStatus { get; set; }
        public string activeFrom { get; set; }
        public string activeTo { get; set; }
    }
}
