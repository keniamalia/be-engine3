﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.OnlineBooking.Admin.Dto
{
    public class UpdateSocialMediaInputDto
    {
        public int Id { get; set; }
        public string socialMediaName { get; set; }
        public string socialMediaIcon { get; set; }
        public string iconStatus { get; set; }
    }
}
