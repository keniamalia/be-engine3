﻿using System;
namespace VDI.Demo.OnlineBooking.Admin.Dto
{
    public class UpdateManageProjectInputDto
    {
        public int projectId { get; set; }

        public int manageId { get; set; }

        //public string imageLogo {get; set; }

        public bool projectStatus { get; set; }

        public string activeFrom { get; set; }

        public string activeTo { get; set; }
    }
}
