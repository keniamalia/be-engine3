﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.OnlineBooking.Admin.Dto
{
    public class GetKeyFeaturesResultDto
    {
        public int ProjectId { get; set; }
        public int Id { get; set; }
        public int keyFeaturesCollectionID { get; set; }
        public string keyFeatures { get; set; }
        public int status { get; set; }
    }
}
