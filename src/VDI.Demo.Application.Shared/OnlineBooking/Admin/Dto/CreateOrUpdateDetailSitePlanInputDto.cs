﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.OnlineBooking.Admin.Dto
{
    public class CreateOrUpdateDetailSitePlanInputDto
    {
        public int ProjectId { get; set; }

        public string sitePlansImageUrl { get; set; }

        public string sitePlansLegend { get; set; }

        public string imageStatus { get; set;  }
    }
}
