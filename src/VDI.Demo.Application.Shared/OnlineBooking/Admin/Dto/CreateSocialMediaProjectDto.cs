﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.OnlineBooking.Admin.Dto
{
    public class CreateSocialMediaProjectDto
    {
        public int projectID { get; set; }
        public int sosialMediaID { get; set; }
        public string socialMediaLink { get; set; }
        public bool? isActive { get; set; }
    }
}
