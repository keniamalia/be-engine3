﻿using System;
namespace VDI.Demo.OnlineBooking.Admin.Dto
{
    public class UpdateDetailProjectInformationResultDto
    {
        public int projectId { get; set; }
        //desc
        //MS_ProjectInfo
        public string projectDesc { get; set; }

        //property detail
        //MS_ProjectInfo
        public string projectDeveloper { get; set; }

        public string projectWebsite { get; set; }

        //marketing office
        //MS_ProjectInfo
        public string projectMarketingOffice { get; set; }

        public string projectMarketingPhone { get; set; }
    }
}
