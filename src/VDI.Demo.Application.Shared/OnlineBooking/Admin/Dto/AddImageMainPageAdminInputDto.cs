﻿using System;
using Abp.Web.Models;

namespace VDI.Demo.OnlineBooking.Admin.Dto
{
    public class AddImageMainPageAdminInputDto : ErrorInfo
    {
        public string ImageURL { get; set; }
        public string ImageAlt { get; set; }
        public int FileTypeId { get; set; }
    }
}
