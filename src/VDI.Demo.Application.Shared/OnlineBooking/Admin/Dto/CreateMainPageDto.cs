﻿using System;
namespace VDI.Demo.OnlineBooking.Admin.Dto
{
    public class CreateMainPageDto
    {
        public int ProjectId { get; set; }
        public string ImageURL { get; set; }
        public string ImageAlt { get; set; }
        public int FileTypeId { get; set; }
    }
}
