﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.OnlineBooking.Admin.Dto
{
    public class GetDetailProjectImageResultDto
    {
        public int Id { get; set; }

        public string imageURL { get; set; }

        public string imageAlt { get; set; }

        public bool imageStatus { get; set; }

        public int projectInfoID { get; set; }

        public int FileTypeId { get; set; }
    }
}
