﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.OnlineBooking.Admin.Dto;

namespace VDI.Demo.OnlineBooking.Admin
{
    public interface IAdminUnitTypeAppService : IApplicationService
    {
        ListResultDto<GetListUnitTypeResultDto> GetListUnitType(int projectId);

        void UpdateUnitType(UpdateUnitTypeInputDto input);
    }
}
