﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VDI.Demo.OnlineBooking.Admin.Dto;
using VDI.Demo.OnlineBooking.Transaction.Dto;

namespace VDI.Demo.OnlineBooking.Admin
{
    public interface IAdminGaleryAppService : IApplicationService
    {
        #region project gallery
        Task CreateGalery(AddProjectImageInputDto input);
        #endregion

    }
}
