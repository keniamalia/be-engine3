﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.OnlineBooking.BookingHistory.Dto
{
    public class ListBookingHistoryNotarisResultDto
    {
        public int orderHeaderID { get; set; }
        public string orderCode { get; set; }
        public string oldOrderCode { get; set; }
        public int payTypeID { get; set; }
        public string payType { get; set; }
        public string membercode { get; set; }
        public string membername { get; set; }
        public string pscode { get; set; }
        public string psname { get; set; }
        public string custemail { get; set; }
        public string custphone { get; set; }
        public string scmcode { get; set; }
        public int statusID { get; set; }
        public string status { get; set; }
        public DateTime orderDate { get; set; }

        public int? tujuanTransaksiID { get; set; }
        public int? sumberDanaID { get; set; }
        public string bankRekeningPemilik { get; set; }
        public string nomorRekeningPemilik { get; set; }
        public decimal totalAmount { get; set; }

        public List<UnitNotarisResultDto> arrUnit { get; set; }
        public string message { get; set; }
    }

    public class UnitNotarisResultDto
    {
        public string unitcode { get; set; }
        public string unitno { get; set; }
        public int unitID { get; set; }
        public int renovID { get; set; }
        public string renovcode { get; set; }
        public int termID { get; set; }
        public int termno { get; set; }
        public string termName { get; set; }
        public decimal sellingprice { get; set; }
        public decimal bfamount { get; set; }
        public string remarks { get; set; }
        public double? disc1 { get; set; }
        public double? disc2 { get; set; }
        public int projectID { get; set; }
        public string projectName { get; set; }
        public string unitName { get; set; }
        public string detail { get; set; }
        public string facingName { get; set; }
        public string PPNo { get; set; }
        public string pathKP { get; set; }
    }
}
