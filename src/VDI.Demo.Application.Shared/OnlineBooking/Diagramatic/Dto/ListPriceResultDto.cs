﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.OnlineBooking.Diagramatic.Dto
{
    public class ListPriceResultDto
    {
        public int termID { get; set; }
        public string termName { get; set; }

        public int unitID { get; set; }
        public decimal grossPrice { get; set; }
        public decimal bookingFee { get; set; }
    }
}
