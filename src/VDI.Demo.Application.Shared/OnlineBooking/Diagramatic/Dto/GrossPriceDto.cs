﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.OnlineBooking.Diagramatic.Dto
{
    public class GrossPriceDto
    {
        public decimal grossPrice { get; set; }
        public decimal bookingFee { get; set; }
    }
    public class listGrossPrice
    {
        public decimal grossPrice { get; set; }
    }
}
