﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VDI.Demo.OnlineBooking.PaymentMidtrans.Dto;
using VDI.Demo.OnlineBooking.Transaction.Dto;

namespace VDI.Demo.OnlineBooking.Transaction
{
    public interface ITransactionAppService : IApplicationService
    {
        InsertTrUnitReservedResultDto InsertTrUnitReserved(InsertTRUnitReservedInputDto input);
        
        TrUnitOrderHeaderResultDto InsertTrUnitOrderHeader(CreateTransactionUniversalDto input);
        
        void SchedulerStatusOrderExpired();

        void SchedulerTRUnitReserved();

        List<GetTrUnitReservedDto> GetTrUnitReserved(int userID);

        ResultMessageDto DeleteTrUnitReserved(int Id);

        ListDetailBookingUnitResultDto GetDetailBookingUnit(int userID, string psCode);

        ResultMessageDto InsertTransactionUniversal(CreateTransactionUniversalDto input);

        Task<PaymentOnlineBookingResponse> DoBookingMidransReq(CreateTransactionUniversalDto input);

        void DoBooking(PaymentOnlineBookingResponse input);

        GenerateKPResultDto GenerateKP(int orderID, int unitID);

        string SendEmailKP(int orderID, int unitID);

        void SchedulerBookingReminder();
    }
}
