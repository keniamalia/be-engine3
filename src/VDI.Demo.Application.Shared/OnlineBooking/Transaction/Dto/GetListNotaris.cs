﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.OnlineBooking.Transaction.Dto
{
    public class GetListNotaris
    {
        public int notarisId { get; set; }
        public string notarisName { get; set; }
    }
}
