﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.OnlineBooking.Transaction.Dto
{
    public class GenerateKPResultDto
    {
        public string unitCode { get; set; }
        public string unitNo { get; set; }
        public string pathKP { get; set; }
        public string psName { get; set; }
        public string bookDate { get; set; }
        public string memberName { get; set; }
        public string memberPhone { get; set; }
        public string psEmail { get; set; }

    }
}
