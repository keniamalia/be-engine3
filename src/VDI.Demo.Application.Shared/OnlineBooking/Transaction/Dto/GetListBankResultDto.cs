﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.OnlineBooking.Transaction.Dto
{
    public class GetListBankResultDto
    {
        public int bankID { get; set; }

        public string bankCode { get; set; }

        public string bankName { get; set; }
    }
}
