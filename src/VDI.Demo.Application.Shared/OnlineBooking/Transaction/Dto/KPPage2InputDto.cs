﻿using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.OnlineBooking.PaymentMidtrans.Dto;

namespace VDI.Demo.OnlineBooking.Transaction.Dto
{
    public class KPPage2InputDto
    {
        public List<listIlustrasiPembayaranDto> ilustrasiPembayaran { get; set; }
        public List<DPNoDtos> listDP { get; set; }
    }
    public class listIlustrasiPembayaranDto
    {
        public int orderHeaderID { get; set; }
        public int termID { get; set; }
        public string termName { get; set; }
        public decimal bookingFee { get; set; }
        public decimal sellingPrice { get; set; }
        public DateTime tglJatuhTempo { get; set; }
        public List<DPNoDtos> listDP { get; set; }
        public List<listCicilan> listCicilan { get; set; }
    }
    public class listCicilan
    {
        public int cicilanNo { get; set; }
        public DateTime tglJatuhTempo { get; set; }
        public string amount { get; set; }
    }
    
}
