﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.OnlineBooking.Transaction.Dto
{
    public class CreateLogMobeyInputDto
    {
        public string orderCode { get; set; }

        public string status { get; set; }

        public string messageError { get; set; }

        public string transactionTime { get; set; }

        public string transactionReference { get; set; }

        public bool isVoid { get; set; }

        public string voidTime { get; set; }
    }
}
