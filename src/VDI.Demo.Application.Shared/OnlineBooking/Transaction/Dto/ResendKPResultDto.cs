﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.OnlineBooking.Transaction.Dto
{
    public class ResendKPResultDto
    {
        public string unitCode { get; set; }
        public string orderCode { get; set; }
        public string unitNo { get; set; }
        public string psCode { get; set; }
    }
}
