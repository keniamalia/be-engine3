﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.OnlineBooking.Project.Dto
{
    public class InsertProjectLocationInputDto
    {
        public double latitude { get; set; }

        public double longitude { get; set; }
        
        public string projectAddress { get; set; }
        
        public string locationImageURL { get; set; }
        
        public int projectInfoID { get; set; }
    }
}
