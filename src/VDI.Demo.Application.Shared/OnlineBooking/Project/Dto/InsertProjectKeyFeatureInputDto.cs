﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.OnlineBooking.Project.Dto
{
    public class InsertProjectKeyFeatureInputDto
    {
        public string keyFeatures { get; set; }

        public int status { get; set; }
        
        public int keyFeaturesCollectionID { get; set; }
    }
}
