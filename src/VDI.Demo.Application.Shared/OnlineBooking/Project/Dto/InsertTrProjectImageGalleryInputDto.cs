﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.OnlineBooking.Project.Dto
{
    public class InsertTrProjectImageGalleryInputDto
    {
        public string imageURL { get; set; }
        
        public string imageAlt { get; set; }

        public bool imageStatus { get; set; }
        
        public int projectInfoID { get; set; }
    }
}
