﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.OnlineBooking.Project.Dto
{
    public class InsertProjectInfoUniversalInputDto
    {
        public int projectID { get; set; }

        public string projectDesc { get; set; }

        public string projectDeveloper { get; set; }

        public string projectMarketingOffice { get; set; }

        public string projectMarketingPhone { get; set; }

        public string sitePlansImageURL { get; set; }

        public string sitePlansLegend { get; set; }

        public bool projectStatus { get; set; }

        public string projectImageLogo { get; set; }

        public string projectWebsite { get; set; }

        public double latitude { get; set; }

        public double longitude { get; set; }

        public string projectAddress { get; set; }

        public string locationImageURL { get; set; }

        public List<KeyFeatureDto> listKeyFeature { get; set; }

        public List<ProjectGalleryDto> listGallery { get; set; }
    }

    public class KeyFeatureDto
    {
        public string keyFeatures { get; set; }

        public int status { get; set; }
    }

    public class ProjectGalleryDto
    {
        public string imageURL { get; set; }

        public string imageAlt { get; set; }

        public bool imageStatus { get; set; }
    }
}
