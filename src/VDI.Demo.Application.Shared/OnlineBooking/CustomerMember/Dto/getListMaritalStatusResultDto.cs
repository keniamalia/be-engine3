﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.OnlineBooking.CustomerMember.Dto
{
    public class getListMaritalStatusResultDto
    {
        public string marStatus { get; set; }

        public string marStatusName { get; set; }
    }
}
