﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.OnlineBooking.CustomerMember.Dto
{
    public class ListProvinceResultDto
    {
        public string provinceCode { get; set; }

        public string provinceName { get; set; }
    }
}
