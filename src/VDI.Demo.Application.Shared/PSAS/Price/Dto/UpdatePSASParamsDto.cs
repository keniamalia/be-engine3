﻿using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.PSAS.Dto;

namespace VDI.Demo.PSAS.Price.Dto
{
    public class UpdatePSASParamsDto
    {
        public GetPSASParamsDto paramsCheck { get; set; }

        public bool isEffectGross { get; set; }

        public decimal grossPriceTanah { get; set; }

        public decimal grossPriceBangunan { get; set; }

        public decimal grossPriceRenov { get; set; }
    }
}
