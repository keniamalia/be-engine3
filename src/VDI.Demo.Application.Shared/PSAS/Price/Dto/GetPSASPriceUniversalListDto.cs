﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PSAS.Price.Dto
{
    public class GetPSASPriceUniversalListDto
    {
        public int? bookingHeaderID { get; set; }

        public string projectCode { get; set; }

        public GetAreaLlistDto area { get; set; }

        public GetGrosspriceLlistDto grossPrice { get; set; }

        public GetDiscountLlistDto discount { get; set; }

        public GetNetpriceLlistDto netPrice { get; set; }

        public GetAddDiscLlistDto addDisc { get; set; }

        public List<GetDiscountAlistDto> discountA { get; set; }

        public GetNetNetPriceLlistDto netNetPrice { get; set; }

        public GetVATLlistDto VATPrice { get; set; }

        public GetInterestLlistDto interest { get; set; }

        public GetTotalLlistDto total { get; set; }

        public  List<double> pctAddDisc { get; set; }
    }
}
