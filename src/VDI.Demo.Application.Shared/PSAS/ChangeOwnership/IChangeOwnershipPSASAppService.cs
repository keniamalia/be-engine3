﻿using Abp.Application.Services;
using System.Collections.Generic;
using VDI.Demo.PSAS.ChangeOwnership.Dto;
using VDI.Demo.PSAS.Dto;

namespace VDI.Demo.PSAS.ChangeOwnership
{
    public interface IChangeOwnershipPSASAppService : IApplicationService
    {
        GetDataUnitChangeOwnerListDto GetDataUnitChangeOwnership(GetPSASParamsDto input);

        void ChangeOwnershipSave(ChangeOwnershipInputDto input);

        List<GetHistoryOwnerListDto> GetHistoryOwnership(GetPSASParamsDto input);

        List<GetSearchPersonalListDto> SearchPersonal(string filter);

        List<GetMsBankDropdownListDto> GetBankDropdown();

        void CreateTrBookingChangeOwner(ChangeOwnershipInputDto input);

        List<string> GetChangeOwnerType();


    }
}
