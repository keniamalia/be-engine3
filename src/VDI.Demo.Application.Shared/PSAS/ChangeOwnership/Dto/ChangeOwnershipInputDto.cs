﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PSAS.ChangeOwnership.Dto
{
    public class ChangeOwnershipInputDto
    {
        public int bookingHeaderID { get; set; }
        public string oldPsCode { get; set; }
        public string newPsCode { get; set; }
        public string addnNo { get; set; }
        public DateTime addnDate { get; set; }
        public string docNo { get; set; }
        public double costPct { get; set; }
        public decimal costAmt { get; set; }
        public string oldBankCode { get; set; }
        public string newBankCode { get; set; }

        public decimal NJOPTanah { get; set; }
        public decimal NJOPBangunan { get; set; }
        public decimal nilaiPengalihan { get; set; }
        public string NOP { get; set; }
        public string NTPN { get; set; }

        public DateTime tanggalPenyetoran { get; set; }
        public decimal jumlahSetoran { get; set; }

        public string changeOwnerType { get; set; }
        public string remarks { get; set; }

        public string oldFinType { get; set; }
        public string newFinType { get; set; }
        public int entityID { get; set; }
        public int seqNo { get; set; }

    }
}
