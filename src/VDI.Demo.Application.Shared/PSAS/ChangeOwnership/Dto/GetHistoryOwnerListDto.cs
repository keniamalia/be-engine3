﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PSAS.ChangeOwnership.Dto
{
    public class GetHistoryOwnerListDto
    {
        public int bookingHeaderID { get; set; }
        public string docNo { get; set; }
        public DateTime docDate { get; set; }
        public string addnNo { get; set; }
        public string finType { get; set; } //new fintype
        public string psName { get; set; }
        public string Remarks { get; set; }
    }
}
