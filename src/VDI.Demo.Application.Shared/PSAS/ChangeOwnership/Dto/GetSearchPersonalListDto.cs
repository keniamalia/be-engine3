﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PSAS.ChangeOwnership.Dto
{
    public class GetSearchPersonalListDto
    {
        public string psCode { get; set; }
        public string name { get; set; }
        public string idNo { get; set; }
    }
}
