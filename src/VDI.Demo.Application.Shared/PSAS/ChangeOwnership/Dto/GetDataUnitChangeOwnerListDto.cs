﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PSAS.ChangeOwnership.Dto
{
    public class GetDataUnitChangeOwnerListDto
    {
        public int bookingHeaderID { get; set; }
        public string bookCode { get; set; }
        public string unitNo { get; set; }
        public string unitCode { get; set; }
        public string psCode { get; set; }
        public string psName { get; set; }
        public string bankCode { get; set; }
        public string bankName { get; set; }
    }
}
