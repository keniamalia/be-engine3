﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PSAS.ChangeOwnership.Dto
{
    public class GetMsBankDropdownListDto
    {
        public int bankID { get; set; }
        public string bankCode { get; set; }
        public string bankName { get; set; }

    }
}
