﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PSAS.Penalties.Dto
{
    public class GetDataPenaltyViewListDto
    {
        public int projectID { get; set; }
        public string project { get; set; }
        public int clusterID { get; set; }
        public string cluster { get; set; }
        public int unitID { get; set; }
        public string unitCode { get; set; }
        public string unitNo { get; set; }
        public string psCode { get; set; }
        public string custName { get; set; }
        public List<GetPenaltyScheduleDto> dataPenaltySchedule { get; set; }
    }

    public class GetPenaltyScheduleDto
    {
        public int penaltyScheduleID { get; set; }
        public int schedNo { get; set; }
        public string allocCode { get; set; }
        public DateTime dueDate { get; set; }
        public decimal amountNo { get; set; }
        public int aging { get; set; }
        public decimal penAmount { get; set; }
        public DateTime penPaymentDate { get; set; }
        public string statusPenalty { get; set; }
    }
}
