﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PSAS.Penalties.Dto
{
    public class GetBookingDetailUpdateListDto
    {
        public int bookingDetailID { get; set; }
        public string itemCode { get; set; }
    }
}
