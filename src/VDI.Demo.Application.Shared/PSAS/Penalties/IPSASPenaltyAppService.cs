﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.PSAS.Dto;
using VDI.Demo.PSAS.Penalties.Dto;

namespace VDI.Demo.PSAS.Penalties
{
    public interface IPSASPenaltyAppService : IApplicationService
    {
        GetDataPenaltyViewListDto GetDataPenaltyView(GetPSASParamsDto input);
        void CreatePenaltyScheduler();
        void CreatePaymentPenaltyWaive();
    }
}
