﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PSAS.SuratPeringatans.Dto
{
    public class SendEmailSPInputDto
    {
        public int reminderLetterID { get; set; }
        public string letterType { get; set; }
        public string psCode { get; set; }
        public string name { get; set; }
        public string projectName { get; set; }
        public string unitCode { get; set; }
        public string unitName { get; set; }
        public string unitNo { get; set; }
        public string templateHtml { get; set; }


    }
}
