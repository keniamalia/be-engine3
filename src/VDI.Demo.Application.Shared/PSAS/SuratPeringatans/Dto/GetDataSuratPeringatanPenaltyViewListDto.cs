﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PSAS.SuratPeringatans.Dto
{
    public class GetDataSuratPeringatanPenaltyViewListDto
    {
        public int projectID { get; set; }
        public string project { get; set; }
        public int clusterID { get; set; }
        public string cluster { get; set; }
        public int unitID { get; set; }
        public string unitCode { get; set; }
        public string unitNo { get; set; }
        public string psCode { get; set; }
        public string custName { get; set; }
        public List<GetDataReminderLetterDto> dataReminderLetter { get; set; }
    }

    public class GetDataReminderLetterDto
    {
        public DateTime generateDate { get; set; }
        public string letterType { get; set; }
        public DateTime? sentDate { get; set; }
        public string status { get; set; }
        public string documentUrl { get; set; }
        public string documentName { get; set; }
    }
}
