﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.PSAS.Dto;
using VDI.Demo.PSAS.SuratPeringatans.Dto;

namespace VDI.Demo.PSAS.SuratPeringatans
{
    public interface IPSASSuratPeringatanAppService : IApplicationService
    {
        GetDataSuratPeringatanPenaltyViewListDto GetDataSuratPeringatanPenaltyView(GetPSASParamsDto input);
        void CreateSuratPeringatanPenaltyScheduler();
        void SendEmailSPScheduler();
    }
}
