﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PSAS.CancelUnit.Dto
{
    public class CreateTrBookingCancelInputDto
    {
        public int bookingHeaderID { get; set; }
        public int entityID { get; set; }
        public int reasonID { get; set; }
        public DateTime cancelDate { get; set; }
        public decimal lostAmount { get; set; }
        public decimal refundAmount { get; set; }
        public string newBookCode { get; set; }
        public string remarks { get; set; }
    }
}
