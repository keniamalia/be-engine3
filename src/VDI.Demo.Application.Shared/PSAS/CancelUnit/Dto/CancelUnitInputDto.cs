﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PSAS.CancelUnit.Dto
{
    public class CancelUnitInputDto
    {
        public int bookingHeaderID { get; set; }
        public string bookCode { get; set; }

        public decimal lostAmount { get; set; }
        public decimal refundAmount { get; set; }
        public int reasonID { get; set; }
        public string reasonCode { get; set; }
        public string remarks { get; set; }

    }
}
