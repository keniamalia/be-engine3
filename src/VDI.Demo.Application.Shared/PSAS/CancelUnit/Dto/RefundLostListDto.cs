﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PSAS.CancelUnit.Dto
{
    public class RefundLostListDto
    {
        public int payNo { get; set; }
        public decimal pct { get; set; }
    }
}
