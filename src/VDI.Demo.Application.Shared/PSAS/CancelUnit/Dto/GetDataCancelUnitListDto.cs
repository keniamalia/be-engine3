﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PSAS.CancelUnit.Dto
{
    public class GetDataCancelUnitListDto
    {
        public int bookingHeaderID { get; set; }
        public string bookCode { get; set; }
        public DateTime? cancelDate { get; set; }
        public string unitNo { get; set; }
        public string unitCode { get; set; }
        public string psCode { get; set; }
        public string customerName { get; set; }

        public decimal totalPaymentAmt { get; set; }
        public decimal totalPaymentVat { get; set; }
        public  decimal totalPayment { get; set; }
        public decimal totalPenalty { get; set; }
        public decimal paymentAdjBankCharge { get; set; }

    }
}
