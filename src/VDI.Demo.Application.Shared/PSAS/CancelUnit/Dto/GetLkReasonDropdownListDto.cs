﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PSAS.CancelUnit.Dto
{
    public class GetLkReasonDropdownListDto
    {
        public int reasonID { get; set; }
        public string reasonCode { get; set; }
        public string reasonDesc { get; set; }
    }
}
