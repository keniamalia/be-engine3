﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.PSAS.CancelUnit.Dto;
using VDI.Demo.PSAS.Dto;

namespace VDI.Demo.PSAS.CancelUnit
{
    public interface ICancelUnitPSASAppService : IApplicationService
    {
        List<GetLkReasonDropdownListDto> GetLkReasonDropdown();

        GetDataCancelUnitListDto GetDataCancelUnit(GetPSASParamsDto input);

        void CancelUnitSave(CancelUnitInputDto input);

        void CreateTrBookingCancel(CreateTrBookingCancelInputDto input);
    }
}
