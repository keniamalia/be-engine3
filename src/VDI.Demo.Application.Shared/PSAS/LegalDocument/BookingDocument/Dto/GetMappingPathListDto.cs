﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PSAS.LegalDocument.BookingDocument.Dto
{
    public class GetMappingPathListDto
    {
        public int mappingTemplateID { get; set; }
        public bool isTandaTerima { get; set; }
        public string templateName { get; set; }
        public string templateFile { get; set; }
        public int copyNo { get; set; }
        public string tandaTerimaFile { get; set; }
    }
}
