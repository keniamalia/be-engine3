﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PSAS.LegalDocument.BookingDocument.Dto
{
    public class DocCodeDto
    {
        public int docID { get; set; }
        public string docCode { get; set; }
    }
}
