﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PSAS.LegalDocument.BookingDocument.Dto
{
    public class fileExtensionGenerateListDto
    {
        public string phisicalPath { get; set; }
        public int orderNumber { get; set; }
    }
}
