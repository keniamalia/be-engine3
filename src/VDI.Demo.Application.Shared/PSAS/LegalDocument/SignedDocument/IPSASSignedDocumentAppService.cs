﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.PSAS.LegalDocument.SignedDocument.Dto;

namespace VDI.Demo.PSAS.LegalDocument.SignedDocument
{
    public interface IPSASSignedDocumentAppService : IApplicationService
    {
        void UploadSignedDocument(UploadSignedDocumentInputDto input);
        List<GetDocNoDropdownListDto> GetDocNoDropdown(string docNo);
        GetDetailBookingDocumentListDto GetDetailBookingDocument(int bookingDocumentID);
        List<GetDetailBookingDocumentListDto> GetBookingDocumentWithSignature();
    }
}
