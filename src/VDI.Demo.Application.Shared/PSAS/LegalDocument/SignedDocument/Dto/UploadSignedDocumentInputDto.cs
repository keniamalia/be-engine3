﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PSAS.LegalDocument.SignedDocument.Dto
{
    public class UploadSignedDocumentInputDto
    {
        public int trBookingDocID { get; set; }
        public string docNo { get; set; }
        public DateTime signedDocumentDate { get; set; }
        public string signedDocumentFile { get; set; }
        public DateTime signatureDate { get; set; }
    }
}
