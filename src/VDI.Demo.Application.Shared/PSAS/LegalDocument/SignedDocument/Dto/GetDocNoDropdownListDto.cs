﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PSAS.LegalDocument.SignedDocument.Dto
{
    public class GetDocNoDropdownListDto
    {
        public int trBookingDocID { get; set; }
        public string docNo { get; set; }
        public string docType { get; set; }
        public string docDate { get; set; }
    }
}
