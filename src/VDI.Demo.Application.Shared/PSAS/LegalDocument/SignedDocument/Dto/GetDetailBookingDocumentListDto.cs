﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PSAS.LegalDocument.SignedDocument.Dto
{
    public class GetDetailBookingDocumentListDto
    {
        public int bookingDocumentId { get; set; }
        public string docCode { get; set; }
        public string docName { get; set; }
        public string docNo { get; set; }
        public DateTime docDate { get; set; }
        public string remarks { get; set; }
        public bool isActive { get; set; }
        public bool isTandaTerima { get; set; }
        public string fileTandaTerima { get; set; }
        public string signedDocumentDate { get; set; }
        public string signatureDate { get; set; }
        public string signatureFile { get; set; }
    }
}
