﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PSAS.LegalDocument.DocTemplate.Dto
{
    public class CreateDocTemplateInputDto
    {
        public string templateName { get; set; }
        public List<TemplateFileListDto> templateFileDto { get; set; }
        public int docID { get; set; }
        public bool isMaster { get; set; }
        public int copyNo { get; set; }
    }

    public class TemplateFileListDto
    {
        public int orderNumber { get; set; }
        public string templateFile { get; set; }
    }
}
