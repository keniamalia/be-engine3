﻿using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.Files.Dto;

namespace VDI.Demo.PSAS.LegalDocument.DocTemplate.Dto
{
    public class GetDocTemplateListDto
    {
        public int docTemplateID { get; set; }
        public string docTemplateCode { get; set; }
        public string docTemplateName { get; set; }
        public string docCode { get; set; }
        public string linkFileDoc { get; set; }
        public bool isActive { get; set; }
        public bool isMaster { get; set; }
        public string creationTime { get; set; }
        public string creationBy { get; set; }
        public long? creatorUserId { get; set; }
        public List<string> filePath { get; set; }
        public List<LinkPathListDto> path { get; set; }
        public int? copyNo { get; set; }
    }
}
