﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PSAS.LegalDocument.DocTemplate.Dto
{
    public class CheckIsMasterDocIdListDto
    {
        public bool isMaster { get; set; }
        public int docTemplateID { get; set; }
    }
}
