﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PSAS.LegalDocument.MappingKadaster.Dto
{
    public class UnitByClusterListDto
    {
        public string unitCode {get;set;}
        public List<UnitDesc> unit {get;set;}
    }

    public class UnitDesc
    {
        public int unitID { get; set; }
        public string unitNo { get; set; }
        public bool isSelected { get; set; }
    }
}
