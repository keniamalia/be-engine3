﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PSAS.LegalDocument.MappingKadaster.Dto
{
    public class UnitDetail
    {
        public string clusterName { get; set; }
        public string clusterCode { get; set; }
        public string unitCode { get; set; }
        public string unitNo { get; set; }
    }
}
