﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PSAS.LegalDocument.MappingKadaster.Dto
{
    public class CreateMappingKadasterInputDto
    {
        public int projectID { get; set; }
        public int categoryID { get; set; }
        public string url { get; set; }
        public bool isGenerateSystem { get; set; }
        public List<ListCodeKadaster> codeKadaster { get; set; }
    }

    public class ListCodeKadaster
    {
        public int clusterID { get; set; }
        public string kadasterCodeName { get; set; }
        public int unitID { get; set; }
    }
}
