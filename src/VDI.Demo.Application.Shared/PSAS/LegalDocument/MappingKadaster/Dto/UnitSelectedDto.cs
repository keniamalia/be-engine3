﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PSAS.LegalDocument.MappingKadaster.Dto
{
    public class UnitSelectedDto
    {
        public string unitCode { get; set; }
        public int unitCodeID { get; set; }
        public int unitID { get; set; }
        public string unitNo { get; set; }
        public bool kadasterCodeSelected { get; set; }                        
    }
}
