﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Runtime.Validation;
using VDI.Demo.Dto;

namespace VDI.Demo.PSAS.LegalDocument.MappingKadaster.Dto
{
    public class ListOfMappingKadasterInputDto : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "projectName,categoryName,url,isActive";
            }
        }
    }
}
