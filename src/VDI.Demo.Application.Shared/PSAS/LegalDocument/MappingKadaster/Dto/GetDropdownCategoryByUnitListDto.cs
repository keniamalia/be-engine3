﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PSAS.LegalDocument.MappingKadaster.Dto
{
    public class GetDropdownCategoryByUnitListDto
    {
        public string categoryCode { get; set; }
        public string categoryName { get; set; }
        public int categoryID { get; set; }
        public string kadasterType { get; set; }
    }
}
