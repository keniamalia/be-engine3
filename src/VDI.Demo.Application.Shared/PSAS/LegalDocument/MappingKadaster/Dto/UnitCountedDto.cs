﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PSAS.LegalDocument.MappingKadaster.Dto
{
    public class UnitCountedDto
    {
        public string kadasterCode { get; set; }
        public int unitAmount { get; set; }

        public List<UnitDetail> unitDetail { get; set; }
    }
}
