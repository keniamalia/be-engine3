﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PSAS.LegalDocument.MappingKadaster.Dto
{
    public class GetMappingKadasterListDto
    {
        public int mappingKadasterID { get; set; }
        public int projectID { get; set; }
        public string projectName { get; set; }
        public int categoryID { get; set; }
        public string categoryName { get; set; }
        public string url { get; set; }
        public string criteriaKadaster { get; set; }
        public bool isActive { get; set; }
        public bool isGenerateBySystem { get; set; }
        public List<UnitCountedDto> kadasterCodeAndDetailUnit { get; set; }
    }
}
