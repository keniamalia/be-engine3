﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.PSAS.LegalDocument.MappingKadaster.Dto;

namespace VDI.Demo.PSAS.LegalDocument.MappingKadaster
{
    public interface IPSASMappingKadasterAppService : IApplicationService
    {
        PagedResultDto<GetMappingKadasterListDto> GetMappingKadaster(ListOfMappingKadasterInputDto input);
        List<GetDropdownCategoryByUnitListDto> GetDropdownCategoryByProjectUnit(int projectID);
        List<UnitByClusterListDto> GetUnitByCluster(int clusterID, int projectID, int categoryID);
        void UpdateIsActiveMappingKadaster(int projectID, int categoryID);
        void CreateMappingKadaster(CreateMappingKadasterInputDto input);
        bool CheckCategoryIsExist(int projectID, int categoryID);
        void UpdateMappingKadasterByCode(CreateMappingKadasterInputDto input);
        //GetMappingKadasterListDto ViewMappingKadaster(int projectID, int categoryID, bool status, int criteriaKadaster);
        //List<UnitCountedDto> GetUnitCounted(int projectID, int categoryID, bool isActive);
        //List<UnitDetail> GetDetailUnitMappingKadaster(string kadasterCode, bool isActive);
        JObject GetDefaultMappingKadasterLocation();
    }
}
