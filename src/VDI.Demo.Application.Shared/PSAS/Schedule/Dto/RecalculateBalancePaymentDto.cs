﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PSAS.Schedule.Dto
{
    public class RecalculateBalancePaymentDto
    {
        public string ket { get; set; }
        public int? payForID { get; set; }
        public int paymentDetailID { get; set; }
        public int entityID { get; set; }
        public decimal netAmt { get; set; }
        public decimal vatAmt { get; set; }
        public decimal totalAmt { get; set; }
    }
}
