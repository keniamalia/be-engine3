﻿using Abp.Application.Services;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using VDI.Demo.Dto;
using VDI.Demo.PSAS.AccountStatement.Dto;
using VDI.Demo.PSAS.ChangeOwnership.Dto;
using VDI.Demo.PSAS.Dto;

namespace VDI.Demo.PSAS.AccountStatement
{
    public interface IAccountStatementPSASAppService : IApplicationService
    {
        FileDto GetAccountStatementToExport(AccountStatementInputDto input);
    }
}
