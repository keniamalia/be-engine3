﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PSAS.AccountStatement.Dto
{
    public class GetAccountStatementToExportDto
    {

        public string projectImage { get; set; }
        public string projectName { get; set; }

        public string publishDate { get; set; }

        public string psName { get; set; }
        public string psCode { get; set; }

        public string companyName { get; set; }
        public string companyImage { get; set; }

        public string unitCode { get; set; }
        public string unitNo { get; set; }

        public string bookCode { get; set; }
        public string bookDate { get; set; }

        public string sellingPrice { get; set; }
        public string paymentTerm { get; set; }

        public string salesName { get; set; }
        public string salesPhone { get; set; }
        public string salesEmail { get; set; }

        public string totalScheduleAmount { get; set; }
        public string totalPaymentAmount { get; set; }
        public string outstandingAmount { get; set; }
        public string outstandingPenalty { get; set; }
        public string totalBalance { get; set; }

        public List<DataSchedulePayment> getDataSchedulePayment { get; set; }
    }

    public class DataSchedulePayment
    {
        public char type { get; set; } //P/S (Payment/Schedule)
        public string dueDate { get; set; }
        public string paymentDate { get; set; }
        public string transactionDesc { get; set; }
        public string allocCode { get; set; }
        public string scheduleAmount { get; set; }
        public decimal totalSchedulePayment { get; set; }
        public string paymentAmount { get; set; }
        public DateTime orderDate { get; set; }
    }
}
