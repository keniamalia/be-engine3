﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PSAS.AccountStatement.Dto
{
    public class AccountStatementInputDto
    {
        public string value { get; set; }
        public string flag { get; set; }
    }
}
