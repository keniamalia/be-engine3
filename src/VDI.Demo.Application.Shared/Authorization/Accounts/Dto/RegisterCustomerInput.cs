﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Authorization.Accounts.Dto
{
    public class RegisterCustomerInput
    {
        public string email { get; set; }

        public string name { get; set; }

        public string password { get; set; }
    }
}
