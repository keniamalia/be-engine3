﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.Commission.MS_PPhRanges.Dto;

namespace VDI.Demo.Commission.MS_PPhRanges
{
    public interface IMsPPhRangeAppService : IApplicationService
    {
        ListResultDto<CreateOrUpdatePPhRangeListDto> GetMsPPhRangeBySchemaCode(string scmCode);
        JObject CreateMsPPhRange(List<CreateOrUpdatePPhRangeListDto> input);
        JObject UpdateMsPPhRange(CreateOrUpdatePPhRangeListDto input);
        JObject DeleteMsPPhRange(DeleteMsPPhRangeDto input);
    }
}
