﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Commission.MS_PPhRanges.Dto
{
    public class CreateOrUpdatePPhRangeListDto
    {
        public string entityCode { get; set; }
        public string scmCode { get; set; }
        public int pphYear { get; set; }
        public decimal pphRangeHighBound { get; set; }        
        public double pphRangePct { get; set; }
        public string tax_code { get; set; }
        public string tax_code_non_npwp { get; set; }
        public string pphRangePct_non_npwp { get; set; }
    }
}
