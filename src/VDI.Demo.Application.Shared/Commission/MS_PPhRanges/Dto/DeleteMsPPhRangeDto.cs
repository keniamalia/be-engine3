﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Commission.MS_PPhRanges.Dto
{
    public class DeleteMsPPhRangeDto
    {
        public string entityCode { get; set; }
        public string scmCode { get; set; }
        public int pphYear { get; set; }
        public decimal pphRangeHighBound { get; set; }
    }
}
