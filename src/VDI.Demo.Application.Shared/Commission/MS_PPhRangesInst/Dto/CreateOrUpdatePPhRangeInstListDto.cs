﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Commission.MS_PPhRangesInst.Dto
{
    public class CreateOrUpdatePPhRangeInstListDto
    {
        public string entityCode { get; set; }
        public string scmCode { get; set; }
        public double pphRangePct { get; set; }
        public DateTime validDate { get; set; }
        public string taxCode { get; set; }
    }
}
