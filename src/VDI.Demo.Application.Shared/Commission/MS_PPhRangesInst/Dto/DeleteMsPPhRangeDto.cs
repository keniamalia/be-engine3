﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Commission.MS_PPhRangesInst.Dto
{
    public class DeleteMsPPhRangeDto
    {
        public string entityCode { get; set; }
        public string scmCode { get; set; }
        public double pphRangePct { get; set; }
    }
}
