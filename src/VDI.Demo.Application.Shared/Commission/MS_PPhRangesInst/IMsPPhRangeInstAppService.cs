﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.Commission.MS_PPhRangesInst.Dto;

namespace VDI.Demo.Commission.MS_PPhRangesInst
{
    public interface IMsPPhRangeInstAppService : IApplicationService
    {
        ListResultDto<CreateOrUpdatePPhRangeInstListDto> GetMsPPhRangeInstBySchemaCode(string scmCode);
        JObject CreateMsPPhRangeInst(List<CreateOrUpdatePPhRangeInstListDto> input);
        JObject UpdateMsPPhRangeInst(CreateOrUpdatePPhRangeInstListDto input);
        JObject DeleteMsPPhRange(DeleteMsPPhRangeDto input);
    }
}
