﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Commission.MS_Developer_Schemas.Dto
{
    public class GetDeveloperSchemasListDto
    {
        public string entityCode { get; set; }
        public string scmCode { get; set; }
        public string propCode { get; set; }
        public string devCode { get; set; }      
        public string propName { get; set; }
        public string schemaName { get; set; }
        public string devName { get; set; }

        [JsonIgnore] public string bankCode { get; set; }
        [JsonIgnore] public string bankAccountName { get; set; }
        [JsonIgnore] public string bankBranchName { get; set; }
        [JsonIgnore] public bool isActive { get; set; }
    }
}
