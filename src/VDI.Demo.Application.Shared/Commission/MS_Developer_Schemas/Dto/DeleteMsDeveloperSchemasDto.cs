﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Commission.MS_Developer_Schemas.Dto
{
    public class DeleteMsDeveloperSchemasDto
    {
        public string entityCode { get; set; }
        public string scmCode { get; set; }
        public string propCode { get; set; }
        public string devCode { get; set; }
    }
}
