﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VDI.Demo.Commission.MS_Developer_Schemas.Dto;

namespace VDI.Demo.Commission.MS_Developer_Schemas
{
    public interface IMsDeveloperSchemasAppService : IApplicationService
    {
        ListResultDto<GetDeveloperSchemasListDto> GetMsDeveloperSchemas();
        ListResultDto<GetDeveloperSchemasListDto> GetAllMsDeveloperSchemaPaging();
        ListResultDto<GetDeveloperSchemasListDto> GetMsDeveloperSchemasBySchema(string scmCode);
        ListResultDto<GetDropDownDeveloperSchemasListDto> GetDropDownMsDeveloperSchemasBySchema(string scmCode);
        GetDeveloperSchemasListDto GetDetailMsDeveloperSchemas(GetDetailMsDeveloperSchemasDto input);
        ListResultDto<GetPropCodeListDto> GetPropCodeBySchemaCode(string scmCode);
        List<GetDropDownDeveloperSchemasListDto> GetDataDeveloperSchemaByProperty(string propCode);
        string GetPropNameByPropCode(string propCode);

        JObject CreateMsDeveloperSchemas(CreateMsDeveloperSchemasInputDto input);
        JObject UpdateMsDeveloperSchemas(UpdateMsDeveloperSchemasInputDto input);
        JObject DeleteMsDeveloperSchemas(DeleteMsDeveloperSchemasDto input);
    }
}
