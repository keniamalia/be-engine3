﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Commission.MS_GroupSchemas.Dto
{
    public class GetDropDownSchemaByGroupSchemaIdListDto
    {
        public string entityCode { get; set; }
        public string schemaName { get; set; }  
        public string schemaCode { get; set; }
        public string groupSchemaName { get; set; }
        public string groupSchemaCode { get; set; }
    }
}
