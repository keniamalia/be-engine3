﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Commission.MS_GroupSchemas.Dto
{
    public class GetAllMsGroupCommPctListDto
    {
        public string groupSchemaCode { get; set; }
        public string entityCode { get; set; }

        public int? schemaID { get; set; }

        public string scmCode { get; set; }

        public DateTime validDate { get; set; }

        public string commTypeCode { get; set; }

        public string commTypeName { get; set; }

        public string statusCode { get; set; }

        public string statusName { get; set; }

        public Byte uplineNo { get; set; }

        public double? commPctPaid { get; set; }

        public decimal? nominal { get; set; }

        public bool? isStandard { get; set; }

        public List<GetMsGroupCommPctListDto> groupCommPctID { get; set; }
    }
}
