﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Commission.MS_GroupSchemas.Dto
{
    public class CreateOrUpdateSetGroupSchemaInputDto
    {
        public string entityCode { get; set; }
        public string scmCode { get; set; }
        public string groupSchemaCode { get; set; }
        public string groupSchemaName { get; set; }
        public DateTime validFrom { get; set; }
        public string documentGrouping { get; set; }
        public string documentGroupingDelete { get; set; }
        public string statusDocument { get; set; } //update, delete, nothing
        public string projectCode { get; set; }
        public string clusterCode { get; set; }
        public bool isActive { get; set; }
    }
}
