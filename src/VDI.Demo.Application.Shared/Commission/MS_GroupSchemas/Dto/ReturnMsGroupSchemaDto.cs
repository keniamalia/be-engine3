﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Commission.MS_GroupSchemas.Dto
{
    public class ReturnMsGroupSchemaDto
    {
        public string entityCode { get; set; }
        public string scmCode { get; set; }
        public string groupSchemaCode { get; set; }
    }
}
