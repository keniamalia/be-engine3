﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.Commission.MS_PointPercentage.Dto;

namespace VDI.Demo.Commission.MS_PointPercentage
{
    public interface IMsPointPctAppService : IApplicationService
    {
        JObject CreateMsPointPct(List<InputPointPctDto> input);
        JObject UpdateMsPointPct(InputPointPctDto input);
        JObject DeleteMsPointPct(DeleteMsPointPctDto input);
        ListResultDto<GetAllPointPctListDto> GetMsPointPctBySchemaCode(string scmCode);
    }
}
