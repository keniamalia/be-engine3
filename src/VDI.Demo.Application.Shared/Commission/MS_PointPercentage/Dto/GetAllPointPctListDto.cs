﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Commission.MS_PointPercentage.Dto
{
    public class GetAllPointPctListDto
    {
        public string entityCode { get; set; }
        public string scmCode { get; set; }
        public string statusCode { get; set; }
        public byte uplineNo { get; set; }
        public string statusName { get; set; }
        public string pointTypeCode { get; set; }
        public string pointTypeName { get; set; }
        public double pointPct { get; set; }
        public Decimal pointKonvert { get; set; }
    }
}
