﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Commission.MS_PointPercentage.Dto
{
    public class DeleteMsPointPctDto
    {
        public string entityCode { get; set; }
        public string scmCode { get; set; }
        public string statusCode { get; set; }
        public byte asUplineNo { get; set; }
    }
}
