﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Commission.MS_Schemas.Dto
{
    public class GetMsCommPctListDto
    {
        public string entityCode { get; set; }
        public string scmCode { get; set; }
        public DateTime validDate { get; set; }
        public int commTypeID { get; set; }
        public string commTypeCode { get; set; }
        public string commTypeName { get; set; }
        public int statusMemberID { get; set; }
        public string statusCode { get; set; }
        public string statusName { get; set; }
        public Byte uplineNo { get; set; }
        public decimal minAmt { get; set; }
        public decimal maxAmt { get; set; }
        public double? commPctPaid { get; set; }
        public double? commPctHold { get; set; }
    }
}
