﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Commission.MS_Schemas.Dto
{
    public class CreateOrUpdateSetSchemaInputDto
    {
        public string entityCode { get; set; }
        public string scmCode { get; set; }
        public string scmName { get; set; }     
        public string digitMemberCode { get; set; }
        public short upline { get; set; }
        public bool isActive { get; set; }
    }
}
