﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Commission.MS_Schemas.Dto
{
    public class GetLkUplineListDto
    {
        public string entityCode { get; set; }
        public string scmCode { get; set; }
        public short uplineNo { get; set; }
    }
}
