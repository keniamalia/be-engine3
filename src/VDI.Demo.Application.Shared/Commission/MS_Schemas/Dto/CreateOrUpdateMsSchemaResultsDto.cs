﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Commission.MS_Schemas.Dto
{
    public class CreateOrUpdateMsSchemaResultsDto
    {
        public string entityCode { get; set; }
        public string scmCode { get; set; }
    }
}
