﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.Commission.MS_Schemas.Dto
{
    public class CreateMsSchemaRequirementInputDto
    {
        public string entityCode { get; set; }
        public string scmCode { get; set; }
        public List<setCommReq> setCommReq { get; set; }
    }

    public class setCommReq
    {
        public string entityCode { get; set; }
        public string scmCode { get; set; }
        public byte reqNo { get; set; }
        public string reqDesc { get; set; }
        public double pctPaid { get; set; }
        public double orPctPaid { get; set; }
    }
}
