﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VDI.Demo.Commission.MS_Schemas.Dto;

namespace VDI.Demo.Commission.MS_Schemas
{
    public interface IMsSchemaAppService : IApplicationService
    {
        Task<PagedResultDto<GetAllMsSchemaListDto>> GetAllMsSchema(GetMsSchemaListInput input);
        List<GetMsSchemaRequirementListDto> GetMsSchemaRequirementBySchemaCode(string scmCode);
        List<GetLkCommTypeListDto> GetLkCommTypeBySchemaCode(string scmCode);
        List<GetMsStatusMemberListDto> GetMsStatusMemberBySchemaCode(string scmCode);
        List<GetLkPointTypeListDto> GetLkPointTypeBySchemaCode(string scmCode);
        List<GetMsCommPctListDto> GetMsCommPctBySchemaCode(string scmCode);
        List<GetLkUplineListDto> GetLkUpline(string scmCode);
        CreateOrUpdateSetSchemaInputDto GetDetailMsSchema(string entityCode, string scmCode);
        JObject CreateOrUpdateMsSchemaRequirement(CreateMsSchemaRequirementInputDto input);
        JObject CreateOrUpdateMsStatusMember(CreateMsStatusMemberInputDto input);
        CreateOrUpdateMsSchemaResultsDto CreateOrUpdateMsSchema(CreateOrUpdateSetSchemaInputDto input);
        JObject CreateOrUpdateLkPointType(CreateOrUpdateSetPointTypeInputDto input);
        JObject CreateOrUpdateLkCommType(CreateOrUpdateSetCommTypeInputDto input);
        JObject CreateOrUpdateMsCommPct(CreateOrUpdateMsCommPctInputDto input);
    }
}
