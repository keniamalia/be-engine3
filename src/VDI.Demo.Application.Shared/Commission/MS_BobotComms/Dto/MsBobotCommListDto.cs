﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace VDI.Demo.Commission.MS_BobotComms.Dto
{
    public class MsBobotCommListDto
    {
        public string entityCode { get; set; }
        public string scmCode { get; set; }
        public string projectCode { get; set; }
        public string clusterCode { get; set; }
        public double pctBobot { get; set; }
        public bool isActive { get; set; }
        public bool isComplete { get; set; }
        public string clusterName { get; set; }
        public string projectName { get; set; }
        
        [JsonIgnore] public DateTime inputTime { get; set; }
    }
}
