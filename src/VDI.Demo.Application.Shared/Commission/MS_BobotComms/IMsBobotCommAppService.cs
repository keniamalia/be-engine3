﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.Commission.MS_BobotComms.Dto;

namespace VDI.Demo.Commission.MS_BobotComms
{
    public interface IMsBobotCommAppService : IApplicationService
    {
        ListResultDto<MsBobotCommListDto> GetMsBobotCommByProject(string projectCode);
        JObject CreateMsBobotComm(List<MsBobotCommListDto> input);
        JObject UpdateMsBobotComm(MsBobotCommListDto input);
        JObject DeleteMsBobotComm(MsBobotCommListDto Id);
    }
}
