﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.CreditManagement.CreditAgreements.Dto
{
    public class GetProgramDetailListDto
    {
        public int agreementDetailID { get; set; }
        public int clusterID { get; set; }
        public string clusterName { get; set; }
        public string programNo { get; set; }
        public string programName { get; set; }
        public DateTime programStartDate { get; set; }
        public DateTime programEndDate { get; set; }
        public decimal bankRate { get; set; }
        public int provisiTypeID { get; set; }
        public string provisiTypeName { get; set; }
        public decimal provisiFee { get; set; }
        public int adminTypeID { get; set; }
        public string adminTypeName { get; set; }
        public decimal adminFee { get; set; }
        public string addendumNo { get; set; }
        public string addendumDocs { get; set; }
        public string addendumDocsUrl { get; set; }
        public string addendumDocsStatus { get; set; }
        public bool status { get; set; }
        public List<GetProgramRateListDto> programRate { get; set; }
        public List<GetRetentionDetailListDto> retentionDetail { get; set; }
        public GetSubsidyDto subsidy { get; set; }
    }
}
