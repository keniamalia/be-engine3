﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.CreditManagement.CreditAgreements.Dto
{
    public class GetSubsidyDto
    {
        public int subsidyID { get; set; }
        public decimal value { get; set; }
        public int typeID { get; set; }
        public string typeName { get; set; }
        public string remarks { get; set; }
        public bool isCalculateSI { get; set; }
    }
}
