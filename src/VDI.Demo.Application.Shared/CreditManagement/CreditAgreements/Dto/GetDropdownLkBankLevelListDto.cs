﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.CreditManagement.CreditAgreements.Dto
{
    public class GetDropdownLkBankLevelListDto
    {
        public int typeID { get; set; }
        public string typeName { get; set; }
    }
}
