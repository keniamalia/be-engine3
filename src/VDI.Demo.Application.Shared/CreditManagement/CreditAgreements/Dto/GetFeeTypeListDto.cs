﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.CreditManagement.CreditAgreements.Dto
{
    public class GetFeeTypeListDto
    {
        public int feeTypeID { get; set; }
        public string feeTypeName { get; set; }
        public bool isActive { get; set; }
    }
}
