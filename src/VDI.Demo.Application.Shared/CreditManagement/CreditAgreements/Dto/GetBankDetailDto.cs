﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.CreditManagement.CreditAgreements.Dto
{
    public class GetBankDetailDto
    {
        public int bankDetailID { get; set; }
        public string bankName { get; set; }
        public string address { get; set; }
        public string pic { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string officePhone { get; set; }
        public bool isPriority { get; set; }
        public bool isPPOnline { get; set; }
        public bool isOB { get; set; }
    }
}
