﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.CreditManagement.CreditAgreements.Dto
{
    public class GetCreditAgreementViewDetail
    {
        public GetAgreementInformationDto agreementInformation { get; set; }
        public List<GetProgramDetailListDto> programDetail { get; set; }
    }
}
