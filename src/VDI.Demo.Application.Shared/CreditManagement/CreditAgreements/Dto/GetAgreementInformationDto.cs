﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.CreditManagement.CreditAgreements.Dto
{
    public class GetAgreementInformationDto
    {
        public int creditAgreementID { get; set; }
        public int projectID { get; set; }
        public string projectName { get; set; }
        public int typeID { get; set; }
        public string typeName { get; set; }
        public int bankID { get; set; }
        public GetBankDetailDto bankDetail { get; set; }
        public string agreementDocsNo { get; set; }
        public string agreementDocsName { get; set; }
        public string agreementDocsUrl { get; set; }
        public string agreementDocsStatus { get; set; }
        public DateTime agreementDate { get; set; }
        public DateTime agreementStartDate { get; set; }
        public DateTime agreementEndDate { get; set; }
        public decimal  agreementRate { get; set; }
        public bool isActive { get; set; }
    }
}
