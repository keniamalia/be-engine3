﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.CreditManagement.CreditAgreements.Dto
{
    public class GetCreditAgreementListDto
    {
        public int creditAgreementID { get; set; }
        public int projectID { get; set; }
        public string projectName { get; set; }
        public int typeID { get; set; }
        public string typeName { get; set; }
        public int bankID { get; set; }
        public string bankName { get; set; }
        public decimal agreementRate { get; set; }
        public string agreementDocs { get; set; }
        public DateTime agreementDate { get; set; }
        public DateTime agreementStartDate { get; set; }
        public DateTime agreementEndDate { get; set; }
        public bool status { get; set; }
    }
}
