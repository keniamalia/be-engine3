﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.CreditManagement.CreditAgreements.Dto
{
    public class GetRetentionDetailListDto
    {
        public int retentionDetailID { get; set; }
        public int sortNo { get; set; }
        public int retentionID { get; set; }
        public string retentionName { get; set; }
        public decimal percentage { get; set; }
    }
}
