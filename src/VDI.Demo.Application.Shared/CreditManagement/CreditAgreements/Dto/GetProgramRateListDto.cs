﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.CreditManagement.CreditAgreements.Dto
{
    public class GetProgramRateListDto
    {
        public int programDetailID { get; set; }
        public int tenor { get; set; }
        public decimal bankRate { get; set; }
        public int fix { get; set; }
    }
}
