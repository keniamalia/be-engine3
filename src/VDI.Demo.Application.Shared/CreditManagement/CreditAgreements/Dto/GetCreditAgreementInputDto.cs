﻿using Abp.Runtime.Validation;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.Dto;

namespace VDI.Demo.CreditManagement.CreditAgreements.Dto
{
    public class GetCreditAgreementInputDto : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "projectName,typeName,bankName,agreementDate,agreementStartDate,agreementEndDate";
            }
        }
    }
}
