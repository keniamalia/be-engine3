﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.CreditManagement.CreditAgreements.Dto;
using VDI.Demo.MasterPlan.Project.MS_Banks.Dto;

namespace VDI.Demo.CreditManagement.CreditAgreements
{
    public interface ICreditAgreementAppService : IApplicationService
    {
        PagedResultDto<GetCreditAgreementListDto> GetCreditAgreement(GetCreditAgreementInputDto input);
        GetAgreementInformationDto GetAgreementInformation(int creditAgreementID);
        List<GetProgramDetailListDto> GetProgramDetailList(int creditAgreementID);
        List<GetProgramRateListDto> GetProgramDetail(int agreementDetailID);
        List<GetRetentionDetailListDto> GetRetentionDetail(int agreementDetailID);
        GetBankDetailDto GetBankDetail(int creditAgreementID);
        GetSubsidyDto GetSubsidy(int agreementDetailID);
        GetCreditAgreementViewDetail GetCreditAgreementViewDetail(int creditAgreementID);
        void CreateOrUpdateUniversalCreditAgreement(GetCreditAgreementViewDetail input);
        GetMsBankDto GetMsBankById(int bankID);
        List<GetBankDropDownListDto> GetMsBankByBankType(int bankTypeID);
        JObject GetProgramNo();
        JObject GetAddendumNo();
        JObject GetAgreementDocsNo(int projectID, int bankID);
        List<GetFeeTypeListDto> GetFeeType();
    }
}
