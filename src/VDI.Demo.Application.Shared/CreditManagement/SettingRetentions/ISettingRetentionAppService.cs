﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.CreditManagement.SettingRetentions.Dto;

namespace VDI.Demo.CreditManagement.SettingRetentions
{
    public interface ISettingRetentionAppService : IApplicationService
    {
        PagedResultDto<GetAllSettingRetentionListDto> GetAllSettingRetention(GetAllSettingRetentionInputDto input);
        void CreateRetention(CreateRetentionInputDto input);
        void UpdateRetention(UpdateRetentionInputDto input);
        List<GetAllSettingRetentionListDto> GetAllMsRetentionDropdown();
        JObject GetRetentionCode();
    }
}
