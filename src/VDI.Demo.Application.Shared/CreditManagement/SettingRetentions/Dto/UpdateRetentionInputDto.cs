﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.CreditManagement.SettingRetentions.Dto
{
    public class UpdateRetentionInputDto
    {
        public int retentionID { get; set; }
        public string retentionName { get; set; }
        public bool status { get; set; }
    }
}
