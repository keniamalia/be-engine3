﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.CreditManagement.SettingRetentions.Dto
{
    public class CreateRetentionInputDto
    {
        public string retentionName { get; set; }
        public bool status { get; set; }
    }
}
