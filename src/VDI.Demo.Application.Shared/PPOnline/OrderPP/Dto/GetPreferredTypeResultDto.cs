﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PPOnline.OrderPP.Dto
{
    public class GetPreferredTypeResultDto
    {
        public int prefferedTypeID { get; set; }

        public string preferredTypeName { get; set; }
    }
}
