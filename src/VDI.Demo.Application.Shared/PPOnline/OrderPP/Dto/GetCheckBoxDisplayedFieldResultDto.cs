﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PPOnline.OrderPP.Dto
{
    public class GetCheckBoxDisplayedFieldResultDto
    {
        public int displayedID { get; set; }

        public string displayedName { get; set; }
    }
}
