﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PPOnline.OrderPP.Dto
{
    public class GetListReportOrderResultDto
    {
        public string year { get; set; }

        public string yearMonth { get; set; }

        public string orderCode { get; set; }

        public string orderDate { get; set; }

        public string orderStatus { get; set; }

        public string projectName { get; set; }

        public string customerName { get; set; }

        public string preferredTypeName { get; set; }

        public string termPayment { get; set; }

        public int qty { get; set; }

        public decimal? price { get; set; }

        public decimal? totalPrice { get; set; }

        public string memberName { get; set; }

        public int projectID { get; set; }

        public int prefferedType { get; set; }

        public int orderStatusID { get; set; }

        public int termID { get; set; }
    }
}
