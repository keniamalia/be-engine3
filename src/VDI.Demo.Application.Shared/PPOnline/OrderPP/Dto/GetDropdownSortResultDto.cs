﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PPOnline.OrderPP.Dto
{
    public class GetDropdownSortResultDto
    {
        public int sortID { get; set; }

        public string sortName { get; set; }
    }
}
