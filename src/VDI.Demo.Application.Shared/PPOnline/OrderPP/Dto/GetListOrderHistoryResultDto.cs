﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PPOnline.OrderPP.Dto
{
    public class GetListOrderHistoryResultDto
    {
        public int PPOrderID { get; set; }

        public int? paymentType { get; set; }

        public int orderStatusID { get; set; }

        public int projectID { get; set; }

        public string projectName { get; set; }
        
        public string orderCode { get; set; }

        public string customerName { get; set; }

        public string orderStatusName { get; set; }

        public string PPNo { get; set; }

        public string psCode { get; set; }

        public DateTime creationTime { get; set; }
    }
}
