﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PPOnline.OrderPP.Dto
{
    public class GetReportOrderPdfInputDto
    {
        public int paperSizeID { get; set; }

        public int orientationID { get; set; }
        
        public ListDataDto listData { get; set; }

    }
    public class ListHeaderDto
    {
        public int headerID { get; set; }
        public string header { get; set; }
    }
    public class ListDataDto
    {
        public List<ListHeaderDto> listHeader { get; set; }
        public List<ListBodyDto> listBody { get; set; }

        public string headerHtml { get; set; }

        public string bodyHtml { get; set; }

        public int colspanTbl { get; set; }

        public string grandTotal { get; set; }

    }
    public class ListBodyDto
    {
        public string year { get; set; }

        public string yearMonth { get; set; }

        public string orderCode { get; set; }

        public string orderDate { get; set; }

        public string orderStatus { get; set; }

        public string projectName { get; set; }

        public string customerName { get; set; }

        public string preferredTypeName { get; set; }

        public string termPayment { get; set; }

        public int qty { get; set; }

        public decimal? price { get; set; }

        public decimal? totalPrice { get; set; }
    }
}
