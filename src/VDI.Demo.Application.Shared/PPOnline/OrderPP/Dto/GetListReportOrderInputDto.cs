﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PPOnline.OrderPP.Dto
{
    public class GetListReportOrderInputDto
    {
        public string orderDateFrom { get; set; }

        public string orderDateTo { get; set; }

        public int? projectID { get; set; }

        public int[] preferredTypeID { get; set; }

        public int[] termID { get; set; }

        public int[] orderStatusID { get; set; }

        public string keyword { get; set; }
    }
   
}
