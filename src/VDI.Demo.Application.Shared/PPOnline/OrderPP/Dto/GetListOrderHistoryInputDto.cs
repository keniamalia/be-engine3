﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PPOnline.OrderPP.Dto
{
    public class GetListOrderHistoryInputDto
    {
        public int? paymentType { get; set; }

        public int? projectID { get; set; }

        public string keyword { get; set; }

        public int? orderStatusID { get; set; }

        public int? sortID { get; set; }

        public int? byID { get; set; }
    }
}
