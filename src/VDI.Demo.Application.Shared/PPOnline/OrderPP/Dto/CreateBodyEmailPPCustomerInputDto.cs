﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PPOnline.OrderPP.Dto
{
    public class CreateBodyEmailPPCustomerInputDto
    {
        public string customerName { get; set; }

        public string productName { get; set; }

        public string orderCode { get; set; }

        public string paymentDate { get; set; }

        public int qty { get; set; }

        public int PPOrderID { get; set; }

        public List<ListPPNo> listPPNo { get; set; }

        public string PPPrice { get; set; }

        public string PPNo { get; set; }

        public string logoProduct { get; set; }

        public string psCode { get; set; }

        public string memberCode { get; set; }
        public string emailCustomer { get; set; }
        public string emailMember { get; set; }

        public string PPHtml { get; set; }

    }
    public class ListPPNo
    {
        public string PPNo { get; set; }
    }
}
