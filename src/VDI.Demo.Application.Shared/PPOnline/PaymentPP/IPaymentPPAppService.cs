﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.PPOnline.PaymentPP.Dto;

namespace VDI.Demo.PPOnline.PaymentPP
{
    public interface IPaymentPPAppService : IApplicationService
    {
        GenerateTTBFResultDto PrintTTBF(int PPOrderID);
    }
}
