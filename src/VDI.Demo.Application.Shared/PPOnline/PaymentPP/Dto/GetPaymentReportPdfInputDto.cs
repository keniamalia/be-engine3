﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PPOnline.PaymentPP.Dto
{
    public class GetPaymentReportPdfInputDto
    {
        public int paperSizeID { get; set; }

        public int orientationID { get; set; }

        public ListDataPaymentDto listData { get; set; }
    }
    public class ListHeaderPaymentDto
    {
        public int headerID { get; set; }
        public string header { get; set; }
    }
    public class ListDataPaymentDto
    {
        public List<ListHeaderPaymentDto> listHeader { get; set; }
        public List<ListBodyPaymentDto> listBody { get; set; }

        public string headerHtml { get; set; }

        public string bodyHtml { get; set; }

        public int colspanTbl { get; set; }

        public decimal? grandTotal { get; set; }

    }
    public class ListBodyPaymentDto
    {
        public string year { get; set; }

        public string yearMonth { get; set; }

        public string orderCode { get; set; }

        public string paymentDate { get; set; }

        public string clearDate { get; set; }

        public string paymentStatus { get; set; }

        public string PPNo { get; set; }

        public string paymentTypeName { get; set; }

        public string bankName { get; set; }

        public string bankBranch { get; set; }

        public string accNo { get; set; }

        public string accName { get; set; }

        public string customerName { get; set; }
        
        public decimal? amount { get; set; }
    }
}
