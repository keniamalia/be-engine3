﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PPOnline.PaymentPP.Dto
{
    public class ListTTBFInputDto
    {
        public string name { get; set; }
        public List<TTBFInputDto> inputDto { get; set; }
    }
    public class TTBFInputDto
    {
        public GenerateTTBFInputDto dto { get; set; }

    }
    public class GenerateTTBFInputDto
    {
        public string customerName { get; set; }

        public string idNo { get; set; }

        public string noHp { get; set; }

        public string email { get; set; }

        public string metodePembayaran { get; set; }

        public string memberCode { get; set; }

        public string memberName { get; set; }

        public string PPPrice { get; set; }

        public string PPNO { get; set; }

        public string banner { get; set; }

        public string TNC { get; set; }

        public string fileName { get; set; }

        public string tanggal { get; set; }
        public string barcode { get; set; }
    }
}
