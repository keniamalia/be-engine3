﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PPOnline.PaymentPP.Dto
{
    public class GetListPaymentTypeResultDto
    {
        public int paymentType { get; set; }

        public string paymentTypeName { get; set; }
    }
}
