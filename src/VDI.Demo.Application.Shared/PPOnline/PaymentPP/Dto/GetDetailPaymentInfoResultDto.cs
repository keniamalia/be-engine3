﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PPOnline.PaymentPP.Dto
{
    public class GetDetailPaymentInfoResultDto
    {
        public string orderCode { get; set; }
        public string customerName { get; set; }
        public DateTime paymentDate { get; set; }
        public string qty { get; set; }
        public string projectName { get; set; }
        public string paymentTypeName { get; set; }
        public string bankName { get; set; }
        public string location { get; set; }
        public string accountNo { get; set; }
        public string accountName { get; set; }
        public string uploadReceipt { get; set; }
        public decimal? totalAmount { get; set; }
        public double paidAmount { get; set; }
        public decimal? outstanding { get; set; }
        public string orderStatusName { get; set; }

        public List<GetPaymentHistory> paymentHistory { get; set; }
    }

    public class GetPaymentHistory
    {
        public string orderCode { get; set; }

        public string projectName { get; set; }

        public string paymentTypeName { get; set; }

        public string bankName { get; set; }

        public int paymentID { get; set; }

        public int paymentType { get; set; }

        public int PPOrderID { get; set; }

        public double amount { get; set; }

    }
}
