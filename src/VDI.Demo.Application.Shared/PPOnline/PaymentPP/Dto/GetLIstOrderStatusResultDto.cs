﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PPOnline.PaymentPP.Dto
{
    public class GetLIstOrderStatusResultDto
    {
        public int orderStatusID { get; set; }
        public string orderStatusName { get; set; }
    }
}
