﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PPOnline.PaymentPP.Dto
{
    public class GetListDropdownBankResultDto
    {
        public int bankID { get; set; }

        public string bankName { get; set; }
    }
}
