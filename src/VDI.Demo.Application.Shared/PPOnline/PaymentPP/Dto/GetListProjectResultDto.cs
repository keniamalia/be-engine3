﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PPOnline.PaymentPP.Dto
{
    public class GetListProjectResultDto
    {
        public int projectID { get; set; }

        public string projectName { get; set; }
    }
}
