﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PPOnline.PaymentPP.Dto
{
    public class GetListPaymentCheckResultDto
    {
        public string orderCode { get; set; }

        public int projectID { get; set; }
        
        public string projectCode { get; set; }

        public string customerName { get; set; }

        public string psCode { get; set; }

        public string orderStatusName { get; set; }

        public int PPOrderID { get; set; }

        public int qty { get; set; }

        public decimal? totalAmount { get; set; }

        public decimal? outstanding { get; set; }

        public double paidAmount { get; set; }
    }
}
