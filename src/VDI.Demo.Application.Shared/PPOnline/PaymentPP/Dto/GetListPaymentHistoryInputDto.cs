﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PPOnline.PaymentPP.Dto
{
    public class GetListPaymentHistoryInputDto
    {
        public int? orderStatusID { get; set; }

        public int? paymentType { get; set; }

        public int? projectID { get; set; }

        public string keyword { get; set; }
    }
}
