﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PPOnline.PaymentPP.Dto
{
    public class GetDetailPaymentInfoInputtDto
    {
        public string customerName { get; set; }
        public string qty { get; set; }
        public int paymentID { get; set; }
        public int PPOrderID { get; set; }
    }
}
