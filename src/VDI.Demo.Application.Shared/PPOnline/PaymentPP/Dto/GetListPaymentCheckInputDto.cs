﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PPOnline.PaymentPP.Dto
{
    public class GetListPaymentCheckInputDto
    {
        public int? projectID { get; set; }

        public string orderCode { get; set; }

        public string customerName { get; set; }
    }
}
