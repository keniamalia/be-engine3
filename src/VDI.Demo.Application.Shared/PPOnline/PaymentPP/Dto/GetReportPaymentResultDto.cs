﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PPOnline.PaymentPP.Dto
{
    public class GetReportPaymentResultDto
    {
        public string year { get; set; }

        public string yearMonth { get; set; }

        public string paymentStatus { get; set; }

        public DateTime paymentDate { get; set; }

        public DateTime orderDate { get; set; }

        public DateTime? clearDate { get; set; }

        public string bankName { get; set; }

        public string customerName { get; set; }

        public string paymentTypeName { get; set; }

        public string orderCode { get; set; }

        public string PPNo { get; set; }

        public string accNo { get; set; }

        public string accName { get; set; }

        public string bankBranch { get; set; }

        public double amount { get; set; }

        public int bankID { get; set; }

        public int paymentStatusID { get; set; }

        public int paymentType { get; set; }

        public string memberName { get; set; }
    }
}
