﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PPOnline.PaymentPP.Dto
{
    public class GenerateTTBFResultDto
    {
        public string url { get; set; }
        public string path { get; set; }
    }
}
