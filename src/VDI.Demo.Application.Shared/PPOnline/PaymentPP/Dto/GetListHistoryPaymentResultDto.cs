﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PPOnline.PaymentPP.Dto
{
    public class GetListHistoryPaymentResultDto
    {
        public int orderStatusID { get; set; }

        public int paymentType { get; set; }

        public int projectID { get; set; }

        public string customerName { get; set; }

        public string orderCode { get; set; }

        public string projectName { get; set; }

        public string TID { get; set; }

        public string MID { get; set; }

        public string approvalCode { get; set; }

        public string paymentTypeName { get; set; }

        public string bankBranch { get; set; }

        public string bankAccNo { get; set; }

        public int qty { get; set; }

        public double paymentAmt { get; set; }

        public int paymentID { get; set; }

        public string psCode { get; set; }

        public string orderStatus { get; set; }

        public int PPOrderID { get; set; }

    }
}
