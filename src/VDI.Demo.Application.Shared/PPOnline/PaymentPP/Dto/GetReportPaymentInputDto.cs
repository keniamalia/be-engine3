﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PPOnline.PaymentPP.Dto
{
    public class GetReportPaymentInputDto
    {
        public string paymentDateFrom { get; set; }

        public string paymentDateTo { get; set; }

        public string orderDateFrom { get; set; }

        public string orderDateTo { get; set; }

        public string clearDateFrom { get; set; }

        public string clearDateTo { get; set; }

        public int[] orderStatusID { get; set; }

        public int[] paymentType { get; set; }

        public int[] bankID { get; set; }

        public string keyword { get; set; }
    }
}
