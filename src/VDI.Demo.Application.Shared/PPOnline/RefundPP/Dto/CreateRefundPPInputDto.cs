﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PPOnline.RefundPP.Dto
{
    public class CreateRefundPPInputDto
    {
        public int PPNoID { get; set; }

        public int refundReasonID { get; set; }
        
        public string namaBank { get; set; }

        public string noRek { get; set; }

        public string namaRek { get; set; }

        public string refundReason { get; set; }

        public string customerName { get; set; }

        public string customerCode { get; set; }

        public string memberName { get; set; }
    }
}
