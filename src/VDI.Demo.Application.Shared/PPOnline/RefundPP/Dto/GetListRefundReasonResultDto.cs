﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PPOnline.RefundPP.Dto
{
    public class GetListRefundReasonResultDto
    {
        public int refundReasonID { get; set; }
        
        public string refundReasonName { get; set; }
    }
}
