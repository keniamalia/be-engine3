﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PPOnline.RefundPP.Dto
{
    public class ResultMessageDto
    {
        public string message { get; set; }

        public bool result { get; set; }
    }
}
