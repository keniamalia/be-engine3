﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PPOnline.RefundPP.Dto
{
    public class GetListRefundHistoryResultDto
    {
        public string PPNumber { get; set; }
        public string clientCode { get; set; }
        public string memberName { get; set; }
        public string salesChannel { get; set; }
        public string refundReason { get; set; }
        public int PPRefundID { get; set; }
    }
}
