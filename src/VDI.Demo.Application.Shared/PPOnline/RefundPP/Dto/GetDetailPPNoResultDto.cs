﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PPOnline.RefundPP.Dto
{
    public class GetDetailPPNoResultDto
    {
        public string customerName { get; set; }

        public string customerCode { get; set; }

        public string scmCode { get; set; }

        public string memberCode { get; set; }

        public string memberName { get; set; }

        public string PPNo { get; set; }

        public string namaBank { get; set; }
        public string noRek { get; set; }
        public string namaRek { get; set; }
        public string refundReason { get; set; }
        public string PPStatus { get; set; }

        public int PPNoID { get; set; }

        public decimal? PPPrice { get; set; }

        public UnitInfoDto unitInfo { get; set; }
    }

    public class UnitInfoDto
    {
        public string unitCode { get; set; }
        public string unitNo { get; set; }
        public DateTime? cancelDate { get; set; }
        public DateTime bookDate { get; set; }
    }
}
