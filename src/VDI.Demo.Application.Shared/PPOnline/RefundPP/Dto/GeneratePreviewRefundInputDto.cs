﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PPOnline.RefundPP.Dto
{
    public class GeneratePreviewRefundInputDto
    {
        public string noForm { get; set; }
        public string tanggal { get; set; }
        public string customerName { get; set; }
        public string customerCode { get; set; }
        public string PPNumber { get; set; }
        public string tglCetak { get; set; }
        public string refundReason { get; set; }
        public string tglTransfer { get; set; }
        public string bankName { get; set; }
        public string noRek { get; set; }
        public string rekName { get; set; }
        public string paymentType { get; set; }
    }
}
