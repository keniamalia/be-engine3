﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PPOnline.CustomerPP.Dto
{
    public class GetListCustomerInputDto
    {
        public string customerName { get; set; }

        public string member { get; set; }

        public string keyword { get; set; }
    }
}
