﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.PPOnline.CustomerPP.Dto
{
    public class GetListCustomerResultDto
    {
        public string customerCode { get; set; }

        public string accountStatus { get; set; }

        public DateTime? activeDate { get; set; }

        public string customerName { get; set; }

        public string birthInfo { get; set; }

        public string city { get; set; }

        public string address { get; set; }

        public string phoneNumber { get; set; }

        public string email { get; set; }

        public string memberCode { get; set; }

        public string memberName { get; set; }

        public string NPWP { get; set; }

        public string regDate { get; set; }

    }
}
