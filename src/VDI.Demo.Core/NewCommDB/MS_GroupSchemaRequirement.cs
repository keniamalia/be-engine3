﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.NewCommDB
{
    [Table("MS_GroupSchemaRequirement")]
    public class MS_GroupSchemaRequirement : Entity<string>
    {
        [NotMapped]
        public override string Id
        {
            get
            {
                return entityCode +
                    "-" + reqNo +
                    "-" + scmCode;
            }
            set { /* nothing */ }
        }

        [Key]
        [Column(Order = 0)]
        [StringLength(1)]
        public string entityCode { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(3)]
        public string scmCode { get; set; }

        [Key]
        public Byte reqNo { get; set; }
        
        [Key]
        [Required]
        [StringLength(4)]
        public string groupSchemaCode { get; set; }

        [Required]
        [StringLength(40)]
        public string reqDesc { get; set; }

        [Required]
        public double pctPaid { get; set; }

        [Required]
        public double orPctPaid { get; set; }

        public DateTime modifTime { get; set; }

        [Required]
        [StringLength(40)]
        public string modifUN { get; set; }

        public DateTime inputTime { get; set; }

        [Required]
        [StringLength(40)]
        public string inputUN { get; set; }
    }
}
