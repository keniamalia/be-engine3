﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.NewCommDB
{
    [Table("MS_GroupSchema")]
    public class MS_GroupSchema : Entity<string>
    {
        [NotMapped]
        public override string Id
        {
            get
            {
                return projectCode +
                    "-" + clusterCode +
                    "-" + scmCode;
            }
            set { /* nothing */ }
        }

        [Key]
        [Column(Order = 0)]
        [StringLength(1)]
        public string entityCode { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(3)]
        public string scmCode { get; set; }

        [Key]
        [Required]
        [StringLength(4)]
        public string groupSchemaCode { get; set; }

        [Required]
        [StringLength(50)]
        public string groupSchemaName { get; set; }

        public bool isActive { get; set; }

        public bool isStandard { get; set; }

        public DateTime validFrom { get; set; }

        public string documentGrouping { get; set; }

        [Required]
        [StringLength(5)]
        public string projectCode { get; set; }

        [Required]
        [StringLength(5)]
        public string clusterCode { get; set; }

        public DateTime modifTime { get; set; }

        [Required]
        [StringLength(40)]
        public string modifUN { get; set; }

        public DateTime inputTime { get; set; }

        [Required]
        [StringLength(40)]
        public string inputUN { get; set; }       

    }
}
