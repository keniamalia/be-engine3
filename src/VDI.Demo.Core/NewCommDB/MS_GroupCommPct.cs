﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.NewCommDB
{
    [Table("MS_GroupCommPct")]
    public class MS_GroupCommPct : Entity<string>
    {
        [NotMapped]
        public override string Id
        {
            get
            {
                return entityCode +
                    "-" + asUplineNo +
                    "-" + scmCode;
            }
            set { /* nothing */ }
        }

        [Key]
        [Column(Order = 0)]
        [StringLength(1)]
        public string entityCode { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(3)]
        public string scmCode { get; set; }

        [Key]
        public Byte asUplineNo { get; set; }

        [Key]
        [Required]
        [StringLength(4)]
        public string groupSchemaCode { get; set; }

        [Required]
        public DateTime validDate { get; set; }

        [Required]
        [Column(TypeName = "money")]
        public decimal minAmt { get; set; }

        [Required]
        [Column(TypeName = "money")]
        public decimal maxAmt { get; set; }

        [Required]
        public double? commPctPaid { get; set; }

        [Required]
        public double commPctHold { get; set; }
        
        [Column(TypeName = "money")]
        public decimal? nominal { get; set; }
        
        public string statusCode { get; set; }
        
        public string commTypeCode { get; set; }

        public DateTime modifTime { get; set; }

        [Required]
        [StringLength(40)]
        public string modifUN { get; set; }

        public DateTime inputTime { get; set; }

        [Required]
        [StringLength(40)]
        public string inputUN { get; set; }

    }
}
