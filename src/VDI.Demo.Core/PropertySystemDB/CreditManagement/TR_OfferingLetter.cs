﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Abp.Domain.Entities.Auditing;
using VDI.Demo.PropertySystemDB.LippoMaster;

namespace VDI.Demo.PropertySystemDB.CreditManagement
{
    [Table("TR_OfferingLetter")]
    public class TR_OfferingLetter : AuditedEntity
    {
        [ForeignKey("TR_SLIKCheckingDetail")]
        public int slikCheckingDetailID { get; set; }
        public virtual TR_SLIKCheckingDetail TR_SLIKCheckingDetail { get; set; }

        [ForeignKey("MS_TenorParameter")]
        public int? tenor { get; set; }
        public virtual MS_TenorParameter MS_TenorParameter { get; set; }

        public int? bankBranch { get; set; }

        [Column(TypeName="money")]
        public decimal plafondRequest { get; set; }

        [Column(TypeName = "money")]
        public decimal? plafondApproved { get; set; }        

        public decimal? bankRate { get; set; }

        [StringLength(150)]
        public string remark { get; set; }
        
        [StringLength(200)]
        public string offeringLetterDoc { get; set; }

        public decimal dpPercentage { get; set; }
        
        [StringLength(350)]
        public string offeringLetterUrl { get; set; }

        public bool? isActive { get; set; }
        
        public string userType { get; set; }

        public virtual ICollection<TR_CN> TR_CN { get; set; }

        public virtual ICollection<TR_SI> TR_SI { get; set; }
    }
}
