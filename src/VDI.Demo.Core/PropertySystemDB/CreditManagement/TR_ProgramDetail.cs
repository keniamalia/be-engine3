﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.CreditManagement
{
    [Table("TR_ProgramDetail")]
    public class TR_ProgramDetail : AuditedEntity
    {
        [ForeignKey("TR_AgreementDetail")]
        public int agreementDetailID { get; set; }
        public virtual TR_AgreementDetail TR_AgreementDetail { get; set; }

        public int? tenor { get; set; }
        public decimal? bankRate { get; set; }
        public int? fix { get; set; }
    }
}
