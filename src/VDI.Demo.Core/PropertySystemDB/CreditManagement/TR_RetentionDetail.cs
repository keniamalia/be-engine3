﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.CreditManagement
{
    [Table("TR_RetentionDetail")]
    public class TR_RetentionDetail : AuditedEntity
    {
        [ForeignKey("TR_AgreementDetail")]
        public int agreementDetailID { get; set; }
        public virtual TR_AgreementDetail TR_AgreementDetail { get; set; }

        [ForeignKey("MS_Retention")]
        public int? retentionID { get; set; }
        public virtual MS_Retention MS_Retention { get; set; }

        public int? sortNo { get; set; }
        public decimal? percentage { get; set; }

        public virtual ICollection<TR_UpdateRetention> TR_UpdateRetention { get; set; }
    }
}
