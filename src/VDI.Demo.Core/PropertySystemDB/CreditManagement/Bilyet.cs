﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Abp.Domain.Entities.Auditing;
using VDI.Demo.PropertySystemDB.LippoMaster;

namespace VDI.Demo.PropertySystemDB.CreditManagement
{
    [Table("Bilyet")]
    public class Bilyet : AuditedEntity
    {
        [StringLength(50)]
        public string key { get; set; }

        [ForeignKey("TR_UpdateRetention")]
        public int updateRetentionID { get; set; }
        public virtual TR_UpdateRetention TR_UpdateRetention { get; set; }
        
        [StringLength(15)]
        public string bilyetNo { get; set; }
        
        [StringLength(200)]
        public string bilyetDocumentName { get; set; }
        
        [StringLength(350)]
        public string bilyetDocumentUrl { get; set; }

        public DateTime? dueDate { get; set; }

        public bool? isActive { get; set; }

        [Required]
        public string userTypeCreation { get; set; }

        [Required]
        public string userTypeModification { get; set; }

    }
}
