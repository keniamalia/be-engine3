﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Abp.Domain.Entities.Auditing;
using VDI.Demo.PropertySystemDB.LippoMaster;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;

namespace VDI.Demo.PropertySystemDB.CreditManagement
{
    [Table("Akad")]
    public class Akad : AuditedEntity
    {
        [ForeignKey("TR_SLIKCheckingDetail")]
        public int slikCheckingDetailID { get; set; }
        public virtual TR_SLIKCheckingDetail TR_SLIKCheckingDetail { get; set; }

        [Required]
        [StringLength(15)]
        public string akadNo { get; set; }

        public DateTime akadDate { get; set; }

        [StringLength(200)]
        public string akadDocumentName { get; set; }

        [StringLength(350)]
        public string akadDocumentUrl { get; set; }

        public bool isActive { get; set; }

        [Required]
        public string userType { get; set; }


    }
}
