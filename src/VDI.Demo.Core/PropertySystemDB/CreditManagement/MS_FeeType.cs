﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.CreditManagement
{
    [Table("MS_FeeType")]
    public class MS_FeeType : AuditedEntity
    {
        [Required]
        [StringLength(100)]
        public string feeTypeName { get; set; }
        public bool isActive { get; set; }
    }
}
