﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Abp.Domain.Entities.Auditing;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;

namespace VDI.Demo.PropertySystemDB.CreditManagement
{
    [Table("TR_UpdateRetention")]
    public class TR_UpdateRetention : AuditedEntity
    {
        [ForeignKey("MS_Cluster")]
        public int clusterID { get; set; }
        public virtual MS_Cluster MS_Cluster { get; set; }

        [ForeignKey("TR_RetentionDetail")]
        public int retentionDetailID { get; set; }
        public virtual TR_RetentionDetail TR_RetentionDetail { get; set; }

        [StringLength(8)]
        public string unitCode { get; set; }
        
        [StringLength(8)]
        public string unitNumber { get; set; }

        [Required]
        [StringLength(8)]
        public string buildingType { get; set; }

        public DateTime finishDate { get; set; }

        [Required]
        [StringLength(200)]
        public string docName { get; set; }

        [Required]
        [StringLength(350)]
        public string docUrl { get; set; }

        [Required]
        [StringLength(100)]
        public string status { get; set; }

        public bool isActive { get; set; }

        [Required]
        public string userType { get; set; }

        public virtual ICollection<Bilyet> Bilyet { get; set; }
    }
}
