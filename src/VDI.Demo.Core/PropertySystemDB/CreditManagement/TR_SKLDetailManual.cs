﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Abp.Domain.Entities.Auditing;

namespace VDI.Demo.PropertySystemDB.CreditManagement
{
    [Table("TR_SKLDetailManual")]
    public class TR_SKLDetailManual : AuditedEntity
    {
        [ForeignKey("TR_SKL")]
        public int sklID { get; set; }
        public virtual TR_SKL TR_SKL { get; set; }

        public int? totalPayment { get; set; }

        public DateTime? paymentDate { get; set; }
    }
}
