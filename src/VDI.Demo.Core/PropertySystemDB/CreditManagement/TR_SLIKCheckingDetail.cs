﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Abp.Domain.Entities.Auditing;

namespace VDI.Demo.PropertySystemDB.CreditManagement
{
    [Table("TR_SLIKCheckingDetail ")]
    public class TR_SLIKCheckingDetail : AuditedEntity
    {
        [ForeignKey("TR_SLIKChecking")]
        public int slikCheckingID { get; set; }
        public virtual TR_SLIKChecking TR_SLIKChecking { get; set; }

        [ForeignKey("TR_BankDetail")]
        public int bankDetailID { get; set; }
        public virtual TR_BankDetail TR_BankDetail { get; set; }

        [ForeignKey("MS_Collectability")]
        public int collectabilityID { get; set; }
        public virtual MS_Collectability MS_Collectability { get; set; }

        public DateTime? slikCheckingDate { get; set; }

        public bool? slikStatus { get; set; }

        [StringLength(50)]
        public string remarks { get; set; }        

        public DateTime? expiredDate{ get; set; }

        public bool isChange { get; set; }

        public bool isActive { get; set; }

        [Required]
        public string userType { get; set; }

        public virtual ICollection<TR_SLIKCheckingHistory> TR_SLIKCheckingHistory { get; set; }

        public virtual ICollection<Akad> Akad { get; set; }

        public virtual ICollection<TR_OfferingLetter> TR_OfferingLetter { get; set; }
    }
}
