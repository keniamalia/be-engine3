﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;

namespace VDI.Demo.PropertySystemDB.CreditManagement
{
    [Table("MS_UserAccountBank")]
    public class MS_UserAccountBank : AuditedEntity
    {
        public int userID { get; set; }

        [ForeignKey("TR_BankDetail")]
        public int bankDetailID { get; set; }
        public virtual TR_BankDetail TR_BankDetail { get; set; }

        [ForeignKey("MS_Cluster")]
        public int clusterID { get; set; }
        public virtual MS_Cluster MS_Cluster { get; set; }
        
        [ForeignKey("MS_BankBranch")]
        public int bankBranchID { get; set; }
        public virtual MS_BankBranch MS_BankBranch { get; set; }

        [Required]
        [StringLength(30)]
        public string picName { get; set; }
        
        [StringLength(20)]
        public string position { get; set; }

        [Required]
        [StringLength(20)]
        public string phoneNo { get; set; }

        [Required]
        [StringLength(20)]
        public string officePhone { get; set; }

        public bool status { get; set; }

        public bool verified { get; set; }

        [Required]
        [StringLength(200)]
        public string link { get; set; }

        public bool isActive { get; set; }
    }
}
