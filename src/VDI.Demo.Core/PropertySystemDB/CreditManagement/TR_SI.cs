﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Abp.Domain.Entities.Auditing;
using VDI.Demo.PropertySystemDB.LippoMaster;

namespace VDI.Demo.PropertySystemDB.CreditManagement
{
    [Table("TR_SI")]
    public class TR_SI : AuditedEntity
    {
        [ForeignKey("TR_OfferingLetter")]
        public int offeringLetterID { get; set; }
        public virtual TR_OfferingLetter TR_OfferingLetter { get; set; }
        
        [StringLength(200)]
        public string SIDocName { get; set; }
        
        [StringLength(350)]
        public string SIDocUrl { get; set; }
        
        [StringLength(10)]
        public string statusGenerate { get; set; }
        
        [StringLength(20)]
        public string generateTo { get; set; }

        [StringLength(100)]
        public string message { get; set; }

        public bool? isActive { get; set; }
    }
}
