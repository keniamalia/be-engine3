﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;

namespace VDI.Demo.PropertySystemDB.CreditManagement
{
    [Table("TR_CreditAgreement")]
    public class TR_CreditAgreement : AuditedEntity
    {
        [ForeignKey("MS_Project")]
        public int projectID { get; set; }
        public virtual MS_Project MS_Project { get; set; }

        [ForeignKey("MS_AgreementType")]
        public int agreementTypeID { get; set; }
        public virtual MS_AgreementType MS_AgreementType { get; set; }

        [ForeignKey("MS_Bank")]
        public int bankID { get; set; }
        public virtual MS_Bank MS_Bank { get; set; }

        [Required]
        [StringLength(50)]
        public string agreementDocsNo { get; set; }
        public DateTime agreementDate { get; set; }
        public DateTime agreementStart { get; set; }
        public DateTime agreementEnd { get; set; }
        public decimal agreementRate { get; set; }
        [Required]
        [StringLength(200)]
        public string agreementDocsName { get; set; }
        [Required]
        public string agreementDocsUrl { get; set; }
        public bool isActive { get; set; }
        public ICollection<TR_BankDetail> TR_BankDetail { get; set; }
    }
}
