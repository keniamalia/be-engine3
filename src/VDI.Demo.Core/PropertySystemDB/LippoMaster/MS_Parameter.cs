﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    [Table("MS_Parameter")]
    public class MS_Parameter : AuditedEntity
    {
        [Required]
        [StringLength(5)]
        public string code { get; set; }

        [Required]
        public string description { get; set; }

        [Required]
        [StringLength(50)]
        public string value { get; set; }

        [Required]
        [StringLength(30)]
        public string dataType { get; set; }

        public bool isActive { get; set; }
    }
}
