﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    [Table("TR_PaymentBulkVA")]
    public class TR_PaymentBulkVA : AuditedEntity
    {
        [Required]
        [StringLength(150)]
        public string BulkVAKey { get; set; }

        public DateTime? TanggalTransaksi { get; set; }

        [StringLength(50)]
        public string JamTransaksi { get; set; }

        public DateTime? TanggalMutasi { get; set; }

        [StringLength(50)]
        public string VirtualAccount { get; set; }

        [StringLength(150)]
        public string NamaVA { get; set; }

        [Column(TypeName = "money")]
        public decimal? NilaiTransaksi { get; set; }

        [StringLength(200)]
        public string JenisTransaksi { get; set; }

        [StringLength(50)]
        public string NomorReferensi { get; set; }

        [StringLength(500)]
        public string Keterangan { get; set; }
    }
}
