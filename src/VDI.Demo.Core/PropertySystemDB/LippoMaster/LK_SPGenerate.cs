﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    [Table("LK_SPGenerate")]
    public class LK_SPGenerate : AuditedEntity
    {
        [Required]
        [StringLength(50)]
        public string SPGenerateDesc { get; set; }

        public ICollection<MS_SPPeriod> MS_SPPeriod { get; set; }
    }
}
