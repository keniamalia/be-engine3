﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    [Table("SYS_ClosingDaily")]
    public class SYS_ClosingDaily : AuditedEntity
    {
        public int entityID { get; set; }

        [Required]
        [StringLength(50)]
        public string closeBy { get; set; }

        [ForeignKey("MS_Account")]
        public int accountID { get; set; }
        public virtual MS_Account MS_Account { get; set; }

        public DateTime closeDate { get; set; }
    }
}
