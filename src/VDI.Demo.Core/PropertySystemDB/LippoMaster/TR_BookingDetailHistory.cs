﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    [Table("TR_BookingDetailHistory")]
    public class TR_BookingDetailHistory : AuditedEntity
    {
        public int entityID { get; set; }

        public int bookingHeaderID { get; set; }

        public short refNo { get; set; }

        public int bookingTrTypeID { get; set; }
        
        public int itemID { get; set; }

        [Required]
        [StringLength(5)]
        public string coCode { get; set; }

        public int bookNo { get; set; }

        [Column(TypeName = "money")]
        public decimal BFAmount { get; set; }

        [Column(TypeName = "money")]
        public decimal amount { get; set; }

        public double pctDisc { get; set; }

        public double pctTax { get; set; }

        public double area { get; set; }
        
        public int finTypeID { get; set; }

        [Required]
        [StringLength(1)]
        public string combineCode { get; set; }

        [Column(TypeName = "money")]
        public decimal amountComm { get; set; }

        [Column(TypeName = "money")]
        public decimal netPriceComm { get; set; }

        [Column(TypeName = "money")]
        public decimal amountMKT { get; set; }

        [Column(TypeName = "money")]
        public decimal netPriceMKT { get; set; }

        [Column(TypeName = "money")]
        public decimal netPriceCash { get; set; }

        [Column(TypeName = "money")]
        public decimal netPrice { get; set; }

        [Column(TypeName = "money")]
        public decimal netNetPrice { get; set; }

        [Column(TypeName = "money")]
        public decimal adjPrice { get; set; }

        public double adjArea { get; set; }

        public short historyNo { get; set; }

    }
}
