﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;

namespace VDI.Demo.PropertySystemDB.LippoMaster
{
    [Table("TR_InventoryHeader")]
    public class TR_InventoryHeader : AuditedEntity
    {
        public int entityID { get; set; }

        [ForeignKey("MS_Account")]
        public int accID { get; set; }
        public virtual MS_Account MS_Account { get; set; }

        [Required]
        [StringLength(20)]
        public string transNo { get; set; }

        [ForeignKey("TR_BookingHeader")]
        public int bookingHeaderID { get; set; }
        public virtual TR_BookingHeader TR_BookingHeader { get; set; }

        public DateTime TTBGDate { get; set; }

        [Required]
        [StringLength(200)]
        public string ket { get; set; }

        public virtual ICollection<TR_InventoryDetail> TR_InventoryDetail { get; set; }
    }
}
