﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;

namespace VDI.Demo.PropertySystemDB.OnlineBooking.ProjectInfo
{
    [Table("MS_ProjectOLBooking")]
    public class MS_ProjectOLBooking : AuditedEntity
    {
        
        [ForeignKey("MS_Project")]
        public int projectID { get; set; }
        public virtual MS_Project MS_Project { get; set; }

        [ForeignKey("MS_Cluster")]
        public int clusterID { get; set; }
        public virtual MS_Cluster MS_Cluster { get; set; }

        [StringLength(100)]
        public string projectName { get; set; }

        [StringLength(100)]
        public string clusterName { get; set; }

        [StringLength(1000)]
        public string projectDesc { get; set; }

        [StringLength(1000)]
        public string imgLogo { get; set; }

        [StringLength(200)]
        public string handOverPeriod { get; set; }

        [StringLength(200)]
        public string graceOverPeriod { get; set; }

        public bool? isActive { get; set; }

        public DateTime? activeFrom { get; set; }

        public DateTime? activeTo { get; set; }

        public bool? isRequiredPP { get; set; }

        [ForeignKey("MS_ProjectInfo")]
        public int? projectInfoID { get; set; }
        public virtual MS_ProjectInfo MS_ProjectInfo { get; set; }
    }
}
