﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;
namespace VDI.Demo.PropertySystemDB.OnlineBooking.ProjectInfo
{
    public class MS_ProjectSocialMedia : AuditedEntity
    {
        [Required]
        [StringLength(50)]
        public string socialMediaName { get; set; }

        [Required]
        [StringLength(100)]
        public string socialMediaIcon { get; set; }

        public bool? isActive { get; set; }

        public virtual ICollection<TR_ProjectSocialMedia> TR_ProjectSocialMedia { get; set; }
    }
}
