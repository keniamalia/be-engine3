﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;

namespace VDI.Demo.PropertySystemDB.OnlineBooking.PropertySystem
{
    [Table("MS_Notaris")]
    public class MS_Notaris : AuditedEntity
    {
        [ForeignKey("MS_Project")]
        public int projectID { get; set; }
        public virtual MS_Project MS_Project { get; set; }

        [ForeignKey("MS_Cluster")]
        public int clusterID { get; set; }
        public virtual MS_Cluster MS_Cluster { get; set; }

        [StringLength(50)]
        public string businessGroup { get; set; }

        [StringLength(150)]
        public string kawasan { get; set; }

        [StringLength(100)]
        public string namaNotaris { get; set; }

        [StringLength(250)]
        public string alamat { get; set; }

        [StringLength(100)]
        public string phoneNo { get; set; }

        [StringLength(100)]
        public string email { get; set; }
    }
}
