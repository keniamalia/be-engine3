﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.MasterPlan.Unit
{
    [Table("MS_ClusterPenalty")]
    public class MS_ClusterPenalty : AuditedEntity
    {
        public int entityID { get; set; }

        public int penaltyBaseRate { get; set; }

        public double penaltyRate { get; set; }

        public DateTime bookingPeriodStart { get; set; }

        public DateTime bookingPeriodEnd { get; set; }

        public bool isAutoWave { get; set; }

        public int autoWavePeriod { get; set; }

        [Required]
        [StringLength(8)]
        public string penaltyFreq { get; set; }

        [Required]
        [StringLength(100)]
        public string penaltyDocName { get; set; }

        [Required]
        [StringLength(200)]
        public string penaltyDocUrl { get; set; }

        [ForeignKey("MS_Cluster")]
        public int clusterID { get; set; }
        public virtual MS_Cluster MS_Cluster { get; set; }
    }
}
