﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PropertySystemDB.MasterPlan.Unit
{
    [Table("LK_CertCode")]
    public class LK_CertCode : AuditedEntity
    {
        //unique
        [Required]
        [StringLength(1)]
        public string certCode { get; set; }

        [Required]
        [StringLength(50)]
        public string certDesc { get; set; }
    }
}
