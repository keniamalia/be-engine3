﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace VDI.Demo.PPOnlineDB
{
    public class LK_CategoryType : AuditedEntity
    {
        public int categoryType { get; set; }

        [Required]
        [StringLength(50)]
        public string categoryTypeName { get; set; }

        public virtual ICollection<LK_CategoryTypePreferred> LK_CategoryTypePreferred { get; set; }

        public virtual ICollection<MS_Projects> MS_Projects { get; set; }
    }
}
