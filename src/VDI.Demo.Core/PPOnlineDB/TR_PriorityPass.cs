﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PPOnlineDB
{
    public class TR_PriorityPass : AuditedEntity
    {
        [Required]
        [StringLength(6)]
        public string PPNo { get; set; }

        public int batchSeq { get; set; }

        [Required]
        [StringLength(8)]
        public string psCode { get; set; }

        [Required]
        [StringLength(3)]
        public string scmCode { get; set; }

        [Required]
        [StringLength(12)]
        public string memberCode { get; set; }

        [StringLength(200)]
        public string idCard { get; set; }

        public DateTime? regTime { get; set; }

        public DateTime? dealingTime { get; set; }

        [ForeignKey("LK_PPStatus")]
        public int PPStatusID { get; set; }
        public virtual LK_PPStatus LK_PPStatus { get; set; }

        [StringLength(20)]
        public string paymentType { get; set; }

        [StringLength(30)]
        public string cardNo { get; set; }

        [StringLength(20)]
        public string bank { get; set; }

        public DateTime buyDate { get; set; }

        [StringLength(50)]
        public string kamar { get; set; }

        [StringLength(50)]
        public string lantai { get; set; }

        [StringLength(50)]
        [Required]
        public string KPA { get; set; }

        [StringLength(6)]
        [Required]
        public string oldPPNo { get; set; }

        public int? tableNo { get; set; }

        public int? idSetting { get; set; }

        public int? token { get; set; }

        [StringLength(300)]
        public string remarks { get; set; }

        [StringLength(50)]
        public string inputFrom { get; set; }

        [ForeignKey("MS_BatchEntry")]
        public int batchID { get; set; }
        public virtual MS_BatchEntry MS_BatchEntry { get; set; }

        public virtual ICollection<TR_PriorityPassRefund> TR_PriorityPassRefund { get; set; }
    }
}
