﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace VDI.Demo.PPOnlineDB
{
    public class LK_RefundReason : AuditedEntity
    {
        [Required]
        [StringLength(1)]
        public string refundReason { get; set; }

        [Required]
        [StringLength(50)]
        public string refundReasonName { get; set; }

        public virtual ICollection<TR_PriorityPassRefund> TR_PriorityPassRefund { get; set; }
    }
}
