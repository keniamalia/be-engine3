﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PPOnlineDB
{
    [Table("TR_PPOrderPPNo")]
    public class TR_PPOrderPPNo : AuditedEntity
    {
        [Required]
        [StringLength(6)]
        public string PPNo { get; set; }

        [ForeignKey("TR_PPOrder")] //belongsTo
        public int PPOrderID { get; set; }
        public virtual TR_PPOrder TR_PPOrder { get; set; }
    }
}
