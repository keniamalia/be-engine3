﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PPOnlineDB
{
    [Table("TR_PPOrder")]
    public class TR_PPOrder : AuditedEntity
    {

        [Required][StringLength(20)]
        public string orderCode { get; set; }

        [Required]
        public int userRef { get; set; }
        
        [ForeignKey("MS_Projects")]
        public int projectID { get; set; }
        public virtual MS_Projects MS_Projects { get; set; }

        [Required]
        public int preferredType { get; set; }

        [ForeignKey("LK_TermRef")]
        public int termRefID { get; set; }
        public virtual LK_TermRef LK_TermRef { get; set; }

        [Required]
        public int qty { get; set; }

        [Required][StringLength(500)]
        public string noted { get; set; }

        [ForeignKey("LK_OrderStatus")] //belongsTo
        public int orderStatusID { get; set; }
        public virtual LK_OrderStatus LK_OrderStatus { get; set; }

        [Required]
        public int userReinputBy { get; set; }
        
        [StringLength(8)]
        public string refferalPsCode { get; set; }
        
        public ICollection<TR_PPOrderPPNo> TR_PPOrderPPNo { get; set; }

        public ICollection<TR_Payment> TR_Payment { get; set; }
    }
}
