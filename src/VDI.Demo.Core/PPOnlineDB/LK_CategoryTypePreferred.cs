﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PPOnlineDB
{
    public class LK_CategoryTypePreferred : AuditedEntity
    {
        [ForeignKey("LK_CategoryType")]
        public int categoryTypeID { get; set; }
        public virtual LK_CategoryType LK_CategoryType { get; set; }

        public int preferredType { get; set; }

        [StringLength(30)]
        [Required]
        public string preferredName { get; set; }

        [ForeignKey("MS_Projects")]
        public int? projectID { get; set; }
        public virtual MS_Projects MS_Projects { get; set; }
    }
}
