﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PPOnlineDB
{
    public class MS_BatchEntry : AuditedEntity
    {
        public int batchSeq { get; set; }

        [StringLength(3)]
        [Required]
        public string batchCode { get; set; }

        public int batchStartNum { get; set; }

        public int? batchMaxNum { get; set; }

        public int maxTopupFromOldBatch { get; set; }

        public bool isTopupOnly { get; set; }

        public bool? isBookingFee { get; set; }

        public bool? isConvertOnly { get; set; }

        public int? isSellOnly { get; set; }

        [StringLength(10)]
        public string projectCode { get; set; }

        public int minToken { get; set; }

        public int maxToken { get; set; }

        [Column(TypeName = "money")]
        public decimal? priorityPassPrice { get; set; }

        [StringLength(50)]
        public string floorSize { get; set; }

        public decimal? floorSizeMath { get; set; }

        [StringLength(100)]
        public string clusterName { get; set; }

        [StringLength(10)]
        public string clusterCode { get; set; }

        public bool isRunOut { get; set; }

        public virtual ICollection<TR_PriorityPass> TR_PriorityPass { get; set; }
    }
}
