﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PPOnlineDB
{
    [Table("TR_Payment")]
    public class TR_Payment : AuditedEntity
    {
        [ForeignKey("TR_PPOrder")]
        public int PPOrderID { get; set; }
        public virtual TR_PPOrder TR_PPOrder { get; set; }

        public int paymentType { get; set; }

        public DateTime paymentDate { get; set; }

        public double paymentAmt { get; set; }

        [ForeignKey("LK_Bank")]
        public int bankID { get; set; }
        public virtual LK_Bank LK_Bank { get; set; }

        [Required]
        [StringLength(50)]
        public string bankBranch { get; set; }

        [Required]
        [StringLength(50)]
        public string bankAccName { get; set; }

        [Required]
        [StringLength(50)]
        public string bankAccNo { get; set; }

        public DateTime? clearDate { get; set; }

        [StringLength(200)]
        public string docFile { get; set; }

        //public int userRefInputBy { get; set; }

        public ICollection<TR_PaymentDetailEDC> TR_PaymentDetailEDC { get; set; }

    }
}
