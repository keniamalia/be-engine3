﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.PPOnlineDB
{
    [Table("MS_Project")]
    public class MS_Projects : AuditedEntity
    {
        [Required]
        [StringLength(5)]
        public string projectCode { get; set; }

        [Required]
        [StringLength(5)]
        public string clusterCode { get; set; }

        [Required]
        [StringLength(50)]
        public string projectName { get; set; }

        [Required]
        [StringLength(500)]
        public string projectDesc { get; set; }

        [StringLength(200)]
        public string logo { get; set; }

        [StringLength(200)]
        public string banner { get; set; }

        public DateTime? startDate { get; set; }

        public DateTime? endDate { get; set; }

        public short sortNo { get; set; }

        [ForeignKey("LK_CategoryType")]
        public int categoryTypeID { get; set; }
        public virtual LK_CategoryType LK_CategoryType { get; set; }

        public double ppAmt { get; set; }

        [StringLength(200)]
        public string termAndCondition { get; set; }

        [StringLength(500)]
        public string contactEmail { get; set; }

        [StringLength(100)]
        public string siteLink { get; set; }

        public ICollection<TR_PPOrder> TR_PPOrder { get; set; }

        public ICollection<LK_CategoryTypePreferred> LK_CategoryTypePreferred { get; set; }
    }
}
