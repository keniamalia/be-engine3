﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.AccountingDB
{
    public class SYS_JournalCounter : AuditedEntity<string>
    {
        [NotMapped]
        public override string Id
        {
            get
            {
                return accCode;
            }
            set { /* nothing */ }
        }

        [Key]
        [Column(Order = 0)]
        [StringLength(5)]
        public string accCode { get; set; }

        [Column(Order = 1)]
        [StringLength(30)]
        public string journalCode { get; set; }
    }
}
