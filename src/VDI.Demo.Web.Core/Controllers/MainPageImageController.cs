﻿using System;
using System.Drawing;
using System.IO;
using Abp.Extensions;
using Abp.Runtime.Session;
using Abp.UI;
using Abp.Web.Models;
using Org.BouncyCastle.Asn1.Ocsp;
using VDI.Demo.IO;
using VDI.Demo.OnlineBooking.Admin.Dto;
using VDI.Demo.Web.Helpers;


using System.Collections.Generic;
using System.Configuration;

using System.Drawing.Imaging;

using System.Linq;
using Abp.AspNetCore.Mvc.Authorization;

using Abp.IO.Extensions;

using Microsoft.AspNetCore.Mvc;
using VDI.Demo.Authorization.Users.Profile.Dto;


namespace VDI.Demo.Web.Controllers
{
    public class MainPageImageController : DemoControllerBase
    {

        private readonly IAppFolders _appFolders;
        private const int maxImageSize = 5242880; //5MB

        public AddImageMainPageAdminInputDto UploadMainPageImage()
        {
            try
            {
                var imageFile = Request.Form.Files.First();

                //Check input
                if (imageFile == null)
                {
                    throw new UserFriendlyException(L("Add image main page error"));
                }

                if (imageFile.Length > maxImageSize)
                {
                    throw new UserFriendlyException(L("Image_Warn_SizeLimit", AppConsts.MaxProfilPictureBytesUserFriendlyValue));
                }

                byte[] fileBytes;
                using (var stream = imageFile.OpenReadStream())
                {
                    fileBytes = stream.GetAllBytes();
                }

                if (!ImageFormatHelper.GetRawImageFormat(fileBytes).IsIn(ImageFormat.Jpeg, ImageFormat.Png, ImageFormat.Gif))
                {
                    throw new Exception("Uploaded file is not an accepted image file !");
                }

                //Delete old temp profile pictures
                AppFileHelper.DeleteFilesInFolderIfExists(_appFolders.TempFileDownloadFolder, "imageMainPage_" + AbpSession.GetUserId());

                //Save new picture
                var fileInfo = new FileInfo(imageFile.FileName);
                string ext = fileInfo.Extension.ToLower().Trim();
                if (ext.Equals(".jpg") || ext.Equals(".png") || ext.Equals(".gif") || ext.Equals(".jpeg"))
                {
                    var tempFileName = fileInfo.FullName + fileInfo.Extension;
                    var tempFilePath = Path.Combine(_appFolders.TempFileDownloadFolder, tempFileName);
                    System.IO.File.WriteAllBytes(tempFilePath, fileBytes);

                    using (var bmpImage = new Bitmap(tempFilePath))
                    {
                        return new AddImageMainPageAdminInputDto
                        {
                            ImageAlt = tempFileName,
                            ImageURL = tempFilePath,
                            FileTypeId = 1
                        };
                        throw new UserFriendlyException("Uploaded file format is not correct !");
                    }
                }
                else
                {
                    throw new UserFriendlyException("Uploaded file format is not correct !");
                }
            }

            catch (Exception ex)
            {
                //return new AddImageMainPageAdminInputDto(new ErrorInfo(ex.Message));
                throw new UserFriendlyException("Upload error!");
            }

        }
    }
}
