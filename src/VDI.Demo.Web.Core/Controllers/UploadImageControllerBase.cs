﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using Abp.AspNetCore.Mvc.Authorization;
using Abp.Extensions;
using Abp.IO.Extensions;
using Abp.Runtime.Session;
using Abp.UI;
using Abp.Web.Models;
using Microsoft.AspNetCore.Mvc;
using VDI.Demo.Authorization.Users.Profile.Dto;
using VDI.Demo.IO;
using VDI.Demo.OnlineBooking.Admin.Dto;
using VDI.Demo.Web.Helpers;

namespace VDI.Demo.Web.Controllers
{
    class UploadImageControllerBase : DemoControllerBase
    {
        private readonly IAppFolders _appFolders;
        private const int MaxProfilePictureSize = 5242880; //5MB

        protected UploadImageControllerBase(IAppFolders appFolders)
        {
            _appFolders = appFolders;
        }

        public JsonResult UploadSocialMediaIcon()
        {
            try
            {
                //Check input
                if (Request.Form.Files.Count <= 0 || Request.Form.Files[0] == null)
                {
                    throw new UserFriendlyException(L("Image_Change_Error"));
                }

                var file = Request.Form.Files[0];

                if (file.Length > 5242880) //5MB.
                {
                    throw new UserFriendlyException(L("Image_Warn_SizeLimit"));
                }

                //Check file type & format
                var fileImage = Image.FromStream(file.OpenReadStream());
                var acceptedFormats = new List<ImageFormat>
                {
                    ImageFormat.Jpeg, ImageFormat.Png
                };

                if (!acceptedFormats.Contains(fileImage.RawFormat))
                {
                    throw new ApplicationException("Uploaded file is not an accepted image file !");
                }

                if (!Directory.Exists(_appFolders.TempFileDownloadFolder + "\\SocialMediaImages\\"))
                {
                    // Try to create the directory.
                    DirectoryInfo di = Directory.CreateDirectory(_appFolders.TempFileDownloadFolder + "\\SocialMediaImages\\");
                }

                byte[] fileBytes;
                using (var stream = file.OpenReadStream())
                {
                    fileBytes = stream.GetAllBytes();
                }

                //Delete old temp profile pictures
                //AppFileHelper.DeleteFilesInFolderIfExists(_appFolders.TempFileDownloadFolder+"/CompanyImage/", "companyImage_" + AbpSession.GetUserId());
                var date = DateTime.Now.ToString("yyyyMMddHHmmss");
                //Save new picture
                var fileInfo = new FileInfo(file.FileName);
                string ext = fileInfo.Extension.ToLower().Trim();
                if (ext.Equals(".jpg") || ext.Equals(".png") || ext.Equals(".gif") || ext.Equals(".jpeg"))
                {

                    var tempFileName = "socialImage_" + date + fileInfo.Extension;
                    var tempFilePath = Path.Combine(_appFolders.TempFileDownloadFolder + "\\SocialMediaImages\\", tempFileName);
                    System.IO.File.WriteAllBytes(tempFilePath, fileBytes);

                    using (var bmpImage = new Bitmap(tempFilePath))
                    {
                        return Json(new AjaxResponse(new { fileName = tempFileName, width = bmpImage.Width, height = bmpImage.Height }));
                    }
                }
                else
                {
                    throw new UserFriendlyException("Uploaded file format is not correct !");
                }
            }
            catch (Exception ex)
            {
                return Json(new AjaxResponse(new ErrorInfo(ex.Message)));
            }
        }
    }
}
