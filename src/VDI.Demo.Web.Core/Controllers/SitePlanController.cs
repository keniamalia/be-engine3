﻿using Abp.Extensions;
using Abp.IO.Extensions;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using VDI.Demo.IO;
using VDI.Demo.OnlineBooking.Admin.Dto;
using VDI.Demo.Web.Helpers;

namespace VDI.Demo.Web.Controllers
{
    class SitePlanController : DemoControllerBase
    {
        private readonly IAppFolders _appFolders;
        private const int maxImageSize = 5242880; //5MB

        public CreateSitePlanInputDto UploadSitePlanImage()
        {
            try
            {
                var imageFile = Request.Form.Files.First();

                //Check input
                if (imageFile == null)
                {
                    throw new UserFriendlyException(L("Add siteplan image error"));
                }

                if (imageFile.Length > maxImageSize)
                {
                    throw new UserFriendlyException(L("Image_Warn_SizeLimit", AppConsts.MaxProfilPictureBytesUserFriendlyValue));
                }

                byte[] fileBytes;
                using (var stream = imageFile.OpenReadStream())
                {
                    fileBytes = stream.GetAllBytes();
                }

                if (!ImageFormatHelper.GetRawImageFormat(fileBytes).IsIn(ImageFormat.Jpeg, ImageFormat.Png, ImageFormat.Gif))
                {
                    throw new Exception("Uploaded file is not an accepted image file !");
                }

                //Delete old temp picture
                //AppFileHelper.DeleteFilesInFolderIfExists(_appFolders.TempFileDownloadFolder, "socialMediaLogo_" + AbpSession.UserId());

                //Save new picture
                var fileInfo = new FileInfo(imageFile.FileName);
                string ext = fileInfo.Extension.ToLower().Trim();
                if (ext.Equals(".jpg") || ext.Equals(".png") || ext.Equals(".gif") || ext.Equals(".jpeg"))
                {
                    var tempFileName = fileInfo.FullName + fileInfo.Extension;
                    var tempFilePath = Path.Combine(_appFolders.TempFileDownloadFolder, tempFileName);
                    System.IO.File.WriteAllBytes(tempFilePath, fileBytes);

                    using (var bmpImage = new Bitmap(tempFilePath))
                    {
                        return new CreateSitePlanInputDto
                        {
                            sitePlansImageUrl = tempFilePath,
                            //socialMediaIcon = tempFilePath,
                        };
                        throw new UserFriendlyException("Uploaded file format is not correct !");
                    }
                }
                else
                {
                    throw new UserFriendlyException("Uploaded file format is not correct !");
                }
            }

            catch (Exception ex)
            {
                //return new AddImageMainPageAdminInputDto(new ErrorInfo(ex.Message));
                throw new UserFriendlyException("Upload error!");
            }
        }
    }
}
