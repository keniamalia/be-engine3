﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Visionet_Backend_NetCore.Komunikasi;

namespace VDI.Demo.Application
{
    public static class FontDataHelper
    {
        public const string StrAssembly = "VDI.Demo";

        public static byte[] TimesNewRoman
        {
            get { return LoadFontData(StrAssembly+".Fonts.times.ttf"); }
        }

        public static byte[] Arial
        {
            get { return LoadFontData(StrAssembly + ".Fonts.arial.ttf"); }
        }

        public static byte[] ArialBold
        {
            get { return LoadFontData(StrAssembly + ".Fonts.Arial-Bold.ttf"); }
        }

        public static byte[] ArialItalic
        {
            get { return LoadFontData(StrAssembly + ".Fonts.Arial-Italic.ttf"); }
        }

        public static byte[] TimesNewRomanBold
        {
            get { return LoadFontData(StrAssembly + ".Fonts.timesbd.ttf"); }
        }

        public static byte[] SegoeUI
        {
            get { return LoadFontData(StrAssembly+".Fonts.segoeui.ttf"); }
        }
        /// <summary>
        /// Gets the Segoe WP Light font.
        /// </summary>


        public static byte[] SegoeWPLight
        {
            get { return LoadFontData(StrAssembly+".Fonts.SegoeWP-Light.ttf"); }
        }

        /// <summary>
        /// Gets the Segoe WP Semilight font.
        /// </summary>
        public static byte[] SegoeWPSemilight
        {
            get { return LoadFontData(StrAssembly+".Fonts.SegoeWP-Semilight.ttf"); }
        }

        /// <summary>
        /// Gets the Segoe WP font.
        /// </summary>
        public static byte[] SegoeWP
        {
            get { return LoadFontData(StrAssembly+".Fonts.SegoeWP.ttf"); }
        }

        /// <summary>
        /// Gets the Segoe WP Semibold font.
        /// </summary>
        public static byte[] SegoeWPSemibold
        {
            get { return LoadFontData(StrAssembly+".Fonts.SegoeWP-Semibold.ttf"); }
        }

        /// <summary>
        /// Gets the Segoe WP Bold font.
        /// </summary>
        public static byte[] SegoeWPBold
        {
            get { return LoadFontData(StrAssembly+".Fonts.SegoeWP-Bold.ttf"); }
        }

        /// <summary>
        /// Gets the Segoe WP Black font.
        /// </summary>
        public static byte[] SegoeWPBlack
        {
            get { return LoadFontData(StrAssembly+".Fonts.SegoeWP-Black.ttf"); }
        }

        /// <summary>
        /// Returns the specified font from an embedded resource.
        /// </summary>
        static byte[] LoadFontData(string name)
        {
            SendConsole("load data font:"+name);
            Assembly assembly = typeof(FontDataHelper).Assembly;
            var datas = typeof(FontDataHelper).Assembly.GetManifestResourceNames();

            SendConsole("assembly:" + JsonConvert.SerializeObject(assembly.GetManifestResourceNames()));

            using (Stream stream = assembly.GetManifestResourceStream(name))
            {
                if (stream == null)
                    throw new ArgumentException("No resource with name " + name);

                var count = (int)stream.Length;
                var data = new byte[count];
                stream.Read(data, 0, count);
                return data;
            }
        }

        #region debug console
        private static void SendConsole(string msg)
        {
            if (Setting_variabel.enable_tcp_debug == true)
            {
                if (Setting_variabel.Komunikasi_TCPListener == null)
                {
                    Setting_variabel.Komunikasi_TCPListener = new Visionet_Backend_NetCore.Komunikasi.Komunikasi_TCPListener(17000);
                    Task.Run(() => StartListenerLokal());
                }

                if (Setting_variabel.Komunikasi_TCPListener != null)
                {
                    if (Setting_variabel.Komunikasi_TCPListener.IsRunning())
                    {
                        if (Setting_variabel.ConsoleBayangan != null)
                        {
                            Setting_variabel.ConsoleBayangan.Send("[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "] " + msg);
                        }
                    }
                }
            }

        }
        #endregion


        #region listener tcp debug
        private static void StartListenerLokal()
        {
            if (Setting_variabel.Komunikasi_TCPListener != null)
                Setting_variabel.Komunikasi_TCPListener.StartListener();
        }

        private static void StopListenerLokal()
        {
            if (Setting_variabel.Komunikasi_TCPListener != null)
                Setting_variabel.Komunikasi_TCPListener.StopListener();
        }
        #endregion
    }
}
