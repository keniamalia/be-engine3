﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using Abp.Extensions;
using Abp.Linq.Extensions;
using System.Linq.Dynamic.Core;
using System.Text;
using VDI.Demo.CreditManagement.CreditAgreements.Dto;
using VDI.Demo.PropertySystemDB.CreditManagement;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using VDI.Demo.Files;
using Abp.UI;
using System.IO;
using System.Text.RegularExpressions;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;
using VDI.Demo.MasterPlan.Project.MS_Banks.Dto;
using Abp.Authorization;
using VDI.Demo.Authorization;
using Newtonsoft.Json.Linq;

namespace VDI.Demo.CreditManagement.CreditAgreements
{
    public class CreditAgreementAppService : DemoAppServiceBase, ICreditAgreementAppService
    {
        private readonly IRepository<TR_CreditAgreement> _trCreditAgreementRepo;
        private readonly IRepository<LK_BankLevel> _lkBankLevelRepo;
        private readonly IRepository<MS_Project> _msProjectRepo;
        private readonly IRepository<MS_Bank> _msBankRepo;
        private readonly IRepository<TR_ProgramDetail> _trProgramDetailRepo;
        private readonly IRepository<TR_RetentionDetail> _trRetentionDetailRepo;
        private readonly IRepository<MS_Retention> _msRetentionRepo;
        private readonly IRepository<TR_BankDetail> _trBankDetailRepo;
        private readonly IRepository<TR_AgreementDetail> _trAgreementDetailRepo;
        private readonly IRepository<TR_SubsidyDetail> _trSubsidyDetailRepo;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly FilesHelper _filesHelper;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IRepository<MS_FeeType> _msFeeType;
        private readonly IRepository<MS_Cluster> _msClusterRepo;

        public CreditAgreementAppService(
            IRepository<TR_CreditAgreement> trCreditAgreementRepo,
            IRepository<LK_BankLevel> lkBankLevelRepo,
            IRepository<MS_Project> msProjectRepo,
            IRepository<MS_Bank> msBankRepo,
            IRepository<TR_ProgramDetail> trProgramDetailRepo,
            IRepository<TR_RetentionDetail> trRetentionDetailRepo,
            IRepository<MS_Retention> msRetentionRepo,
            IRepository<TR_BankDetail> trBankDetailRepo,
            IRepository<TR_AgreementDetail> trAgreementDetailRepo,
            IRepository<TR_SubsidyDetail> trSubsidyDetailRepo,
            IRepository<MS_FeeType> msFeeType,
            IRepository<MS_Cluster> msClusterRepo,
            IHttpContextAccessor httpContextAccessor,
            FilesHelper filesHelper,
            IHostingEnvironment environment
        )
        {
            _trCreditAgreementRepo = trCreditAgreementRepo;
            _lkBankLevelRepo = lkBankLevelRepo;
            _msProjectRepo = msProjectRepo;
            _msBankRepo = msBankRepo;
            _trProgramDetailRepo = trProgramDetailRepo;
            _trRetentionDetailRepo = trRetentionDetailRepo;
            _msRetentionRepo = msRetentionRepo;
            _trBankDetailRepo = trBankDetailRepo;
            _trAgreementDetailRepo = trAgreementDetailRepo;
            _msFeeType = msFeeType;
            _msClusterRepo = msClusterRepo;
            _trSubsidyDetailRepo = trSubsidyDetailRepo;
            _httpContextAccessor = httpContextAccessor;
            _filesHelper = filesHelper;
            _hostingEnvironment = environment;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CreditManagement_CreditAgreement_Create, AppPermissions.Pages_Tenant_CreditManagement_CreditAgreement_Edit)]
        public void CreateOrUpdateUniversalCreditAgreement(GetCreditAgreementViewDetail input)
        {
            Logger.DebugFormat("CreateOrUpdateUniversalCreditAgreement() - Start");

            var agreementInformation = input.agreementInformation;
            var setProgram = input.programDetail;
            var bankDetail = agreementInformation.bankDetail;
            bool getCreditAgreement;

            if (agreementInformation.creditAgreementID == 0)
            {
                Logger.DebugFormat("CreateOrUpdateUniversalCreditAgreement() - Start check existing TR_CreditAgreement {0}" +
                "   projectID       = {1}{0}" +
                "   agreementTypeID = {2}{0}" +
                "   bankID          = {3}{0}" +
                "   agreementStart  = {4}{0}" +
                "   agreementEnd    = {5}{0}"
                , Environment.NewLine, agreementInformation.projectID, agreementInformation.typeID, agreementInformation.bankID, agreementInformation.agreementStartDate);
                getCreditAgreement = (from x in _trCreditAgreementRepo.GetAll()
                                      where x.projectID == agreementInformation.projectID &&
                                      x.agreementTypeID == agreementInformation.typeID &&
                                      x.bankID == agreementInformation.bankID 
                                      //&& (x.agreementStart.Date >= agreementInformation.agreementStartDate.Date && x.agreementEnd.Date <= agreementInformation.agreementStartDate.Date)
                                      select x.Id).Any();
                Logger.DebugFormat("CreateOrUpdateUniversalCreditAgreement() - End check existing TR_CreditAgreement. Result = " + getCreditAgreement);
            }
            else
            {
                Logger.DebugFormat("CreateOrUpdateUniversalCreditAgreement() - Start check existing TR_CreditAgreement {0}" +
                "   projectID       = {1}{0}" +
                "   agreementTypeID = {2}{0}" +
                "   bankID          = {3}{0}" +
                "   agreementStart  = {4}{0}" +
                "   agreementEnd    = {5}{0}"
                , Environment.NewLine, agreementInformation.projectID, agreementInformation.typeID, agreementInformation.bankID, agreementInformation.agreementStartDate);
                getCreditAgreement = (from x in _trCreditAgreementRepo.GetAll()
                                      where x.Id != agreementInformation.creditAgreementID &&
                                      x.projectID == agreementInformation.projectID &&
                                      x.agreementTypeID == agreementInformation.typeID &&
                                      x.bankID == agreementInformation.bankID 
                                      //&& (x.agreementStart.Date >= agreementInformation.agreementStartDate.Date && x.agreementEnd.Date <= agreementInformation.agreementStartDate.Date)
                                      select x.Id).Any();
                Logger.DebugFormat("CreateOrUpdateUniversalCreditAgreement() - End check existing TR_CreditAgreement. Result = " + getCreditAgreement);
            }

            if (!getCreditAgreement)
            {
                Logger.DebugFormat("CreateOrUpdateUniversalCreditAgreement() - Start get MS_Bank {0}" +
                "   bankID          = {1}{0}"
                , Environment.NewLine, agreementInformation.bankID);
                var getBankCode = (from x in _msBankRepo.GetAll()
                                   where x.Id == agreementInformation.bankID
                                   select x.bankCode).FirstOrDefault();
                Logger.DebugFormat("CreateOrUpdateUniversalCreditAgreement() - End get MS_Bank. Result = " + getBankCode);

                Logger.DebugFormat("CreateOrUpdateUniversalCreditAgreement() - Start get MS_Project {0}" +
                "   projectID          = {1}{0}"
                , Environment.NewLine, agreementInformation.projectID);
                var getProjectCode = (from x in _msProjectRepo.GetAll()
                                      where x.Id == agreementInformation.projectID
                                      select x.projectCode).FirstOrDefault();
                Logger.DebugFormat("CreateOrUpdateUniversalCreditAgreement() - End get MS_Project. Result = " + getProjectCode);

                Logger.DebugFormat("CreateOrUpdateUniversalCreditAgreement() - Start get TR_CreditAgreement {0}" +
                "   Year = {1}{0}"
                , Environment.NewLine, DateTime.Now.Year);
                var getAgreementDocsNo = (from x in _trCreditAgreementRepo.GetAll()
                                          where x.agreementDocsNo.Split(new[] { '/' })[4].Contains(Convert.ToString(DateTime.Now.Year))
                                          select x.agreementDocsNo).FirstOrDefault();
                Logger.DebugFormat("CreateOrUpdateUniversalCreditAgreement() - End get TR_CreditAgreement. Result = " + getAgreementDocsNo);

                var agreementDocsNo = getAgreementDocsNo == null ? 1.ToString("D3") : (Convert.ToInt32(getAgreementDocsNo.Split("/")[0]) + 1).ToString("D3");

                var dataCreditAgreement = new GetAgreementInformationDto
                {
                    creditAgreementID = agreementInformation.creditAgreementID,
                    projectID = agreementInformation.projectID,
                    typeID = agreementInformation.typeID,
                    bankID = agreementInformation.bankID,
                    agreementDocsNo = agreementInformation.creditAgreementID == 0 ? agreementDocsNo + "/PKS/" + getBankCode + "/" + getProjectCode + "/" + DateTime.Now.Year : agreementInformation.agreementDocsNo, // counter
                    agreementDate = agreementInformation.agreementDate,
                    agreementStartDate = agreementInformation.agreementStartDate,
                    agreementEndDate = agreementInformation.agreementEndDate,
                    agreementRate = agreementInformation.agreementRate,
                    agreementDocsName = agreementInformation.agreementDocsName,
                    isActive = agreementInformation.isActive
                };
                Logger.DebugFormat("CreateOrUpdateUniversalCreditAgreement() - Start Create or Update TR_CreditAgreement {0}" +
                "   creditAgreementID   = {1}{0}" +
                "   projectID           = {2}{0}" +
                "   typeID              = {3}{0}" +
                "   bankID              = {4}{0}" +
                "   agreementDocsNo     = {5}{0}" +
                "   agreementDate       = {6}{0}" +
                "   agreementStartDate  = {7}{0}" +
                "   agreementEndDate    = {8}{0}" +
                "   agreementRate       = {9}{0}" +
                "   agreementDocsName   = {10}{0}" +
                "   isActive            = {11}{0}" +
                "   creditAgreementID   = {12}{0}"
                , Environment.NewLine, dataCreditAgreement.creditAgreementID, dataCreditAgreement.projectID, dataCreditAgreement.typeID,
                dataCreditAgreement.bankID, dataCreditAgreement.agreementDocsNo, dataCreditAgreement.agreementDate, dataCreditAgreement.agreementStartDate, 
                dataCreditAgreement.agreementEndDate, dataCreditAgreement.agreementRate, dataCreditAgreement.agreementDocsName, dataCreditAgreement.isActive, dataCreditAgreement.creditAgreementID);
                var creditAgreementID = CreateOrUpdateTrCreditAgreement(dataCreditAgreement);
                Logger.DebugFormat("CreateOrUpdateUniversalCreditAgreement() - End Create or Update TR_CreditAgreement. Result = " + dataCreditAgreement);

                var dataBankDetail = new GetBankDetailDto
                {
                    bankDetailID = bankDetail.bankDetailID,
                    address     = bankDetail.address,
                    pic         = bankDetail.pic,
                    phone       = bankDetail.phone,
                    email       = bankDetail.email,
                    officePhone = bankDetail.officePhone,
                    isPriority  = bankDetail.isPriority,
                    isOB        = bankDetail.isOB,
                    isPPOnline  = bankDetail.isPPOnline
                };
                Logger.DebugFormat("CreateOrUpdateUniversalCreditAgreement() - Start Create or Update TR_BankDetail {0}" +
                "   bankDetailID = {1}{0}" +
                "   address      = {2}{0}" +
                "   pic          = {3}{0}" +
                "   phone        = {4}{0}" +
                "   email        = {5}{0}" +
                "   officePhone  = {6}{0}" +
                "   isPriority   = {7}{0}" +
                "   isOB         = {8}{0}" +
                "   isPPOnline   = {9}{0}" +
                "   creditAgreementID   = {10}{0}"
                , Environment.NewLine, dataBankDetail.bankDetailID, dataBankDetail.address, dataBankDetail.pic, dataBankDetail.phone,
                dataBankDetail.email, dataBankDetail.officePhone, dataBankDetail.isPriority, dataBankDetail.isOB, dataBankDetail.isPPOnline, creditAgreementID);
                CreateOrUpdateTrBankDetail(dataBankDetail, creditAgreementID);
                Logger.DebugFormat("CreateOrUpdateUniversalCreditAgreement() - End Create or Update TR_BankDetail");

                foreach (var data in setProgram)
                {
                    Logger.DebugFormat("CreateOrUpdateUniversalCreditAgreement() - Start get TR_AgreementDetail {0}");
                    var getNumberAgremeent = (from x in _trAgreementDetailRepo.GetAll()
                                              orderby x.Id descending
                                              select new { x.addendumNo, x.programNo }).FirstOrDefault();
                    Logger.DebugFormat("CreateOrUpdateUniversalCreditAgreement() - End get TR_AgreementDetail. Result = " + getNumberAgremeent);

                    var programNo = getNumberAgremeent == null ? "1" : (getNumberAgremeent.programNo + 1).ToString();
                    var addendumNo = getNumberAgremeent == null ? 1.ToString("D3") : (Convert.ToInt32(getNumberAgremeent.addendumNo.Substring(getNumberAgremeent.addendumNo.Length - 3)) + 1).ToString("D3");

                    var dataAgreementDetail = new GetProgramDetailListDto
                    {
                        agreementDetailID   = data.agreementDetailID,
                        clusterID           = data.clusterID,
                        programNo           = data.agreementDetailID == 0 ? programNo : data.programNo, // counter
                        programName         = data.programName,
                        programStartDate    = data.programStartDate,
                        programEndDate      = data.programEndDate,
                        bankRate            = data.bankRate,
                        provisiTypeID       = data.provisiTypeID,
                        provisiFee          = data.provisiFee,
                        adminTypeID         = data.adminTypeID,
                        adminFee            = data.adminFee,
                        addendumNo          = data.agreementDetailID == 0 ? "AgreementDocsNo_ADD" + addendumNo : data.addendumNo, // counter
                        addendumDocs        = data.addendumDocs,
                        status              = data.status
                    };
                    Logger.DebugFormat("CreateOrUpdateUniversalCreditAgreement() - Start Create or Update TR_AgreementDetail {0}" +
                    "   agreementDetailID   = {1}{0}" +
                    "   clusterID           = {2}{0}" +
                    "   programNo           = {3}{0}" +
                    "   programName         = {4}{0}" +
                    "   programStartDate    = {5}{0}" +
                    "   programEndDate      = {6}{0}" +
                    "   bankRate            = {7}{0}" +
                    "   provisiTypeID       = {8}{0}" +
                    "   provisiFee          = {9}{0}" +
                    "   adminTypeID         = {10}{0}" +
                    "   adminFee            = {11}{0}" +
                    "   addendumNo          = {12}{0}" +
                    "   addendumDocs        = {13}{0}" +
                    "   status              = {14}{0}"
                    , Environment.NewLine, dataAgreementDetail.agreementDetailID, dataAgreementDetail.clusterID, dataAgreementDetail.programNo,
                    dataAgreementDetail.programName, dataAgreementDetail.programStartDate, dataAgreementDetail.programEndDate, dataAgreementDetail.bankRate, 
                    dataAgreementDetail.provisiTypeID, dataAgreementDetail.provisiFee, dataAgreementDetail.adminTypeID, dataAgreementDetail.adminFee, 
                    dataAgreementDetail.addendumNo, dataAgreementDetail.addendumDocs, dataAgreementDetail.status);

                    var agreementDetailID = CreateOrUpdateTrAgreementDetail(dataAgreementDetail, creditAgreementID);

                    Logger.DebugFormat("CreateOrUpdateUniversalCreditAgreement() - End Create or Update TR_AgreementDetail. Result = " + agreementDetailID);

                    foreach (var program in data.programRate)
                    {
                        Logger.DebugFormat("CreateOrUpdateUniversalCreditAgreement() - Start Create or Update TR_ProgramDetail {0}" +
                        "   programDetailID = {1}{0}" +
                        "   tenor           = {2}{0}" +
                        "   bankRate        = {3}{0}" +
                        "   fix             = {4}{0}" +
                        "   creditAgreementID = {5}{0}"
                        , Environment.NewLine, program.programDetailID, program.tenor, program.bankRate, program.fix, creditAgreementID);
                        Logger.DebugFormat("CreateOrUpdateUniversalCreditAgreement() - Start Create or Update TR_ProgramDetail");
                        var dataProgramDetail = new GetProgramRateListDto
                        {
                            programDetailID = program.programDetailID,
                            tenor = program.tenor,
                            bankRate = program.bankRate,
                            fix = program.fix
                        };
                        CreateOrUpdateTrProgramDetail(dataProgramDetail, agreementDetailID);
                        Logger.DebugFormat("CreateOrUpdateUniversalCreditAgreement() - End Create or Update TR_ProgramDetail");
                    }

                    foreach (var rentention in data.retentionDetail)
                    {
                        Logger.DebugFormat("CreateOrUpdateUniversalCreditAgreement() - Start Create or Update TR_ProgramDetail {0}" +
                        "   retentionDetailID = {1}{0}" +
                        "   sortNo            = {2}{0}" +
                        "   retentionID       = {3}{0}" +
                        "   percentage        = {4}{0}" +
                        "   creditAgreementID = {5}{0}"
                        , Environment.NewLine, rentention.retentionDetailID, rentention.sortNo, rentention.retentionID, rentention.percentage, creditAgreementID);
                        var dataRentention = new GetRetentionDetailListDto
                        {
                            retentionDetailID = rentention.retentionDetailID,
                            sortNo = rentention.sortNo,
                            retentionID = rentention.retentionID,
                            percentage = rentention.percentage
                        };
                        CreateOrUpdateTrRetentionDetail(dataRentention, creditAgreementID);
                        Logger.DebugFormat("CreateOrUpdateUniversalCreditAgreement() - End Create or Update TR_ProgramDetail");
                    }

                    
                    if (data.subsidy != null)
                    {
                        Logger.DebugFormat("CreateOrUpdateUniversalCreditAgreement() - Start Create or Update TR_SubsidyDetail {0}" +
                        "   subsidyDetailID     = {1}{0}" +
                        "   subsidyType         = {2}{0}" +
                        "   subsidyValue        = {3}{0}" +
                        "   remarks             = {4}{0}" +
                        "   isCalculateOnSI     = {5}{0}" +
                        "   creditAgreementID   = {6}{0}"
                        , Environment.NewLine, data.subsidy.subsidyID, data.subsidy.typeID, data.subsidy.value, data.subsidy.remarks, data.subsidy.isCalculateSI, creditAgreementID);
                        Logger.DebugFormat("CreateOrUpdateUniversalCreditAgreement() - Start Create or Update TR_SubsidyDetail");
                        CreateOrUpdateTrSubsidyDetail(data.subsidy, creditAgreementID);
                        Logger.DebugFormat("CreateOrUpdateUniversalCreditAgreement() - End Create or Update TR_SubsidyDetail");
                    }
                }
            }
            else
            {
                throw new UserFriendlyException("Agreement Information with this Project, Type and Bank Already exist.");
            }
        }

        #region Create Or Update Pecahan
        private int CreateOrUpdateTrCreditAgreement(GetAgreementInformationDto input)
        {
            try
            {
                string agreementDocsUrl;
                string module = @"CreditManagement\AgreementsDocs\";

                var getOldDocs = (from x in _trCreditAgreementRepo.GetAll()
                                  where x.Id == input.creditAgreementID
                                  select x).FirstOrDefault();

                if (input.creditAgreementID == 0 && getOldDocs == null)
                {
                    agreementDocsUrl = MoveFile(input.agreementDocsName, module);
                    GetURLWithoutHost(agreementDocsUrl, out agreementDocsUrl);
                    Logger.Info("CreateOrUpdateTrCreditAgreement Insert");
                }
                else
                {
                    Logger.Info("CreateOrUpdateTrCreditAgreement Update");
                    if ((input.agreementDocsStatus != null ? input.agreementDocsStatus.ToLower() : input.agreementDocsStatus) == "updated")
                    {
                        // updated the docs
                        var docsToDelete = getOldDocs.agreementDocsUrl;
                        DeleteFile(docsToDelete, module);

                        agreementDocsUrl = MoveFile(input.agreementDocsName, module);
                        GetURLWithoutHost(agreementDocsUrl, out agreementDocsUrl);
                    }
                    else
                    {
                        // didnt update the docs
                        agreementDocsUrl = getOldDocs.agreementDocsUrl;
                    }
                }

                var data = new TR_CreditAgreement
                {
                    Id = input.creditAgreementID,
                    projectID = input.projectID,
                    agreementTypeID = input.typeID,
                    bankID = input.bankID,
                    agreementDocsNo = input.agreementDocsNo,
                    agreementDate = input.agreementDate,
                    agreementStart = input.agreementStartDate,
                    agreementEnd = input.agreementEndDate,
                    agreementRate = input.agreementRate,
                    agreementDocsName = input.agreementDocsName,
                    agreementDocsUrl = agreementDocsUrl,
                    isActive = input.isActive
                };

                if (input.creditAgreementID != 0)
                {
                    var getTrCreditAgreement = (from x in _trCreditAgreementRepo.GetAll()
                                                where x.Id == input.creditAgreementID
                                                select x).FirstOrDefault();

                    getTrCreditAgreement.projectID = input.projectID;
                    getTrCreditAgreement.agreementTypeID = input.typeID;
                    getTrCreditAgreement.bankID = input.bankID;
                    getTrCreditAgreement.agreementDocsNo = input.agreementDocsNo;
                    getTrCreditAgreement.agreementDate = input.agreementDate;
                    getTrCreditAgreement.agreementStart = input.agreementStartDate;
                    getTrCreditAgreement.agreementEnd = input.agreementEndDate;
                    getTrCreditAgreement.agreementRate = input.agreementRate;
                    getTrCreditAgreement.agreementDocsName = input.agreementDocsName;
                    getTrCreditAgreement.agreementDocsUrl = agreementDocsUrl;
                    getTrCreditAgreement.isActive = input.isActive;

                    data = ObjectMapper.Map<TR_CreditAgreement>(getTrCreditAgreement);
                }

                var Id = _trCreditAgreementRepo.InsertOrUpdateAndGetId(data);

                return Id;
            }
            catch (Exception ex)
            {
                Logger.DebugFormat("TR_CreditAgreement CreateOrUpdate Error - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException(ex.Message);
            }
        }

        private void CreateOrUpdateTrBankDetail(GetBankDetailDto input, int creditAgreementID)
        {
            try
            {
                var data = new TR_BankDetail
                {
                    Id = input.bankDetailID,
                    address = input.address,
                    picName = input.pic,
                    phone = input.phone,
                    email = input.email,
                    officePhone = input.officePhone,
                    isPriority = input.isPriority,
                    isShowOnOB = input.isOB,
                    isShowOnPPOnline = input.isPPOnline,
                    creditAgreementID = creditAgreementID
                };

                if (input.bankDetailID != 0)
                {
                    var getTrBankDetail = (from x in _trBankDetailRepo.GetAll()
                                                where x.Id == input.bankDetailID
                                                select x).FirstOrDefault();

                   getTrBankDetail.address = input.address;
                   getTrBankDetail.picName = input.pic;
                   getTrBankDetail.phone = input.phone;
                   getTrBankDetail.email = input.email;
                   getTrBankDetail.officePhone = input.officePhone;
                   getTrBankDetail.isPriority = input.isPriority;
                   getTrBankDetail.isShowOnOB = input.isOB;
                   getTrBankDetail.isShowOnPPOnline = input.isPPOnline;
                   getTrBankDetail.creditAgreementID = creditAgreementID;

                    data = ObjectMapper.Map<TR_BankDetail>(getTrBankDetail);
                }

                _trBankDetailRepo.InsertOrUpdate(data);
            }
            catch (Exception ex)
            {
                Logger.DebugFormat("TR_BankDetail CreateOrUpdate Error - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException(ex.Message);
            }
        }

        private int CreateOrUpdateTrAgreementDetail(GetProgramDetailListDto input, int creditAgreementID)
        {
            try
            {
                bool getAgreementDetail;

                if (input.agreementDetailID == 0)
                {
                    getAgreementDetail = false;
                        //(from x in _trAgreementDetailRepo.GetAll()
                        //                  where x.clusterID == input.clusterID ||
                        //                  x.programName.ToLowerInvariant() == input.programName.ToLowerInvariant() ||
                        //                  (x.programStart.Date >= input.programStartDate.Date && x.programEnd.Date <= input.programStartDate.Date)
                        //                  select x.Id).Any();
                }
                else
                {
                    getAgreementDetail = (from x in _trAgreementDetailRepo.GetAll()
                                          where x.Id != input.agreementDetailID &&
                                          x.clusterID == input.clusterID &&
                                          x.programName.ToLowerInvariant() == input.programName.ToLowerInvariant() 
                                          //&& (x.programStart.Date >= input.programStartDate.Date && x.programEnd.Date <= input.programStartDate.Date)
                                          select x.Id).Any();
                }

                if (!getAgreementDetail)
                {
                    string addendumDocsUrl;
                    string module = @"CreditManagement\AddendumDocs\";

                    var getOldDocs = (from x in _trAgreementDetailRepo.GetAll()
                                      where x.Id == input.agreementDetailID
                                      select x).FirstOrDefault();

                    if (input.agreementDetailID == 0 && getOldDocs == null)
                    {
                        if (input.addendumDocs != null)
                        {
                            // upload new data
                            addendumDocsUrl = MoveFile(input.addendumDocs, module);
                            GetURLWithoutHost(addendumDocsUrl, out addendumDocsUrl);
                        }
                        else
                        {
                            addendumDocsUrl = input.addendumDocs;
                            Logger.Info("Without addendum docs");
                        }
                    }
                    else
                    {
                        if ((input.addendumDocsStatus != null ? input.addendumDocsStatus.ToLower() : input.addendumDocsStatus) == "updated")
                        {
                            if (getOldDocs.addendumDocsUrl != null)
                            {
                                // updated the docs
                                var docsToDelete = getOldDocs.addendumDocsUrl;
                                DeleteFile(docsToDelete, module);

                                addendumDocsUrl = MoveFile(input.addendumDocs, module);
                                GetURLWithoutHost(addendumDocsUrl, out addendumDocsUrl);
                            }
                            else
                            {
                                addendumDocsUrl = getOldDocs.addendumDocsUrl;
                            }
                        }
                        else
                        {
                            // didnt update the docs
                            addendumDocsUrl = getOldDocs.addendumDocsUrl;
                        }
                    }

                    var data = new TR_AgreementDetail
                    {
                        Id = input.agreementDetailID,
                        clusterID = input.clusterID,
                        programNo = Convert.ToInt32(input.programNo),
                        programName = input.programName,
                        programStart = input.programStartDate,
                        programEnd = input.programEndDate,
                        bankRate = input.bankRate,
                        provisiFeeType = input.provisiTypeID,
                        provisiValue = input.provisiFee,
                        adminFeeType = input.adminTypeID,
                        adminFeeValue = input.adminFee,
                        addendumNo = input.addendumNo,
                        addendumDocsName = input.addendumDocs,
                        addendumDocsUrl = addendumDocsUrl,
                        isActive = input.status,
                        creditAgreementID = creditAgreementID
                    };

                    if (input.agreementDetailID != 0)
                    {
                        var getTrAgreementDetail = (from x in _trAgreementDetailRepo.GetAll()
                                               where x.Id == input.agreementDetailID
                                               select x).FirstOrDefault();

                        getTrAgreementDetail.clusterID = input.clusterID;
                        getTrAgreementDetail.programNo = Convert.ToInt32(input.programNo);
                        getTrAgreementDetail.programName = input.programName;
                        getTrAgreementDetail.programStart = input.programStartDate;
                        getTrAgreementDetail.programEnd = input.programEndDate;
                        getTrAgreementDetail.bankRate = input.bankRate;
                        getTrAgreementDetail.provisiFeeType = input.provisiTypeID;
                        getTrAgreementDetail.provisiValue = input.provisiFee;
                        getTrAgreementDetail.adminFeeType = input.adminTypeID;
                        getTrAgreementDetail.adminFeeValue = input.adminFee;
                        getTrAgreementDetail.addendumNo = input.addendumNo;
                        getTrAgreementDetail.addendumDocsName = input.addendumDocs;
                        getTrAgreementDetail.addendumDocsUrl = addendumDocsUrl;
                        getTrAgreementDetail.isActive = input.status;
                        getTrAgreementDetail.creditAgreementID = creditAgreementID;

                        data = ObjectMapper.Map<TR_AgreementDetail>(getTrAgreementDetail);
                    }

                    var Id = _trAgreementDetailRepo.InsertOrUpdateAndGetId(data);

                    return Id;
                }
                else
                {
                    throw new UserFriendlyException("Program with this Cluster and Program Name Already exist.");
                }
            }
            catch (Exception ex)
            {
                Logger.DebugFormat("TR_AgreementDetail CreateOrUpdate Error - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException(ex.Message);
            }
        }

        private void CreateOrUpdateTrProgramDetail(GetProgramRateListDto input, int agreementDetailID)
        {
            try
            {
                var data = new TR_ProgramDetail
                {
                    Id = input.programDetailID,
                    tenor = input.tenor,
                    bankRate = input.bankRate,
                    fix = input.fix,
                    agreementDetailID = agreementDetailID
                };

                if (input.programDetailID != 0)
                {
                    var getTrProgramDetail = (from x in _trProgramDetailRepo.GetAll()
                                                where x.Id == input.programDetailID
                                                select x).FirstOrDefault();

                    getTrProgramDetail.tenor = input.tenor;
                    getTrProgramDetail.bankRate = input.bankRate;
                    getTrProgramDetail.fix = input.fix;
                    getTrProgramDetail.agreementDetailID = agreementDetailID;

                    data = ObjectMapper.Map<TR_ProgramDetail>(getTrProgramDetail);
                }
                _trProgramDetailRepo.InsertOrUpdate(data);
            }
            catch (Exception ex)
            {
                Logger.DebugFormat("TR_ProgramDetail CreateOrUpdate Error - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException(ex.Message);
            }
        }

        private void CreateOrUpdateTrRetentionDetail(GetRetentionDetailListDto input, int agreementDetailID)
        {
            try
            {
                var data = new TR_RetentionDetail
                {
                    Id = input.retentionDetailID,
                    sortNo = input.sortNo,
                    retentionID = input.retentionID,
                    percentage = input.percentage,
                    agreementDetailID = agreementDetailID
                };
                if (input.retentionDetailID != 0)
                {
                    var getTrRetentionDetail = (from x in _trRetentionDetailRepo.GetAll()
                                              where x.Id == input.retentionDetailID
                                              select x).FirstOrDefault();

                    getTrRetentionDetail.sortNo = input.sortNo;
                    getTrRetentionDetail.retentionID = input.retentionID;
                    getTrRetentionDetail.percentage = input.percentage;
                    getTrRetentionDetail.agreementDetailID = agreementDetailID;

                    data = ObjectMapper.Map<TR_RetentionDetail>(getTrRetentionDetail);
                }
                _trRetentionDetailRepo.InsertOrUpdate(data);
            }
            catch (Exception ex)
            {
                Logger.DebugFormat("TR_RetentionDetail CreateOrUpdate Error - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException(ex.Message);
            }
        }

        private void CreateOrUpdateTrSubsidyDetail(GetSubsidyDto input, int agreementDetailID)
        {
            try
            {
                var data = new TR_SubsidyDetail
                {
                    Id = input.subsidyID,
                    subsidyType = input.typeID,
                    subsidyValue = input.value,
                    remarks = input.remarks,
                    isCalculateOnSI = input.isCalculateSI,
                    agreementDetailID = agreementDetailID
                };

                if (input.subsidyID != 0)
                {
                    var getTrSubsidyDetail = (from x in _trSubsidyDetailRepo.GetAll()
                                                where x.Id == input.subsidyID
                                                select x).FirstOrDefault();

                    getTrSubsidyDetail.subsidyType = input.typeID;
                    getTrSubsidyDetail.subsidyValue = input.value;
                    getTrSubsidyDetail.remarks = input.remarks;
                    getTrSubsidyDetail.isCalculateOnSI = input.isCalculateSI;
                    getTrSubsidyDetail.agreementDetailID = agreementDetailID;

                    data = ObjectMapper.Map<TR_SubsidyDetail>(getTrSubsidyDetail);
                }

                _trSubsidyDetailRepo.InsertOrUpdate(data);
            }
            catch (Exception ex)
            {
                Logger.DebugFormat("TR_SubsidyDetail CreateOrUpdate Error - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException(ex.Message);
            }
        }
        #endregion

        public GetBankDetailDto GetBankDetail(int creditAgreementID)
        {
            var getBankDetail = (from A in _trBankDetailRepo.GetAll()
                                 join B in _trCreditAgreementRepo.GetAll() on A.creditAgreementID equals B.Id
                                 join C in _msBankRepo.GetAll() on B.bankID equals C.Id
                                 where A.creditAgreementID == creditAgreementID
                                 select new GetBankDetailDto
                                 {
                                  bankDetailID = A.Id,
                                  address = A.address,
                                  bankName = C.bankName,
                                  email = A.email,
                                  isOB = A.isShowOnOB,
                                  isPPOnline = A.isShowOnPPOnline,
                                  isPriority = A.isPriority,
                                  officePhone = A.officePhone,
                                  phone = A.phone,
                                  pic = A.picName
                                 }).FirstOrDefault();

            return getBankDetail;
        }

        public PagedResultDto<GetCreditAgreementListDto> GetCreditAgreement(GetCreditAgreementInputDto input)
        {
            var queryCreditAgreement = (from A in _trCreditAgreementRepo.GetAll()
                                        join B in _msProjectRepo.GetAll() on A.projectID equals B.Id
                                        join C in _msBankRepo.GetAll() on A.bankID equals C.Id
                                        join D in _msFeeType.GetAll() on A.agreementTypeID equals D.Id
                                        select new GetCreditAgreementListDto
                                        {
                                            creditAgreementID = A.Id,
                                            projectID = B.Id,
                                            projectName = B.projectName,
                                            bankID = C.Id,
                                            bankName = C.bankName,
                                            typeID = D.Id,
                                            typeName = D.feeTypeName,
                                            agreementDate = A.agreementDate,
                                            agreementStartDate = A.agreementStart,
                                            agreementEndDate = A.agreementEnd,
                                            status = A.isActive
                                        })
                                        .WhereIf(!input.Filter.IsNullOrWhiteSpace(),
                                            u =>
                                                u.projectName.ToLower().Contains(input.Filter.ToLower()) ||
                                                u.typeName.ToLower().Contains(input.Filter.ToLower()) ||
                                                u.bankName.ToLower().Contains(input.Filter.ToLower())
                                        );

            var dataCount = queryCreditAgreement.Count();

            var getListOfCreditAgreement = queryCreditAgreement
            .OrderBy(input.Sorting)
            .PageBy(input)
            .ToList();

            return new PagedResultDto<GetCreditAgreementListDto>(
                dataCount,
                getListOfCreditAgreement
                );
        }

        public List<GetProgramRateListDto> GetProgramDetail(int agreementDetailID)
        {
            var getSetProgram = (from A in _trProgramDetailRepo.GetAll()
                                 where A.agreementDetailID == agreementDetailID
                                 select new GetProgramRateListDto
                                 {
                                     programDetailID = A.Id,
                                     bankRate = A.bankRate.GetValueOrDefault(),
                                     tenor = A.tenor.GetValueOrDefault(),
                                     fix = A.fix.GetValueOrDefault()
                                 }).ToList();

            return getSetProgram;
        }

        public List<GetRetentionDetailListDto> GetRetentionDetail(int agreementDetailID)
        {
            var getRetentionDetail = (from A in _trRetentionDetailRepo.GetAll()
                                      join B in _msRetentionRepo.GetAll() on A.retentionID equals B.Id
                                      where A.agreementDetailID == agreementDetailID
                                      select new GetRetentionDetailListDto
                                      {
                                          retentionDetailID = A.Id,
                                          retentionID = B.Id,
                                          retentionName = B.retentionName,
                                          sortNo = A.sortNo.GetValueOrDefault(),
                                          percentage = A.percentage.GetValueOrDefault()
                                      }).ToList();

            return getRetentionDetail;
        }

        public GetSubsidyDto GetSubsidy(int agreementDetailID)
        {
            var getSubsidy = (from A in _trSubsidyDetailRepo.GetAll()
                              join B in _msFeeType.GetAll() on A.subsidyType equals B.Id
                                      where A.agreementDetailID == agreementDetailID
                                      select new GetSubsidyDto
                                      {
                                          subsidyID = A.Id,
                                          typeID = B.Id,
                                          typeName = B.feeTypeName,
                                          value = A.subsidyValue.GetValueOrDefault(),
                                          remarks = A.remarks,
                                          isCalculateSI = A.isCalculateOnSI
                                      }).FirstOrDefault();

            return getSubsidy;
        }

        public GetAgreementInformationDto GetAgreementInformation(int creditAgreementID)
        {
            var getBankDetail = GetBankDetail(creditAgreementID);

            var getCreditAgreement = (from A in _trCreditAgreementRepo.GetAll()
                                      join B in _msProjectRepo.GetAll() on A.projectID equals B.Id
                                      join C in _lkBankLevelRepo.GetAll() on A.agreementTypeID equals C.Id
                                      where A.Id == creditAgreementID
                                      select new GetAgreementInformationDto
                                      {
                                          creditAgreementID = A.Id,
                                          projectID = B.Id,
                                          projectName = B.projectName,
                                          bankID = A.bankID,
                                          bankDetail = getBankDetail,
                                          agreementDate = A.agreementDate,
                                          agreementEndDate = A.agreementEnd,
                                          agreementStartDate = A.agreementStart,
                                          agreementRate = A.agreementRate,
                                          typeID = A.agreementTypeID,
                                          typeName = C.bankLevelName,
                                          agreementDocsName = A.agreementDocsName,
                                          agreementDocsNo = A.agreementDocsNo,
                                          agreementDocsUrl = (A != null && A.agreementDocsUrl != null) ? getAbsoluteUriWithoutTail() + GetURLWithoutHost(A.agreementDocsUrl) : null ,
                                          isActive = A.isActive
                                      }).FirstOrDefault();

            return getCreditAgreement;
        }

        public List<GetFeeTypeListDto> GetFeeType()
        {
            var getFeeType = (from A in _msFeeType.GetAll()
                              where A.isActive
                              select new GetFeeTypeListDto
                              {
                                  isActive = A.isActive,
                                  feeTypeID = A.Id,
                                  feeTypeName = A.feeTypeName
                              }).ToList();

            return getFeeType;
        }

        public List<GetProgramDetailListDto> GetProgramDetailList(int creditAgreementID)
        {
            var getAgreementDetail = (from A in _trAgreementDetailRepo.GetAll()
                                      join B in _msClusterRepo.GetAll() on A.clusterID equals B.Id
                                      where A.creditAgreementID == creditAgreementID
                                      select new GetProgramDetailListDto
                                      {
                                          agreementDetailID = A.Id,
                                          addendumDocs = A.addendumDocsName,
                                          addendumDocsUrl = (A != null && A.addendumDocsUrl != null) ? getAbsoluteUriWithoutTail() + GetURLWithoutHost(A.addendumDocsUrl) : null,
                                          addendumNo = A.addendumNo,
                                          clusterID = B.Id,
                                          clusterName = B.clusterName,
                                          adminTypeName = _msFeeType.GetAll().Where(x => x.Id == A.adminFeeType).Select(x => x.feeTypeName).FirstOrDefault(),
                                          adminTypeID = A.adminFeeType.GetValueOrDefault(),
                                          adminFee = A.adminFeeValue.GetValueOrDefault(),
                                          provisiTypeName = _msFeeType.GetAll().Where(x => x.Id == A.provisiFeeType).Select(x => x.feeTypeName).FirstOrDefault(),
                                          provisiTypeID = A.provisiFeeType.GetValueOrDefault(),
                                          provisiFee = A.provisiValue.GetValueOrDefault(),
                                          bankRate = A.bankRate,
                                          programEndDate = A.programEnd,
                                          programStartDate = A.programStart,
                                          programNo = A.programNo.ToString().PadLeft(4, '0'),
                                          programName = A.programName,
                                          status = A.isActive,
                                          //programRate = GetProgramDetail(A.Id),
                                          //subsidy = GetSubsidy(A.Id),
                                          //retentionDetail = GetRetentionDetail(A.Id)
                                      }).ToList();

            foreach (var X in getAgreementDetail)
            {
                X.programRate = GetProgramDetail(X.agreementDetailID);
                X.subsidy = GetSubsidy(X.agreementDetailID);
                X.retentionDetail = GetRetentionDetail(X.agreementDetailID);
            }

            return getAgreementDetail;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CreditManagement_CreditAgreement_Detail)]
        public GetCreditAgreementViewDetail GetCreditAgreementViewDetail(int creditAgreementID)
        {
            var getAgreementInformation = GetAgreementInformation(creditAgreementID);
            var getProgram = GetProgramDetailList(creditAgreementID);

            var returnData = new GetCreditAgreementViewDetail
            {
                agreementInformation = getAgreementInformation,
                programDetail = getProgram
            };

            return returnData;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CreditManagement_CreditAgreement)]
        public GetMsBankDto GetMsBankById(int bankID)
        {
            var getData = (from A in _msBankRepo.GetAll()
                           where A.Id == bankID
                           select new GetMsBankDto
                           {
                               bankID = A.Id,
                               bankName = A.bankName,
                               PICName = A.PICName,
                               phone = A.phoneCodeArea + A.phone,
                               email = A.email,
                               address = A.address
                           }).FirstOrDefault();

            return getData;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CreditManagement_CreditAgreement)]
        public List<GetBankDropDownListDto> GetMsBankByBankType(int bankTypeID)
        {
            var getBankDropDown = (from A in _msBankRepo.GetAll()
                                   where A.bankTypeID == bankTypeID
                                   select new GetBankDropDownListDto
                                   {
                                       bankID = A.Id,
                                       bankName = A.bankName
                                   }).ToList();

            return new List<GetBankDropDownListDto>(getBankDropDown);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CreditManagement_CreditAgreement)]
        public JObject GetProgramNo()
        {
            JObject obj = new JObject();

            var getProgramNo = (from x in _trAgreementDetailRepo.GetAll()
                                orderby x.Id descending
                                select x.programNo).FirstOrDefault();

            var programNo = getProgramNo.ToString() == null ? "1" : (getProgramNo + 1).ToString("D3");

            obj.Add("programNo", programNo);
            return obj;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CreditManagement_CreditAgreement)]
        public JObject GetAddendumNo()
        {
            JObject obj = new JObject();

            var getAddendumNo = (from x in _trAgreementDetailRepo.GetAll()
                                 orderby x.Id descending
                                 select x.addendumNo).FirstOrDefault();

            var addendumNo = getAddendumNo == null ? 1.ToString("D3") : (Convert.ToInt32(getAddendumNo.Substring(getAddendumNo.Length - 3)) + 1).ToString("D3");

            obj.Add("addendumNo", "AgreementDocsNo_ADD" + addendumNo);
            return obj;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CreditManagement_CreditAgreement)]
        public JObject GetAgreementDocsNo(int projectID, int bankID)
        {
            JObject obj = new JObject();

            var getProjectCode = (from x in _msProjectRepo.GetAll()
                                  where x.Id == projectID
                                  select x.projectCode).FirstOrDefault();

            var getBankCode = (from x in _msBankRepo.GetAll()
                               where x.Id == bankID
                               select x.bankCode).FirstOrDefault();

            var getAgreementDocsNo = (from x in _trCreditAgreementRepo.GetAll()
                                      where x.agreementDocsNo.Split(new[] { '/' })[4].Contains(Convert.ToString(DateTime.Now.Year))
                                      select x.agreementDocsNo).FirstOrDefault();

            var docNo = getAgreementDocsNo == null ? 1.ToString("D3") : (Convert.ToInt32(getAgreementDocsNo.Split("/")[0]) + 1).ToString("D3");
            
            obj.Add("agreementDocsNo", docNo + "/PKS/" + getBankCode + "/" + getProjectCode + "/" + DateTime.Now.Year);
            return obj;
        }

        #region helper function
        private string MoveFile(string filename, string module)
        {
            try
            {
                return _filesHelper.MoveFiles(filename, @"Temp\Downloads\" + module, @"Assets\Upload\" + module);
            }
            catch (Exception ex)
            {
                Logger.DebugFormat("test() - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Error : {0}", ex.Message);
            }
        }

        private void DeleteFile(string filename, string module)
        {
            var imageToDelete = filename;
            var filenameToDelete = imageToDelete.Split(@"/");
            var nameFileToDelete = filenameToDelete[filenameToDelete.Count() - 1];
            var deletePath = @"Assets\Upload\" + module;

            var deleteImage = _hostingEnvironment.WebRootPath + deletePath + nameFileToDelete;

            if (File.Exists(deleteImage))
            {
                var file = new FileInfo(deleteImage);
                file.Delete();
            }
            else
            {
                Logger.Info("File not exist!");
            }
        }

        private void GetURLWithoutHost(string path, out string finalpath)
        {
            finalpath = path;
            try
            {
                Regex RegexObj = new Regex("[\\w\\W]*([\\/]Assets[\\w\\W\\s]*)");
                if (RegexObj.IsMatch(path))
                {
                    finalpath = RegexObj.Match(path).Groups[1].Value;
                }
            }
            catch (ArgumentException ex)
            {
                Logger.DebugFormat("test() - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Error : {0}", ex.Message);
            }
        }

        private string GetURLWithoutHost(string path)
        {
            string finalpath = path;
            try
            {
                Regex RegexObj = new Regex("[\\w\\W]*([\\/]Assets[\\w\\W\\s]*)");
                if (RegexObj.IsMatch(path))
                {
                    finalpath = RegexObj.Match(path).Groups[1].Value;
                }
            }
            catch (ArgumentException ex)
            {
                Logger.DebugFormat("test() - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Error : {0}", ex.Message);
            }
            return finalpath;
        }

        private string getAbsoluteUriWithoutTail()
        {
            var request = _httpContextAccessor.HttpContext.Request;
            UriBuilder uriBuilder = new UriBuilder();
            uriBuilder.Scheme = request.Scheme;
            uriBuilder.Host = request.Host.ToString();
            var test = uriBuilder.ToString();
            var result = test.Replace("[", "").Replace("]", "");
            int position = result.LastIndexOf('/');
            if (position > -1)
                result = result.Substring(0, result.Length - 1);

            if (request.PathBase != null)
            {
                if (!string.IsNullOrWhiteSpace(request.PathBase.Value))
                {
                    result += request.PathBase.Value;
                }
            }
            return result;
        }
        #endregion
    }
}
