﻿using Abp.Domain.Repositories;
using Abp.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using Abp.Extensions;
using Abp.Linq.Extensions;
using System.Linq.Dynamic.Core;
using System.Text;
using VDI.Demo.CreditManagement.SettingRetentions.Dto;
using VDI.Demo.PropertySystemDB.CreditManagement;
using Abp.Application.Services.Dto;
using Abp.UI;
using Abp.AutoMapper;
using Abp.Authorization;
using VDI.Demo.Authorization;
using Newtonsoft.Json.Linq;

namespace VDI.Demo.CreditManagement.SettingRetentions
{
    public class SettingRetentionAppService : DemoAppServiceBase, ISettingRetentionAppService
    {
        private readonly IRepository<MS_Retention> _msRetentionRepo;

        public SettingRetentionAppService(
            IRepository<MS_Retention> msRetentionRepo
        )
        {
            _msRetentionRepo = msRetentionRepo;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CreditManagement_SettingRetention_Create)]
        public void CreateRetention(CreateRetentionInputDto input)
        {
            Logger.DebugFormat("CreateRetention() - Start");
            var retentionCode = GetRetentionCode();

            var dataRetention = new MS_Retention
            {
                retentionCode = retentionCode.GetValue("NewRetentionCode").ToString(),
                retentionName = input.retentionName,
                isActive = input.status
            };
            Logger.DebugFormat("CreateRetention() - Start Insert MS_Retention " +
               "   retentionCode = {1}{0}" +
               "   retentionName = {2}{0}" +
               "   isActive = {3}{0}" 
               , Environment.NewLine, dataRetention.retentionCode, dataRetention.retentionName, dataRetention.isActive);

            Logger.DebugFormat("CreateRetention() - Start Insert");
            try
            {
                _msRetentionRepo.Insert(dataRetention);
                Logger.DebugFormat("CreateRetention() - End Insert");
            }
            catch (Exception ex)
            {
                Logger.DebugFormat("CreateRetention() - ERROR Exception. Result = {0}", ex.Message);
                Logger.DebugFormat("CreateRetention() - End Insert");
                throw new UserFriendlyException("CreateRetention Error !", ex.Message);
            }

            Logger.DebugFormat("CreateRetention() - End");
        }

        public JObject GetRetentionCode()
        {
            JObject obj = new JObject();

            var getNewestRetention = (from A in _msRetentionRepo.GetAll()
                                      orderby A.Id descending
                                      select A).FirstOrDefault();

            int runningNumberRetention;

            if (getNewestRetention != null)
            {
                var retentionCode = getNewestRetention.retentionCode.Substring(3);
                Logger.Info("Old Running Number Retention Code : " + retentionCode);

                runningNumberRetention = Int32.Parse(retentionCode) + 1;
                Logger.Info("New Running Number Retention Code : " + runningNumberRetention);
            }
            else
            {
                runningNumberRetention = 1;
                Logger.Info("New Running Number Retention Code : " + runningNumberRetention);
            }

            string NewRetentionCode = "RET" + runningNumberRetention;
            Logger.Info("New Retention Code : " + NewRetentionCode);

            obj.Add("NewRetentionCode", NewRetentionCode);
            return obj;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CreditManagement_SettingRetention)]
        public PagedResultDto<GetAllSettingRetentionListDto> GetAllSettingRetention(GetAllSettingRetentionInputDto input)
        {
            var queryMsRetention = (from A in _msRetentionRepo.GetAll()
                                       select new GetAllSettingRetentionListDto
                                       {
                                           retentionID = A.Id,
                                           retentionCode = A.retentionCode,
                                           retentionName = A.retentionName,
                                           status = A.isActive
                                       })
                                       .WhereIf(!input.Filter.IsNullOrWhiteSpace(),
                                            u =>
                                                u.retentionCode.ToLower().Contains(input.Filter.ToLower()) ||
                                                u.retentionName.ToLower().Contains(input.Filter.ToLower()) 
                                        );

            var dataCount = queryMsRetention.Count();

            var getListOfMsRetention = queryMsRetention
            .OrderBy(input.Sorting)
            .PageBy(input)
            .ToList();

            return new PagedResultDto<GetAllSettingRetentionListDto>(
                dataCount,
                getListOfMsRetention
                );
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CreditManagement_SettingRetention_Edit)]
        public void UpdateRetention(UpdateRetentionInputDto input)
        {
            Logger.DebugFormat("UpdateRetention() - Start");

            Logger.DebugFormat("UpdateRetention() - Start Update MS_Retention " +
                "   retentionID = {1}{0}"
                , Environment.NewLine, input.retentionID);

            var getMsRetention = (from x in _msRetentionRepo.GetAll()
                                   where x.Id == input.retentionID
                                   select x).FirstOrDefault();


            var updateMsRetention = getMsRetention.MapTo<MS_Retention>();
            updateMsRetention.retentionName = input.retentionName;
            updateMsRetention.isActive = input.status;

            Logger.DebugFormat("UpdateRetention() - Start Update MS_Retention " +
              "   retentionName = {1}{0}" +
              "   isActive = {2}{0}"
              , Environment.NewLine, updateMsRetention.retentionName, updateMsRetention.isActive);

            Logger.DebugFormat("UpdateRetention() - Start Update MS_Retention ");
            try
            {
                _msRetentionRepo.Update(updateMsRetention);
                Logger.DebugFormat("UpdateRetention() - End Update");
            }
            catch (Exception ex)
            {
                Logger.DebugFormat("UpdateRetention() - ERROR Exception. Result = {0}", ex.Message);
                Logger.DebugFormat("UpdateRetention() - End Update");
                throw new UserFriendlyException("UpdateRetention Error !", ex.Message);
            }

            Logger.DebugFormat("UpdateRetention() - End");
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CreditManagement_CreditAgreement)]
        public List<GetAllSettingRetentionListDto> GetAllMsRetentionDropdown()
        {
            var result = (from x in _msRetentionRepo.GetAll()
                          where x.isActive
                          select new GetAllSettingRetentionListDto
                          {
                              retentionID = x.Id,
                              retentionCode = x.retentionCode,
                              retentionName = x.retentionName,
                              status = x.isActive
                          }).ToList();

            return result;
        }
    }
}
