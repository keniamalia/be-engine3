﻿using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.UI;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using VDI.Demo.AccountingDB;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.Payment.AutoDebetPayment.Dto;
using VDI.Demo.Payment.BulkPayment;
using VDI.Demo.Payment.BulkPayment.Dto;
using VDI.Demo.Payment.InputPayment;
using VDI.Demo.Payment.InputPayment.Dto;
using VDI.Demo.PropertySystemDB.LippoMaster;

namespace VDI.Demo.Payment.AutoDebetPayment
{
    public class AutoDebetPaymentAppService : DemoAppServiceBase, IAutoDebetPaymentAppService
    {
        private readonly PropertySystemDbContext _contextPropertySystem;
        private readonly AccountingDbContext _contextAccounting;
        private readonly TAXDbContext _contextTAX;
        private readonly PersonalsNewDbContext _contextPersonals;
        private readonly DemoDbContext _contextEngine3;
        private readonly IInputPaymentAppService _iInputPaymentAppService;
        private readonly IBulkPaymentAppService _iBulkPaymentAppService;
        private readonly IRepository<TR_PaymentAutoDebet> _trPaymentAutoDebetRepo;

        public AutoDebetPaymentAppService(
            PropertySystemDbContext contextPropertySystem,
            AccountingDbContext contextAccounting,
            TAXDbContext contextTAX,
            PersonalsNewDbContext contextPersonals,
            DemoDbContext contextEngine3,
            IInputPaymentAppService iInputPaymentAppService, 
            IBulkPaymentAppService iBulkPaymentAppService,
            IRepository<TR_PaymentAutoDebet> trPaymentAutoDebetRepo
            )
        {
            _contextPropertySystem = contextPropertySystem;
            _contextAccounting = contextAccounting;
            _contextTAX = contextTAX;
            _contextPersonals = contextPersonals;
            _contextEngine3 = contextEngine3;
            _iInputPaymentAppService = iInputPaymentAppService;
            _iBulkPaymentAppService = iBulkPaymentAppService;
            _trPaymentAutoDebetRepo = trPaymentAutoDebetRepo;
        }

        private string ReplaceCharacter(string accNo)
        {
            var replace = Regex.Replace(accNo, "[^0-9]", "");
            return replace;
        }

        [HttpPost]
        public List<GetDataCheckUploadAutoDebetListDto> GetDataCheckUploadAutoDebet(List<GetDataCheckUploadAutoDebetInputDto> input)
        {
            List<GetDataCheckUploadAutoDebetListDto> listResult = new List<GetDataCheckUploadAutoDebetListDto>();

            foreach (var data in input)
            {
                var result = new GetDataCheckUploadAutoDebetListDto();

                var checkAccount = (from a in _contextPropertySystem.MS_Account
                                  where ReplaceCharacter(a.accNo) == data.accNo && a.entityID == data.entityID
                                  select a).FirstOrDefault();
                if (checkAccount == null)
                {
                    result = new GetDataCheckUploadAutoDebetListDto
                    {
                        accNo = data.accNo,
                        bookCode = data.bookCode,
                        uniqueNo = data.uniqueNo,
                        message = "This Account doesn't exist"
                    };
                    listResult.Add(result);
                    continue;
                }

                var checkBookCode = (from a in _contextPropertySystem.TR_BookingHeader
                                     join b in _contextPropertySystem.TR_BookingDetail on a.Id equals b.bookingHeaderID
                                     join c in _contextPropertySystem.MS_Unit on a.unitID equals c.Id
                                     join d in _contextPropertySystem.MS_UnitCode on c.unitCodeID equals d.Id
                                     where a.bookCode == data.bookCode && b.coCode == checkAccount.devCode
                                     group b by new
                                     {
                                         b.bookingHeaderID,
                                         a.bookCode,
                                         b.pctTax,
                                         a.psCode,
                                         a.unitID,
                                         c.unitNo,
                                         d.unitCode
                                     } into G
                                     select new
                                     {
                                         G.Key.bookingHeaderID,
                                         G.Key.bookCode,
                                         G.Key.pctTax,
                                         G.Key.psCode,
                                         G.Key.unitID,
                                         G.Key.unitCode,
                                         G.Key.unitNo
                                     }).FirstOrDefault();

                if (checkBookCode == null)
                {
                    result = new GetDataCheckUploadAutoDebetListDto
                    {
                        accNo = data.accNo,
                        bookCode = data.bookCode,
                        uniqueNo = data.uniqueNo,
                        message = "Book Code Not Found"
                    };
                    listResult.Add(result);
                    continue;
                }

                var ADBKey = data.bookCode + "#" + data.uniqueNo;
                var checkPaymentAutoDebet = (from a in _contextPropertySystem.TR_PaymentAutoDebet
                                             where a.ADBKey == ADBKey
                                             select a).FirstOrDefault();

                if(checkPaymentAutoDebet != null)
                {
                    result = new GetDataCheckUploadAutoDebetListDto
                    {
                        accNo = data.accNo,
                        bookCode = data.bookCode,
                        uniqueNo = data.uniqueNo,
                        message = "This Payment has been paid"
                    };
                    listResult.Add(result);
                    continue;
                }

                var getParamPayFor = (from a in _contextPropertySystem.MS_Parameter
                                      where a.code == "PFP"
                                      select a.value).FirstOrDefault();
                var getParamPayType = (from a in _contextPropertySystem.MS_Parameter
                                      where a.code == "PTPA"
                                       select a.value).FirstOrDefault();
                var getParamOthersType = (from a in _contextPropertySystem.MS_Parameter
                                      where a.code == "OTPVA"
                                          select a.value).FirstOrDefault();
                var getParamBankName = (from a in _contextPropertySystem.MS_Parameter
                                      where a.code == "BNPA"
                                        select a.value).FirstOrDefault();

                var payForID = (from a in _contextPropertySystem.LK_PayFor
                                where a.payForCode.Contains(getParamPayFor)
                                select a.Id).FirstOrDefault();
                var payTypeID = (from a in _contextPropertySystem.LK_PayType
                                where a.payTypeCode.Contains(getParamPayType)
                                select a.Id).FirstOrDefault();
                var othersTypeID = (from a in _contextPropertySystem.LK_OthersType
                                where a.othersTypeCode.Contains(getParamOthersType)
                                select a.Id).FirstOrDefault();

                //get personal by bookCode
                var getPersonal = (from a in _contextPersonals.PERSONAL
                                   join c in _contextPersonals.TR_Address on a.psCode equals c.psCode into address
                                   from e in address.DefaultIfEmpty()
                                   join b in _contextPersonals.LK_AddrType on new { addrType = (e == null ? "" : e.addrType), addrName = "Corress" } equals new { addrType = b.addrType, addrName = b.addrTypeName } into addrType
                                   from f in addrType.DefaultIfEmpty()
                                   where a.psCode == checkBookCode.psCode
                                   select new
                                   {
                                       a.name,
                                       a.NPWP,
                                       address = e == null ? null : e.address
                                   }).FirstOrDefault();

                //get booking detail schedule
                var getDataSceduleInput = new GetDataSchedulePaymentInputDto
                {
                    accountID = checkAccount.Id,
                    bookingHeaderID = checkBookCode.bookingHeaderID,
                    payForID = payForID
                };

                var getTrBookingDetailSchedule = _iInputPaymentAppService.GetDataSchedulePayment(getDataSceduleInput);

                //get schedule dengan outstanding != 0
                var getDataScheduleOutNoNol = (from a in getTrBookingDetailSchedule
                                               where a.netOutstanding != 0 && a.VATOutstanding != 0
                                               select new GetDataSchedule
                                               {
                                                   dueDate = a.dueDate,
                                                   schedNo = a.schedNo,
                                                   allocDesc = a.allocCode,
                                                   allocID = a.allocID,
                                                   amount = a.netOutstanding + a.VATOutstanding
                                               }).ToList();

                if (getDataScheduleOutNoNol == null)
                {
                    result = new GetDataCheckUploadAutoDebetListDto
                    {
                        accNo = data.accNo,
                        bookCode = data.bookCode,
                        uniqueNo = data.uniqueNo,
                        message = "All Schedule has been paid"
                    };
                    listResult.Add(result);
                    continue;
                }

                result = new GetDataCheckUploadAutoDebetListDto
                {
                    accNo = data.accNo,
                    bookCode = data.bookCode,
                    uniqueNo = data.uniqueNo,
                    accID = checkAccount.Id,
                    bookingHeaderID = checkBookCode.bookingHeaderID,
                    payForID = payForID,
                    payTypeID = payTypeID,
                    othersTypeID = othersTypeID,
                    psCode = checkBookCode.psCode,
                    address = getPersonal.address,
                    name = getPersonal.name,
                    NPWP = getPersonal.NPWP,
                    othersTypeCode = getParamOthersType,
                    payForCode = getParamPayFor,
                    payTypeCode = getParamPayType,
                    pctTax = checkBookCode.pctTax,
                    unitID = checkBookCode.unitID,
                    unitNo = checkBookCode.unitNo,
                    unitCode = checkBookCode.unitCode,
                    bankName = getParamBankName,
                    dataSchedule = getDataScheduleOutNoNol,
                    message = null
                };
                listResult.Add(result);
            }

            return listResult;
        }
        private void CreateTrPaymentAutoDebet(CreateUniversalAutoDebetInputDto input)
        {
            Logger.Info("CreateTrPaymentAutoDebet() - Started.");

            var data = new TR_PaymentAutoDebet
            {
                ADBKey = input.ADBKey,
                accountNo = input.accountNo,
                bookingHeaderID = input.bookingHeaderID,
                custName = input.custName,
                curr = input.curr,
                transDate = input.transDate,
                amount = input.amount
            };

            try
            {
                Logger.DebugFormat("CreateTrPaymentAutoDebet() - Start insert TR Payment Auto Debet. Parameters sent:{0}" +
                    "ADBKey     = {1}{0}" +
                    "accountNo    = {2}{0}" +
                    "bookingHeaderID    = {3}{0}" +
                    "custName           = {4}{0}" +
                    "curr          = {5}{0}" +
                    "transDate       = {6}{0}" +
                    "amount       = {7}{0}"
                    , Environment.NewLine, input.ADBKey, input.accountNo, input.bookingHeaderID, input.custName, input.curr
                    , input.transDate, input.amount);

                _trPaymentAutoDebetRepo.Insert(data);

                Logger.DebugFormat("CreateTrPaymentAutoDebet() - Ended insert TR Payment Bulk.");

                Logger.Info("CreateTrPaymentAutoDebet() - Finished.");
            }
            catch (DataException ex)
            {
                Logger.ErrorFormat("CreateTrPaymentAutoDebet() - ERROR DataException. Result = {0}", ex.Message);
                throw new UserFriendlyException("Db Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("CreateTrPaymentAutoDebet() - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Error: " + ex.Message);
            }
        }

        public void CreateUniversalAutoDebet(List<CreateUniversalAutoDebetInputDto> input)
        {
            Logger.Info("CreateUniversalBulkPayment() - Started.");

            var accountID = (from x in input
                             select x.accID).FirstOrDefault();
            var dataCount = input.Count();
            var code = (from A in _contextPropertySystem.MS_Account where A.Id == accountID select new { A.devCode, A.accCode }).FirstOrDefault();

            var getTempJournalCode = (from x in _contextAccounting.SYS_JournalCounter
                                      where x.accCode == code.accCode
                                      select x).FirstOrDefault();

            var journalCodeStart = "";
            var journalCode = "";
            var runningNumber = "";
            if (getTempJournalCode != null)
            {
                var dataExisting = getTempJournalCode.journalCode.Split('.');

                string coCodeExisting = dataExisting[1];
                string accCodeExisting = dataExisting[2];
                string yearExisting = dataExisting[3];
                string monthExisting = dataExisting[4];
                int runningNumberExisting = Convert.ToInt32(dataExisting[5]) + dataCount;
                runningNumber = runningNumberExisting.ToString("D5");

                if (code.devCode == coCodeExisting && code.accCode == accCodeExisting && DateTime.Now.Year.ToString() == yearExisting && DateTime.Now.Month.ToString() == monthExisting)
                {
                    journalCodeStart = getTempJournalCode.journalCode;
                    journalCode = "PRS." + code.devCode + "." + code.accCode + "." + yearExisting + "." + monthExisting + "." + runningNumber;
                }
                else
                {
                    int i = 0;
                    var runningNumberStart = i.ToString("D5");
                    runningNumber = dataCount.ToString("D5");
                    journalCodeStart = "PRS." + code.devCode + "." + code.accCode + "." + DateTime.Now.Year.ToString() + "." + DateTime.Now.Month.ToString() + "." + runningNumberStart;
                    journalCode = "PRS." + code.devCode + "." + code.accCode + "." + DateTime.Now.Year.ToString() + "." + DateTime.Now.Month.ToString() + "." + runningNumber;
                }

                var dataJournalUpdate = getTempJournalCode.MapTo<SYS_JournalCounter>();
                dataJournalUpdate.journalCode = journalCode;
                _contextAccounting.SYS_JournalCounter.Update(dataJournalUpdate);
            }
            else
            {
                int i = 0;
                runningNumber = dataCount.ToString("D5");
                var startRunningNumber = i.ToString("D5");
                journalCode = "PRS." + code.devCode + "." + code.accCode + "." + DateTime.Now.Year.ToString() + "." + DateTime.Now.Month.ToString() + "." + runningNumber;
                journalCodeStart = "PRS." + code.devCode + "." + code.accCode + "." + DateTime.Now.Year.ToString() + "." + DateTime.Now.Month.ToString() + "." + startRunningNumber;

                var dataJournalInsert = new SYS_JournalCounter
                {
                    accCode = code.accCode,
                    journalCode = journalCode
                };
                _contextAccounting.SYS_JournalCounter.Add(dataJournalInsert);
            }
            _contextAccounting.SaveChanges();


            var currentIteration = 0;
            foreach (var autoDebetData in input)
            {
                currentIteration++;
                Logger.DebugFormat("CreateUniversalBulkPayment() - Process VA Bulk. Iteration No : {1}{0}" +
                     "ADBKey     = {2}{0}" +
                    "accountNo    = {3}{0}" +
                    "bookingHeaderID    = {4}{0}" +
                    "custName           = {5}{0}" +
                    "curr          = {6}{0}" +
                    "transDate       = {7}{0}" +
                    "amount       = {8}{0}"
                    , Environment.NewLine, currentIteration, autoDebetData.ADBKey, autoDebetData.accountNo, autoDebetData.bookingHeaderID
                    , autoDebetData.custName, autoDebetData.curr, autoDebetData.transDate, autoDebetData.amount);

                var journalData = journalCodeStart.Split(".");
                var formatJournal = journalData[0] + "." + journalData[1] + "." + journalData[2] + "." + journalData[3] + "." + journalData[4];
                var runningJournal = Convert.ToInt32(journalData[5]);

                var data = new CreateUniversalBulkPaymentInputDto()
                {
                    bookingHeaderID = autoDebetData.bookingHeaderID,
                    accID = autoDebetData.accID,
                    clearDate = autoDebetData.transDate,
                    name = autoDebetData.name,
                    bankName = autoDebetData.bankName,
                    psCode = autoDebetData.psCode,
                    unitCode = autoDebetData.unitCode,
                    journalCode = formatJournal,
                    runningJournal = runningJournal,
                    unitNo = autoDebetData.unitNo,
                    keterangan = "[Auto]" + autoDebetData.custName,
                    paymentDate = DateTime.Now,
                    payForID = autoDebetData.payForID,
                    pctTax = autoDebetData.pctTax,
                    address = autoDebetData.address,
                    NPWP = autoDebetData.NPWP,
                    flag = 2,
                    dataForPayment = autoDebetData.dataForPayment,
                    dataScheduleList = autoDebetData.dataScheduleList
                };

                _iBulkPaymentAppService.CreateBulkPayment(data);

                var countInsertedjournal = autoDebetData.dataForPayment.Count();

                int runningNumberExisting = runningJournal + countInsertedjournal;
                runningNumber = runningNumberExisting.ToString("D5");

                journalCodeStart = formatJournal + "." + runningNumber;

                CreateTrPaymentAutoDebet(autoDebetData);

                CurrentUnitOfWork.SaveChanges();
                _contextTAX.SaveChanges();
                _contextAccounting.SaveChanges();
            }
            Logger.Info("CreateUniversalBulkPayment() - Finished.");
        }
    }
}
