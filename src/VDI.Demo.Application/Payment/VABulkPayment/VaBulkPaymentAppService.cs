﻿using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.UI;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using VDI.Demo.AccountingDB;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.Payment.BulkPayment;
using VDI.Demo.Payment.BulkPayment.Dto;
using VDI.Demo.Payment.InputPayment;
using VDI.Demo.Payment.InputPayment.Dto;
using VDI.Demo.Payment.VABulkPayment.Dto;
using VDI.Demo.PropertySystemDB.LippoMaster;
using VDI.Demo.PSAS.Schedule;
using VDI.Demo.PSAS.Schedule.Dto;

namespace VDI.Demo.Payment.VABulkPayment
{
    public class VaBulkPaymentAppService : DemoAppServiceBase, IVaBulkPaymentAppService
    {
        private readonly PropertySystemDbContext _contextPropertySystem;
        private readonly AccountingDbContext _contextAccounting;
        private readonly TAXDbContext _contextTAX;
        private readonly PersonalsNewDbContext _contextPersonals;
        private readonly DemoDbContext _contextEngine3;
        private readonly IPSASScheduleAppService _iPSASScheduleAppService;
        private readonly IBulkPaymentAppService _iBulkPaymentAppService;
        private readonly IRepository<TR_PaymentBulkVA> _trBulkPaymentVaRepo;
        private readonly IInputPaymentAppService _iInputPaymentAppService;

        public VaBulkPaymentAppService(
            PropertySystemDbContext contextPropertySystem,
            AccountingDbContext contextAccounting,
            TAXDbContext contextTAX,
            PersonalsNewDbContext contextPersonals,
            DemoDbContext contextEngine3,
            IPSASScheduleAppService iPSASScheduleAppService,
            IBulkPaymentAppService iBulkPaymentAppService,
            IRepository<TR_PaymentBulkVA> trBulkPaymentVaRepo,
            IInputPaymentAppService iInputPaymentAppService)
        {
            _trBulkPaymentVaRepo = trBulkPaymentVaRepo;
            _iBulkPaymentAppService = iBulkPaymentAppService;
            _iPSASScheduleAppService = iPSASScheduleAppService;
            _contextPropertySystem = contextPropertySystem;
            _contextAccounting = contextAccounting;
            _contextTAX = contextTAX;
            _contextPersonals = contextPersonals;
            _contextEngine3 = contextEngine3;
            _iInputPaymentAppService = iInputPaymentAppService;
        }

        public List<GetDataCheckUploadVaBulkListDto> CheckDataUploadVaBulk(List<CheckDataVaBulkInputDto> input)
        {
            List<GetDataCheckUploadVaBulkListDto> listResult = new List<GetDataCheckUploadVaBulkListDto>();
            
            foreach (var data in input)
            {
                var result = new GetDataCheckUploadVaBulkListDto();

                var bulkVAKey = data.vaNumber + "#" + data.refNo;

                var checkExistingPaymentVA = (from x in _contextPropertySystem.TR_PaymentBulkVA
                                              where x.BulkVAKey == bulkVAKey
                                              select x).Any();

                if (checkExistingPaymentVA)
                {
                    result = new GetDataCheckUploadVaBulkListDto
                    {
                        virtualAccount = data.vaNumber,
                        message = "This Payment has been paid"
                    };
                    listResult.Add(result);
                    continue;
                }

                var checkUnitVirtualAccount = (from a in _contextPropertySystem.MS_UnitVirtualAccount
                                               where a.VA_BankAccNo == data.vaNumber
                                               select a).Any();

                if (!checkUnitVirtualAccount)
                {
                    result = new GetDataCheckUploadVaBulkListDto
                    {
                        virtualAccount = data.vaNumber,
                        message = "This Virtual Account doesn't exist"
                    };
                    listResult.Add(result);
                    continue;
                }

                var checkDataBooking = (from x in _contextPropertySystem.TR_BookingHeader
                                      join a in _contextPropertySystem.MS_UnitVirtualAccount on x.unitID equals a.unitID
                                      join b in _contextPropertySystem.MS_Unit on x.unitID equals b.Id
                                      join c in _contextPropertySystem.MS_UnitCode on b.unitCodeID equals c.Id
                                      join d in _contextPropertySystem.TR_BookingDetail on x.Id equals d.bookingHeaderID
                                      where a.VA_BankAccNo == data.vaNumber && x.cancelDate == null
                                        group b by new
                                        {
                                            bookingHeaderID = x.Id,
                                            x.bookCode,
                                            d.pctTax,
                                            x.psCode,
                                            x.unitID,
                                            b.unitNo,
                                            c.unitCode
                                        } into G
                                        select new
                                        {
                                            G.Key.bookingHeaderID,
                                            G.Key.bookCode,
                                            G.Key.pctTax,
                                            G.Key.psCode,
                                            G.Key.unitID,
                                            G.Key.unitNo,
                                            G.Key.unitCode
                                        }).FirstOrDefault();
                
                if (checkDataBooking == null)
                {
                    result = new GetDataCheckUploadVaBulkListDto
                    {
                        virtualAccount = data.vaNumber,
                        message = "Data Booking Not Found"
                    };
                    listResult.Add(result);
                    continue;
                }

                //get role by user id
                var getRoleId = (from A in _contextEngine3.UserRoles.ToList()
                                 where A.UserId == data.userID
                                 select A.RoleId).ToList();

                //get list pay for by role
                var getPayForCheckRole = (from A in _contextPropertySystem.SYS_RolesPayFor
                                          join B in _contextPropertySystem.LK_PayFor on A.payForID equals B.Id
                                          where B.isActive == true && getRoleId.Contains(A.rolesID) && B.payForCode == "PMT"
                                          orderby B.payForCode
                                          select new { B.payForCode, A.payForID }).FirstOrDefault();

                //get list pay type by role
                var getPayTypeCheckRole = (from A in _contextPropertySystem.SYS_RolesPayType
                                           join B in _contextPropertySystem.LK_PayType on A.payTypeID equals B.Id
                                           where B.isActive == true && getRoleId.Contains(A.rolesID) && B.payTypeCode == "VRT"
                                           orderby B.payTypeCode
                                           select new { B.payTypeCode, A.payTypeID }).FirstOrDefault();

                //get list others type by role
                var getOthersTypeCheckRole = (from a in _contextPropertySystem.SYS_RolesOthersType
                                              join b in _contextPropertySystem.LK_OthersType on a.othersTypeID equals b.Id
                                              where getRoleId.Contains(a.rolesID) && b.isActive == true && b.othersTypeCode == "PMT"
                                              orderby b.othersTypeCode
                                              select b.othersTypeCode).FirstOrDefault();

                if (getPayForCheckRole == null && getPayTypeCheckRole == null && getOthersTypeCheckRole == null)
                {
                    result = new GetDataCheckUploadVaBulkListDto
                    {
                        virtualAccount = data.vaNumber,
                        message = "You don't have access in your PayForCode(PMT) or PayTypeCode(VRT) or OthersTypeCode(PMT)"
                    };
                    listResult.Add(result);
                    continue;
                }

                //get personal by bookCode
                var getPersonal = (from a in _contextPersonals.PERSONAL
                                   join c in _contextPersonals.TR_Address on a.psCode equals c.psCode into address
                                   from e in address.DefaultIfEmpty()
                                   join b in _contextPersonals.LK_AddrType on new { addrType = (e == null ? "" : e.addrType), addrName = "Corress" } equals new { addrType = b.addrType, addrName = b.addrTypeName } into addrType
                                   from f in addrType.DefaultIfEmpty()
                                   where a.psCode == checkDataBooking.psCode
                                   select new
                                   {
                                       a.name,
                                       a.NPWP,
                                       address = e == null ? null : e.address
                                   }).FirstOrDefault();

                //get booking detail schedule
                var getDataSceduleInput = new GetDataSchedulePaymentInputDto
                {
                    accountID = data.accID,
                    bookingHeaderID = checkDataBooking.bookingHeaderID,
                    payForID = getPayForCheckRole.payForID
                };

                var getTrBookingDetailSchedule = _iInputPaymentAppService.GetDataSchedulePayment(getDataSceduleInput);

                //get schedule dengan outstanding != 0
                var getDataScheduleOutNoNol = (from a in getTrBookingDetailSchedule
                                               where a.netOutstanding != 0 && a.VATOutstanding != 0
                                               select new GetDataSchedule
                                               {
                                                   schedNo = a.schedNo,
                                                   allocDesc = a.allocCode,
                                                   allocID = a.allocID,
                                                   amount = a.netOutstanding + a.VATOutstanding
                                               }).ToList();

                if (getDataScheduleOutNoNol == null)
                {
                    result = new GetDataCheckUploadVaBulkListDto
                    {
                        virtualAccount = data.vaNumber,
                        message = "All Booking Schedule was paid"
                    };
                    listResult.Add(result);
                    continue;
                }

                result = new GetDataCheckUploadVaBulkListDto
                {
                    bookingHeaderID = checkDataBooking.bookingHeaderID,
                    bookCode = checkDataBooking.bookCode,
                    payForID = getPayForCheckRole.payForID,
                    payForCode = "PMT",
                    payTypeID = getPayTypeCheckRole.payTypeID,
                    payTypeCode = "VRT",
                    othersTypeCode = "PMT",
                    unitID = checkDataBooking.unitID,
                    unitCode = checkDataBooking.unitCode,
                    unitNo = checkDataBooking.unitNo,
                    pctTax = checkDataBooking.pctTax,
                    psCode = checkDataBooking.psCode,
                    name = getPersonal.name,
                    NPWP = getPersonal.NPWP,
                    address = getPersonal.address,
                    virtualAccount = data.vaNumber,
                    dataSchedule = getDataScheduleOutNoNol,
                    message = null
                };

                listResult.Add(result);
            }
            
            return listResult;
        }

        public void CreateUniversalBulkPayment(List<CreateUniversalVaBulkPaymentInputDto> input)
        {
            Logger.Info("CreateUniversalBulkPayment() - Started.");

                var accountID = (from x in input
                             select x.accID).FirstOrDefault();
                var dataCount = input.Count();
                var code = (from A in _contextPropertySystem.MS_Account where A.Id == accountID select new { A.devCode, A.accCode }).FirstOrDefault();

                var getTempJournalCode = (from x in _contextAccounting.SYS_JournalCounter
                                          where x.accCode == code.accCode
                                          select x).FirstOrDefault();

                var journalCodeStart = "";
                var journalCode = "";
                var runningNumber = ""; 
                if (getTempJournalCode != null)
                {
                    var dataExisting = getTempJournalCode.journalCode.Split('.');
                    
                    string coCodeExisting = dataExisting[1];
                    string accCodeExisting = dataExisting[2];
                    string yearExisting = dataExisting[3];
                    string monthExisting = dataExisting[4];
                    int runningNumberExisting = Convert.ToInt32(dataExisting[5]) + dataCount;
                    runningNumber = runningNumberExisting.ToString("D5");

                    if (code.devCode == coCodeExisting && code.accCode == accCodeExisting && DateTime.Now.Year.ToString() == yearExisting && DateTime.Now.Month.ToString() == monthExisting)
                    {
                        journalCodeStart = getTempJournalCode.journalCode;
                        journalCode = "PRS." + code.devCode + "." + code.accCode + "." + yearExisting + "." + monthExisting + "." + runningNumber;
                    }
                    else
                    {
                        int i = 0;
                        var runningNumberStart = i.ToString("D5");
                        runningNumber = dataCount.ToString("D5");
                        journalCodeStart = "PRS." + code.devCode + "." + code.accCode + "." + DateTime.Now.Year.ToString() + "." + DateTime.Now.Month.ToString() + "." + runningNumberStart;
                        journalCode = "PRS." + code.devCode + "." + code.accCode + "." + DateTime.Now.Year.ToString() + "." + DateTime.Now.Month.ToString() + "." + runningNumber;
                    }

                    var dataJournalUpdate = getTempJournalCode.MapTo<SYS_JournalCounter>();
                    dataJournalUpdate.journalCode = journalCode;
                    _contextAccounting.SYS_JournalCounter.Update(dataJournalUpdate);
                }
                else
                {
                    int i = 0;
                    runningNumber = dataCount.ToString("D5");
                    var startRunningNumber = i.ToString("D5");
                    journalCode = "PRS." + code.devCode + "." + code.accCode + "." + DateTime.Now.Year.ToString() + "." + DateTime.Now.Month.ToString() + "." + runningNumber;
                    journalCodeStart = "PRS." + code.devCode + "." + code.accCode + "." + DateTime.Now.Year.ToString() + "." + DateTime.Now.Month.ToString() + "." + startRunningNumber;
                
                    var dataJournalInsert = new SYS_JournalCounter
                    {
                        accCode = code.accCode,
                        journalCode = journalCode
                    };
                    _contextAccounting.SYS_JournalCounter.Add(dataJournalInsert);
                }
                _contextAccounting.SaveChanges();
            

            var currentIteration = 0;
            foreach(var vaBulkData in input)
            {
                currentIteration++;
                Logger.DebugFormat("CreateUniversalBulkPayment() - Process VA Bulk. Iteration No : {1}{0}" +
                     "Tanggal Transaksi          = {2}{0}" +
                     "Jam Transaksi              = {3}{0}" +
                     "Tanggal Mutasi             = {4}{0}" +
                     "Virtual Account            = {5}{0}" +
                     "Nama                       = {6}{0}" +
                     "Nilai Transaksi            = {7}{0}" +
                     "Jenis Transaksi            = {8}{0}" +
                     "Nomor Referensi            = {9}{0}" +
                     "Keterangan                 = {10}{0}",
                     Environment.NewLine, vaBulkData.tanggalTransaksi, vaBulkData.jamTransaksi, vaBulkData.tanggalMutasi,
                     vaBulkData.virtualAccount, vaBulkData.nama, vaBulkData.nilaiTransaksi, vaBulkData.jenisTransaksi,
                     vaBulkData.noRef, vaBulkData.keterangan);
                
                var journalData = journalCodeStart.Split(".");
                var formatJournal = journalData[0] + "." + journalData[1] + "." + journalData[2] + "." + journalData[3] + "." + journalData[4];
                var runningJournal = Convert.ToInt32(journalData[5]);

                var getParamBankName = (from a in _contextPropertySystem.MS_Parameter
                                        where a.code == "BNPA"
                                        select a.value).FirstOrDefault();

                var data = new CreateUniversalBulkPaymentInputDto()
                {
                    bookingHeaderID = vaBulkData.bookingHeaderID,
                    accID = vaBulkData.accID,
                    clearDate = vaBulkData.tanggalMutasi,
                    name = vaBulkData.name,
                    bankName = getParamBankName,
                    psCode = vaBulkData.psCode,
                    unitCode = vaBulkData.unitCode,
                    journalCode = formatJournal,
                    runningJournal = runningJournal,
                    unitNo = vaBulkData.unitNo,
                    keterangan = vaBulkData.keterangan,
                    paymentDate = vaBulkData.tanggalTransaksi,
                    payForID = vaBulkData.payForID,
                    pctTax = vaBulkData.pctTax,
                    address = vaBulkData.address,
                    NPWP = vaBulkData.NPWP,
                    flag = 1,
                    dataForPayment = vaBulkData.dataForPayment,
                    dataScheduleList = vaBulkData.dataScheduleList
                };

                _iBulkPaymentAppService.CreateBulkPayment(data);

                var countInsertedjournal = vaBulkData.dataForPayment.Count();
                
                int runningNumberExisting = runningJournal + countInsertedjournal;
                runningNumber = runningNumberExisting.ToString("D5");

                journalCodeStart = formatJournal + "." + runningNumber;

                CreateTrBulkPaymentVa(vaBulkData);

                CurrentUnitOfWork.SaveChanges();
                _contextTAX.SaveChanges();
                _contextAccounting.SaveChanges();
            }
            Logger.Info("CreateUniversalBulkPayment() - Finished.");
        }

        public void CreateTrBulkPaymentVa(CreateUniversalVaBulkPaymentInputDto input)
        {
            Logger.Info("CreateTrBulkPaymentVa() - started.");

            var data = new TR_PaymentBulkVA
            {
                BulkVAKey = input.virtualAccount+"#"+input.noRef,
                JamTransaksi = input.jamTransaksi,
                JenisTransaksi = input.jenisTransaksi,
                Keterangan = input.keterangan,
                NamaVA = input.nama,
                NilaiTransaksi = input.nilaiTransaksi,
                NomorReferensi = input.noRef,
                TanggalMutasi = input.tanggalMutasi,
                TanggalTransaksi = input.tanggalTransaksi,
                VirtualAccount = input.virtualAccount
            };

            try
            {
                Logger.DebugFormat("CreateTrBulkPaymentVa() - Create TR_BulkPaymentVA. Param Sent : {0}" +
                     "BulkVaKey                  = {1}{0}" +
                     "Tanggal Transaksi          = {2}{0}" +
                     "Jam Transaksi              = {3}{0}" +
                     "Tanggal Mutasi             = {4}{0}" +
                     "Virtual Account            = {5}{0}" +
                     "Nama                       = {6}{0}" +
                     "Nilai Transaksi            = {7}{0}" +
                     "Jenis Transaksi            = {8}{0}" +
                     "Nomor Referensi            = {9}{0}" +
                     "Keterangan                 = {10}{0}",
                     Environment.NewLine, data.BulkVAKey, data.TanggalTransaksi, data.JamTransaksi, data.TanggalMutasi,
                     data.VirtualAccount, data.NamaVA, data.NilaiTransaksi, data.JenisTransaksi,
                     data.NomorReferensi, data.Keterangan);

                _trBulkPaymentVaRepo.Insert(data);
            }
            catch (DataException ex)
            {
                Logger.ErrorFormat("CreateTrBulkPaymentVa() - ERROR DataException. Result = {0}", ex.Message);
                Logger.Info("CreateTrBulkPaymentVa() - Finished.");
                throw new UserFriendlyException("Db Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("CreateTrBulkPaymentVa() - ERROR Exception. Result = {0}", ex.Message);
                Logger.Info("CreateTrBulkPaymentVa() - Finished.");
                throw new UserFriendlyException("Error: " + ex.Message);
            }
            Logger.Info("CreateTrBulkPaymentVa() - Finished.");
        }
    }
}
