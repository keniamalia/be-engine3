﻿using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.UI;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.Payment.InputInventoryPayment.Dto;
using VDI.Demo.Payment.InputPayment.Dto;
using VDI.Demo.PropertySystemDB.LippoMaster;
using VDI.Demo.PSAS.Schedule.Dto;

namespace VDI.Demo.Payment.InputInventoryPayment
{
    public class InputInventoryPaymentAppService : DemoAppServiceBase, IInputInventoryPaymentAppService
    {
        private readonly PropertySystemDbContext _contextPropertySystem;
        private readonly AccountingDbContext _contextAccounting;
        private readonly TAXDbContext _contextTAX;
        private readonly PersonalsNewDbContext _contextPersonals;
        private readonly DemoDbContext _contextEngine3;
        private readonly IRepository<TR_InventoryHeader> _trInventoryHeaderRepo;
        private readonly IRepository<TR_InventoryDetail> _trInventoryDetailRepo;
        private readonly IRepository<SYS_FinanceCounter> _sysFinanceCounterRepo;

        public InputInventoryPaymentAppService(
            PropertySystemDbContext contextPropertySystem,
            AccountingDbContext contextAccounting,
            TAXDbContext contextTAX,
            PersonalsNewDbContext contextPersonals,
            IRepository<TR_InventoryDetail> trInventoryDetailRepo,
            DemoDbContext contextEngine3,
            IRepository<SYS_FinanceCounter> sysFinanceCounterRepo,
            IRepository<TR_InventoryHeader> trInventoryHeaderRepo
            )
        {
            _sysFinanceCounterRepo = sysFinanceCounterRepo;
            _trInventoryDetailRepo = trInventoryDetailRepo;
            _contextPropertySystem = contextPropertySystem;
            _contextAccounting = contextAccounting;
            _contextTAX = contextTAX;
            _contextPersonals = contextPersonals;
            _contextEngine3 = contextEngine3;
            _trInventoryHeaderRepo = trInventoryHeaderRepo;
        }

        public void CreateUniversalInputInventory(CreateUniversalInputInventoryDto input)
        {
            Logger.Info("CreateUniversalInputInventory - Started.");
            var inventoryHeaderID = CreateTrInventoryHeader(input.inventoryHeader);

            foreach(var inventoryDetail in input.dataInventoryDetail)
            {
                inventoryDetail.inventoryHeaderID = inventoryHeaderID;
                CreateTrInventoryDetail(inventoryDetail);
            }
            Logger.Info("CreateUniversalInputInventory - Finished.");
        }

        
        public List<GetDataTransNoListDto> GetDataTransNoInventory(string filter, int accountID)
        {
            var getDataTransNo = (from a in _contextPropertySystem.TR_InventoryHeader
                                  join b in _contextPropertySystem.TR_InventoryDetail on a.Id equals b.inventoryHeaderID
                                  join c in _contextPropertySystem.TR_BookingHeader on a.bookingHeaderID equals c.Id 
                                  join d in _contextPropertySystem.MS_Unit on c.unitID equals d.Id 
                                  join e in _contextPropertySystem.MS_UnitCode on d.unitCodeID equals e.Id 
                                  join f in _contextPersonals.PERSONAL.ToList() on c.psCode equals f.psCode
                                  join g in _contextPropertySystem.LK_Status on b.statusID equals g.Id
                                  join h in _contextPropertySystem.TR_BookingDetail on c.Id equals h.bookingHeaderID
                                  join i in _contextPropertySystem.MS_Account on h.coCode equals i.devCode
                                  where a.transNo.ToLower().Contains(filter.ToLower()) || c.bookCode.ToLower().Contains(filter.ToLower()) || e.unitCode.ToLower().Contains(filter.ToLower()) || i.Id == accountID
                                  group b by new
                                  {
                                      a.transNo,
                                      a.bookingHeaderID,
                                      c.bookCode,
                                      a.TTBGDate,
                                      c.unitID,
                                      d.unitCodeID,
                                      d.unitNo,
                                      e.unitCode,
                                      f.name,
                                      f.remarks,
                                      accountID = i.Id
                                  } into G
                                  select new GetDataTransNoListDto
                                  {
                                      bookingHeaderID = G.Key.bookingHeaderID,
                                      accountID = G.Key.accountID,
                                      bookCode = G.Key.bookCode,
                                      payDate = G.Key.TTBGDate,
                                      unitID = G.Key.unitID,
                                      unitCodeID = G.Key.unitCodeID,
                                      unitNo = G.Key.unitNo,
                                      unitCode = G.Key.unitCode,
                                      clientName = G.Key.name,
                                      remarks = G.Key.remarks,
                                      amount = G.Sum(x => x.amount),
                                      transNo = G.Key.transNo
                                  })
                                  .OrderByDescending(x => x.payDate)
                                  .ToList();
            return getDataTransNo;
        }

        public JObject GenerateTransNoTTBG(GenerateTransNoInputDto input)
        {
            Logger.Info("GenerateTransNoTTBG() - Started.");

            JObject obj = new JObject();
            string transNoTTBG = "";
            var year = DateTime.Now.Year.ToString();
            string accCode = (from A in _contextPropertySystem.MS_Account
                              where A.Id == input.accID
                              select A.accCode).FirstOrDefault();

            var checkSysFinance = (from sfc in _contextPropertySystem.SYS_FinanceCounter
                                   where sfc.accID == input.accID && sfc.entityID == input.entityID && sfc.year == year
                                   select sfc);

            Logger.DebugFormat("GenerateTransNoTTBG() - Check Sys Finance. sysFinance Exist = ", checkSysFinance.Any());
            //kalau belum ada
            if (!checkSysFinance.Any())
            {
                var dataToInsert = new SYS_FinanceCounter
                {
                    accID = input.accID == 0 ? 958 : input.accID,
                    entityID = input.entityID,
                    year = year,
                    transNo = 0,
                    TTBGNo = 1,
                    ADJNo = 0,
                    pmtBatchNo = 0
                };

                _sysFinanceCounterRepo.Insert(dataToInsert);

                //generate transNo
                int i = 1;
                string incNo1 = i.ToString("D6");

                transNoTTBG = accCode + "/T/" + year + "/" + incNo1;

            }
            else
            {
                var getDataSysFinance = checkSysFinance.FirstOrDefault();

                var update = getDataSysFinance.MapTo<SYS_FinanceCounter>();

                update.TTBGNo = getDataSysFinance.TTBGNo + 1;

                _sysFinanceCounterRepo.Update(update);

                //generate transNo
                string incNo = getDataSysFinance.TTBGNo.ToString("D6");

                transNoTTBG = accCode + "/T/" + year + "/" + incNo;
            }

            Logger.DebugFormat("GenerateTransNoTTBG() - Data Transno = ", transNoTTBG);

            obj.Add("transNoTTBG", transNoTTBG);
            Logger.Info("GenerateTransNoTTBG() - Finished.");
            return obj;
        }

        public List<GetDropdownLkStatusListDto> GetDropdownLkStatus()
        {
            var getLkStatus = (from a in _contextPropertySystem.LK_Status
                               where a.statusCode != "C"
                               select new GetDropdownLkStatusListDto
                               {
                                   Id = a.Id,
                                   statusCode = a.statusCode,
                                   statusName = a.statusDesc
                               }).ToList();
            return getLkStatus;
        }

        private void CreateTrInventoryDetail(CreateTrInventoryDetailInputDto input)
        {
            Logger.Info("CreateTrInventoryDetail - Started.");

            var data = new TR_InventoryDetail
            {
                entityID = 1,
                bankID = input.bankID,
                chequeNo = input.chequeNo,
                clearDate = input.clearDate,
                dueDate = input.dueDate,
                inventoryHeaderID = input.inventoryHeaderID,
                ket = input.ket,
                payForID = input.payForID,
                payTypeID = input.payTypeID,
                payNo = input.payNo,
                statusID = input.statusID,
                transNoPayment = "-",
                amount = input.amount
            };

            try
            {
                Logger.DebugFormat("CreateTrInventoryDetail() - Start insert inventory detail. Parameters sent:{0}" +
                        "entityID               = {1}{0}" +
                        "bankID                 = {2}{0}" +
                        "chequeNo               = {3}{0}" +
                        "clearDate              = {4{0}" +
                        "dueDate                = {5}{0}" +
                        "inventoryHeaderID      = {6}{0}" +
                        "ket                    = {7}{0}" +
                        "payForID               = {8}{0}" +
                        "payTypeID              = {9}{0}" +
                        "payNo                  = {10}{0}" +
                        "statusID               = {11}{0}" +
                        "transNoPayment         = {12}{0}" +
                        "amount                 = {13}{0}" 
                        , Environment.NewLine, data.entityID, data.bankID, data.chequeNo, data.clearDate, 
                        data.dueDate, data.inventoryHeaderID, data.ket, data.payForID, data.payTypeID,
                        data.payNo, data.statusID, data.transNoPayment, data.amount);

                _trInventoryDetailRepo.Insert(data);
                CurrentUnitOfWork.SaveChanges(); //execution saved inside try
            }
            catch (DataException ex)
            {
                Logger.ErrorFormat("CreateTrInventoryDetail() - ERROR DataException. Result = {0}", ex.Message);
                throw new UserFriendlyException("Db Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("CreateTrInventoryDetail() - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Error: " + ex.Message);
            }
            Logger.Info("CreateTrInventoryDetail - Finished.");

        }
        
        private int CreateTrInventoryHeader(CreateTrInventoryHeaderInputDto input)
        {
            Logger.Info("CreateTrInventoryHeader() - Started.");

            var data = new TR_InventoryHeader
            {
                entityID        = 1,
                accID           = input.accID == 0 ? 958 : input.accID,
                bookingHeaderID = input.bookingHeaderID,
                transNo         = input.transNo,
                ket             = input.ket,
                TTBGDate        = input.TTBGDate
            };

            try
            {
                Logger.DebugFormat("CreateTrInventoryHeader() - Start insert TR Inventory Header. Parameters sent:{0}" +
                    "entityID          = {1}{0}" +
                    "accID             = {2}{0}" +
                    "bookingHeaderID   = {3}{0}" +
                    "transNo           = {4}{0}" +
                    "ket               = {5}{0}" +
                    "TTBGDate          = {6}{0}"
                    , Environment.NewLine, input.entityID, input.accID, input.bookingHeaderID, input.transNo, input.ket, input.TTBGDate);

                var inventoryHeaderID = _trInventoryHeaderRepo.InsertAndGetId(data);
                CurrentUnitOfWork.SaveChanges();

                Logger.DebugFormat("CreateTrInventoryHeader() - Ended insert TR Inventory Header.");

                Logger.Info("CreateTrInventoryHeader() - Finished.");

                return inventoryHeaderID;
            }
            catch (DataException ex)
            {
                Logger.ErrorFormat("CreateTrInventoryHeader() - ERROR DataException. Result = {0}", ex.Message);
                throw new UserFriendlyException("Db Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("CreateTrInventoryHeader() - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Error: " + ex.Message);
            }
        }

        public CreateUniversalInputInventoryDto GetDataInventoryByTransNo(string transNoTTBG)
        {
            var dataInventoryHeader = (from x in _contextPropertySystem.TR_InventoryHeader
                                       join a in _contextPropertySystem.TR_BookingHeader on x.bookingHeaderID equals a.Id
                                       where x.transNo == transNoTTBG
                                       select new CreateTrInventoryHeaderInputDto
                                       {
                                         accID = x.accID,
                                         bookingHeaderID = x.bookingHeaderID,
                                         entityID = x.entityID,
                                         ket = x.ket,
                                         transNo = x.transNo,
                                         TTBGDate = x.TTBGDate,
                                         bookCode = a.bookCode
                                       }).FirstOrDefault();

            var dataInventoryDetail = (from x in _contextPropertySystem.TR_InventoryDetail
                                       join a in _contextPropertySystem.LK_Status on x.statusID equals a.Id
                                       join b in _contextPropertySystem.MS_Bank on x.bankID equals b.Id
                                       join c in _contextPropertySystem.TR_InventoryHeader on x.inventoryHeaderID equals c.Id
                                       join d in _contextPropertySystem.LK_PayType on x.payTypeID equals d.Id
                                       where c.transNo == transNoTTBG
                                       select new CreateTrInventoryDetailInputDto
                                       {
                                           bankID = x.bankID,
                                           amount = x.amount,
                                           chequeNo = x.chequeNo,
                                           clearDate = x.clearDate,
                                           dueDate = x.dueDate,
                                           entityID = x.entityID,
                                           inventoryHeaderID = x.inventoryHeaderID,
                                           ket = x.ket,
                                           payForID = x.payForID,
                                           payNo = x.payNo,
                                           payTypeID = x.payTypeID,
                                           statusID = x.statusID,
                                           transNoPayment = x.transNoPayment,
                                           bankName = b.bankName,
                                           statusCode = a.statusCode,
                                           statusDesc = a.statusDesc,
                                           payTypeCode = d.payTypeCode,
                                           payTypeDesc = d.payTypeDesc
                                       }).ToList();

            var dataInventory = new CreateUniversalInputInventoryDto
            {
                inventoryHeader = dataInventoryHeader,
                dataInventoryDetail = dataInventoryDetail
            };

            return dataInventory;
        }

        public List<GetDataInventoryInputPaymentListDto> GetDataInventoryInputPayment(GetDataInventoryInputPaymentInputDto input)
        {
            var listResult = new List<GetDataInventoryInputPaymentListDto>();

            var getInventoryHeader = (from a in _contextPropertySystem.TR_InventoryHeader
                                      join b in _contextPropertySystem.TR_InventoryDetail on a.Id equals b.inventoryHeaderID
                                      join c in _contextPropertySystem.LK_PayFor on b.payForID equals c.Id
                                      join d in _contextPropertySystem.LK_Status on b.statusID equals d.Id
                                      where d.statusCode.Contains("R") && a.accID == input.accID && a.bookingHeaderID == input.bookingHeaderID && b.payForID == input.payForID
                                      select new
                                      {
                                          a.Id,
                                          a.transNo,
                                          a.TTBGDate
                                      })
                                      .Distinct()
                                      .ToList();
            
            foreach(var item in getInventoryHeader)
            {
                var getDataInventory = (from a in _contextPropertySystem.TR_InventoryHeader
                                        join b in _contextPropertySystem.TR_InventoryDetail on a.Id equals b.inventoryHeaderID
                                        join c in _contextPropertySystem.LK_PayFor on b.payForID equals c.Id
                                        join d in _contextPropertySystem.LK_Status on b.statusID equals d.Id
                                        join e in _contextPropertySystem.LK_PayType on b.payTypeID equals e.Id
                                        join f in _contextPropertySystem.MS_Bank on b.bankID equals f.Id
                                        where b.transNoPayment == "-" && a.transNo == item.transNo
                                        select new GetInventory
                                        {
                                            payNo = b.payNo,
                                            payTypeID = b.payTypeID,
                                            payTypeCode = e.payTypeCode,
                                            payTypeName = e.payTypeDesc,
                                            bankID = b.bankID,
                                            bankCode = f.bankCode,
                                            bankName = f.bankName,
                                            chequeNo = b.chequeNo,
                                            amount = b.amount,
                                            keterangan = b.ket,
                                            dueDate = b.dueDate
                                        }).ToList();

                var result = new GetDataInventoryInputPaymentListDto
                {
                    inventoryHeaderID = item.Id,
                    transNo = item.transNo,
                    tanggalTransNo = item.TTBGDate,
                    dataInventory = getDataInventory
                };

                listResult.Add(result);
            }
            return listResult;
        }

        public void UpdateTrInventoryDetail(UpdateTrInventoryDetailInputDto input)
        {
            Logger.Info("UpdateTrInventoryDetail() - Started.");
            
            var statusID = (from a in _contextPropertySystem.LK_Status
                            where a.statusCode == input.status
                            select a.Id).FirstOrDefault();

            var getDataInventory = (from x in _contextPropertySystem.TR_InventoryDetail
                                    where x.inventoryHeaderID == input.inventoryHeaderID && x.payNo == input.payNoInventory
                                    select x).FirstOrDefault();
            //var dataInventoryToUpdate = getDataInventory.MapTo<TR_InventoryDetail>();
            getDataInventory.statusID          = statusID;
            getDataInventory.clearDate         = input.clearDate;
            getDataInventory.transNoPayment    = input.transNo;

            try
            {
                Logger.DebugFormat("UpdateTrInventoryDetail() - Start update TR Inventory Detail. Parameters sent:{0}" +
                    "statusID = {1}{0}" +
                    "clearDate = {2}{0}" +
                    "transNoPayment = {3}{0}"
                    , Environment.NewLine, statusID, input.clearDate, input.transNo);

                _contextPropertySystem.TR_InventoryDetail.Attach(getDataInventory);
                Logger.DebugFormat("UpdateTrInventoryDetail() - Ended update TR Inventory Detail.");

                Logger.Info("UpdateTrInventoryDetail() - Finished.");
            }
            catch (DataException ex)
            {
                Logger.ErrorFormat("UpdateTrInventoryDetail() - ERROR DataException. Result = {0}", ex.Message);
                throw new UserFriendlyException("Db Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("UpdateTrInventoryDetail() - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Error: " + ex.Message);
            }
        }
    }
}
