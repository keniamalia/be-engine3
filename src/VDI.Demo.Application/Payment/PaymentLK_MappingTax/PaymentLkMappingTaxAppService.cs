﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using VDI.Demo.Payment.PaymentLK_MappingTax.Dto;
using VDI.Demo.PropertySystemDB.LippoMaster;
using Abp.AutoMapper;
using System.Data;
using Abp.UI;

namespace VDI.Demo.Payment.PaymentLK_MappingTax
{
    public class PaymentLkMappingTaxAppService : DemoAppServiceBase, IPaymentLkMappingTaxAppService
    {
        private readonly IRepository<LK_MappingTax> _lkMappingTaxRepo;
        private readonly IRepository<LK_PayFor> _lkPayForRepo;
        private readonly IRepository<LK_PayType> _lkPayTypeRepo;
        private readonly IRepository<LK_OthersType> _lkOthersTypeRepo;

        public PaymentLkMappingTaxAppService(
            IRepository<LK_MappingTax> lkMappingTaxRepo,
            IRepository<LK_PayFor> lkPayForRepo,
            IRepository<LK_PayType> lkPayTypeRepo,
            IRepository<LK_OthersType> lkOthersTypeRepo
        )
        {
            _lkMappingTaxRepo = lkMappingTaxRepo;
            _lkPayForRepo = lkPayForRepo;
            _lkPayTypeRepo = lkPayTypeRepo;
            _lkOthersTypeRepo = lkOthersTypeRepo;
        }

        public List<CreateOrUpdateLkMappingTaxDto> GetAllLkMappingTax()
        {
            Logger.Info("GetAllLkMappingTax() - Started.");
            var getAllMappingTax = (from x in _lkMappingTaxRepo.GetAll()
                                    join a in _lkPayForRepo.GetAll() on x.payForID equals a.Id
                                    join b in _lkPayTypeRepo.GetAll() on x.payTypeID equals b.Id
                                    join c in _lkOthersTypeRepo.GetAll() on x.othersTypeID equals c.Id
                                    select new CreateOrUpdateLkMappingTaxDto
                                    {
                                        Id = x.Id,
                                        payForID = a.Id,
                                        payTypeID = b.Id,
                                        othersTypeID = c.Id,
                                        payForName = a.payForName,
                                        payTypeName = b.payTypeDesc,
                                        othersTypeName = c.othersTypeDesc,
                                        isVAT = x.isVAT,
                                        isActive = x.isActive,
                                        payForCode = a.payForCode,
                                        payTypeCode = b.payTypeCode,
                                        othersTypeCode = c.othersTypeCode
                                    }).ToList();

            Logger.Info("GetAllLkMappingTax() - Finished.");
            return getAllMappingTax;
        }

        public void CreateLkMappingTax(CreateOrUpdateLkMappingTaxDto input)
        {
            Logger.InfoFormat("CreateLkMappingTax() - Started.");

            Logger.DebugFormat("CreateLkMappingTax(). Check duplicate payForID, payTypeID, othersTypeID. param sent: {0}" +
                "payForID     = {1}{0}" +
                "payTypeID     = {2}{0}" +
                "othersTypeID     = {3}{0}",
            Environment.NewLine, input.payForID, input.payTypeID, input.othersTypeID);

            var checkDuplicateData = (from x in _lkMappingTaxRepo.GetAll()
                                      where x.payForID == input.payForID && x.payTypeID == input.payTypeID && x.othersTypeID == input.othersTypeID
                                      select x).Any();

            Logger.DebugFormat("CreateLkMappingTax(). Check duplicate payForID, payTypeID, othersTypeID. Result isDuplicate: {0}", checkDuplicateData);

            if (!checkDuplicateData)
            {
                var dataCreateLkMappingTax = new LK_MappingTax
                {
                    payForID        = input.payForID,
                    payTypeID       = input.payTypeID,
                    othersTypeID    = input.othersTypeID,
                    isVAT           = input.isVAT,
                    isActive        = input.isActive
                };

                try
                {
                    Logger.DebugFormat("CreateLkMappingTax() - Start Insert LkMappingTax. Parameters sent: {0} " +
                           "   payForID         = {1}{0}" +
                           "   payTypeID        = {2}{0}" +
                           "   othersTypeID     = {3}{0}" +
                           "   isVAT            = {4}{0}" +
                           "   isActive         = {5}{0}" ,
                    Environment.NewLine, input.payForID, input.payTypeID, input.othersTypeID, input.isVAT, input.isActive);

                    _lkMappingTaxRepo.Insert(dataCreateLkMappingTax);
                    CurrentUnitOfWork.SaveChanges();

                    Logger.DebugFormat("CreateLkMappingTax() - End Insert LkMappingTax.");
                }
                catch (DataException exDb)
                {
                    Logger.DebugFormat("CreateLkMappingTax() - ERROR DataException. Result = {0}", exDb.Message);
                    throw new UserFriendlyException("Database Error : {0}", exDb.Message);
                }
                // Handle all other exceptions.
                catch (Exception ex)
                {
                    Logger.DebugFormat("CreateLkMappingTax() - ERROR Exception. Result = {0}", ex.Message);
                    throw new UserFriendlyException("Error : {0}", ex.Message);
                }
            }
            else
            {
                Logger.ErrorFormat("CreateLkMappingTax() - ERROR Exception. Result = {0}", "payForID, payTypeID, othersTypeID is Already Exist!");
                throw new UserFriendlyException("PayFor, PayType, OthersType is Already Exist!");
            }

            Logger.InfoFormat("CreateLkMappingTax() - End.");
        }

        public void UpdateLkMappingTax(CreateOrUpdateLkMappingTaxDto input)
        {
            Logger.Info("UpdateLkMappingTax() - Started.");
            Logger.DebugFormat("UpdateLkMappingTax() - Start checking before update Mapping Tax. Parameters sent:{0}" +
                        "mappingTaxID   = {1}{0}" +
                        "payForID       = {2}{0}" +
                        "payTypeID      = {3}{0}" +
                        "othersTypeID   = {4}{0}"
                        , Environment.NewLine, input.Id, input.payForID, input.payTypeID, input.othersTypeID);
            var checkDataExisting = (from x in _lkMappingTaxRepo.GetAll()
                                     where x.payForID == input.payForID && x.payTypeID == input.payTypeID && 
                                     x.othersTypeID == input.othersTypeID && x.Id != input.Id
                                     select x).Any();
            Logger.DebugFormat("UpdateLkMappingTax() - Ended checking LK_MappingTax before update Mapping Tax. Result = {0}", checkDataExisting);

            if (!checkDataExisting)
            {
                Logger.DebugFormat("UpdateLkMappingTax() - Start get data before update mapping tax. Parameters sent:{0}" +
                            "mappingTaxID = {1}{0}"
                            , Environment.NewLine, input.Id);

                var dataToUpdate = (from x in _lkMappingTaxRepo.GetAll()
                                    where x.Id == input.Id
                                    select x).FirstOrDefault();

                Logger.DebugFormat("UpdateMsBank() - Ended get data before update mapping tax.");

                dataToUpdate.payForID = input.payForID;
                dataToUpdate.payTypeID = input.payTypeID;
                dataToUpdate.othersTypeID = input.othersTypeID;
                dataToUpdate.isVAT = input.isVAT;
                dataToUpdate.isActive = input.isActive;
                try
                {
                    Logger.DebugFormat("UpdateLkMappingTax() - Start checking before update Mapping Tax. Parameters sent:{0}" +
                           "mappingTaxID   = {1}{0}" +
                           "payForID       = {2}{0}" +
                           "payTypeID      = {3}{0}" +
                           "othersTypeID   = {4}{0}" +
                           "isVAT          = {5}{0}" +
                           "isActive       = {6}{0}"
                           , Environment.NewLine, input.Id, input.payForID, input.payTypeID, input.othersTypeID, input.isVAT, input.isActive);

                    _lkMappingTaxRepo.Update(dataToUpdate);
                    CurrentUnitOfWork.SaveChanges();

                    Logger.DebugFormat("UpdateLkMappingTax() - Ended update mapping tax.");
                }
                catch (DataException ex)
                {
                    Logger.ErrorFormat("UpdateLkMappingTax() - ERROR DataException. Result = {0}", ex.Message);
                    throw new UserFriendlyException("Db Error: " + ex.Message);
                }
                catch (Exception ex)
                {
                    Logger.ErrorFormat("UpdateLkMappingTax() - ERROR Exception. Result = {0}", ex.Message);
                    throw new UserFriendlyException("Error: " + ex.Message);
                }
            }
            else
            {
                Logger.ErrorFormat("UpdateLkMappingTax() - ERROR Result = {0}.", "Combination of PayForID, payTypeID, and othersTypeID is Already Exist !");
                throw new UserFriendlyException("Combination of PayForID, payTypeID, and othersTypeID is Already Exist !");
            }

            Logger.Info("UpdateLkMappingTax() - Finished.");
        }
    }
}
