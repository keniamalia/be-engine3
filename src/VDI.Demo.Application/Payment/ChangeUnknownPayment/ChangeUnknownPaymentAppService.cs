﻿using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.UI;
using EFCore.BulkExtensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.Payment.ChangeUnknownPayment.Dto;
using VDI.Demo.Payment.InputPayment;
using VDI.Demo.Payment.InputPayment.Dto;
using VDI.Demo.PropertySystemDB.LippoMaster;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;
using VDI.Demo.PSAS.Schedule;
using VDI.Demo.PSAS.Schedule.Dto;

namespace VDI.Demo.Payment.ChangeUnknownPayment
{
    public class ChangeUnknownPaymentAppService : DemoAppServiceBase, IChangeUnknownPaymentAppService
    {
        private readonly IRepository<LK_Alloc> _lkAllocRepo;
        private readonly IRepository<LK_PayFor> _lkPayForRepo;
        private readonly IRepository<LK_PayType> _lkPayTypeRepo;
        private readonly IRepository<LK_OthersType> _lkOthersTypeRepo;
        private readonly IRepository<SYS_RolesPayFor> _sysRolesPayForRepo;
        private readonly IRepository<SYS_RolesPayType> _sysRolesPayTypeRepo;
        private readonly IRepository<SYS_RolesOthersType> _sysRolesOthersTypeRepo;
        private readonly PropertySystemDbContext _contextPropertySystem;
        private readonly AccountingDbContext _contextAccounting;
        private readonly TAXDbContext _contextTAX;
        private readonly PersonalsNewDbContext _contextPersonals;
        private readonly DemoDbContext _contextEngine3;
        private readonly IRepository<TR_PaymentHeader> _trPaymentHeaderRepo;
        private readonly IRepository<TR_BookingHeader> _trBookingHeaderRepo;
        private readonly IRepository<TR_PaymentDetail> _trPaymentDetailRepo;
        private readonly IRepository<TR_PaymentDetailAlloc> _trPaymentDetailAllocRepo;
        private readonly IRepository<TR_PaymentChangeUnknown> _trPaymentChangeUnknownRepo;
        private readonly IRepository<MS_Unit> _msUnitRepo;
        private readonly IRepository<MS_UnitCode> _msUnitCodeRepo;
        private readonly IRepository<MS_Project> _msProjectRepo;
        private readonly IRepository<MS_Area> _msAreaRepo;
        private readonly IRepository<MS_Category> _msCategoryRepo;
        private readonly IRepository<MS_Cluster> _msClusterRepo;
        private readonly IRepository<TR_BookingDetail> _trBookingDetailRepo;
        private readonly IRepository<TR_BookingDetailSchedule> _trBookingDetailScheduleRepo;
        private readonly IRepository<MS_Company> _msCompanyRepo;
        private readonly IRepository<MS_Account> _msAccountRepo;
        private readonly IInputPaymentAppService _iInputPaymentAppService;
        private readonly IPSASScheduleAppService _iPSASScheduleAppService;


        public ChangeUnknownPaymentAppService(
            IRepository<LK_Alloc> lkAllocRepo,
            IRepository<LK_PayFor> lkPayForRepo,
            IRepository<LK_PayType> lkPayTypeRepo,
            IRepository<LK_OthersType> lkOthersTypeRepo,
            IRepository<SYS_RolesPayFor> sysRolesPayForRepo,
            IRepository<SYS_RolesPayType> sysRolesPayTypeRepo,
            IRepository<SYS_RolesOthersType> sysRolesOthersTypeRepo,
            PropertySystemDbContext contextPropertySystem,
            AccountingDbContext contextAccounting,
            TAXDbContext contextTAX,
            PersonalsNewDbContext contextPersonals,
            DemoDbContext contextEngine3,
            IRepository<TR_PaymentHeader> trPaymentHeaderRepo,
            IRepository<TR_BookingHeader> trBookingHeaderRepo,
            IRepository<TR_PaymentDetail> trPaymentDetailRepo,
            IRepository<TR_PaymentDetailAlloc> trPaymentDetailAllocRepo,
            IRepository<TR_PaymentChangeUnknown> trPaymentChangeUnknownRepo,
            IRepository<MS_Unit> msUnitRepo,
            IRepository<MS_UnitCode> msUnitCodeRepo,
            IRepository<MS_Project> msProjectRepo,
            IRepository<MS_Area> msAreaRepo,
            IRepository<MS_Category> msCategoryRepo,
            IRepository<MS_Cluster> msClusterRepo,
            IRepository<TR_BookingDetail> trBookingDetailRepo,
            IRepository<TR_BookingDetailSchedule> trBookingDetailScheduleRepo,
            IRepository<MS_Company> msCompanyRepo,
            IRepository<MS_Account> msAccountRepo,
            IInputPaymentAppService iInputPaymentAppService,
            IPSASScheduleAppService iPSASScheduleAppService
            )
        {
            _lkAllocRepo = lkAllocRepo;
            _lkPayForRepo = lkPayForRepo;
            _lkPayTypeRepo = lkPayTypeRepo;
            _lkOthersTypeRepo = lkOthersTypeRepo;
            _sysRolesPayForRepo = sysRolesPayForRepo;
            _sysRolesPayTypeRepo = sysRolesPayTypeRepo;
            _sysRolesOthersTypeRepo = sysRolesOthersTypeRepo;
            _contextPropertySystem = contextPropertySystem;
            _contextAccounting = contextAccounting;
            _contextTAX = contextTAX;
            _contextPersonals = contextPersonals;
            _contextEngine3 = contextEngine3;
            _trPaymentHeaderRepo = trPaymentHeaderRepo;
            _trBookingHeaderRepo = trBookingHeaderRepo;
            _trPaymentDetailRepo = trPaymentDetailRepo;
            _trPaymentDetailAllocRepo = trPaymentDetailAllocRepo;
            _trPaymentChangeUnknownRepo = trPaymentChangeUnknownRepo;
            _msUnitRepo = msUnitRepo;
            _msUnitCodeRepo = msUnitCodeRepo;
            _msProjectRepo = msProjectRepo;
            _msAreaRepo = msAreaRepo;
            _msCategoryRepo = msCategoryRepo;
            _msClusterRepo = msClusterRepo;
            _trBookingDetailRepo = trBookingDetailRepo;
            _trBookingDetailScheduleRepo = trBookingDetailScheduleRepo;
            _msCompanyRepo = msCompanyRepo;
            _msAccountRepo = msAccountRepo;
            _iInputPaymentAppService = iInputPaymentAppService;
            _iPSASScheduleAppService = iPSASScheduleAppService;
        }
        public decimal GetDataOutstandingSchedule(GetDataOutstandingInputDto input)
        {
            var getDataOutstanding = (from a in _trBookingDetailScheduleRepo.GetAll()
                                      join b in _trBookingDetailRepo.GetAll() on a.bookingDetailID equals b.Id
                                      join c in _trBookingHeaderRepo.GetAll() on b.bookingHeaderID equals c.Id
                                      join d in _lkAllocRepo.GetAll() on a.allocID equals d.Id
                                      where c.Id == input.bookingHeaderID && input.itemID.Contains(b.itemID) && d.payForID == input.payForID
                                      group a by new
                                      {
                                          c.Id
                                      } into G
                                      select G.Sum(a => a.netOut + a.vatOut)).FirstOrDefault();

            return getDataOutstanding;
        }

        public void CreateUniversalChangeUnknown(CreateUniversalChangeUnknownInputDto input)
        {
            Logger.Info("CreateUniversalChangeUnknown() - Started.");

            Logger.DebugFormat("CreateUniversalChangeUnknown() - Start get data schedule. Parameters sent:{0}" +
                    "bookingHeaderID        = {1}{0}" +
                    "itemID                 = {2}{0}" +
                    "payForID               = {3}{0}"
                    , Environment.NewLine, input.bookingHeaderID, input.itemID, input.payForID);
            var getDataSchedule = (from x in _contextPropertySystem.TR_BookingDetailSchedule
                                   join a in _contextPropertySystem.TR_BookingDetail on x.bookingDetailID equals a.Id
                                   join b in _contextPropertySystem.TR_BookingHeader on a.bookingHeaderID equals b.Id
                                   join d in _contextPropertySystem.LK_Alloc on x.allocID equals d.Id
                                   where b.Id == input.bookingHeaderID && input.itemID.Contains(a.itemID) && d.payForID == input.payForID
                                   //orderby new { x.schedNo, x.bookingDetailID}
                                   select x).OrderBy(x => x.schedNo).ThenBy(n => n.bookingDetailID).ToList();
            Logger.DebugFormat("CreateUniversalChangeUnknown() - Ended get data schedule. Result = {0}", getDataSchedule.Count());


            var accountCode = (from a in _contextPropertySystem.TR_PaymentHeader
                               join b in _contextPropertySystem.MS_Account on a.accountID equals b.Id
                               where a.accountID == input.accountID
                               select new { b.accCode, b.devCode }).FirstOrDefault();

            var bookCode = (from A in _trBookingHeaderRepo.GetAll() where A.Id == input.bookingHeaderID select A.bookCode).FirstOrDefault();

            var paymentHeaderID = (from a in _contextPropertySystem.TR_PaymentHeader
                                   where a.transNo == input.controlNo
                                   select a.Id).FirstOrDefault();

            var totalPaymentChangeUnknown = input.dataPaymentDetail.Count() + 2;

            var dataInputJournalCode = new GenerateJurnalInputDto
            {
                accCode = accountCode.accCode,
                bookCode = bookCode == null ? "-" : bookCode,
                coCode = accountCode.devCode,
                totalPaymentDetail = totalPaymentChangeUnknown
            };

            var journalCode = _iInputPaymentAppService.GenerateJurnalCode(dataInputJournalCode);

            Logger.Info("CreateUniversalChangeUnknown() - Started Insert TR Payment Change Unknown.");
            var dataPaymentChangeUnknown = new CreateTrPaymentChangeUnknownInputDto
            {
                entityID = 1,
                paymentHeaderID = paymentHeaderID,
                changeNo = accountCode.accCode + DateTime.Now.ToString("yyMMddHHmmssfff"),
                changeDate = input.changeDate,
                reason = input.reason
            };
            CreateTrPaymentChangeUnknown(dataPaymentChangeUnknown);
            Logger.Info("CreateUniversalChangeUnknown() - Finished Insert TR Payment Change Unknown.");

            Logger.Info("CreateUniversalChangeUnknown() - Started Step 1.");
            //insert pembalik unknown
            Logger.DebugFormat("CreateUniversalChangeUnknown() - Start get data for insert pembalik unknown. Parameters sent:{0}" +
                    "controlNo        = {1}{0}" +
                    "amount           = {2}{0}"
                    , Environment.NewLine, input.controlNo, null);
            var dataPaymentPembalik = getDataInputPayment(input.controlNo, null, journalCode.GetValue("journalCodeStart").ToString());
            Logger.DebugFormat("CreateUniversalChangeUnknown() - Ended get data for insert pembalik unknown.");
            Logger.Info("CreateUniversalChangeUnknown() - Started Insert Pembalik Step 1.");
            _iInputPaymentAppService.CreateInputPaymentUniversal(dataPaymentPembalik);
            Logger.Info("CreateUniversalChangeUnknown() - Finished Insert Pembalik Step 1.");
            Logger.Info("CreateUniversalChangeUnknown() - Finished Step 1.");

            var dataInputTransNo = new GenerateTransNoInputDto
            {
                accID = input.accountID,
                entityID = 1
            };

            Logger.DebugFormat("CreateUniversalChangeUnknown() - Start generate transNo. Parameters sent:{0}" +
                    "entityID        = {1}{0}" +
                    "accountID           = {2}{0}"
                    , Environment.NewLine, 1, input.accountID);
            var transNo = _iPSASScheduleAppService.GenerateTransNo(dataInputTransNo);
            Logger.DebugFormat("CreateUniversalChangeUnknown() - Ended generate transNo.");

            var scheduleTemp = getDataSchedule;
            var updateSchedule = new List<TR_BookingDetailSchedule>();
            var dtoCreatePaymentDetail = new CreatePaymentDetail();
            var detail = new List<CreatePaymentDetail>();

            //untuk payFor Other, Unknown, UOP, UOA
            if (input.bookingHeaderID == null)
            {
                foreach (var payment in input.dataPaymentDetail)
                {
                    var alloc = new List<CreatePaymentAlloc>();
                    var singleAlloc = new CreatePaymentAlloc()
                    {
                        schedNo = 0,
                        netAmt = payment.totalAmountDetail,
                        netOut = 0,
                        vatAmt = 0,
                        vatOut = 0
                    };
                    alloc.Add(singleAlloc);

                    //payNo = payment.payNo + 1;

                    dtoCreatePaymentDetail = new CreatePaymentDetail
                    {
                        chequeNo = payment.chequeNo,
                        bankName = payment.bankName,
                        dueDate = payment.dueDate,
                        ket = payment.ket,
                        payNo = payment.payNo,
                        payTypeID = payment.payTypeID,
                        othersTypeCode = payment.othersTypeCode,
                        ORToAdjust = input.controlNo,
                        totalAmountDetail = payment.totalAmountDetail,
                        dataAlloc = alloc
                    };
                    detail.Add(dtoCreatePaymentDetail);
                }
            }
            else
            {
                //insert payment 
                foreach (var payment in input.dataPaymentDetail)
                {
                    var amtPay = payment.totalAmountDetail;
                    var schedTemp = new TR_BookingDetailSchedule();
                    var scheduleUpdate = new List<TR_BookingDetailSchedule>();
                    var dtoCreatePaymentAlloc = new CreatePaymentAlloc();
                    var alloc = new List<CreatePaymentAlloc>();
                    //payNo = payment.payNo + 1;

                    foreach (var schedule in scheduleTemp)
                    {
                        if (amtPay > 0 && (schedule.netOut + schedule.vatOut) > 0)
                        {
                            var amtPaySisa = amtPay;
                            amtPay = amtPay - (schedule.netOut + schedule.vatOut);

                            //update schedule
                            schedTemp = schedule;
                            if (amtPay < 0)
                            {
                                dtoCreatePaymentAlloc = new CreatePaymentAlloc
                                {
                                    schedNo = schedule.schedNo,
                                    netOut = 0,
                                    vatOut = 0,
                                    netAmt = (amtPaySisa < payment.totalAmountDetail ? amtPaySisa : payment.totalAmountDetail) / (1 + (decimal)input.pctTax),
                                    vatAmt = (amtPaySisa < payment.totalAmountDetail ? amtPaySisa : payment.totalAmountDetail) / (1 + (decimal)input.pctTax) * (decimal)input.pctTax
                                };

                                schedTemp.netOut = (amtPay * -1) / (1 + (decimal)input.pctTax);
                                schedTemp.vatOut = ((amtPay * -1) / (1 + (decimal)input.pctTax)) * (decimal)input.pctTax;
                            }
                            else
                            {
                                dtoCreatePaymentAlloc = new CreatePaymentAlloc
                                {
                                    schedNo = schedule.schedNo,
                                    netOut = 0,
                                    vatOut = 0,
                                    netAmt = schedule.netOut,
                                    vatAmt = schedule.vatOut
                                };

                                schedTemp.netOut = 0;
                                schedTemp.vatOut = 0;
                            }
                            //end update schedule
                            updateSchedule.Add(schedTemp);
                            //menjumlah alloc per item
                            if (alloc.LastOrDefault() != null && alloc.LastOrDefault().schedNo == schedule.schedNo)
                            {
                                alloc.Where(x => x.schedNo == schedule.schedNo).ToList().ForEach(u =>
                                {
                                    u.netAmt = u.netAmt + dtoCreatePaymentAlloc.netAmt;
                                    u.vatAmt = u.vatAmt + dtoCreatePaymentAlloc.vatAmt;
                                }
                                );
                            }
                            else
                            {
                                alloc.Add(dtoCreatePaymentAlloc);
                            }
                        }
                        scheduleUpdate.Add(schedTemp);

                    }
                    scheduleTemp = scheduleUpdate;

                    dtoCreatePaymentDetail = new CreatePaymentDetail
                    {
                        chequeNo = payment.chequeNo,
                        bankName = payment.bankName,
                        dueDate = payment.dueDate,
                        ket = payment.ket,
                        payNo = payment.payNo,
                        payTypeID = payment.payTypeID,
                        othersTypeCode = payment.othersTypeCode,
                        ORToAdjust = input.controlNo,
                        totalAmountDetail = payment.totalAmountDetail,
                        dataAlloc = alloc
                    };

                    detail.Add(dtoCreatePaymentDetail);
                }
            }

            var sisaAmount = input.maxAmount - input.amountTotal; // - totalAmountAllocated; 

            var journalData = journalCode.GetValue("journalCodeStart").ToString().Split(".");
            var formatJournal = journalData[0] + "." + journalData[1] + "." + journalData[2] + "." + journalData[3] + "." + journalData[4];

            int runningNumberExisting = Convert.ToInt32(journalData[5]) + 1;
            var runningNumber = runningNumberExisting.ToString("D5");

            var journalCodeResult = formatJournal + "." + runningNumber;

            Logger.Info("CreateUniversalChangeUnknown() - Started Step 2.");
            Logger.DebugFormat("CreateUniversalChangeUnknown() - Start get data for insert sisa amount unknown. Parameters sent:{0}" +
                "accountID            = {1}{0}" +
                "bookingHeaderID      = {2}{0}" +
                "clearDate            = {3}{0}" +
                "payForID             = {4}{0}" +
                "paymentDate          = {5}{0}" +
                "description          = {6}{0}" +
                "pctTax               = {7}{0}" +
                "address              = {8}{0}" +
                "name                 = {9}{0}" +
                "NPWP                 = {10}{0}" +
                "PPNo                 = {11}{0}" +
                "psCode               = {12}{0}" +
                "transNo              = {13}{0}" +
                "unitCode             = {14}{0}" +
                "unitNo               = {15}{0}" +
                "isSaveChanges        = {16}{0}" +
                "dataPaymentDetail    = {17}{0}"
                , Environment.NewLine, input.accountID, input.bookingHeaderID, input.clearDate, input.payForID
                , input.paymentDate, input.description, input.pctTax, input.address, input.name, input.NPWP, input.PPNo
                , input.psCode, transNo.GetValue("transNo").ToString(), input.unitCode, input.unitNo, false, detail);
            var dtoCreateUniversal = new CreateInputPaymentUniversalInputDto
            {
                accountID = input.accountID,
                bookingHeaderID = input.bookingHeaderID,
                clearDate = input.clearDate,
                payForID = input.payForID,
                paymentDate = input.paymentDate,
                description = input.description,
                pctTax = input.pctTax,
                address = input.address,
                name = input.name,
                NPWP = input.NPWP,
                PPNo = input.PPNo,
                psCode = input.psCode,
                transNo = transNo.GetValue("transNo").ToString(),
                unitCode = input.unitCode,
                unitNo = input.unitNo,
                isSaveChanges = false,
                journalCode = journalCodeResult,
                dataPaymentDetail = detail
            };
            Logger.DebugFormat("CreateUniversalChangeUnknown() - Ended get data for insert sisa amount unknown.");
            Logger.Info("CreateUniversalChangeUnknown() - Started Insert Payment change unknown Step 2.");
            _iInputPaymentAppService.CreateInputPaymentUniversal(dtoCreateUniversal);
            Logger.Info("CreateUniversalChangeUnknown() - Finished Insert Payment change unknown Step 2.");
            Logger.Info("CreateUniversalChangeUnknown() - Finished Step 2.");

            if (sisaAmount > 0)
            {
                journalData = journalCode.GetValue("journalCodeStart").ToString().Split(".");
                formatJournal = journalData[0] + "." + journalData[1] + "." + journalData[2] + "." + journalData[3] + "." + journalData[4];

                runningNumberExisting = Convert.ToInt32(journalData[5]) + input.dataPaymentDetail.Count() + 1;
                runningNumber = runningNumberExisting.ToString("D5");

                journalCodeResult = formatJournal + "." + runningNumber;

                Logger.Info("CreateUniversalChangeUnknown() - Started Step 3.");
                Logger.DebugFormat("CreateUniversalChangeUnknown() - Start get data for insert sisa amount unknown. Parameters sent:{0}" +
                    "controlNo        = {1}{0}" +
                    "amount           = {2}{0}"
                    , Environment.NewLine, input.controlNo, sisaAmount);
                var dataPaymentSisa = getDataInputPayment(input.controlNo, sisaAmount, journalCodeResult);
                Logger.DebugFormat("CreateUniversalChangeUnknown() - Ended get data for insert sisa amount unknown.");
                Logger.Info("CreateUniversalChangeUnknown() - Started Insert Sisa Payment Step 3.");
                _iInputPaymentAppService.CreateInputPaymentUniversal(dataPaymentSisa);
                Logger.Info("CreateUniversalChangeUnknown() - Finished Insert Sisa Payment Step 3.");
                Logger.Info("CreateUniversalChangeUnknown() - Finished Step 3.");
            }

            Logger.DebugFormat("CreateUniversalChangeUnknown() - Start update controlNo TR Payment Header. Parameters sent:{0}" +
                    "controlNo        = {1}{0}"
                    , Environment.NewLine, input.controlNo);
            _iInputPaymentAppService.UpdateTrPaymentHeader(input.controlNo, input.controlNo);
            Logger.DebugFormat("CreateUniversalChangeUnknown() - Ended update controlNo TR Payment Header.");

            foreach (var scheduleUpdated in updateSchedule)
            {
                Logger.DebugFormat("CreateUniversalChangeUnknown() - Start update schedule. Parameters sent:{0}" +
                    "schedule        = {1}{0}"
                    , Environment.NewLine, scheduleUpdated);
                _trBookingDetailScheduleRepo.Update(scheduleUpdated);
                Logger.DebugFormat("CreateUniversalChangeUnknown() - Ended update schedule.");
            }

            _contextPropertySystem.SaveChanges();
            _contextAccounting.SaveChanges();
            _contextTAX.SaveChanges();
            CurrentUnitOfWork.SaveChanges();
            Logger.Info("CreateUniversalChangeUnknown() - Finished.");
        }

        private CreateInputPaymentUniversalInputDto getDataInputPayment(string transNo, decimal? amount, string journalCode)
        {
            var result = new CreateInputPaymentUniversalInputDto();

            var getDataHeader = (from a in _contextPropertySystem.TR_PaymentHeader
                                 where a.transNo == transNo
                                 select new
                                 {
                                     paymentHeaderID = a.Id,
                                     a.PPNo,
                                     a.bookingHeaderID,
                                     a.clearDate,
                                     a.payForID,
                                     a.paymentDate,
                                     description = a.ket,
                                     a.accountID
                                 }).FirstOrDefault();

            var newTransNo = _iPSASScheduleAppService.GenerateTransNo(new GenerateTransNoInputDto() { accID = getDataHeader.accountID, entityID = 1 });

            var getDataPaymentDetail = (from a in _trPaymentDetailRepo.GetAll()
                                        join b in _lkPayTypeRepo.GetAll() on a.payTypeID equals b.Id
                                        where a.paymentHeaderID == getDataHeader.paymentHeaderID
                                        select new
                                        {
                                            paymentDetailID = a.Id,
                                            a.payNo,
                                            b.payTypeCode,
                                            payTypeID = b.Id,
                                            a.bankName,
                                            a.chequeNo,
                                            a.status,
                                            a.ket,
                                            a.dueDate,
                                            a.othersTypeCode
                                        }).FirstOrDefault();//.ToList();

            List<CreatePaymentDetail> listPayment = new List<CreatePaymentDetail>();

            //foreach (var dataPaymentDetail in getDataPaymentDetail)
            //{
            var dataAlloc = new List<CreatePaymentAlloc>();
            decimal totalAmount;
            if (amount == null)
            {
                var getDataPerPaymentDetail = (from a in _trPaymentDetailAllocRepo.GetAll()
                                               where a.paymentDetailID == getDataPaymentDetail.paymentDetailID
                                               select new
                                               {
                                                   a.paymentDetailID,
                                                   a.netAmt,
                                                   a.vatAmt,
                                                   a.schedNo
                                               }).ToList();

                var getDataTotalPayment = (from x in getDataPerPaymentDetail
                                           group x by new
                                           {
                                               x.paymentDetailID
                                           } into G
                                           select new
                                           {
                                               amount = G.Sum(x => x.netAmt) + G.Sum(x => x.vatAmt),
                                               netAlloc = G.Sum(x => x.netAmt)
                                           }).FirstOrDefault();

                totalAmount = getDataTotalPayment.amount * -1;

                dataAlloc = getDataPerPaymentDetail.Select(x => new CreatePaymentAlloc
                {
                    netAmt = x.netAmt * -1,
                    vatAmt = x.vatAmt * -1,
                    schedNo = x.schedNo,
                    netOut = 0,
                    vatOut = 0
                }).ToList();
            }
            else
            {
                var dataSingleAlloc = new CreatePaymentAlloc
                {
                    netAmt = (decimal)amount,
                    vatAmt = 0,
                    schedNo = 0,
                    netOut = 0,
                    vatOut = 0
                };

                totalAmount = (decimal)amount;
                dataAlloc.Add(dataSingleAlloc);
            }


            var dataPayment = new CreatePaymentDetail
            {
                payNo = 1,
                payTypeID = getDataPaymentDetail.payTypeID,
                ORToAdjust = amount == null ? transNo : null, //need to update
                othersTypeCode = getDataPaymentDetail.othersTypeCode,
                bankName = getDataPaymentDetail.bankName,
                chequeNo = getDataPaymentDetail.chequeNo,
                dataAlloc = dataAlloc,
                ket = getDataPaymentDetail.ket,
                dueDate = getDataPaymentDetail.dueDate,
                totalAmountDetail = totalAmount
            };

            listPayment.Add(dataPayment);
            //}

            return result = new CreateInputPaymentUniversalInputDto
            {
                accountID = getDataHeader.accountID,
                bookingHeaderID = getDataHeader.bookingHeaderID,
                clearDate = getDataHeader.clearDate,
                payForID = getDataHeader.payForID,
                paymentDate = getDataHeader.paymentDate,
                PPNo = getDataHeader.PPNo,
                isSaveChanges = false,
                description = getDataHeader.description,
                transNo = newTransNo.GetValue("transNo").ToString(),
                journalCode = journalCode,
                dataPaymentDetail = listPayment
            };
        }

        public void CreateTrPaymentChangeUnknown(CreateTrPaymentChangeUnknownInputDto input)
        {
            Logger.Info("CreateTrPaymentChangeUnknown() - Started.");

            var data = new TR_PaymentChangeUnknown
            {
                entityID        = input.entityID,
                paymentHeaderID = input.paymentHeaderID,
                changeDate      = input.changeDate,
                changeNo        = input.changeNo,
                reason          = input.reason
            };

            try
            {
                Logger.DebugFormat("CreateTrPaymentChangeUnknown() - Start insert TR Payment Change Unknown. Parameters sent:{0}" +
                    "entityID       = {1}{0}" +
                    "paymentHeaderID= {2}{0}" +
                    "changeDate     = {3}{0}" +
                    "changeNo       = {4}{0}" +
                    "reason         = {5}{0}"
                    , Environment.NewLine, input.entityID, input.paymentHeaderID, input.changeDate, input.changeNo, input.reason);

                _trPaymentChangeUnknownRepo.Insert(data);
                Logger.DebugFormat("CreateTrPaymentChangeUnknown() - Ended insert TR Payment Change Unknown.");
                Logger.Info("CreateTrPaymentChangeUnknown() - Finished.");
            }
            catch (DataException ex)
            {
                Logger.ErrorFormat("CreateTrPaymentChangeUnknown() - ERROR DataException. Result = {0}", ex.Message);
                Logger.Info("CreateTrPaymentChangeUnknown() - Finished.");
                throw new UserFriendlyException("Db Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("CreateTrPaymentChangeUnknown() - ERROR Exception. Result = {0}", ex.Message);
                Logger.Info("CreateTrPaymentDetailAlloc() - Finished.");
                throw new UserFriendlyException("Error: " + ex.Message);
            }
        }
    }
}
