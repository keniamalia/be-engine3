﻿using Abp.Collections.Extensions;
using Abp.Extensions;
using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using VDI.Demo.DataExporting.Pdf.OR;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.Payment.InputPayment;
using VDI.Demo.Payment.InputPayment.Dto;
using VDI.Demo.Payment.OfficialReceiptPayment.Dto;

namespace VDI.Demo.Payment.OfficialReceiptPayment
{
    public class OfficialReceiptPaymentAppService : DemoAppServiceBase, IOfficialReceiptPaymentAppService
    {
        private readonly PropertySystemDbContext _contextPropertySystem;
        private readonly AccountingDbContext _contextAccounting;
        private readonly TAXDbContext _contextTAX;
        private readonly PersonalsNewDbContext _contextPersonals;
        private readonly DemoDbContext _contextEngine3;
        private readonly IInputPaymentAppService _iInputPaymentAppService;
        private readonly IOfficialReceiptGeneratePdfExporter _iORExporter;
        private readonly IHostingEnvironment _hostingEnvironment;

        public OfficialReceiptPaymentAppService(
            PropertySystemDbContext contextPropertySystem,
            AccountingDbContext contextAccounting,
            TAXDbContext contextTAX,
            PersonalsNewDbContext contextPersonals,
            DemoDbContext contextEngine3,
            IInputPaymentAppService iInputPaymentAppService,
            IOfficialReceiptGeneratePdfExporter iORExporter,
            IHostingEnvironment hostingEnvironment
            )
        {
            _contextPropertySystem = contextPropertySystem;
            _contextAccounting = contextAccounting;
            _contextTAX = contextTAX;
            _contextPersonals = contextPersonals;
            _contextEngine3 = contextEngine3;
            _iInputPaymentAppService = iInputPaymentAppService;
            _iORExporter = iORExporter;
            _hostingEnvironment = hostingEnvironment;
        }

        public List<GetDataPaymentListDto> GetDataPaymentSearchingOfficialReceipt(GetDataPaymentSearchingInputDto input)
        {
            List<GetDataPaymentListDto> listResult = new List<GetDataPaymentListDto>();
            var getParamPayFor = (from a in _contextPropertySystem.MS_Parameter
                                  where a.code == "PFP"
                                  select a.value).FirstOrDefault();

            var getParamPayType = (from a in _contextPropertySystem.LK_PayType
                                  where a.payTypeCode == "ADJ"
                                  select a.Id).FirstOrDefault();

            var getDataPayment = (from a in _contextPropertySystem.TR_PaymentHeader
                                  join pd in _contextPropertySystem.TR_PaymentDetail on a.Id equals pd.paymentHeaderID
                                  //join pda in _contextPropertySystem.TR_PaymentDetailAlloc on pd.Id equals pda.paymentDetailID
                                  join b in _contextPropertySystem.TR_BookingHeader on a.bookingHeaderID equals b.Id
                                  join c in _contextPropertySystem.MS_Unit on b.unitID equals c.Id
                                  join d in _contextPropertySystem.MS_Project on c.projectID equals d.Id
                                  join e in _contextPropertySystem.MS_Cluster on c.clusterID equals e.Id
                                  join f in _contextPropertySystem.MS_UnitCode on c.unitCodeID equals f.Id
                                  join h in _contextPropertySystem.LK_PayFor on a.payForID equals h.Id
                                  join i in _contextPropertySystem.MS_Account on a.accountID equals i.Id
                                  join j in _contextPropertySystem.MS_Company on i.devCode equals j.coCode
                                  where h.payForCode.Contains(getParamPayFor) && pd.payTypeID != getParamPayType
                                  orderby a.paymentDate descending
                                  select new
                                  {
                                      projectID = c.projectID,
                                      project = d.projectName,
                                      clusterID = e.Id,
                                      cluster = e.clusterName,
                                      psCode = b.psCode,
                                      payDate = a.paymentDate,
                                      transNo = a.transNo,
                                      unitID = c.Id,
                                      unitNo = c.unitNo,
                                      unitCode = f.unitCode,
                                      payFor = h.payForName,
                                      accountID = a.accountID,
                                      accountNo = i.accNo,
                                      companyAddress = j.address
                                  })
                                  .WhereIf(input.projectID != 0, item => item.projectID == input.projectID)
                                  .WhereIf(input.clusterID != 0, item => item.clusterID == input.clusterID)
                                  .WhereIf(input.unitID != 0, item => item.unitID == input.unitID)
                                  .WhereIf(!input.psCode.IsNullOrWhiteSpace(), item => item.psCode == input.psCode)
                                  .WhereIf(input.paymentStart != null, item => item.payDate.Date >= input.paymentStart.Value.Date)
                                  .WhereIf(input.paymentEnd != null, item => item.payDate.Date <= input.paymentEnd.Value.Date)
                                  .ToList();

            foreach(var data in getDataPayment)
            {
                var getDataPersonal = (from a in _contextPersonals.PERSONAL
                                       join k in _contextPersonals.TR_Email.ToList() on a.psCode equals k.psCode into email
                                       from k in email.DefaultIfEmpty()
                                       where a.psCode == data.psCode
                                       select new
                                       {
                                           custName = a.name,
                                           email = k == null ? null : k.email
                                       }
                                       ).FirstOrDefault();
                var search = new SearchPdfORInputDto
                {
                    transNo = data.transNo,
                    unitCode = data.unitCode,
                    unitNo = data.unitNo,
                    flag = 1
                };
                var document = _iInputPaymentAppService.SearchPdf(search);

                var dataResult = new GetDataPaymentListDto
                {
                    projectID = data.projectID,
                    transNo = data.transNo,
                    project = data.project,
                    cluster = data.cluster,
                    paymentDate = data.payDate,
                    custName = getDataPersonal == null ? null : getDataPersonal.custName,
                    payFor = data.payFor,
                    accountID = data.accountID,
                    accountNo = data.accountNo,
                    companyAddress = data.companyAddress,
                    email = getDataPersonal == null ? null : getDataPersonal.email,
                    unitCode = data.unitCode,
                    unitNo = data.unitNo,
                    document = document
                };
                listResult.Add(dataResult);
            }

            return listResult;
        }

        public JObject OfficialReceiptPrint(List<string> input)
        {
            JObject obj = new JObject();
            var destinationPath = Path.Combine(_hostingEnvironment.WebRootPath, @"Assets\Upload\PrintOR");
            var filePathResult = _iORExporter.PrintORGeneratePdfFromListFile(input, destinationPath, @"Assets\Upload\PrintOR");
            obj.Add("filePath", filePathResult);

            return obj;
        }
    }
}
