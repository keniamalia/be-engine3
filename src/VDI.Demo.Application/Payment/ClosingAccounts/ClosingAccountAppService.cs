﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using VDI.Demo.Payment.ClosingAccounts.Dto;
using VDI.Demo.PropertySystemDB.LippoMaster;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;
using Abp.Extensions;
using VDI.Demo.OracleStageDB;
using VDI.Demo.AccountingDB;
using VDI.Demo.EntityFrameworkCore;
using Abp.UI;
using System.Data;
using System.IO;
using Newtonsoft.Json.Linq;
using Visionet_Backend_NetCore.Komunikasi;
using System.Net.Mail;
using Microsoft.AspNetCore.Hosting;
using Abp.Net.Mail;
using Abp.Authorization;
using VDI.Demo.Authorization;

namespace VDI.Demo.Payment.ClosingAccounts
{
    public class ClosingAccountAppService : DemoAppServiceBase, IClosingAccountAppService
    {
        private readonly PropertySystemDbContext _contextPropertySystem;
        private readonly AccountingDbContext _contextAccounting;
        private readonly OracleStageDbContext _contextOracleStage;
        private readonly DemoDbContext _contextEngine3;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IEmailSender _emailSender;

        public ClosingAccountAppService(
            PropertySystemDbContext contextPropertySystem,
            AccountingDbContext contextAccounting,
            OracleStageDbContext contextOracleStage,
            DemoDbContext contextEngine3,
            IHostingEnvironment hostingEnvironment,
            IEmailSender emailSender
            )
        {
            _contextPropertySystem = contextPropertySystem;
            _contextAccounting = contextAccounting;
            _contextOracleStage = contextOracleStage;
            _contextEngine3 = contextEngine3;
            _hostingEnvironment = hostingEnvironment;
            _emailSender = emailSender;
        }

        private string GetIdUsername(long? Id)
        {
            string getUsername = (from u in _contextEngine3.Users
                                  where u.Id == Id
                                  select u.UserName)
                       .DefaultIfEmpty("").First();

            return getUsername;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Payment_ClosingAccount)]
        public void ClosingAccountUniversal(ClosingAccountInputDto input)
        {
            Logger.Info("ClosingAccountUniversal() - Started.");

            var listAccountCodeMV = new List<string>
            {
                "MV1", "MV2","MV3"
            };

            var listAccountCodeHL = new List<string>
            {
                "HL1", "HL2"
            };

            foreach (var data in input.accountData)
            {
                var getTransNo = (from x in _contextPropertySystem.TR_PaymentHeader
                                  where x.accountID == data.accountID && (x.clearDate == input.transactionCutOffDate || x.paymentDate == input.transactionCutOffDate)
                                  select x.transNo).ToList();

                int getGroupID = Convert.ToInt32((from x in _contextOracleStage.LK_GL_INTERFACE
                                                  where x.ACC_CODE == data.accountCode && x.ACCOUNTING_DATE == input.transactionCutOffDate.ToString("dd/MM/yyyy")
                                                  orderby x.GROUP_ID descending
                                                  select x == null ? "1" : x.GROUP_ID).FirstOrDefault()) == 1 ? 1 : +1;

                if (getTransNo != null && getTransNo.Count != 0)
                {
                    var getSegment4Segment7 = new GetDataSegment4ListDto();
                    var getSegment6 = "";
                    var getJournalCode = (from x in _contextAccounting.TR_PaymentDetailJournal
                                          where getTransNo.Contains(x.transNo)
                                          select x.journalCode).ToList();
                    var getCoaCodeFin = (from a in _contextAccounting.TR_Journal
                                         where getJournalCode.Contains(a.journalCode)
                                         group a by new
                                         {
                                             a.COACodeFIN
                                         } into G
                                         select new
                                         {
                                             G.Key.COACodeFIN,
                                             debit = G.Sum(x => x.debit),
                                             kredit = G.Sum(x => x.kredit)
                                         }).ToList();

                    var getDataProject = (from a in _contextPropertySystem.MS_Account
                                          join b in _contextPropertySystem.MS_Project on a.projectID equals b.Id
                                          where a.Id == data.accountID
                                          select new { b.BIZ_UNIT_ID, b.PROJECT_ID }).FirstOrDefault();

                    var getDataAccount = (from a in _contextPropertySystem.MS_Account
                                          where a.Id == data.accountID
                                          select new { a.NATURE_ACCOUNT_BANK, a.NATURE_ACCOUNT_DEP, a.ORG_ID, a.PROVINCE_ID, a.accCode }).FirstOrDefault();

                    foreach (var coaCodeFin in getCoaCodeFin)
                    {
                        var listCoaCodeFin1 = new List<string>
                        {
                            "BNK", "SBS"
                        };

                        var listCoaCodeFin2 = new List<string>
                        {
                            "DEP", "DPR", "DPS", "ESW"
                        };

                        if (listCoaCodeFin1.Contains(coaCodeFin.COACodeFIN))
                        {
                            getSegment4Segment7 = GetDataSegment4(new GetDataSegment4InputDto { coaCodeFin = coaCodeFin.COACodeFIN, flag = 1, natureAccount = getDataAccount.NATURE_ACCOUNT_BANK });
                            getSegment6 = getDataAccount.NATURE_ACCOUNT_BANK;
                        }
                        else if (coaCodeFin.COACodeFIN == "CD")
                        {
                            var natureAccount = (from a in _contextOracleStage.LK_BUSINESS_UNIT
                                                 where a.BIZ_UNIT_ID == getDataProject.BIZ_UNIT_ID
                                                 select a.NATURE_ACCOUNT_CD).FirstOrDefault();
                            getSegment4Segment7 = GetDataSegment4(new GetDataSegment4InputDto { coaCodeFin = coaCodeFin.COACodeFIN, flag = 2, natureAccount = natureAccount });
                            getSegment6 = natureAccount;
                        }
                        else if (listCoaCodeFin2.Contains(coaCodeFin.COACodeFIN))
                        {
                            getSegment4Segment7 = GetDataSegment4(new GetDataSegment4InputDto { coaCodeFin = coaCodeFin.COACodeFIN, flag = 3, natureAccount = getDataAccount.NATURE_ACCOUNT_DEP });
                            getSegment6 = getDataAccount.NATURE_ACCOUNT_DEP;
                        }
                        else
                        {
                            var natureAccount = (from a in _contextAccounting.MS_COAFIN
                                                 where a.COACodeFIN == coaCodeFin.COACodeFIN
                                                 select a.COA_CODE).FirstOrDefault();
                            getSegment4Segment7 = GetDataSegment4(new GetDataSegment4InputDto { coaCodeFin = coaCodeFin.COACodeFIN, flag = 4, natureAccount = natureAccount });
                            getSegment6 = (from a in _contextOracleStage.LK_COA
                                           where a.REF_CODE == coaCodeFin.COACodeFIN
                                           select a.NATURE_ACCOUNT).FirstOrDefault();
                        }

                        // insert lk_gl_interface
                        var dataInsertInterface = new LK_GL_INTERFACE
                        {
                            STATUS = "NEW",
                            SET_OF_BOOKS_ID = 1004,
                            ACCOUNTING_DATE = input.transactionCutOffDate.ToString("dd/MM/yyyy"),
                            CURRENCY_CODE = "IDR",
                            DATE_CREATED = DateTime.Now.ToString("dd/MM/yyyy"),
                            CREATED_BY = -1,
                            ACTUAL_FLAG = "A",
                            USER_JE_CATEGORY_NAME = getDataAccount == null ? null : getDataAccount.ORG_ID + "-GJV",
                            USER_JE_SOURCE_NAME = "SAD",
                            SEGMENT1 = getDataAccount == null ? null : getDataAccount.ORG_ID,
                            SEGMENT2 = getDataProject == null ? null : listAccountCodeHL.Contains(getDataAccount.accCode) ? "25" : getDataProject.BIZ_UNIT_ID,
                            SEGMENT3 = getDataAccount == null ? null : getDataAccount.PROVINCE_ID,
                            SEGMENT4 = getSegment4Segment7 == null ? null : getSegment4Segment7.division_ID,
                            SEGMENT5 = getDataProject == null ? null : getDataProject.PROJECT_ID,
                            SEGMENT6 = getSegment6 == null ? null : listAccountCodeMV.Contains(getDataAccount.accCode) ? "000" : listAccountCodeHL.Contains(getDataAccount.accCode) ? "026" : getSegment6,
                            SEGMENT7 = getSegment4Segment7 == null ? null : getSegment4Segment7.interCompany,
                            ENTERED_DR = coaCodeFin.debit,
                            ENTERED_CR = coaCodeFin.kredit,
                            TRANSACTION_DATE = DateTime.Now.ToString("dd/MM/yyyy"),
                            GROUP_ID = getGroupID == 0 ? null : Convert.ToString(getGroupID),
                            ACC_CODE = getDataAccount == null ? null : getDataAccount.accCode,
                            INPUT_UN = GetIdUsername(AbpSession.UserId),
                            INPUT_TIME = DateTime.Now
                        };
                        try
                        {
                            Logger.DebugFormat("ClosingAccountUniversal() - Start insert LK GL Interface. Parameters sent:{0}" +
                                "STATUS                 = {1}{0}" +
                                "SET_OF_BOOKS_ID        = {2}{0}" +
                                "ACCOUNTING_DATE        = {3}{0}" +
                                "CURRENCY_CODE          = {4}{0}" +
                                "DATE_CREATED           = {5}{0}" +
                                "CREATED_BY             = {6}{0}" +
                                "ACTUAL_FLAG            = {7}{0}" +
                                "USER_JE_CATEGORY_NAME  = {8}{0}" +
                                "USER_JE_SOURCE_NAME    = {9}{0}" +
                                "SEGMENT1               = {10}{0}" +
                                "SEGMENT2               = {11}{0}" +
                                "SEGMENT3               = {12}{0}" +
                                "SEGMENT4               = {13}{0}" +
                                "SEGMENT5               = {14}{0}" +
                                "SEGMENT6               = {15}{0}" +
                                "SEGMENT7               = {16}{0}" +
                                "ENTERED_DR             = {17}{0}" +
                                "ENTERED_CR             = {18}{0}" +
                                "TRANSACTION_DATE       = {19}{0}" +
                                "GROUP_ID               = {20}{0}" +
                                "ACC_CODE               = {21}{0}" +
                                "INPUT_USER             = {22}{0}" +
                                "INPUT_TIME             = {23}{0}"
                                , Environment.NewLine, dataInsertInterface.STATUS, dataInsertInterface.SET_OF_BOOKS_ID, dataInsertInterface.TRANSACTION_DATE, dataInsertInterface.CURRENCY_CODE, dataInsertInterface.DATE_CREATED, dataInsertInterface.CREATED_BY
                                , dataInsertInterface.ACTUAL_FLAG, dataInsertInterface.USER_JE_CATEGORY_NAME, dataInsertInterface.USER_JE_SOURCE_NAME, dataInsertInterface.SEGMENT1, dataInsertInterface.SEGMENT2, dataInsertInterface.SEGMENT3, dataInsertInterface.SEGMENT4, dataInsertInterface.SEGMENT5, dataInsertInterface.SEGMENT6, dataInsertInterface.SEGMENT7, dataInsertInterface.ENTERED_DR, dataInsertInterface.ENTERED_CR, dataInsertInterface.TRANSACTION_DATE, dataInsertInterface.GROUP_ID, dataInsertInterface.INPUT_UN, dataInsertInterface.INPUT_TIME);

                            _contextOracleStage.LK_GL_INTERFACE.Add(dataInsertInterface);

                            Logger.DebugFormat("ClosingAccountUniversal() - Ended insert LK GL Interface.");
                        }
                        catch (DataException ex)
                        {
                            Logger.ErrorFormat("ClosingAccountUniversal() - ERROR DataException. Result = {0}", ex.Message);
                            throw new UserFriendlyException("Db Error: " + ex.Message);
                        }
                        catch (Exception ex)
                        {
                            Logger.ErrorFormat("ClosingAccountUniversal() - ERROR Exception. Result = {0}", ex.Message);
                            throw new UserFriendlyException("Error: " + ex.Message);
                        }
                    }
                    try
                    {
                        Logger.DebugFormat("ClosingAccountUniversal() - Start insert SYS Closing Daily. Parameters sent:{0}" +
                            "accountID                 = {1}{0}" +
                            "transactionCutOffDate     = {2}{0}" +
                            "userName                  = {3}{0}"
                            , Environment.NewLine, data.accountID, input.transactionCutOffDate, GetIdUsername(AbpSession.UserId));


                        CreateSysClosingDaily(data.accountID, input.transactionCutOffDate, input.isCheckAll == true ? "SYSTEM" : GetIdUsername(AbpSession.UserId));

                        Logger.DebugFormat("ClosingAccountUniversal() - Ended insert SYS Closing Daily.");
                    }
                    catch (DataException ex)
                    {
                        Logger.ErrorFormat("ClosingAccountUniversal() - ERROR DataException. Result = {0}", ex.Message);
                        throw new UserFriendlyException("Db Error: " + ex.Message);
                    }
                    catch (Exception ex)
                    {
                        Logger.ErrorFormat("ClosingAccountUniversal() - ERROR Exception. Result = {0}", ex.Message);
                        throw new UserFriendlyException("Error: " + ex.Message);
                    }
                }
                else
                {
                    throw new UserFriendlyException("Unable to close period. Account Code " + data.accountCode + " for " + input.transactionCutOffDate.ToString("dd/mm/yyyy") + " is unavailable.");
                }

            }
            _contextOracleStage.SaveChanges();
            _contextPropertySystem.SaveChanges();
            
            Logger.Info("ClosingAccountUniversal() - Finished.");
        }

        private void CreateSysClosingDaily(int accountID, DateTime transactionCutOffDate, string userName)
        {
            var getAccountEntity = (from x in _contextPropertySystem.MS_Account
                                    join y in _contextPropertySystem.MS_Entity on x.entityID equals y.Id
                                    where x.Id == accountID
                                    select y).FirstOrDefault();

            if (getAccountEntity != null)
            {
                var data = new SYS_ClosingDaily
                {
                    entityID = getAccountEntity.Id,
                    accountID = accountID,
                    closeDate = transactionCutOffDate,
                    closeBy = userName
                };
                _contextPropertySystem.SYS_ClosingDaily.Add(data);
            }
            else
            {
                throw new UserFriendlyException("AccountID " + accountID + " not found!");
            }
        }
        private GetDataSegment4ListDto GetDataSegment4(GetDataSegment4InputDto input)
        {
            var getSegment4 = new GetDataSegment4ListDto();

            var listCoaCodeFin1 = new List<string>
                        {
                            "BNK", "SBS", "LHS"
                        };
            var listCoaCodeFin2 = new List<string>
                        {
                            "DEP", "DPR", "DPS", "ESW"
                        };
            var listCoaCodeFin3 = new List<string>
                        {
                            "BNK", "SBS", "DEP", "DPR", "DPS", "ESW"
                        };

            if (input.flag == 1)
            {
                getSegment4 = (from a in _contextOracleStage.LK_COA
                               where a.REF_CODE == input.coaCodeFin && a.NATURE_ACCOUNT == input.natureAccount && listCoaCodeFin1.Contains(input.coaCodeFin)
                               select new GetDataSegment4ListDto
                               {
                                   coaCodeFin = input.coaCodeFin,
                                   division_ID = a.DIVISION_ID,
                                   interCompany = a.INTER_COMPANY
                               }).FirstOrDefault();
            }
            else if (input.flag == 2)
            {
                getSegment4 = (from a in _contextOracleStage.LK_COA
                               where a.REF_CODE == input.coaCodeFin && a.NATURE_ACCOUNT == input.natureAccount && input.coaCodeFin == "CD"
                               select new GetDataSegment4ListDto
                               {
                                   coaCodeFin = input.coaCodeFin,
                                   division_ID = a.DIVISION_ID,
                                   interCompany = a.INTER_COMPANY
                               }).FirstOrDefault();
            }
            else if (input.flag == 3)
            {
                getSegment4 = (from a in _contextOracleStage.LK_COA
                               where a.REF_CODE == input.coaCodeFin && a.NATURE_ACCOUNT == input.natureAccount && listCoaCodeFin2.Contains(input.coaCodeFin)
                               select new GetDataSegment4ListDto
                               {
                                   coaCodeFin = input.coaCodeFin,
                                   division_ID = a.DIVISION_ID,
                                   interCompany = a.INTER_COMPANY
                               }).FirstOrDefault();
            }
            else
            {
                getSegment4 = (from a in _contextOracleStage.LK_COA
                               where a.REF_CODE == input.coaCodeFin && a.NATURE_ACCOUNT == input.natureAccount && listCoaCodeFin3.Contains(input.coaCodeFin)
                               select new GetDataSegment4ListDto
                               {
                                   coaCodeFin = input.coaCodeFin,
                                   division_ID = a.DIVISION_ID,
                                   interCompany = a.INTER_COMPANY
                               }).FirstOrDefault();
            }
            return getSegment4;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Payment_ClosingAccount)]
        public ListResultDto<GetAllMsAccountClosingListDto> GetAllMsAccountClosing(DateTime cutOffDate)
        {
            var result = (from acc in _contextPropertySystem.MS_Account
                          join ca in _contextPropertySystem.SYS_ClosingAccount on new { a = acc.Id, b = (int)AbpSession.UserId } equals new { a = ca.accountID, b = ca.userID } into caa
                          from ca in caa.DefaultIfEmpty()
                          join cd in _contextPropertySystem.SYS_ClosingDaily on new { a = acc.Id, b = cutOffDate.Date } equals new { a = cd.accountID, b = cd.closeDate.Date } into cdd
                          from cd in cdd.DefaultIfEmpty()
                          join us in _contextEngine3.Users.ToList() on (ca == null ? 0 : ca.userID) equals us.Id into uss
                          from us in uss.DefaultIfEmpty()
                          select new GetAllMsAccountClosingListDto
                          {
                              accountID = acc.Id,
                              accountCode = acc.accCode,
                              accountName = acc.accName,
                              companyCode = acc.devCode,
                              closeDate = cd == null ? default(DateTime) : cd.closeDate,
                              userName = ca == null ? null : us.Name,
                              closeBy = cd == null ? null : cd.closeBy,
                              isSystemClosing = ca == null ? (bool?)null : ca.isSystemClosing
                          }).ToList();

            return new ListResultDto<GetAllMsAccountClosingListDto>(result);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Payment_ClosingAccount)]
        public void SendEmailClosingAccount(List<GetDataClosingEmailListDto> input)
        {
            Logger.Info("SendEmailClosingAccount() - Started.");

            var filePath = _hostingEnvironment.WebRootPath + @"\Assets\HtmlTemplates\CA\ClosingAccountEmail.html";
            string html = "";
            var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
            var webConfigApp = (JObject)appsettingsjson["App"];
            var emailDev = webConfigApp.Property("emailDev").Value.ToString();

            using (StreamReader reader = new StreamReader(filePath))
            {
                html = reader.ReadToEnd();
            }

            var getSourceName = "SAD";

            foreach(var data in input)
            {
                var getDataTabel = (from a in _contextOracleStage.LK_GL_INTERFACE
                                    where a.ACCOUNTING_DATE == data.accountingDate && a.ACC_CODE == data.accCode && a.USER_JE_SOURCE_NAME == getSourceName
                                    select new GetDataClosingEmailListDto
                                    {
                                        accountingDate = a.ACCOUNTING_DATE,
                                        groupID = a.GROUP_ID,
                                        accCode = a.ACC_CODE
                                    }).Distinct().ToList();
                var dataTemplate = new
                {
                    dataAccount = FormatingDataClosing(getDataTabel)
                };
                string htmlContent = Setting_variabel.GetHTMLMappedValue(html, dataTemplate);

                try
                {
                    MailMessage mail = new MailMessage();
                    mail.From = new MailAddress("denykalpar@gmail.com");
                    mail.To.Add(emailDev);
                    mail.CC.Add(emailDev);
                    mail.Subject = "Accounting Group ID Info - " + data.accCode + " " + data.accountingDate;
                    mail.Body = htmlContent;
                    mail.IsBodyHtml = true;

                    Logger.DebugFormat("SendEmailClosingAccount() - email data. {0}" +
                        "to             = {1}{0}" +
                        "subject        = {2}{0}" +
                        "body           = {3}{0}",
                        Environment.NewLine, mail.To.ToString(), mail.Subject, mail.Body);

                    _emailSender.Send(mail);

                    Logger.Info("SendEmailClosingAccount() - Success.");
                }
                catch (Exception ex)
                {
                    Logger.Info("SendEmailClosingAccount() - Error. " + ex);
                    throw new UserFriendlyException("Send Email Error: " + ex.Message);
                }
            }
            
            Logger.Info("SendEmailClosingAccount() - Finished.");
        }
        private string FormatingDataClosing(List<GetDataClosingEmailListDto> data)
        {
            string dataClosingHtml = "";
            foreach (var dataClosing in data)
            {
                dataClosingHtml += "<tr>" +
                                    "<td>" + dataClosing.accountingDate + "</td>" +
                                    "<td>" + dataClosing.groupID + "</td>" +
                                    "<td>" + dataClosing.accCode + "</td>" +
                                  "</tr>";
            }
            return dataClosingHtml;
        }
    }
}
