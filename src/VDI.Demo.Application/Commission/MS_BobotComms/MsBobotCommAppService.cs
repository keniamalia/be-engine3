﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.UI;
using System.Linq;
using VDI.Demo.Authorization;
using VDI.Demo.Commission.MS_BobotComms.Dto;
using VDI.Demo.NewCommDB;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;
using Abp.AutoMapper;
using Abp.Domain.Uow;
using Visionet_Backend_NetCore.Komunikasi;
using System.Threading.Tasks;
using Newtonsoft.Json;
using VDI.Demo.EntityFrameworkCore;
using Newtonsoft.Json.Linq;

namespace VDI.Demo.Commission.MS_BobotComms
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_MasterBobotCommission)]
    public class MsBobotCommAppService : DemoAppServiceBase, IMsBobotCommAppService
    {
        private readonly IRepository<MS_BobotComm, string> _msBobotCommRepo;
        private readonly IRepository<MS_Project> _msProjectRepo;
        private readonly IRepository<MS_Cluster> _msClusterRepo;
        private readonly IRepository<MS_Unit> _msUnitRepo;
        private readonly IRepository<MS_UnitCode> _msUnitCodeRepo;
        private readonly NewCommDbContext _context;
        private readonly PropertySystemDbContext _contextProp;
        private readonly DemoDbContext _contextDemo;

        public MsBobotCommAppService(
            IRepository<MS_BobotComm, string> msBobotCommRepo,
            IRepository<MS_Project> msProjectRepo,
            IRepository<MS_Cluster> msClusterRepo,
            IRepository<MS_Unit> msUnitRepo,
            IRepository<MS_UnitCode> msUnitCodeRepo,
            NewCommDbContext context,
            PropertySystemDbContext contextProp,
            DemoDbContext contextDemo
        )
        {
            _msBobotCommRepo = msBobotCommRepo;
            _msProjectRepo = msProjectRepo;
            _msClusterRepo = msClusterRepo;
            _msUnitRepo = msUnitRepo;
            _msUnitCodeRepo = msUnitCodeRepo;
            _context = context;
            _contextProp = contextProp;
            _contextDemo = contextDemo;
        }

        private string GetIdUsername(long? Id)
        {
            string getUsername = (from u in _contextDemo.Users
                                  where u.Id == Id
                                  select u.UserName)
                       .DefaultIfEmpty("").First();

            return getUsername;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_MasterBobotCommission_Create)]
        public JObject CreateMsBobotComm(List<MsBobotCommListDto> input)
        {
            JObject obj = new JObject();
            Logger.InfoFormat("CreateMsBobotComm() Started.");
            Logger.DebugFormat("CreateMsBobotComm() - Foreach input Started.");
            foreach (var item in input)
            {
                Logger.DebugFormat("CreateMsBobotComm() - Start checking existing projectID, clusterID and schemaID. Parameters sent: {0} " +
                    "   projectCode = {1}{0}" +
                    "   clusterCode = {2}{0}" +
                    "   scmCode = {3}"
                    , Environment.NewLine, item.projectCode, item.clusterCode, item.scmCode);

                var cekExistingData = (from bobotComm in _context.MS_BobotComm
                                       where bobotComm.projectCode == item.projectCode && bobotComm.clusterCode == item.clusterCode
                                       && bobotComm.scmCode == item.scmCode && bobotComm.entityCode == item.entityCode
                                       && bobotComm.isComplete == true && bobotComm.isActive == true
                                       select bobotComm).Any();

                Logger.DebugFormat("CreateMsBobotComm() - End checking existing projectID, clusterID and schemaID. Result = {0}", cekExistingData);

                if (!cekExistingData)
                {
                    var createBobotComm = new MS_BobotComm
                    {
                        clusterCode = item.clusterCode,
                        entityCode = item.entityCode,
                        projectCode = item.projectCode,
                        scmCode = item.scmCode,
                        pctBobot = item.pctBobot,
                        isActive = item.isActive,
                        isComplete = true,
                        inputTime = DateTime.Now,
                        modifTime = DateTime.Now,
                        inputUN = GetIdUsername(AbpSession.UserId),
                        modifUN = GetIdUsername(AbpSession.UserId)
                    };

                    try
                    {
                        Logger.DebugFormat("CreateMsBobotComm() - Start insert bobotComm. Parameters sent: {0} " +
                    "   clusterCode = {1}{0}" +
                    "   entityCode = {2}{0}" +
                    "   projectCode = {3}{0}" +
                    "   scmCode = {4}{0}" +
                    "   pctBobot = {5}{0}" +
                    "   isActive = {6}{0}" +
                    "   isComplete = {7}"
                    , Environment.NewLine, item.clusterCode, item.entityCode, item.projectCode, item.scmCode, item.pctBobot, item.isActive, true);

                        _context.MS_BobotComm.Add(createBobotComm);
                        _context.SaveChanges();

                        Logger.DebugFormat("CreateMsBobotComm() - End insert bobotComm.");

                        obj.Add("message", "Created Successfully");
                    }
                    catch (Exception ex)
                    {
                        SendConsole("" + ex.Message + " " + ex.StackTrace);
                        Logger.DebugFormat("CreateMsBobotComm() - ERROR Exception. Result = {0}", ex.Message);
                        throw new UserFriendlyException("Error: " + ex.Message);
                    }
                }
                else
                {
                    Logger.DebugFormat("CreateMsBobotComm() - ERROR. Result = {0}", "The Cluster Already Exist For the current project and schema!");
                    throw new UserFriendlyException("The cluster already exist for the current project, schema, and entity!");
                }
            }

            Logger.InfoFormat("CreateMsBobotComm() - Foreach input Ended.");
            Logger.InfoFormat("CreateMsBobotComm() - Finished.");

            return obj;
        }

        public JObject DeleteMsBobotComm(MsBobotCommListDto input)
        {
            if (string.IsNullOrEmpty(input.clusterCode))
                throw new UserFriendlyException("Cluster code is required!");
            if (string.IsNullOrEmpty(input.projectCode))
                throw new UserFriendlyException("Project code is required!");
            if (string.IsNullOrEmpty(input.scmCode))
                throw new UserFriendlyException("Schema code is required!");
            if (string.IsNullOrEmpty(input.entityCode))
                throw new UserFriendlyException("Entity code is required!");

            JObject obj = new JObject();
            Logger.InfoFormat("DeleteMsBobotComm() Started.");

            Logger.DebugFormat("DeleteMsBobotComm() - Start get data bobotComm. Parameters sent: {0} " +
                    "   projectCode = {1}{0}" +
                    "   clusterCode = {2}{0}" +
                    "   scmCode = {3}{0}" +
                    "   entityCode = {4}{0}"
                    , Environment.NewLine, input.projectCode, input.clusterCode, input.scmCode, input.entityCode);

            var getBobotComm = (from bobotComm in _context.MS_BobotComm
                                where bobotComm.projectCode == input.projectCode && bobotComm.clusterCode == input.clusterCode
                                && bobotComm.scmCode == input.scmCode && bobotComm.entityCode == input.entityCode
                                select bobotComm).FirstOrDefault();

            if (getBobotComm == null)
                throw new UserFriendlyException("The cluster is not exist!");

            Logger.DebugFormat("DeleteMsBobotComm() - End Start get data bobotComm. Result = {0}", getBobotComm);

            try
            {
                Logger.DebugFormat("DeleteMsBobotComm() - Start get data bobotComm. Parameters sent: {0} " +
                   "   projectCode = {1}{0}" +
                   "   clusterCode = {2}{0}" +
                   "   scmCode = {3}{0}" +
                   "   entityCode = {4}{0}" +
                   "   isComplete = {5}{0}"
                   , Environment.NewLine, input.projectCode, input.clusterCode, input.scmCode, input.entityCode, false);

                _context.MS_BobotComm.Remove(getBobotComm);
                _context.SaveChanges();

                Logger.DebugFormat("DeleteMsBobotComm() - End Update bobotComm.");

                obj.Add("message", "Deleted successfully");
            }
            catch (Exception ex)
            {
                SendConsole("" + ex.Message + " " + ex.StackTrace);
                Logger.DebugFormat("DeleteMsBobotComm() - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Error: " + ex.Message);
            }
            Logger.InfoFormat("DeleteMsBobotComm() - Finished.");
            return obj;
        }

        [UnitOfWork(isTransactional: false)]
        public ListResultDto<MsBobotCommListDto> GetMsBobotCommByProject(string projectCode)
        {
            if (string.IsNullOrEmpty(projectCode))
                throw new UserFriendlyException("Project code is required!");

            List<MsBobotCommListDto> listResult = new List<MsBobotCommListDto>();
            try
            {
                #region test only without msdtc - change it if you want       

                #region non compact version
                /*
                var dataBobot = (from bobotComm in _context.MS_BobotComm
                                 join scm in _context.MS_Schema on bobotComm.schemaID equals scm.Id
                                 where bobotComm.projectID == projectID && bobotComm.isComplete == true
                                 select new MsBobotCommListDto
                                 {
                                     Id = bobotComm.Id,
                                     entityID = bobotComm.entityID,
                                     projectID = bobotComm.projectID,
                                     schemaID = bobotComm.schemaID,
                                     clusterID = bobotComm.clusterID,
                                     pctBobot = bobotComm.pctBobot,
                                     isActive = bobotComm.isActive,
                                     isComplete = bobotComm.isComplete,
                                     scmCode = (scm.scmCode ?? "")
                                 }).ToList();
                
                
                listResult = (from resultBobot in dataBobot.ToList()                             
                              join mp in _contextProp.MS_Project on resultBobot.projectID equals mp.Id
                              join mc in _contextProp.MS_Cluster on resultBobot.clusterID equals mc.Id into res
                              from resultBobotMc in res.DefaultIfEmpty()
                              select new MsBobotCommListDto
                              {
                                  Id = resultBobot.Id,
                                  scmCode = (resultBobot==null || resultBobot.scmCode == null ? null : resultBobot.scmCode),
                                  projectCode = (mp==null || mp.projectCode == null ? null : mp.projectCode),
                                  clusterCode = (resultBobotMc==null || resultBobotMc.clusterCode == null ? null : resultBobotMc.clusterCode),
                                  entityID = resultBobot.entityID,
                                  projectID = resultBobot.projectID,
                                  schemaID = resultBobot.schemaID,
                                  clusterID = resultBobot.clusterID,
                                  pctBobot = resultBobot.pctBobot,
                                  isActive = resultBobot.isActive,
                                  isComplete = resultBobot.isComplete,
                                  clusterName = (resultBobotMc==null || resultBobotMc.clusterName== null ? null : resultBobotMc.clusterName),
                                  projectName = (mp==null || mp.projectName == null ? null : mp.projectName)
                              }
                              ).OrderByDescending(a=>a.Id).ToList();
                              */
                #endregion

                listResult = (from resultBobot in _context.MS_BobotComm.ToList()
                              join scm in _context.MS_Schema.ToList() on resultBobot.scmCode equals scm.scmCode
                              join mp in _contextProp.MS_Project on resultBobot.projectCode equals mp.projectCode
                              join mc in _contextProp.MS_Cluster on resultBobot.clusterCode equals mc.clusterCode into res
                              from resultBobotMc in res.DefaultIfEmpty()
                              where resultBobot.projectCode == projectCode && resultBobot.isComplete == true
                              select new MsBobotCommListDto
                              {
                                  scmCode = (scm == null || scm.scmCode == null ? null : scm.scmCode),
                                  projectCode = (mp == null || mp.projectCode == null ? null : mp.projectCode),
                                  clusterCode = (resultBobotMc == null || resultBobotMc.clusterCode == null ? null : resultBobotMc.clusterCode),
                                  entityCode = resultBobot.entityCode,
                                  pctBobot = resultBobot.pctBobot,
                                  isActive = resultBobot.isActive,
                                  isComplete = resultBobot.isComplete,
                                  clusterName = (resultBobotMc == null || resultBobotMc.clusterName == null ? null : resultBobotMc.clusterName),
                                  projectName = (mp == null || mp.projectName == null ? null : mp.projectName),
                                  inputTime = resultBobot.inputTime
                              }
                              ).OrderByDescending(a => a.inputTime).ToList();
                #endregion

            }
            catch (Exception e)
            {
                SendConsole("" + e.Message + " " + e.StackTrace);
            }
            return new ListResultDto<MsBobotCommListDto>(listResult);
        }

        public JObject UpdateMsBobotComm(MsBobotCommListDto input)
        {
            if (string.IsNullOrEmpty(input.clusterCode))
                throw new UserFriendlyException("Cluster code is required!");
            if (string.IsNullOrEmpty(input.projectCode))
                throw new UserFriendlyException("Project code is required!");
            if (string.IsNullOrEmpty(input.scmCode))
                throw new UserFriendlyException("Schema code is required!");
            if (string.IsNullOrEmpty(input.entityCode))
                throw new UserFriendlyException("Entity code is required!");

            JObject obj = new JObject();
            Logger.InfoFormat("UpdateMsBobotComm() Started.");

            Logger.DebugFormat("UpdateMsBobotComm() - Start get data for update. Parameters sent: {0} " +
                "   projectCode = {1}{0}" +
                "   clusterCode = {2}{0}" +
                "   scmCode = {3}{0}" +
                "   entityCode = {4}{0}"
                , Environment.NewLine, input.projectCode, input.clusterCode, input.scmCode, input.entityCode);

            var getMsBobotComm = (from bobotComm in _context.MS_BobotComm
                                  where bobotComm.projectCode == input.projectCode && bobotComm.clusterCode == input.clusterCode && bobotComm.scmCode == input.scmCode && bobotComm.entityCode == input.entityCode
                                  select bobotComm).FirstOrDefault();

            if (getMsBobotComm == null)
                throw new UserFriendlyException("The cluster is not exist!");

            var updateBobotComm = getMsBobotComm.MapTo<MS_BobotComm>();
            updateBobotComm.pctBobot = input.pctBobot;
            updateBobotComm.isActive = input.isActive;
            Logger.DebugFormat("UpdateMsBobotComm() - End get data for update. Result = {0}", updateBobotComm);

            try
            {
                Logger.DebugFormat("UpdateMsBobotComm() - Start update bobotComm. Parameters sent: {0} " +
                    "   clusterCode = {1}{0}" +
                    "   pctBobot = {2}{0}" +
                    "   isActive = {3}{0}"
                    , Environment.NewLine, input.clusterCode, input.pctBobot, input.isActive);

                _msBobotCommRepo.Update(updateBobotComm);
                CurrentUnitOfWork.SaveChanges();
                Logger.DebugFormat("UpdateMsBobotComm() - End update bobotComm.");

                obj.Add("message", "Updated successfully");
            }
            catch (Exception ex)
            {
                SendConsole("" + ex.Message + " " + ex.StackTrace);
                Logger.DebugFormat("UpdateMsBobotComm() - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Error: " + ex.Message);
            }
            Logger.InfoFormat("UpdateMsBobotComm() - Finished.");

            return obj;
        }

        #region debug console
        private void SendConsole(string msg)
        {
            if (Setting_variabel.enable_tcp_debug == true)
            {
                if (Setting_variabel.Komunikasi_TCPListener == null)
                {
                    Setting_variabel.Komunikasi_TCPListener = new Visionet_Backend_NetCore.Komunikasi.Komunikasi_TCPListener(17000);
                    Task.Run(() => StartListenerLokal());
                }

                if (Setting_variabel.Komunikasi_TCPListener != null)
                {
                    if (Setting_variabel.Komunikasi_TCPListener.IsRunning())
                    {
                        if (Setting_variabel.ConsoleBayangan != null)
                        {
                            Setting_variabel.ConsoleBayangan.Send("[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "] " + msg);
                        }
                    }
                }
            }

        }
        #endregion

        #region listener tcp debug
        protected void StartListenerLokal()
        {
            if (Setting_variabel.Komunikasi_TCPListener != null)
                Setting_variabel.Komunikasi_TCPListener.StartListener();
        }

        protected void StopListenerLokal()
        {
            if (Setting_variabel.Komunikasi_TCPListener != null)
                Setting_variabel.Komunikasi_TCPListener.StopListener();
        }
        #endregion

    }
}
