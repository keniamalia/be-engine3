﻿using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.UI;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using System.Text;
using VDI.Demo.Authorization;
using VDI.Demo.Commission.MS_PointPercentage.Dto;
using VDI.Demo.NewCommDB;
using Abp.AutoMapper;
using Abp.Application.Services.Dto;
using VDI.Demo.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using Visionet_Backend_NetCore.Komunikasi;
using System.Threading.Tasks;

namespace VDI.Demo.Commission.MS_PointPercentage
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_MasterPointPercent)]
    public class MsPointPctAppService : DemoAppServiceBase, IMsPointPctAppService
    {
        private readonly IRepository<MS_PointPct, string> _msPointPctRepo;
        private readonly IRepository<MS_Schema, string> _msSchemaRepo;
        private readonly IRepository<MS_StatusMember, string> _msStatusMemberRepo;
        private readonly IRepository<LK_PointType, string> _lkPointTypeRepo;
        private readonly NewCommDbContext _contextCom;
        private readonly DemoDbContext _contextDemo;

        public MsPointPctAppService(
            IRepository<MS_PointPct, string> msPointPctRepo,
            IRepository<MS_Schema, string> msSchemaRepo,
            IRepository<MS_StatusMember, string> msStatusMemberRepo,
            IRepository<LK_PointType, string> lkPointTypeRepo,
            NewCommDbContext contextCom,
            DemoDbContext contextDemo
        )
        {
            _msPointPctRepo = msPointPctRepo;
            _msSchemaRepo = msSchemaRepo;
            _msStatusMemberRepo = msStatusMemberRepo;
            _lkPointTypeRepo = lkPointTypeRepo;
            _contextCom = contextCom;
            _contextDemo = contextDemo;
        }

        private string GetIdUsername(long? Id)
        {
            string getUsername = (from u in _contextDemo.Users
                                  where u.Id == Id
                                  select u.UserName)
                       .DefaultIfEmpty("").First();

            return getUsername;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_MasterPointPercent_Create)]
        public JObject CreateMsPointPct(List<InputPointPctDto> input)
        {
            JObject obj = new JObject();
            Logger.Info("CreateMsPointPct() - Started.");

            foreach (var item in input)
            {
                var createPointPct = new MS_PointPct
                {
                    entityCode = item.entityCode,
                    scmCode = item.scmCode,
                    statusCode = item.statusCode,
                    asUplineNo = item.asUplineNo,
                    pointTypeCode = item.pointTypeCode,
                    pointPct = item.pointPct,
                    pointKonvert = item.pointKonvert,
                    inputTime = DateTime.Now,
                    modifTime = DateTime.Now,
                    inputUN = GetIdUsername(AbpSession.UserId),
                    modifUN = GetIdUsername(AbpSession.UserId)
                };

                try
                {
                    Logger.DebugFormat("CreateMsPointPct() - Start insert PointPct. Params sent:{0}" +
                    "scmCode       = {1}{0}" +
                    "statusCode = {2}{0}" +
                    "pointTypeCode    = {3}{0}" +
                    "pointPct       = {4}{0}" +
                    "pointKonvert   = {5}{0}" +
                    "asUplineNo     = {6}{0}"
                    , Environment.NewLine, item.scmCode, item.statusCode, item.pointTypeCode, item.pointPct, item.pointKonvert, item.asUplineNo);

                    _msPointPctRepo.Insert(createPointPct);
                    CurrentUnitOfWork.SaveChanges(); //execution saved inside try

                    Logger.DebugFormat("CreateMsPointPct() - End insert PointPct.");

                    obj.Add("message", "Created successfully");
                }
                catch (Exception ex)
                {
                    Logger.ErrorFormat("UpdateMsDepartment() ERROR Exception. Result = {0}", ex.Message);
                    throw new UserFriendlyException("Error: " + ex.Message);
                }
            }
            Logger.Info("CreateMsPointPct() - Finished.");
            return obj;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_MasterPointPercent_Delete)]
        public JObject DeleteMsPointPct(DeleteMsPointPctDto input)
        {
            if (string.IsNullOrEmpty(input.scmCode))
                throw new UserFriendlyException("Schema code is required!");
            if (string.IsNullOrEmpty(input.entityCode))
                throw new UserFriendlyException("Entity code is required!");

            JObject obj = new JObject();
            Logger.Info("DeleteMsPointPct() - Started.");

            var getPointPct = (from p in _contextCom.MS_PointPct
                               where p.entityCode == input.entityCode && p.scmCode == input.scmCode && p.asUplineNo == input.asUplineNo
                               && p.statusCode == input.statusCode
                               select p).FirstOrDefault();

            if (getPointPct == null)
                throw new UserFriendlyException("The point percentage is not exist!");

            try
            {
                Logger.DebugFormat("DeleteMsPointPct() - Start delete PointPct with scmCode: {0}", input.scmCode);

                _contextCom.MS_PointPct.Remove(getPointPct);
                _contextCom.SaveChanges();

                Logger.DebugFormat("DeleteMsPointPct() - End delete PointPct.");

                obj.Add("message", "Deleted successfully");
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("DeleteMsPointPct() ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Error: " + ex.Message);
            }
            Logger.Info("DeleteMsPointPct() - Finished.");
            return obj;
        }

        public ListResultDto<GetAllPointPctListDto> GetMsPointPctBySchemaCode(string scmCode)
        {
            if (string.IsNullOrEmpty(scmCode))
                throw new UserFriendlyException("Schema code is required!");

            var listResult = (from x in _contextCom.MS_PointPct
                              join schema in _contextCom.MS_Schema on x.scmCode equals schema.scmCode
                              join statusMember in _msStatusMemberRepo.GetAll() on x.statusCode equals statusMember.statusCode
                              join lkPointType in _lkPointTypeRepo.GetAll() on x.pointTypeCode equals lkPointType.pointTypeCode
                              where x.scmCode == scmCode
                              && schema.entityCode == x.entityCode
                              && statusMember.entityCode == x.entityCode && statusMember.scmCode == x.scmCode
                              && lkPointType.entityCode == x.entityCode && lkPointType.scmCode == x.scmCode
                              orderby x.Id descending
                              select new GetAllPointPctListDto
                              {
                                  entityCode = x.entityCode,
                                  scmCode = x.scmCode,
                                  statusCode = statusMember.statusCode,
                                  statusName = statusMember.statusName,
                                  pointTypeCode = lkPointType.pointTypeCode,
                                  pointTypeName = lkPointType.pointTypeName,
                                  uplineNo = x.asUplineNo,
                                  pointPct = x.pointPct,
                                  pointKonvert = x.pointKonvert
                              }).ToList();

            return new ListResultDto<GetAllPointPctListDto>(listResult);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_MasterPointPercent_Edit)]
        public JObject UpdateMsPointPct(InputPointPctDto input)
        {
            if (string.IsNullOrEmpty(input.scmCode))
                throw new UserFriendlyException("Schema code is required!");
            if (string.IsNullOrEmpty(input.entityCode))
                throw new UserFriendlyException("Entity code is required!");

            JObject obj = new JObject();
            Logger.Info("UpdateMsPointPct() - Started.");

            var getPointPct = (from p in _contextCom.MS_PointPct
                               where p.entityCode == input.entityCode && p.scmCode == input.scmCode && p.asUplineNo == input.asUplineNo
                               && p.statusCode == input.statusCode
                               select p).FirstOrDefault();

            if (getPointPct == null)
                throw new UserFriendlyException("The point percentage is not exist!");

            var updatePointPct = getPointPct.MapTo<MS_PointPct>();

            updatePointPct.pointTypeCode = input.pointTypeCode;
            updatePointPct.pointPct = input.pointPct;
            updatePointPct.pointKonvert = input.pointKonvert;

            try
            {
                Logger.DebugFormat("UpdateMsPointPct() - Start update PointPct. Params sent:{0}" +
                    "statusCode = {1}{0}" +
                    "pointTypeCode    = {2}{0}" +
                    "pointPct       = {3}{0}" +
                    "pointKonvert   = {4}{0}" +
                    "asUplineNo     = {5}{0}"
                    , Environment.NewLine, input.statusCode, input.pointTypeCode, input.pointPct, input.pointKonvert, input.asUplineNo);
                _msPointPctRepo.Update(updatePointPct);
                CurrentUnitOfWork.SaveChanges(); //execution saved inside try
                Logger.DebugFormat("UpdateMsPointPct() - End update PointPct.");

                obj.Add("message", "Updated successfully");
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("UpdateMsPointPct() ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Error: " + ex.Message);
            }
            Logger.Info("UpdateMsPointPct() - Finished.");

            return obj;
        }

        #region debug console
        private void SendConsole(string msg)
        {
            if (Setting_variabel.enable_tcp_debug == true)
            {
                if (Setting_variabel.Komunikasi_TCPListener == null)
                {
                    Setting_variabel.Komunikasi_TCPListener = new Visionet_Backend_NetCore.Komunikasi.Komunikasi_TCPListener(17000);
                    Task.Run(() => StartListenerLokal());
                }

                if (Setting_variabel.Komunikasi_TCPListener != null)
                {
                    if (Setting_variabel.Komunikasi_TCPListener.IsRunning())
                    {
                        if (Setting_variabel.ConsoleBayangan != null)
                        {
                            Setting_variabel.ConsoleBayangan.Send("[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "] " + msg);
                        }
                    }
                }
            }

        }
        #endregion

        #region listener tcp debug
        protected void StartListenerLokal()
        {
            if (Setting_variabel.Komunikasi_TCPListener != null)
                Setting_variabel.Komunikasi_TCPListener.StartListener();
        }

        protected void StopListenerLokal()
        {
            if (Setting_variabel.Komunikasi_TCPListener != null)
                Setting_variabel.Komunikasi_TCPListener.StopListener();
        }
        #endregion
    }
}
