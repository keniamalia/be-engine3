﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.UI;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using System.Text;
using VDI.Demo.Authorization;
using VDI.Demo.Commission.MS_PPhRanges.Dto;
using VDI.Demo.NewCommDB;
using Abp.AutoMapper;
using VDI.Demo.EntityFrameworkCore;
using Newtonsoft.Json.Linq;

namespace VDI.Demo.Commission.MS_PPhRanges
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_MasterPPHRange)]
    public class MsPPhRangeAppService : DemoAppServiceBase, IMsPPhRangeAppService
    {
        private readonly IRepository<MS_PPhRange, string> _msPPhRangesRepo;
        private readonly DemoDbContext _contextDemo;
        private readonly NewCommDbContext _contextCom;

        public MsPPhRangeAppService(
            IRepository<MS_PPhRange, string> msPPhRangesRepo,
            DemoDbContext contextDemo,
            NewCommDbContext contextCom
        )
        {
            _msPPhRangesRepo = msPPhRangesRepo;
            _contextDemo = contextDemo;
            _contextCom = contextCom;
        }

        private string GetIdUsername(long? Id)
        {
            string getUsername = (from u in _contextDemo.Users
                                  where u.Id == Id
                                  select u.UserName)
                       .DefaultIfEmpty("").First();

            return getUsername;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_MasterPPHRange_Create)]
        public JObject CreateMsPPhRange(List<CreateOrUpdatePPhRangeListDto> input)
        {
            JObject obj = new JObject();
            Logger.Info("CreateMsPPhRange() - Started.");
            foreach (var item in input)
            {
                var createPPhRange = new MS_PPhRange
                {
                    entityCode = item.entityCode,
                    scmCode = item.scmCode,
                    PPhYear = item.pphYear,
                    PPhRangeHighBound = item.pphRangeHighBound,
                    PPhRangePct = item.pphRangePct,
                    TAX_CODE = item.tax_code,
                    TAX_CODE_NON_NPWP = item.tax_code_non_npwp,
                    pphRangePct_NON_NPWP = item.pphRangePct_non_npwp,
                    inputTime = DateTime.Now,
                    modifTime = DateTime.Now,
                    inputUN = GetIdUsername(AbpSession.UserId),
                    modifUN = GetIdUsername(AbpSession.UserId)
                };

                try
                {
                    Logger.DebugFormat("CreateMsPPhRange() - Start insert PPh Range. Parameters sent:{0}" +
                        "schemaID = {1}{0}" +
                        "pphYear = {2}{0}" +
                        "pphRangeHighBound = {3}{0}" +
                        "pphRangePct = {4}{0}" +
                        "TAX_CODE = {5}{0}" +
                        "TAX_CODE_NON_NPWP = {6}{0}" +
                        "pphRangePct_NON_NPWP = {7}{0}" +
                        "entityCode = {8}{0}"
                        , Environment.NewLine, item.scmCode, item.pphYear, item.pphRangeHighBound, item.pphRangePct
                        , item.tax_code, item.tax_code_non_npwp, item.pphRangePct_non_npwp, item.entityCode);

                    _msPPhRangesRepo.Insert(createPPhRange);
                    CurrentUnitOfWork.SaveChanges(); //execution saved inside try
                    Logger.DebugFormat("CreateMsPPhRange() - Ended insert PPh Range.");

                    obj.Add("message", "Created successfully");
                }
                catch (Exception ex)
                {
                    Logger.ErrorFormat("CreateMsPPhRange() - ERROR Exception. Result = {0}", ex.Message);
                    throw new UserFriendlyException("Error: " + ex.Message);
                }

            }
            Logger.Info("CreateMsPPhRange() - Finished.");
            return obj;
        }

        public JObject UpdateMsPPhRange(CreateOrUpdatePPhRangeListDto input)
        {
            if (string.IsNullOrEmpty(input.scmCode))
                throw new UserFriendlyException("Schema code is required!");
            if (string.IsNullOrEmpty(input.entityCode))
                throw new UserFriendlyException("Entity code is required!");

            JObject obj = new JObject();
            Logger.Info("UpdateMsPPhRange() - Started.");

            Logger.DebugFormat("UpdateMsPPhRange() - Start get data before update PPh Range. Parameters sent:{0}" +
                        "pphYear = {1}{0}" +
                        "pphRangeHighBound = {2}{0}"
                        , Environment.NewLine, input.pphYear, input.pphRangeHighBound);

            var getPPhRange = (from p in _contextCom.MS_PPHRange
                               where p.entityCode == input.entityCode && p.scmCode == input.scmCode && p.PPhYear == input.pphYear && p.PPhRangeHighBound == input.pphRangeHighBound
                               select p).FirstOrDefault();

            if (getPPhRange == null)
                throw new UserFriendlyException("PPH range is not exist!");

            Logger.DebugFormat("UpdateMsPPhRange() - Ended get data before update PPh Range. Result = {0}", getPPhRange);

            var updatePPhRange = getPPhRange.MapTo<MS_PPhRange>();

            updatePPhRange.PPhRangePct = input.pphRangePct;
            updatePPhRange.TAX_CODE = input.tax_code;
            updatePPhRange.TAX_CODE_NON_NPWP = input.tax_code_non_npwp;
            updatePPhRange.pphRangePct_NON_NPWP = input.pphRangePct_non_npwp;

            try
            {
                Logger.DebugFormat("UpdateMsPPhRange() - Start update PPh Range. Parameters sent:{0}" +
                        "pphYear = {1}{0}" +
                        "pphRangeHighBound = {2}{0}" +
                        "pphRangePct = {3}{0}" +
                        "TAX_CODE = {4}{0}" +
                        "TAX_CODE_NON_NPWP = {5}{0}" +
                        "pphRangePct_NON_NPWP = {6}{0}"
                        , Environment.NewLine, input.pphYear, input.pphRangeHighBound, input.pphRangePct
                        , input.tax_code, input.tax_code_non_npwp, input.pphRangePct_non_npwp);

                _msPPhRangesRepo.Update(updatePPhRange);
                CurrentUnitOfWork.SaveChanges(); //execution saved inside try

                Logger.DebugFormat("UpdateMsPPhRange() - Ended update PPh Range.");

                obj.Add("message", "Updated successfully");
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("UpdateMsPPhRange() - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Error: " + ex.Message);
            }

            Logger.Info("UpdateMsPPhRange() - Finished.");
            return obj;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_MasterPPHRange_Delete)]
        public JObject DeleteMsPPhRange(DeleteMsPPhRangeDto input)
        {
            if (string.IsNullOrEmpty(input.scmCode))
                throw new UserFriendlyException("Schema code is required!");
            if (string.IsNullOrEmpty(input.entityCode))
                throw new UserFriendlyException("Entity code is required!");

            JObject obj = new JObject();
            Logger.Info("DeleteMsPPhRange() - Started.");

            Logger.DebugFormat("DeleteMsPPhRange() - Start get data before delete PPh Range. Parameters sent:{0}" +
                        "entityCode = {1}{0}" +
                        "scmCode = {2}{0}" +
                        "pphYear = {3}{0}" +
                        "pphRangeHighBound = {4}{0}"
                        , Environment.NewLine, input.entityCode, input.scmCode, input.pphYear, input.pphRangeHighBound);

            var getPphRange = (from p in _contextCom.MS_PPHRange
                               where p.entityCode == input.entityCode && p.scmCode == input.scmCode && p.PPhYear == input.pphYear && p.PPhRangeHighBound == input.pphRangeHighBound
                               select p).FirstOrDefault();

            Logger.DebugFormat("DeleteMsPPhRange() - Ended get data before delete PPh Range. Result = {0}", getPphRange);

            if (getPphRange == null)
                throw new UserFriendlyException("PPH range is not exist!");

            try
            {
                Logger.DebugFormat("DeleteMsPPhRange() - Start delete PPh Range.");

                _contextCom.MS_PPHRange.Remove(getPphRange);
                _contextCom.SaveChanges();

                Logger.DebugFormat("DeleteMsPPhRange() - Ended delete PPh Range.");

                obj.Add("message", "Deleted successfully");
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("DeleteMsPPhRange() - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Error: " + ex.Message);
            }
            Logger.Info("DeleteMsPPhRange() - Finished.");
            return obj;
        }

        public ListResultDto<CreateOrUpdatePPhRangeListDto> GetMsPPhRangeBySchemaCode(string scmCode)
        {
            if (string.IsNullOrEmpty(scmCode))
                throw new UserFriendlyException("Schema code is required!");

            var listResult = (from p in _contextCom.MS_PPHRange
                              where p.scmCode == scmCode
                              orderby p.Id descending
                              select new CreateOrUpdatePPhRangeListDto
                              {
                                   entityCode=p.entityCode,
                                   scmCode=p.scmCode,
                                   pphYear=p.PPhYear,
                                   pphRangeHighBound=p.PPhRangeHighBound,
                                   pphRangePct=p.PPhRangePct,
                                   tax_code=p.TAX_CODE,
                                   tax_code_non_npwp=p.TAX_CODE_NON_NPWP,
                                   pphRangePct_non_npwp=p.pphRangePct_NON_NPWP
                              }).ToList();

            return new ListResultDto<CreateOrUpdatePPhRangeListDto>(listResult);
        }

    }
}
