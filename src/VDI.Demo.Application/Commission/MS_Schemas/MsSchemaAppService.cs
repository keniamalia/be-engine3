﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using VDI.Demo.Authorization;
using VDI.Demo.Commission.MS_Schemas.Dto;
using VDI.Demo.NewCommDB;
using Microsoft.EntityFrameworkCore;
using Abp.Linq.Extensions;
using Abp.UI;
using System.Data.Common;
using Abp.AutoMapper;
using System.Data;
using Microsoft.AspNetCore.Http;
using VDI.Demo.Files;
using Abp.Extensions;
using Microsoft.AspNetCore.Hosting;
using System.Text.RegularExpressions;
using VDI.Demo.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using Visionet_Backend_NetCore.Komunikasi;

namespace VDI.Demo.Commission.MS_Schemas
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_MasterSchema)]
    public class MsSchemaAppService : DemoAppServiceBase, IMsSchemaAppService
    {
        private readonly IRepository<MS_Schema, string> _msSchemaRepo;
        private readonly IRepository<MS_SchemaRequirement, string> _msSchemaRequirementRepo;
        private readonly IRepository<LK_CommType, string> _lkCommTypeRepo;
        private readonly IRepository<MS_StatusMember, string> _msStatusMemberRepo;
        private readonly IRepository<LK_PointType, string> _lkPointTypeRepo;
        private readonly IRepository<LK_Upline, string> _lkUplineRepo;
        private readonly IRepository<MS_CommPct, string> _msCommPctRepo;
        private readonly IRepository<MS_Property, string> _msPropertyRepo;
        private readonly IRepository<MS_Developer_Schema, string> _msDeveloperSchemaRepo;
        private readonly IRepository<MS_GroupSchema, string> _msGroupSchemaRepo;
        private readonly IRepository<MS_GroupCommPct, string> _msGroupCommPctRepo;
        private readonly IRepository<MS_GroupCommPctNonStd, string> _msGroupCommPctNonStdRepo;
        private readonly IRepository<MS_PPhRange, string> _msPPhRangeRepo;
        private readonly IRepository<MS_PPhRangeIns, string> _msPPhRangeInsRepo;
        private readonly IRepository<MS_PointPct, string> _msPointPctRepo;
        private readonly IRepository<TR_CommPayment, string> _trCommPaymentRepo;
        private readonly IRepository<TR_CommPaymentPph, string> _trCommPaymentPphRepo;
        private readonly IRepository<TR_ManagementFee, string> _trManagementFeeRepo;
        private readonly IRepository<TR_SoldUnit, string> _trSoldUnitRepo;
        private readonly IRepository<TR_SoldUnitRequirement, string> _trSoldUnitReqRepo;
        private static IHttpContextAccessor _HttpContextAccessor;
        private readonly FilesHelper _filesHelper;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly NewCommDbContext _context;
        private readonly DemoDbContext _contextDemo;

        public MsSchemaAppService(
            IRepository<MS_Schema, string> msSchemaRepo,
            IRepository<MS_SchemaRequirement, string> msSchemaRequirementRepo,
            IRepository<LK_CommType, string> lkCommTypeRepo,
            IRepository<MS_StatusMember, string> msStatusMemberRepo,
            IRepository<LK_PointType, string> lkPointTypeRepo,
            IRepository<LK_Upline, string> lkUplineRepo,
            IRepository<MS_CommPct, string> msCommPctRepo,
            IRepository<MS_Property, string> msPropertyRepo,
            IRepository<MS_Developer_Schema, string> msDeveloperSchemaRepo,
            IRepository<MS_GroupSchema, string> msGroupSchemaRepo,
            IRepository<MS_GroupCommPct, string> msGroupCommPctRepo,
            IRepository<MS_GroupCommPctNonStd, string> msGroupCommPctNonStdRepo,
            IRepository<MS_PPhRange, string> msPPhRangeRepo,
            IRepository<MS_PPhRangeIns, string> msPPhRangeInsRepo,
            IRepository<MS_PointPct, string> msPointPctRepo,
            IRepository<TR_CommPayment, string> trCommPaymentRepo,
            IRepository<TR_CommPaymentPph, string> trCommPaymentPphRepo,
            IRepository<TR_ManagementFee, string> trManagementFeeRepo,
            IRepository<TR_SoldUnit, string> trSoldUnitRepo,
            IRepository<TR_SoldUnitRequirement, string> trSoldUnitReqRepo,
            IHttpContextAccessor httpContextAccessor,
            FilesHelper filesHelper,
            IHostingEnvironment environment,
            NewCommDbContext context,
            DemoDbContext contextDemo
        )
        {
            _msSchemaRepo = msSchemaRepo;
            _msSchemaRequirementRepo = msSchemaRequirementRepo;
            _lkCommTypeRepo = lkCommTypeRepo;
            _msStatusMemberRepo = msStatusMemberRepo;
            _lkPointTypeRepo = lkPointTypeRepo;
            _lkUplineRepo = lkUplineRepo;
            _msCommPctRepo = msCommPctRepo;
            _msPropertyRepo = msPropertyRepo;
            _msDeveloperSchemaRepo = msDeveloperSchemaRepo;
            _msGroupSchemaRepo = msGroupSchemaRepo;
            _msGroupCommPctRepo = msGroupCommPctRepo;
            _msGroupCommPctNonStdRepo = msGroupCommPctNonStdRepo;
            _msPPhRangeRepo = msPPhRangeRepo;
            _msPPhRangeInsRepo = msPPhRangeInsRepo;
            _msPointPctRepo = msPointPctRepo;
            _trCommPaymentRepo = trCommPaymentRepo;
            _trCommPaymentPphRepo = trCommPaymentPphRepo;
            _trManagementFeeRepo = trManagementFeeRepo;
            _trSoldUnitRepo = trSoldUnitRepo;
            _trSoldUnitReqRepo = trSoldUnitReqRepo;
            _HttpContextAccessor = httpContextAccessor;
            _filesHelper = filesHelper;
            _hostingEnvironment = environment;
            _context = context;
            _contextDemo = contextDemo;
        }

        private string GetIdUsername(long? Id)
        {
            string getUsername = (from u in _contextDemo.Users
                                  where u.Id == Id
                                  select u.UserName)
                       .DefaultIfEmpty("").First();

            return getUsername;
        }

        private static Uri getAbsolutUri()
        {
            var request = _HttpContextAccessor.HttpContext.Request;
            UriBuilder uriBuilder = new UriBuilder();
            uriBuilder.Scheme = request.Scheme;
            uriBuilder.Host = request.Host.ToString();
            uriBuilder.Path = request.Path.ToString();
            uriBuilder.Query = request.QueryString.ToString();
            return uriBuilder.Uri;
        }

        private string uploadFile(string filename)
        {
            try
            {
                return _filesHelper.MoveFiles(filename, @"Temp\Downloads\SchemaFile\", @"Assets\Upload\SchemaFile\");
            }
            catch (Exception ex)
            {
                Logger.DebugFormat("test() - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Error : {0}", ex.Message);
            }
        }

        public async Task<PagedResultDto<GetAllMsSchemaListDto>> GetAllMsSchema(GetMsSchemaListInput input)
        {
            var getSchema = (from x in _context.MS_Schema
                             select new GetAllMsSchemaListDto
                             {
                                 entityCode = x.entityCode,
                                 scmCode = x.scmCode,
                                 scmName = x.scmName,
                                 isActive = x.isActive
                             })
                             .WhereIf(
                                !input.Filter.IsNullOrWhiteSpace(),
                                u =>
                                    u.scmCode.Contains(input.Filter) ||
                                    u.scmName.Contains(input.Filter) ||
                                    u.dueDateComm.Equals(input.Filter)
                            );

            var dataCount = await getSchema.AsQueryable().CountAsync();

            var resultList = await getSchema.AsQueryable()
                .OrderByDescending(s => s.scmCode)
                .PageBy(input)
                .ToListAsync();

            var listDtos = resultList;

            return new PagedResultDto<GetAllMsSchemaListDto>(
                dataCount,
                listDtos);
        }

        public List<GetMsSchemaRequirementListDto> GetMsSchemaRequirementBySchemaCode(string scmCode)
        {
            if (string.IsNullOrEmpty(scmCode))
                throw new UserFriendlyException("Schema code is required!");

            var listResult = (from x in _context.MS_SchemaRequirement
                              where x.scmCode == scmCode
                              orderby x.reqNo
                              select new GetMsSchemaRequirementListDto
                              {
                                  entityCode = x.entityCode,
                                  scmCode = x.scmCode,
                                  reqNo = x.reqNo,
                                  reqDesc = x.reqDesc,
                                  pctPaid = x.pctPaid
                              }).ToList();

            return listResult;
        }

        public List<GetLkCommTypeListDto> GetLkCommTypeBySchemaCode(string scmCode)
        {
            if (string.IsNullOrEmpty(scmCode))
                throw new UserFriendlyException("Schema code is required!");

            var listResult = (from x in _context.LK_CommType
                              where x.scmCode == scmCode
                              orderby x.Id descending
                              select new GetLkCommTypeListDto
                              {
                                  entityCode = x.entityCode,
                                  scmCode = x.scmCode,
                                  commTypeCode = x.commTypeCode,
                                  commTypeName = x.commTypeName
                              }).ToList();

            return listResult;
        }

        public List<GetMsStatusMemberListDto> GetMsStatusMemberBySchemaCode(string scmCode)
        {
            if (string.IsNullOrEmpty(scmCode))
                throw new UserFriendlyException("Schema code is required!");

            var listResult = (from x in _context.MS_StatusMember
                              where x.scmCode == scmCode
                              orderby x.Id descending
                              select new GetMsStatusMemberListDto
                              {
                                  entityCode = x.entityCode,
                                  scmCode = x.scmCode,
                                  statusCode = x.statusCode,
                                  statusName = x.statusName,
                                  pointMin = x.pointMin,
                                  reviewTimeYear = x.reviewTimeYear,
                                  reviewStartMonth = x.reviewStartMonth,
                                  pointToKeepStatus = x.pointToKeepStatus,
                                  statusStar = x.statusStar
                              }).ToList();

            return listResult;
        }

        public List<GetLkPointTypeListDto> GetLkPointTypeBySchemaCode(string scmCode)
        {
            if (string.IsNullOrEmpty(scmCode))
                throw new UserFriendlyException("Schema code is required!");

            var listResult = (from x in _context.LK_PointType
                              where x.scmCode == scmCode
                              orderby x.Id descending
                              select new GetLkPointTypeListDto
                              {
                                  entityCode = x.entityCode,
                                  scmCode = x.scmCode,
                                  pointTypeCode = x.pointTypeCode,
                                  pointTypeName = x.pointTypeName
                              }).ToList();

            return listResult;
        }

        public List<GetMsCommPctListDto> GetMsCommPctBySchemaCode(string scmCode)
        {
            if (string.IsNullOrEmpty(scmCode))
                throw new UserFriendlyException("Schema code is required!");

            var listResult = (from commPct in _context.MS_CommPct
                              join commType in _context.LK_CommType on commPct.commTypeCode equals commType.commTypeCode
                              join status in _context.MS_StatusMember on commPct.statusCode equals status.statusCode
                              where commPct.scmCode == scmCode
                              && commPct.entityCode == commType.entityCode && commPct.scmCode == commType.scmCode
                              && commPct.entityCode == status.entityCode && commPct.scmCode == status.scmCode
                              select new GetMsCommPctListDto
                              {
                                  entityCode = commPct.entityCode,
                                  scmCode = commPct.scmCode,
                                  validDate = commPct.validDate,
                                  commTypeCode = commType.commTypeCode,
                                  commTypeName = commType.commTypeName,
                                  uplineNo = commPct.asUplineNo,
                                  minAmt = commPct.minAmt,
                                  maxAmt = commPct.maxAmt,
                                  statusCode = status.statusCode,
                                  statusName = status.statusName,
                                  commPctPaid = commPct.commPctPaid,
                                  commPctHold = commPct.commPctHold
                              }).ToList();
            return new List<GetMsCommPctListDto>(listResult);
        }

        public List<GetLkUplineListDto> GetLkUpline(string scmCode)
        {
            if (string.IsNullOrEmpty(scmCode))
                throw new UserFriendlyException("Schema code is required!");

            var listResult = (from x in _context.LK_Upline
                              where x.scmCode == scmCode
                              select new GetLkUplineListDto
                              {
                                  entityCode = x.entityCode,
                                  scmCode = x.scmCode,
                                  uplineNo = x.uplineNo
                              }).ToList();

            return listResult;
        }

        public CreateOrUpdateSetSchemaInputDto GetDetailMsSchema(string entityCode, string scmCode)
        {
            if (string.IsNullOrEmpty(entityCode))
                throw new UserFriendlyException("Entity code is required!");
            if (string.IsNullOrEmpty(scmCode))
                throw new UserFriendlyException("Schema code is required!");

            CreateOrUpdateSetSchemaInputDto listSchema = null;
            try
            {
                listSchema = (from x in _context.MS_Schema
                              join y in _context.LK_Upline
                              on x.scmCode equals y.scmCode
                              where x.scmCode == scmCode && x.entityCode == entityCode
                              orderby x.inputTime descending
                              select new CreateOrUpdateSetSchemaInputDto
                              {
                                  entityCode = x.entityCode,
                                  scmCode = x.scmCode,
                                  scmName = x.scmName,
                                  digitMemberCode = x.digitMemberCode,
                                  isActive = x.isActive
                              }).FirstOrDefault();
            }
            catch (Exception e)
            {
                SendConsole("" + e.Message + " " + e.StackTrace);
            }

            return listSchema;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_MasterSchema_Create)]
        public JObject CreateOrUpdateMsSchemaRequirement(CreateMsSchemaRequirementInputDto input)
        {
            JObject obj = new JObject();
            Logger.Info("CreateOrUpdateMsSchemaRequirement() Started.");

            foreach (var item in input.setCommReq)
            {
                var getMsSchemaReq = (from msr in _context.MS_SchemaRequirement
                                      where msr.entityCode == item.entityCode && msr.scmCode == item.scmCode && msr.reqNo == item.reqNo
                                      select msr).FirstOrDefault();

                if (getMsSchemaReq == null)
                {
                    var createMsSchemaRequirement = new MS_SchemaRequirement
                    {
                        entityCode = item.entityCode,
                        scmCode = item.scmCode,
                        reqNo = item.reqNo,
                        reqDesc = item.reqDesc,
                        pctPaid = item.pctPaid,
                        orPctPaid = item.pctPaid,
                        inputTime = DateTime.Now,
                        modifTime = DateTime.Now,
                        inputUN = GetIdUsername(AbpSession.UserId),
                        modifUN = GetIdUsername(AbpSession.UserId)
                    };

                    try
                    {
                        Logger.DebugFormat("CreateOrUpdateMsSchemaRequirement() - Start insert Schema Requirement. Parameters sent:{0}" +
                            "reqNo      = {1}{0}" +
                            "reqDesc    = {2}{0}" +
                            "pctPaid    = {3}{0}" +
                            "orPctPaid  = {4}{0}" +
                            "scmCode   = {5}{0}" +
                            "entityCode   = {6}"
                            , Environment.NewLine, item.reqNo, item.reqDesc, item.pctPaid, item.pctPaid, input.scmCode, item.entityCode);

                        _context.MS_SchemaRequirement.Add(createMsSchemaRequirement);
                        _context.SaveChanges();

                        obj.Add("message", "Created Successfully");

                        Logger.DebugFormat("CreateOrUpdateMsSchemaRequirement() - Ended insert Schema Requirement.");
                    }
                    catch (DbException ex)
                    {
                        Logger.ErrorFormat("CreateOrUpdateMsSchemaRequirement() ERROR DbException. Result = {0}", ex.Message);
                        throw new UserFriendlyException("DB Error: " + ex.Message);
                    }
                    catch (Exception ex)
                    {
                        Logger.ErrorFormat("CreateOrUpdateMsSchemaRequirement() ERROR Exception. Result = {0}", ex.Message);
                        throw new UserFriendlyException("Error: " + ex.Message);
                    }
                }
                else
                {
                    Logger.DebugFormat("CreateOrUpdateMsSchemaRequirement() - Start get data for update. Parameters sent:{0}" +
                            "scmCode      = {1}", item.scmCode);

                    var update = getMsSchemaReq.MapTo<MS_SchemaRequirement>();

                    update.reqDesc = item.reqDesc;
                    update.pctPaid = item.pctPaid;
                    update.orPctPaid = item.pctPaid;

                    try
                    {
                        Logger.DebugFormat("CreateOrUpdateMsSchemaRequirement() - Start update Schema Requirement. Parameters sent:{0}" +
                            "reqDesc    = {1}{0}" +
                            "pctPaid    = {2}{0}" +
                            "orPctPaid  = {3}{0}"
                            , Environment.NewLine, item.reqDesc, item.pctPaid, item.pctPaid);

                        _msSchemaRequirementRepo.Update(update);
                        CurrentUnitOfWork.SaveChanges();
                        obj.Add("message", "Updated successfully");

                        Logger.DebugFormat("CreateOrUpdateMsSchemaRequirement() - Ended update Schema Requirement.");
                    }
                    catch (DbException ex)
                    {
                        Logger.ErrorFormat("CreateOrUpdateMsSchemaRequirement() ERROR DbException. Result = {0}", ex.Message);
                        throw new UserFriendlyException("DB Error: " + ex.Message);
                    }
                    catch (Exception ex)
                    {
                        Logger.ErrorFormat("CreateOrUpdateMsSchemaRequirement() ERROR Exception. Result = {0}", ex.Message);
                        throw new UserFriendlyException("Error: " + ex.Message);
                    }
                }

            }
            Logger.Info("CreateOrUpdateMsSchemaRequirement() - Finished.");
            return obj;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_MasterSchema_Create)]
        public JObject CreateOrUpdateMsStatusMember(CreateMsStatusMemberInputDto input)
        {
            JObject obj = new JObject();
            Logger.Info("CreateOrUpdateMsStatusMember() Started.");

            foreach (var item in input.setStatusMember)
            {
                var getMsStatusMember = (from ms in _context.MS_StatusMember
                                         where ms.entityCode == item.entityCode && ms.scmCode == item.scmCode && ms.statusCode == item.statusCode
                                         select ms).FirstOrDefault();

                if (getMsStatusMember == null)
                {
                    var createMsStatusMember = new MS_StatusMember
                    {
                        entityCode = item.entityCode,
                        scmCode = item.scmCode,
                        statusCode = item.statusCode,
                        statusName = item.statusName,
                        pointMin = item.pointMin,
                        pointToKeepStatus = item.pointToKeepStatus,
                        reviewTimeYear = item.reviewTimeYear,
                        reviewStartMonth = item.reviewStartMonth,
                        statusStar = item.statusStar,
                        inputTime = DateTime.Now,
                        modifTime = DateTime.Now,
                        inputUN = GetIdUsername(AbpSession.UserId),
                        modifUN = GetIdUsername(AbpSession.UserId)
                    };

                    try
                    {
                        Logger.DebugFormat("CreateOrUpdateMsStatusMember() - Start insert Status Member. Parameters sent:{0}" +
                            "statusCode         = {1}{0}" +
                            "statusName         = {2}{0}" +
                            "pointMin           = {3}{0}" +
                            "pointToKeepStatus  = {4}{0}" +
                            "reviewTimeYear     = {5}{0}" +
                            "reviewStartMonth   = {6}{0}" +
                            "statusStar         = {7}{0}" +
                            "scmCode            = {8}{0}" +
                            "entityCode         = {9}{0}"
                            , Environment.NewLine, item.statusCode, item.statusName, item.pointMin, item.pointToKeepStatus, item.reviewTimeYear, item.reviewStartMonth, item.statusStar, input.scmCode, item.entityCode);

                        _context.MS_StatusMember.Add(createMsStatusMember);
                        _context.SaveChanges();

                        obj.Add("message", "Created Successfully");

                        Logger.DebugFormat("CreateOrUpdateMsStatusMember() - Ended insert Status Member.");
                    }
                    catch (DbException ex)
                    {
                        Logger.ErrorFormat("CreateOrUpdateMsStatusMember() ERROR DbException. Result = {0}", ex.Message);
                        throw new UserFriendlyException("DB Error: " + ex.Message);
                    }
                    catch (Exception ex)
                    {
                        Logger.ErrorFormat("CreateOrUpdateMsStatusMember() ERROR Exception. Result = {0}", ex.Message);
                        throw new UserFriendlyException("Error: " + ex.Message);
                    }
                }
                else
                {
                    Logger.DebugFormat("CreateOrUpdateMsStatusMember() - Start get data for update. Parameters sent:{0}" +
                            "scmCode      = {1}", item.scmCode);

                    var update = getMsStatusMember.MapTo<MS_StatusMember>();

                    update.statusName = item.statusName;
                    update.pointMin = item.pointMin;
                    update.pointToKeepStatus = item.pointToKeepStatus;
                    update.reviewTimeYear = item.reviewTimeYear;
                    update.reviewStartMonth = item.reviewStartMonth;
                    update.statusStar = item.statusStar;

                    try
                    {
                        Logger.DebugFormat("CreateOrUpdateMsStatusMember() - Start update Status Member. Parameters sent:{0}" +
                            "statusCode         = {1}{0}" +
                            "statusName         = {2}{0}" +
                            "pointMin           = {3}{0}" +
                            "pointToKeepStatus  = {4}{0}" +
                            "reviewTimeYear     = {5}{0}" +
                            "reviewStartMonth   = {6}{0}" +
                            "statusStar         = {7}{0}"
                            , Environment.NewLine, item.statusCode, item.statusName, item.pointMin, item.pointToKeepStatus, item.reviewTimeYear, item.reviewStartMonth, item.statusStar);

                        _msStatusMemberRepo.Update(update);
                        CurrentUnitOfWork.SaveChanges();
                        obj.Add("message", "Updated successfully");

                        Logger.DebugFormat("CreateOrUpdateMsStatusMember() - Ended update Status Member.");
                    }
                    catch (DbException ex)
                    {
                        Logger.ErrorFormat("CreateOrUpdateMsStatusMember() ERROR DbException. Result = {0}", ex.Message);
                        throw new UserFriendlyException("DB Error: " + ex.Message);
                    }
                    catch (Exception ex)
                    {
                        Logger.ErrorFormat("CreateOrUpdateMsStatusMember() ERROR Exception. Result = {0}", ex.Message);
                        throw new UserFriendlyException("Error: " + ex.Message);
                    }
                }

            }
            Logger.Info("CreateOrUpdateMsStatusMember() - Finished.");
            return obj;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_MasterSchema_Create)]
        public CreateOrUpdateMsSchemaResultsDto CreateOrUpdateMsSchema(CreateOrUpdateSetSchemaInputDto input)
        {
            if (string.IsNullOrEmpty(input.scmCode))
                throw new UserFriendlyException("Schema code is required!");
            if (string.IsNullOrEmpty(input.entityCode))
                throw new UserFriendlyException("Entity code is required!");

            CreateOrUpdateMsSchemaResultsDto scmID = new CreateOrUpdateMsSchemaResultsDto
            {
                entityCode = input.entityCode,
                scmCode = input.scmCode
            };
            short i = 0;

            Logger.Info("CreateOrUpdateMsSchema() - Started.");

            Logger.DebugFormat("CreateOrUpdateMsSchema() - Start checking existing code or name. Parameters sent:{0}" +
                            "scmCode        = {1}{0}" +
                            "entityCode        = {2}{0}"
                            , Environment.NewLine, input.scmCode, input.entityCode);

            var getMsSchema = (from ms in _context.MS_Schema
                               where ms.entityCode == input.entityCode && ms.scmCode == input.scmCode
                               select ms).FirstOrDefault();

            Logger.DebugFormat("CreateOrUpdateMsSchema() - End checking existing code or name.");

            if (getMsSchema == null)
            {
                Logger.DebugFormat("CreateOrUpdateMsSchema() - Start checking existing name and code. Parameters sent:{0}" +
                            "scmCode   = {1}{0}" +
                            "scmName   = {2}{0}"
                            , Environment.NewLine, input.scmCode, input.scmName);

                bool checkNameCode = (from x in _context.MS_Schema
                                      where x.scmCode == input.scmCode || x.scmName == input.scmName
                                      select x).Any();

                Logger.DebugFormat("CreateOrUpdateMsSchema() - End checking existing name and code. Resuly ={0}", checkNameCode);

                var createSetSchema = new MS_Schema
                {
                    entityCode = input.entityCode,
                    scmCode = input.scmCode,
                    scmName = input.scmName,
                    digitMemberCode = input.scmCode.Substring(1, 2),
                    accACDPeriod = 0,
                    accCDPeriod = 0,
                    accPeriod = 0,
                    isAcc = false,
                    isAccACD = false,
                    isAccCD = false,
                    isACDGetComm = false,
                    isActive = input.isActive,
                    isAutomaticMemberStatus = false,
                    isBudget = false,
                    isCapacity = false,
                    isCDGetComm = false,
                    isCommHold = false,
                    isFix = false,
                    isFixACD = false,
                    isFixCD = false,
                    isHaveACD = false,
                    isHaveCD = false,
                    isOverRiding = false,
                    isPointCalc = false,
                    isSendSMSPaid = false,
                    isTeam = false,
                    accACDPeriodType = "-",
                    accCDPeriodType = "-",
                    accPeriodType = "-",
                    isClub_ = false,
                    inputTime = DateTime.Now,
                    modifTime = DateTime.Now,
                    inputUN = GetIdUsername(AbpSession.UserId),
                    modifUN = GetIdUsername(AbpSession.UserId)
                };

                var dbContextTransaction = _context.Database.BeginTransaction();
                try
                {
                    Logger.DebugFormat("CreateOrUpdateMsSchema() - Start insert Schema. Parameters sent:{0}" +
                        "scmCode        = {1}{0}" +
                        "scmName        = {2}{0}" +
                        "digitMemberCode= {3}{0}" +
                        "accACDPeriod   = {4}{0}" +
                        "accCDPeriod    = {5}{0}" +
                        "accPeriod      = {6}{0}" +
                        "isAcc          = {7}{0}" +
                        "isAccACD       = {8}{0}" +
                        "isAccCD        = {9}{0}" +
                        "isACDGetComm   = {10}{0}" +
                        "isActive       = {11}{0}" +
                        "isAutomaticMemberStatus    = {12}{0}" +
                        "isBudget       = {13}{0}" +
                        "isCapacity     = {14}{0}" +
                        "isCDGetComm    = {15}{0}" +
                        "isClub         = {16}{0}" +
                        "isCommHold     = {17}{0}" +
                        "isFix          = {18}{0}" +
                        "isFixACD       = {19}{0}" +
                        "isFixCD        = {20}{0}" +
                        "isHaveACD      = {21}{0}" +
                        "isHaveCD       = {22}{0}" +
                        "isOverRiding   = {23}{0}" +
                        "isPointCalc    = {24}{0}" +
                        "isSendSMSPaid  = {25}{0}" +
                        "isTeam         = {26}{0}" +
                        "accACDPeriodType   = {27}{0}" +
                        "accCDPeriodType    = {28}{0}" +
                        "accPeriodType      = {29}{0}" +
                        "entityID           = {30}{0}"
                        , Environment.NewLine, input.scmCode, input.scmName, input.scmCode.Substring(1, 2), 0, 0, 0,
                        false, false, false, false, input.isActive, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
                        "-", "-", "-", input.entityCode);

                    _context.MS_Schema.Add(createSetSchema);
                    _context.SaveChanges();

                    Logger.DebugFormat("CreateOrUpdateMsSchema() - Ended insert Schema.");

                    do
                    {
                        var createLKUpline = new LK_Upline
                        {
                            entityCode = scmID.entityCode,
                            scmCode = scmID.scmCode,
                            uplineNo = i,
                            inputTime = DateTime.Now,
                            modifTime = DateTime.Now,
                            inputUN = GetIdUsername(AbpSession.UserId),
                            modifUN = GetIdUsername(AbpSession.UserId)
                        };

                        Logger.DebugFormat("CreateOrUpdateMsSchema() - Start insert LK_Upline. Parameters sent:{0}" +
                        "schemaID        = {1}{0}" +
                        "uplineNo        = {2}{0}"
                        , Environment.NewLine, scmID, i);

                        _context.LK_Upline.Add(createLKUpline);
                        _context.SaveChanges();

                        Logger.DebugFormat("CreateOrUpdateMsSchema() - Ended insert LK_Upline.");

                        i++;
                    } while (i <= input.upline);

                    dbContextTransaction.Commit();
                }
                catch (Exception ex)
                {
                    dbContextTransaction.Rollback();
                    Logger.ErrorFormat("CreateOrUpdateMsStatusMember() ERROR Exception. Result = {0}", ex.Message);
                    throw new UserFriendlyException("Error: " + ex.Message);
                }
                dbContextTransaction.Dispose();

                return scmID;
            }
            else
            {
                var updateMsSchema = getMsSchema.MapTo<MS_Schema>();

                updateMsSchema.scmName = input.scmName;
                updateMsSchema.isActive = input.isActive;

                var getUpline = (from upline in _context.LK_Upline
                                 where upline.scmCode == input.scmCode && upline.entityCode == input.entityCode
                                 select upline).ToList();

                var dbContextTransaction = _context.Database.BeginTransaction();
                try
                {
                    Logger.DebugFormat("CreateOrUpdateMsSchema() - Start update Schema. Parameters sent:{0}" +
                        "scmName      = {1}{0}" +
                        "isActive    = {2}{0}"
                        , Environment.NewLine, input.scmName, input.isActive);

                    _msSchemaRepo.Update(updateMsSchema);
                    Logger.DebugFormat("CreateOrUpdateMsSchema() - End update Schema.");

                    foreach (var item in getUpline)
                    {
                        try
                        {
                            Logger.DebugFormat("CreateOrUpdateMsSchema() - Start DELETE LK_Upline. Params sent:{0}", item);

                            _context.LK_Upline.Remove(item);
                            _context.SaveChanges();

                            Logger.DebugFormat("CreateOrUpdateMsSchema() - End DELETE LK_Upline.");
                        }
                        catch (Exception ex)
                        {
                            Logger.ErrorFormat("CreateOrUpdateMsSchema() ERROR Exception. Result = {0}", ex.Message);
                            throw new UserFriendlyException("Error: " + ex.Message);
                        }
                    }

                    do
                    {
                        var createLKUpline = new LK_Upline
                        {
                            entityCode = input.entityCode,
                            scmCode = input.scmCode,
                            uplineNo = i,
                            inputTime = DateTime.Now,
                            modifTime = DateTime.Now,
                            inputUN = GetIdUsername(AbpSession.UserId),
                            modifUN = GetIdUsername(AbpSession.UserId)
                        };

                        Logger.DebugFormat("CreateOrUpdateMsSchema() - Start INSERT LK_Upline. Params sent:{0}" +
                            "scmCode   ={1}{0}" +
                            "uplineNo   ={2}"
                            , Environment.NewLine, input.scmCode, i);

                        _context.LK_Upline.Add(createLKUpline);
                        _context.SaveChanges();

                        Logger.InfoFormat("INSERT LK_Upline Data: {0} {1}", input.scmCode, i);

                        Logger.DebugFormat("CreateOrUpdateMsSchema() - End INSERT LK_Upline.");

                        i++;
                    } while (i <= input.upline);

                    dbContextTransaction.Commit();
                }
                catch (Exception ex)
                {
                    dbContextTransaction.Rollback();
                    Logger.ErrorFormat("CreateOrUpdateMsSchema() ERROR Exception. Result = {0}", ex.Message);
                    throw new UserFriendlyException("Error: " + ex.Message);
                }
                dbContextTransaction.Dispose();

                Logger.Info("CreateOrUpdateMsSchema() - Finished.");
            }

            return scmID;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_MasterSchema_Create)]
        public JObject CreateOrUpdateLkPointType(CreateOrUpdateSetPointTypeInputDto input)
        {
            JObject obj = new JObject();
            Logger.Info("CreateOrUpdateLkPointType() - Started.");

            foreach (var item in input.setPointType)
            {
                Logger.DebugFormat("CreateOrUpdateLkPointType() - Start checking before insert LK Point Type. Parameters sent:{0}" +
                                   "scmCode = {1}{0}" +
                                   "pointTypeCode = {2}{0}" +
                                   "pointTypeName = {3}{0}"
                                   , Environment.NewLine, input.scmCode, item.pointTypeCode, item.pointTypeName);

                var getLKPointType = (from ms in _context.LK_PointType
                                      where ms.entityCode == item.entityCode && ms.scmCode == item.scmCode && ms.pointTypeCode == item.pointTypeCode
                                      select ms).FirstOrDefault();

                Logger.DebugFormat("CreateOrUpdateLkPointType() - Ended checking before insert LK Point Type.");

                if (getLKPointType == null)
                {
                    var createLkPointType = new LK_PointType
                    {
                        entityCode = item.entityCode,
                        scmCode = item.scmCode,
                        pointTypeCode = item.pointTypeCode,
                        pointTypeName = item.pointTypeName,
                        inputTime = DateTime.Now,
                        modifTime = DateTime.Now,
                        inputUN = GetIdUsername(AbpSession.UserId),
                        modifUN = GetIdUsername(AbpSession.UserId)
                    };

                    try
                    {
                        Logger.DebugFormat("CreateOrUpdateLkPointType() - Start insert LK Point Type. Parameters sent:{0}" +
                                    "pointTypeCode = {1}{0}" +
                                    "pointTypeName = {2}{0}" +
                                    "scmCode = {3}{0}" +
                                    "entityCode = {4}{0}"
                                    , Environment.NewLine, item.pointTypeCode, item.pointTypeName, input.scmCode, item.entityCode);

                        _context.LK_PointType.Add(createLkPointType);
                        _context.SaveChanges();

                        Logger.DebugFormat("CreateOrUpdateLkPointType() - Ended insert LK Point Type.");

                        obj.Add("message", "Created successfully");
                    }
                    catch (Exception ex)
                    {
                        Logger.ErrorFormat("CreateOrUpdateLkPointType() - ERROR Exception. Result = {0}", ex.Message);
                        throw new UserFriendlyException("Error: " + ex.Message);
                    }
                }
                else
                {
                    var update = getLKPointType.MapTo<LK_PointType>();
                    update.pointTypeName = item.pointTypeName;

                    try
                    {
                        Logger.DebugFormat("CreateOrUpdateLkPointType() - Start update LK Point Type. Parameters sent:{0}" +
                                    "pointTypeCode = {1}{0}" +
                                    "pointTypeName = {2}{0}"
                                    , Environment.NewLine, item.pointTypeCode, item.pointTypeName);

                        _context.LK_PointType.Update(update);
                        _context.SaveChanges();

                        Logger.DebugFormat("CreateOrUpdateLkPointType() - Ended update LK Point Type.");

                        obj.Add("message", "Updated successfully");
                    }
                    catch (Exception ex)
                    {
                        Logger.ErrorFormat("CreateOrUpdateLkPointType() - ERROR Exception. Result = {0}", ex.Message);
                        throw new UserFriendlyException("Error: " + ex.Message);
                    }
                }
            }

            Logger.Info("CreateOrUpdateLkPointType() - Finished.");
            return obj;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_MasterSchema_Create)]
        public JObject CreateOrUpdateLkCommType(CreateOrUpdateSetCommTypeInputDto input)
        {
            JObject obj = new JObject();
            Logger.Info("CreateOrUpdateLkCommType() - Started.");

            foreach (var item in input.setCommType)
            {
                Logger.DebugFormat("CreateOrUpdateLkCommType() - Start checking before insert LK Comm Type. Parameters sent:{0}" +
                                  "scmCode = {1}{0}" +
                                  "commTypeCode = {2}{0}" +
                                  "commTypeName = {3}{0}" +
                                  "entityCode = {4}{0}"
                                  , Environment.NewLine, input.scmCode, item.commTypeCode, item.commTypeName, item.entityCode);

                var getLkCommType = (from ms in _context.LK_CommType
                                     where ms.entityCode == item.entityCode && ms.scmCode == item.scmCode && ms.commTypeCode == item.commTypeCode
                                     select ms).FirstOrDefault();

                Logger.DebugFormat("CreateOrUpdateLkCommType() - Ended checking before insert LK Comm Type.");

                if (getLkCommType == null)
                {
                    var createLkCommType = new LK_CommType
                    {
                        entityCode = item.entityCode,
                        scmCode = item.scmCode,
                        commTypeCode = item.commTypeCode,
                        commTypeName = item.commTypeName,
                        inputTime = DateTime.Now,
                        modifTime = DateTime.Now,
                        inputUN = GetIdUsername(AbpSession.UserId),
                        modifUN = GetIdUsername(AbpSession.UserId)
                    };

                    try
                    {
                        Logger.DebugFormat("CreateOrUpdateLkCommType() - Start insert LK Comm Type. Parameters sent:{0}" +
                                    "commTypeCode = {1}{0}" +
                                    "pointTypeName = {2}{0}" +
                                    "scmCode = {3}{0}" +
                                    "entityCode = {4}{0}"
                                    , Environment.NewLine, item.commTypeCode, item.commTypeName, input.scmCode, item.entityCode);

                        _context.LK_CommType.Add(createLkCommType);
                        _context.SaveChanges();

                        Logger.DebugFormat("CreateOrUpdateLkCommType() - Ended insert LK Comm Type.");

                        obj.Add("message", "Created successfully");
                    }
                    catch (Exception ex)
                    {
                        Logger.ErrorFormat("CreateOrUpdateLkCommType() - ERROR Exception. Result = {0}", ex.Message);
                        throw new UserFriendlyException("Error: " + ex.Message);
                    }
                }
                else
                {
                    var update = getLkCommType.MapTo<LK_CommType>();
                    update.commTypeName = item.commTypeName;

                    try
                    {
                        Logger.DebugFormat("CreateOrUpdateLkCommType() - Start update LK Comm Type. Parameters sent:{0}" +
                                    "commTypeCode = {1}{0}" +
                                    "commTypeName = {2}{0}"
                                    , Environment.NewLine, item.commTypeCode, item.commTypeName);

                        _context.LK_CommType.Update(update);
                        _context.SaveChanges();

                        Logger.DebugFormat("CreateOrUpdateLkCommType() - Ended update LK Comm Type.");

                        obj.Add("message", "Updated successfully");
                    }
                    catch (Exception ex)
                    {
                        Logger.ErrorFormat("CreateOrUpdateLkCommType() - ERROR Exception. Result = {0}", ex.Message);
                        throw new UserFriendlyException("Error: " + ex.Message);
                    }
                }

            }
            Logger.Info("CreateOrUpdateLkCommType() - Finished.");
            return obj;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_MasterSchema_Create)]
        public JObject CreateOrUpdateMsCommPct(CreateOrUpdateMsCommPctInputDto input)
        {
            JObject obj = new JObject();
            Logger.Info("CreateOrUpdateMsCommPct() - Started.");

            foreach (var item in input.setCommPct)
            {
                var getMsCommPct = (from ms in _context.MS_CommPct
                                    where ms.entityCode == item.entityCode && ms.scmCode == item.scmCode && ms.statusCode == item.statusCode
                                    && ms.asUplineNo == item.uplineNo && ms.validDate == item.validDate && ms.minAmt == item.minAmt
                                    select ms).FirstOrDefault();

                if (getMsCommPct == null)
                {
                    var createMsCommPct = new MS_CommPct
                    {
                        entityCode = item.entityCode,
                        scmCode = item.scmCode,
                        statusCode = item.statusCode,
                        validDate = item.validDate,
                        asUplineNo = item.uplineNo,
                        commTypeCode = item.commTypeCode,
                        commPctPaid = item.commPctPaid,
                        commPctHold = item.commPctHold,
                        minAmt = item.minAmt,
                        maxAmt = item.maxAmt,
                        inputTime = DateTime.Now,
                        modifTime = DateTime.Now,
                        inputUN = GetIdUsername(AbpSession.UserId),
                        modifUN = GetIdUsername(AbpSession.UserId)
                    };

                    try
                    {
                        Logger.DebugFormat("CreateOrUpdateMsCommPct() - Start insert Comm Pct. Parameters sent:{0}" +
                                        "scmCode = {1}{0}" +
                                        "validDate = {2}{0}" +
                                        "commTypeCode = {3}{0}" +
                                        "statusCode = {4}{0}" +
                                        "asUplineNo = {5}{0}" +
                                        "commPctPaid = {6}{0}"
                                        , Environment.NewLine, input.scmCode, item.validDate, item.commTypeCode, item.statusCode
                                        , item.uplineNo, item.commPctPaid);

                        _context.MS_CommPct.Add(createMsCommPct);
                        _context.SaveChanges();

                        Logger.DebugFormat("CreateOrUpdateMsCommPct() - Ended insert Comm Pct.");

                        obj.Add("message", "Created successfully");
                    }
                    catch (Exception ex)
                    {
                        Logger.ErrorFormat("CreateOrUpdateMsCommPct() - ERROR Exception. Result = {0}", ex.Message);
                        throw new UserFriendlyException("Error: " + ex.Message);
                    }
                }
                else
                {
                    var update = getMsCommPct.MapTo<MS_CommPct>();

                    update.commTypeCode = item.commTypeCode;
                    update.commPctPaid = item.commPctPaid;
                    update.commPctHold = item.commPctHold;
                    update.maxAmt = item.maxAmt;

                    try
                    {
                        Logger.DebugFormat("CreateOrUpdateMsCommPct() - Start update Comm Pct. Parameters sent:{0}" +
                                        "commTypeCode = {1}{0}" +
                                        "statusCode = {2}{0}" +
                                        "asUplineNo = {3}{0}" +
                                        "commPctPaid = {4}{0}" +
                                        "commPctHold = {5}{0}"
                                        , Environment.NewLine, item.commTypeCode, item.statusCode
                                        , item.uplineNo, item.commPctPaid, item.commPctHold);

                        _context.MS_CommPct.Update(update);
                        _context.SaveChanges();

                        Logger.DebugFormat("CreateOrUpdateMsCommPct() - Ended update Comm Pct.");

                        obj.Add("message", "Updated successfully");
                    }
                    catch (Exception ex)
                    {
                        Logger.ErrorFormat("CreateOrUpdateMsCommPct() - ERROR Exception. Result = {0}", ex.Message);
                        throw new UserFriendlyException("Error: " + ex.Message);
                    }
                }
            }

            Logger.Info("CreateOrUpdateMsCommPct() - Finished.");
            return obj;
        }

        /*
        [AbpAuthorize(AppPermissions.Pages_Tenant_MasterSchema_Delete)]
        public void DeleteLkPointType(int Id, string flag)
        {
            Logger.Info("DeleteLkPointType() - Started.");

            //delete-add
            if (flag == "add")
            {
                try
                {
                    Logger.DebugFormat("DeleteLkPointType() - Start hard delete LK Point Type. Parameters sent:{0}" +
                        "pointTypeID = {1}{0}"
                        , Environment.NewLine, Id);

                    _lkPointTypeRepo.Delete(Id);
                    CurrentUnitOfWork.SaveChanges(); //execution saved inside try

                    Logger.DebugFormat("DeleteLkPointType() - Ended hard delete LK Point Type.");
                }
                catch (DataException ex)
                {
                    Logger.ErrorFormat("DeleteLkPointType() - ERROR DataException. Result = {0}", ex.Message);
                    throw new UserFriendlyException("Db Error: " + ex.Message);
                }
                catch (Exception ex)
                {
                    Logger.ErrorFormat("DeleteLkPointType() - ERROR Exception. Result = {0}", ex.Message);
                    throw new UserFriendlyException("Error: " + ex.Message);
                }
            }
            //delete-edit
            else
            {
                //LkPointType
                var getLkPointType = (from x in _lkPointTypeRepo.GetAll()
                                      where x.Id == Id
                                      select x).FirstOrDefault();


                var updateLkPointType = getLkPointType.MapTo<LK_PointType>();

                updateLkPointType.isComplete = false;

                Logger.DebugFormat("DeleteLkPointType() - Start soft delete LK Point Type. Parameters sent:{0}" +
                        "isComplete = {1}{0}"
                        , Environment.NewLine, false);

                _lkPointTypeRepo.Update(updateLkPointType);

                Logger.DebugFormat("DeleteLkPointType() - Ended soft delete LK Point Type.");

                //PointPct
                var getPointPct = (from x in _msPointPctRepo.GetAll()
                                   where x.pointTypeID == Id
                                   select x).ToList();

                foreach (var item in getPointPct)
                {
                    var updatePointPct = item.MapTo<MS_PointPct>();

                    updatePointPct.isComplete = false;

                    Logger.DebugFormat("DeleteLkPointType() - Start soft delete Point Pct. Parameters sent:{0}" +
                        "isComplete = {1}{0}"
                        , Environment.NewLine, false);

                    _msPointPctRepo.Update(updatePointPct);

                    Logger.DebugFormat("DeleteLkPointType() - Ended soft delete Point Pct.");
                }
            }
            Logger.Info("DeleteLkPointType() - Finished.");
        }
       
                [AbpAuthorize(AppPermissions.Pages_Tenant_MasterSchema_Delete)]
                public void DeleteMsSchema(int Id)
                {
                    Logger.Info("DeleteMsSchema() - Started.");

                    Logger.DebugFormat("DeleteMsSchema() - Start checking before delete TR Comm Pay. Parameters sent:{0}" +
                                "schemaID = {1}{0}"
                                , Environment.NewLine, Id);

                    var cekTrCommPay = (from a in _trCommPaymentRepo.GetAll()
                                        where a.schemaID == Id
                                        select a).Any();

                    Logger.DebugFormat("DeleteMsSchema() - Ended checking before delete TR Comm Pay. Result = {0}", cekTrCommPay);

                    Logger.DebugFormat("DeleteMsSchema() - Start checking before delete TR Comm Pay PPH. Parameters sent:{0}" +
                                "schemaID = {1}{0}"
                                , Environment.NewLine, Id);

                    var cekTrCommPayPph = (from b in _trCommPaymentPphRepo.GetAll()
                                           where b.schemaID == Id
                                           select b).Any();

                    Logger.DebugFormat("DeleteMsSchema() - Ended checking before delete TR Comm Pay PPH. Result = {0}", cekTrCommPayPph);

                    Logger.DebugFormat("DeleteMsSchema() - Start checking before delete TR Management Fee. Parameters sent:{0}" +
                                "schemaID = {1}{0}"
                                , Environment.NewLine, Id);

                    var cekTrManagementFee = (from c in _trManagementFeeRepo.GetAll()
                                              where c.schemaID == Id
                                              select c).Any();

                    Logger.DebugFormat("DeleteMsSchema() - Ended checking before delete TR Management Fee. Result = {0}", cekTrManagementFee);

                    Logger.DebugFormat("DeleteMsSchema() - Start checking before delete TR Sold Unit. Parameters sent:{0}" +
                                "schemaID = {1}{0}"
                                , Environment.NewLine, Id);

                    var cekTrSoldUnit = (from d in _trSoldUnitRepo.GetAll()
                                         where d.schemaID == Id
                                         select d).Any();

                    Logger.DebugFormat("DeleteMsSchema() - Ended checking before delete TR Sold Unit. Result = {0}", cekTrSoldUnit);

                    Logger.DebugFormat("DeleteMsSchema() - Start checking before delete TR Sold Unit Req. Parameters sent:{0}" +
                                "schemaID = {1}{0}"
                                , Environment.NewLine, Id);

                    var cekTrSoldUnitReq = (from e in _trSoldUnitReqRepo.GetAll()
                                            where e.schemaID == Id
                                            select e).Any();

                    Logger.DebugFormat("DeleteMsSchema() - Ended checking before delete TR Sold Unit Req. Result = {0}", cekTrSoldUnitReq);

                    if (!cekTrCommPay && !cekTrCommPayPph && !cekTrManagementFee && !cekTrSoldUnit && !cekTrSoldUnitReq)
                    {
                        //Schema
                        var getSchema = (from x in _msSchemaRepo.GetAll()
                                         where x.Id == Id && x.isComplete == true
                                         select x).FirstOrDefault();

                        var updateSchema = getSchema.MapTo<MS_Schema>();

                        updateSchema.isComplete = false;

                        Logger.DebugFormat("DeleteMsSchema() - Start soft delete Schema. Parameters sent:{0}" +
                                "isComplete = {1}{0}"
                                , Environment.NewLine, false);

                        _msSchemaRepo.Update(updateSchema);

                        Logger.DebugFormat("DeleteMsSchema() - Ended soft delete Schema.");

                        //SchemaRequirement
                        var getSchemaRequirement = (from x in _msSchemaRequirementRepo.GetAll()
                                                    where x.schemaID == Id && x.isComplete == true
                                                    select x).ToList();

                        foreach (var item in getSchemaRequirement)
                        {
                            var updateSchemaRequirement = item.MapTo<MS_SchemaRequirement>();

                            updateSchemaRequirement.isComplete = false;

                            Logger.DebugFormat("DeleteMsSchema() - Start soft delete Schema Req. Parameters sent:{0}" +
                                "isComplete = {1}{0}"
                                , Environment.NewLine, false);

                            _msSchemaRequirementRepo.Update(updateSchemaRequirement);

                            Logger.DebugFormat("DeleteMsSchema() - Ended soft delete Schema Req.");
                        }

                        //Property
                        var getProperty = (from x in _msPropertyRepo.GetAll()
                                           where x.schemaID == Id && x.isComplete == true
                                           select x).ToList();

                        foreach (var item in getProperty)
                        {
                            var updateProperty = item.MapTo<MS_Property>();

                            updateProperty.isComplete = false;

                            Logger.DebugFormat("DeleteMsSchema() - Start soft delete Property. Parameters sent:{0}" +
                                "isComplete = {1}{0}"
                                , Environment.NewLine, false);

                            _msPropertyRepo.Update(updateProperty);

                            Logger.DebugFormat("DeleteMsSchema() - Ended soft delete Property.");
                        }

                        //DevSchema
                        var getDevSchema = (from x in _msDeveloperSchemaRepo.GetAll()
                                            where x.schemaID == Id && x.isComplete == true
                                            select x).ToList();

                        foreach (var item in getDevSchema)
                        {
                            var updateDevSchema = item.MapTo<MS_Developer_Schema>();

                            updateDevSchema.isComplete = false;

                            Logger.DebugFormat("DeleteMsSchema() - Start soft delete Developer Schema. Parameters sent:{0}" +
                                "isComplete = {1}{0}"
                                , Environment.NewLine, false);

                            _msDeveloperSchemaRepo.Update(updateDevSchema);

                            Logger.DebugFormat("DeleteMsSchema() - Ended soft delete Developer Schema.");
                        }

                        //GroupSchema
                        var getGroupSchema = (from x in _msGroupSchemaRepo.GetAll()
                                              where x.schemaID == Id && x.isComplete == true
                                              select x).ToList();

                        foreach (var item in getGroupSchema)
                        {
                            var updateGroupSchema = item.MapTo<MS_GroupSchema>();

                            updateGroupSchema.isComplete = false;

                            Logger.DebugFormat("DeleteMsSchema() - Start soft delete Group Schema. Parameters sent:{0}" +
                                "isComplete = {1}{0}"
                                , Environment.NewLine, false);

                            _msGroupSchemaRepo.Update(updateGroupSchema);

                            Logger.DebugFormat("DeleteMsSchema() - Ended soft delete Group Schema.");
                        }

                        //Status
                        var getStatus = (from x in _msStatusMemberRepo.GetAll()
                                         where x.schemaID == Id && x.isComplete == true
                                         select x).ToList();

                        foreach (var item in getStatus)
                        {
                            DeleteMsStatusMember(item.Id, "edit");
                        }

                        //CommType
                        var getCommType = (from x in _lkCommTypeRepo.GetAll()
                                           where x.schemaID == Id && x.isComplete == true
                                           select x).ToList();

                        foreach (var item in getCommType)
                        {
                            DeleteLkCommType(item.Id, "edit");
                        }

                        ////GroupCommPct
                        //var getGroupCommPct = (from x in _msGroupCommPctRepo.GetAll()
                        //                       where x.schemaID == Id && x.isComplete == true
                        //                       select x).ToList();

                        //foreach (var item in getGroupCommPct)
                        //{
                        //    var updateGroupCommPct = item.MapTo<MS_GroupCommPct>();

                        //    updateGroupCommPct.isComplete = false;

                        //    _msGroupCommPctRepo.Update(updateGroupCommPct);
                        //}

                        ////GroupCommPctNonStd
                        //var getGroupCommPctNonStd = (from x in _msGroupCommPctNonStdRepo.GetAll()
                        //                             where x.schemaID == Id && x.isComplete == true
                        //                             select x).ToList();

                        //foreach (var item in getGroupCommPctNonStd)
                        //{
                        //    var updateGroupCommPctNonStd = item.MapTo<MS_GroupCommPctNonStd>();

                        //    updateGroupCommPctNonStd.isComplete = false;

                        //    _msGroupCommPctNonStdRepo.Update(updateGroupCommPctNonStd);
                        //}

                        //Upline
                        var getUpline = (from x in _lkUplineRepo.GetAll()
                                         where x.schemaID == Id && x.isComplete == true
                                         select x).ToList();

                        foreach (var item in getUpline)
                        {
                            var updateUpline = item.MapTo<LK_Upline>();

                            updateUpline.isComplete = false;

                            Logger.DebugFormat("DeleteMsSchema() - Start soft delete Upline. Parameters sent:{0}" +
                                "isComplete = {1}{0}"
                                , Environment.NewLine, false);

                            _lkUplineRepo.Update(updateUpline);

                            Logger.DebugFormat("DeleteMsSchema() - Ended soft delete Upline.");
                        }

                        //CommPct
                        var getCommPct = (from x in _msCommPctRepo.GetAll()
                                          where x.schemaID == Id && x.isComplete == true
                                          select x).ToList();

                        foreach (var item in getCommPct)
                        {
                            DeleteMsCommPct(item.Id, "edit");
                        }

                        //PPhRange
                        var getPPhRange = (from x in _msPPhRangeRepo.GetAll()
                                           where x.schemaID == Id && x.isComplete == true
                                           select x).ToList();

                        foreach (var item in getPPhRange)
                        {
                            var updatePPhRange = item.MapTo<MS_PPhRange>();

                            updatePPhRange.isComplete = false;

                            Logger.DebugFormat("DeleteMsSchema() - Start soft delete PPH Range. Parameters sent:{0}" +
                                "isComplete = {1}{0}"
                                , Environment.NewLine, false);

                            _msPPhRangeRepo.Update(updatePPhRange);

                            Logger.DebugFormat("DeleteMsSchema() - Ended soft delete PPH Range.");
                        }

                        //PPhRangeIns
                        var getPPhRangeIns = (from x in _msPPhRangeInsRepo.GetAll()
                                              where x.schemaID == Id && x.isComplete == true
                                              select x).ToList();

                        foreach (var item in getPPhRangeIns)
                        {
                            var updatePPhRangeIns = item.MapTo<MS_PPhRangeIns>();

                            updatePPhRangeIns.isComplete = false;

                            Logger.DebugFormat("DeleteMsSchema() - Start soft delete PPH Range Ins. Parameters sent:{0}" +
                                "isComplete = {1}{0}"
                                , Environment.NewLine, false);

                            _msPPhRangeInsRepo.Update(updatePPhRangeIns);

                            Logger.DebugFormat("DeleteMsSchema() - Ended soft delete PPH Range Ins.");
                        }

                        //PointType
                        var getPointType = (from x in _lkPointTypeRepo.GetAll()
                                            where x.schemaID == Id && x.isComplete == true
                                            select x).ToList();

                        foreach (var item in getPointType)
                        {
                            DeleteLkPointType(item.Id, "edit");
                        }

                        //PointPct
                        var getPointPct = (from x in _msPointPctRepo.GetAll()
                                           where x.schemaID == Id && x.isComplete == true
                                           select x).ToList();

                        foreach (var item in getPointPct)
                        {
                            var updatePointPct = item.MapTo<MS_PointPct>();

                            Logger.DebugFormat("DeleteMsSchema() - Start soft delete Point Pct. Parameters sent:{0}" +
                                "isComplete = {1}{0}"
                                , Environment.NewLine, false);

                            updatePointPct.isComplete = false;

                            _msPointPctRepo.Update(updatePointPct);

                            Logger.DebugFormat("DeleteMsSchema() - Ended soft delete Point Pct.");
                        }

                    }
                    else
                    {
                        Logger.ErrorFormat("DeleteMsSchema() - ERROR. Result = {0}", "This Schema is used in transaction");
                        throw new UserFriendlyException("This Schema is used in transaction");
                    }
                    Logger.Info("DeleteMsSchema() - Finished.");
                }

                [AbpAuthorize(AppPermissions.Pages_Tenant_MasterSchema_Delete)]
                public void DeleteMsSchemaRequirement(int Id, string flag)
                {
                    Logger.Info("DeleteMsSchemaRequirement() - Started.");

                    //delete-add
                    if (flag == "add")
                    {
                        try
                        {
                            Logger.DebugFormat("DeleteMsSchemaRequirement() - Start hard delete Schema Requirement. Parameters sent:{0}" +
                                "pointTypeID = {1}{0}"
                                , Environment.NewLine, Id);

                            _msSchemaRequirementRepo.Delete(Id);
                            CurrentUnitOfWork.SaveChanges(); //execution saved inside try

                            Logger.DebugFormat("DeleteMsSchemaRequirement() - Ended hard delete Schema Requirement.");
                        }
                        catch (DataException ex)
                        {
                            Logger.ErrorFormat("DeleteMsSchemaRequirement() - ERROR DataException. Result = {0}", ex.Message);
                            throw new UserFriendlyException("Db Error: " + ex.Message);
                        }
                        catch (Exception ex)
                        {
                            Logger.ErrorFormat("DeleteMsSchemaRequirement() - ERROR Exception. Result = {0}", ex.Message);
                            throw new UserFriendlyException("Error: " + ex.Message);
                        }
                    }
                    //delete-edit
                    else
                    {
                        var getMsSchemaRequirement = (from A in _msSchemaRequirementRepo.GetAll()
                                                      where A.Id == Id
                                                      select A).FirstOrDefault();

                        var update = getMsSchemaRequirement.MapTo<MS_SchemaRequirement>();

                        update.isComplete = false;

                        try
                        {
                            Logger.DebugFormat("DeleteMsSchemaRequirement() - Start soft delete Schema Requirement. Parameters sent:{0}" +
                                "isComplete = {1}{0}"
                                , Environment.NewLine, false);

                            _msSchemaRequirementRepo.Update(update);

                            Logger.DebugFormat("DeleteMsSchemaRequirement() - Ended soft delete Schema Requirement.");
                        }
                        catch (DataException ex)
                        {
                            Logger.ErrorFormat("DeleteMsSchemaRequirement() - ERROR DataException. Result = {0}", ex.Message);
                            throw new UserFriendlyException("Db Error: " + ex.Message);
                        }
                        catch (Exception ex)
                        {
                            Logger.ErrorFormat("DeleteMsSchemaRequirement() - ERROR Exception. Result = {0}", ex.Message);
                            throw new UserFriendlyException("Error: " + ex.Message);
                        }
                    }
                    Logger.Info("DeleteMsSchemaRequirement() - Finished.");
                }

                [AbpAuthorize(AppPermissions.Pages_Tenant_MasterSchema_Delete)]
                public void DeleteMsStatusMember(int Id, string flag)
                {
                    Logger.Info("DeleteMsStatusMember() - Started.");

                    //delete-add
                    if (flag == "add")
                    {
                        try
                        {
                            Logger.DebugFormat("DeleteMsStatusMember() - Start hard delete Status Member. Parameters sent:{0}" +
                                "statusMemberID = {1}{0}"
                                , Environment.NewLine, Id);

                            _msStatusMemberRepo.Delete(Id);
                            CurrentUnitOfWork.SaveChanges(); //execution saved inside try

                            Logger.DebugFormat("DeleteMsStatusMember() - Ended hard delete Status Member.");
                        }
                        catch (DataException ex)
                        {
                            Logger.ErrorFormat("DeleteMsStatusMember() - ERROR DataException. Result = {0}", ex.Message);
                            throw new UserFriendlyException("Db Error: " + ex.Message);
                        }
                        catch (Exception ex)
                        {
                            Logger.ErrorFormat("DeleteMsStatusMember() - ERROR Exception. Result = {0}", ex.Message);
                            throw new UserFriendlyException("Error: " + ex.Message);
                        }
                    }
                    //delete-edit
                    else
                    {

                        //StatusMember
                        var getStatusMember = (from x in _msStatusMemberRepo.GetAll()
                                               where x.Id == Id && x.isComplete == true
                                               select x).FirstOrDefault();


                        var updateStatusMember = getStatusMember.MapTo<MS_StatusMember>();

                        updateStatusMember.isComplete = false;

                        Logger.DebugFormat("DeleteMsStatusMember() - Start soft delete Status Member. Parameters sent:{0}" +
                                "isComplete = {1}{0}"
                                , Environment.NewLine, false);

                        _msStatusMemberRepo.Update(updateStatusMember);

                        Logger.DebugFormat("DeleteMsStatusMember() - Ended soft delete Status Member.");

                        //CommPct
                        var getCommPct = (from x in _msCommPctRepo.GetAll()
                                          where x.statusMemberID == Id && x.isComplete == true
                                          select x).ToList();

                        foreach (var item in getCommPct)
                        {
                            var updateCommPct = item.MapTo<MS_CommPct>();

                            updateCommPct.isComplete = false;

                            Logger.DebugFormat("DeleteMsStatusMember() - Start soft delete Comm Pct. Parameters sent:{0}" +
                                "isComplete = {1}{0}"
                                , Environment.NewLine, false);

                            _msCommPctRepo.Update(updateCommPct);

                            Logger.DebugFormat("DeleteMsStatusMember() - Ended soft delete Comm Pct.");
                        }

                        //GroupCommPct
                        var getGroupCommPct = (from x in _msGroupCommPctRepo.GetAll()
                                               where x.statusMemberID == Id && x.isComplete == true
                                               select x).ToList();

                        foreach (var item in getGroupCommPct)
                        {
                            var updateGroupCommPct = item.MapTo<MS_GroupCommPct>();

                            updateGroupCommPct.isComplete = false;

                            Logger.DebugFormat("DeleteMsStatusMember() - Start soft delete Group Comm Pct. Parameters sent:{0}" +
                                "isComplete = {1}{0}"
                                , Environment.NewLine, false);

                            _msGroupCommPctRepo.Update(updateGroupCommPct);

                            Logger.DebugFormat("DeleteMsStatusMember() - Ended soft delete Group Comm Pct.");
                        }

                        //GroupCommPctNonStd
                        var getGroupCommPctNonStd = (from x in _msGroupCommPctNonStdRepo.GetAll()
                                                     where x.statusMemberID == Id && x.isComplete == true
                                                     select x).ToList();

                        foreach (var item in getGroupCommPctNonStd)
                        {
                            var updateGroupCommPctNonStd = item.MapTo<MS_GroupCommPctNonStd>();

                            updateGroupCommPctNonStd.isComplete = false;

                            Logger.DebugFormat("DeleteMsStatusMember() - Start soft delete Group Comm Pct Non STD. Parameters sent:{0}" +
                                "isComplete = {1}{0}"
                                , Environment.NewLine, false);

                            _msGroupCommPctNonStdRepo.Update(updateGroupCommPctNonStd);

                            Logger.DebugFormat("DeleteMsStatusMember() - Ended soft delete Group Comm Pct Non STD.");
                        }

                        //PointPct
                        var getPointPct = (from x in _msPointPctRepo.GetAll()
                                           where x.statusMemberID == Id && x.isComplete == true
                                           select x).ToList();

                        foreach (var item in getPointPct)
                        {
                            var updatePointPct = item.MapTo<MS_PointPct>();

                            updatePointPct.isComplete = false;

                            Logger.DebugFormat("DeleteMsStatusMember() - Start soft delete Point Pct. Parameters sent:{0}" +
                                "isComplete = {1}{0}"
                                , Environment.NewLine, false);

                            _msPointPctRepo.Update(updatePointPct);

                            Logger.DebugFormat("DeleteMsStatusMember() - Ended soft delete Point Pct.");
                        }

                    }
                    Logger.Info("DeleteMsStatusMember() - Finished.");
                }

                public void DeleteLkCommType(int Id, string flag)
                {
                    Logger.Info("DeleteLkCommType() - Started.");

                    //delete-add
                    if (flag == "add")
                    {
                        try
                        {
                            Logger.DebugFormat("DeleteLkCommType() - Start hard delete LK Comm Type. Parameters sent:{0}" +
                                "commTypeID = {1}{0}"
                                , Environment.NewLine, Id);

                            _lkCommTypeRepo.Delete(Id);
                            CurrentUnitOfWork.SaveChanges(); //execution saved inside try

                            Logger.DebugFormat("DeleteLkCommType() - Ended hard delete LK Point Type.");
                        }
                        catch (DataException ex)
                        {
                            Logger.ErrorFormat("DeleteLkCommType() - ERROR DataException. Result = {0}", ex.Message);
                            throw new UserFriendlyException("Db Error: " + ex.Message);
                        }
                        catch (Exception ex)
                        {
                            Logger.ErrorFormat("DeleteLkCommType() - ERROR Exception. Result = {0}", ex.Message);
                            throw new UserFriendlyException("Error: " + ex.Message);
                        }
                    }
                    //delete-edit
                    else
                    {
                        //CommPct
                        var getCommPct = (from x in _msCommPctRepo.GetAll()
                                          where x.commTypeID == Id && x.isComplete == true
                                          select x).ToList();

                        foreach (var item in getCommPct)
                        {
                            var updateCommPct = item.MapTo<MS_CommPct>();

                            updateCommPct.isComplete = false;

                            Logger.DebugFormat("DeleteLkCommType() - Start soft delete Comm Pct. Parameters sent:{0}" +
                                "isComplete = {1}{0}"
                                , Environment.NewLine, false);

                            _msCommPctRepo.Update(updateCommPct);

                            Logger.DebugFormat("DeleteLkCommType() - Ended soft delete Comm Pct.");
                        }

                        //GroupCommPct
                        var getGroupCommPct = (from x in _msGroupCommPctRepo.GetAll()
                                               where x.commTypeID == Id && x.isComplete == true
                                               select x).ToList();

                        foreach (var item in getGroupCommPct)
                        {
                            var updateGroupCommPct = item.MapTo<MS_GroupCommPct>();

                            updateGroupCommPct.isComplete = false;

                            Logger.DebugFormat("DeleteLkCommType() - Start soft delete Group Comm Pct. Parameters sent:{0}" +
                                "isComplete = {1}{0}"
                                , Environment.NewLine, false);

                            _msGroupCommPctRepo.Update(updateGroupCommPct);

                            Logger.DebugFormat("DeleteLkCommType() - Ended soft delete Group Comm Pct.");
                        }

                        //GroupCommPctNonStd
                        var getGroupCommPctNonStd = (from x in _msGroupCommPctNonStdRepo.GetAll()
                                                     where x.commTypeID == Id && x.isComplete == true
                                                     select x).ToList();

                        foreach (var item in getGroupCommPctNonStd)
                        {
                            var updateGroupCommPctNonStd = item.MapTo<MS_GroupCommPctNonStd>();

                            updateGroupCommPctNonStd.isComplete = false;

                            Logger.DebugFormat("DeleteLkCommType() - Start soft delete Group Comm Pct Non STD. Parameters sent:{0}" +
                                "isComplete = {1}{0}"
                                , Environment.NewLine, false);

                            _msGroupCommPctNonStdRepo.Update(updateGroupCommPctNonStd);

                            Logger.DebugFormat("DeleteLkCommType() - Ended soft delete Group Comm Pct Non STD.");
                        }

                        //CommType
                        var getLkCommType = (from A in _lkCommTypeRepo.GetAll()
                                             where A.Id == Id
                                             select A).FirstOrDefault();

                        var update = getLkCommType.MapTo<LK_CommType>();

                        update.isComplete = false;

                        try
                        {
                            Logger.DebugFormat("DeleteLkCommType() - Start soft delete LK Comm Type. Parameters sent:{0}" +
                                "isComplete = {1}{0}"
                                , Environment.NewLine, false);

                            _lkCommTypeRepo.Update(update);

                            Logger.DebugFormat("DeleteLkCommType() - Ended soft delete LK Comm Type.");
                        }
                        catch (DataException ex)
                        {
                            Logger.ErrorFormat("DeleteLkCommType() - ERROR DataException. Result = {0}", ex.Message);
                            throw new UserFriendlyException("Db Error: " + ex.Message);
                        }
                        catch (Exception ex)
                        {
                            Logger.ErrorFormat("DeleteLkCommType() - ERROR Exception. Result = {0}", ex.Message);
                            throw new UserFriendlyException("Error: " + ex.Message);
                        }
                    }
                    Logger.Info("DeleteLkCommType() - Finished.");
                }

                public void DeleteMsCommPct(int Id, string flag)
                {
                    Logger.Info("DeleteMsCommPct() - Started.");

                    //delete-add
                    if (flag == "add")
                    {
                        try
                        {
                            Logger.DebugFormat("DeleteMsCommPct() - Start hard delete Comm Pct. Parameters sent:{0}" +
                                "commPctID = {1}{0}"
                                , Environment.NewLine, Id);

                            _msCommPctRepo.Delete(Id);
                            CurrentUnitOfWork.SaveChanges(); //execution saved inside try

                            Logger.DebugFormat("DeleteMsCommPct() - Ended hard delete LK Point Type.");
                        }
                        catch (DataException ex)
                        {
                            Logger.ErrorFormat("DeleteMsCommPct() - ERROR DataException. Result = {0}", ex.Message);
                            throw new UserFriendlyException("Db Error: " + ex.Message);
                        }
                        catch (Exception ex)
                        {
                            Logger.ErrorFormat("DeleteMsCommPct() - ERROR Exception. Result = {0}", ex.Message);
                            throw new UserFriendlyException("Error: " + ex.Message);
                        }
                    }
                    //delete-edit
                    else
                    {
                        //CommPct
                        var getCommPct = (from x in _msCommPctRepo.GetAll()
                                          where x.Id == Id && x.isComplete == true
                                          select x).FirstOrDefault();

                        var updateCommPct = getCommPct.MapTo<MS_CommPct>();

                        updateCommPct.isComplete = false;

                        try
                        {
                            Logger.DebugFormat("DeleteMsCommPct() - Start soft delete Comm Pct. Parameters sent:{0}" +
                                "isComplete = {1}{0}"
                                , Environment.NewLine, false);

                            _msCommPctRepo.Update(updateCommPct);

                            Logger.DebugFormat("DeleteMsCommPct() - Ended soft delete Comm Pct.");
                        }
                        catch (DataException ex)
                        {
                            Logger.ErrorFormat("DeleteMsCommPct() - ERROR DataException. Result = {0}", ex.Message);
                            throw new UserFriendlyException("Db Error: " + ex.Message);
                        }
                        catch (Exception ex)
                        {
                            Logger.ErrorFormat("DeleteMsCommPct() - ERROR Exception. Result = {0}", ex.Message);
                            throw new UserFriendlyException("Error: " + ex.Message);
                        }
                    }
                    Logger.Info("DeleteMsCommPct() - Finished.");
                }
                 */

        private void GetURLWithoutHost(string path, out string finalpath)
        {
            finalpath = path;
            try
            {
                Regex RegexObj = new Regex("[\\w\\W]*([\\/]Assets[\\w\\W\\s]*)");
                if (RegexObj.IsMatch(path))
                {
                    finalpath = RegexObj.Match(path).Groups[1].Value;
                }
            }
            catch (ArgumentException ex)
            {
            }
        }

        private string GetURLWithoutHost(string path)
        {
            string finalpath = path;
            try
            {
                Regex RegexObj = new Regex("[\\w\\W]*([\\/]Assets[\\w\\W\\s]*)");
                if (RegexObj.IsMatch(path))
                {
                    finalpath = RegexObj.Match(path).Groups[1].Value;
                }
            }
            catch (ArgumentException ex)
            {
            }
            return finalpath;
        }

        private string getAbsoluteUriWithoutTail()
        {
            var request = _HttpContextAccessor.HttpContext.Request;
            UriBuilder uriBuilder = new UriBuilder();
            uriBuilder.Scheme = request.Scheme;
            uriBuilder.Host = request.Host.ToString();
            var test = uriBuilder.ToString();
            var result = test.Replace("[", "").Replace("]", "");
            int position = result.LastIndexOf('/');
            if (position > -1)
                result = result.Substring(0, result.Length - 1);

            if (request.PathBase != null)
            {
                if (!string.IsNullOrWhiteSpace(request.PathBase.Value))
                {
                    result += request.PathBase.Value;
                }
            }
            return result;
        }

        #region debug console
        private void SendConsole(string msg)
        {
            if (Setting_variabel.enable_tcp_debug == true)
            {
                if (Setting_variabel.Komunikasi_TCPListener == null)
                {
                    Setting_variabel.Komunikasi_TCPListener = new Visionet_Backend_NetCore.Komunikasi.Komunikasi_TCPListener(17000);
                    Task.Run(() => StartListenerLokal());
                }

                if (Setting_variabel.Komunikasi_TCPListener != null)
                {
                    if (Setting_variabel.Komunikasi_TCPListener.IsRunning())
                    {
                        if (Setting_variabel.ConsoleBayangan != null)
                        {
                            Setting_variabel.ConsoleBayangan.Send("[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "] " + msg);
                        }
                    }
                }
            }

        }
        #endregion

        #region listener tcp debug
        protected void StartListenerLokal()
        {
            if (Setting_variabel.Komunikasi_TCPListener != null)
                Setting_variabel.Komunikasi_TCPListener.StartListener();
        }

        protected void StopListenerLokal()
        {
            if (Setting_variabel.Komunikasi_TCPListener != null)
                Setting_variabel.Komunikasi_TCPListener.StopListener();
        }
        #endregion
    }
}
