﻿using Abp.Domain.Repositories;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.Commission.TR_DealClosers.Dto;
using VDI.Demo.NewCommDB;
using VDI.Demo.PersonalsDB;
using VDI.Demo.PropertySystemDB;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;
using Abp.UI;
using System.Data;
using Abp.AutoMapper;
using VDI.Demo.Commission.TR_SoldUnits;
using VDI.Demo.Commission.TR_SoldUnits.Dto;
using VDI.Demo.PropertySystemDB.LippoMaster;
using VDI.Demo.EntityFrameworkCore;
using Visionet_Backend_NetCore.Komunikasi;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace VDI.Demo.Commission.TR_DealClosers
{
    public class TrDealCloserAppService : DemoAppServiceBase, ITrDealCloserAppService
    {
        private readonly IRepository<PERSONALS_MEMBER, string> _personalMemberRepo;
        private readonly IRepository<PERSONALS, string> _personalRepo;
        private readonly IRepository<TR_SoldUnit, string> _trSoldUnitRepo;
        private readonly IRepository<TR_BookingHeader> _trBookingHeaderRepo;
        private readonly IRepository<TR_SoldUnit, string> _soldUnitRepo;
        private readonly IRepository<MS_Unit> _unitRepo;
        private readonly IRepository<MS_Project> _projectRepo;
        private readonly IRepository<MS_Cluster> _clusterRepo;
        private readonly IRepository<TR_SoldUnitRequirement, string> _soldUnitRequirementRepo;
        private readonly IRepository<TR_CommPct, string> _trCommPctRepo;
        //private readonly ITrSoldUnitAppService _trSoldUnitAppService;
        private readonly IRepository<MS_Developer_Schema, string> _msDeveloperSchemaRepo;
        private readonly IRepository<MS_Schema, string> _msSchemaRepo;
        private readonly IRepository<MS_Property, string> _msPropertyRepo;
        private readonly IRepository<MS_Unit> _msUnitRepo;
        private readonly IRepository<MS_UnitCode> _msUnitCodeRepo;
        private readonly PropertySystemDbContext _contextProp;
        private readonly PersonalsNewDbContext _contextPers;
        private readonly NewCommDbContext _contextCom;

        public TrDealCloserAppService(
            IRepository<PERSONALS_MEMBER, string> personalMemberRepo,
            IRepository<PERSONALS, string> personalRepo,
            IRepository<TR_BookingHeader> trBookingHeaderRepo,
            IRepository<TR_SoldUnit, string> trSoldUnitRepo,
            IRepository<TR_SoldUnit, string> soldUnitRepo,
            IRepository<MS_Unit> unitRepo,
            IRepository<MS_Project> projectRepo,
            IRepository<MS_Cluster> clusterRepo,
            IRepository<TR_SoldUnitRequirement, string> soldUnitRequirementRepo,
            IRepository<TR_CommPct, string> trCommPctRepo,
            IRepository<MS_Developer_Schema, string> msDeveloperSchemaRepo,
            IRepository<MS_Schema, string> msSchemaRepo,
            IRepository<MS_Property, string> msPropertyRepo,
            IRepository<MS_Unit> msUnitRepo,
            IRepository<MS_UnitCode> msUnitCodeRepo,
            //ITrSoldUnitAppService trSoldUnitAppService,            
            PropertySystemDbContext contextProp,
            PersonalsNewDbContext contextPers,
            NewCommDbContext contextCom
        )
        {
            _msUnitCodeRepo = msUnitCodeRepo;
            _msUnitRepo = msUnitRepo;
            _msSchemaRepo = msSchemaRepo;
            _msDeveloperSchemaRepo = msDeveloperSchemaRepo;
            _personalMemberRepo = personalMemberRepo;
            _personalRepo = personalRepo;
            _trBookingHeaderRepo = trBookingHeaderRepo;
            _trSoldUnitRepo = trSoldUnitRepo;
            _trCommPctRepo = trCommPctRepo;
            _soldUnitRepo = soldUnitRepo;
            _unitRepo = unitRepo;
            _projectRepo = projectRepo;
            _clusterRepo = clusterRepo;
            _soldUnitRequirementRepo = soldUnitRequirementRepo;
            _msPropertyRepo = msPropertyRepo;
            //_trSoldUnitAppService = trSoldUnitAppService;

            _contextProp = contextProp;
            _contextPers = contextPers;
            _contextCom = contextCom;
        }

        public List<GetTasklistDealCloserByProjectListDto> GetTasklistDealCloserByProject(int ProjectId)
        {
            SendConsole("testing");
            #region performance test
            SendConsole("start stopwatch");
            var watch = System.Diagnostics.Stopwatch.StartNew();
            #endregion

            SendConsole("step1");
            var statusNew = "New";
            var dataSoldUnit = (from tsu in _contextCom.TR_SoldUnit
                                join mds in _contextCom.MS_Developer_Schema on tsu.propCode equals mds.propCode
                                join mp in _contextCom.MS_Property on tsu.propCode equals mp.propCode
                                join tsur in _contextCom.TR_SoldUnitRequirement on tsu.bookNo equals tsur.bookNo into tsurs
                                from tsur in tsurs.DefaultIfEmpty().Distinct()
                                where !tsu.holdDate.HasValue && !tsu.cancelDate.HasValue
                                && tsu.devCode == mds.devCode && tsu.scmCode == mds.scmCode && tsu.entityCode == mds.entityCode
                                && mp.entityCode == tsu.entityCode && mp.scmCode == tsu.scmCode && tsur.bookNo == tsu.bookNo
                                select new
                                {
                                    entityCode = tsu.entityCode,
                                    scmCode = tsu.scmCode,
                                    devCode = tsu.devCode,
                                    memberCode = tsu.memberCode,
                                    propCode = mp.propCode,
                                    bookingCode = tsu.bookNo,
                                    unitCode = tsu.roadCode,
                                    unitNo = tsu.unitNo,
                                    status = statusNew,
                                    tsur.processDate
                                }).Distinct().ToList().Join(_contextProp.MS_Unit,
                                  t1 => new { t1.unitNo },
                                  t2 => new { t2.unitNo },
                                  (t1, t2) => new { t1, t2 })
                              .Where(o => o.t2.unitNo == o.t1.unitNo && o.t2.projectID == ProjectId)
                              .Select(o => new
                              {
                                  entityCode = o.t1.entityCode,
                                  scmCode = o.t1.scmCode,
                                  devCode = o.t1.devCode,
                                  memberCode = o.t1.memberCode,
                                  propCode = o.t1.propCode,
                                  bookingCode = o.t1.bookingCode,
                                  unitCode = o.t1.unitCode,
                                  unitNo = o.t1.unitNo,
                                  status = statusNew,
                                  o.t1.processDate,
                                  o.t2.projectID,
                                  o.t2.clusterID
                              });

            SendConsole("step2");
            var tasklistDealCloserByProject = (from dsu in dataSoldUnit
                                               join mp in _contextProp.MS_Project on dsu.projectID equals mp.Id
                                               join mc in _contextProp.MS_Cluster on dsu.clusterID equals mc.Id
                                               where mp.Id == ProjectId
                                               select new GetTasklistDealCloserByProjectListDto
                                               {
                                                   entityCode = dsu.entityCode,
                                                   scmCode = dsu.scmCode,
                                                   projectName = mp.projectName,
                                                   memberCode = dsu.memberCode,
                                                   propCode = dsu.propCode,
                                                   devCode = dsu.devCode,
                                                   bookingCode = dsu.bookingCode,
                                                   clusterName = mc.clusterName,
                                                   unitCode = dsu.unitCode,
                                                   unitNo = dsu.unitNo,
                                                   status = dsu.processDate.HasValue ? "Not New" : statusNew
                                               });

            #region performance test
            watch.Stop();
            var elapsedMs = watch.ElapsedMilliseconds;
            SendConsole("stop stopwatch: " + elapsedMs + " ms");
            #endregion

            return tasklistDealCloserByProject.Where(x => x.status == statusNew).ToList();
        }

        public List<GetMemberFromPersonalListDto> GetDropdownMemberFromPersonal()
        {
            SendConsole("Testing");
            var dataMember = (from A in _contextPers.PERSONALS_MEMBER
                              join B in _contextPers.PERSONAL on A.psCode equals B.psCode
                              where A.psCode == B.psCode
                              select new GetMemberFromPersonalListDto
                              {
                                  psCode = A.psCode,
                                  memberCode = A.memberCode,
                                  memberName = B.name
                              }).ToList();

            return dataMember;
        }

        /*
        public void UpdateDealCloser(MemberFromPersonalInputDto input, int limitAsUplineNo)
        {
            Logger.Info("UpdateDealCloser() - Started.");

            Logger.DebugFormat("UpdateDealCloser() - Start get data before update Deal Closer. Parameters sent:{0}" +
                        "bookNo = {1}{0}"
                        , Environment.NewLine, input.bookNo);

            var getDealCloser = (from A in _trSoldUnitRepo.GetAll()
                                 where A.bookNo == input.bookNo
                                 select A).FirstOrDefault();

            Logger.DebugFormat("UpdateDealCloser() - Ended get data before update Deal Closer. Result = {0}", getDealCloser);

            if (getDealCloser != null)
            {
                Logger.DebugFormat("UpdateDealCloser() - Start get data Member before update Deal Closer. Parameters sent:{0}" +
                        "memberCode = {1}{0}"
                        , Environment.NewLine, input.memberCode);

                var getDataMember = (from A in _personalMemberRepo.GetAll()
                                     where A.memberCode == input.memberCode
                                     select A).FirstOrDefault();

                Logger.DebugFormat("UpdateDealCloser() - Ended get data Member before update Deal Closer. Result = {0}", getDataMember);

                Logger.DebugFormat("UpdateDealCloser() - Start get data Unit ID before update Deal Closer. Parameters sent:{0}" +
                        "unitNo = {1}{0}" +
                        "unitCode = {2}{0}"
                        , Environment.NewLine, getDealCloser.unitNo, getDealCloser.roadCode);

                var unitID = (from x in _msUnitRepo.GetAll()
                              join a in _msUnitCodeRepo.GetAll() on x.unitCodeID equals a.Id
                              where x.unitNo == getDealCloser.unitNo && a.unitCode == getDealCloser.roadCode
                              select x.Id).FirstOrDefault();

                Logger.DebugFormat("UpdateDealCloser() - Ended get data Unit ID before update Deal Closer. Result = {0}", unitID);

                Logger.DebugFormat("UpdateDealCloser() - Start get data TR Booking Header before update Deal Closer. Parameters sent:{0}" +
                        "unitID = {1}{0}"
                        , Environment.NewLine, unitID);

                var getTrBookingHeader = (from A in _trBookingHeaderRepo.GetAll()
                                          where A.unitID == unitID
                                          select A).FirstOrDefault();

                Logger.DebugFormat("UpdateDealCloser() - Ended get data TR Booking Header before update Deal Closer. Result = {0}", getTrBookingHeader);

                Logger.DebugFormat("UpdateDealCloser() - Start get data Schema ID before update Deal Closer. Parameters sent:{0}" +
                        "scmCode = {1}{0}"
                        , Environment.NewLine, getDataMember.scmCode);

                var getIDSchema = (from A in _msSchemaRepo.GetAll()
                                   where getDataMember.scmCode == A.scmCode
                                   select A.Id).FirstOrDefault();

                Logger.DebugFormat("UpdateDealCloser() - Ended get data Schema ID before update Deal Closer. Result = {0}", getIDSchema);

                //Update TR_BookingHeader
                var updateTrBookingHeader = getTrBookingHeader.MapTo<TR_BookingHeader>();
                updateTrBookingHeader.schemaID = getIDSchema;
                updateTrBookingHeader.memberCode = input.memberCode;
                updateTrBookingHeader.remarks = input.reason;
                //Update TR_SoldUnit
                var updateDealCloser = getDealCloser.MapTo<TR_SoldUnit>();
                updateDealCloser.memberCode = input.memberCode;
                updateDealCloser.Remarks = input.reason;
                updateDealCloser.schemaID = getIDSchema;
                //Insert TR_CommPct
                GetDataMemberUplineInsertInputDto inputTrCommPct = new GetDataMemberUplineInsertInputDto();
                inputTrCommPct.bookNo = input.bookNo;
                inputTrCommPct.devCode = input.devCode;
                inputTrCommPct.developerSchemaID = input.developerSchemaID;
                inputTrCommPct.memberCode = input.memberCode;

                var dataUpline = _trSoldUnitAppService.GetDataMemberUplineInsert(inputTrCommPct, limitAsUplineNo);

                if (dataUpline.Any())
                {
                    var dataUplineExist = dataUpline.Select(x => new TR_CommPct
                    {
                        bookNo = x.bookNo,
                        developerSchemaID = x.developerSchemaID,
                        asUplineNo = (short)x.asUplineNo,
                        commPctPaid = (double)x.commPctPaid,
                        commTypeID = x.commTypeID,
                        memberCodeN = x.memberCodeN,
                        memberCodeR = x.memberCodeR,
                        pointTypeID = x.pointTypeID,
                        pphRangeID = x.pphRangeID,
                        pphRangeInsID = x.pphRangeInsID,
                        statusMemberID = x.statusMemberID
                    }).ToList();

                    try
                    {
                        Logger.DebugFormat("UpdateDealCloser() - Start update TR Booking Header. Parameters sent:{0}" +
                                            "scmCode = {1}{0}" +
                                            "memberCode = {2}{0}" +
                                            "remarks = {3}{0}"
                                            , Environment.NewLine, getDataMember.scmCode, input.memberCode, input.reason);

                        _trBookingHeaderRepo.Update(updateTrBookingHeader);

                        Logger.DebugFormat("UpdateDealCloser() - Ended update TR Booking Header.");

                        Logger.DebugFormat("UpdateDealCloser() - Start update TR Sold Unit. Parameters sent:{0}" +
                                            "memberCode = {1}{0}" +
                                            "Remarks = {2}{0}" +
                                            "schemaID = {3}{0}"
                                            , Environment.NewLine, input.memberCode, input.reason, getIDSchema);

                        _trSoldUnitRepo.Update(updateDealCloser);

                        Logger.DebugFormat("UpdateDealCloser() - Ended update TR Sold Unit.");

                        var insertData = dataUplineExist.MapTo<List<TR_CommPct>>();
                        //_trCommPctRepo.BulkInsertOrUpdate(insertData);

                        CurrentUnitOfWork.SaveChanges(); //execution saved inside try
                    }
                    catch (DataException ex)
                    {
                        Logger.ErrorFormat("UpdateDealCloser() - ERROR DataException. Result = {0}", ex.Message);
                        throw new UserFriendlyException("Db Error: " + ex.Message);
                    }
                    catch (Exception ex)
                    {
                        Logger.ErrorFormat("UpdateDealCloser() - ERROR Exception. Result = {0}", ex.Message);
                        throw new UserFriendlyException("Error: " + ex.Message);
                    }
                }
                else
                {
                    Logger.ErrorFormat("UpdateDealCloser() - ERROR. Result = {0}", "Member with commission not exist !");
                    throw new UserFriendlyException("Member with commission not exist !");
                }


            }
            Logger.Info("UpdateDealCloser() - Finished.");
        }

    */

        public GetDataEditDealCloserListDto GetDataEditDealCloser(string bookNo)
        {
            SendConsole("testing");
            #region performance test
            SendConsole("start stopwatch");
            var watch = System.Diagnostics.Stopwatch.StartNew();
            #endregion

            SendConsole("step1");
            var dataSoldUnit = (from tsu in _contextCom.TR_SoldUnit
                                join mds in _contextCom.MS_Developer_Schema on tsu.propCode equals mds.propCode
                                join mp in _contextCom.MS_Property on tsu.propCode equals mp.propCode
                                where !tsu.holdDate.HasValue && !tsu.cancelDate.HasValue
                                && tsu.devCode == mds.devCode && tsu.scmCode == mds.scmCode && tsu.entityCode == mds.entityCode
                                && mp.entityCode == tsu.entityCode && mp.scmCode == tsu.scmCode
                                && tsu.bookNo == bookNo
                                select new
                                {
                                    entityCode = tsu.entityCode,
                                    scmCode = tsu.scmCode,
                                    devCode = tsu.devCode,
                                    memberCode = tsu.memberCode,
                                    propCode = mp.propCode,
                                    bookingCode = tsu.bookNo,
                                    unitCode = tsu.roadCode,
                                    unitNo = tsu.unitNo,
                                    unitPrice = tsu.unitPrice,
                                    ppjbDate = tsu.PPJBDate,
                                }).Distinct().ToList()
                                .Join(
                                    from mu in _contextProp.MS_Unit
                                    join mp in _contextProp.MS_Project on mu.projectID equals mp.Id                                   
                                    select new { mu.unitNo, mu.projectID, mu.clusterID, mu.unitCodeID, mp.projectName}
                                  ,
                                  t1 => new { t1.unitNo },
                                  t2 => new { t2.unitNo },
                                  (t1, t2) => new { t1, t2 })
                              .Where(o => o.t2.unitNo == o.t1.unitNo)
                              .Select(o => new
                              {
                                  entityCode = o.t1.entityCode,
                                  scmCode = o.t1.scmCode,
                                  devCode = o.t1.devCode,
                                  memberCode = o.t1.memberCode,
                                  propCode = o.t1.propCode,
                                  bookingCode = o.t1.bookingCode,
                                  unitCode = o.t1.unitCode,
                                  unitNo = o.t1.unitNo,
                                  unitPrice = o.t1.unitPrice,
                                  ppjbDate = o.t1.ppjbDate,
                                  o.t2.projectID,
                                  o.t2.clusterID,
                                  o.t2.unitCodeID,
                                  o.t2.projectName
                              })
                              .Distinct().Join(
                                    from pm in _contextPers.PERSONALS_MEMBER
                                    join p in _contextPers.PERSONAL on pm.psCode equals p.psCode
                                    select new { pm.memberCode, pm.entityCode, pm.scmCode, p.name, p.psCode }
                                    ,
                                  t1 => new { t1.memberCode, t1.entityCode, t1.scmCode },
                                  t2 => new { t2.memberCode, t2.entityCode, t2.scmCode },
                                  (t1, t2) => new { t1, t2 })
                              .Where(o => o.t2.memberCode == o.t1.memberCode && o.t2.entityCode == o.t1.entityCode && o.t2.scmCode == o.t1.scmCode)
                              .Select(o => new GetDataEditDealCloserListDto
                              {
                                  entityCode = o.t1.entityCode,
                                  scmCode = o.t1.scmCode,
                                  devCode = o.t1.devCode,
                                  memberCode = o.t1.memberCode,
                                  propCode = o.t1.propCode,
                                  bookingCode = o.t1.bookingCode,
                                  unitCode = o.t1.unitCode,
                                  unitNo = o.t1.unitNo,
                                  unitPrice = o.t1.unitPrice,
                                  ppjbDate = o.t1.ppjbDate,
                                  name = o.t2.name,
                                  projectName = o.t1.projectName
                              }).Take(1).ElementAtOrDefault(0);

            SendConsole("step2");
            /*
            if (dataDealCloser != null)
            {
                var unitID = (from x in _contextProp.MS_Unit
                              join a in _contextProp.MS_UnitCode on x.unitCodeID equals a.Id
                              where x.unitNo == dataDealCloser.unitCode && a.unitCode == dataDealCloser.unitNo
                              select x.Id).FirstOrDefault();

                var getTrBookingHeader = (from A in _contextProp.TR_BookingHeader
                                          where A.unitID == unitID
                                          select A).FirstOrDefault();

                var getPersonalsMember = (from A in _contextPers.PERSONALS_MEMBER
                                          where A.memberCode == dataDealCloser.memberCode
                                          select A).FirstOrDefault();

                var getPersonal = (from A in _contextPers.PERSONAL
                                   where A.psCode == getPersonalsMember.psCode
                                   select A).FirstOrDefault();

                dataDealCloser.termRemarks = getTrBookingHeader.termRemarks;
                dataDealCloser.name = getPersonal.name;
            }
            */

            #region performance test
            watch.Stop();
            var elapsedMs = watch.ElapsedMilliseconds;
            SendConsole("stop stopwatch: " + elapsedMs + " ms");
            #endregion

            return dataSoldUnit;
        }


        #region debug console
        private void SendConsole(string msg)
        {
            if (Setting_variabel.enable_tcp_debug == true)
            {
                if (Setting_variabel.Komunikasi_TCPListener == null)
                {
                    Setting_variabel.Komunikasi_TCPListener = new Visionet_Backend_NetCore.Komunikasi.Komunikasi_TCPListener(17000);
                    Task.Run(() => StartListenerLokal());
                }

                if (Setting_variabel.Komunikasi_TCPListener != null)
                {
                    if (Setting_variabel.Komunikasi_TCPListener.IsRunning())
                    {
                        if (Setting_variabel.ConsoleBayangan != null)
                        {
                            Setting_variabel.ConsoleBayangan.Send("[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "] " + msg);
                        }
                    }
                }
            }

        }
        #endregion

        #region listener tcp debug
        protected void StartListenerLokal()
        {
            if (Setting_variabel.Komunikasi_TCPListener != null)
                Setting_variabel.Komunikasi_TCPListener.StartListener();
        }

        protected void StopListenerLokal()
        {
            if (Setting_variabel.Komunikasi_TCPListener != null)
                Setting_variabel.Komunikasi_TCPListener.StopListener();
        }
        #endregion
    }
}
