﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.UI;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using VDI.Demo.Authorization;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.NewCommDB;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;
using Visionet_Backend_NetCore.Komunikasi;

namespace VDI.Demo.Commission.MS_Developer_Schemas
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_MasterDeveloperSchema)]
    public class MsDeveloperSchemasAppService : DemoAppServiceBase, IMsDeveloperSchemasAppService
    {
        private readonly IRepository<MS_Developer_Schema,string> _msDeveloperSchemasRepo;
        private readonly IRepository<MS_Schema, string> _msSchemaRepo;
        private readonly IRepository<MS_Property, string> _msPropertyRepo;
        private readonly IRepository<MS_Company> _msCompanyRepo;
        private readonly NewCommDbContext _context;
        private readonly PropertySystemDbContext _contextProp;
        private readonly DemoDbContext _contextDemo;
        private const string _leadingEmptyDevName= "DEV_";

        public MsDeveloperSchemasAppService(
            IRepository<MS_Developer_Schema, string> msDeveloperSchemasRepo,
            IRepository<MS_Schema, string> msSchemaRepo,
            IRepository<MS_Property, string> msPropertyRepo,
            IRepository<MS_Company> msCompanyRepo,
            NewCommDbContext context,
            PropertySystemDbContext contextProp,
            DemoDbContext contextDemo           
            )
        {
            _msDeveloperSchemasRepo = msDeveloperSchemasRepo;
            _msSchemaRepo = msSchemaRepo;
            _msPropertyRepo = msPropertyRepo;
            _msCompanyRepo = msCompanyRepo;
            _context = context;
            _contextProp = contextProp;
            _contextDemo = contextDemo;
        }

        private string GetIdUsername(long? Id)
        {
            string getUsername = (from u in _contextDemo.Users
                              where u.Id == Id
                              select u.UserName)
                       .DefaultIfEmpty("").First();

            return getUsername;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_MasterDeveloperSchema_Create)]
        public JObject CreateMsDeveloperSchemas(CreateMsDeveloperSchemasInputDto input)
        {
            JObject obj = new JObject();
            Logger.InfoFormat("CreateMsDeveloperSchemas() Started.");
            Logger.DebugFormat("CreateMsDeveloperSchemas() - Start checking existing Property. Parameters sent: {0} " +
                "   isComplete = {1}{0}" +
                "   propCode = {2}{0}" +
                "   scmCode = {3}"
                , Environment.NewLine, true, input.propCode, input.scmCode);

            var checkProp = (from x in _msPropertyRepo.GetAll()
                             where x.propCode == input.propCode && x.scmCode == input.scmCode && x.entityCode == input.entityCode
                             select x).Any();

            Logger.DebugFormat("CreateMsDeveloperSchemas() - End checking existing Property. Result = {0}", checkProp);

            if (!checkProp)
            {
                var dataProperty = new MS_Property
                {
                    entityCode = input.entityCode,
                    scmCode = input.scmCode,
                    propCode = input.propCode,
                    propName = input.propName,
                    propDesc = "desc",
                    inputTime = DateTime.Now,
                    modifTime = DateTime.Now,
                    inputUN = GetIdUsername(AbpSession.UserId),
                    modifUN = GetIdUsername(AbpSession.UserId)
                };

                try
                {
                    Logger.DebugFormat("CreateMsDeveloperSchemas() - Start insert property. Parameters sent: {0} " +
                    "   scmCode = {1}{0}" +
                    "   propCode = {2}{0}" +
                    "   propName = {3}{0}" +
                    "   propDesc = {4}{0}" +
                    "   entityCode = {5}"
                    , Environment.NewLine, input.scmCode, input.propCode, input.propName, "desc",  input.entityCode);

                    _msPropertyRepo.Insert(dataProperty);
                    CurrentUnitOfWork.SaveChanges();

                    Logger.DebugFormat("CreateMsDeveloperSchemas() - End insert property. Result = {0}", input.propCode);
                }
                catch (Exception ex)
                {
                    SendConsole("" + ex.Message + " " + ex.StackTrace);
                    Logger.DebugFormat("CreateMsDeveloperSchemas() - ERROR Exception. Result = {0}", ex.Message);
                    throw new UserFriendlyException("Error: " + ex.Message);
                }
            }

            #region old
            /*
            Logger.DebugFormat("CreateMsDeveloperSchemas() - Started Loop Data = {0}", input.setDataDev);
            foreach (var item in input.setDataDev)
            {
                Logger.DebugFormat("CreateMsDeveloperSchemas() - Start checking existing Developer Schema. Parameters sent: {0} " +
                    "   isComplete = {1}{0}" +
                    "   propCode = {2}{0}" +
                    "   scmCode = {3}{0}" +
                    "   devCode = {4}"
                    , Environment.NewLine, true, input.propCode, input.scmCode, item.devCode);

                var checkDev = (from x in _context.MS_Developer_Schema
                                where x.propCode == input.propCode && x.scmCode == input.scmCode && x.devCode == item.devCode && x.entityCode == input.entityCode
                                select x).Any();

                Logger.DebugFormat("CreateMsDeveloperSchemas() - End checking existing Developer Schema. Result = {0}", checkDev);

                SendConsole("AbpSession.UserId:"+ AbpSession.UserId);

                if (!checkDev)
                {
                    var data = new MS_Developer_Schema
                    {
                        entityCode = input.entityCode,
                        scmCode = input.scmCode,
                        propCode = input.propCode,
                        devCode = item.devCode,
                        inputTime = DateTime.Now,
                        modifTime=DateTime.Now,
                        inputUN = GetIdUsername(AbpSession.UserId),
                        modifUN= GetIdUsername(AbpSession.UserId)
                    };

                    SendConsole("data:"+JsonConvert.SerializeObject(data));

                    try
                    {
                        Logger.DebugFormat("CreateMsDeveloperSchemas() - Start insert developerSchema. Parameters sent: {0} " +
                        "   devCode = {1}{0}" +
                        "   devName = {2}{0}" +
                        "   scmCode = {3}{0}" +
                        "   propCode = {4}{0}" +
                        "   bankCode = {5}{0}" +
                        "   isActive = {6}{0}" +
                        "   bankAccountName = {7}{0}" +
                        "   bankBranchName = {8}{0}" +
                        "   isComplete = {9}"
                        , Environment.NewLine, item.devCode, item.devName, input.scmCode, input.propCode,
                            item.bankCode, item.isActive, item.bankAccountName, item.bankBranchName, true);

                        _msDeveloperSchemasRepo.Insert(data);
                        CurrentUnitOfWork.SaveChanges(); //execution saved inside try
                        Logger.DebugFormat("CreateMsDeveloperSchemas() - End insert developerSchema.");
                    }
                    catch (Exception ex)
                    {
                        SendConsole("" + ex.Message + " " + ex.StackTrace);
                        Logger.DebugFormat("CreateMsDeveloperSchemas() - ERROR Exception. Result = {0}", ex.Message);
                        throw new UserFriendlyException("Error: " + ex.Message);
                    }
                }
                else
                {
                    Logger.DebugFormat("CreateMsDeveloperSchemas() - ERROR. Result = {0}", "Already Exist! You could edit the value.");
                    throw new UserFriendlyException("This schema is already exist!");
                }
            }
            */
            #endregion

            #region new - keep this approach
            Logger.DebugFormat("CreateMsDeveloperSchemas() - Started Loop Data = {0}", input.setDataDev);

            if (input.setDataDev == null)
                throw new UserFriendlyException("Developer code is required!");
            else
                if (input.setDataDev.Count == 0)
                throw new UserFriendlyException("Developer code is required!");

            //normalisasi
            foreach (var item in input.setDataDev.ToList())
            {
                if (string.IsNullOrWhiteSpace(item.devCode))
                {
                    input.setDataDev.Remove(item);
                }
            }

            List<MS_Developer_Schema> getDetail = (from dd in input.setDataDev
                                                   where !_context.MS_Developer_Schema.Any(x => x.propCode == input.propCode && x.scmCode == input.scmCode && x.entityCode == input.entityCode && x.devCode == dd.devCode)
                                                   select new MS_Developer_Schema
                                                   {
                                                       entityCode = input.entityCode,
                                                       scmCode = input.scmCode,
                                                       propCode = input.propCode,
                                                       devCode = dd.devCode,
                                                       devName = string.IsNullOrWhiteSpace(dd.devName) ? null : dd.devName,
                                                       inputTime = DateTime.Now,
                                                       modifTime = DateTime.Now,
                                                       inputUN = GetIdUsername(AbpSession.UserId),
                                                       modifUN = GetIdUsername(AbpSession.UserId)
                                                   }).ToList();

            if (getDetail == null)
                throw new UserFriendlyException("This schema is already exist!");
            else
                if (getDetail.Count == 0)
                throw new UserFriendlyException("This schema is already exist!");
            
            using (var dbContextTransaction = _context.Database.BeginTransaction())
            {
                string itemDev = input.setDataDev.Select(i => i.devCode).Aggregate((i, j) => i + ", " + j);
                try
                {
                    Logger.DebugFormat("CreateMsDeveloperSchemas() - Start insert developerSchema");

                    _context.MS_Developer_Schema.AddRange(getDetail);
                    _context.SaveChanges();

                    SendConsole("Commit");
                    dbContextTransaction.Commit();
                    Logger.DebugFormat("CreateMsDeveloperSchemas() - End insert developerSchema.");
                    obj.Add("message", "Created successfully for developer code : " + itemDev);
                }
                catch (Exception ex)
                {
                    SendConsole("Rollback");
                    SendConsole("" + ex.Message + " " + ex.StackTrace);
                    Logger.DebugFormat("CreateMsDeveloperSchemas() - ERROR Exception. Result = {0}", ex.Message);
                    dbContextTransaction.Rollback();
                    obj.Add("message", "Failed to create for developer code : " + itemDev);
                }
            }
            #endregion

            Logger.InfoFormat("CreateMsDeveloperSchemas() - End Loop Data.");
            Logger.InfoFormat("CreateMsDeveloperSchemas() - Finished.");

            return obj;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_MasterDeveloperSchema_Delete)]
        public JObject DeleteMsDeveloperSchemas(DeleteMsDeveloperSchemasDto input)
        {
            if (string.IsNullOrEmpty(input.scmCode))
                throw new UserFriendlyException("Schema code is required!");
            if (string.IsNullOrEmpty(input.entityCode))
                throw new UserFriendlyException("Entity code is required!");

            JObject obj = new JObject();
            Logger.InfoFormat("DeleteMsDeveloperSchemas() Started.");

            Logger.DebugFormat("DeleteMsBobotComm() - Start get data Developer Schemas for update. Parameters sent: {0} " +
                    "   devCode = {1}{0}" +
                    "   entityCode = {2}{0}" +
                    "   propCode = {3}{0}" +
                    "   scmCode = {4}{0}"
                    , Environment.NewLine, input.devCode, input.entityCode, input.propCode, input.scmCode);

            var getDeveloperSchema = (from x in _context.MS_Developer_Schema
                                      where x.devCode == input.devCode && x.scmCode == input.scmCode && x.propCode == input.propCode && x.entityCode == input.entityCode
                                      select x).FirstOrDefault();

            if (getDeveloperSchema == null)
                throw new UserFriendlyException("The schema is not exist!");

            Logger.DebugFormat("DeleteMsDeveloperSchemas() - End get data Developer Schemas  for update. Result = {0}", getDeveloperSchema);

            try
            {
                Logger.DebugFormat("DeleteMsDeveloperSchemas() - Start Delete Developer Schemas. Parameters sent: {0} " +
                "   devCode = {1}{0}" +
                "   entityCode = {2}{0}" +
                "   propCode = {3}{0}" +
                "   scmCode = {4}{0}"
                , Environment.NewLine, input.devCode, input.entityCode, input.propCode, input.scmCode, false);

                _context.MS_Developer_Schema.Remove(getDeveloperSchema);
                _context.SaveChanges();

                Logger.DebugFormat("DeleteMsDeveloperSchemas() - End Delete Developer Schemas.");

                obj.Add("message", "Deleted successfully");
            }
            catch (DataException ex)
            {
                Logger.DebugFormat("DeleteMsDeveloperSchemas() - ERROR DataException. Result = {0}", ex.Message);
                throw new UserFriendlyException("Db Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                Logger.DebugFormat("DeleteMsDeveloperSchemas() - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Error: " + ex.Message);
            }
            Logger.InfoFormat("DeleteMsDeveloperSchemas() - Finished.");
            return obj;
        }
               
        [AbpAuthorize(AppPermissions.Pages_Tenant_MasterDeveloperSchema_Edit)]
        public JObject UpdateMsDeveloperSchemas(UpdateMsDeveloperSchemasInputDto input)
        {
            if (string.IsNullOrEmpty(input.scmCode))
                throw new UserFriendlyException("Schema code is required!");
            if (string.IsNullOrEmpty(input.entityCode))
                throw new UserFriendlyException("Entity code is required!");

            JObject obj = new JObject();
            Logger.InfoFormat("UpdateMsDeveloperSchemas() Started.");

            Logger.DebugFormat("UpdateMsDeveloperSchemas() - Start checking existing Code. Parameters sent: {0} " +
                "   devCode = {1}{0}" +
                "   entityCode = {2}{0}" +
                "   isComplete = {3}{0}" +
                "   scmCode = {4}{0}" +
                "   propCode = {5}{0}"
                , Environment.NewLine, input.devCode, input.entityCode, true, input.scmCode, input.propCode);

            Logger.DebugFormat("UpdateMsDeveloperSchemas() - Start get developerSchema for update. Parameters sent: {0} " +
                 "   entityCode = {1}{0}", Environment.NewLine, input.entityCode);

            var getDeveloperSchemas = (from x in _context.MS_Developer_Schema
                                       where x.devCode == input.devCode && x.scmCode == input.scmCode && x.propCode == input.propCode && x.entityCode == input.entityCode
                                       select x).FirstOrDefault();
            
            if (getDeveloperSchemas == null)
                throw new UserFriendlyException("The schema is not exist!");

            var updateDeveloperSchemas = getDeveloperSchemas.MapTo<MS_Developer_Schema>();
            updateDeveloperSchemas.devName = input.devName;

            Logger.DebugFormat("UpdateMsDeveloperSchemas() - End get developerSchema for update. Result = {0}", getDeveloperSchemas);

            try
            {
                Logger.DebugFormat("UpdateMsDeveloperSchemas() - Start update developerSchema. Parameters sent: {0} " +
            "   devCode = {1}{0}" +
            "   devName = {2}{0}" +
            "   bankCode = {3}{0}" +
            "   bankAccountName = {4}{0}" +
            "   bankBranchName = {5}{0}" +
            "   isActive = {6}{0}"
            , Environment.NewLine, input.devCode, input.devName, input.bankCode, input.bankAccountName, input.bankBranchName, input.isActive);

                _msDeveloperSchemasRepo.Update(updateDeveloperSchemas);
                CurrentUnitOfWork.SaveChanges();
                Logger.DebugFormat("UpdateMsDeveloperSchemas() - End update developerSchema.");

                obj.Add("message", "Updated successfully");
            }
            catch (Exception ex)
            {
                Logger.DebugFormat("UpdateMsDeveloperSchemas() - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Error: " + ex.Message);
            }

            Logger.InfoFormat("UpdateMsDeveloperSchemas() - Finished.");
            return obj;
        }

        public ListResultDto<GetDeveloperSchemasListDto> GetMsDeveloperSchemas()
        {
            var listResult = (from DeveloperSchemas in _context.MS_Developer_Schema
                              join schema in _msSchemaRepo.GetAll() on DeveloperSchemas.scmCode equals schema.scmCode
                              join property in _msPropertyRepo.GetAll() on DeveloperSchemas.propCode equals property.propCode
                              orderby DeveloperSchemas.inputTime descending
                              select new GetDeveloperSchemasListDto
                              {
                                  entityCode = DeveloperSchemas.entityCode,
                                  scmCode = DeveloperSchemas.scmCode,
                                  propCode = property.propCode,
                                  devCode = DeveloperSchemas.devCode,
                                  devName = DeveloperSchemas.devName == null ? _leadingEmptyDevName + DeveloperSchemas.devCode : DeveloperSchemas.devName,
                                  schemaName = schema.scmName,
                                  propName = property.propName
                              }).ToList();

            return new ListResultDto<GetDeveloperSchemasListDto>(listResult);
        }

        public ListResultDto<GetDeveloperSchemasListDto> GetMsDeveloperSchemasBySchema(string scmCode)
        {
            if (string.IsNullOrEmpty(scmCode))
                throw new UserFriendlyException("Schema code is required!");

            var listResult = (from x in _context.MS_Developer_Schema
                              where x.scmCode == scmCode
                              join schema in _context.MS_Schema on x.scmCode equals schema.scmCode
                              join property in _context.MS_Property on x.propCode equals property.propCode
                              orderby x.scmCode descending
                              select new GetDeveloperSchemasListDto
                              {
                                  entityCode=x.entityCode,
                                  scmCode=x.scmCode,
                                  propCode = property.propCode,
                                  devCode = x.devCode,
                                  devName = x.devName == null ? _leadingEmptyDevName + x.devCode : x.devName,
                                  schemaName = schema.scmName,
                                  propName = property.propName
                              }).ToList();

            return new ListResultDto<GetDeveloperSchemasListDto>(listResult);
        }

        public GetDeveloperSchemasListDto GetDetailMsDeveloperSchemas(GetDetailMsDeveloperSchemasDto input)
        {
            if (string.IsNullOrEmpty(input.scmCode))
                throw new UserFriendlyException("Schema code is required!");
            if (string.IsNullOrEmpty(input.entityCode))
                throw new UserFriendlyException("Entity code is required!");
            if (string.IsNullOrEmpty(input.propCode))
                throw new UserFriendlyException("Property code is required!");
            if (string.IsNullOrEmpty(input.devCode))
                throw new UserFriendlyException("Developer code is required!");

            var listResult = (from x in _context.MS_Developer_Schema
                              join schema in _context.MS_Schema on x.scmCode equals schema.scmCode
                              join property in _context.MS_Property on x.propCode equals property.propCode
                              orderby x.Id descending
                              where x.devCode == input.devCode && x.scmCode == input.scmCode && x.propCode == input.propCode && x.entityCode == input.entityCode
                              select new GetDeveloperSchemasListDto
                              {
                                  entityCode = x.entityCode,
                                  scmCode = x.scmCode,
                                  propCode = property.propCode,
                                  devCode = x.devCode,
                                  devName = x.devName == null ? _leadingEmptyDevName + x.devCode : x.devName,
                                  schemaName = schema.scmName,
                                  propName = property.propName
                              }).FirstOrDefault();
            return listResult;
        }

        public ListResultDto<GetPropCodeListDto> GetPropCodeBySchemaCode(string scmCode)
        {
            if (string.IsNullOrEmpty(scmCode))
                throw new UserFriendlyException("Schema code is required!");

            var listResult = (from property in _context.MS_Property
                              where property.scmCode == scmCode
                              orderby property.Id descending
                              select new GetPropCodeListDto
                              {
                                  propCode = property.propCode,
                                  propName = property.propName
                              }).ToList();

            return new ListResultDto<GetPropCodeListDto>(listResult);
        }

        public string GetPropNameByPropCode(string propCode)
        {
            if (string.IsNullOrEmpty(propCode))
                throw new UserFriendlyException("Property code is required!");

            var propName = (from property in _context.MS_Property
                            where property.propCode == propCode
                            select property.propName).FirstOrDefault();
            return propName;
        }

        public ListResultDto<GetDeveloperSchemasListDto> GetAllMsDeveloperSchemaPaging()
        {
            var listResult = (from DeveloperSchemas in _context.MS_Developer_Schema
                              join schema in _msSchemaRepo.GetAll() on DeveloperSchemas.scmCode equals schema.scmCode
                              join property in _msPropertyRepo.GetAll() on DeveloperSchemas.propCode equals property.propCode
                              orderby DeveloperSchemas.inputTime descending
                              select new GetDeveloperSchemasListDto
                              {
                                  entityCode = DeveloperSchemas.entityCode,
                                  propCode = property.propCode,
                                  devCode = DeveloperSchemas.devCode,
                                  devName = DeveloperSchemas.devName == null ? _leadingEmptyDevName + DeveloperSchemas.devCode : DeveloperSchemas.devName,
                                  scmCode = DeveloperSchemas.scmCode,
                                  schemaName = schema.scmName,
                                  propName = property.propName
                              }).ToList();
            return new ListResultDto<GetDeveloperSchemasListDto>(listResult);
        }

        public ListResultDto<GetDropDownDeveloperSchemasListDto> GetDropDownMsDeveloperSchemasBySchema(string scmCode)
        {
            if (string.IsNullOrEmpty(scmCode))
                throw new UserFriendlyException("Schema code is required!");

            var listResult = (from x in _context.MS_Developer_Schema
                              where x.scmCode == scmCode
                              orderby x.devCode descending
                              select new GetDropDownDeveloperSchemasListDto
                              {
                                  devCode = x.devCode,
                                  devName = x.devName == null ? _leadingEmptyDevName + x.devCode : x.devName,
                              }).ToList();

            return new ListResultDto<GetDropDownDeveloperSchemasListDto>(listResult);
        }

        public List<GetDropDownDeveloperSchemasListDto> GetDataDeveloperSchemaByProperty(string propCode)
        {
            if (string.IsNullOrEmpty(propCode))
                throw new UserFriendlyException("Property code is required!");

            var getDev = (from dev in _msDeveloperSchemasRepo.GetAll()
                          where dev.propCode == propCode
                          select new GetDropDownDeveloperSchemasListDto
                          {
                              devCode = dev.devCode,
                              devName = dev.devName == null ? _leadingEmptyDevName + dev.devCode : dev.devName,
                          }).ToList();

            return getDev;
        }

        #region debug console
        private void SendConsole(string msg)
        {
            if (Setting_variabel.enable_tcp_debug == true)
            {
                if (Setting_variabel.Komunikasi_TCPListener == null)
                {
                    Setting_variabel.Komunikasi_TCPListener = new Visionet_Backend_NetCore.Komunikasi.Komunikasi_TCPListener(17000);
                    Task.Run(() => StartListenerLokal());
                }

                if (Setting_variabel.Komunikasi_TCPListener != null)
                {
                    if (Setting_variabel.Komunikasi_TCPListener.IsRunning())
                    {
                        if (Setting_variabel.ConsoleBayangan != null)
                        {
                            Setting_variabel.ConsoleBayangan.Send("[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "] " + msg);
                        }
                    }
                }
            }

        }
        #endregion

        #region listener tcp debug
        protected void StartListenerLokal()
        {
            if (Setting_variabel.Komunikasi_TCPListener != null)
                Setting_variabel.Komunikasi_TCPListener.StartListener();
        }

        protected void StopListenerLokal()
        {
            if (Setting_variabel.Komunikasi_TCPListener != null)
                Setting_variabel.Komunikasi_TCPListener.StopListener();
        }
        #endregion

    }
}
