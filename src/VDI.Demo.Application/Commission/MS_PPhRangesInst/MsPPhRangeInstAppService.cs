﻿using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.UI;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using System.Text;
using VDI.Demo.Authorization;
using VDI.Demo.Commission.MS_PPhRangesInst.Dto;
using VDI.Demo.NewCommDB;
using Abp.AutoMapper;
using Abp.Application.Services.Dto;
using VDI.Demo.EntityFrameworkCore;
using Newtonsoft.Json.Linq;

namespace VDI.Demo.Commission.MS_PPhRangesInst
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_MasterPPHRangeInst)]
    public class MsPPhRangeInstAppService : DemoAppServiceBase, IMsPPhRangeInstAppService
    {
        private readonly IRepository<MS_PPhRangeIns, string> _msPPhRangesInstRepo;
        private readonly DemoDbContext _contextDemo;
        private readonly NewCommDbContext _contextCom;

        public MsPPhRangeInstAppService(
            IRepository<MS_PPhRangeIns,
            string> msPPhRangeInstRepo,
            DemoDbContext contextDemo,
            NewCommDbContext contextCom
        )
        {
            _msPPhRangesInstRepo = msPPhRangeInstRepo;
            _contextDemo = contextDemo;
            _contextCom = contextCom;
        }

        private string GetIdUsername(long? Id)
        {
            string getUsername = (from u in _contextDemo.Users
                                  where u.Id == Id
                                  select u.UserName)
                       .DefaultIfEmpty("").First();

            return getUsername;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_MasterPPHRangeInst_Create)]
        public JObject CreateMsPPhRangeInst(List<CreateOrUpdatePPhRangeInstListDto> input)
        {
            JObject obj = new JObject();
            Logger.InfoFormat("CreateMsPPhRangeInst() Started.");

            Logger.DebugFormat("CreateMsPPhRangeInst() - Started Loop Data = {0}", input);
            foreach (var item in input)
            {
                var createPPhRangeInst = new MS_PPhRangeIns
                {
                    scmCode = item.scmCode,
                    pphRangePct = item.pphRangePct,
                    validDate = item.validDate,
                    TAX_CODE = item.taxCode,
                    entityCode = item.entityCode,
                    inputTime = DateTime.Now,
                    modifTime = DateTime.Now,
                    inputUN = GetIdUsername(AbpSession.UserId),
                    modifUN = GetIdUsername(AbpSession.UserId)
                };

                try
                {
                    Logger.DebugFormat("CreateMsPPhRangeInst() - Start insert PPhRangeInst. Parameters sent: {0} " +
                    "   scmCode = {1}{0}" +
                    "   pphRangePct = {2}{0}" +
                    "   validDate = {3}{0}" +
                    "   TAX_CODE = {4}{0}" +
                    "   entityCode = {5}{0}"
                    , Environment.NewLine, item.scmCode, item.pphRangePct, item.validDate, item.taxCode, item.entityCode);

                    _msPPhRangesInstRepo.Insert(createPPhRangeInst);
                    CurrentUnitOfWork.SaveChanges();
                    Logger.DebugFormat("CreateMsPPhRangeInst() - End insert PPhRangeInst.");

                    obj.Add("message", "Created successfully");
                }
                catch (Exception ex)
                {
                    Logger.DebugFormat("CreateMsPPhRangeInst() - ERROR Exception. Result = {0}", ex.Message);
                    throw new UserFriendlyException("Error: " + ex.Message);
                }
            }
            Logger.DebugFormat("CreateMsPPhRangeInst() - End Loop Data.");
            Logger.InfoFormat("CreateMsPPhRangeInst() - Finished.");
            return obj;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_MasterPPHRangeInst_Delete)]
        public JObject DeleteMsPPhRange(DeleteMsPPhRangeDto input)
        {
            if (string.IsNullOrEmpty(input.scmCode))
                throw new UserFriendlyException("Schema code is required!");
            if (string.IsNullOrEmpty(input.entityCode))
                throw new UserFriendlyException("Entity code is required!");

            JObject obj = new JObject();
            Logger.InfoFormat("DeleteMsPPhRangeInst() Started.");

            Logger.DebugFormat("DeleteMsPPhRangeInst() - Start get data PPhRangeInst for update. Parameters sent: {0} " +
                    "   pphRangePct = {1}{0}" +
                    "   scmCode = {2}{0}" +
                    "   entityCode = {3}{0}"
                    , Environment.NewLine, input.pphRangePct, input.scmCode, input.entityCode);

            var getPPhRangeInst = (from p in _contextCom.MS_PPHRangeIns
                                   where p.entityCode == input.entityCode && p.scmCode == input.scmCode && p.pphRangePct == input.pphRangePct
                                   select p).FirstOrDefault();

            Logger.DebugFormat("DeleteMsPPhRangeInst() - End get data PPhRangeInst.");

            if (getPPhRangeInst == null)
                throw new UserFriendlyException("PPH rangeInst is not exist!");
            try
            {
                Logger.DebugFormat("DeleteMsPPhRangeInst() - Start delete PPhRangeInst. Parameters sent: {0} " +
                "   pphRangePct = {1}{0}" +
                "   scmCode = {2}{0}"
                , Environment.NewLine, input.pphRangePct, input.scmCode);

                _contextCom.MS_PPHRangeIns.Remove(getPPhRangeInst);
                _contextCom.SaveChanges();

                Logger.DebugFormat("DeleteMsPPhRangeInst() - End delete PPhRangeInst.");

                obj.Add("message", "Deleted successfully");
            }
            catch (DataException ex)
            {
                Logger.DebugFormat("DeleteMsPPhRangeInst() - ERROR DataException. Result = {0}", ex.Message);
                throw new UserFriendlyException("Db Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                Logger.DebugFormat("DeleteMsPPhRangeInst() - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Error: " + ex.Message);
            }
            Logger.InfoFormat("DeleteMsPPhRangeInst() - Finished.");
            return obj;
        }

        public JObject UpdateMsPPhRangeInst(CreateOrUpdatePPhRangeInstListDto input)
        {
            if (string.IsNullOrEmpty(input.scmCode))
                throw new UserFriendlyException("Schema code is required!");
            if (string.IsNullOrEmpty(input.entityCode))
                throw new UserFriendlyException("Entity code is required!");

            JObject obj = new JObject();
            Logger.InfoFormat("UpdateMsPPhRangeInst() - Started.");

            Logger.DebugFormat("UpdateMsPPhRangeInst() - Start get data PPhRangeInst for update. Parameters sent: {0} " +
                    "   pphRangePct = {1}{0}"+
                    "   scmCode = {1}{0}"
                    , Environment.NewLine, input.pphRangePct, input.scmCode);

            var getPPhRangeInst = (from p in _contextCom.MS_PPHRangeIns
                                   where p.entityCode == input.entityCode && p.scmCode == input.scmCode && p.pphRangePct == input.pphRangePct
                                   select p).FirstOrDefault();

            if (getPPhRangeInst == null)
                throw new UserFriendlyException("PPH rangeInst is not exist!");

            var updatePPhRangeInst = getPPhRangeInst.MapTo<MS_PPhRangeIns>();
            Logger.DebugFormat("UpdateMsPPhRangeInst() - End get data PPhRangeInst  for update. Result = {0}", updatePPhRangeInst);
            
            updatePPhRangeInst.TAX_CODE = input.taxCode;

            try
            {
                Logger.DebugFormat("UpdateMsPPhRangeInst() - Start update pphRangeInst. Parameters sent: {0} " +
            "   scmCode = {1}{0}" +
            "   pphRangePct = {2}{0}" +
            "   TAX_CODE = {3}{0}" 
            , Environment.NewLine, input.scmCode, input.pphRangePct, input.taxCode);

                _msPPhRangesInstRepo.Update(updatePPhRangeInst);
                CurrentUnitOfWork.SaveChanges();

                Logger.DebugFormat("UpdateMsPPhRangeInst() - End update PPhRangeInst.");

                obj.Add("message", "Updated successfully");
            }
            catch (Exception ex)
            {
                Logger.DebugFormat("UpdateMsPPhRangeInst() - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Error: " + ex.Message);
            }

            Logger.InfoFormat("UpdateMsPPhRangeInst() - Finished.");
            return obj;
        }

        public ListResultDto<CreateOrUpdatePPhRangeInstListDto> GetMsPPhRangeInstBySchemaCode(string scmCode)
        {
            if (string.IsNullOrEmpty(scmCode))
                throw new UserFriendlyException("Schema code is required!");

            var listResult = (from p in _contextCom.MS_PPHRangeIns
                              where p.scmCode == scmCode
                              orderby p.Id descending
                              select new CreateOrUpdatePPhRangeInstListDto
                              {
                                  entityCode = p.entityCode,
                                  scmCode = p.scmCode,
                                  pphRangePct = p.pphRangePct,
                                  validDate = p.validDate,
                                  taxCode = p.TAX_CODE
                              }).ToList();
            return new ListResultDto<CreateOrUpdatePPhRangeInstListDto>(listResult);
        }
    }
}
