﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using System;
using System.Collections.Generic;
using System.Linq;
using VDI.Demo.Authorization;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.Files;
using VDI.Demo.OnlineBooking.Diagramatic.Dto;
using VDI.Demo.OnlineBooking.Transaction;
using VDI.Demo.OnlineBooking.Transaction.Dto;
using VDI.Demo.PersonalsDB;
using VDI.Demo.PropertySystemDB.LippoMaster;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;
using VDI.Demo.PropertySystemDB.OnlineBooking.PPOnline;
using VDI.Demo.PropertySystemDB.OnlineBooking.PropertySystem;
using VDI.Demo.PropertySystemDB.Pricing;

namespace VDI.Demo.OnlineBooking.Diagramatic
{
    //[AbpAuthorize(AppPermissions.Pages_Tenant_OnlineBooking_Diagramatic)]
    public class DiagramaticAppService : DemoAppServiceBase, IDiagramaticAppService
    {
        private readonly IRepository<MS_Unit> _msUnit;
        private readonly IRepository<MS_UnitItem> _msUnitItem;
        private readonly IRepository<MS_UnitItemPrice> _msUnitItemPrice;
        private readonly IRepository<MS_Detail> _msDetail;
        private readonly IRepository<MS_UnitCode> _msUnitCode;
        private readonly IRepository<MS_UnitRoom> _msUnitRoomRepo;
        private readonly IRepository<MS_Zoning> _msZoningRepo;
        private readonly IRepository<MS_Renovation> _msRenovation;
        private readonly IRepository<MS_Project> _msProject;
        private readonly IRepository<TR_UnitOrderHeader> _trUnitOrderHeader;
        private readonly IRepository<TR_UnitOrderDetail> _trUnitOrderDetail;
        private readonly IRepository<MS_TermMain> _msTermMainRepo;
        private readonly IRepository<MS_Term> _msTermRepo;
        private readonly IRepository<MS_TermAddDisc> _msTermAddDiscRepo;
        private readonly IRepository<MS_Cluster> _msCluster;
        private readonly IRepository<LK_Item> _lkItem;
        private readonly IRepository<PERSONALS, string> _personalsRepo;
        private readonly IRepository<PERSONALS_MEMBER, string> _personalMemberRepo;
        private readonly IRepository<TR_UnitReserved> _trUnitReserved;
        private readonly IRepository<LK_UnitStatus> _lkUnitStatus;
        private readonly IRepository<MS_SumberDana> _msSumberDanaRepo;
        private readonly IRepository<MS_TujuanTransaksi> _msTujuanTransaksiRepo;
        private readonly PropertySystemDbContext _contextProp;
        private readonly IRepository<LK_PaymentType> _lkPaymentType;
        private readonly IRepository<MS_Category> _msCategory;
        private readonly ITransactionAppService _transactionAppService;
        private readonly FilesHelper _fileHelper;

        public DiagramaticAppService(
            IRepository<MS_Unit> msUnit,
            IRepository<MS_UnitItem> msUnitItem,
            IRepository<MS_UnitItemPrice> msUnitItemPrice,
            IRepository<MS_Detail> msDetail,
            IRepository<MS_UnitCode> msUnitCode,
            IRepository<MS_Renovation> msRenovation,
            IRepository<TR_UnitOrderHeader> trUnitOrderHeader,
            IRepository<MS_TermMain> msTermMainRepo,
            IRepository<MS_Term> msTermRepo,
            IRepository<TR_UnitOrderDetail> trUnitOrderDetail,
            IRepository<MS_Cluster> msCluster,
            IRepository<LK_Item> lkItem,
            IRepository<MS_TermAddDisc> msTermAddDisc,
            IRepository<PERSONALS, string> personalsRepo,
            IRepository<PERSONALS_MEMBER, string> personalsMemberRepo,
            IRepository<TR_UnitReserved> trUnitReserved,
            IRepository<LK_UnitStatus> lkUnitStatus,
            IRepository<MS_Project> msProject,
            IRepository<MS_UnitRoom> msUnitRoomRepo,
            IRepository<MS_Zoning> msZoningRepo,
            IRepository<MS_SumberDana> msSumberDanaRepo,
            IRepository<MS_TujuanTransaksi> msTujuanTransaksiRepo,
            PropertySystemDbContext contextProp,
            IRepository<LK_PaymentType> lkPaymentType,
            IRepository<MS_Category> msCategory,
            ITransactionAppService transactionAppService,
            FilesHelper filesHelper
        )
        {
            _msUnit = msUnit;
            _msUnitItem = msUnitItem;
            _msUnitItemPrice = msUnitItemPrice;
            _msDetail = msDetail;
            _msUnitCode = msUnitCode;
            _trUnitOrderHeader = trUnitOrderHeader;
            _msTermMainRepo = msTermMainRepo;
            _msTermRepo = msTermRepo;
            _trUnitOrderDetail = trUnitOrderDetail;
            _msCluster = msCluster;
            _lkItem = lkItem;
            _msTermAddDiscRepo = msTermAddDisc;
            _personalsRepo = personalsRepo;
            _personalMemberRepo = personalsMemberRepo;
            _trUnitReserved = trUnitReserved;
            _msRenovation = msRenovation;
            _msProject = msProject;
            _msUnitRoomRepo = msUnitRoomRepo;
            _msZoningRepo = msZoningRepo;
            _lkUnitStatus = lkUnitStatus;
            _msSumberDanaRepo = msSumberDanaRepo;
            _msTujuanTransaksiRepo = msTujuanTransaksiRepo;
            _contextProp = contextProp;
            _lkPaymentType = lkPaymentType;
            _transactionAppService = transactionAppService;
            _msCategory = msCategory;
            _fileHelper = filesHelper;
        }

        //[AbpAuthorize(AppPermissions.Pages_Tenant_OnlineBooking_Diagramatic_GetDetailUnitByProject)]
        //public DetailUnitByProjectResultDto GetDetailUnitByProject(int unitID, int projectID)
        //{
        //    var getUnitById = (from unit in _msUnit.GetAll()
        //                       join unitItem in _msUnitItem.GetAll()
        //                       on unit.Id equals unitItem.unitID
        //                       join detail in _msDetail.GetAll()
        //                       on unit.detailID equals detail.Id
        //                       join unitCode in _msUnitCode.GetAll()
        //                       on unit.unitCodeID equals unitCode.Id
        //                       join unitPrice in _msUnitItemPrice.GetAll()
        //                       on unit.Id equals unitPrice.unitID
        //                       join term in _msTermRepo.GetAll()
        //                       on unitPrice.termID equals term.Id
        //                       where unit.Id == unitID && unit.projectID == projectID && unitItem.itemID == 2 && term.termNo == 02
        //                       group unitPrice by new
        //                       {
        //                           unitCode.unitName,
        //                           unitItem.area,
        //                           detail.detailName
        //                       } into G
        //                       select new DetailUnitByProjectResultDto
        //                       {
        //                           floor = G.Key.unitName,
        //                           area = G.Key.area,
        //                           detailName = G.Key.detailName,
        //                           sellingPrice = G.Sum(s => s.grossPrice)
        //                       }).FirstOrDefault();

        //    return getUnitById;
        //}

        //[AbpAuthorize(AppPermissions.Pages_Tenant_OnlineBooking_Diagramatic_GetUnitDetailWithTower)]
        public UnitDetailResultDto GetUnitDetailWithTower(int unitID)
        {
            var getRenov = (from a in _msUnitItemPrice.GetAll()
                            join b in _msUnitItem.GetAll() on a.unitItemID equals b.Id
                            join c in _msRenovation.GetAll() on a.renovID equals c.Id
                            where b.unitID == unitID
                            select new ListRenovResultDto
                            {
                                renovID = c.Id,
                                renovName = c.renovationName
                            }).Distinct().ToList();

            var getDetail = (from a in _msTermRepo.GetAll()
                             join b in _msTermMainRepo.GetAll()
                             on a.termMainID equals b.Id
                             join c in _msUnit.GetAll()
                             on a.projectID equals c.projectID
                             join e in _msUnitCode.GetAll()
                             on c.unitCodeID equals e.Id
                             where c.Id == unitID
                             select new UnitDetailResultDto
                             {
                                 unitID = c.Id,
                                 unitCode = e.unitCode,
                                 unitNo = c.unitNo,
                                 bookingFee = b.BFAmount
                             }).FirstOrDefault();

            getDetail = new UnitDetailResultDto
            {
                unitCode = getDetail.unitCode,
                unitNo = getDetail.unitNo,
                bookingFee = getDetail.bookingFee,
                ListRenov = getRenov
            };

            return getDetail;
        }

        //[AbpAuthorize(AppPermissions.Pages_Tenant_OnlineBooking_Diagramatic_ListPrice)]
        public ListResultDto<ListPriceResultDto> ListPrice(int unitID, int renovId)
        {
            var getTerm = (from a in _msUnitItemPrice.GetAll()
                           join b in _msUnitItem.GetAll() on a.unitItemID equals b.Id
                           join c in _msTermRepo.GetAll() on a.termID equals c.Id
                           join d in _msUnit.GetAll() on b.unitID equals d.Id
                           join e in _msTermMainRepo.GetAll() on d.termMainID equals e.Id
                           where d.Id == unitID && a.renovID == renovId
                           select new ListPriceResultDto
                           {
                               unitID = d.Id,
                               termID = a.termID,
                               termName = c.remarks,
                               bookingFee = e.BFAmount
                           }).Distinct().ToList();

            foreach (var item in getTerm)
            {
                var getGrossPrice = (from unitItemPrice in _msUnitItemPrice.GetAll()
                                     join unitItem in _msUnitItem.GetAll() on unitItemPrice.unitItemID equals unitItem.Id
                                     join unit in _msUnit.GetAll()
                                     on unitItem.unitID equals unit.Id
                                     where unit.Id == unitID
                                     && unitItemPrice.renovID == renovId
                                     && unitItemPrice.termID == item.termID
                                     group unitItemPrice by new
                                     {
                                         unitItem.pctDisc,
                                         unitItem.pctTax
                                     } into G
                                     select new ListPrice
                                     {
                                         grossprice = G.Sum(a => a.grossPrice),
                                         pctTax = G.Key.pctTax,
                                         pctDisc = G.Key.pctDisc
                                     }).FirstOrDefault();

                getGrossPrice.grossprice = getGrossPrice.grossprice * (1 - (decimal)getGrossPrice.pctDisc);

                var getDisc = (from a in _msTermAddDiscRepo.GetAll()
                               where a.termID == item.termID
                               select new
                               {
                                   a.addDiscPct
                               }).ToList();

                foreach (var dto in getDisc)
                {
                    getGrossPrice.grossprice = getGrossPrice.grossprice * (1 - (decimal)dto.addDiscPct);
                }

                getGrossPrice.grossprice = getGrossPrice.grossprice * (1 + (decimal)getGrossPrice.pctTax);

                item.grossPrice = getGrossPrice.grossprice;
            }
            
            return new ListResultDto<ListPriceResultDto>(getTerm);
        }
        
        //[AbpAuthorize(AppPermissions.Pages_Tenant_OnlineBooking_Diagramatic_GetListUnitByUnitCode)]
        public ListResultDto<ListUnitByUnitCodeResultDto> GetListUnitByUnitCode(int unitCodeId, int projectId)
        {
            var getUnit = (from unit in _msUnit.GetAll()
                           where unit.unitCodeID == unitCodeId && new[] { 1, 8, 12 }.Contains(unit.unitStatusID)  //A, S, Z
                           orderby unit.unitNo ascending
                           select new ListUnitByUnitCodeResultDto
                           {
                               unit = unit.unitNo,
                               unitStatus = unit.unitStatusID
                           }).ToList();

            return new ListResultDto<ListUnitByUnitCodeResultDto>(getUnit);
        }
        
        //[AbpAuthorize(AppPermissions.Pages_Tenant_OnlineBooking_Diagramatic_GetListTowerByProjectID)]
        public ListResultDto<ListTowerResultDto> GetListTowerByProjectID(int projectId)
        {
            var listTower = (from unit in _msUnit.GetAll()
                             join cluster in _msCluster.GetAll()
                             on unit.clusterID equals cluster.Id
                             where unit.projectID == projectId
                             orderby unit.clusterID ascending
                             select new ListTowerResultDto
                             {
                                 clusterID = cluster.Id,
                                 clusterCode = cluster.clusterCode,
                                 clusterName = cluster.clusterName
                             }).Distinct().ToList();

            if (listTower == null)
            {
                var error = new List<ListTowerResultDto>();

                var message = new ListTowerResultDto
                {
                    message = "data not found"
                };

                error.Add(message);

                return new ListResultDto<ListTowerResultDto>(error);
            }
            else
            {
                return new ListResultDto<ListTowerResultDto>(listTower);
            }
        }

        [UnitOfWork(isTransactional: false)]
        //[AbpAuthorize(AppPermissions.Pages_Tenant_OnlineBooking_Diagramatic_GetListDiagramatic)]
        public List<GetDiagramaticResultDto> GetListDiagramatic(DiagramaticMobileInputDto input)
        {
            var getUnit = (from a in _contextProp.MS_Unit
                           join b in _contextProp.MS_UnitItem on a.Id equals b.unitID
                           join c in _contextProp.MS_UnitItemPrice on b.Id equals c.unitItemID
                           join d in _contextProp.LK_UnitStatus on a.unitStatusID equals d.Id
                           join e in _contextProp.MS_UnitCode on a.unitCodeID equals e.Id
                           join f in _contextProp.MS_Category on a.categoryID equals f.Id
                           where a.projectID == input.projectId && a.clusterID == input.clusterId
                           && (d.unitStatusCode == "A")
                           select new GetDiagramaticResultDto
                           {
                               unitCodeId = a.unitCodeID,
                               floor = e.floor,
                               unitCode = e.unitCode,
                               diagramaticType = f.diagramaticType,
                               unitName = e.unitName
                           }).Distinct().ToList();

            if (getUnit.Count == 0)
            {
                var error = new List<GetDiagramaticResultDto>();

                var message = new GetDiagramaticResultDto
                {
                    message = "projectId/clusterId Tidak Valid"
                };

                error.Add(message);

                //throw new UserFriendlyException("projectId/clusterId Tidak Valid");

                return new List<GetDiagramaticResultDto>(error);
            }

            foreach (var item in getUnit)
            {
                if(item.diagramaticType.ToLowerInvariant() == "highrise")
                {
                    var getItem1 = (from a in _lkItem.GetAll()
                                    where a.itemCode == "01"
                                    select a.Id).FirstOrDefault();

                    var getItem2 = (from a in _lkItem.GetAll()
                                    where a.itemCode == "02"
                                    select a.Id).FirstOrDefault();


                    var getUnits = (from a in _contextProp.MS_Unit
                                    join b in _contextProp.MS_UnitItem on a.Id equals b.unitID
                                    join c in _contextProp.MS_UnitItemPrice on b.Id equals c.unitItemID
                                    join d in _contextProp.LK_UnitStatus on a.unitStatusID equals d.Id
                                    join e in _contextProp.MS_UnitRoom on b.Id equals e.unitItemID into unitRoom
                                    from e in unitRoom.DefaultIfEmpty()
                                    join f in _contextProp.MS_Zoning on a.zoningID equals f.Id
                                    join g in _contextProp.MS_Detail on a.detailID equals g.Id
                                    join h in _contextProp.MS_UnitCode on a.unitCodeID equals h.Id
                                    where a.projectID == input.projectId && a.clusterID == input.clusterId
                                    && a.unitCodeID == item.unitCodeId
                                    && (d.unitStatusCode == "A")
                                    && !new[] { getItem1, getItem2 }.Contains(b.itemID)
                                    orderby a.Id ascending
                                    select new UnitMobileDto
                                    {
                                        floorType = h.floor + a.unitNo,
                                        unitID = a.Id,
                                        unitNo = a.unitNo,
                                        unitStatusCode = d.unitStatusCode,
                                        unitStatusName = d.unitStatusName,
                                        bedroom = e.bedroom,
                                        zoningName = f.zoningName,
                                        detailID = a.detailID,
                                        detailCode = g.detailCode,
                                        detailImage = _fileHelper.getAbsoluteUri() + g.detailImage,
                                        zoningID = f.Id
                                    }).Distinct().ToList();

                    if (input.bedroom != null)
                    {
                        getUnits = (from a in getUnits
                                    where a.bedroom == input.bedroom
                                    select a).Distinct().ToList();
                    }

                    if (input.unitType != null)
                    {
                        getUnits = (from a in getUnits
                                    where a.unitNo.Contains(input.unitType)
                                    select a).Distinct().ToList();
                    }
                    if (input.zoningID != 0)
                    {
                        getUnits = (from a in getUnits
                                    where a.zoningID == input.zoningID
                                    select a).Distinct().ToList();
                    }

                    item.units = getUnits;
                }
                else if(item.diagramaticType.ToLowerInvariant() == "landed" || item.diagramaticType.ToLowerInvariant() == "kavling")
                {
                    var getItem1 = (from a in _lkItem.GetAll()
                                    where a.itemCode == "01"
                                    select a.Id).FirstOrDefault();

                    var getItem2 = (from a in _lkItem.GetAll()
                                    where a.itemCode == "02"
                                    select a.Id).FirstOrDefault();

                    item.floor = item.unitName + " " + item.floor;


                    var getUnits = (from a in _contextProp.MS_Unit
                                    join b in _contextProp.MS_UnitItem on a.Id equals b.unitID
                                    join c in _contextProp.MS_UnitItemPrice on b.Id equals c.unitItemID
                                    join d in _contextProp.LK_UnitStatus on a.unitStatusID equals d.Id
                                    join e in _contextProp.MS_UnitRoom on b.Id equals e.unitItemID into unitRoom
                                    from e in unitRoom.DefaultIfEmpty()
                                    join f in _contextProp.MS_Zoning on a.zoningID equals f.Id
                                    join g in _contextProp.MS_Detail on a.detailID equals g.Id
                                    join h in _contextProp.MS_UnitCode on a.unitCodeID equals h.Id
                                    where a.projectID == input.projectId && a.clusterID == input.clusterId
                                    && a.unitCodeID == item.unitCodeId
                                    && (d.unitStatusCode == "A")
                                    && !new[] { getItem1, getItem2 }.Contains(b.itemID)
                                    orderby a.Id ascending
                                    select new UnitMobileDto
                                    {
                                        floorType = item.floor + a.unitNo,
                                        unitID = a.Id,
                                        unitNo = a.unitNo,
                                        unitStatusCode = d.unitStatusCode,
                                        unitStatusName = d.unitStatusName,
                                        bedroom = e.bedroom,
                                        zoningName = f.zoningName,
                                        detailID = a.detailID,
                                        detailCode = g.detailCode,
                                        detailImage = _fileHelper.getAbsoluteUri() + g.detailImage,
                                        zoningID = f.Id
                                    }).Distinct().ToList();

                    if (input.bedroom != null)
                    {
                        getUnits = (from a in getUnits
                                    where a.bedroom == input.bedroom
                                    select a).Distinct().ToList();
                    }

                    if (input.unitType != null)
                    {
                        getUnits = (from a in getUnits
                                    where a.unitNo.Contains(input.unitType)
                                    select a).Distinct().ToList();
                    }
                    if (input.zoningID != 0)
                    {
                        getUnits = (from a in getUnits
                                    where a.zoningID == input.zoningID
                                    select a).Distinct().ToList();
                    }

                    item.units = getUnits;
                }
            }
            return getUnit.OrderByDescending(x => x.floor).ToList();

        }


        //[AbpAuthorize(AppPermissions.Pages_Tenant_OnlineBooking_Diagramatic_GetListRenovation)]
        public List<ListRenovationResultDto> GetListRenovation(int unitID)
        {
            var getRenovation = (from a in _msUnitItemPrice.GetAll()
                                 join b in _msUnitItem.GetAll() on a.unitItemID equals b.Id
                                 join c in _msRenovation.GetAll().Distinct() on a.renovID equals c.Id
                                 join d in _lkItem.GetAll() on c.renovationCode equals d.itemCode
                                 where b.unitID == unitID
                                 select new ListRenovationResultDto
                                 {
                                     renovationID = c.Id,
                                     renovationCode = c.renovationCode,
                                     renovationName = d.shortName
                                 }).Distinct().ToList();

            return new List<ListRenovationResultDto>(getRenovation);
        }

        //[AbpAuthorize(AppPermissions.Pages_Tenant_OnlineBooking_Diagramatic_GetListTerm)]
        public List<ListTermResultDto> GetListTerm(int unitID)
        {
            var getTerm = (from a in _msUnit.GetAll()
                           join b in _msUnitItem.GetAll() on a.Id equals b.unitID
                           join c in _msUnitItemPrice.GetAll() on b.Id equals c.unitItemID
                           join d in _msTermRepo.GetAll() on c.termID equals d.Id
                           join e in _msTermMainRepo.GetAll() on d.termMainID equals e.Id
                           join f in _msTermRepo.GetAll() on a.termMainID equals f.termMainID
                           where a.Id == unitID
                           select new ListTermResultDto
                           {
                               termID = d.Id,
                               termName = d.remarks
                           }).Distinct().ToList();

            return new List<ListTermResultDto>(getTerm);
        }

        //[AbpAuthorize(AppPermissions.Pages_Tenant_OnlineBooking_Diagramatic_GetListTerm)]
        public GrossPriceDto GetGrossPrice(GetGrossPriceInputDto input)
        {
            var getGrossPrice = (from unitItemPrice in _msUnitItemPrice.GetAll()
                                 join unitItem in _msUnitItem.GetAll() on unitItemPrice.unitItemID equals unitItem.Id
                                 join unit in _msUnit.GetAll()
                                 on unitItem.unitID equals unit.Id
                                 where unit.Id == input.unitID
                                 && unitItemPrice.renovID == input.renovID
                                 && unitItemPrice.termID == input.termID
                                 group unitItemPrice by new
                                 {
                                     unitItem.pctDisc,
                                     unitItem.pctTax
                                 }into G
                                 select new
                                 {
                                     grossprice = G.Sum(a => a.grossPrice),
                                     pctTax = G.Key.pctTax,
                                     pctDisc = G.Key.pctDisc
                                 }).FirstOrDefault();

            var netPrice = getGrossPrice.grossprice * (1 - (decimal)getGrossPrice.pctDisc);

            var getDisc = (from a in _msTermAddDiscRepo.GetAll()
                           where a.termID == input.termID
                           select new
                           {
                               a.addDiscPct
                           }).ToList();

            foreach (var item in getDisc)
            {
                netPrice = netPrice * (1 - (decimal)item.addDiscPct);
            }

            netPrice = netPrice * (1 + (decimal)getGrossPrice.pctTax);
            
            var result = new GrossPriceDto
            {
                grossPrice = netPrice
            };

            return result;
        }
        
        //[AbpAuthorize(AppPermissions.Pages_Tenant_OnlineBooking_Diagramatic_GetListDiagramaticWeb)]
        public List<GetDiagramaticForWebListDto> GetListDiagramaticWeb(DiagramaticInputDto input)
        {
            List<GetDiagramaticForWebListDto> getUnit = new List<GetDiagramaticForWebListDto>();
            #region old

            getUnit = (from a in _contextProp.MS_Unit
                       join b in _contextProp.MS_UnitItem on a.Id equals b.unitID
                       join c in _contextProp.MS_UnitItemPrice on b.Id equals c.unitItemID
                       join d in _contextProp.LK_UnitStatus on a.unitStatusID equals d.Id
                       join e in _contextProp.MS_UnitCode on a.unitCodeID equals e.Id
                       join f in _contextProp.MS_Category on a.categoryID equals f.Id
                       where a.projectID == input.projectId && a.clusterID == input.clusterId
                       && (d.unitStatusCode == "A")
                       select new GetDiagramaticForWebListDto
                       {
                           unitCodeId = a.unitCodeID,
                           floor = e.floor,
                           unitCode = e.unitCode,
                           diagramaticType = f.diagramaticType,
                           unitName = e.unitName
                       }).Distinct().ToList();

            //SendConsole("getUnit:" + JsonConvert.SerializeObject(getUnit));

            if (getUnit.Count == 0)
            {
                var error = new List<GetDiagramaticForWebListDto>();

                var message = new GetDiagramaticForWebListDto
                {
                    message = "projectId/clusterId Tidak Valid"
                };

                error.Add(message);

                //throw new UserFriendlyException("projectId/clusterId Tidak Valid");

                return new List<GetDiagramaticForWebListDto>(error);
            }

            foreach (var item in getUnit)
            {
                if(item.diagramaticType.ToLowerInvariant() == "highrise")
                {
                    var getItem1 = (from a in _lkItem.GetAll()
                                    where a.itemCode == "01"
                                    select a.Id).FirstOrDefault();

                    var getItem2 = (from a in _lkItem.GetAll()
                                    where a.itemCode == "02"
                                    select a.Id).FirstOrDefault();

                    var getUnits = (from a in _contextProp.MS_Unit
                                    join b in _contextProp.MS_UnitItem on a.Id equals b.unitID
                                    join c in _contextProp.MS_UnitItemPrice on b.Id equals c.unitItemID
                                    join d in _contextProp.LK_UnitStatus on a.unitStatusID equals d.Id
                                    join e in _contextProp.MS_UnitRoom on b.Id equals e.unitItemID into unitRoom
                                    from e in unitRoom.DefaultIfEmpty()
                                    join f in _contextProp.MS_Zoning on a.zoningID equals f.Id
                                    join g in _contextProp.MS_Detail on a.detailID equals g.Id
                                    where a.projectID == input.projectId && a.clusterID == input.clusterId
                                    && a.unitCodeID == item.unitCodeId
                                    && (d.unitStatusCode == "A")
                                    && !new[] { getItem1, getItem2 }.Contains(b.itemID)
                                    orderby a.Id ascending
                                    select new UnitsDto
                                    {
                                        unitID = a.Id,
                                        unitNo = a.unitNo,
                                        unitStatusCode = d.unitStatusCode,
                                        unitStatusName = d.unitStatusName,
                                        bedroom = e.bedroom,
                                        zoningName = f.zoningName,
                                        detailID = a.detailID,
                                        detailCode = g.detailCode,
                                        detailImage = _fileHelper.getAbsoluteUri() + g.detailImage
                                    }).Distinct().ToList();

                    if (input.detailID != null && input.detailID != 0)
                    {
                        getUnits = (from a in getUnits
                                    join b in _contextProp.MS_Detail on a.detailID equals b.Id
                                    where a.detailID == input.detailID
                                    select a).Distinct().ToList();
                    }

                    item.units = getUnits;
                }
                else if(item.diagramaticType.ToLowerInvariant() == "landed" || item.diagramaticType.ToLowerInvariant() == "kavling")
                {
                    var getItem1 = (from a in _lkItem.GetAll()
                                    where a.itemCode == "01"
                                    select a.Id).FirstOrDefault();

                    var getItem2 = (from a in _lkItem.GetAll()
                                    where a.itemCode == "02"
                                    select a.Id).FirstOrDefault();

                    var getUnits = (from a in _contextProp.MS_Unit
                                    join b in _contextProp.MS_UnitItem on a.Id equals b.unitID
                                    join c in _contextProp.MS_UnitItemPrice on b.Id equals c.unitItemID
                                    join d in _contextProp.LK_UnitStatus on a.unitStatusID equals d.Id
                                    join e in _contextProp.MS_UnitRoom on b.Id equals e.unitItemID into unitRoom
                                    from e in unitRoom.DefaultIfEmpty()
                                    join f in _contextProp.MS_Zoning on a.zoningID equals f.Id
                                    join g in _contextProp.MS_Detail on a.detailID equals g.Id
                                    where a.projectID == input.projectId && a.clusterID == input.clusterId
                                    && a.unitCodeID == item.unitCodeId
                                    && (d.unitStatusCode == "A")
                                    && !new[] { getItem1, getItem2 }.Contains(b.itemID)
                                    orderby a.Id ascending
                                    select new UnitsDto
                                    {
                                        unitID = a.Id,
                                        unitNo = a.unitNo,
                                        unitStatusCode = d.unitStatusCode,
                                        unitStatusName = d.unitStatusName,
                                        bedroom = e.bedroom,
                                        zoningName = f.zoningName,
                                        detailID = a.detailID,
                                        detailCode = g.detailCode,
                                        detailImage = _fileHelper.getAbsoluteUri() + g.detailImage
                                    }).Distinct().ToList();

                    if (input.detailID != null && input.detailID != 0)
                    {
                        getUnits = (from a in getUnits
                                    join b in _contextProp.MS_Detail on a.detailID equals b.Id
                                    where a.detailID == input.detailID
                                    select a).Distinct().ToList();
                    }

                    item.units = getUnits;
                    item.floor = item.unitName +" "+ item.floor;
                }
                
            }
            #endregion
            #region new
            //var getAllData = (from mu in _contextProp.MS_Unit
            //                  join mui in _contextProp.MS_UnitItem on mu.Id equals mui.unitID
            //                  //into unitItem from mui_ in unitItem.DefaultIfEmpty()
            //                  join mur in _contextProp.MS_UnitRoom on mui.Id equals mur.unitItemID 
            //                  into unitRoom from mur_ in unitRoom.DefaultIfEmpty()
            //                  join lus in _contextProp.LK_UnitStatus on mu.unitStatusID equals lus.Id
            //                  join mz in _contextProp.MS_Zoning on mu.zoningID equals mz.Id
            //                  join muc in _contextProp.MS_UnitCode on mu.unitCodeID equals muc.Id
            //                  where mu.projectID == input.projectId && mu.clusterID == input.clusterId
            //                  && (lus.unitStatusCode == "A" || lus.unitStatusCode == "Z")
            //                  orderby mu.Id ascending
            //                  select new CustomGetDiagramaticForWebListDto
            //                  {
            //                      unitID = mu.Id,
            //                      unitCodeId = mu.unitCodeID,
            //                      unitCode = muc.unitCode,
            //                      floor = muc.unitCode.Substring(muc.unitCode.Length - 2),
            //                      unitNo = mu.unitNo,
            //                      unitStatusCode = lus.unitStatusCode,
            //                      unitStatusName = lus.unitStatusName,
            //                      bedroom = mur_.bedroom,
            //                      zoningName = mz.zoningName
            //                  }).GroupBy(u => u.unitCodeId)
            //                      .Select(group => new GetDiagramaticForWebListDto
            //                      {
            //                          unitID = group.First().unitID,
            //                          unitCodeId = group.First().unitCodeId,
            //                          unitCode = group.First().unitCode,
            //                          floor = group.First().floor,
            //                          units = group.Select(item => new UnitsDto
            //                          {
            //                              unitID = item.unitID,
            //                              unitNo = item.unitNo,
            //                              unitStatusCode = item.unitStatusCode,
            //                              unitStatusName = item.unitStatusName,
            //                              bedroom = item.bedroom,
            //                              zoningName = item.zoningName
            //                          }).ToList()
            //                      })
            //                    .ToList();

            if (getUnit == null)
            {
                var error = new List<GetDiagramaticForWebListDto>();
                var message = new GetDiagramaticForWebListDto
                {
                    message = "projectId/clusterId Tidak Valid"
                };
                error.Add(message);
                return new List<GetDiagramaticForWebListDto>(error);
            }
            //getUnit.AddRange(getAllData);

            #endregion

            return getUnit.OrderByDescending(x => x.floor).ToList();
        }

        //[AbpAuthorize(AppPermissions.Pages_Tenant_OnlineBooking_Diagramatic_GetUnitSelectionDetail)]
        public GetUnitSelectionDetailDto GetUnitSelectionDetail(int unitID)
        {
            var getUnit = (from a in _msUnit.GetAll()
                           join b in _msProject.GetAll() on a.projectID equals b.Id
                           join c in _msCluster.GetAll() on a.clusterID equals c.Id
                           join d in _msUnitCode.GetAll() on a.unitCodeID equals d.Id
                           where a.Id == unitID
                           select new GetUnitSelectionDetailDto
                           {
                               projectName = b.projectName,
                               tower = c.clusterName,
                               unitNo = a.unitNo,
                               unitCode = d.unitCode
                           }).FirstOrDefault();

            return getUnit;
        }

        //[AbpAuthorize(AppPermissions.Pages_Tenant_OnlineBooking_Diagramatic_GetUnitSelectionDetailMobile)]
        public GetUnitSelectionDetailDto GetUnitSelectionDetailMobile(int unitID)
        {
            var getUnit = (from a in _msUnit.GetAll()
                           join b in _msProject.GetAll() on a.projectID equals b.Id
                           join c in _msCluster.GetAll() on a.clusterID equals c.Id
                           join d in _msUnitCode.GetAll() on a.unitCodeID equals d.Id
                           join e in _msTermMainRepo.GetAll() on a.termMainID equals e.Id
                           where a.Id == unitID
                           select new GetUnitSelectionDetailDto
                           {
                               projectName = b.projectName,
                               tower = c.clusterName,
                               unitNo = a.unitNo,
                               unitCode = d.unitCode,
                               bfAmount = e.BFAmount
                           }).FirstOrDefault();

            return getUnit;
        }

        //[AbpAuthorize(AppPermissions.Pages_Tenant_OnlineBooking_Diagramatic_GetTypeDiagramatic)]
        public List<TypeDiagramaticResultDto> GetTypeDiagramatic(DiagramaticInputDto input)
        {
            var getType = (from a in _msUnit.GetAll()
                           where a.projectID == input.projectId
                           && a.clusterID == input.clusterId
                           //&& a.unitStatusID == (from z in _lkUnitStatus.GetAll()
                           //                      where z.unitStatusCode.ToLowerInvariant() == "z"
                           //                      select z.Id).FirstOrDefault()
                           select new TypeDiagramaticResultDto
                           {
                               type = a.unitNo
                           }).Distinct().ToList();

            return getType;
        }

        //[AbpAuthorize(AppPermissions.Pages_Tenant_OnlineBooking_Diagramatic_GetListBedroom)]
        public List<ListBedroomResultDto> GetListBedroom(int projectID, int clusterID)
        {
            var getBedroom = (from a in _msUnitRoomRepo.GetAll()
                              join b in _msUnitItem.GetAll() on a.unitItemID equals b.Id
                              join c in _msUnit.GetAll() on b.unitID equals c.Id
                              join d in _lkItem.GetAll() on b.itemID equals d.Id
                              where c.projectID == projectID && c.clusterID == clusterID && !new[] { "02", "01" }.Contains(d.itemCode)
                              select new ListBedroomResultDto
                              {
                                  bedroom = a.bedroom
                              }).Distinct().ToList();
            return getBedroom;
        }

        //[AbpAuthorize(AppPermissions.Pages_Tenant_OnlineBooking_Diagramatic_GetListZoning)]
        public List<ListZoningResultDto> GetListZoning(int projectID, int clusterID)
        {
            var getZoning = (from a in _msZoningRepo.GetAll()
                             join b in _msUnit.GetAll() on a.Id equals b.zoningID
                             where b.projectID == projectID && b.clusterID == clusterID
                             select new ListZoningResultDto
                             {
                                 zoningId = a.Id,
                                 zoningCode = a.zoningCode,
                                 zoningName = a.zoningName
                             }).Distinct().ToList();
            return getZoning;
        }

        //[AbpAuthorize(AppPermissions.Pages_Tenant_OnlineBooking_Diagramatic_GetListSumberDana)]
        public List<ListSumberDanaResultDto> GetListSumberDana()
        {
            var getSumberDana = (from a in _msSumberDanaRepo.GetAll()
                                 select new ListSumberDanaResultDto
                                 {
                                     sumberDanaId = a.Id,
                                     sumberDanaCode = a.sumberDanaCode,
                                     sumberDanaName = a.sumberDanaName
                                 }).ToList();

            return getSumberDana;
        }

        //[AbpAuthorize(AppPermissions.Pages_Tenant_OnlineBooking_Diagramatic_GetListTujuanTransaksi)]
        public List<ListTujuanTransaksiResultDto> GetListTujuanTransaksi()
        {
            var getTujuanTransaksi = (from a in _msTujuanTransaksiRepo.GetAll()
                                      select new ListTujuanTransaksiResultDto
                                      {
                                          tujuanTransaksiId = a.Id,
                                          tujuanTransaksiCode = a.tujuanTransaksiCode,
                                          tujuanTransaksiName = a.tujuanTransaksiName
                                      }).ToList();

            return getTujuanTransaksi;
        }

        //[AbpAuthorize(AppPermissions.Pages_Tenant_OnlineBooking_Diagramatic_GetDetailDiagramatic)]
        public ListDetailDiagramaticWebResultDto GetDetailDiagramatic(int unitID)
        {
            var getItem2 = (from a in _lkItem.GetAll()
                            where a.itemCode == "02"
                            select a.Id).FirstOrDefault();

            var getUnit = (from a in _msUnitItemPrice.GetAll()
                           join b in _msUnitItem.GetAll() on a.unitItemID equals b.Id
                           join c in _msUnit.GetAll() on b.unitID equals c.Id
                           join d in _lkUnitStatus.GetAll() on c.unitStatusID equals d.Id
                           join e in _msUnitCode.GetAll() on c.unitCodeID equals e.Id
                           join f in _msTermMainRepo.GetAll() on c.termMainID equals f.Id
                           where b.unitID == unitID && b.itemID == getItem2
                           select new ListDetailDiagramaticWebResultDto
                           {
                               unitItemID = b.Id,
                               unitID = c.Id,
                               unitNo = c.unitNo,
                               unitCode = e.unitCode,
                               unitStatus = d.unitStatusName,
                               size = b.area,
                               bookingFee = f.BFAmount
                           }).FirstOrDefault();
            var getUnitRoom = (from a in _msUnitRoomRepo.GetAll()
                               where a.unitItemID == getUnit.unitItemID
                               select a).FirstOrDefault();

            if (getUnitRoom != null)
            {
                getUnit.bedroom = getUnitRoom.bedroom;
            }

            var listPrice = new List<ListTerm>();

            var getTerm = (from a in _msUnitItemPrice.GetAll()
                           join b in _msTermRepo.GetAll() on a.termID equals b.Id
                           join c in _msRenovation.GetAll() on a.renovID equals c.Id
                           join d in _lkItem.GetAll() on c.renovationCode equals d.itemCode
                           join e in _msUnitItem.GetAll() on a.unitItemID equals e.Id
                           where e.unitID == unitID
                           select new ListTerm
                           {
                               termID = a.termID,
                               termName = b.remarks,
                               renovID = a.renovID,
                               renovName = d.shortName,
                               termNo = b.termNo
                           }).Distinct().ToList();

            foreach (var item in getTerm)
            {
                var listGP = new List<ListPrice>();

                var getGrossPrice = (from unitItemPrice in _msUnitItemPrice.GetAll()
                                     join unitItem in _msUnitItem.GetAll() on unitItemPrice.unitItemID equals unitItem.Id
                                     join unit in _msUnit.GetAll()
                                     on unitItem.unitID equals unit.Id
                                     where unit.Id == unitID
                                     && unitItemPrice.renovID == item.renovID
                                     && unitItemPrice.termID == item.termID
                                     group unitItemPrice by new
                                     {
                                         unitItem.pctDisc,
                                         unitItem.pctTax
                                     } into G
                                     select new ListPrice
                                     {
                                         grossprice = G.Sum(a => a.grossPrice),
                                         pctTax = G.Key.pctTax,
                                         pctDisc = G.Key.pctDisc
                                     }).FirstOrDefault();
                getGrossPrice.grossprice = getGrossPrice.grossprice * (1 - (decimal)getGrossPrice.pctDisc);

                var getDisc = (from a in _msTermAddDiscRepo.GetAll()
                               where a.termID == item.termID
                               select new
                               {
                                   a.addDiscPct
                               }).ToList();

                foreach (var dto in getDisc)
                {
                    getGrossPrice.grossprice = getGrossPrice.grossprice * (1 - (decimal)dto.addDiscPct);
                }

                getGrossPrice.grossprice = getGrossPrice.grossprice * (1 + (decimal)getGrossPrice.pctTax);

                item.listPrice = getGrossPrice.grossprice;
            }

            var getPriceArea = (from a in getTerm
                                where a.termNo == 3
                                select a.listPrice).FirstOrDefault();

            var getArea = (from a in _msUnitItem.GetAll()
                           where a.itemID == getItem2 && a.unitID == unitID
                           select a).FirstOrDefault();

            var priceArea = getPriceArea / (decimal)getArea.area;

            var result = new ListDetailDiagramaticWebResultDto
            {
                unitNo = getUnit == null ? null : getUnit.unitNo,
                unitCode = getUnit == null ? null : getUnit.unitCode,
                unitStatus = getUnit == null ? null : getUnit.unitStatus,
                size = getUnit == null ? 0 : getUnit.size,
                bedroom = getUnit == null ? null : getUnit.bedroom,
                bookingFee = getUnit == null ? 0 : getUnit.bookingFee,
                term = getTerm,
                pricePerArea = priceArea,
                itemID = getUnit.itemID,
                category = getUnit.category,
                unitItemID = getUnit.unitItemID,
                sellingPrice = getUnit.sellingPrice,
                unitID = getUnit.unitID
            };

            return result;
        }

        //[AbpAuthorize(AppPermissions.Pages_Tenant_OnlineBooking_Diagramatic_GetPaymentType)]
        public List<ListPaymentTypeResultDto> GetPaymentType()
        {
            var getPaymentType = (from a in _lkPaymentType.GetAll()
                                  where new[] { 4, 3, 10, 11, 16 }.Contains(a.paymentType)
                                  select new ListPaymentTypeResultDto
                                  {
                                      paymentTypeID = a.Id,
                                      paymentType = a.paymentType,
                                      paymentTypeName = a.paymentTypeName
                                  }).ToList();

            return getPaymentType;
        }

        public ResultMessageDto GetDetailImage (int unitID)
        {
            var getDetail = (from a in _contextProp.MS_Unit
                             join b in _contextProp.MS_Detail on a.detailID equals b.Id
                             select b.detailImage).FirstOrDefault();

            return new ResultMessageDto
            {
                result = true,
                message = _fileHelper.getAbsoluteUri() + getDetail
            };
        }
    }
}