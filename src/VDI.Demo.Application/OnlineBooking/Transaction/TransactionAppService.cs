﻿using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.Authorization;
using VDI.Demo.Authorization.Users;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.NewCommDB;
using VDI.Demo.OnlineBooking.Email;
using VDI.Demo.OnlineBooking.PaymentMidtrans;
using VDI.Demo.OnlineBooking.Transaction.Dto;
using VDI.Demo.PersonalsDB;
using VDI.Demo.PropertySystemDB.LippoMaster;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;
using VDI.Demo.PropertySystemDB.OnlineBooking.DemoDB;
using VDI.Demo.PropertySystemDB.OnlineBooking.PPOnline;
using VDI.Demo.PropertySystemDB.OnlineBooking.ProjectInfo;
using VDI.Demo.PropertySystemDB.OnlineBooking.PropertySystem;
using VDI.Demo.PropertySystemDB.Pricing;
using System.Linq;
using Abp.AutoMapper;
using Newtonsoft.Json;
using VDI.Demo.OnlineBooking.PaymentMidtrans.Dto;
using VDI.Demo.OnlineBooking.Email.Dto;
using System.Net;
using Abp.Extensions;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.IO;
using Abp.UI;
using System.Globalization;
using Microsoft.AspNetCore.Hosting;
using VDI.Demo.Files;
using Abp.Application.Services.Dto;
using System.Data;

namespace VDI.Demo.OnlineBooking.Transaction
{
    public class TransactionAppService : DemoAppServiceBase, ITransactionAppService
    {
        private readonly IRepository<TR_BookingHeader> _trBookingHeaderRepo;
        private readonly IRepository<TR_UnitReserved> _trUnitReservedRepo;
        private readonly IRepository<TR_UnitOrderHeader> _trUnitOrderHeader;
        private readonly IRepository<TR_UnitOrderDetail> _trUnitOrderDetail;
        private readonly IRepository<TR_BookingDetail> _trBookingDetailRepo;
        private readonly IRepository<PERSONALS, string> _personalsRepo;
        private readonly IRepository<PERSONALS_MEMBER, string> _personalsMemberRepo;
        private readonly IRepository<MS_UnitItemPrice> _msUnitItemPriceRepo;
        private readonly IRepository<MS_Term> _msTermRepo;
        private readonly IRepository<MS_TermAddDisc> _msTermAddDiscRepo;
        private readonly IRepository<MS_Unit> _msUnitRepo;
        private readonly IRepository<TR_ID, string> _trIDRepo;
        private readonly IRepository<LK_Item> _lkItem;
        private readonly IRepository<MS_Cluster> _msClusterRepo;
        private readonly IRepository<MS_Project> _msProjectRepo;
        private readonly IRepository<TR_Email, string> _trEmailRepo;
        private readonly IRepository<TR_Phone, string> _trPhoneRepo;
        private readonly IRepository<MS_UnitCode> _msUnitCode;
        private readonly IRepository<MS_TermMain> _msTermMain;
        private readonly IRepository<MS_DiscOnlineBooking> _msDiscOnlineBooking;
        private readonly IRepository<MP_UserPersonals> _mpUserPersonals;
        private readonly IRepository<MS_UnitItem> _msUnitItem;
        private readonly IRepository<MS_Project> _msProject;
        private readonly IRepository<MS_Schema, string> _msSchema;
        private readonly IRepository<MS_TransFrom> _msTransFrom;
        private readonly IRepository<MS_SalesEvent> _msSalesEvent;
        private readonly IRepository<MS_TermPmt> _msTermPmt;
        private readonly IRepository<LK_FinType> _lkFinType;
        private readonly IRepository<TR_BookingDetailAddDisc> _trBookingAddDisc;
        private readonly IRepository<TR_MKTAddDisc> _trMKTAddDisc;
        private readonly IRepository<TR_CommAddDisc> _trCommAddDisc;
        private readonly IRepository<MS_TermDP> _msTermDP;
        private readonly IRepository<TR_BookingDetailDP> _trBookingDetailDP;
        private readonly IRepository<MS_Renovation> _msRenovation;
        private readonly IRepository<LK_BookingTrType> _lkBookingTrType;
        private readonly IRepository<TR_CashAddDisc> _trCashAddDisc;
        private readonly IRepository<TR_BookingSalesDisc> _trBookingSalesDisc;
        private readonly IRepository<MS_TermDiscOnlineBooking> _msTermDiscOnlineBooking;
        private readonly IRepository<LK_BookingOnlineStatus> _lkBookingOnlineStatus;
        private readonly IRepository<TR_BookingHeaderTerm> _trBookingHeaderTerm;
        private readonly IRepository<TR_BookingItemPrice> _trBookingItemPrice;
        private readonly IRepository<LK_UnitStatus> _lkUnitStatus;
        private readonly IRepository<MS_Category> _msCategory;
        private readonly IRepository<MS_TaxType> _msTaxType;
        private readonly IRepository<TR_BookingTax> _trBookingTax;
        private readonly IRepository<LK_Alloc> _lkAlloc;
        private readonly IRepository<LK_DPCalc> _lkDpCalc;
        private readonly IRepository<TR_BookingDetailSchedule> _trBookingDetailSchedule;
        private readonly IRepository<TR_PaymentDetailAlloc> _trPaymentDetailAlloc;
        private readonly IRepository<TR_PaymentDetail> _trPaymentDetail;
        private readonly IRepository<TR_PaymentHeader> _trPaymentHeader;
        private readonly IRepository<LK_PayFor> _lkPayFor;
        private readonly IRepository<LK_PaymentType> _lkPaymentType;
        private readonly PropertySystemDbContext _context;
        private readonly IRepository<TR_SoldUnit, string> _trSoldUnit;
        private readonly IRepository<TR_SoldUnitRequirement, string> _trSoldUnitRequirement;
        private readonly IRepository<SYS_BookingCounter> _sysBookingCounter;
        private readonly IRepository<MS_BobotComm, string> _msBobotComm;
        private readonly IRepository<MS_SchemaRequirement, string> _msSchemaRequirement;
        private readonly IRepository<User, long> _userRepo;
        private readonly IPaymentOBAppService _paymentMidtrans;
        private readonly IEmailAppService _emailAppService;
        private readonly IRepository<MS_ProjectInfo> _msProjectInfo;
        private readonly IRepository<TR_PaymentOnlineBook> _trPaymentOnlineBook;
        private readonly IRepository<MS_SumberDana> _msSumberDana;
        private readonly IRepository<MS_TujuanTransaksi> _msTujuanTransaksi;
        private readonly IRepository<MS_BankOLBooking> _msBankOLBooking;
        private readonly IRepository<MS_Detail> _msDetail;
        private readonly IRepository<MS_Cluster> _msCluster;
        private readonly PersonalsNewDbContext _contextPers;
        private readonly NewCommDbContext _contextNew;
        private readonly PaymentOBConfiguration _configuration;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<MS_Bank> _msBank;
        private readonly IRepository<LK_FormulaDP> _lkFormulaDP;
        private readonly IRepository<LK_Promotion> _lkPromotion;
        private readonly IRepository<MS_ShopBusiness> _msShopBusiness;
        private readonly IRepository<LK_SADStatus> _lkSadStatus;
        private readonly IRepository<MS_Facade> _msFacade;
        private readonly IRepository<MS_Discount> _msDiscount;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly DemoDbContext _contextDemo;
        private readonly FilesHelper _fileHelper;
        private readonly IRepository<MS_Notaris> _msNotaris;

        public TransactionAppService(
            IRepository<MS_Unit> msUnit,
            IRepository<TR_BookingHeader> trBookingHeaderRepo,
            IRepository<TR_UnitReserved> trUnitReservedRepo,
            IRepository<TR_UnitOrderHeader> trUnitOrderHeader,
            IRepository<TR_UnitOrderDetail> trUnitOrderDetail,
            IRepository<TR_BookingDetail> trBookingDetailRepo,
            IRepository<PERSONALS, string> personalsRepo,
            IRepository<PERSONALS_MEMBER, string> personalsMemberRepo,
            IRepository<MS_UnitItemPrice> msUnitItemPrice,
            IRepository<MS_Term> msTermRepo,
            IRepository<MS_TermAddDisc> msTermAddDiscRepo,
            IRepository<MS_Unit> msUnitRepo,
            IRepository<TR_ID, string> trIDRepo,
            IRepository<LK_Item> lkItem,
            IRepository<MS_UnitCode> msUnitCode,
            IRepository<MS_TermMain> msTermMain,
            IRepository<MS_DiscOnlineBooking> msDiscOnlineBooking,
            IRepository<MP_UserPersonals> mpUserPersonals,
            IRepository<MS_UnitItem> msUnitItem,
            IRepository<MS_Project> msProject,
            IRepository<MS_Schema, string> msSchema,
            IRepository<MS_Cluster> msClusterRepo,
            IRepository<MS_Project> msProjectRepo,
            IRepository<TR_Email, string> trEmailRepo,
            IRepository<TR_Phone, string> trPhoneRepo,
            IRepository<MS_TransFrom> msTransFrom,
            IRepository<MS_SalesEvent> msSalesEvent,
            IRepository<MS_TermPmt> msTermPmt,
            IRepository<LK_FinType> lkFinType,
            IRepository<MS_TermDP> msTermDP,
            IRepository<TR_BookingDetailDP> trBookingDetailDP,
            IRepository<TR_BookingDetailAddDisc> trBookingAddDisc,
            IRepository<TR_MKTAddDisc> trMKTAddDisc,
            IRepository<TR_CommAddDisc> trCommAddDisc,
            IRepository<MS_Renovation> msRenovation,
            IRepository<LK_BookingTrType> lkBookingTrType,
            IRepository<TR_CashAddDisc> trCashAddDisc,
            IRepository<TR_BookingSalesDisc> trBookingSalesDisc,
            IRepository<MS_TermDiscOnlineBooking> msTermDiscOnlineBooking,
            IRepository<LK_BookingOnlineStatus> lkOnlineBookingStatus,
            IRepository<TR_BookingHeaderTerm> trBookingHeaderTerm,
            IRepository<TR_BookingItemPrice> trBookingItemPrice,
            IRepository<LK_UnitStatus> lkUnitStatus,
            IRepository<MS_Category> msCategory,
            IRepository<MS_TaxType> msTaxType,
            IRepository<TR_BookingTax> trBookingTax,
            IRepository<LK_Alloc> lkAlloc,
            PropertySystemDbContext context,
            IRepository<LK_DPCalc> lkDpCalc,
            IRepository<TR_BookingDetailSchedule> trBookingDetailSchedule,
            IRepository<TR_PaymentDetailAlloc> trPaymentDetailAlloc,
            IRepository<TR_PaymentDetail> trPaymentDetail,
            IRepository<TR_PaymentHeader> trPaymentHeader,
            IRepository<LK_PayFor> lkPayFor,
            IRepository<TR_SoldUnit, string> trSoldUnit,
            IRepository<TR_SoldUnitRequirement, string> trSoldUnitRequirement,
            IRepository<SYS_BookingCounter> sysBookingCounter,
            IRepository<MS_BobotComm, string> msBobotComm,
            IRepository<MS_SchemaRequirement, string> msSchemaRequirement,
            IRepository<User, long> userRepo,
            IRepository<LK_PaymentType> lkPaymentType,
            IPaymentOBAppService paymentMidtrans,
            IEmailAppService emailAppService,
            IRepository<MS_ProjectInfo> msProjectInfo,
            IRepository<TR_PaymentOnlineBook> trPaymentOnlineBook,
            IRepository<MS_Cluster> msCluster,
            PersonalsNewDbContext contextPers,
            NewCommDbContext contextNew,
            PaymentOBConfiguration configuration,
            IRepository<MS_TujuanTransaksi> msTujuanTransaksi,
            IRepository<MS_SumberDana> msSumberDana,
            IRepository<MS_Detail> msDetail,
            IRepository<MS_BankOLBooking> msBankOlBooking,
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<MS_Bank> msBank,
            IRepository<LK_FormulaDP> lkFormulaDP,
            IRepository<LK_Promotion> lkPromotion,
            IRepository<MS_ShopBusiness> msShopBusiness,
            IRepository<LK_SADStatus> lkSadStatus,
            IRepository<MS_Facade> msFacade,
            IRepository<MS_Discount> msDiscount,
            IHostingEnvironment hostingEnvironment,
            DemoDbContext contextDemo,
            FilesHelper fileHelper,
            IRepository<MS_Notaris> msNotaris
            )
        {
            _trBookingHeaderRepo = trBookingHeaderRepo;
            _trUnitReservedRepo = trUnitReservedRepo;
            _trUnitOrderHeader = trUnitOrderHeader;
            _trUnitOrderDetail = trUnitOrderDetail;
            _trBookingDetailRepo = trBookingDetailRepo;
            _personalsRepo = personalsRepo;
            _personalsMemberRepo = personalsMemberRepo;
            _msUnitItemPriceRepo = msUnitItemPrice;
            _msTermRepo = msTermRepo;
            _msTermAddDiscRepo = msTermAddDiscRepo;
            _msUnitRepo = msUnitRepo;
            _trIDRepo = trIDRepo;
            _lkItem = lkItem;
            _msUnitRepo = msUnit;
            _msUnitCode = msUnitCode;
            _msTermMain = msTermMain;
            _msClusterRepo = msClusterRepo;
            _msProjectRepo = msProjectRepo;
            _trEmailRepo = trEmailRepo;
            _trPhoneRepo = trPhoneRepo;
            _msDiscOnlineBooking = msDiscOnlineBooking;
            _mpUserPersonals = mpUserPersonals;
            _msUnitItem = msUnitItem;
            _msProject = msProject;
            _msSchema = msSchema;
            _msTransFrom = msTransFrom;
            _msSalesEvent = msSalesEvent;
            _msTermPmt = msTermPmt;
            _msTermDP = msTermDP;
            _trBookingDetailDP = trBookingDetailDP;
            _trBookingAddDisc = trBookingAddDisc;
            _trMKTAddDisc = trMKTAddDisc;
            _trCommAddDisc = trCommAddDisc;
            _msRenovation = msRenovation;
            _lkBookingTrType = lkBookingTrType;
            _trCashAddDisc = trCashAddDisc;
            _trBookingSalesDisc = trBookingSalesDisc;
            _msTermDiscOnlineBooking = msTermDiscOnlineBooking;
            _lkBookingOnlineStatus = lkOnlineBookingStatus;
            _trBookingHeaderTerm = trBookingHeaderTerm;
            _lkFinType = lkFinType;
            _trBookingItemPrice = trBookingItemPrice;
            _lkUnitStatus = lkUnitStatus;
            _msCategory = msCategory;
            _msTaxType = msTaxType;
            _trBookingTax = trBookingTax;
            _context = context;
            _lkAlloc = lkAlloc;
            _trBookingDetailSchedule = trBookingDetailSchedule;
            _lkDpCalc = lkDpCalc;
            _trPaymentDetail = trPaymentDetail;
            _trPaymentDetailAlloc = trPaymentDetailAlloc;
            _trPaymentHeader = trPaymentHeader;
            _lkPayFor = lkPayFor;
            _trSoldUnit = trSoldUnit;
            _trSoldUnitRequirement = trSoldUnitRequirement;
            _sysBookingCounter = sysBookingCounter;
            _msBobotComm = msBobotComm;
            _msSchemaRequirement = msSchemaRequirement;
            _userRepo = userRepo;
            _lkPaymentType = lkPaymentType;
            _paymentMidtrans = paymentMidtrans;
            _emailAppService = emailAppService;
            _msProjectInfo = msProjectInfo;
            _trPaymentOnlineBook = trPaymentOnlineBook;
            _msCluster = msCluster;
            _contextPers = contextPers;
            _contextNew = contextNew;
            _configuration = configuration;
            _msBankOLBooking = msBankOlBooking;
            _unitOfWorkManager = unitOfWorkManager;
            _msTujuanTransaksi = msTujuanTransaksi;
            _msSumberDana = msSumberDana;
            _msDetail = msDetail;
            _msBank = msBank;
            _lkFormulaDP = lkFormulaDP;
            _lkPromotion = lkPromotion;
            _msShopBusiness = msShopBusiness;
            _lkSadStatus = lkSadStatus;
            _msFacade = msFacade;
            _msDiscount = msDiscount;
            _hostingEnvironment = hostingEnvironment;
            _contextDemo = contextDemo;
            _fileHelper = fileHelper;
            _msNotaris = msNotaris;
        }

        [UnitOfWork(isTransactional: false)]
        public TrUnitOrderHeaderResultDto ReorderUnitMobey(CreateTransactionUniversalDto input)
        {
            var getData = (from a in _trUnitOrderHeader.GetAll()
                           where a.orderCode == input.orderCode
                           select a).FirstOrDefault();

            var getStatusCancel = (from status in _lkBookingOnlineStatus.GetAll()
                                   where status.statusType == "4"
                                   select status.Id).FirstOrDefault();

            var update = getData.MapTo<TR_UnitOrderHeader>();

            update.statusID = getStatusCancel;

            _trUnitOrderHeader.Update(update);

            var result = InsertTrUnitOrderHeader(input);

            return result;
        }

        public ResultMessageDto CreateLogMobey(CreateLogMobeyInputDto input)
        {
            var transTime = Convert.ToDateTime(input.transactionTime);

            var voidTime = Convert.ToDateTime(input.voidTime);

            Logger.InfoFormat("CreateLogMobey() - Start.");

            Logger.DebugFormat("Param TR_LogMobey. {0}" +
                "orderCode              = {1}{0}" +
                "status                 = {2}{0}" +
                "messageError           = {3}{0}" +
                "transactionReference   = {4}{0}" +
                "transactionTime        = {5}{0}" +
                "isVoid                 = {6}{0}" +
                "voidTime               = {7}{0}" ,
            Environment.NewLine, input.orderCode, input.status, input.messageError, input.transactionReference, input.transactionTime,
            input.isVoid, input.voidTime);

            var data = new TR_LogMobey
            {
                messageError = input.messageError,
                orderCode = input.orderCode,
                description = input.status,
                transactionReference = input.transactionReference,
                transactionTime = transTime,
                isVoid = input.isVoid,
                voidTime = voidTime
            };
            _context.TR_LogMobey.Add(data);
            _context.SaveChanges();

            Logger.InfoFormat("CreateLogMobey() - End.");


            return new ResultMessageDto
            {
                message = "Success",
                result = true
            };
        }

        public ListResultDto<GetListNotaris> GetListNotaris()
        {
            var notaris = (from n in _msNotaris.GetAll()
                               select new GetListNotaris
                               {
                                   notarisId = n.Id,
                                   notarisName = n.namaNotaris,
                               }).ToList();
            try
            {
                return new ListResultDto<GetListNotaris>(notaris);
            }
            // Handle data errors.
            catch (DataException exDb)
            {
                throw new UserFriendlyException("Database Error : {0}", exDb.Message);
            }
            // Handle all other exceptions.
            catch (Exception ex)
            {
                throw new UserFriendlyException("Error : {0}", ex.Message);
            }

        }

        [UnitOfWork(isTransactional: false)]
        [AbpAuthorize(AppPermissions.Pages_Tenant_OnlineBooking_Transaction_InsertTrUnitReserved)]
        public InsertTrUnitReservedResultDto InsertTrUnitReserved(InsertTRUnitReservedInputDto input)
        {
            var checkStatusUnit = (from unit in _msUnitRepo.GetAll()
                                   join status in _lkUnitStatus.GetAll()
                                   on unit.unitStatusID equals status.Id
                                   where status.unitStatusCode == "A" && unit.Id == input.unitID
                                   select unit).Count();

            var checkData = (from reserved in _trUnitReservedRepo.GetAll()
                             where reserved.unitID == input.unitID && reserved.releaseDate == null
                             select reserved).FirstOrDefault();

            var checkBooking = (from a in _trBookingHeaderRepo.GetAll()
                                where a.cancelDate == null && a.unitID == input.unitID
                                select a).Count();

            if (checkStatusUnit != 0 && checkData == null && checkBooking == 0)
            {
                var getData = (from unit in _msUnitRepo.GetAll()
                               join term in _msTermRepo.GetAll()
                               on unit.termMainID equals term.termMainID
                               join termMain in _msTermMain.GetAll()
                               on term.termMainID equals termMain.Id
                               where unit.Id == input.unitID && term.Id == input.termID
                               select new
                               {
                                   unitID = unit.Id,
                                   termID = term.Id,
                                   termMain.BFAmount
                               }).FirstOrDefault();

                var getSales = (from user in UserManager.Users
                                join sales in _mpUserPersonals.GetAll()
                                on user.Id equals sales.userID
                                where user.Id == input.userID
                                select new
                                {
                                    user.Id,
                                    sales.userType,
                                    user.UserName,
                                    sales.psCode
                                }).FirstOrDefault();

                var getDisc1 = (from a in _msDiscOnlineBooking.GetAll()
                                where a.projectID == input.projectID
                                select new
                                {
                                    a.discPct,
                                    a.discDesc,
                                }).FirstOrDefault();

                var getDisc2 = (from a in _msTermDiscOnlineBooking.GetAll()
                                where a.termID == input.termID
                                select new
                                {
                                    a.pctDisc,
                                    a.discName
                                }).FirstOrDefault();

                var data = new TR_UnitReserved
                {
                    unitID = getData.unitID,
                    reservedBy = getSales.UserName,
                    renovID = input.renovID,
                    termID = input.termID,
                    SellingPrice = input.sellingPrice,
                    BFAmount = getData.BFAmount,
                    reserveDate = DateTime.Now,
                    releaseDate = null,
                    pscode = null,
                    remarks = null,
                    disc1 = getDisc1 != null ? getDisc1.discPct : 0,
                    disc2 = getDisc2 != null ? getDisc2.pctDisc : 0,
                    specialDiscType = getDisc1 != null ? getDisc1.discDesc : null,
                    groupBU = "-"
                };
                var unitResevedID = _trUnitReservedRepo.InsertAndGetId(data);

                var getUnit = (from unit in _msUnitRepo.GetAll()
                               where unit.Id == input.unitID
                               select unit).FirstOrDefault();

                var updateUnit = getUnit.MapTo<MS_Unit>();

                updateUnit.unitStatusID = 12;

                _msUnitRepo.Update(updateUnit);

                return new InsertTrUnitReservedResultDto { result = true, message = "Success" };
            }
            else
            {
                return new InsertTrUnitReservedResultDto { result = false, message = "Unit is already reserved" };
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_OnlineBooking_Transaction_UpdatePsCodeTrUnitReserved)]
        public List<ResultMessageDto> UpdatePsCodeTrUnitReserved(UpdateTRUnitReserved input)
        {
            var arrResult = new List<ResultMessageDto>();
            foreach (var item in input.unit)
            {
                var getUnit = (from x in _trUnitReservedRepo.GetAll()
                               where x.unitID == item.unitID
                               && x.releaseDate == null
                               select x).FirstOrDefault();
                if (getUnit != null)
                {
                    var updatePsCode = getUnit.MapTo<TR_UnitReserved>();

                    updatePsCode.pscode = input.psCode;

                    _trUnitReservedRepo.Update(updatePsCode);

                    var result = new ResultMessageDto { result = true, message = "Success" };

                    arrResult.Add(result);
                }
                else
                {


                    var result = new ResultMessageDto { result = false, message = "Failed, unit is not exist" };

                    arrResult.Add(result);
                }
            }

            return arrResult;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_OnlineBooking_Transaction_DeleteTrUnitReserved)]
        public ResultMessageDto DeleteTrUnitReserved(int unitReservedID)
        {
            Logger.InfoFormat("DeleteTrUnitReserved() - Start.");

            Logger.DebugFormat("Param TR_UnitReserved. {0}" +
                "unitReservedID         = {1}{0}",
            Environment.NewLine, unitReservedID);

            var checkId = (from a in _trUnitReservedRepo.GetAll()
                           where a.Id == unitReservedID
                           select a).FirstOrDefault();

            Logger.DebugFormat("Target Updating TR_UnitReserved. {0}" +
                "unitReservedID         = {1}{0}",
            Environment.NewLine, checkId.Id);

            if (checkId != null)
            {
                var updateUnitReserved = checkId.MapTo<TR_UnitReserved>();

                updateUnitReserved.releaseDate = DateTime.Now;

                var getUnit = (from unit in _msUnitRepo.GetAll()
                               where unit.Id == checkId.unitID
                               select unit).FirstOrDefault();

                Logger.DebugFormat("Target Updating MS_Unit. {0}" +
                "unitID         = {1}{0}",
            Environment.NewLine, getUnit.Id);

                var getAvailableCode = (from status in _lkUnitStatus.GetAll()
                                        where status.unitStatusCode == "A"
                                        select status.Id).FirstOrDefault();

                var updateUnit = getUnit.MapTo<MS_Unit>();

                updateUnit.unitStatusID = getAvailableCode;

                _msUnitRepo.Update(updateUnit);


                Logger.DebugFormat("Result TR_UnitReserved. {0}" +
                "result         = {1}{0}",
                "message        = {2}{0}",
                Environment.NewLine, true, "Unit was deleted");

                Logger.InfoFormat("BookCode() - End.");

                return new ResultMessageDto
                {
                    message = "Unit was deleted",
                    result = true
                };
            }
            else
            {
                Logger.DebugFormat("Result TR_UnitReserved. {0}" +
                "result         = {1}{0}",
                "message        = {2}{0}",
                Environment.NewLine, false, "Failed");

                Logger.InfoFormat("BookCode() - End.");

                return new ResultMessageDto
                {
                    message = "Failed",
                    result = false
                };
            }

        }

        [UnitOfWork(isTransactional: false)]
        //[AbpAuthorize(AppPermissions.Pages_Tenant_OnlineBooking_Transaction_GetDetailBookingUnit)]
        public ListDetailBookingUnitResultDto GetDetailBookingUnit(int userID, string psCode)
        {
            //get user personal
            var getUserPersonal = (from a in _mpUserPersonals.GetAll()
                                   where a.userID == userID
                                   select a).FirstOrDefault();

            //get member
            var getMember = (from a in _personalsMemberRepo.GetAll()
                             where a.psCode == getUserPersonal.psCode
                             select new
                             {
                                 a.memberCode
                             }).FirstOrDefault();

            //customer detail
            var getCustomer = (from a in _personalsRepo.GetAll()
                               join b in _trIDRepo.GetAll() on a.psCode equals b.psCode
                               join c in _trEmailRepo.GetAll() on a.psCode equals c.psCode
                               where a.psCode == psCode
                               select new GetCustomer
                               {
                                   customerName = a.name,
                                   birthDate = a.birthDate,
                                   idNo = b.idNo,
                                   email = c.email
                               }).FirstOrDefault();

            //unit detail
            var getDetailBooking = (from unitReserved in _trUnitReservedRepo.GetAll()
                                    join term in _msTermRepo.GetAll()
                                    on unitReserved.termID equals term.Id
                                    //REPAIR BENER NGGK?
                                    join renov in _msRenovation.GetAll()
                                    on unitReserved.renovID equals renov.Id
                                    join unit in _msUnitRepo.GetAll()
                                    on unitReserved.unitID equals unit.Id
                                    join cluster in _msClusterRepo.GetAll()
                                    on unit.clusterID equals cluster.Id
                                    join project in _msProjectRepo.GetAll()
                                    on unit.projectID equals project.Id
                                    join unitCode in _msUnitCode.GetAll()
                                    on unit.unitCodeID equals unitCode.Id
                                    where unitReserved.releaseDate == null
                                    && unitReserved.reservedBy == getMember.memberCode
                                    && unitReserved.pscode == psCode
                                    && unitReserved.remarks == null
                                    select new ListUnit
                                    {
                                        clusterID = cluster.Id,
                                        projectID = project.Id,
                                        renovID = renov.Id,
                                        termID = term.Id,
                                        unitID = unit.Id,
                                        clusterName = cluster.clusterName,
                                        unitCode = unitCode.unitCode,
                                        unitNo = unit.unitNo,
                                        projectName = project.projectName,
                                        termName = term.remarks,
                                        renovName = renov.renovationName,
                                        sellingPrice = unitReserved.SellingPrice,
                                        bookingFee = unitReserved.BFAmount
                                    }).ToList();

            //total amount to be paid
            var amount = (from a in _trUnitReservedRepo.GetAll()
                          where a.releaseDate == null
                              && a.reservedBy == getMember.memberCode
                              && a.pscode == psCode
                              && a.remarks == null
                          group a by new
                          {
                              a.pscode
                          } into G
                          select new
                          {
                              sellingPrice = G.Sum(x => x.BFAmount)
                          }
                          ).FirstOrDefault();

            if (getDetailBooking != null)
            {
                var result = new ListDetailBookingUnitResultDto
                {
                    unit = getDetailBooking,
                    amountToBePaid = amount.sellingPrice,
                    customer = getCustomer
                };
                return result;
            }
            else
            {
                var result = new ListDetailBookingUnitResultDto
                {
                    message = "Data not found"
                };
                return result;
            }
        }

        [UnitOfWork(isTransactional: false)]
        //[AbpAuthorize(AppPermissions.Pages_Tenant_OnlineBooking_Transaction_InsertTrUnitOrderHeader)]
        public TrUnitOrderHeaderResultDto InsertTrUnitOrderHeader(CreateTransactionUniversalDto input)
        {
            var getOrderCode = (from a in _trUnitOrderHeader.GetAll()
                                orderby a.orderCode descending
                                select a).FirstOrDefault();

            int order = 0;
            if (getOrderCode != null)
            {
                var getOrder = getOrderCode.orderCode.Substring(5, 7);
                order = Convert.ToInt32(getOrder) + 1;
            }

            var OrderCode = String.Format("{0:LK-OB0000000}", order);

            var userInfo = (from user in _mpUserPersonals.GetAll()
                            where user.userID == input.userID
                            select user.psCode).FirstOrDefault();

            var memberName = (from personal in _personalsRepo.GetAll()
                              join member in _personalsMemberRepo.GetAll()
                              on personal.psCode equals member.psCode
                              join phone in _trPhoneRepo.GetAll()
                              on member.psCode equals phone.psCode into trphone
                              from phone in trphone.DefaultIfEmpty()
                              where member.psCode == userInfo
                              select new { personal, member, phone }).FirstOrDefault();

            var getCustomer = (from a in _personalsRepo.GetAll()
                               join b in _trEmailRepo.GetAll()
                               on a.psCode equals b.psCode into trEmail
                               from b in trEmail.DefaultIfEmpty()
                               join c in _trPhoneRepo.GetAll()
                               on a.psCode equals c.psCode into trPhone
                               from c in trPhone.DefaultIfEmpty()
                               where a.psCode == input.pscode
                               select new
                               {
                                   psName = a.name,
                                   psEmail = b.email,
                                   psPhone = c.number
                               }).FirstOrDefault();

            var getStatus = (from a in _lkBookingOnlineStatus.GetAll()
                             where a.statusType == "1"
                             select a.Id).FirstOrDefault();

            var getPaymentName = (from payment in _lkPaymentType.GetAll()
                                  where payment.Id == input.payTypeID
                                  select payment.paymentTypeName).FirstOrDefault();

            var dataUnitOrderHeader = new TR_UnitOrderHeader
            {
                orderCode = OrderCode,
                orderDate = DateTime.Now,
                psCode = input.pscode,
                psName = getCustomer.psName,
                psEmail = getCustomer.psEmail,
                psPhone = getCustomer.psPhone,
                memberCode = memberName.member.memberCode,
                memberName = memberName.personal.name,
                totalAmt = input.totalAmt,
                paymentTypeID = input.payTypeID,
                //REPAIR OUTSTANDING
                statusID = getStatus,
                userID = input.userID,
                scmCode = memberName.member.scmCode,
                oldOrderCode = string.Empty,
                sumberDanaID = input.sumberDanaID,
                tujuanTransaksiID = input.tujuanTransaksiID,
                bankRekeningPemilik = input.bankRekeningPemilik,
                nomorRekeningPemilik = input.nomorRekeningPemilik
            };
            var id = _trUnitOrderHeader.InsertAndGetId(dataUnitOrderHeader);

            var insertUnitOrderDetail = new TrUnitOrderDetailInputDto
            {
                orderHeaderID = id,
                arrUnit = input.arrUnit,
                userID = input.userID
            };

            InsertTrUnitOrderDetail(insertUnitOrderDetail);

            var insertPaymentOB = new InsertTrPaymentOnlineBookInputDto
            {
                bankAccName = "Not Defined",
                bankAccNo = "Not Defined",
                paymentAmt = input.totalAmt,
                bankName = "Not Defined",
                paymentType = getPaymentName,
                UnitOrderHeaderID = id
            };
            InsertTrPaymentOnlineBook(insertPaymentOB);

            return new TrUnitOrderHeaderResultDto
            {
                scmCode = memberName.member.scmCode,
                memberCode = memberName.member.memberCode,
                memberName = memberName.personal.name,
                unitOrderHeaderID = id,
                orderCode = OrderCode,
                result = true
            };
        }

        //[AbpAuthorize(AppPermissions.Pages_Tenant_OnlineBooking_Transaction_InsertTrUnitOrderDetail)]
        private ResultMessageDto InsertTrUnitOrderDetail(TrUnitOrderDetailInputDto input)
        {
            foreach (var items in input.arrUnit)
            {
                var checkTrUnitReserved = (from x in _trUnitReservedRepo.GetAll()
                                           where x.unitID == items.unitID
                                           && x.releaseDate == null
                                           select x).FirstOrDefault();

                var getDisc1 = (from a in _msDiscOnlineBooking.GetAll()
                                where a.projectID == items.projectID
                                select new
                                {
                                    a.discPct,
                                    a.discDesc,
                                }).FirstOrDefault();

                var getDisc2 = (from a in _msTermDiscOnlineBooking.GetAll()
                                where a.termID == checkTrUnitReserved.termID
                                select new
                                {
                                    a.pctDisc,
                                    a.discName
                                }).FirstOrDefault();

                var remarks = "Selling Price: " + checkTrUnitReserved.SellingPrice + ", Disc1: "
                        + (getDisc1 != null ? getDisc1.discDesc : "0") + ", Disc2: "
                        + (getDisc2 != null ? getDisc2.discName : "0") + ".";

                var dataUnitOrderDetail = new TR_UnitOrderDetail
                {
                    UnitOrderHeaderID = input.orderHeaderID,
                    unitID = checkTrUnitReserved.unitID,
                    renovID = checkTrUnitReserved.renovID,
                    termID = checkTrUnitReserved.termID,
                    BFAmount = items.bfAmount,
                    sellingPrice = items.sellingprice,
                    PPNo = string.Empty,
                    remarks = remarks,
                    disc1 = getDisc1 != null ? getDisc1.discPct : 0,
                    disc2 = getDisc2 != null ? getDisc2.pctDisc : 0,
                    specialDiscType = getDisc1 != null ? getDisc1.discDesc : null,
                    groupBU = string.Empty
                };

                _trUnitOrderDetail.Insert(dataUnitOrderDetail);

                var updateRemarksReserved = new UpdateRemarksReservedInputDto
                {
                    projectID = items.projectID,
                    sellingPrice = items.sellingprice,
                    termID = items.termID,
                    unitID = items.unitID,
                    userID = input.userID
                };
                UpdateRemarksReserved(updateRemarksReserved);
            }

            return new ResultMessageDto
            {
                result = true,
                message = "Success"
            };
        }

        //[AbpAuthorize(AppPermissions.Pages_Tenant_OnlineBooking_Transaction_InsertTrPaymentOnlineBook)]
        private void InsertTrPaymentOnlineBook(InsertTrPaymentOnlineBookInputDto input)
        {
            var data = new TR_PaymentOnlineBook
            {
                bankAccName = input.bankAccName,
                bankAccNo = input.bankAccNo,
                paymentAmt = input.paymentAmt,
                bankName = input.bankName,
                offlineType = input.offlineType,
                paymentDate = DateTime.Now,
                paymentType = input.paymentType,
                resiImage = input.resiImage,
                resiNo = input.resiNo,
                UnitOrderHeaderID = input.UnitOrderHeaderID
            };
            _trPaymentOnlineBook.Insert(data);
        }

        //[UnitOfWork(isTransactional: false)]
        //[AbpAuthorize(AppPermissions.Pages_Tenant_OnlineBooking_Transaction_InsertTrBookingHeader)]
        private TrBookingHeaderResultDto Step1InsertTrBookingHeader(TrBookingHeaderInputDto input)
        {
            var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
            var appUrl = (JObject)appsettingsjson["App"];

            var transCode = appUrl.Property("transCode").Value.ToString();
            var eventCode = appUrl.Property("eventCode").Value.ToString();
            var promotionCode = appUrl.Property("promotionCode").Value.ToString();
            var shopBusinessCode = appUrl.Property("shopBusinessCode").Value.ToString();
            var SADstatusCode = appUrl.Property("SADstatusCode").Value.ToString();
            
            Logger.InfoFormat("Step 1 : Insert to TR_BookingHeader - Start ");

            Logger.DebugFormat("Input Param Step 1. {0}" +
                "userID                 = {1}{0}" +
                "unitID                 = {2}{0}" +
                "psCode                 = {3}{0}" +
                "termID                 = {4}{0}" +
                "sumberDanaID           = {5}{0}" +
                "tujuanTransaksiID      = {6}{0}" +
                "nomorRekeningPemilik   = {7}{0}" +
                "bankRekeningPemilik    = {8}{0}" +
                "sellingPrice           = {9}{0}" +
                "memberCode             = {10}{0}" +
                "memberName             = {11}{0}",

            Environment.NewLine, input.userID, input.unitID,
                input.psCode, input.termID, input.sumberDanaID,
                input.tujuanTransaksiID, input.nomorRekeningPemilik, input.bankRekeningPemilik,
                input.sellingPrice, input.memberCode, input.memberName);

            var unit = (from term in _msTermRepo.GetAll()
                        join units in _msUnitRepo.GetAll()
                        on term.termMainID equals units.termMainID
                        join unitCode in _msUnitCode.GetAll()
                        on units.unitCodeID equals unitCode.Id
                        where term.Id == input.termID
                        && units.Id == input.unitID
                        select new { term, units, unitCode.unitCode }).FirstOrDefault();

            var getProject = (from units in _msUnitRepo.GetAll()
                              join project in _msProject.GetAll()
                              on units.projectID equals project.Id
                              where units.Id == input.unitID
                              select project.projectCode).FirstOrDefault();

            var getTransID = (from x in _msTransFrom.GetAll()
                              where x.transCode == transCode
                              select x).FirstOrDefault();

            var getEvent = (from x in _msSalesEvent.GetAll()
                            where x.eventCode == eventCode
                            select x).FirstOrDefault();

            var getPromotionID = (from a in _lkPromotion.GetAll()
                                  where a.promotionCode == promotionCode
                                  select a).FirstOrDefault();

            var getShopBusinessID = (from a in _msShopBusiness.GetAll()
                                     where a.shopBusinessCode == shopBusinessCode
                                     select a).FirstOrDefault();

            var getSadStatusID = (from a in _lkSadStatus.GetAll()
                                  where a.statusCode == SADstatusCode
                                  select a).FirstOrDefault();

            var bookCode = BookCode(getProject);

            Logger.DebugFormat("Param Insert TR_BookingHeader. {0}" +
                "entityID           = {1}{0}" + //
                "bookCode           = {2}{0}" +//
                "unitID             = {3}{0}" + //
                "bookDate           = {4}{0}" + //
                "scmCode            = {5}{0}" +//
                "memberCode         = {6}{0}" +//
                "memberName         = {7}{0}" +//
                "termID             = {8}{0}" +//
                "PPJBDue            = {9}{0}" +//
                "termRemarks        = {10}{0}" +//
                "NUP                = {11}{0}" +//
                "isSK               = {12}{0}" +//
                "BFPayTypeCode      = {13}{0}" +//
                "bankNo             = {14}{0}" +//
                "bankName           = {15}{0}" +//
                "transID            = {16}{0}" +//
                "discBFCalcType     = {17}{0}" +//
                "DPCalcType         = {18}{0}" +//
                "netPriceComm       = {19}{0}" +//
                "KPRBankCode        = {20}{0}" +//
                "isPenaltyStop      = {21}{0}" +//
                "isSMS              = {22}{0}" +//
                "bankRekeningPemilik    = {23}{0}" +//
                "nomorRekeningPemilik   = {24}{0}" +//
                "sumberDanaID       = {25}{0}" +//
                "tujuanTransaksiID  = {26}{0}" +//
                "eventID            = {27}{0}" +
                "promotionID        = {28}{0}" +
                "shopBusinessID     = {29}{0}" +
                "SADStatusID        = {30}{0}" +
                "remarks            = {31}{0}",

            Environment.NewLine, 1, bookCode, input.unitID, DateTime.Now, input.scmCode, input.memberCode, //0,1,2,3,4,5,6
            input.memberName, input.termID, unit == null ? string.Empty : unit.term.PPJBDue.ToString(),//7,8,9
            unit == null ? string.Empty : unit.term.remarks, string.Empty, false, string.Empty, string.Empty, string.Empty, //10,11,12,13,14,15
            getTransID == null ? string.Empty : getTransID.Id.ToString(), unit == null ? string.Empty : unit.term.discBFCalcType, //16,17
            unit == null ? string.Empty : unit.term.DPCalcType, 0, string.Empty, false, false, input.bankRekeningPemilik, //18,19,20,21,22,23
            input.nomorRekeningPemilik, input.sumberDanaID, input.tujuanTransaksiID, //24,25,26
            getEvent == null ? string.Empty : getEvent.Id.ToString(), getPromotionID == null ? string.Empty : getPromotionID.Id.ToString(),//27,28
            getShopBusinessID == null ? string.Empty : getShopBusinessID.Id.ToString(), //29
            getSadStatusID == null ? string.Empty : getSadStatusID.Id.ToString(), //30
            "-" //31
            );

            var data = new TR_BookingHeader
            {
                entityID = 1,
                bookCode = bookCode,
                unitID = input.unitID,
                bookDate = DateTime.Now,
                psCode = input.psCode,
                scmCode = input.scmCode,
                memberCode = input.memberCode,
                memberName = input.memberName,
                termID = input.termID,
                PPJBDue = unit.term.PPJBDue,
                termRemarks = unit.term.remarks,
                NUP = string.Empty,
                isSK = false,
                BFPayTypeCode = string.Empty,
                bankNo = string.Empty,
                bankName = string.Empty,
                transID = getTransID.Id,
                discBFCalcType = unit.term.discBFCalcType,
                DPCalcType = unit.term.DPCalcType,
                netPriceComm = 0,
                KPRBankCode = string.Empty,
                isPenaltyStop = false,
                isSMS = false,
                bankRekeningPemilik = input.bankRekeningPemilik,
                nomorRekeningPemilik = input.nomorRekeningPemilik,
                sumberDanaID = input.sumberDanaID,
                tujuanTransaksiID = input.tujuanTransaksiID,
                eventID = getEvent.Id,
                promotionID = getPromotionID.Id,
                shopBusinessID = getShopBusinessID.Id,
                SADStatusID = getSadStatusID.Id,
                remarks = "-"
            };
            //PR

            var id = _trBookingHeaderRepo.InsertAndGetId(data);

            Logger.DebugFormat("Return Param TR_BookingHeader. {0}" +
                "bookingHeaderID         = {1}{0}" +
                "bookCode                = {2}{0}" +
                "bookDate                = {3}{0}" +
                "unitID                  = {4}{0}" +
                "termRemarks             = {5}{0}" +
                "termRemarks             = {6}{0}",

            Environment.NewLine, id, true, data.bookCode,
                data.bookDate, data.unitID, data.termRemarks, data.termID);

            Logger.InfoFormat("Step 1 : Insert to TR_BookingHeader - End.");

            return new TrBookingHeaderResultDto
            {
                bookingHeaderID = id,
                result = true,
                message = data.bookCode,
                bookDate = data.bookDate,
                unitID = data.unitID,
                termRemarks = data.termRemarks,
                termID = data.termID
            };

        }

        //[AbpAuthorize(AppPermissions.Pages_Tenant_OnlineBooking_Transaction_InsertTrBookingDetail)]
        private List<bookingDetailIDDto> Step2InsertTrBookingDetail(TrBookingDetailInputDto input)
        {
            Logger.InfoFormat(" Step 2 : Insert to TR_BookingDetail - Start.");

            Logger.DebugFormat("Input Param Step 2. {0}" +
                "unitID                  = {1}{0}" +
                "termID                  = {2}{0}" +
                "bookingHeaderID         = {3}{0}" +
                "renovID                 = {4}{0}",

            Environment.NewLine, input.unitID, input.termID, input.bookingHeaderID, input.renovID);

            var getUnit = (from a in _context.MS_Unit
                           join b in _context.MS_UnitItem on a.Id equals b.unitID
                           join c in _context.MS_UnitItemPrice on b.Id equals c.unitItemID
                           join d in _context.MS_TermPmt on c.termID equals d.termID
                           where a.Id == input.unitID && c.termID == input.termID && c.renovID == input.renovID
                           select new
                           {
                               pctTax = b.pctTax,
                               amount = b.amount,
                               area = b.area,
                               coCode = b.coCode,
                               pctDisc = b.pctDisc,
                               itemID = b.itemID,
                               finTypeID = d.finTypeID,
                               finStartDue = d.finStartDue,
                               entityID = a.entityID
                           }).Distinct().ToList();

            var penampung = new List<bookingDetailIDDto>();

            if (getUnit != null)
            {
                short refno = 0;
                var combineCodes = 0;

                List<string> coCodes = new List<string>();


                Logger.Info("Start Loop Step 2 per Item.");
                foreach (var item in getUnit)
                {
                    Logger.Info("Iteration item: " + item.itemID);

                    var lastBookNo = (from t in _sysBookingCounter.GetAll()
                                      where t.coCode == item.coCode
                                      orderby t.bookNo descending
                                      select t).FirstOrDefault();

                    int bookNo;

                    if (lastBookNo == null)
                    {
                        bookNo = 1;

                        var data = new SYS_BookingCounter
                        {
                            coCode = item.coCode,
                            bookNo = bookNo,
                            BASTNo = 0,
                            entityID = 1
                        };
                        _sysBookingCounter.Insert(data);
                    }
                    else
                    {
                        bookNo = lastBookNo.bookNo + 1;

                        var updateBookNo = lastBookNo.MapTo<SYS_BookingCounter>();

                        updateBookNo.bookNo = bookNo;
                    }
                    refno++;

                    var checkCoCode = coCodes.Where(x => x == item.coCode).Select(x => x).Count();

                    if (checkCoCode == 0)
                    {
                        combineCodes++;

                    }
                    else
                    {
                        combineCodes = 1;

                    }
                    coCodes.Add(item.coCode);

                    var getBookingTrTypeID = (from type in _lkBookingTrType.GetAll()
                                              where type.bookingTrType == "BK"
                                              select type).FirstOrDefault();

                    Logger.DebugFormat("Param Insert TR_BookingDetail. {0}" +
                        "entityID           = {1}{0}" +
                        "bookingHeaderID    = {2}{0}" +
                        "refNo              = {3}{0}" +
                        "itemID             = {4}{0}" +
                        "coCode             = {5}{0}" +
                        "bookNo             = {6}{0}" +
                        "BFAmount           = {7}{0}" +
                        "amount             = {8}{0}" +
                        "pctDisc            = {9}{0}" +
                        "pctTax             = {10}{0}" +
                        "area               = {11}{0}" +
                        "finTypeID          = {12}{0}" +
                        "combineCode        = {13}{0}" +
                        "amountComm         = {14}{0}" +
                        "netPriceComm       = {15}{0}" +
                        "amountMKT          = {16}{0}" +
                        "netPriceMKT        = {17}{0}" +
                        "netPriceCash       = {18}{0}" +
                        "netPrice           = {19}{0}" +
                        "netNetPrice        = {20}{0}" +
                        "adjPrice           = {21}{0}" +
                        "adjArea            = {22}{0}" +
                        "bookingTrTypeID    = {23}{0}",

            Environment.NewLine, 1, input.bookingHeaderID, refno, item.itemID, item.coCode, bookNo, 0, 0,//0,1,2,3,4,5,6,7,8
                item.pctDisc, item.pctTax, item.area, item.finTypeID, combineCodes,//9,10,11,12,13
                0, 0, 0, 0, 0, 0, 0, 0, 0, //14,15,16,17,18,19,20,21,22
                getBookingTrTypeID == null ? string.Empty : getBookingTrTypeID.Id.ToString() //23
                );

                    var detail = new TR_BookingDetail
                    {
                        entityID = item.entityID,
                        bookingHeaderID = input.bookingHeaderID,
                        refNo = refno,
                        itemID = item.itemID,
                        coCode = item.coCode,
                        bookNo = bookNo,
                        BFAmount = 0,
                        amount = 0,
                        pctDisc = item.pctDisc,
                        pctTax = item.pctTax,
                        area = item.area,
                        finTypeID = item.finTypeID,
                        combineCode = combineCodes.ToString(),
                        amountComm = 0,
                        netPriceComm = 0,
                        amountMKT = 0,
                        netPriceMKT = 0,
                        netPriceCash = 0,
                        netPrice = 0,
                        netNetPrice = 0,
                        adjPrice = 0,
                        adjArea = 0,
                        bookingTrTypeID = getBookingTrTypeID.Id
                    };
                    var bookingDetailID = _trBookingDetailRepo.InsertAndGetId(detail);

                    Logger.DebugFormat("Return Param TR_BookingDetail. {0}" +
                        "bookingHeaderID       = {1}{0}" +
                        "bookingDetailID       = {2}{0}" +
                        "itemID                = {3}{0}" +
                        "area                  = {4}{0}" +
                        "bookNo                = {5}{0}" +
                        "coCode                = {6}{0}" +
                        "pctTax                = {7}{0}",

            Environment.NewLine, detail.bookingHeaderID, bookingDetailID, detail.itemID, detail.area, detail.bookNo,//0,1,2,3,4,5
            detail.coCode, detail.pctTax//6,7
                );

                    var result = new bookingDetailIDDto
                    {
                        bookingHeaderID = input.bookingHeaderID,
                        bookingDetailID = bookingDetailID,
                        itemID = item.itemID,
                        result = true,
                        area = item.area,
                        bookNo = bookNo,
                        coCode = item.coCode,
                        pctTax = item.pctTax
                    };
                    penampung.Add(result);
                }

                var log = JsonConvert.SerializeObject(penampung);

                Logger.InfoFormat(log);
            }

            Logger.InfoFormat("Step 2 : Insert to TR_BookingDetail - End.");

            return penampung;
        }

        private List<InsertTrBookingSalesAddDiscResultDto> Step3InsertTrBookingSalesAddDisc(TrBookingSalesAddDiscInputDto input)
        {
            Logger.InfoFormat(" Step 2 : Insert to TR_BookingSalesAddDisc - Start.");

            Logger.DebugFormat("Input Param Step 3. {0}" +
                "unitID                  = {1}{0}" +
                "termID                  = {2}{0}" +
                "bookingHeaderID         = {3}{0}",

            Environment.NewLine, input.unitID, input.termID, input.bookingHeaderID);

            var getSalesDisc = (from a in _msUnitItem.GetAll()
                                join b in _msUnitItemPriceRepo.GetAll()
                                on a.Id equals b.unitItemID
                                join c in _trBookingHeaderRepo.GetAll()
                                on new { a.unitID, b.termID } equals new { c.unitID, c.termID }
                                where a.unitID == input.unitID && b.termID == input.termID && c.Id == input.bookingHeaderID
                                select new
                                {
                                    a.pctDisc,
                                    a.pctTax,
                                    a.itemID,
                                    c.Id
                                }).Distinct().ToList();

            var penampung = new List<InsertTrBookingSalesAddDiscResultDto>();

            Logger.Info("Start Loop Step 3 per Item.");
            foreach (var item in getSalesDisc)
            {
                Logger.Info("Iteration item: " + item.itemID);

                Logger.DebugFormat("Param Insert TR_BookingSalesAddDisc. {0}" +
                "bookingHeaderID  = {1}{0}" +
                "itemID           = {2}{0}" +
                "pctDisc          = {3}{0}" +
                "pctTax           = {4}{0}",

            Environment.NewLine, input.bookingHeaderID, item.itemID, item.pctDisc, item.pctTax);

                var data = new TR_BookingSalesDisc
                {
                    bookingHeaderID = item.Id,
                    itemID = item.itemID,
                    pctDisc = item.pctDisc,
                    pctTax = item.pctTax
                };
                _trBookingSalesDisc.Insert(data);

                Logger.DebugFormat("Return Param TR_BookingSalesAddDisc. {0}" +
                "bookingHeaderID       = {1}{0}" +
                "pctTax                = {2}{0}" +
                "itemID                = {3}{0}",

            Environment.NewLine, item.Id, item.pctTax, item.itemID);

                var result = new InsertTrBookingSalesAddDiscResultDto
                {
                    bookingHeaderID = item.Id,
                    pctTax = item.pctTax,
                    itemID = item.itemID,
                    pctDisc = item.pctDisc
                };
                penampung.Add(result);
            }
            Logger.InfoFormat("Step 3 : Insert to TR_BookingSalesDisc - End.");

            return penampung;

        }

        //[AbpAuthorize(AppPermissions.Pages_Tenant_OnlineBooking_Transaction_InsertAddDisc)]
        private ResultMessageDto Step4InsertAddDisc(InsertAddDiscInputDto input)
        {
            Logger.InfoFormat("Step 4 : Insert to TR_BookingDetailAddDisc, TR_MKTAddDisc, TR_CommAddDisc - Start ");

            Logger.DebugFormat("Input Param Step 1. {0}" +
                "bookingDetailID        = {1}{0}" +
                "termID                 = {2}{0}" +
                "unitID                 = {3}{0}",

            Environment.NewLine, input.bookingDetailID, input.termID, input.unitID);

            var getListUnitAddDisc = (from a in _msUnitRepo.GetAll()
                                      join b in _msUnitItem.GetAll() on a.Id equals b.unitID
                                      join c in _msUnitItemPriceRepo.GetAll() on b.Id equals c.unitItemID
                                      join d in _msTermRepo.GetAll() on c.termID equals d.Id
                                      join e in _msTermAddDiscRepo.GetAll() on d.Id equals e.termID
                                      join f in _trBookingHeaderRepo.GetAll() on a.Id equals f.unitID
                                      join g in _trBookingDetailRepo.GetAll() on f.Id equals g.bookingHeaderID
                                      join h in _msDiscount.GetAll() on e.discountID equals h.Id
                                      where a.Id == input.unitID && c.termID == input.termID
                                      && g.Id == input.bookingDetailID
                                      select new
                                      {
                                          unitNo = a.unitNo,
                                          addDiscNo = e.addDiscNo,
                                          addDisc = e.addDiscPct,
                                          coCode = b.coCode,
                                          bookingDetailID = g.Id,
                                          bookNo = g.bookNo,
                                          entityID = g.entityID,
                                          addDisctAmt = e.addDiscAmt,
                                          addDiscDesc = h.discountName
                                      }).Distinct().ToList();

            var getDisc1 = (from a in _msDiscOnlineBooking.GetAll()
                            join b in _msUnitRepo.GetAll() on a.projectID equals b.projectID into disc
                            from b in disc.DefaultIfEmpty()
                            join c in _msUnitItem.GetAll() on b.Id equals c.unitID
                            join d in _msUnitItemPriceRepo.GetAll() on c.Id equals d.unitItemID
                            join e in _msTermAddDiscRepo.GetAll() on d.termID equals e.termID
                            join f in _trBookingHeaderRepo.GetAll()
                            on b.Id equals f.unitID
                            join g in _trBookingDetailRepo.GetAll()
                            on f.Id equals g.bookingHeaderID
                            where b.Id == input.unitID && d.termID == input.termID && g.Id == input.bookingDetailID
                            select new
                            {
                                addDiscNo = e.addDiscNo,
                                e.addDiscAmt,
                                addDisc = a.discPct,
                                addDiscDesc = a.discDesc,
                                coCode = c.coCode,
                                bookingDetailID = g.Id,
                                bookNo = g.bookNo,
                                entityID = g.entityID
                            }).Distinct().ToList();

            var getDisc2 = (from a in _msTermDiscOnlineBooking.GetAll()
                            join b in _msTermRepo.GetAll()
                            on a.termID equals b.Id
                            join c in _msTermAddDiscRepo.GetAll()
                            on b.Id equals c.termID
                            join d in _msUnitRepo.GetAll()
                            on b.projectID equals d.projectID into msUnit
                            from d in msUnit.DefaultIfEmpty()
                            join e in _msUnitItem.GetAll()
                            on d.Id equals e.unitID
                            join f in _msUnitItemPriceRepo.GetAll()
                            on e.Id equals f.unitItemID
                            join g in _trBookingHeaderRepo.GetAll()
                            on b.Id equals g.unitID
                            join h in _trBookingDetailRepo.GetAll()
                            on g.Id equals h.bookingHeaderID
                            where d.Id == input.unitID && b.Id == input.termID && h.Id == input.bookingDetailID
                            select new
                            {
                                addDiscNo = c.addDiscNo,
                                c.addDiscAmt,
                                addDisc = a.pctDisc,
                                addDiscDesc = a.discName,
                                coCode = e.coCode,
                                bookingDetailID = h.Id,
                                bookNo = h.bookNo,
                                entityID = h.entityID
                            }).Distinct().ToList();

            Logger.Info("Start Loop Step 4 per GetListAddDisc.");
            foreach (var item in getListUnitAddDisc)
            {
                Logger.Info("Iteration addDiscNo: " + item.addDiscNo);

                var AddDisc = new TR_BookingDetailAddDisc
                {
                    entityID = item.entityID,
                    bookingDetailID = item.bookingDetailID,
                    addDiscNo = item.addDiscNo,
                    addDiscDesc = item.addDiscDesc,
                    pctAddDisc = item.addDisc,
                    isAmount = false,
                    amtAddDisc = 0
                };

                _trBookingAddDisc.InsertAsync(AddDisc);

                Logger.DebugFormat("Param Insert TR_BookingDetailAddDisc. {0}" +
                "entityID           = {1}{0}" +
                "bookingDetailID    = {2}{0}" +
                "addDiscNo          = {3}{0}" +
                "addDiscDesc        = {4}{0}" +
                "pctAddDisc         = {5}{0}" +
                "isAmount           = {6}{0}" +
                "amtAddDisc         = {7}{0}",

            Environment.NewLine, 1, AddDisc.bookingDetailID, AddDisc.addDiscNo, AddDisc.addDiscDesc, AddDisc.pctAddDisc,
            AddDisc.isAmount, AddDisc.amtAddDisc);

                var MKTDisc = new TR_MKTAddDisc
                {
                    entityID = item.entityID,
                    bookingDetailID = item.bookingDetailID,
                    pctAddDisc = item.addDisc,
                    addDiscNo = item.addDiscNo,
                    addDiscDesc = item.addDiscDesc,
                    isAmount = item.addDisctAmt == 0 ? false : true,
                    amtAddDisc = 0
                };
                _trMKTAddDisc.InsertAsync(MKTDisc);

                Logger.DebugFormat("Param Insert TR_MKTAddDisc. {0}" +
                "entityID           = {1}{0}" +
                "bookingDetailID    = {2}{0}" +
                "addDiscNo          = {3}{0}" +
                "addDiscDesc        = {4}{0}" +
                "pctAddDisc         = {5}{0}" +
                "isAmount           = {6}{0}" +
                "amtAddDisc         = {7}{0}",

            Environment.NewLine, 1, MKTDisc.bookingDetailID, MKTDisc.addDiscNo, MKTDisc.addDiscDesc, MKTDisc.pctAddDisc,
            MKTDisc.isAmount, MKTDisc.amtAddDisc);

                var CommDisc = new TR_CommAddDisc
                {
                    entityID = item.entityID,
                    bookingDetailID = item.bookingDetailID,
                    pctAddDisc = item.addDisc,
                    addDiscNo = item.addDiscNo,
                    addDiscDesc = item.addDiscDesc,
                    isAmount = item.addDisctAmt == 0 ? false : true,
                    amtAddDisc = 0
                };
                _trCommAddDisc.InsertAsync(CommDisc);

                Logger.DebugFormat("Param Insert TR_CommAddDisc. {0}" +
                "entityID           = {1}{0}" +
                "bookingDetailID    = {2}{0}" +
                "addDiscNo          = {3}{0}" +
                "addDiscDesc        = {4}{0}" +
                "pctAddDisc         = {5}{0}" +
                "isAmount           = {6}{0}" +
                "amtAddDisc         = {7}{0}",

            Environment.NewLine, 1, CommDisc.bookingDetailID, CommDisc.addDiscNo, CommDisc.addDiscDesc, CommDisc.pctAddDisc,
            CommDisc.isAmount, CommDisc.amtAddDisc);
            }

            Logger.Info("Start Loop Step 4 per getDisc1.");
            foreach (var item in getDisc1)
            {
                Logger.Info("Iteration addDiscNo: " + item.addDiscNo);

                var AddDisc = new TR_BookingDetailAddDisc
                {
                    entityID = item.entityID,
                    bookingDetailID = item.bookingDetailID,
                    addDiscNo = item.addDiscNo,
                    addDiscDesc = item.addDiscDesc,
                    pctAddDisc = item.addDisc,
                    isAmount = false,
                    amtAddDisc = 0
                };
                _trBookingAddDisc.InsertAsync(AddDisc);

                Logger.DebugFormat("Param Insert TR_BookingDetailAddDisc. {0}" +
                "entityID           = {1}{0}" +
                "bookingDetailID    = {2}{0}" +
                "addDiscNo          = {3}{0}" +
                "addDiscDesc        = {4}{0}" +
                "pctAddDisc         = {5}{0}" +
                "isAmount           = {6}{0}" +
                "amtAddDisc         = {7}{0}",

            Environment.NewLine, 1, AddDisc.bookingDetailID, AddDisc.addDiscNo, AddDisc.addDiscDesc, AddDisc.pctAddDisc,
            AddDisc.isAmount, AddDisc.amtAddDisc);

                var MKTDisc = new TR_MKTAddDisc
                {
                    entityID = item.entityID,
                    bookingDetailID = item.bookingDetailID,
                    addDiscNo = item.addDiscNo,
                    addDiscDesc = item.addDiscDesc,
                    pctAddDisc = item.addDisc,
                    isAmount = item.addDiscAmt == 0 ? false : true,
                    amtAddDisc = 0
                };
                _trMKTAddDisc.InsertAsync(MKTDisc);

                Logger.DebugFormat("Param Insert TR_MKTAddDisc. {0}" +
                "entityID           = {1}{0}" +
                "bookingDetailID    = {2}{0}" +
                "addDiscNo          = {3}{0}" +
                "addDiscDesc        = {4}{0}" +
                "pctAddDisc         = {5}{0}" +
                "isAmount           = {6}{0}" +
                "amtAddDisc         = {7}{0}",

            Environment.NewLine, 1, MKTDisc.bookingDetailID, MKTDisc.addDiscNo, MKTDisc.addDiscDesc, MKTDisc.pctAddDisc,
            MKTDisc.isAmount, MKTDisc.amtAddDisc);

                var CommDisc = new TR_CommAddDisc
                {
                    entityID = item.entityID,
                    bookingDetailID = item.bookingDetailID,
                    addDiscNo = item.addDiscNo,
                    addDiscDesc = item.addDiscDesc,
                    pctAddDisc = item.addDisc,
                    isAmount = item.addDiscAmt == 0 ? false : true,
                    amtAddDisc = 0
                };
                _trCommAddDisc.InsertAsync(CommDisc);

                Logger.DebugFormat("Param Insert TR_CommAddDisc. {0}" +
                "entityID           = {1}{0}" +
                "bookingDetailID    = {2}{0}" +
                "addDiscNo          = {3}{0}" +
                "addDiscDesc        = {4}{0}" +
                "pctAddDisc         = {5}{0}" +
                "isAmount           = {6}{0}" +
                "amtAddDisc         = {7}{0}",

            Environment.NewLine, 1, CommDisc.bookingDetailID, CommDisc.addDiscNo, CommDisc.addDiscDesc, CommDisc.pctAddDisc,
            CommDisc.isAmount, CommDisc.amtAddDisc);
            }

            Logger.Info("Start Loop Step 4 per getDisc1.");
            foreach (var item in getDisc2)
            {
                Logger.Info("Iteration addDiscNo: " + item.addDiscNo);

                var AddDisc = new TR_BookingDetailAddDisc
                {
                    entityID = item.entityID,
                    bookingDetailID = item.bookingDetailID,
                    addDiscNo = item.addDiscNo,
                    addDiscDesc = item.addDiscDesc,
                    pctAddDisc = item.addDisc,
                    isAmount = false,
                    amtAddDisc = 0
                };
                _trBookingAddDisc.InsertAsync(AddDisc);

                Logger.DebugFormat("Param Insert TR_BookingDetailAddDisc. {0}" +
                "entityID           = {1}{0}" +
                "bookingDetailID    = {2}{0}" +
                "addDiscNo          = {3}{0}" +
                "addDiscDesc        = {4}{0}" +
                "pctAddDisc         = {5}{0}" +
                "isAmount           = {6}{0}" +
                "amtAddDisc         = {7}{0}",

                 Environment.NewLine, 1, AddDisc.bookingDetailID, AddDisc.addDiscNo, AddDisc.addDiscDesc, AddDisc.pctAddDisc,
                 AddDisc.isAmount, AddDisc.amtAddDisc);

                var MKTDisc = new TR_MKTAddDisc
                {
                    entityID = item.entityID,
                    bookingDetailID = item.bookingDetailID,
                    addDiscNo = item.addDiscNo,
                    addDiscDesc = item.addDiscDesc,
                    pctAddDisc = item.addDisc,
                    isAmount = false,
                    amtAddDisc = 0

                };
                _trMKTAddDisc.InsertAsync(MKTDisc);

                Logger.DebugFormat("Param Insert TR_MKTAddDisc. {0}" +
                "entityID           = {1}{0}" +
                "bookingDetailID    = {2}{0}" +
                "addDiscNo          = {3}{0}" +
                "addDiscDesc        = {4}{0}" +
                "pctAddDisc         = {5}{0}" +
                "isAmount           = {6}{0}" +
                "amtAddDisc         = {7}{0}",

            Environment.NewLine, 1, MKTDisc.bookingDetailID, MKTDisc.addDiscNo, MKTDisc.addDiscDesc, MKTDisc.pctAddDisc,
            MKTDisc.isAmount, MKTDisc.amtAddDisc);

                var CommDisc = new TR_CommAddDisc
                {
                    entityID = item.entityID,
                    bookingDetailID = item.bookingDetailID,
                    addDiscNo = item.addDiscNo,
                    addDiscDesc = item.addDiscDesc,
                    pctAddDisc = item.addDisc,
                    isAmount = false,
                    amtAddDisc = 0
                };
                _trCommAddDisc.InsertAsync(CommDisc);

                Logger.DebugFormat("Param Insert TR_CommAddDisc. {0}" +
                "entityID           = {1}{0}" +
                "bookingDetailID    = {2}{0}" +
                "addDiscNo          = {3}{0}" +
                "addDiscDesc        = {4}{0}" +
                "pctAddDisc         = {5}{0}" +
                "isAmount           = {6}{0}" +
                "amtAddDisc         = {7}{0}",

            Environment.NewLine, 1, CommDisc.bookingDetailID, CommDisc.addDiscNo, CommDisc.addDiscDesc, CommDisc.pctAddDisc,
            CommDisc.isAmount, CommDisc.amtAddDisc);
            }

            Logger.InfoFormat("Step 4 : Insert to TR_BookingDetailAddDisc, TR_MKTAddDisc, TR_CommAddDisc - End.");

            return new ResultMessageDto { result = true };
        }

        //[AbpAuthorize(AppPermissions.Pages_Tenant_OnlineBooking_Transaction_InsertTrBookingDetailDP)]
        private List<InsertTRDetailDPResultDto> Step5InsertTrBookingDetailDP(TrBookingDetailDPInputDto input)
        {
            Logger.InfoFormat("Step 1 : Insert to TR_BookingDetailDP - Start ");

            Logger.DebugFormat("Input Param Step 1. {0}" +
                "bookingDetailID        = {1}{0}" +
                "unitID                 = {2}{0}" +
                "termID                 = {3}{0}",

            Environment.NewLine, input.bookingDetailID, input.termID, input.unitID);

            var getTermDP = (from term in _msTermRepo.GetAll()
                             join termDP in _msTermDP.GetAll()
                             on term.Id equals termDP.termID
                             join termPmt in _msTermPmt.GetAll()
                             on term.Id equals termPmt.termID
                             join unitItemPrice in _msUnitItemPriceRepo.GetAll() on term.Id equals unitItemPrice.termID
                             join unitItem in _msUnitItem.GetAll() on unitItemPrice.unitItemID equals unitItem.Id
                             join unit in _msUnitRepo.GetAll()
                             on unitItem.unitID equals unit.Id
                             join bookingHeader in _trBookingHeaderRepo.GetAll()
                             on termPmt.termID equals bookingHeader.termID
                             join bookingDetail in _trBookingDetailRepo.GetAll()
                             on bookingHeader.Id equals bookingDetail.bookingHeaderID
                             where term.Id == input.termID && unit.Id == input.unitID && bookingDetail.Id == input.bookingDetailID
                             select new GetListTermDPResultDto
                             {
                                 daysDue = termDP.daysDue,
                                 DPNo = termDP.DPNo,
                                 DPPct = termDP.DPPct,
                                 DPAmount = termDP.DPAmount,
                                 bookingDetailID = bookingDetail.Id,
                                 entityID = bookingDetail.entityID
                             }).Distinct().ToList();

            var FormulaCalcID = (from a in _lkFormulaDP.GetAll()
                                 where a.formulaDPType == "1"
                                 select a).FirstOrDefault();

            var arrDetailDP = new List<InsertTRDetailDPResultDto>();

            Logger.Info("Start Loop Step 5 per DPNo.");
            foreach (var item in getTermDP)
            {
                Logger.Info("Iteration DPNo: " + item.DPNo);

                var detailDP = new TR_BookingDetailDP
                {
                    entityID = item.entityID,
                    bookingDetailID = item.bookingDetailID,
                    dpNo = item.DPNo,
                    daysDue = item.daysDue,
                    DPPct = item.DPPct,
                    DPAmount = 0,
                    formulaDPID = FormulaCalcID == null ? (int?)null : FormulaCalcID.Id,
                    isSetting = true
                };
                _trBookingDetailDP.Insert(detailDP);

                Logger.DebugFormat("Param Insert TR_BookingDetailDP. {0}" +
                    "entityID           = {1}{0}" +
                    "bookingDetailID    = {2}{0}" +
                    "dpNo               = {3}{0}" +
                    "daysDue            = {4}{0}" +
                    "DPPct              = {5}{0}" +
                    "DPAmount           = {6}{0}" +
                    "formulaDPID        = {7}{0}" +
                    "isSetting          = {8}{0}",

        Environment.NewLine, detailDP.entityID, detailDP.bookingDetailID, detailDP.dpNo, detailDP.daysDue, detailDP.DPPct,
        detailDP.DPAmount, detailDP.formulaDPID, detailDP.isSetting);

                Logger.DebugFormat("Return Param TR_BookingDetailDP. {0}" +
            "DPNo         = {1}{0}" +
            "daysDue      = {2}{0}" +
            "DPPct        = {3}{0}" +
            "DPAmount     = {4}{0}" +
            "formulaCalcID  = {5}{0}",

        Environment.NewLine, item.DPNo, item.daysDue, item.DPPct,
            0, FormulaCalcID == null ? (int?)null : FormulaCalcID.Id);

                var detailDPResult = new InsertTRDetailDPResultDto
                {
                    DPNo = item.DPNo,
                    daysDue = item.daysDue,
                    DPPct = item.DPPct,
                    DPAmount = 0,
                    formulaCalcID = FormulaCalcID.Id
                };
                arrDetailDP.Add(detailDPResult);
            }

            Logger.InfoFormat("Step 5 : Insert to TR_BookingDetailDP - End.");

            return arrDetailDP;

        }

        private ResultMessageDto Step6InsertTrCashAddDisc(TrCashAddDiscInputDto input)
        {

            Logger.InfoFormat("Step 6 : Insert to TR_CashAddDisc - Start ");

            Logger.DebugFormat("Input Param Step 1. {0}" +
                "bookingDetailID        = {1}{0}" +
                "unitID                 = {2}{0}" +
                "termID                 = {3}{0}",

            Environment.NewLine, input.bookingDetailID, input.termID, input.unitID);

            var checkTerm = (from a in _msTermRepo.GetAll()
                             where a.Id == input.termID
                             select a.termNo).FirstOrDefault();

            if (checkTerm == 1)
            {
                var getDisc = (from a in _msUnitItemPriceRepo.GetAll()
                               join b in _msUnitItem.GetAll()
                               on a.unitItemID equals b.Id
                               join c in _msTermRepo.GetAll()
                               on a.termID equals c.Id
                               join d in _msTermAddDiscRepo.GetAll()
                               on c.Id equals d.termID
                               join e in _trBookingHeaderRepo.GetAll()
                               on b.unitID equals e.unitID
                               join f in _trBookingDetailRepo.GetAll()
                               on e.Id equals f.bookingHeaderID
                               join g in _msDiscount.GetAll()
                               on d.discountID equals g.Id
                               where b.unitID == input.unitID && a.termID == input.termID && f.Id == input.bookingDetailID
                               select new
                               {
                                   addDisc = d.addDiscPct,
                                   addDiscNo = d.addDiscNo,
                                   coCOde = b.coCode,
                                   bookingDetailID = f.Id,
                                   bookNo = f.bookNo,
                                   entityID = f.entityID,
                                   addDiscDesc = g.discountName
                               }).FirstOrDefault();

                if (getDisc != null)
                {
                    Logger.DebugFormat("Param Insert TR_CashAddDisc. {0}" +
                "pctAddDisc           = {1}{0}" +
                "addDiscNo            = {2}{0}" +
                "bookingDetailID      = {3}{0}" +
                "entityID             = {4}{0}" +
                "addDiscDesc          = {5}{0}" +
                "amtAddDisc           = {6}{0}",

            Environment.NewLine, getDisc.addDisc, getDisc.addDiscNo, getDisc.bookingDetailID, getDisc.entityID,
            getDisc.addDiscDesc, 0);

                    var data = new TR_CashAddDisc
                    {
                        pctAddDisc = getDisc.addDisc,
                        addDiscNo = getDisc.addDiscNo,
                        bookingDetailID = getDisc.bookingDetailID,
                        entityID = getDisc.entityID,
                        addDiscDesc = getDisc.addDiscDesc,
                        amtAddDisc = 0
                    };
                    _trCashAddDisc.Insert(data);
                }
            }

            Logger.InfoFormat("Step 6 : Insert to TR_CashAddDisc - End.");

            return new ResultMessageDto { result = true };
        }

        //[AbpAuthorize(AppPermissions.Pages_Tenant_OnlineBooking_Transaction_InsertTrBookingTax)]
        private ResultMessageDto Step7InsertTrBookingTax(InsertTrBookingTaxInputDto input)
        {
            Logger.InfoFormat("Step 7 : Insert to TR_BookingTax - Start ");

            Logger.DebugFormat("Input Param Step 7. {0}" +
                "bookingDetailID        = {1}{0}" +
                "unitID                 = {2}{0}" +
                "termID                 = {3}{0}" +
                "netNetPrice            = {3}{0}" +
                "sellingPrice           = {3}{0}",

            Environment.NewLine, input.bookingDetailID, input.termID, input.unitID, input.netNetPrice, input.sellingPrice);

            var checkCategory = (from unit in _msUnitRepo.GetAll()
                                 join category in _msCategory.GetAll()
                                 on unit.categoryID equals category.Id
                                 where unit.Id == input.unitID
                                 select category.categoryCode).FirstOrDefault();

            var getArea = (from unit in _msUnitRepo.GetAll()
                           join unitItem in _msUnitItem.GetAll()
                           on unit.Id equals unitItem.unitID
                           where unit.Id == input.unitID && unitItem.itemID == 2
                           select unitItem.area).FirstOrDefault();

            var getTerm = (from term in _msTermRepo.GetAll()
                           where term.Id == input.termID
                           select term.termNo).FirstOrDefault();

            var getTaxType = (from taxType in _msTaxType.GetAll()
                              where taxType.taxTypeCode.ToLowerInvariant() == "pph22"
                              select taxType.Id).FirstOrDefault();
            decimal tax = 0;
            if ((checkCategory.ToLowerInvariant() == "lan" && input.sellingPrice >= 5000000000 && getArea >= 400 && getTerm == 1) ||
                (checkCategory.ToLowerInvariant() == "kdm" && input.sellingPrice >= 5000000000 && getArea >= 150 && getTerm == 1))
            {
                tax = input.netNetPrice * (decimal)0.05;
            }

            Logger.DebugFormat("Param Insert TR_BookingTax. {0}" +
                "amount             = {1}{0}" +
                "bookingDetailID    = {2}{0}",

            Environment.NewLine, tax, input.bookingDetailID, getTaxType);

            var data = new TR_BookingTax
            {
                amount = tax,
                bookingDetailID = input.bookingDetailID,
                taxTypeID = getTaxType
            };
            _trBookingTax.Insert(data);

            Logger.InfoFormat("Step 7 : Insert to TR_BookingTax - End.");

            return new ResultMessageDto
            {
                message = "Success",
                result = true
            };
        }

        private List<UpdateBfAmoutResultDto> Step8UpdateBFAmount(UpdateBFAmountInputDto input)
        {
            Logger.InfoFormat("Step 8 : Update to TR_BookingDetail - Start ");

            Logger.DebugFormat("Input Param Step 8. {0}" +
                "unitID             = {1}{0}",

            Environment.NewLine, input.unitID);

            var arrBfAmount = new List<UpdateBfAmoutResultDto>();

            var getNetPrice = (from a in _msUnitRepo.GetAll()
                               join b in _msTermMain.GetAll() on a.termMainID equals b.Id
                               join c in _msTermRepo.GetAll() on b.Id equals c.termMainID
                               join d in _msUnitItem.GetAll() on a.Id equals d.unitID
                               join e in _msUnitItemPriceRepo.GetAll() on new { unitItemID = d.Id, termID = c.Id } equals new { unitItemID = e.unitItemID, termID = e.termID }
                               where a.Id == input.unitID && c.termNo == 3 && e.renovID == input.renovID
                               //&& (c.remarks.ToLowerInvariant() == "cicilan 12 kali" || c.remarks.ToLowerInvariant() == "cicilan 12x")
                               orderby d.itemID ascending
                               select new
                               {
                                   BFAmount = b.BFAmount,
                                   grossPrice = e.grossPrice,
                                   itemID = d.itemID,
                                   remarks = c.remarks
                               }).Distinct().ToList();

            var totalGrossPrice = getNetPrice.Sum(s => s.grossPrice);
            Logger.Info("Start Loop Step 8 per itemID.");
            foreach (var item in getNetPrice)
            {
                Logger.Info("Iteration itemID: " + item.itemID);

                var BFAlloc = Math.Round((item.grossPrice / totalGrossPrice) * item.BFAmount, 2);

                var getBookingDetail = (from a in _trBookingHeaderRepo.GetAll()
                                        join b in _trBookingDetailRepo.GetAll() on a.Id equals b.bookingHeaderID
                                        where a.unitID == input.unitID && b.itemID == item.itemID
                                        select b).FirstOrDefault();

                var updateBookingDetail = getBookingDetail.MapTo<TR_BookingDetail>();

                updateBookingDetail.BFAmount = BFAlloc;

                _trBookingDetailRepo.Update(updateBookingDetail);

                Logger.DebugFormat("Return Param TR_BookingDetail. {0}" +
                "bfAmount         = {1}{0}",

                Environment.NewLine, BFAlloc);

                var bfAmount = new UpdateBfAmoutResultDto
                {
                    bfAmount = BFAlloc
                };
                arrBfAmount.Add(bfAmount);
            }

            Logger.InfoFormat("Step 8 : Update to TR_BookingDetail - End.");

            return arrBfAmount;
        }

        private List<InsertTrBookingHeaderTermResultDto> Step9InsertTrBookingHeaderTerm(InsertTrBookingHeaderTermInputDto input)
        {
            Logger.InfoFormat("Step 9 : Insert to TR_BookingHeaderTerm - Start ");

            Logger.DebugFormat("Input Param Step 9. {0}" +
                "unitID                 = {1}{0}",

            Environment.NewLine, input.unitID);

            var getTerm = (from a in _msUnitRepo.GetAll()
                           join b in _msTermMain.GetAll() on a.termMainID equals b.Id
                           join c in _msTermRepo.GetAll() on b.Id equals c.termMainID
                           //join d in _msTermAddDiscRepo.GetAll() on c.Id equals d.termID
                           join e in _msTermPmt.GetAll() on c.Id equals e.termID
                           join f in _trBookingHeaderRepo.GetAll() on a.Id equals f.unitID
                           join g in _lkFinType.GetAll() on e.finTypeID equals g.Id
                           where a.Id == input.unitID
                           select new
                           {
                               c.DPCalcType,
                               c.PPJBDue,
                               c.discBFCalcType,
                               //d.addDiscPct,
                               //d.addDiscNo,
                               bookingHeaderID = f.Id,
                               e.finStartDue,
                               termID = c.Id,
                               g.finTypeCode,
                               c.remarks
                           }).ToList();

            var penampung = new List<InsertTrBookingHeaderTermResultDto>();

            Logger.Info("Start Loop Step 9 per TermID.");
            foreach (var item in getTerm)
            {
                Logger.Info("Iteration TermID: " + item.termID);

                var getTermAddDisc = (from a in _msTermAddDiscRepo.GetAll()
                                      where a.termID == item.termID
                                      select a).Distinct().ToList();

                if (getTermAddDisc.Count() == 0)
                {
                    Logger.DebugFormat("Param Insert TR_BookingHeaderTerm. {0}" +
                       "DPCalcType        = {1}{0}" +
                       "PPJBDue           = {2}{0}" +
                       "addDiscNo         = {3}{0}" +
                       "addDisc           = {4}{0}" +
                       "bookingHeaderID   = {5}{0}" +
                       "discBFCalcType    = {6}{0}" +
                       "finStartDue       = {7}{0}" +
                       "finTypeCode       = {8}{0}" +
                       "remarks           = {9}{0}" +
                       "termID            = {10}{0}",

           Environment.NewLine, item.DPCalcType, item.PPJBDue, 0, 0, item.bookingHeaderID,
           item.discBFCalcType, item.finStartDue, item.finTypeCode, item.remarks, item.termID);

                    var data = new TR_BookingHeaderTerm
                    {
                        DPCalcType = item.DPCalcType,
                        PPJBDue = item.PPJBDue,
                        addDiscNo = 0,
                        addDisc = 0,
                        bookingHeaderID = item.bookingHeaderID,
                        discBFCalcType = item.discBFCalcType,
                        finStartDue = item.finStartDue,
                        finTypeCode = item.finTypeCode,
                        remarks = item.remarks,
                        termID = item.termID,

                    };
                    _trBookingHeaderTerm.Insert(data);
                }
                else
                {
                    Logger.Info("Start Loop Step 9 per AddDisc.");
                    foreach (var addDisc in getTermAddDisc)
                    {
                        Logger.Info("Iteration addDiscNo: " + addDisc.addDiscNo);

                        Logger.DebugFormat("Param Insert TR_BookingHeaderTerm. {0}" +
                           "DPCalcType        = {1}{0}" +
                           "PPJBDue           = {2}{0}" +
                           "addDiscNo         = {3}{0}" +
                           "addDisc           = {4}{0}" +
                           "bookingHeaderID   = {5}{0}" +
                           "discBFCalcType    = {6}{0}" +
                           "finStartDue       = {7}{0}" +
                           "finTypeCode       = {8}{0}" +
                           "remarks           = {9}{0}" +
                           "termID            = {10}{0}",

               Environment.NewLine, item.DPCalcType, item.PPJBDue, addDisc.addDiscNo, addDisc.addDiscPct, item.bookingHeaderID,
               item.discBFCalcType, item.finStartDue, item.finTypeCode, item.remarks, item.termID);

                        var data = new TR_BookingHeaderTerm
                        {
                            DPCalcType = item.DPCalcType,
                            PPJBDue = item.PPJBDue,
                            addDiscNo = addDisc.addDiscNo,
                            addDisc = addDisc.addDiscPct,
                            bookingHeaderID = item.bookingHeaderID,
                            discBFCalcType = item.discBFCalcType,
                            finStartDue = item.finStartDue,
                            finTypeCode = item.finTypeCode,
                            remarks = item.remarks,
                            termID = item.termID,

                        };
                        _trBookingHeaderTerm.Insert(data);
                    }
                }

                Logger.DebugFormat("Return Param TR_BookingHeaderTerm. {0}" +
                "bookingHeaderID         = {1}{0}" +
                "remarks                 = {2}{0}" +
                "termID                  = {3}{0}",

            Environment.NewLine, item.bookingHeaderID, item.remarks, item.termID);


                var result = new InsertTrBookingHeaderTermResultDto
                {
                    bookingHeaderID = item.bookingHeaderID,
                    remarks = item.remarks,
                    termID = item.termID
                };
                penampung.Add(result);
            }

            Logger.InfoFormat("Step 9 : Insert to TR_BookingHeader - End.");
            return penampung;
        }

        private List<InsertTrBookingItemPriceResultDto> Step10InsertTrBookingItemPrice(InsertTrBookingHeaderTermInputDto input)
        {
            Logger.InfoFormat("Step 10 : Insert to TR_BookingItemPrice - Start ");

            Logger.DebugFormat("Input Param Step 10. {0}" +
                "unitID                 = {1}{0}",
            Environment.NewLine, input.unitID);

            var getItemPrice = (from a in _msUnitItemPriceRepo.GetAll()
                                join c in _msUnitItem.GetAll() on a.unitItemID equals c.Id
                                join b in _trBookingHeaderRepo.GetAll() on c.unitID equals b.unitID
                                join d in _msRenovation.GetAll() on a.renovID equals d.Id
                                where c.unitID == input.unitID
                                select new
                                {
                                    a.entityID,
                                    a.grossPrice,
                                    c.itemID,
                                    a.termID,
                                    c.unitID,
                                    b.Id,
                                    d.renovationCode
                                }).Distinct().ToList();

            var penampung = new List<InsertTrBookingItemPriceResultDto>();

            Logger.Info("Start Loop Step 10 per UnitItemPrice.");
            foreach (var item in getItemPrice)
            {
                Logger.Info("Iteration UnitID: " + item.unitID + "itemID: " + item.itemID + "TermID: " + item.termID);


                Logger.DebugFormat("Param Insert TR_BookingItemPrice. {0}" +
                "bookingHeaderID    = {1}{0}" +
                "entityID           = {2}{0}" +
                "itemID             = {3}{0}" +
                "grossPrice         = {4}{0}" +
                "renovCode          = {5}{0}" +
                "termID             = {6}{0}",

            Environment.NewLine, item.Id, item.entityID, item.itemID, item.grossPrice, item.renovationCode, item.termID);

                var data = new TR_BookingItemPrice
                {
                    bookingHeaderID = item.Id,
                    entityID = item.entityID,
                    itemID = item.itemID,
                    grossPrice = item.grossPrice,
                    renovCode = item.renovationCode,
                    termID = item.termID
                };
                _trBookingItemPrice.Insert(data);

                Logger.DebugFormat("Return Param TR_BookingItemPrice. {0}" +
                "bookingHeaderID         = {1}{0}" +
                "grossPrice              = {2}{0}" +
                "itemID                  = {3}{0}" +
                "termID                  = {4}{0}",

            Environment.NewLine, item.Id, item.grossPrice, item.itemID, item.termID);


                var result = new InsertTrBookingItemPriceResultDto
                {
                    bookingHeaderID = item.Id,
                    grossPrice = item.grossPrice,
                    itemID = item.itemID,
                    termID = item.termID
                };
                penampung.Add(result);
            }

            Logger.InfoFormat("Step 10 : Insert to TR_BookingItemPrice - End.");
            return penampung;

        }

        //[AbpAuthorize(AppPermissions.Pages_Tenant_OnlineBooking_Transaction_UpdateUnitSold)]
        private ResultMessageDto Step11UpdateUnitSold(InsertTrBookingHeaderTermInputDto input)
        {
            Logger.InfoFormat("Step 11 : Update Status Sold MS_Unit - Start ");

            var getSoldStatusID = (from a in _lkUnitStatus.GetAll()
                                   where a.unitStatusCode == "S"
                                   select a).FirstOrDefault();

            Logger.InfoFormat("SoldStatusID = " + getSoldStatusID == null ? string.Empty : getSoldStatusID.Id.ToString());

            var getUnit = (from a in _msUnitRepo.GetAll()
                           where a.Id == input.unitID
                           select a).FirstOrDefault();

            if (getUnit != null)
            {
                var updateUnit = getUnit.MapTo<MS_Unit>();

                updateUnit.unitStatusID = getSoldStatusID.Id;

                _msUnitRepo.Update(updateUnit);

                Logger.InfoFormat("Step 11 : Update Status Sold MS_Unit - End. ");

                return new ResultMessageDto { result = true };
            }
            else
            {
                return new ResultMessageDto { result = false, message = "Data not found" };
            }
        }

        //[AbpAuthorize(AppPermissions.Pages_Tenant_OnlineBooking_Transaction_UpdateNetPriceBookingHeaderDetail)]
        private List<UpdateNetPriceResultDto> Step12UpdateNetPriceBookingHeaderDetail(UpdateBookingDetailInputDto input)
        {
            Logger.InfoFormat("Step 12 : Update NetPriceComm TR_BookingHeader, Price TR_BookingDetail - Start ");

            var getBookingItemPrice = input.listBookingItemPrice.ToList();
            var getSalesDisc = input.listSalesDisc.ToList();
            var getHeaderTerm = input.listBookingHeaderTerm.ToList();
            var getBookingDetail = input.listBookingDetail.ToList();
            var penampung = new List<UpdateNetPriceResultDto>();

            var getUpdateBookingDetail = (from itemPrice in getBookingItemPrice
                                          join salesDisc in getSalesDisc
                                          on new { itemPrice.bookingHeaderID, itemPrice.itemID }
                                          equals new { salesDisc.bookingHeaderID, salesDisc.itemID }
                                          join headerTerm in getHeaderTerm
                                          on itemPrice.termID equals headerTerm.termID
                                          join bookingDetail in getBookingDetail
                                          on new { itemPrice.bookingHeaderID, itemPrice.itemID }
                                          equals new { bookingDetail.bookingHeaderID, bookingDetail.itemID }
                                          //join baru
                                          join term in _msTermRepo.GetAll()
                                          on itemPrice.termID equals term.Id
                                          where itemPrice.bookingHeaderID == input.bookingHeaderID
                                          && term.termNo == 3
                                          orderby itemPrice.itemID ascending
                                          select new
                                          {
                                              itemPrice.grossPrice,
                                              salesDisc.pctTax,
                                              headerTerm.remarks,
                                              bookingDetail.bookingDetailID,
                                              salesDisc.pctDisc,
                                              itemPrice.termID
                                          }).ToList();

            var arrNetPriceComm = new List<ListNetPriceCommResultDto>();

            var netPrice = new ListNetPriceCommResultDto();

            Logger.Info("Start Loop Step 12(Update netPriceComm, netPriceMKT, amountComm, amountMKT) per BookingDetailID.");
            foreach (var item in getUpdateBookingDetail)
            {
                Logger.Info("Iteration bookingDetailID: " + item.bookingDetailID);

                var getBookDetail = (from a in _trBookingDetailRepo.GetAll()
                                     where a.bookingHeaderID == input.bookingHeaderID && a.Id == item.bookingDetailID
                                     orderby a.itemID ascending
                                     select a).FirstOrDefault();

                var updateBookingDetail = getBookDetail.MapTo<TR_BookingDetail>();

                var netPriceComm = item.grossPrice - (item.grossPrice * (decimal)item.pctDisc);

                Logger.DebugFormat("Param Update netPriceComm, netPriceMKT, amountComm, amountMKT TR_BookingDetail. {0}" +
                "netPriceComm          = {1}{0}" +
                "netPriceMKT           = {2}{0}" +
                "amountComm            = {3}{0}" +
                "amountMKT             = {4}{0}",

            Environment.NewLine, netPriceComm, netPriceComm, item.grossPrice, item.grossPrice);

                updateBookingDetail.netPriceComm = netPriceComm;
                updateBookingDetail.netPriceMKT = netPriceComm;
                updateBookingDetail.amountComm = item.grossPrice;
                updateBookingDetail.amountMKT = item.grossPrice;

                _trBookingDetailRepo.Update(updateBookingDetail);

                netPrice = new ListNetPriceCommResultDto
                {
                    netPriceComm = item.grossPrice - (item.grossPrice * (decimal)item.pctDisc)
                };
                arrNetPriceComm.Add(netPrice);
            }

            var getUpdateNetPrice = (from itemPrice in getBookingItemPrice
                                     join salesDisc in getSalesDisc
                                     on new { itemPrice.bookingHeaderID, itemPrice.itemID }
                                     equals new { salesDisc.bookingHeaderID, salesDisc.itemID }
                                     join headerTerm in getHeaderTerm
                                     on itemPrice.termID equals headerTerm.termID
                                     join bookingDetail in getBookingDetail
                                     on new { itemPrice.bookingHeaderID, itemPrice.itemID }
                                     equals new { bookingDetail.bookingHeaderID, bookingDetail.itemID }
                                     where itemPrice.bookingHeaderID == input.bookingHeaderID
                                     && headerTerm.termID == input.termID
                                     orderby itemPrice.itemID ascending
                                     select new
                                     {
                                         itemPrice.grossPrice,
                                         salesDisc.pctTax,
                                         headerTerm.remarks,
                                         bookingDetail.bookingDetailID,
                                         salesDisc.pctDisc,
                                         itemPrice.termID
                                     }).Distinct().ToList();

            Logger.Info("Start Loop Step 12(Update netPrice, amount, netNetPrice) per BookingDetailID.");
            foreach (var items in getUpdateNetPrice)
            {
                Logger.Info("Iteration bookingDetailID: " + items.bookingDetailID);

                //var getDisc = (from a in _trUnitOrderDetail.GetAll()
                //               join b in _trBookingHeaderRepo.GetAll() on new { a.unitID, a.termID } equals new { b.unitID, b.termID }
                //               where b.Id == input.bookingHeaderID
                //               select new
                //               {
                //                   a.disc1,
                //                   a.disc2
                //               }).FirstOrDefault();

                var getBookDetail = (from a in _trBookingDetailRepo.GetAll()
                                     where a.bookingHeaderID == input.bookingHeaderID && a.Id == items.bookingDetailID
                                     orderby a.itemID ascending
                                     select a).FirstOrDefault();

                var updateBookingDetail = getBookDetail.MapTo<TR_BookingDetail>();

                var netNetPrice = items.grossPrice * (1 - (decimal)items.pctDisc);

                var getAddDisc = (from a in _msTermAddDiscRepo.GetAll()
                                  where a.termID == items.termID
                                  select a.addDiscPct).ToList();

                foreach (var item in getAddDisc)
                {
                    netNetPrice = netNetPrice * (1 - (decimal)item);
                }


                Logger.DebugFormat("Param Update netPrice, amount, netNetPrice TR_BookingDetail. {0}" +
                "netPrice          = {1}{0}" +
                "amount            = {2}{0}" +
                "netNetPrice       = {3}{0}",

            Environment.NewLine, items.grossPrice - (items.grossPrice * (decimal)items.pctDisc),
            items.grossPrice, netNetPrice);

                updateBookingDetail.netPrice = items.grossPrice - (items.grossPrice * (decimal)items.pctDisc);
                updateBookingDetail.amount = items.grossPrice;
                updateBookingDetail.netNetPrice = netNetPrice;

                var getArrNetNetPrice = new UpdateNetPriceResultDto
                {
                    result = true,
                    netNetPrice = netNetPrice,
                    bookingDetailID = items.bookingDetailID
                };
                penampung.Add(getArrNetNetPrice);
            }

            var getUpdateNetPriceCash = (from itemPrice in getBookingItemPrice
                                         join salesDisc in getSalesDisc
                                         on new { itemPrice.bookingHeaderID, itemPrice.itemID }
                                         equals new { salesDisc.bookingHeaderID, salesDisc.itemID }
                                         join headerTerm in getHeaderTerm
                                         on itemPrice.termID equals headerTerm.termID
                                         join bookingDetail in getBookingDetail
                                         on new { itemPrice.bookingHeaderID, itemPrice.itemID }
                                         equals new { bookingDetail.bookingHeaderID, bookingDetail.itemID }
                                         join term in _msTermRepo.GetAll()
                                         on itemPrice.termID equals term.Id
                                         where itemPrice.bookingHeaderID == input.bookingHeaderID
                                         && term.termNo == 3
                                         //&& headerTerm.remarks.ToLowerInvariant() == "cash"
                                         orderby itemPrice.itemID ascending
                                         select new
                                         {
                                             itemPrice.grossPrice,
                                             salesDisc.pctTax,
                                             headerTerm.remarks,
                                             bookingDetail.bookingDetailID,
                                             salesDisc.pctDisc
                                         }).ToList();

            Logger.Info("Start Loop Step 12(Update netPriceCash) per BookingDetailID.");
            foreach (var itm in getUpdateNetPriceCash)
            {
                Logger.Info("Iteration bookingDetailID: " + itm.bookingDetailID);

                var getBookDetail = (from a in _trBookingDetailRepo.GetAll()
                                     where a.bookingHeaderID == input.bookingHeaderID && a.Id == itm.bookingDetailID
                                     orderby a.itemID ascending
                                     select a).FirstOrDefault();

                var updateBookingDetail = getBookDetail.MapTo<TR_BookingDetail>();

                Logger.DebugFormat("Param Update netPriceCash TR_BookingDetail. {0}" +
                "netPriceCash          = {1}{0}",

                Environment.NewLine, itm.grossPrice - (itm.grossPrice * (decimal)itm.pctDisc));

                updateBookingDetail.netPriceCash = itm.grossPrice - (itm.grossPrice * (decimal)itm.pctDisc);
            }


            //Update Net Price Comm Booking Header
            var totalNetPriceComm = arrNetPriceComm.Sum(s => s.netPriceComm);

            var getBookingHeader = (from a in _trBookingHeaderRepo.GetAll()
                                    where a.Id == input.bookingHeaderID
                                    select a).FirstOrDefault();

            var updateBookingHeader = getBookingHeader.MapTo<TR_BookingHeader>();

            Logger.DebugFormat("Param Update netPriceComm TR_BookingHeader. {0}" +
                "netPriceComm          = {1}{0}",

            Environment.NewLine, totalNetPriceComm);

            updateBookingHeader.netPriceComm = totalNetPriceComm;

            Logger.InfoFormat("Step 12 : Update NetPriceComm TR_BookingHeader, Price TR_BookingDetail - End ");

            return penampung;
        }

        private GetScheduleUniversalsDto Step13InsertTrBookingDetailSchedule(GetOriginalScheduleInputDto input)
        {
            Logger.InfoFormat("Step 13 : Insert to TR_BookingDetailSchedule - Start ");

            List<GetSchedulerListDto> dataSchedule = new List<GetSchedulerListDto>();

            var listBfAmount = input.listBfAmount.ToList();

            var pctTax = input.listPctTax.FirstOrDefault().pctTax;

            var bookingDetail = input.listPctTax.ToList();

            var totalAmount = input.listBfAmount.Sum(x => x.bfAmount);

            var sellingPrices = input.listNetNetPrice.Sum(x => x.netNetPrice);

            var netNetPrice = input.listNetNetPrice.ToList();

            var bookDate = input.bookDate.FirstOrDefault().bookDate;

            var dataDP = input.listDetailDP;

            var bookingHeader = input.bookDate;

            short schecNo = 1;

            Logger.DebugFormat("Input Param Step 13. {0}" +
                "bookDate                = {1}{0}" +
                "bookingHeaderID         = {2}{0}" +
                "listBfAmount            = {3}{0}" +
                "listDetailDP            = {4}{0}" +
                "listNetNetPrice         = {5}{0}" +
                "listPctTax              = {6}{0}" +
                "sellingPrice            = {7}{0}",

            Environment.NewLine, input.bookDate, input.bookingHeaderID, JsonConvert.SerializeObject(input.listBfAmount),
            JsonConvert.SerializeObject(input.listDetailDP), JsonConvert.SerializeObject(input.listNetNetPrice),
            JsonConvert.SerializeObject(input.listPctTax), JsonConvert.SerializeObject(input.sellingPrice));

            var getBookingDetail = (from a in bookingDetail
                                    join b in netNetPrice
                                    on a.bookingDetailID equals b.bookingDetailID
                                    select new
                                    {
                                        a.bookingHeaderID,
                                        a.bookingDetailID,
                                        percentage = b.netNetPrice / sellingPrices
                                    }).ToList();

            //harga jual
            var sellingPrice = sellingPrices;

            //data BF untuk di push
            var dataBFFinal = new GetSchedulerListDto
            {
                dueDate = bookDate,
                allocCode = "BF",
                allocID = GetAllocIDbyCode("BF"),
                totalAmount = totalAmount,
                netAmount = totalAmount / (decimal)(1 + pctTax),
                VATAmount = (totalAmount / (decimal)(1 + pctTax)) * (decimal)(pctTax),
                netOutstanding = totalAmount / (decimal)(1 + pctTax),
                VATOutstanding = (totalAmount / (decimal)(1 + pctTax)) * (decimal)(pctTax),
                paymentAmount = 0,
                totalOutstanding = totalAmount,
                schedNo = schecNo
            };

            //push data BF
            dataSchedule.Add(dataBFFinal);

            int countDP = dataDP.Count;

            decimal totalDP = 0;

            //selling
            var DPFinal = new GetSchedulerListDto();

            for (var i = 0; i < countDP; i++)
            {
                schecNo++;


                var DPValue = (input.sellingPrice * (decimal)dataDP.FirstOrDefault().DPPct);

                if (i == 0)
                {
                    DPValue = DPValue - totalAmount;
                }

                //first DP
                DPFinal = new GetSchedulerListDto
                {
                    dueDate = dataBFFinal.dueDate.AddDays(dataDP[0].daysDue),
                    allocCode = "DP",
                    allocID = GetAllocIDbyCode("DP"),
                    totalAmount = DPValue,
                    netAmount = DPValue / (decimal)(1 + pctTax),
                    VATAmount = (DPValue / (decimal)(1 + pctTax)) * (decimal)(pctTax),
                    netOutstanding = DPValue / (decimal)(1 + pctTax),
                    VATOutstanding = (DPValue / (decimal)(1 + pctTax)) * (decimal)(pctTax),
                    paymentAmount = 0,
                    totalOutstanding = DPValue,
                    schedNo = schecNo
                };

                //DP selain pertama
                if (i != 0)
                {
                    DPFinal.dueDate = dataSchedule[dataSchedule.Count - 1].dueDate.AddDays(dataDP[i].daysDue);
                }

                totalDP += DPFinal.totalAmount;

                dataSchedule.Add(DPFinal);
            }

            //INS
            var j = 0;

            var dataINS = (from bh in bookingHeader
                           join t in _msTermPmt.GetAll() on bh.termID equals t.termID
                           join ft in _lkFinType.GetAll() on t.finTypeID equals ft.Id
                           select new
                           {
                               ft.finTimes,
                               t.finStartDue,
                               t.finStartM
                           }).FirstOrDefault();

            //looping untuk mengasih schedNo dll
            while (j < dataINS.finTimes)
            {
                schecNo++;

                var dataINSFinal = new GetSchedulerListDto();

                //INS Selain pertama
                dataINSFinal = new GetSchedulerListDto
                {
                    dueDate = dataSchedule[dataSchedule.Count - 1].dueDate.AddMonths(j),
                    allocCode = "INS",
                    allocID = GetAllocIDbyCode("INS"),
                    totalAmount = (input.sellingPrice - totalDP - dataBFFinal.totalAmount) / dataINS.finTimes,
                    netAmount = ((input.sellingPrice - totalDP - dataBFFinal.totalAmount) / dataINS.finTimes) / (decimal)(1 + pctTax),
                    VATAmount = (((input.sellingPrice - totalDP - dataBFFinal.totalAmount) / dataINS.finTimes) / (decimal)(1 + pctTax)) * (decimal)(pctTax),
                    netOutstanding = ((input.sellingPrice - totalDP - dataBFFinal.totalAmount) / dataINS.finTimes) / (decimal)(1 + pctTax),
                    VATOutstanding = (((input.sellingPrice - totalDP - dataBFFinal.totalAmount) / dataINS.finTimes) / (decimal)(1 + pctTax)) * (decimal)(pctTax),
                    paymentAmount = 0,
                    totalOutstanding = (input.sellingPrice - totalDP - dataBFFinal.totalAmount) / dataINS.finTimes,
                    schedNo = schecNo
                };

                //ins pertama
                if (j == 0)
                {
                    dataINSFinal.dueDate = dataINS.finStartDue != 0 ? dataBFFinal.dueDate.AddDays(dataINS.finStartDue) : dataBFFinal.dueDate.AddMonths((int)dataINS.finStartM);
                }
                dataSchedule.Add(dataINSFinal);
                j++;
            }
            //dataResult.OrderBy(x => x.schedNo);

            var bookingDetails = (from a in bookingDetail
                                  join b in netNetPrice
                                  on a.bookingDetailID equals b.bookingDetailID
                                  select new
                                  {
                                      a.bookingHeaderID,
                                      a.bookingDetailID,
                                      percentage = b.netNetPrice / sellingPrices
                                  }).Distinct().ToList();

            int schedNumber = 0;

            foreach (var item in dataSchedule)
            {
                schedNumber++;

                if (item.allocID != 0)
                {

                    foreach (var book in bookingDetails)
                    {
                        var data = new TR_BookingDetailSchedule
                        {
                            allocID = item.allocID,
                            bookingDetailID = book.bookingDetailID,
                            dueDate = item.dueDate,
                            entityID = 1,
                            netAmt = item.netAmount * book.percentage,
                            netOut = item.netOutstanding * book.percentage,
                            remarks = String.IsNullOrEmpty(item.remarks) ? string.Empty : item.remarks,
                            schedNo = (short)schedNumber,
                            vatAmt = item.VATAmount * book.percentage,
                            vatOut = item.VATOutstanding * book.percentage
                        };

                        _trBookingDetailSchedule.Insert(data);
                    }
                }
                else
                {
                    foreach (var book in bookingDetails)
                    {
                        var data = new TR_BookingDetailSchedule
                        {
                            bookingDetailID = book.bookingDetailID,
                            dueDate = item.dueDate,
                            entityID = 1,
                            netAmt = item.netAmount * book.percentage,
                            netOut = item.netOutstanding * book.percentage,
                            remarks = String.IsNullOrEmpty(item.remarks) ? string.Empty : item.remarks,
                            schedNo = (short)schedNumber,
                            vatAmt = item.VATAmount * book.percentage,
                            vatOut = item.VATOutstanding * book.percentage
                        };

                        _trBookingDetailSchedule.Insert(data);
                    }
                }
            }


            var dataResult = new GetScheduleUniversalsDto
            {
                pctTax = pctTax,
                dataSchedule = dataSchedule
            };

            Logger.InfoFormat("Step 13 : Insert to TR_BookingDetailSchedule - End.");
            return dataResult;
        }

        //[AbpAuthorize(AppPermissions.Pages_Tenant_OnlineBooking_Transaction_UpdateRemarksTrBookingHeader)]
        private ResultMessageDto Step14UpdateRemarksTrBookingHeader(UpdateRemarksDto input)
        {
            Logger.InfoFormat("Step 14 : Update Remarks TR_BookingHeader - Start ");

            Logger.DebugFormat("Input Param Step 14. {0}" +
                "bookCode                 = {1}{0}" +
                "bookingHeaderID          = {2}{0}" +
                "sellingPrice             = {3}{0}",

            Environment.NewLine, input.bookCode, input.bookingHeaderID, input.sellingPrice);

            var getData = (from header in _trBookingHeaderRepo.GetAll()
                           where header.Id == input.bookingHeaderID
                           select header).FirstOrDefault();

            var getRemarks = (from reserved in _trUnitReservedRepo.GetAll()
                              join unit in _msUnitRepo.GetAll()
                              on reserved.unitID equals unit.Id
                              join unitcode in _msUnitCode.GetAll()
                              on unit.unitCodeID equals unitcode.Id
                              join renovation in _msRenovation.GetAll()
                              on reserved.renovID equals renovation.Id
                              join term in _msTermRepo.GetAll()
                              on reserved.termID equals term.Id
                              join project in _msProject.GetAll()
                              on unit.projectID equals project.Id
                              where unit.Id == getData.unitID && term.Id == getData.termID
                              select new
                              {
                                  unitcode.unitCode,
                                  unit.unitNo,
                                  renovation.renovationCode,
                                  term.termCode,
                                  term.termNo,
                                  term.remarks,
                                  project.projectCode
                              }).FirstOrDefault();

            var remarks = "[DailyDealCloser] SP "
            + getRemarks.unitCode + " " + getRemarks.unitNo + " "
            + getRemarks.renovationCode + " " + getRemarks.termCode + " " + getRemarks.termNo + "-"
            + getRemarks.remarks + " Booking Success. Book Code: "
            + input.bookCode + " NetNetPriceSP+PPN: " + string.Format("{0:n2}", input.sellingPrice) + " "
            + "NetNetPriceSPR+PPN: " + string.Format("{0:n2}", 0.00);



            var updateRemarks = getData.MapTo<TR_BookingHeader>();

            updateRemarks.remarks = remarks;

            _trBookingHeaderRepo.Update(updateRemarks);

            return new ResultMessageDto
            {
                message = "Success",
                result = true
            };
        }

        //[AbpAuthorize(AppPermissions.Pages_Tenant_OnlineBooking_Transaction_UpdateOrderStatusFullyPaid)]
        private ResultMessageDto Step15UpdateOrderStatusFullyPaid(UpdateOrderStatusFullyPaid input)
        {
            Logger.InfoFormat("Step 15 : Update Status FullyPaid TR_UnitOrderHeader - Start ");

            var getStatus = (from status in _lkBookingOnlineStatus.GetAll()
                             where status.statusType == "2"
                             select status).FirstOrDefault();

            var getOrderHeader = (from orderHeader in _trUnitOrderHeader.GetAll()
                                  where orderHeader.Id == input.orderHeaderID
                                  select orderHeader).FirstOrDefault();

            var update = getOrderHeader.MapTo<TR_UnitOrderHeader>();

            Logger.DebugFormat("Param Update TR_UnitOrderHeader. {0}" +
                "statusID           = {1}{0}",

            Environment.NewLine, getStatus == null ? string.Empty : getStatus.Id.ToString());

            update.statusID = getStatus.Id;

            _trUnitOrderHeader.Update(update);

            var getOrderDetail = (from orderDetail in _trUnitOrderDetail.GetAll()
                                  where orderDetail.UnitOrderHeaderID == input.orderHeaderID && orderDetail.unitID == input.unitID
                                  select orderDetail).Distinct().ToList();

            Logger.Info("Start Loop Step 15(Update BookingHeaderID TR_UnitOrderDetail) per UnitID.");
            foreach (var item in getOrderDetail)
            {
                Logger.Info("Iteration unitID: " + item.unitID);

                var updateDetail = item.MapTo<TR_UnitOrderDetail>();

                updateDetail.bookingHeaderID = input.bookingHeaderID;

                _trUnitOrderDetail.Update(updateDetail);
            }

            Logger.InfoFormat("Step 15 : Update Status FullyPaid TR_UnitOrderHeader - End ");

            return new ResultMessageDto
            {
                result = true
            };
        }

        //[AbpAuthorize(AppPermissions.Pages_Tenant_OnlineBooking_Transaction_UpdateReleaseDate)]
        private ResultMessageDto Step16UpdateReleaseDate(UpdateReleaseDateInputDto input)
        {
            Logger.InfoFormat("Step 16 : Update ReleaseDate TR_UnitReserved - Start ");

            var getReserved = (from reserved in _trUnitReservedRepo.GetAll()
                               where reserved.unitID == input.unitID
                               && reserved.termID == input.termID
                               && reserved.renovID == input.renovID
                               && reserved.pscode == input.psCode
                               select reserved).FirstOrDefault();

            var update = getReserved.MapTo<TR_UnitReserved>();

            Logger.DebugFormat("Param Update TR_UnitReserved. {0}" +
               "ReleaseDate           = {1}{0}",

           Environment.NewLine, DateTime.Now);

            update.releaseDate = DateTime.Now;

            _trUnitReservedRepo.Update(update);

            Logger.InfoFormat("Step 16 : Update ReleaseDate TR_UnitReserved - End ");

            return new ResultMessageDto
            {
                result = true
            };
        }

        //[AbpAuthorize(AppPermissions.Pages_Tenant_OnlineBooking_Transaction_InsertTrSoldUnit)]
        private ResultMessageDto Step17InsertTrSoldUnit(InsertTrSoldUnitInputDto input)
        {
            Logger.InfoFormat("Step 17 : Insert to TR_SoldUnit - Start ");

            Logger.DebugFormat("Input Param Step 17. {0}" +
                "memberCode            = {1}{0}" +
                "netNetPrice           = {2}{0}" +
                "scmCode               = {3}{0}" +
                "sellingPrice          = {4}{0}" +
                "unitID                = {5}{0}" +
                "userID                = {6}{0}" +
                "listBookingDetail     = {7}{0}" +
                "listBookingHeader     = {8}{0}",

            Environment.NewLine, input.memberCode, input.netNetPrice, input.scmCode, input.sellingPrice, input.unitID,
            input.userID, JsonConvert.SerializeObject(input.listBookingDetail), JsonConvert.SerializeObject(input.listBookingHeader));

            var getProject = (from project in _msProject.GetAll()
                              join unit in _msUnitRepo.GetAll()
                              on project.Id equals unit.projectID
                              join unitCode in _msUnitCode.GetAll()
                              on unit.unitCodeID equals unitCode.Id
                              join cluster in _msCluster.GetAll()
                              on unit.clusterID equals cluster.Id
                              where unit.Id == input.unitID
                              select new
                              {
                                  project.BusinessGroup,
                                  unitCode.unitCode,
                                  projectID = project.Id,
                                  project.projectCode,
                                  unit.clusterID,
                                  cluster.clusterCode,
                                  unitCode.unitName,
                                  unit.unitNo
                              }).FirstOrDefault();


            var getLandItemID = (from item in _lkItem.GetAll()
                                 where item.itemCode == "01"
                                 select item.Id).FirstOrDefault();

            var getBuildItemID = (from item in _lkItem.GetAll()
                                  where item.itemCode == "02"
                                  select item.Id).FirstOrDefault();

            var listBookingDetail = input.listBookingDetail.ToList();

            var getLandArea = (from detail in listBookingDetail
                               where detail.itemID == getLandItemID
                               select new
                               {
                                   detail.area,
                                   detail.bookNo,
                                   detail.coCode
                               }).FirstOrDefault();

            var getBuildArea = (from detail in listBookingDetail
                                where detail.itemID == getBuildItemID
                                select detail.area).FirstOrDefault();

            var getPctBobot = (from bobotComm in _contextNew.MS_BobotComm.ToList()
                               where bobotComm.projectCode == getProject.projectCode
                               && bobotComm.clusterCode == getProject.clusterCode
                               && bobotComm.scmCode == input.scmCode
                               select bobotComm).FirstOrDefault();

            var getPctComm = (from termPmt in _msTermPmt.GetAll()
                              join finType in _lkFinType.GetAll()
                              on termPmt.finTypeID equals finType.Id
                              where termPmt.termID == input.listBookingHeader.termID
                              select finType).FirstOrDefault();

            var totalNetNetPrice = input.netNetPrice.Sum(x => x.netNetPrice);

            decimal unitPrice = 0;

            if (getPctBobot != null)
            {
                unitPrice = totalNetNetPrice * (decimal)getPctBobot.pctBobot;
            }

            Logger.DebugFormat("Param Insert TR_SoldUnit. {0}" +
                "bookNo                 = {1}{0}" +
                "bookDate               = {2}{0}" +
                "calculateUseMaster     = {3}{0}" +
                "memberCode             = {4}{0}" +
                "pctBobot               = {5}{0}" +
                "roadCode               = {6}{0}" +
                "roadName               = {7}{0}" +
                "unitBuildArea          = {8}{0}" +
                "unitLandArea           = {9}{0}" +
                "scmCode                = {10}{0}" +
                "unitNo                 = {11}{0}" +
                "ACDCode                = {12}{0}" +
                "batchNo                = {13}{0}" +
                "cancelDate             = {14}{0}" +
                "CDCode                 = {15}{0}" +
                "entityCode             = {16}{0}" +
                "holdDate               = {17}{0}" +
                "PPJBDate               = {18}{0}" +
                "Remarks                = {19}{0}" +
                "xprocessDate           = {20}{0}" +
                "xreqInstPayDate        = {21}{0}" +
                "devCode                = {22}{0}" +
                "netNetPrice            = {23}{0}" +
                "unitPrice              = {24}{0}" +
                "pctComm                = {25}{0}" +
                "propCode               = {26}{0}" +
                "inputTime              = {27}{0}" +
                "modifTime              = {28}{0}" +
                "modifUN                = {29}{0}" +
                "inputUN                = {30}{0}",

            Environment.NewLine, getLandArea == null ? string.Empty : getLandArea.bookNo.ToString(),//0,1
            DateTime.Now, true, input.memberCode, getPctBobot == null ? 0 : getPctBobot.pctBobot,//2,3,4,5
            getProject == null ? string.Empty : getProject.unitCode, getProject == null ? string.Empty : getProject.unitName,//6,7
            getBuildArea, getLandArea == null ? string.Empty : getLandArea.area.ToString(), input.scmCode,//8,9,10
            getProject == null ? string.Empty : getProject.unitNo, "-", "-", string.Empty, "-", "1",//11,12,13,14,15,16
            string.Empty, string.Empty, input.listBookingHeader.termRemarks, string.Empty, string.Empty,//17,18,19,20,21
            getLandArea == null ? string.Empty : getLandArea.coCode, totalNetNetPrice, unitPrice,//22,23,24
            getPctComm == null ? 0 : getPctComm.pctComm, "LK", DateTime.Now, DateTime.Now,//25,26,27,28
            input.memberCode, input.memberCode); //29,30


            var insert = new TR_SoldUnit
            {
                bookNo = getLandArea.bookNo.ToString(),
                bookDate = DateTime.Now,
                calculateUseMaster = true,
                memberCode = input.memberCode,
                pctBobot = getPctBobot == null ? 0 : getPctBobot.pctBobot,
                roadCode = getProject.unitCode,
                roadName = getProject.unitName,
                unitBuildArea = (float)getBuildArea,
                unitLandArea = (float)getLandArea.area,
                scmCode = input.scmCode,
                unitNo = getProject.unitNo,
                ACDCode = "-",
                batchNo = "-",
                cancelDate = null,
                CDCode = "-",
                entityCode = "1",
                holdDate = null,
                PPJBDate = null,
                Remarks = input.listBookingHeader.termRemarks,
                xprocessDate = null,
                xreqInstPayDate = null,
                devCode = getLandArea.coCode,
                netNetPrice = totalNetNetPrice,
                unitPrice = unitPrice,
                pctComm = getPctComm == null ? 0 : getPctComm.pctComm,
                propCode = "LK",
                inputTime = DateTime.Now,
                inputUN = input.memberCode,
                modifTime = DateTime.Now,
                modifUN = input.memberCode
            };
            _contextNew.Add(insert);

            //_contextNew.SaveChanges();

            Logger.InfoFormat("Step 17 : Insert to TR_SoldUnit - End ");

            return new ResultMessageDto
            {
                result = true
            };
        }

        //[AbpAuthorize(AppPermissions.Pages_Tenant_OnlineBooking_Transaction_InsertTrSoldUnitRequirement)]
        private ResultMessageDto Step18InsertTrSoldUnitRequirement(InsertTrSoldUnitRequirementInputDto input)
        {
            Logger.InfoFormat("Step 18 : Insert to TR_SoldUnitRequirement - Start ");

            var listBookingDetail = input.listBookingDetail.ToList();

            var getScmID = (from schema in _contextNew.MS_Schema.ToList()
                            where schema.scmCode == input.scmCode
                            select schema).FirstOrDefault();

            if (getScmID != null)
            {
                var getSoldUnitRequirement = (from schemaRequirement in _contextNew.MS_SchemaRequirement.ToList()
                                              where schemaRequirement.scmCode == getScmID.scmCode
                                              select schemaRequirement).ToList();

                if (getSoldUnitRequirement != null)
                {


                    var getLandItemID = (from item in _lkItem.GetAll()
                                         where item.itemCode == "01"
                                         select item.Id).FirstOrDefault();

                    var getBookNo = (from detail in listBookingDetail
                                     where detail.itemID == getLandItemID
                                     select new
                                     {
                                         detail.bookNo,
                                         detail.coCode
                                     }).FirstOrDefault();

                    Logger.Info("Start Loop Step 18 per reqNo MS_SchemaRequirement.");
                    foreach (var item in getSoldUnitRequirement)
                    {
                        Logger.Info("Iteration reqNo: " + item.reqNo + "scmCode : " + item.scmCode);

                        Logger.DebugFormat("Param Insert TR_SoldUnitRequirement. {0}" +
                            "bookNo           = {1}{0}" +
                            "entityCode       = {2}{0}" +
                            "orPctPaid        = {3}{0}" +
                            "pctPaid          = {4}{0}" +
                            "reqDesc          = {5}{0}" +
                            "reqNo            = {6}{0}" +
                            "scmCode          = {7}{0}" +
                            "processDate      = {8}{0}" +
                            "reqDate          = {9}{0}" +
                            "devCode          = {10}{0}" +
                            "inputTime        = {11}{0}" +
                            "inputUN          = {12}{0}" +
                            "modifTime        = {13}{0}" +
                            "modifUN          = {14}{0}",

            Environment.NewLine, getBookNo == null ? string.Empty : getBookNo.bookNo.ToString(),
            item.entityCode, item.orPctPaid, item.pctPaid, item.reqDesc, item.reqNo, item.scmCode,
            string.Empty, string.Empty, getBookNo == null ? string.Empty : getBookNo.coCode,
            DateTime.Now, input.memberCode, DateTime.Now, input.memberCode);

                        var data = new TR_SoldUnitRequirement
                        {
                            bookNo = getBookNo.bookNo.ToString(),
                            entityCode = item.entityCode,
                            orPctPaid = item.orPctPaid,
                            pctPaid = item.pctPaid,
                            reqDesc = item.reqDesc,
                            reqNo = (byte)item.reqNo,
                            scmCode = item.scmCode,
                            processDate = null,
                            reqDate = null,
                            devCode = getBookNo.coCode,
                            inputTime = DateTime.Now,
                            inputUN = input.memberCode,
                            modifTime = DateTime.Now,
                            modifUN = input.memberCode
                        };

                        _contextNew.Add(data);

                    }
                }
            }

            Logger.InfoFormat("Step 18 : Insert to TR_SoldUnitRequirement - End ");

            return new ResultMessageDto
            {
                result = true
            };
        }

        public void testTime(DateTime CreationTime)
        {
            var stringTime7 = CreationTime.AddDays(1).Date.ToString("dd/MM/yyyy");

            var stringTime22 = CreationTime.Date.ToString("dd/MM/yyyy");

            Logger.Info("jam7 = " + stringTime7);

            var jam7 = DateTime.ParseExact(stringTime7 + " 07:00 AM", "dd/MM/yyyy hh:mm tt", CultureInfo.InvariantCulture);

            Logger.Info("jam7 exact = " + jam7);

            var jam22 = DateTime.ParseExact(stringTime22 + " 10:00 PM", "dd/MM/yyyy hh:mm tt", CultureInfo.InvariantCulture);

            Logger.Info("jam22 Exact = " + jam22);
        }

        public void SchedulerStatusOrderExpired()
        {
            var getStatusID = (from a in _context.LK_BookingOnlineStatus
                             where a.statusTypeName == "Outstanding"
                             select a.Id).FirstOrDefault();

            var getType = (from b in _context.LK_PaymentType
                           where b.paymentType == 4
                           orderby b.Id ascending
                           select b.Id).FirstOrDefault();

            var getData = (from orderHeader in _context.TR_UnitOrderHeader
                           where orderHeader.statusID == getStatusID && orderHeader.paymentTypeID == getType
                           orderby orderHeader.CreationTime descending
                           select orderHeader).Distinct().ToList();

            TimeSpan banding = TimeSpan.Parse("12:00:00");

            

            foreach (var item in getData)
            {
                var time = DateTime.Now - item.CreationTime;

                if (time >= banding)
                {
                    var expiredDate = item.CreationTime.AddHours(12);

                    var stringTime7 = item.CreationTime.AddDays(1).Date.ToString("dd/MM/yyyy");

                    var stringTime22 = item.CreationTime.Date.ToString("dd/MM/yyyy");

                    Logger.Info("jam7 = " + stringTime7);

                    var jam7 = DateTime.ParseExact(stringTime7 + " 07:00 AM", "dd/MM/yyyy hh:mm tt", CultureInfo.InvariantCulture);

                    Logger.Info("jam7 exact = " + jam7);

                    var jam22 = DateTime.ParseExact(stringTime22 + " 10:00 PM", "dd/MM/yyyy hh:mm tt", CultureInfo.InvariantCulture);

                    Logger.Info("jam22 Exact = " + jam22);

                    //REPAIR JAM 7 dan 10 dan 9
                    //if (expiredDate <= jam7 && expiredDate >= jam22)

                    var getCustInfo = (from personals in _contextPers.PERSONAL.ToList()
                                       where personals.psCode == item.psCode
                                       select personals).FirstOrDefault();

                    var getSalesInfo = (from member in _contextPers.PERSONALS_MEMBER.ToList()
                                        join phone in _contextPers.TR_Phone.ToList() on member.psCode equals phone.psCode into trPhone
                                        from c in trPhone.DefaultIfEmpty()
                                        where member.memberCode == item.memberCode
                                        select new { member, c }).FirstOrDefault();

                    if (expiredDate.CompareTo(jam7) <= 0 && expiredDate.CompareTo(jam22) >= 0)
                    {
                        var stringTime9 = DateTime.Now.Date.ToString("dd/MM/yyyy");

                        Logger.Info("jam9 = " + stringTime9);

                        var jam9 = DateTime.ParseExact(stringTime9 + " 09:00 AM", "dd/MM/yyyy hh:mm tt", CultureInfo.InvariantCulture);

                        if (DateTime.Now.CompareTo(jam9) >= 0 )
                        {
                            var getStatusCancel = (from status in _lkBookingOnlineStatus.GetAll()
                                                   where status.statusType == "4"
                                                   select status.Id).FirstOrDefault();

                            var update = item.MapTo<TR_UnitOrderHeader>();

                            update.statusID = getStatusCancel;

                            _trUnitOrderHeader.Update(update);

                            var getOrderDetail = (from orderheader in _trUnitOrderHeader.GetAll()
                                                  join orderdetail in _trUnitOrderDetail.GetAll()
                                                  on orderheader.Id equals orderdetail.UnitOrderHeaderID
                                                  where orderheader.Id == item.Id
                                                  select new
                                                  {
                                                      orderHeaderID = orderheader.Id,
                                                      unitID = orderdetail.unitID
                                                  }).Distinct().ToList();

                            foreach (var unit in getOrderDetail)
                            {
                                var getUnit = (from msUnit in _msUnitRepo.GetAll()
                                               where msUnit.Id == unit.unitID
                                               select msUnit).FirstOrDefault();

                                var getUnitStatus = (from unitStatus in _lkUnitStatus.GetAll()
                                                     where unitStatus.unitStatusCode == "A"
                                                     select unitStatus.Id).FirstOrDefault();
                                
                                var getProjectInfo = (from project in _msProject.GetAll()
                                                      join info in _msProjectInfo.GetAll() on project.Id equals info.projectID into a
                                                      from projectInfo in a.DefaultIfEmpty()
                                                      where project.Id == getUnit.projectID
                                                      orderby projectInfo.CreationTime descending
                                                      select new { project, projectInfo }).FirstOrDefault();

                                var getUnitCode = (from unitcode in _msUnitCode.GetAll()
                                                   where unitcode.Id == getUnit.unitCodeID
                                                   select unitcode.unitCode).FirstOrDefault();

                                var updateUnit = getUnit.MapTo<MS_Unit>();

                                updateUnit.unitStatusID = getUnitStatus;

                                _msUnitRepo.Update(updateUnit);

                                var emailExpired = new UnitExpiredInputDto
                                {
                                    customerName = getCustInfo.name,
                                    devPhone = getProjectInfo.projectInfo.projectMarketingPhone == null ? "-" : getProjectInfo.projectInfo.projectMarketingPhone,
                                    marketingOffice = getProjectInfo.projectInfo.projectMarketingOffice == null ? "-" : getProjectInfo.projectInfo.projectMarketingOffice,
                                    memberName = item.memberName,
                                    memberPhone = getSalesInfo.c.number == null ? "-" : getSalesInfo.c.number,
                                    orderCode = item.orderCode,
                                    projectImage = getProjectInfo.project.image,
                                    projectName = getProjectInfo.project.projectName,
                                    unitCode = getUnitCode,
                                    unitNo = getUnit.unitNo
                                };

                                var body = _emailAppService.UnitExpired(emailExpired);

                                var email = new SendEmailInputDto
                                {
                                    body = body,
                                    toAddress = item.psEmail /*"keniamalia1@gmail.com"*/, //dataUnitOrderHeader.psEmail,
                                    subject = "Pembatalan  Order" + item.orderCode + "atas Unit" + getUnit.unitNo + "/" + getUnitCode
                                };

                                _emailAppService.ConfigurationEmailBankTransfer(email);
                            }
                        }
                    }
                    else
                    {
                        var getStatus = (from status in _lkBookingOnlineStatus.GetAll()
                                         where status.statusType == "4"
                                         select status.Id).FirstOrDefault();

                        var getOrderHeader = (from orderheader in _trUnitOrderHeader.GetAll()
                                              where orderheader.Id == item.Id
                                              select orderheader).Distinct().FirstOrDefault();
                        
                        var update = getOrderHeader.MapTo<TR_UnitOrderHeader>();

                        update.statusID = getStatus;

                        _trUnitOrderHeader.Update(update);

                        var getDetail = (from orderDetail in _trUnitOrderDetail.GetAll()
                                         where orderDetail.UnitOrderHeaderID == getOrderHeader.Id
                                         select orderDetail.unitID).Distinct().ToList();

                        foreach (var unit in getDetail)
                        {
                            var getUnit = (from msUnit in _msUnitRepo.GetAll()
                                           where msUnit.Id == unit
                                           select msUnit).FirstOrDefault();

                            var getUnitStatus = (from unitStatus in _lkUnitStatus.GetAll()
                                                 where unitStatus.unitStatusCode == "A"
                                                 select unitStatus.Id).FirstOrDefault();

                            var updateUnit = getUnit.MapTo<MS_Unit>();

                            updateUnit.unitStatusID = getUnitStatus;

                            _msUnitRepo.Update(updateUnit);


                            var getProjectInfo = (from project in _msProject.GetAll()
                                                  join info in _msProjectInfo.GetAll() on project.Id equals info.projectID into a
                                                  from projectInfo in a.DefaultIfEmpty()
                                                  where project.Id == getUnit.projectID
                                                  orderby projectInfo.CreationTime descending
                                                  select new { project, projectInfo }).FirstOrDefault();

                            var getUnitCode = (from unitcode in _msUnitCode.GetAll()
                                               where unitcode.Id == getUnit.unitCodeID
                                               select unitcode.unitCode).FirstOrDefault();

                            var emailExpired = new UnitExpiredInputDto
                            {
                                customerName = getCustInfo.name,
                                devPhone = getProjectInfo.projectInfo.projectMarketingPhone == null ? "-" : getProjectInfo.projectInfo.projectMarketingPhone,
                                marketingOffice = getProjectInfo.projectInfo.projectMarketingOffice == null ? "-" : getProjectInfo.projectInfo.projectMarketingOffice,
                                memberName = item.memberName,
                                memberPhone = getSalesInfo.c.number == null ? "-" : getSalesInfo.c.number,
                                orderCode = item.orderCode,
                                projectImage = getProjectInfo.project.image,
                                projectName = getProjectInfo.project.projectName,
                                unitCode = getUnitCode,
                                unitNo = getUnit.unitNo
                            };

                            var body = _emailAppService.UnitExpired(emailExpired);

                            var email = new SendEmailInputDto
                            {
                                body = body,
                                toAddress = item.psEmail /*"keniamalia1@gmail.com"*/, //dataUnitOrderHeader.psEmail,
                                subject = "Pembatalan  Order" + item.orderCode + "atas Unit" + getUnit.unitNo + "/" + getUnitCode
                            };

                            _emailAppService.ConfigurationEmailBankTransfer(email);
                        }
                    }
                }
            }
        }

        //harusnya scheduler
        public void SchedulerTRUnitReserved()
        {
            var getData = (from a in _trUnitReservedRepo.GetAll()
                           where a.releaseDate == null && a.remarks == null
                           orderby a.CreationTime descending
                           select new
                           {
                               a.Id,
                               a.reserveDate,
                               a.unitID
                           }).ToList();

            TimeSpan banding = TimeSpan.Parse("00:30:00");

            foreach (var item in getData)
            {
                var time = DateTime.Now - item.reserveDate;
                if (time >= banding)
                {
                    var getUnitReserved = (from x in _trUnitReservedRepo.GetAll()
                                           where x.Id == item.Id
                                           select x).FirstOrDefault();

                    var deletebyReserve = (from a in _trUnitReservedRepo.GetAll()
                                           where a.reservedBy == getUnitReserved.reservedBy
                                           select a).ToList();

                    foreach (var data in deletebyReserve)
                    {
                        var updateUnitReserved = data.MapTo<TR_UnitReserved>();

                        updateUnitReserved.releaseDate = DateTime.Now;

                        _trUnitReservedRepo.Update(updateUnitReserved);

                        var getUnit = (from y in _msUnitRepo.GetAll()
                                       where y.Id == item.unitID
                                       select y).FirstOrDefault();

                        var updateUnit = getUnit.MapTo<MS_Unit>();

                        updateUnit.unitStatusID = 1;

                        _msUnitRepo.Update(updateUnit);
                    }
                }
            }
        }

        [UnitOfWork(isTransactional: false)]
        [AbpAuthorize(AppPermissions.Pages_Tenant_OnlineBooking_Transaction_GetTrUnitReserved)]
        public List<GetTrUnitReservedDto> GetTrUnitReserved(int userID)
        {
            //get user personal
            var getUserPersonal = (from a in _contextDemo.Users
                                   join b in _contextDemo.MP_UserPersonals
                                   on a.Id equals b.userID
                                   where a.Id == userID
                                   select new { a.UserName, b.psCode }).FirstOrDefault();

            ////get member
            //var getMember = (from a in _personalsMemberRepo.GetAll()
            //                 where a.psCode == getUserPersonal.psCode
            //                 && a.memberCode == getUserPersonal.UserName
            //                 select new
            //                 {
            //                     a.memberCode
            //                 }).FirstOrDefault();

            var getTrUnitReserved = (from unitReserved in _trUnitReservedRepo.GetAll()
                                     join term in _msTermRepo.GetAll()
                                     on unitReserved.termID equals term.Id
                                     join renov in _msRenovation.GetAll()
                                     on unitReserved.renovID equals renov.Id
                                     join unit in _msUnitRepo.GetAll()
                                     on unitReserved.unitID equals unit.Id
                                     join cluster in _msClusterRepo.GetAll()
                                     on unit.clusterID equals cluster.Id
                                     join project in _msProjectRepo.GetAll()
                                     on unit.projectID equals project.Id
                                     join unitCode in _msUnitCode.GetAll()
                                     on unit.unitCodeID equals unitCode.Id
                                     join detail in _msDetail.GetAll() on unit.detailID equals detail.Id
                                     where unitReserved.releaseDate == null
                                        && unitReserved.reservedBy == getUserPersonal.UserName
                                        && unitReserved.remarks == null
                                     select new GetTrUnitReservedDto
                                     {
                                         unitReservedID = unitReserved.Id,
                                         clusterName = cluster.clusterName,
                                         unitID = unit.Id,
                                         unitCode = unitCode.unitCode,
                                         unitNo = unit.unitNo,
                                         projectID = project.Id,
                                         projectName = project.projectName,
                                         termID = term.Id,
                                         termName = term.remarks,
                                         renovID = renov.Id,
                                         renovName = renov.renovationName,
                                         sellingPrice = unitReserved.SellingPrice,
                                         bookingFee = unitReserved.BFAmount,
                                         clusterID = unit.clusterID,
                                         detailImage = _fileHelper.getAbsoluteUri() + detail.detailImage,
                                         creationTime = unitReserved.CreationTime.ToString()
                                     }).Distinct().ToList();

            return getTrUnitReserved;
        }

        public List<GetListBankResultDto> GetGetListBank()
        {
            var getData = (from a in _msBank.GetAll()
                           where a.isActive == true
                           select new GetListBankResultDto
                           {
                               bankCode = a.bankCode,
                               bankID = a.Id,
                               bankName = a.bankName
                           }).ToList();

            return getData;
        }

        private void UpdateRemarksReserved(UpdateRemarksReservedInputDto input)
        {
            //get user personal
            var getUserPersonal = (from a in _mpUserPersonals.GetAll()
                                   where a.userID == input.userID
                                   select a).FirstOrDefault();

            //get member
            var getMember = (from a in _personalsMemberRepo.GetAll()
                             where a.psCode == getUserPersonal.psCode
                             select new
                             {
                                 a.memberCode
                             }).FirstOrDefault();

            var getDisc1 = (from a in _msDiscOnlineBooking.GetAll()
                            where a.projectID == input.projectID
                            select new
                            {
                                a.discPct,
                                a.discDesc,
                            }).FirstOrDefault();

            var getDisc2 = (from a in _msTermDiscOnlineBooking.GetAll()
                            where a.termID == input.termID
                            select new
                            {
                                a.pctDisc,
                                a.discName
                            }).FirstOrDefault();


            var remarks = "Selling Price: " + input.sellingPrice + ", Disc1: " + (getDisc1 != null ? getDisc1.discDesc : "0")
                + ", Disc2: " + (getDisc2 != null ? getDisc2.discName : "0") + ".";

            var getReserved = (from reserved in _trUnitReservedRepo.GetAll()
                               where reserved.unitID == input.unitID
                               && reserved.reservedBy == getMember.memberCode
                               && reserved.releaseDate == null
                               select reserved).FirstOrDefault();

            var update = getReserved.MapTo<TR_UnitReserved>();

            update.remarks = remarks;

            _trUnitReservedRepo.Update(update);
        }

        public string BookCode(string projectCode)
        {
            Logger.InfoFormat("BookCode() - Start.");

            Logger.DebugFormat("Param BookCode. {0}" +
                "projectCode         = {1}{0}",
            Environment.NewLine, projectCode);

            var dateTime = DateTime.Now;
            var dateString = dateTime.ToString();
            var year = (dateTime.Year).ToString().Substring(2, 2);
            var date = dateString.Substring(3, 2);
            var month = dateString.Substring(0, 2);
            var time = dateString.IndexOf(" ");
            var tm = dateString.Substring(time + 1).Replace(@":", string.Empty).Replace(@" ", string.Empty);
            var dt = tm.Remove(tm.Length - 2);
            var bookCode = "";

            if (tm.Length > 5)
            {
                var bookCodeSlash = projectCode + year + month + date + dt + dateTime.Millisecond.ToString();
                bookCode = bookCodeSlash.Replace(@"/", string.Empty);
            }
            else
            {
                var bookCodeSlash = projectCode + year + month + date + "0" + dt + dateTime.Millisecond.ToString();
                bookCode = bookCodeSlash.Replace(@"/", string.Empty);
            }

            Logger.DebugFormat("Result BookCode. {0}" +
                "bookCode         = {1}{0}",
            Environment.NewLine, bookCode);

            Logger.InfoFormat("BookCode() - End.");

            return bookCode;
        }

        [UnitOfWork(isTransactional: false)]
        public async Task<PaymentOnlineBookingResponse> ReorderUnit(CreateTransactionUniversalDto input)
        {
            Logger.InfoFormat("ReorderUnit() - Start.");

            string inputs = JsonConvert.SerializeObject(input);
            Logger.InfoFormat(inputs);


            var getOrderHeader = (from header in _trUnitOrderHeader.GetAll()
                                  where header.Id == input.orderHeaderID
                                  select header).FirstOrDefault();

            Logger.InfoFormat("ReorderUnit() - getOrderHeader.");
            string getOrderHeaders = JsonConvert.SerializeObject(getOrderHeader);
            Logger.InfoFormat(getOrderHeaders);

            var getStatusCancel = (from status in _lkBookingOnlineStatus.GetAll()
                                   where status.statusType == "4"
                                   select status.Id).FirstOrDefault();

            Logger.InfoFormat("ReorderUnit() - getStatusCancel.");
            string getStatusCancels = JsonConvert.SerializeObject(getStatusCancel);
            Logger.InfoFormat(getStatusCancels);

            var update = getOrderHeader.MapTo<TR_UnitOrderHeader>();

            update.statusID = getStatusCancel;

            _trUnitOrderHeader.Update(update);


            var dataDoBooking = new CreateTransactionUniversalDto
            {
                arrPP = input.arrPP,
                arrUnit = input.arrUnit,
                totalAmt = input.totalAmt,
                bankRekeningPemilik = input.bankRekeningPemilik,
                memberCode = input.memberCode,
                memberName = input.memberName,
                nomorRekeningPemilik = input.nomorRekeningPemilik,
                orderCode = input.orderCode,
                orderHeaderID = input.orderHeaderID,
                payTypeID = input.payTypeID,
                pscode = input.pscode,
                scmCode = input.scmCode,
                sumberDanaID = input.sumberDanaID,
                tujuanTransaksiID = input.tujuanTransaksiID,
                userID = input.userID
            };

            var result = await DoBookingMidransReq(dataDoBooking);

            Logger.InfoFormat("ReorderUnit() - dataDoBooking.");
            string dataDoBookings = JsonConvert.SerializeObject(dataDoBooking);
            Logger.InfoFormat(dataDoBookings);

            return result;
        }

        private int GetAllocIDbyCode(string allocCode)
        {
            var allocID = (from a in _lkAlloc.GetAll()
                           where a.allocCode == allocCode
                           select a.Id).FirstOrDefault();

            return allocID;
        }

        //[AbpAuthorize(AppPermissions.Pages_Tenant_OnlineBooking_Transaction_InsertTransactionUniversal)]
        [UnitOfWork(IsDisabled = true)]
        public ResultMessageDto InsertTransactionUniversal(CreateTransactionUniversalDto input)
        {
            string bookCode = "";

            bool isSuccess = false;

            var arrSendEmail = new List<BookingSuccessInputDto>();

            Logger.InfoFormat("Start InsertTransactionUniversal()");
            var param = JsonConvert.SerializeObject(input);
            Logger.InfoFormat(param);

            using (var unitOfWork = _unitOfWorkManager.Begin())
            {
                if (input.pscode != null || input.payTypeID != 0 || input.arrUnit != null)
                {
                    TrBookingHeaderResultDto bookingHeaderID = new TrBookingHeaderResultDto();

                    var trBookingDetailDP = new List<InsertTRDetailDPResultDto>();

                    Logger.Info("Start Loop unit in cart");
                    foreach (var item in input.arrUnit)
                    {
                        isSuccess = false;

                        Logger.Info("Iteration Unit: " + item.unitID);

                        var checkBooked = (from a in _trBookingHeaderRepo.GetAll()
                                           where a.cancelDate == null && a.unitID == item.unitID
                                           select a).FirstOrDefault();

                        if (checkBooked == null)
                        {
                            var resultUpdateNetPrice = new List<UpdateNetPriceResultDto>();

                            //Step 1

                            var dataInsertTrBookingHeader = new TrBookingHeaderInputDto
                            {
                                userID = input.userID,
                                unitID = item.unitID,
                                termID = item.termID,
                                psCode = input.pscode,
                                sellingPrice = item.sellingprice,
                                tujuanTransaksiID = input.tujuanTransaksiID,
                                sumberDanaID = input.sumberDanaID,
                                bankRekeningPemilik = input.bankRekeningPemilik,
                                nomorRekeningPemilik = input.nomorRekeningPemilik,
                                memberCode = input.memberCode,
                                memberName = input.memberName,
                                scmCode = input.scmCode
                            };

                            bookingHeaderID = Step1InsertTrBookingHeader(dataInsertTrBookingHeader);

                            bookCode = bookingHeaderID.message;

                            //Step 2
                            var dataInsertTrBookingDetail = new TrBookingDetailInputDto
                            {
                                bookingHeaderID = bookingHeaderID.bookingHeaderID,
                                termID = item.termID,
                                unitID = item.unitID,
                                renovID = item.renovID
                            };
                            var trBookingDetailID = Step2InsertTrBookingDetail(dataInsertTrBookingDetail);

                            //Step 3
                            var dataInsertTrBookingSalesAddDisc = new TrBookingSalesAddDiscInputDto
                            {
                                bookingHeaderID = bookingHeaderID.bookingHeaderID,
                                termID = item.termID,
                                unitID = item.unitID
                            };
                            var trBookingSalesAddDisc = Step3InsertTrBookingSalesAddDisc(dataInsertTrBookingSalesAddDisc);

                            Logger.Info("Start Loop BookingDetail");
                            foreach (var bookingDetail in trBookingDetailID)
                            {
                                Logger.Info("Iteration : BookingDetailID " + bookingDetail.bookingDetailID +
                                    "Item " + bookingDetail.itemID);

                                //Step 4
                                var dataInsertAddDisc = new InsertAddDiscInputDto
                                {
                                    bookingDetailID = bookingDetail.bookingDetailID,
                                    unitID = item.unitID,
                                    termID = item.termID
                                };
                                Step4InsertAddDisc(dataInsertAddDisc);

                                //Step 5
                                var dataTrBookingDetailDP = new TrBookingDetailDPInputDto
                                {
                                    bookingDetailID = bookingDetail.bookingDetailID,
                                    termID = item.termID,
                                    unitID = item.unitID
                                };
                                trBookingDetailDP = Step5InsertTrBookingDetailDP(dataTrBookingDetailDP);

                                //Step 6
                                var dataTrCashAddDisc = new TrCashAddDiscInputDto
                                {
                                    bookingDetailID = bookingDetail.bookingDetailID,
                                    termID = item.termID,
                                    unitID = item.unitID
                                };
                                var trCashAddDisc = Step6InsertTrCashAddDisc(dataTrCashAddDisc);

                                //Step 7
                                var dataInsertTrBookingTax = new InsertTrBookingTaxInputDto
                                {
                                    bookingDetailID = bookingDetail.bookingDetailID,
                                    sellingPrice = item.sellingprice,
                                    termID = item.termID,
                                    unitID = item.unitID
                                };
                                var resultInsertTrBookingTax = Step7InsertTrBookingTax(dataInsertTrBookingTax);

                            }

                            //Step 8
                            var dataUpdateBFAmount = new UpdateBFAmountInputDto
                            {
                                unitID = item.unitID,
                                renovID = item.renovID
                            };

                            var updateBFAmount = Step8UpdateBFAmount(dataUpdateBFAmount);

                            //Step 9
                            var dataTrBookingHeaderTerm = new InsertTrBookingHeaderTermInputDto
                            {
                                unitID = item.unitID
                            };
                            var insertTrBookingHeaderTerm = Step9InsertTrBookingHeaderTerm(dataTrBookingHeaderTerm);

                            //Step 10
                            var dataTrBookingItemPrice = new InsertTrBookingHeaderTermInputDto
                            {
                                unitID = item.unitID
                            };
                            var insertTrBookingItemPrice = Step10InsertTrBookingItemPrice(dataTrBookingItemPrice);

                            //Step 11
                            var dataUpdateUnit = new InsertTrBookingHeaderTermInputDto
                            {
                                unitID = item.unitID
                            };
                            var updateUnitSold = Step11UpdateUnitSold(dataUpdateUnit);

                            //Step 12
                            var dataUpdateNetPriceBookingHeaderDetail = new UpdateBookingDetailInputDto
                            {
                                bookingHeaderID = bookingHeaderID.bookingHeaderID,
                                termID = item.termID,
                                listSalesDisc = trBookingSalesAddDisc,
                                listBookingItemPrice = insertTrBookingItemPrice,
                                listBookingHeaderTerm = insertTrBookingHeaderTerm,
                                listBookingDetail = trBookingDetailID
                            };
                            resultUpdateNetPrice = Step12UpdateNetPriceBookingHeaderDetail(dataUpdateNetPriceBookingHeaderDetail);

                            var listBookingHeader = new List<TrBookingHeaderResultDto>();
                            listBookingHeader.Add(bookingHeaderID);

                            //step 13
                            var dataGetOriginalSchedule = new GetOriginalScheduleInputDto
                            {
                                listBfAmount = updateBFAmount,
                                bookDate = listBookingHeader,
                                bookingHeaderID = bookingHeaderID.bookingHeaderID,
                                listDetailDP = trBookingDetailDP,
                                listNetNetPrice = resultUpdateNetPrice,
                                listPctTax = trBookingDetailID,
                                sellingPrice = item.sellingprice
                            };
                            var resultGetOriginalSchedule = Step13InsertTrBookingDetailSchedule(dataGetOriginalSchedule);


                            //step 14
                            var dataUpdateRemarks = new UpdateRemarksDto
                            {
                                bookingHeaderID = bookingHeaderID.bookingHeaderID,
                                sellingPrice = item.sellingprice,
                                bookCode = bookingHeaderID.message
                            };
                            var updateRemarks = Step14UpdateRemarksTrBookingHeader(dataUpdateRemarks);

                            //Step 15
                            var dataUpdateOrderStatusFullyPaid = new UpdateOrderStatusFullyPaid
                            {
                                bookingHeaderID = bookingHeaderID.bookingHeaderID,
                                orderHeaderID = input.orderHeaderID,
                                unitID = item.unitID
                            };
                            var resultUpdateOrderStatusFullyPaid = Step15UpdateOrderStatusFullyPaid(dataUpdateOrderStatusFullyPaid);

                            //Step 16
                            var dataUpdateReleaseDate = new UpdateReleaseDateInputDto
                            {
                                psCode = input.pscode,
                                renovID = item.renovID,
                                termID = item.termID,
                                unitID = item.unitID
                            };
                            var resultdataUpdateReleaseDate = Step16UpdateReleaseDate(dataUpdateReleaseDate);

                            //Step 17
                            var dataInsertTrSoldUnit = new InsertTrSoldUnitInputDto
                            {
                                sellingPrice = item.sellingprice,
                                unitID = item.unitID,
                                userID = input.userID,
                                memberCode = input.memberCode,
                                scmCode = input.scmCode,
                                netNetPrice = resultUpdateNetPrice,
                                listBookingDetail = trBookingDetailID,
                                listBookingHeader = bookingHeaderID,

                            };
                            var resultInsertTrSoldUnit = Step17InsertTrSoldUnit(dataInsertTrSoldUnit);

                            //Step 18
                            var dataInsertTrSoldUnitRequirement = new InsertTrSoldUnitRequirementInputDto
                            {
                                unitID = item.unitID,
                                userID = input.userID,
                                scmCode = input.scmCode,
                                memberCode = input.memberCode,
                                listBookingDetail = trBookingDetailID
                            };
                            var resultInsertTrSoldUnitRequirement = Step18InsertTrSoldUnitRequirement(dataInsertTrSoldUnitRequirement);

                            isSuccess = true;
                        }
                    }
                    _contextNew.SaveChanges();

                    _context.SaveChanges();

                    unitOfWork.Complete();

                    Logger.InfoFormat("UnitOfWork InsertTransactionUniversal - End ");
                }
            }

            if (isSuccess == true)
            {
                var getDataKP = (from orderHeader in _context.TR_UnitOrderHeader
                                 join orderDetail in _context.TR_UnitOrderDetail on orderHeader.Id equals orderDetail.UnitOrderHeaderID
                                 where orderHeader.Id == input.orderHeaderID
                                 select new
                                 {
                                     orderHeader.Id,
                                     orderDetail.unitID
                                 }).Distinct().ToList();

                Logger.Info("Start Loop Send Email per UnitID.");

                foreach (var item in getDataKP)
                {
                    Logger.Info("Iteration UnitID: " + item.unitID);

                    Logger.Info("Run Generate KP");
                    SendEmailKP(item.Id, item.unitID);

                }
            }

            Logger.InfoFormat("InsertTransactionUniversal - End.");

            return new ResultMessageDto { result = true, message = bookCode };
        }

        [UnitOfWork(isTransactional: false)]
        [AbpAuthorize(AppPermissions.Pages_Tenant_OnlineBooking_Transaction_DoBookingMidransReq)]
        public async Task<PaymentOnlineBookingResponse> DoBookingMidransReq(CreateTransactionUniversalDto input)
        {
            var dataUnitOrderHeader = new CreateTransactionUniversalDto
            {
                payTypeID = input.payTypeID,
                pscode = input.pscode,
                totalAmt = input.totalAmt,
                userID = input.userID,
                arrUnit = input.arrUnit,
                bankRekeningPemilik = input.bankRekeningPemilik,
                nomorRekeningPemilik = input.nomorRekeningPemilik,
                sumberDanaID = input.sumberDanaID,
                tujuanTransaksiID = input.tujuanTransaksiID

            };

            var orderID = InsertTrUnitOrderHeader(dataUnitOrderHeader);

            var getPaymentType = (from a in _lkPaymentType.GetAll()
                                  where a.Id == input.payTypeID
                                  select a.paymentType).FirstOrDefault();

            var getCustomer = (from a in _personalsRepo.GetAll()
                               join b in _trEmailRepo.GetAll()
                               on a.psCode equals b.psCode into trEmail
                               from b in trEmail.DefaultIfEmpty()
                               join c in _trPhoneRepo.GetAll()
                               on a.psCode equals c.psCode into trPhone
                               from c in trPhone.DefaultIfEmpty()
                               where a.psCode == input.pscode
                               select new
                               {
                                   a.name,
                                   b.email,
                                   c.number,
                               }).FirstOrDefault();


            var dataMidtrans = new PaymentOnlineBookingRequest();

            var dataTransaction = new transactionDetailsDto
            {
                gross_amount = (long)input.totalAmt,
                order_id = orderID.orderCode.ToString()
            };

            var dataCustomer = new customerDetailsDto
            {
                first_name = getCustomer.name,
                email = getCustomer.email,
                last_name = getCustomer.name,
                phone = getCustomer.number
            };

            var arrUnits = new List<itemDetailsDto>();

            foreach (var item in input.arrUnit)
            {
                var unitNo = (from a in _msUnitRepo.GetAll()
                              where a.Id == item.unitID
                              select a.unitNo).FirstOrDefault();

                var dataUnit = new itemDetailsDto
                {
                    id = item.unitID.ToString(),
                    name = unitNo,
                    price = (long)item.bfAmount,
                    quantity = 1
                };

                arrUnits.Add(dataUnit);
            }

            var returnMidtrans = new PaymentOnlineBookingResponse();

            var dataBankTransfer = new bankTransferDto
            {
                bank = "permata"
            };

            var dataCreditCard = new creditCard
            {
                secure = true
            };

            var dataBcaClickPay = new bcaKlikPayDto
            {
                type = "1",
                description = "Pembelian Unit"
            };

            var dataCimbClick = new cimbClicksDto
            {
                description = "Pembelian Unit"
            };

            var dataExpired = new customExpiryDto
            {
                expiry_duration = "1",
                unit = "hour"
            };

            if (getPaymentType == 4)
            {
                dataExpired = new customExpiryDto
                {
                    expiry_duration = "24",
                    unit = "hour"
                };

                dataMidtrans = new PaymentOnlineBookingRequest
                {
                    payment_type = "bank_transfer",
                    transaction_details = dataTransaction,
                    customer_details = dataCustomer,
                    item_details = arrUnits,
                    bank_transfer = dataBankTransfer,
                    custom_expiry = dataExpired
                };
                returnMidtrans = await _paymentMidtrans.CreatePayment(dataMidtrans);
            }
            else if (getPaymentType == 3)
            {

                dataMidtrans = new PaymentOnlineBookingRequest
                {

                    payment_type = "credit_card",
                    transaction_details = dataTransaction,
                    customer_details = dataCustomer,
                    item_details = arrUnits,
                    credit_card = dataCreditCard,
                    custom_expiry = dataExpired
                };
                returnMidtrans = await _paymentMidtrans.CreatePaymentLite(dataMidtrans);
            }
            else if (getPaymentType == 10)
            {
                dataMidtrans = new PaymentOnlineBookingRequest
                {
                    payment_type = "bca_klikpay",
                    transaction_details = dataTransaction,
                    customer_details = dataCustomer,
                    item_details = arrUnits,
                    bca_klikpay = dataBcaClickPay,
                    custom_expiry = dataExpired
                };
                returnMidtrans = await _paymentMidtrans.CreatePayment(dataMidtrans);
            }
            else
            {
                dataMidtrans = new PaymentOnlineBookingRequest
                {
                    payment_type = "cimb_clicks",
                    transaction_details = dataTransaction,
                    customer_details = dataCustomer,
                    item_details = arrUnits,
                    cimb_clicks = dataCimbClick,
                    custom_expiry = dataExpired
                };
                returnMidtrans = await _paymentMidtrans.CreatePayment(dataMidtrans);
            }


            ////Kirim Email
            if (input.payTypeID == 4)
            {
                foreach (var item in input.arrUnit)
                {
                    var getUnitInfo = (from a in _msUnitRepo.GetAll()
                                       join b in _msUnitCode.GetAll() on a.unitCodeID equals b.Id
                                       join c in _msClusterRepo.GetAll() on a.clusterID equals c.Id
                                       join d in _msProject.GetAll() on a.projectID equals d.Id
                                       join e in _msProjectInfo.GetAll() on d.Id equals e.projectID into projectInfo
                                       from e in projectInfo.DefaultIfEmpty()
                                       where a.Id == item.unitID
                                       select new
                                       {
                                           a.unitNo,
                                           b.unitCode,
                                           c.clusterName,
                                           d.projectName,
                                           d.image,
                                           e.projectMarketingOffice,
                                           e.projectMarketingPhone
                                       }).FirstOrDefault();

                    var getSalesInfo = (from member in _contextPers.PERSONALS_MEMBER.ToList()
                                        join phone in _contextPers.TR_Phone.ToList() on member.psCode equals phone.psCode into trPhone
                                        from c in trPhone.DefaultIfEmpty()
                                        where member.memberCode == input.memberCode
                                        select new { member, c }).FirstOrDefault();

                    var dataEmail = new AfterReservedInputDto
                    {
                        customerName = getCustomer.name,
                        BFAmount = dataUnitOrderHeader.totalAmt,
                        bankName = "Permata",
                        memberName = dataUnitOrderHeader.memberName,
                        orderCode = orderID.orderCode,
                        unitCode = getUnitInfo.unitCode,
                        unitNo = getUnitInfo.unitNo,
                        orderDate = DateTime.Now,
                        expiredDate = DateTime.Now + (TimeSpan.Parse("12:00:00")),
                        memberPhone = getSalesInfo.c.number,
                        clusterName = getUnitInfo.clusterName,
                        projectName = getUnitInfo.projectName,
                        devPhone = getUnitInfo.projectMarketingPhone,
                        marketingOffice = getUnitInfo.projectMarketingOffice,
                        vaNumber = returnMidtrans.permata_va_number,
                        projectImage = getUnitInfo.image
                    };
                    var body = _emailAppService.bodyAfterReserved(dataEmail);

                    var email = new SendEmailInputDto
                    {
                        body = body,
                        toAddress = getCustomer.email,
                        subject = " Konfirmasi Order " + dataUnitOrderHeader.orderCode + " atas Unit " + getUnitInfo.unitCode
                    };

                    _emailAppService.ConfigurationEmailBankTransfer(email);
                }
            }

            return new PaymentOnlineBookingResponse
            {
                status_code = returnMidtrans.status_code,
                status_message = returnMidtrans.status_message,
                redirect_url = returnMidtrans.redirect_url,
                permata_va_number = returnMidtrans.permata_va_number
            };

        }

        [UnitOfWork(IsDisabled = true)]
        public void DoBooking(PaymentOnlineBookingResponse input)
        {
            CreateTransactionUniversalDto data = new CreateTransactionUniversalDto();

            if (input.transaction_status.ToLowerInvariant() == "settlement")
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    Logger.InfoFormat("DoBooking() - Start.");
                    //var responseinput = _paymentMidtranshelper.ValidateResponseStatus(JsonConvert.DeserializeObject<PaymentOnlineBookingResponse>(input));

                    Logger.DebugFormat("response payment midtrans. {0}" +
                        "status code            = {1}{0}" +
                        "status message         = {2}{0}" +
                        "transaction id         = {3}{0}" +
                        "transaction time       = {4}{0}" +
                        "transaction status     = {5}{0}" +
                        "payment type           = {6}{0}" +
                        "gross amount           = {7}{0}" +
                        "order id               = {8}{0}" +
                        "error message          = {9}{0}" +
                        "fraud status           = {10}{0}" +
                        "signature key          = {11}{0}" +
                        "approval code          = {12}{0}" +
                        "billerKey|billerCode   = {13}{0}",
                    Environment.NewLine, input.status_code, input.status_message,
                        input.transaction_id, input.transaction_time, input.transaction_status,
                        input.payment_type, input.gross_amount, input.order_id,
                        input.error_messages, input.fraud_status, input.signature_key,
                        input.approval_code, input.bill_key + "|" + input.biller_code);

                    var getOrder = (from a in _trUnitOrderHeader.GetAll()
                                    where a.orderCode == input.order_id
                                    select a).FirstOrDefault();

                    var getDetail = (from a in _trUnitOrderHeader.GetAll()
                                     join b in _trUnitOrderDetail.GetAll()
                                     on a.Id equals b.UnitOrderHeaderID
                                     where a.Id == getOrder.Id
                                     select new UnitUniversalResultDto
                                     {
                                         renovID = b.renovID,
                                         sellingprice = b.sellingPrice,
                                         termID = b.termID,
                                         unitID = b.unitID
                                     }).Distinct().ToList();

                    var arrUnits = new List<UnitUniversalResultDto>();

                    Logger.Info("Start Loop ArrUnit per unit TR_UnitOrderDetail.");
                    foreach (var item in getDetail)
                    {
                        Logger.Info("Iteration unitID: " + item.unitID);

                        Logger.DebugFormat("Param ArrUnit. {0}" +
                                "renovID           = {1}{0}" +
                                "sellingprice      = {2}{0}" +
                                "termID            = {3}{0}" +
                                "unitID            = {4}{0}",

                Environment.NewLine, item.renovID, item.sellingprice, item.termID, item.unitID);

                        var dataUnit = new UnitUniversalResultDto
                        {
                            renovID = item.renovID,
                            sellingprice = item.sellingprice,
                            termID = item.termID,
                            unitID = item.unitID
                        };

                        arrUnits.Add(dataUnit);
                    }

                    Logger.DebugFormat("Param Data Customer. {0}" +
                                "payTypeID              = {1}{0}" +
                                "pscode                 = {2}{0}" +
                                "orderHeaderID          = {3}{0}" +
                                "orderCode              = {4}{0}" +
                                "arrUnit                = {5}{0}" +
                                "userID                 = {6}{0}" +
                                "memberCode             = {7}{0}" +
                                "memberName             = {8}{0}" +
                                "sumberDanaID           = {9}{0}" +
                                "bankRekeningPemilik    = {10}{0}" +
                                "nomorRekeningPemilik   = {11}{0}" +
                                "tujuanTransaksiID      = {12}{0}" +
                                "scmCode                = {13}{0}",

                Environment.NewLine, getOrder == null ? string.Empty : getOrder.paymentTypeID.ToString(),
                getOrder == null ? string.Empty : getOrder.psCode,
                getOrder == null ? string.Empty : getOrder.Id.ToString(),
                getOrder == null ? string.Empty : getOrder.orderCode,
                JsonConvert.SerializeObject(arrUnits),
                getOrder == null ? string.Empty : getOrder.userID.ToString(),
                getOrder == null ? string.Empty : getOrder.memberCode,
                getOrder == null ? string.Empty : getOrder.sumberDanaID.ToString(),
                getOrder == null ? string.Empty : getOrder.bankRekeningPemilik,
                getOrder == null ? string.Empty : getOrder.nomorRekeningPemilik,
                getOrder == null ? string.Empty : getOrder.tujuanTransaksiID.ToString(),
                getOrder == null ? string.Empty : getOrder.scmCode
                );

                    data = new CreateTransactionUniversalDto
                    {
                        payTypeID = getOrder.paymentTypeID,
                        pscode = getOrder.psCode,
                        orderHeaderID = getOrder.Id,
                        orderCode = getOrder.orderCode,
                        arrUnit = arrUnits,
                        userID = getOrder.userID,
                        memberCode = getOrder.memberCode,
                        memberName = getOrder.memberName,
                        sumberDanaID = getOrder.sumberDanaID,
                        bankRekeningPemilik = getOrder.bankRekeningPemilik,
                        nomorRekeningPemilik = getOrder.nomorRekeningPemilik,
                        tujuanTransaksiID = getOrder.tujuanTransaksiID,
                        scmCode = getOrder.scmCode
                    };

                    unitOfWork.Complete();
                }

                InsertTransactionUniversal(data);

                Logger.InfoFormat("DoBooking() - End.");

            }
            else if(input.transaction_status.ToLowerInvariant() == "expire")
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    var getStatusCancel = (from status in _lkBookingOnlineStatus.GetAll()
                                           where status.statusType == "4"
                                           select status.Id).FirstOrDefault();

                    var getOrder = (from header in _trUnitOrderHeader.GetAll()
                                    where header.orderCode == input.order_id
                                    select header).FirstOrDefault();

                    var updateCancel = getOrder.MapTo<TR_UnitOrderHeader>();

                    updateCancel.statusID = getStatusCancel;

                    _trUnitOrderHeader.Update(updateCancel);

                    var getStatusA = (from us in _lkUnitStatus.GetAll()
                                      where us.unitStatusCode == "A"
                                      select us.Id).FirstOrDefault();

                    var getUnit = (from unit in _msUnitRepo.GetAll()
                                   join detail in _trUnitOrderDetail.GetAll() on unit.Id equals detail.unitID
                                   where detail.UnitOrderHeaderID == getOrder.Id
                                   select unit).FirstOrDefault();

                    var updateUnit = getUnit.MapTo<MS_Unit>();

                    updateUnit.unitStatusID = getStatusA;

                    _msUnitRepo.Update(updateUnit);

                    var getReserved = (from reserved in _trUnitReservedRepo.GetAll()
                                       where reserved.unitID == getUnit.Id
                                       select reserved).FirstOrDefault();

                    var updateRelease = getReserved.MapTo<TR_UnitReserved>();

                    updateRelease.releaseDate = DateTime.Now;

                    _trUnitReservedRepo.Update(updateRelease);

                    unitOfWork.Complete();
                }

            }
            else
            {
                throw new UserFriendlyException("Transaksi pending");
            }

        }

        public void DoBookingMobey(CreateTransactionUniversalDto input)
        {
            using (var unitOfWork = _unitOfWorkManager.Begin())
            {
                var dataInsertTransactionUniversal = new CreateTransactionUniversalDto
                {
                    arrPP = input.arrPP,
                    arrUnit = input.arrUnit,
                    totalAmt = input.totalAmt,
                    bankRekeningPemilik = input.bankRekeningPemilik,
                    memberCode = input.memberCode,
                    memberName = input.memberName,
                    scmCode = input.scmCode,
                    sumberDanaID = input.sumberDanaID,
                    nomorRekeningPemilik = input.nomorRekeningPemilik,
                    orderCode = input.orderCode,
                    orderHeaderID = input.orderHeaderID,
                    payTypeID = input.payTypeID,
                    pscode = input.pscode,
                    tujuanTransaksiID = input.tujuanTransaksiID,
                    userID = input.userID
                };
                InsertTransactionUniversal(dataInsertTransactionUniversal);

                unitOfWork.Complete();
            }
        }

        public void SchedulerBookingReminder()
        {
            var data = (from orderDetail in _trUnitOrderDetail.GetAll()
                        join orderHeader in _trUnitOrderHeader.GetAll()
                        on orderDetail.UnitOrderHeaderID equals orderHeader.Id
                        where orderDetail.bookingHeaderID == null
                        && orderHeader.paymentTypeID == (from a in _lkPaymentType.GetAll()
                                                         where a.paymentType == 4
                                                         select a.Id).FirstOrDefault()
                        && orderHeader.statusID == (from status in _lkBookingOnlineStatus.GetAll()
                                                    where status.statusTypeName == "Outstanding"
                                                    select status.Id).FirstOrDefault()
                        orderby orderDetail.CreationTime descending
                        select new { orderDetail, orderHeader }).Distinct().ToList();

            TimeSpan banding = TimeSpan.Parse("10:00:00");
            
            foreach (var item in data)
            {
                if (DateTime.Now - (item.orderDetail.CreationTime) >= banding)
                {
                    var getCustInfo = (from personals in _contextPers.PERSONAL.ToList()
                                       where personals.psCode == item.orderHeader.psCode
                                       select personals).FirstOrDefault();

                    var getUnitInfo = (from a in _msUnitRepo.GetAll()
                                       join b in _msUnitCode.GetAll() on a.unitCodeID equals b.Id
                                       join c in _msClusterRepo.GetAll() on a.clusterID equals c.Id
                                       where a.Id == item.orderDetail.unitID
                                       select new
                                       {
                                           a.unitNo,
                                           b.unitCode,
                                           c.clusterName,
                                           a.projectID
                                       }).FirstOrDefault();

                    var getProjectInfo = (from project in _msProject.GetAll()
                                          join info in _msProjectInfo.GetAll() on project.Id equals info.projectID into a
                                          from projectInfo in a.DefaultIfEmpty()
                                          where project.Id == getUnitInfo.projectID
                                          orderby projectInfo.CreationTime descending
                                          select new { project, projectInfo }).FirstOrDefault();

                    var getSalesInfo = (from member in _contextPers.PERSONALS_MEMBER.ToList()
                                        join phone in _contextPers.TR_Phone.ToList() on member.psCode equals phone.psCode into trPhone
                                        from c in trPhone.DefaultIfEmpty()
                                        where member.memberCode == item.orderHeader.memberCode
                                        select new { member, c }).FirstOrDefault();

                    var statusMidtrans = _paymentMidtrans.CheckPaymentStatus(item.orderHeader.Id.ToString());

                    var sendEmail = new Reminder2JamInputDto
                    {
                        bookingFee = item.orderDetail.sellingPrice,
                        customerName = getCustInfo.name,
                        devPhone = getProjectInfo.projectInfo.projectMarketingPhone != null ? getProjectInfo.projectInfo.projectMarketingPhone : "-",
                        marketingOffice = getProjectInfo.projectInfo.projectMarketingOffice != null ? getProjectInfo.projectInfo.projectMarketingOffice : "-",
                        memberName = item.orderHeader.memberName,
                        memberPhone = getSalesInfo.c.number != null ? getSalesInfo.c.number : "-",
                        orderDate = item.orderHeader.orderDate,
                        projectImage = getProjectInfo.project.image,
                        projectName = getProjectInfo.project.projectName,
                        unitCode = getUnitInfo.unitCode,
                        unitNo = getUnitInfo.unitNo,
                        bankName = "Permata",
                        vaNumber = statusMidtrans.Result.permata_va_number == null ? "_________" : statusMidtrans.Result.permata_va_number
                    };
                    var body = _emailAppService.Reminder2Jam(sendEmail);

                    var email = new SendEmailInputDto
                    {
                        body = body,
                        toAddress = item.orderHeader.psEmail /*"keniamalia1@gmail.com"*/, //dataUnitOrderHeader.psEmail,
                        subject = "Reminder Booking Fee Payment atas Order" + item.orderHeader.orderCode
                    };

                    _emailAppService.ConfigurationEmail(email);
                }
            }
        }

        [UnitOfWork(IsDisabled = true)]
        public GenerateKPResultDto GenerateKP(int orderID, int unitID)
        {
            string trimResult;

            var getResult = new GenerateKPResultDto();
            
            using (var unitOfWork = _unitOfWorkManager.Begin())
            {
                Logger.InfoFormat("GenerateKP - Start.");

                Logger.DebugFormat("Param Generate KP. {0}" +
                            "orderID              = {1}{0}" +
                            "unitID               = {2}{0}",

                Environment.NewLine, orderID, unitID);

                Logger.InfoFormat("Get Data KP Page 1 - Start.");

                var getData = (from orderHeader in _trUnitOrderHeader.GetAll()
                               join orderDetail in _trUnitOrderDetail.GetAll() on orderHeader.Id equals orderDetail.UnitOrderHeaderID
                               join bookingHeader in _trBookingHeaderRepo.GetAll() on orderDetail.bookingHeaderID equals bookingHeader.Id
                               join unit in _msUnitRepo.GetAll() on bookingHeader.unitID equals unit.Id
                               join tujuanTransaksi in _msTujuanTransaksi.GetAll() on bookingHeader.tujuanTransaksiID equals tujuanTransaksi.Id into tj
                               from tujuanTransaksi in tj.DefaultIfEmpty()
                               join sumberDana in _msSumberDana.GetAll() on bookingHeader.sumberDanaID equals sumberDana.Id into sd
                               from sumberDana in sd.DefaultIfEmpty()
                               join project in _msProject.GetAll() on unit.projectID equals project.Id
                               where orderHeader.Id == orderID && orderDetail.unitID == unitID
                               select new KonfirmasiPesananDto
                               {
                                   orderCode = orderHeader.orderCode == null ? string.Empty : orderHeader.orderCode,
                                   BookCode = bookingHeader.bookCode == null ? string.Empty : bookingHeader.bookCode,
                                   tanggalBooking = String.Format("{0: dd MMM yyyy}", bookingHeader.bookDate),
                                   psName = orderHeader.psName == null ? string.Empty : orderHeader.psName,
                                   kodePelanggan = orderHeader.psCode == null ? string.Empty : orderHeader.psCode,
                                   noHpPembeli = orderHeader.psPhone == null ? string.Empty : orderHeader.psPhone,
                                   caraPembayaran = bookingHeader.termRemarks == null ? string.Empty : bookingHeader.termRemarks,
                                   bfAmount = orderDetail.BFAmount.ToString("C", CultureInfo.CreateSpecificCulture("id-ID")).Replace("Rp", ""),
                                   email = orderHeader.psEmail == null ? string.Empty : orderHeader.psEmail,
                                   hargaJual = orderDetail.sellingPrice.ToString("C", CultureInfo.CreateSpecificCulture("id-ID")).Replace("Rp", ""),
                                   namaDealCloser = orderHeader.memberName == null ? string.Empty : orderHeader.memberName,
                                   noDealCloser = orderHeader.memberCode == null ? string.Empty : orderHeader.memberCode,
                                   unitID = bookingHeader.unitID,
                                   renovID = orderDetail.renovID,
                                   namaBank = bookingHeader.bankRekeningPemilik,
                                   noRekening = bookingHeader.nomorRekeningPemilik,
                                   tujuanTransaksi = tujuanTransaksi.tujuanTransaksiName,
                                   sumberDanaPembelian = sumberDana.sumberDanaName,
                                   imageProject = project.image == null ? string.Empty : project.image,
                                   termID = bookingHeader.termID,
                                   BF = orderDetail.BFAmount == 0 ? 0 : orderDetail.BFAmount,
                                   SP = orderDetail.sellingPrice == 0 ? 0 : orderDetail.sellingPrice,
                                   bookingHeaderID = bookingHeader.Id
                               }).FirstOrDefault();

                Logger.DebugFormat("Result Get Data KP Page 1 From Property System. {0}" +
                    "orderCode        = {1}{0}" +
                    "BookCode         = {2}{0}" +
                    "tanggalBooking   = {3}{0}" +
                    "psName           = {4}{0}" +
                    "kodePelanggan    = {5}{0}" +
                    "noHpPembeli      = {6}{0}" +
                    "caraPembayaran   = {7}{0}" +
                    "bfAmount         = {8}{0}" +
                    "email            = {9}{0}" +
                    "hargaJual        = {10}{0}" +
                    "namaDealCloser   = {11}{0}" +
                    "noDealCloser     = {12}{0}" +
                    "unitID           = {13}{0}" +
                    "renovID          = {14}{0}" +
                    "namaBank         = {15}{0}" +
                    "noRekening       = {16}{0}" +
                    "tujuanTransaksi  = {17}{0}" +
                    "sumberDanaPembelian =  {18}{0}" +
                    "imageProject        =  {19}{0}",
                Environment.NewLine, getData.orderCode, getData.BookCode, getData.tanggalBooking, getData.psName
                                    , getData.kodePelanggan, getData.noHpPembeli, getData.caraPembayaran, getData.bfAmount
                                    , getData.email, getData.hargaJual, getData.namaDealCloser, getData.noDealCloser, getData.unitID
                                    , getData.renovID, getData.namaBank, getData.noRekening, getData.tujuanTransaksi, getData.sumberDanaPembelian
                                    , getData.imageProject);

                //Logger.InfoFormat("Convert Image Project To Base64 String - Start.");

                if(getData.imageProject != null || getData.imageProject != string.Empty)
                {
                    var webRoot = _hostingEnvironment.WebRootPath;

                    getData.imageProject = Path.Combine(webRoot + getData.imageProject);

                    Logger.DebugFormat("Result Image Project with webRoot {0}" +
                        "webRoot      = {1}{0}" +
                    Environment.NewLine, getData.imageProject == null ? string.Empty : getData.imageProject);

                    if (File.Exists(getData.imageProject))
                    {

                        byte[] imageByte = System.IO.File.ReadAllBytes(getData.imageProject);

                        getData.imageProject = Convert.ToBase64String(imageByte);

                    }
                    else
                    {
                        getData.imageProject = string.Empty;
                    }

                    Logger.DebugFormat("Result Image Project Base64 {0}" +
                        "webRoot      = {1}{0}" +
                    Environment.NewLine, getData.imageProject == null ? string.Empty : getData.imageProject);

                    Logger.InfoFormat("Convert Image Project To Base64 String - End.");
                }
                
                Logger.InfoFormat("Get Data Personals - Start.");

                var getPersonal = (from b in _contextPers.PERSONALS_MEMBER
                                   join c in _contextPers.TR_Phone on b.psCode equals c.psCode into phone
                                   from c in phone.DefaultIfEmpty()
                                   join d in _contextPers.PERSONAL on getData.kodePelanggan equals d.psCode
                                   join e in _contextPers.TR_ID on d.psCode equals e.psCode
                                   where new string[] { "1", "3", "5", "7" }.Contains(e.idType) && b.memberCode == getData.noDealCloser
                                   select new
                                   {
                                       birthdate = String.Format("{0: dd MMM yyyy}", d.birthDate),
                                       memberPhone = c.number == null ? string.Empty : c.number,
                                       npwp = d.NPWP == null ? string.Empty : d.NPWP,
                                       noKtp = e.idNo == null ? string.Empty : e.idNo
                                   }).FirstOrDefault();

                //Logger.DebugFormat("Result Get Data KP Page 1 From Personals. {0}" +
                //    "birthdate        = {1}{0}" +
                //    "memberPhone      = {2}{0}" +
                //    "npwp             = {3}{0}" ,
                //Environment.NewLine, getPersonal.birthdate == null ? string.Empty : getPersonal.birthdate,
                //                     getPersonal.memberPhone == null ? string.Empty : getPersonal.memberPhone,
                //                     getPersonal.npwp == null ? string.Empty : getPersonal.npwp);

                if (getPersonal == null)
                {
                    getData.birthDate = string.Empty;
                    getData.noHp = string.Empty;
                    getData.noNPWP = string.Empty;
                    getData.noIdentitas = string.Empty;
                }
                else
                {
                    getData.birthDate = getPersonal.birthdate;
                    getData.noHp = getPersonal.memberPhone;
                    getData.noNPWP = getPersonal.npwp;
                    getData.noIdentitas = getPersonal.noKtp;
                }
                
                Logger.InfoFormat("Get Data Personals - End.");

                Logger.InfoFormat("Get ItemID Tanah & Bangunan - Start.");

                var getItem1 = (from a in _context.LK_Item
                                where a.itemCode == "01"
                                select a.Id).FirstOrDefault();

                Logger.DebugFormat("Result ItemID Tanah {0}" +
                    "itemID      = {1}{0}" +
                Environment.NewLine, getItem1);

                var getItem2 = (from a in _context.LK_Item
                                where a.itemCode == "02"
                                select a.Id).FirstOrDefault();

                Logger.DebugFormat("Result ItemID Bangunan {0}" +
                    "itemID      = {1}{0}" +
                Environment.NewLine, getItem2);

                Logger.InfoFormat("Get ItemID Tanah & Bangunan - End.");

                Logger.InfoFormat("Get Unit - Start.");

                Logger.Info("Iteration : unitID " + getData.unitID);

                var getUnit = (from unit in _context.MS_Unit
                               join cluster in _context.MS_Cluster on unit.clusterID equals cluster.Id
                               join unitItem in _context.MS_UnitItem on unit.Id equals unitItem.unitID
                               join detail in _context.MS_Detail on unit.detailID equals detail.Id
                               join renov in _context.MS_Renovation on getData.renovID equals renov.Id
                               join category in _context.MS_Category on unit.categoryID equals category.Id
                               join unitCode in _context.MS_UnitCode on unit.unitCodeID equals unitCode.Id
                               where unit.Id == unitID
                               select new ListunitDto
                               {
                                   category = category.categoryCode == null ? string.Empty : category.categoryCode,
                                   UnitCode = unitCode.unitCode == null ? string.Empty : unitCode.unitCode,
                                   UnitNo = unit.unitNo == null ? string.Empty : unit.unitNo,
                                   tipe = detail.detailName == null ? string.Empty : detail.detailName,
                                   cluster = cluster.clusterName == null ? string.Empty : cluster.clusterName,
                                   renovation = renov.renovationName == null ? string.Empty : renov.renovationName,
                                   item = unitItem.itemID == 0 ? 0 : unitItem.itemID,
                                   luas = unitItem.area.ToString() == null ? string.Empty : unitItem.area.ToString()
                               }).ToList();

                var getUnitLog = getUnit.FirstOrDefault();

                Logger.DebugFormat("Result Get Unit {0}" +
                    "category      = {1}{0}" +
                    "unitCoe       = {2}{0}" +
                    "unitNo        = {3}{0}" +
                    "tipe          = {4}{0}" +
                    "cluster       = {5}{0}" +
                    "renovation    = {6}{0}" +
                    "item          = {7}{0}" +
                    "luas          = {8}{0}" ,
                Environment.NewLine, getUnitLog.category, getUnitLog.UnitCode, getUnitLog.UnitNo, getUnitLog.tipe,
                                     getUnitLog.cluster, getUnitLog.renovation, getUnitLog.item, getUnitLog.luas);

                if (getUnit != null)
                {
                    Logger.Info("Get Unit Is Not Null");

                    if (getUnit.FirstOrDefault().category.ToLowerInvariant() == "lan")
                    {
                        Logger.Info("Iteration : Category Landed");

                        getUnit = (from a in getUnit
                                   where a.item == getItem1
                                   select a).ToList();
                    }
                    else
                    {
                        Logger.Info("Iteration : Category Highrise");

                        getUnit = (from a in getUnit
                                   where a.item == getItem2
                                   select a).ToList();
                    }
                }

                getData.listUnit = getUnit;

                Logger.InfoFormat("Get Unit - End.");

                Logger.InfoFormat("Get Bank - Start.");

                var dataBank = (from bank in _context.MS_BankOLBooking
                                join unit in _context.MS_Unit on new { bank.projectID, bank.clusterID } equals new { unit.projectID, unit.clusterID }
                                where unit.Id == getData.unitID
                                select new listBankDto
                                {
                                    bankName = bank.bankName == null ? string.Empty : bank.bankName,
                                    noVA = bank.bankRekNo == null ? string.Empty : bank.bankRekNo
                                }).ToList();

                Logger.InfoFormat(JsonConvert.SerializeObject(dataBank));

                getData.listBank = dataBank;

                Logger.InfoFormat("Get Bank - End.");

                Logger.InfoFormat("Get KP Page 1 - End.");

                var listTerm = new List<listIlustrasiPembayaran>();

                Logger.InfoFormat("Get Data Term Yang Dipilih - Start.");

                var dataTerm1 = new listIlustrasiPembayaran
                {
                    termID = getData.termID,
                    termName = getData.caraPembayaran,
                    bookingFee = getData.BF,
                    totalBayar = getData.SP.ToString("C", CultureInfo.CreateSpecificCulture("id-ID")).Replace("Rp", ""),
                    sellingPrice = getData.SP - getData.BF,
                    tglJatuhTempo = Convert.ToDateTime(getData.tanggalBooking)
                };

                Logger.DebugFormat("Result Get Term Yang di Pilih {0}" +
                    "Term ID       = {1}{0}" +
                    "Term Name     = {2}{0}" +
                    "Booking Fee   = {3}{0}" +
                    "Total Bayar   = {4}{0}" +
                    "Selling Price = {5}{0}" +
                    "Book Date     = {6}{0}" ,
                Environment.NewLine, dataTerm1.termID, dataTerm1.termName, dataTerm1.bookingFee, dataTerm1.totalBayar,
                                     dataTerm1.sellingPrice, dataTerm1.tglJatuhTempo);

                listTerm.Add(dataTerm1);

                Logger.InfoFormat("Get Data Term Yang Dipilih - End.");

                Logger.InfoFormat("Get Data 5 Term - Start.");

                var getTerm5 = (from orderheader in _context.TR_UnitOrderHeader
                                join orderDetail in _context.TR_UnitOrderDetail on orderheader.Id equals orderDetail.UnitOrderHeaderID
                                join bookingHeader in _trBookingHeaderRepo.GetAll() on orderDetail.bookingHeaderID equals bookingHeader.Id
                                join headerTerm in _trBookingHeaderTerm.GetAll() on bookingHeader.Id equals headerTerm.bookingHeaderID
                                join itemPrice in _trBookingItemPrice.GetAll() on headerTerm.termID equals itemPrice.termID
                                where headerTerm.termID != getData.termID && bookingHeader.unitID == unitID
                                select new listIlustrasiPembayaran
                                {
                                    termID = headerTerm.termID,
                                    termName = headerTerm.remarks,
                                    bookingFee = getData.BF,
                                    tglJatuhTempo = Convert.ToDateTime(getData.tanggalBooking)
                                }).Distinct().ToList();

                Logger.InfoFormat(JsonConvert.SerializeObject(getTerm5));

                if (getTerm5.Count() >= 5)
                {
                    Logger.InfoFormat("Term lebih dari 5 - Start.");

                    getTerm5.Take(5).ToList();
                }

                Logger.InfoFormat("Get SellingPrice 5 Term - Start.");

                Logger.InfoFormat("Start Loop 5 Term.");

                foreach (var item in getTerm5)
                {
                    Logger.Info("Iteration : TermID" + item.termID);

                    var getGp = (from bookingHeader in _trBookingHeaderRepo.GetAll()
                                 join itemPrice in _trBookingItemPrice.GetAll() on bookingHeader.Id equals itemPrice.bookingHeaderID
                                 join renov in _context.MS_Renovation on itemPrice.renovCode equals renov.renovationCode into rnv
                                 from renov in rnv.DefaultIfEmpty()
                                 join headerTerm in _trBookingHeaderTerm.GetAll() on itemPrice.termID equals headerTerm.termID
                                 group itemPrice by new
                                 {
                                     itemPrice.termID,
                                     bookingHeader.Id,
                                     renovID = renov.Id
                                 } into G
                                 where G.Key.termID == item.termID && G.Key.Id == getData.bookingHeaderID && G.Key.renovID == getData.renovID
                                 select new SellingPriceDto
                                 {
                                     price = G.Sum(a => a.grossPrice)
                                 }).FirstOrDefault();

                    Logger.DebugFormat("Result Get GrossPrice Per Term {0}" +
                                       "grossPrice      = {1}{0}" +
                                       Environment.NewLine, getGp.price);

                    var getSalesDisc = (from salesDisc in _trBookingSalesDisc.GetAll()
                                        where salesDisc.bookingHeaderID == getData.bookingHeaderID
                                        select new
                                        {
                                            salesDisc.pctDisc,
                                            salesDisc.pctTax
                                        }).FirstOrDefault();

                    Logger.DebugFormat("Result Get Sales Discount Per Term {0}" +
                                       "pctDisc      = {1}{0}" +
                                       "pctTax      = {2}{0}" +
                                       Environment.NewLine, getSalesDisc.pctDisc, getSalesDisc.pctTax);

                    getGp.price = getGp.price * (1 - (decimal)getSalesDisc.pctDisc);

                    Logger.DebugFormat("Result Net Price Per Term {0}" +
                                       "netPrice      = {1}{0}" +
                                       Environment.NewLine, getGp.price);

                    var getAddDisc = (from addDisc in _trBookingHeaderTerm.GetAll()
                                      where addDisc.termID == item.termID && addDisc.bookingHeaderID == getGp.bookingHeaderID
                                      select addDisc).Distinct().ToList();

                    Logger.InfoFormat("Start Loop Add Discount.");
                    foreach (var addDisc in getAddDisc)
                    {
                        Logger.InfoFormat("Iteration: addDiscNo" + addDisc.addDiscNo);

                        getGp.price = getGp.price * (1 - (decimal)addDisc.addDisc);

                        Logger.DebugFormat("Result NetNetPrice Per Term {0}" +
                                       "netNetPrice      = {1}{0}" +
                                       Environment.NewLine, getGp.price);
                    }

                    getGp.price = getGp.price * (1 + (decimal)getSalesDisc.pctTax);

                    Logger.DebugFormat("Result Selling Price Per Term {0}" +
                                       "sellingPrice      = {1}{0}" +
                                       Environment.NewLine, getGp.price);

                    item.totalBayar = getGp.price.ToString("C", CultureInfo.CreateSpecificCulture("id-ID")).Replace("Rp", "");

                    item.sellingPrice = getGp.price - item.bookingFee;

                    Logger.DebugFormat("Result Selling Price - BookingFee Per Term {0}" +
                                       "Price      = {1}{0}" +
                                       Environment.NewLine, item.sellingPrice);
                }

                Logger.InfoFormat("End Loop 5 Term.");

                listTerm.AddRange(getTerm5);

                Logger.InfoFormat("Get Data 5 Term - End.");

                getData.ilustrasiPembayaran = listTerm;

                Logger.InfoFormat("Get Data DP - Start.");

                var dataDP = new List<DPNoDto>();

                Logger.InfoFormat("Start Loop List All Term.");

                foreach (var item in listTerm)
                {
                    Logger.InfoFormat("Iteration: termID " + item.termID);

                    var data = new List<DPNoDtos>();

                    var getDPNo = (from termDP in _context.MS_TermDP
                                   where termDP.termID == item.termID
                                   select new DPNoDtos
                                   {
                                       DPNo = termDP.DPNo,
                                       tglJatuhTempo = item.tglJatuhTempo.AddDays(termDP.daysDue)
                                   }).Distinct().ToList();

                    Logger.InfoFormat("Result DP No PerTerm: " + JsonConvert.SerializeObject(getDPNo));

                    data.AddRange(getDPNo);

                    var DPCount = new DPNoDto
                    {
                        countDP = data.Count,
                        listDPNos = data,
                        termID = item.termID
                    };

                    Logger.DebugFormat("Result DPCount {0}" +
                                       "countDP      = {1}{0}" +
                                       "listDPNos    = {2}{0}" +
                                       "termID       = {3}{0}" ,
                                       Environment.NewLine, DPCount.countDP, JsonConvert.SerializeObject(DPCount.listDPNos), DPCount.termID);

                    dataDP.Add(DPCount);

                }

                Logger.InfoFormat("End Loop List All Term.");

                var DPNo = dataDP.OrderByDescending(a => a.countDP).FirstOrDefault();

                Logger.InfoFormat("Get List DPNo Terbanyak");

                var allAmountDP = new List<listAmountDto>();

                Logger.InfoFormat("Start Loop List DPNo.");
                foreach (var item in DPNo.listDPNos)
                {
                    Logger.InfoFormat("Iteration DPNo " + item.DPNo);

                    var listAmount = new List<listAmountDto>();

                    Logger.InfoFormat("Start Loop List Term.");

                    foreach (var term in listTerm)
                    {
                        Logger.InfoFormat("Iteration termID " + term.termID);

                        var getDPPct = (from a in _context.MS_TermDP
                                        where a.termID == term.termID
                                        select new
                                        {
                                            a.DPPct,
                                            term.tglJatuhTempo,
                                            a.daysDue
                                        }).ToList();

                        Logger.InfoFormat("Result getDPPct: " + JsonConvert.SerializeObject(getDPPct));

                        var DPPct = getDPPct.Where(a => a.tglJatuhTempo.AddDays(a.daysDue) == item.tglJatuhTempo).Select(a => a.DPPct).FirstOrDefault();

                        Logger.DebugFormat("Result DPPct {0}" +
                                           "DPPct      = {1}{0}" ,
                                           Environment.NewLine, DPPct);

                        if (DPPct != 0)
                        {
                            var dataAmount = new listAmountDto()
                            {
                                amount = (term.sellingPrice * (decimal)DPPct),
                                termID = term.termID
                            };

                            Logger.DebugFormat("Result dataAmount {0}" +
                                               "amount      = {1}{0}" +
                                               "termID      = {2}{0}" ,
                                               Environment.NewLine, dataAmount.amount, dataAmount.termID);

                            listAmount.Add(dataAmount);
                            allAmountDP.Add(dataAmount);
                        }
                        else
                        {
                            var dataAmount = new listAmountDto()
                            {
                                amount = 0,
                                termID = term.termID
                            };

                            Logger.DebugFormat("Result dataAmount {0}" +
                                              "amount      = {1}{0}" +
                                              "termID      = {2}{0}" ,
                                              Environment.NewLine, dataAmount.amount, dataAmount.termID);

                            listAmount.Add(dataAmount);
                            allAmountDP.Add(dataAmount);
                        }
                    }

                    item.listAmount = listAmount;
                }

                Logger.InfoFormat("End Loop List Term.");

                getData.listDPNo = DPNo.listDPNos;

                Logger.InfoFormat("Result ListDP: " + JsonConvert.SerializeObject(getData.listDPNo));

                var getTotalDP = (from totalDP in allAmountDP
                                  group totalDP by new
                                  {
                                      totalDP.termID
                                  } into G
                                  select new
                                  {
                                      G.Key.termID,
                                      DP = G.Sum(a => a.amount)
                                  }).ToList();

                Logger.InfoFormat("Result List Total DP: " + JsonConvert.SerializeObject(getTotalDP));

                Logger.InfoFormat("Get Data DP - End.");

                Logger.InfoFormat("Get Cicilan & Pelunasan - Start.");

                var listPelunasan1 = new List<ListPenulasanDto>();
                var listPelunasan2 = new List<ListPenulasanDto>();
                var listFin = new List<listFinTimesDto>();

                Logger.InfoFormat("Start Loop List Term");

                for (var i = 1; i <= listTerm.Count(); i++)
                {
                    Logger.InfoFormat("Iteration I " + i);

                    var listCicil = new List<listCicilanDto>();

                    var totalDP = getTotalDP.Where(a => a.termID == listTerm[i - 1].termID).FirstOrDefault();

                    Logger.DebugFormat("Result Total DP PerTerm {0}" +
                                       "totalDP      = {1}{0}" ,
                                       Environment.NewLine, totalDP);

                    listTerm[i - 1].totalDP = totalDP.DP;

                    listTerm[i - 1].sellingPrice = listTerm[i - 1].sellingPrice - totalDP.DP;

                    var getFinTimes = (from headerTerm in _trBookingHeaderTerm.GetAll()
                                       join finType in _context.LK_FinType on headerTerm.finTypeCode equals finType.finTypeCode
                                       where headerTerm.bookingHeaderID == getData.bookingHeaderID && headerTerm.termID == listTerm[i - 1].termID
                                       select new listFinTimesDto
                                       {
                                           finStartDue = headerTerm.finStartDue,
                                           termID = listTerm[i - 1].termID,
                                           finTimes = finType.finTimes
                                       }).FirstOrDefault();

                    Logger.DebugFormat("Result Get FinTimes {0}" +
                                       "finStartDue      = {1}{0}" +
                                       "termID           = {2}{0}" +
                                       "finTimes         = {3}{0}" ,
                                       Environment.NewLine, getFinTimes.finStartDue, getFinTimes.termID, getFinTimes.finTimes);

                    var totalCicilan = listTerm[i - 1].sellingPrice;

                    Logger.DebugFormat("Result Total Cicilan {0}" +
                                       "totalCicilan     = {1}{0}" ,
                                       Environment.NewLine, totalCicilan);

                    if (getFinTimes.finTimes != 1)
                    {
                        Logger.InfoFormat("Fin Times Lebih dari 1");
                        Logger.InfoFormat("Start Loop FinTimes");

                        for (var x = 1; x <= getFinTimes.finTimes; x++)
                        {
                            Logger.InfoFormat("Iteration X " + x);

                            var dataCicilanAmt = new listCicilanDto()
                            {
                                amount = totalCicilan / getFinTimes.finTimes,
                                cicilanNo = x
                            };

                            Logger.DebugFormat("Result Data Cicilan Amount{0}" +
                                       "amount     = {1}{0}" +
                                       "amount     = {2}{0}" ,
                                       Environment.NewLine, dataCicilanAmt.amount, dataCicilanAmt.cicilanNo);

                            listCicil.Add(dataCicilanAmt);
                        }

                        Logger.InfoFormat("End Loop FinTimes");

                        listTerm[i - 1].listCicilan = listCicil;
                    }
                    else
                    {
                        Logger.InfoFormat("Fin Times = 1");

                        var dataPelunasan = new ListPenulasanDto
                        {
                            amount = totalCicilan,
                            termID = listTerm[i - 1].termID,
                            tglJatuhTempo = listTerm[i - 1].tglJatuhTempo.AddDays(getFinTimes.finStartDue)
                        };

                        Logger.DebugFormat("Result Data Pelunasan {0}" +
                                       "amount     = {1}{0}" +
                                       "amount     = {2}{0}" ,
                                       Environment.NewLine, dataPelunasan.amount, dataPelunasan.termID, dataPelunasan.tglJatuhTempo);

                        if (i == 1)
                        {
                            listPelunasan1.Add(dataPelunasan);

                            var dataPelunasan2 = new ListPenulasanDto
                            {
                                amount = 0,
                                termID = listTerm[i - 1].termID,
                                tglJatuhTempo = listTerm[i - 1].tglJatuhTempo.AddDays(getFinTimes.finStartDue)
                            };
                            listPelunasan2.Add(dataPelunasan2);
                        }
                        else
                        {
                            listPelunasan2.Add(dataPelunasan);

                            var dataPelunasan1 = new ListPenulasanDto
                            {
                                amount = 0,
                                termID = listTerm[i - 1].termID,
                                tglJatuhTempo = listTerm[i - 1].tglJatuhTempo.AddDays(getFinTimes.finStartDue)
                            };
                            listPelunasan1.Add(dataPelunasan1);
                        }
                    }

                    listFin.Add(getFinTimes);
                }
                Logger.InfoFormat("End Loop List Term");

                var finTimesTerbersar = listFin.OrderByDescending(a => a.finTimes).FirstOrDefault();

                Logger.InfoFormat("Fin Times Terbesar: " + finTimesTerbersar);

                var listCicilan = new List<listCicilans>();

                Logger.InfoFormat("Start Loop FinTimes");
                for (var i = 1; i <= finTimesTerbersar.finTimes; i++)
                {
                    Logger.InfoFormat("Iteration I: " + i);

                    var getTgl = DPNo.listDPNos.OrderByDescending(a => a.tglJatuhTempo).FirstOrDefault();

                    var getTglJatuhTempo = getTgl.tglJatuhTempo.AddDays(finTimesTerbersar.finStartDue);

                    Logger.InfoFormat("Tanggal Jatuh Tempo Cicilan Pertama: " + getTglJatuhTempo);

                    if (i == 1)
                    {
                        var dataCicilan = new listCicilans
                        {
                            cicilanNo = i,
                            tglJatuhTempo = getTglJatuhTempo
                        };

                        Logger.DebugFormat("Result Data Cicilan {0}" +
                                       "cicilanNo  = {1}{0}" +
                                       "tglJatuhTempo     = {2}{0}" ,
                                       Environment.NewLine, dataCicilan.cicilanNo, dataCicilan.tglJatuhTempo);

                        listCicilan.Add(dataCicilan);

                    }
                    else
                    {
                        var dataCicilan = new listCicilans
                        {
                            cicilanNo = i,
                            tglJatuhTempo = getTglJatuhTempo.AddMonths(i)
                        };

                        Logger.DebugFormat("Result Data Cicilan {0}" +
                                       "cicilanNo  = {1}{0}" +
                                       "tglJatuhTempo     = {2}{0}" ,
                                       Environment.NewLine, dataCicilan.cicilanNo, dataCicilan.tglJatuhTempo);

                        listCicilan.Add(dataCicilan);
                    }
                }
                Logger.InfoFormat("End Loop FinTimes");

                Logger.InfoFormat("Start Loop ListCicilan");
                foreach (var cicilan in listCicilan)
                {
                    Logger.InfoFormat("Iteration cicilan:" + cicilan.cicilanNo);

                    var amtCicilan = new List<listCicilanAmount>();

                    Logger.InfoFormat("Start Loop List Tern");
                    foreach (var dto in listTerm)
                    {
                        Logger.InfoFormat("Iteration Term:" + dto.termID);

                        if (dto.listCicilan != null)
                        {
                            var getAmount = (from a in dto.listCicilan
                                             where a.cicilanNo == cicilan.cicilanNo
                                             select a.amount).FirstOrDefault();

                            Logger.DebugFormat("Result Data getAmount {0}" +
                                       "amount  = {1}{0}" +
                                       Environment.NewLine, getAmount);

                            var amountCicilan = new listCicilanAmount
                            {
                                amount = getAmount
                            };
                            amtCicilan.Add(amountCicilan);
                        }
                        else
                        {
                            var amountCicilan = new listCicilanAmount
                            {
                                amount = 0
                            };
                            amtCicilan.Add(amountCicilan);
                        }
                    }
                    Logger.InfoFormat("End Loop List Term");
                    cicilan.listCicilanAmount = amtCicilan;
                }



                Logger.InfoFormat("Start Loop List Cicilan");

                getData.listPelunasan2 = listPelunasan2;
                getData.listPelunasan1 = listPelunasan1;
                getData.listCicilan = listCicilan;

                Logger.InfoFormat("Get Cicilan & Pelunasan - End.");


                Logger.InfoFormat("Request API KP - Start.");

                using (var client = new WebClient())
                {
                    var url = _configuration.ApiPdfUrl.EnsureEndsWith('/') + "api/Pdf/KonfirmasiPesananPdf";
                    client.Headers.Add("Content-Type:application/json");
                    client.Headers.Add("Accept:application/json");
                    var result = client.UploadString(url, JsonConvert.SerializeObject(getData));
                    Logger.InfoFormat(result);
                    trimResult = result.Replace(@"\\", @"\").Trim(new char[1] { '"' });
                    Logger.InfoFormat(trimResult);

                    Logger.InfoFormat("Path Local KP: " + trimResult);

                }
                unitOfWork.Complete();

                getResult = new GenerateKPResultDto
                {
                    unitCode = getData.listUnit.FirstOrDefault().UnitCode,
                    unitNo = getData.listUnit.FirstOrDefault().UnitNo,
                    bookDate = getData.tanggalBooking,
                    memberName = getData.namaDealCloser,
                    memberPhone = getData.noHp,
                    pathKP = trimResult,
                    psEmail = getData.email,
                    psName = getData.psName
                };

                Logger.Info("Unit Of Work Complete");
            }
            return getResult;
        }

        [UnitOfWork(isTransactional: false)]
        public string SendEmailKP(int orderID, int unitID)
        {
            Logger.InfoFormat("Get Data Email - Start.");

            Logger.InfoFormat("Get Data Email - orderID : " + orderID + "unitID " + unitID);

            using (var unitOfWork = _unitOfWorkManager.Begin())
            {
                var dataKP = GenerateKP(orderID, unitID);



            var getProjectInfo = (from project in _context.MS_Project
                                  join info in _context.MS_ProjectInfo on project.Id equals info.projectID into a
                                  from projectInfo in a.DefaultIfEmpty()
                                  join unit in _context.MS_Unit on project.Id equals unit.projectID
                                  where unit.Id == unitID
                                  orderby projectInfo.CreationTime descending
                                  select new
                                  {
                                      project.projectName,
                                      projectInfo.projectMarketingOffice,
                                      projectInfo.projectMarketingPhone,
                                      projectInfo.projectImageLogo
                                  }).FirstOrDefault();

            Logger.DebugFormat("Result Data Project Info {0}" +
                                   "Project Name                = {1}{0}" +
                                   "Project Marketing Office    = {2}{0}" +
                                   "project Marketing Phone     = {3}{0}" +
                                   "Project Image Logo          = {4}{0}",
                                   Environment.NewLine, getProjectInfo.projectName, getProjectInfo.projectMarketingOffice,
                                                        getProjectInfo.projectMarketingPhone, getProjectInfo.projectImageLogo);

            var sendEmail = new BookingSuccessInputDto
            {
                bookDate = dataKP.bookDate,
                customerName = dataKP.psName,
                devPhone = getProjectInfo.projectMarketingPhone != null ? getProjectInfo.projectMarketingPhone : "-",
                memberName = dataKP.memberName,
                memberPhone = dataKP.memberPhone != null ? dataKP.memberPhone : "-",
                projectImage = getProjectInfo.projectImageLogo != null ? getProjectInfo.projectImageLogo : "-",
                projectName = getProjectInfo.projectName,
                email = dataKP.psEmail,
                urlNotaris = _configuration.linkNotaris.EnsureEndsWith('/') + orderID
            };

            Logger.DebugFormat("Result Data Email {0}" +
                               "Book Date        = {1}{0}" +
                               "Customer Name    = {2}{0}" +
                               "Dev Phone        = {3}{0}" +
                               "Member Name      = {4}{0}" +
                               "Member Phone     = {6}{0}" +
                               "Project Image    = {7}{0}" +
                               "Project Name     = {8}{0}" +
                               "Email            = {9}{0}",
                            Environment.NewLine, sendEmail.bookDate, sendEmail.customerName,
                                                sendEmail.devPhone, sendEmail.memberName,
                                                sendEmail.memberPhone, sendEmail.projectImage,
                                                sendEmail.projectName, sendEmail.email);

            var body = _emailAppService.bookingSuccess(sendEmail);

            Logger.Info("Body: " + JsonConvert.SerializeObject(body));

            var email = new SendEmailInputDto
            {
                body = body,
                toAddress = sendEmail.email,
                subject = "Konfirmasi Pemesanan Unit" + dataKP.unitCode + " " + dataKP.unitNo,
                pathKP = dataKP.pathKP
            };

            Logger.DebugFormat("Param Email {0}" +
                           "Body          = {1}{0}" +
                           "To Address    = {2}{0}" +
                           "Subject       = {3}{0}" +
                           "Path KP       = {4}{0}",
                        Environment.NewLine, email.body, email.toAddress,
                                            email.subject, email.pathKP);

            Logger.Info("Sending Email per UnitCode : " + dataKP.unitCode + "UnitNo");

            _emailAppService.ConfigurationEmail(email);

                Logger.Info("Sending Email per UnitID. Done");

                unitOfWork.Complete();
            }
            return "Success";
        }
        
        public void ResendKP(int orderHeaderID)
        {
            Logger.Info("Resend KP. Start()");

            Logger.DebugFormat("Param ResendKP {0}" +
                               "orderHeaderID = {1}{0}",
                            Environment.NewLine, orderHeaderID);

            var getOrder = (from a in _context.TR_UnitOrderHeader
                            join b in _context.TR_UnitOrderDetail on a.Id equals b.UnitOrderHeaderID
                            join c in _context.TR_BookingHeader on b.bookingHeaderID equals c.Id
                            where a.Id == orderHeaderID
                            select new
                            {
                                b.unitID,
                                a.orderCode,
                                c.bookCode,
                                c.bookDate,
                                a.psName,
                                a.psEmail,
                                a.psCode,
                                a.memberName,
                                a.memberCode,
                                orderID = a.Id
                            }).ToList();

            Logger.Info("Result Get Order: " + JsonConvert.SerializeObject(getOrder));

            Logger.Info("Looping Get Order. Start()");

            foreach (var item in getOrder)
            {
                Logger.Info("Iteration UnitID: " + item.unitID);

                var dataUnit = (from unit in _context.MS_Unit
                                join unitCode in _context.MS_UnitCode on unit.unitCodeID equals unitCode.Id
                                where unit.Id == item.unitID
                                select new
                                {
                                    unit.unitNo,
                                    unitCode.unitCode
                                }).FirstOrDefault();

                var dataResend = new ResendKPResultDto()
                {
                    orderCode = item.orderCode,
                    unitCode = dataUnit.unitCode,
                    unitNo = dataUnit.unitNo,
                    psCode = item.psCode
                };

                Logger.DebugFormat("Param Get KP Path {0}" +
                               "orderCode = {1}{0}"+
                               "unitCode  = {2}{0}" +
                               "unitNo    = {3}{0}" +
                               "psCode    = {4}{0}",
                            Environment.NewLine, item.orderCode == null ? string.Empty : item.orderCode,
                            dataUnit.unitCode == null ? string.Empty : dataUnit.unitCode,
                            dataUnit.unitNo == null ? string.Empty : dataUnit.unitNo,
                            item.psCode == null ? string.Empty : item.psCode);

                var getNumber = (from a in _contextPers.PERSONALS_MEMBER
                                 join b in _contextPers.TR_Phone on a.psCode equals b.psCode into phone
                                 from b in phone.DefaultIfEmpty()
                                 where a.memberCode == item.memberCode
                                 select b.number).FirstOrDefault();

                Logger.DebugFormat("Result Get Number {0}" +
                               "number = {1}{0}",
                            Environment.NewLine, getNumber == null ? string.Empty : getNumber);

                var getProjectInfo = (from project in _context.MS_Project
                                      join info in _context.MS_ProjectInfo on project.Id equals info.projectID into a
                                      from projectInfo in a.DefaultIfEmpty()
                                      join unit in _context.MS_Unit on project.Id equals unit.projectID
                                      where unit.Id == item.unitID
                                      orderby projectInfo.CreationTime descending
                                      select new
                                      {
                                          project.projectName,
                                          project.image,
                                          projectInfo.projectMarketingOffice,
                                          projectInfo.projectMarketingPhone
                                      }).FirstOrDefault();

                Logger.DebugFormat("Result Get Project Info {0}" +
                               "ProjectName             = {1}{0}" +
                               "Image                   = {2}{0}" +
                               "projectMarketingOffice  = {3}{0}" +
                               "projectMarketingPhone   = {4}{0}",
                            Environment.NewLine, getProjectInfo.projectName == null ? string.Empty : getProjectInfo.projectName,
                            getProjectInfo.image == null ? string.Empty : getProjectInfo.image,
                            getProjectInfo.projectMarketingOffice == null ? string.Empty : getProjectInfo.projectMarketingOffice,
                            getProjectInfo.projectMarketingPhone == null ? string.Empty : getProjectInfo.projectMarketingPhone);
                
                var sendEmail = new BookingSuccessInputDto
                {
                    bookDate = item.bookDate.ToString(),
                    customerName = item.psName,
                    devPhone = getProjectInfo.projectMarketingPhone != null ? getProjectInfo.projectMarketingPhone : "-",
                    memberName = item.memberName,
                    memberPhone = getNumber != null ? getNumber : "-",
                    projectImage = getProjectInfo.image != null ? getProjectInfo.image : "-",
                    projectName = getProjectInfo.projectName,
                    urlNotaris = _configuration.linkNotaris.EnsureEndsWith('/') + orderHeaderID
                };

                Logger.DebugFormat("Param Email Template {0}" +
                               "bookDate     = {1}{0}" +
                               "customerName = {2}{0}" +
                               "devPhone     = {3}{0}" +
                               "memberName   = {4}{0}"+
                               "memberPhone  = {5}{0}" +
                               "projectImage = {6}{0}" +
                               "projectName  = {7}{0}",
                            Environment.NewLine, item.bookDate.ToString() == null ? string.Empty : item.bookDate.ToString(),
                            item.psName == null ? string.Empty : item.psName,
                            getProjectInfo.projectMarketingPhone == null ? string.Empty : getProjectInfo.projectMarketingPhone,
                            item.memberName == null ? string.Empty : item.memberName,
                            getNumber == null ? string.Empty : getNumber,
                            getProjectInfo.image == null ? string.Empty : getProjectInfo.image,
                            getProjectInfo.projectName == null ? string.Empty : getProjectInfo.projectName);

                var body = _emailAppService.bookingSuccess(sendEmail);

                Logger.Info("Set Body Email: " + JsonConvert.SerializeObject(body));

                Logger.Info("Request API ResendKP. Start");

                using (var client = new WebClient())
                {
                    var url = _configuration.ApiPdfUrl.EnsureEndsWith('/') + "api/Pdf/ResendKP";

                    Logger.Info("Url: " + url);

                    client.Headers.Add("Content-Type:application/json");
                    client.Headers.Add("Accept:application/json");
                    var result = client.UploadString(url, JsonConvert.SerializeObject(dataResend));

                    Logger.Info("Result: " + result);
                    
                    var trimResult = result.Replace(@"\\", @"\").Trim(new char[1] { '"' });

                    Logger.Info("Trim Result: " + trimResult);
                    
                    if (!File.Exists(trimResult))
                    {
                        Logger.Info("KP Not Exists");

                        Logger.Info("Request Generate KP & Send Email. Start");

                        SendEmailKP(item.orderID, item.unitID);

                        Logger.Info("Request Generate KP & Send Email. End");
                    }
                    else
                    {
                        Logger.Info("KP is Exists");

                        var email = new SendEmailInputDto
                        {
                            body = body,
                            toAddress = item.psEmail,
                            subject = "Konfirmasi Pemesanan Unit" + dataUnit.unitCode + " " + dataUnit.unitNo,
                            pathKP = trimResult
                        };

                        Logger.DebugFormat("Param Send Email {0}" +
                               "toAddress  = {2}{0}" +
                               "subject    = {3}{0}",
                            Environment.NewLine, email.toAddress == null ? string.Empty : email.toAddress,
                            email.subject == null ? string.Empty : email.subject);

                        Logger.Info("Request Method Send Email. Start");

                        _emailAppService.ConfigurationEmail(email);

                        Logger.Info("Request Method Send Email. End");
                    }

                }
            }
        }

    }
}