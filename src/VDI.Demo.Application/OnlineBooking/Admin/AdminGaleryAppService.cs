﻿using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.UI;
using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json.Linq;
using VDI.Demo.Files;
using VDI.Demo.IO;
using VDI.Demo.OnlineBooking.Admin.Dto;
using VDI.Demo.OnlineBooking.Transaction.Dto;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;
using VDI.Demo.PropertySystemDB.OnlineBooking.ProjectInfo;

namespace VDI.Demo.OnlineBooking.Admin
{
    public class AdminGaleryAppService : DemoAppServiceBase, IAdminGaleryAppService
    {
        #region fields
        private readonly IRepository<MS_ProjectInfo> _msProjectInfoRepo;
        private readonly IRepository<MS_ProjectKeyFeaturesCollection> _msProjectKeyFeaturesCollectionRepo;
        private readonly IRepository<MS_ProjectLocation> _msProjectLocationRepo;
        private readonly IRepository<MS_ProjectOLBooking> _msProjectOLBookingRepo;
        private readonly IRepository<MS_ProjectSocialMedia> _msProjectSocialMediaRepo;
        private readonly IRepository<MS_PromoOnlineBooking> _msPromoOnlineBookingRepo;
        private readonly IRepository<TR_ProjectImageGallery> _trProjectImageGalleryRepo;
        private readonly IRepository<TR_ProjectKeyFeatures> _trProjectKeyFeaturesRepo;
        private readonly IRepository<TR_ProjectSocialMedia> _trProjectSocialMediaRepo;
        private readonly IRepository<MS_Project> _msProjectRepo;
        private readonly IRepository<MS_Cluster> _msClusterRepo;
        private readonly IRepository<MS_Unit> _msUnit;
        private readonly IRepository<MS_Detail> _msDetail;
        private readonly FilesHelper _fileHelper;
        private readonly IHostingEnvironment _hostingEnvironment;
        #endregion

     
        public AdminGaleryAppService(
          IRepository<MS_ProjectInfo> msProjectInfoRepo,
          IRepository<MS_ProjectKeyFeaturesCollection> msProjectKeyFeaturesCollectionRepo,
          IRepository<MS_ProjectLocation> msProjectLocationRepo,
          IRepository<MS_ProjectOLBooking> msProjectOLBookingRepo,
          IRepository<MS_ProjectSocialMedia> msProjectSocialMediaRepo,
          IRepository<MS_PromoOnlineBooking> msPromoOnlineBookingRepo,
          IRepository<TR_ProjectImageGallery> trProjectImageGalleryRepo,
          IRepository<TR_ProjectKeyFeatures> trProjectKeyFeaturesRepo,
          IRepository<TR_ProjectSocialMedia> trProjectSocialMediaRepo,
          IRepository<MS_Project> msProjectRepo,
          IRepository<MS_Cluster> msClusterRepo,
          IRepository<MS_Unit> msUnit,
          IRepository<MS_Detail> msDetail,
          FilesHelper filesHelper,
          IHostingEnvironment environment
        )
        {
            _msProjectInfoRepo = msProjectInfoRepo;
            _msProjectKeyFeaturesCollectionRepo = msProjectKeyFeaturesCollectionRepo;
            _msProjectLocationRepo = msProjectLocationRepo;
            _msProjectOLBookingRepo = msProjectOLBookingRepo;
            _msProjectSocialMediaRepo = msProjectSocialMediaRepo;
            _msPromoOnlineBookingRepo = msPromoOnlineBookingRepo;
            _trProjectImageGalleryRepo = trProjectImageGalleryRepo;
            _trProjectKeyFeaturesRepo = trProjectKeyFeaturesRepo;
            _trProjectSocialMediaRepo = trProjectSocialMediaRepo;
            _msProjectRepo = msProjectRepo;
            _msClusterRepo = msClusterRepo;
            _msUnit = msUnit;
            _msDetail = msDetail;
            _fileHelper = filesHelper;
            _hostingEnvironment = environment;
        }

        #region upload media hanan

        

        private string MoveSosmedIcon(string socialMediaIcon)
        {
            try
            {
                var newPath = _fileHelper.MoveFiles(socialMediaIcon, @"Temp\Downloads\SosialMediaIcon\", @"Assets\Upload\OnlineBooking\Admin\SocialMedia\");

                var result = @"Assets\Upload\OnlineBooking\Admin\SocialMedia\m-" + socialMediaIcon;

                return result;
            }
            catch (Exception ex)
            {
                Logger.DebugFormat("test() - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Error : {0}", ex.Message);
            }
        }

        private void DeleteSocialMediaIcon(string filename)
        {
            var imageToDelete = filename;
            var filenameToDelete = imageToDelete.Split(@"/");
            var nameFileToDelete = filenameToDelete[filenameToDelete.Count() - 1];
            var deletePath = @"\Assets\Upload\OnlineBooking\Admin\SocialMedia\";

            var deleteImage = _hostingEnvironment.WebRootPath + deletePath + nameFileToDelete;

            if (File.Exists(deleteImage))
            {
                var file = new FileInfo(deleteImage);
                file.Delete();
            }
        }

       
     

        public JObject UpdateSosialMedia(UpdateSocialMediaInputDto input)
        {
            JObject obj = new JObject();

            var getSosmed = (from sosmed in _msProjectSocialMediaRepo.GetAll()
                             where sosmed.Id == input.Id
                             select sosmed).FirstOrDefault();

            if (getSosmed == null)
            {
                throw new UserFriendlyException("Invalid parameter");
            }
            else
            {
                var checkSosmed = (from a in _msProjectSocialMediaRepo.GetAll()
                                   where a.socialMediaName == input.socialMediaName
                                   && a.isActive == true && a.Id != input.Id
                                   select a).Any();
                if (checkSosmed)
                {
                    throw new UserFriendlyException(" already exist!");
                }
                else
                {
                    var getOldImage = getSosmed.socialMediaIcon;

                    var updateMsSocialMedia = getSosmed.MapTo<MS_ProjectSocialMedia>();

                    if (input.iconStatus == "updated")
                    {
                        Logger.InfoFormat("Input.iconStatus : updated");

                        var imageUrl = MoveSosmedIcon(input.socialMediaIcon);

                        Logger.InfoFormat("imageUrl : " + imageUrl);

                        updateMsSocialMedia.socialMediaIcon = imageUrl;

                        if (!string.IsNullOrWhiteSpace(getOldImage))
                        {
                            DeleteSocialMediaIcon(getOldImage);
                        }
                    }
                    else if (input.iconStatus == "removed")
                    {
                        Logger.InfoFormat("Input.iconStatus : removed");

                        DeleteSocialMediaIcon(getOldImage);
                        updateMsSocialMedia.socialMediaIcon = "-";
                    }
                    else
                    {
                        updateMsSocialMedia.socialMediaIcon = getSosmed.socialMediaIcon;
                    }

                    updateMsSocialMedia.isActive = true;
                    updateMsSocialMedia.socialMediaName = input.socialMediaName;

                    _msProjectSocialMediaRepo.Update(updateMsSocialMedia);

                    obj.Add("message", "Edit Successfully");

                    return obj;
                }
            }
        }

        #endregion
        

    }
}
