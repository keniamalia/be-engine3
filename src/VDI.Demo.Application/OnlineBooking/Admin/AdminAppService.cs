﻿using System;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.UI;
using VDI.Demo.Files;
using VDI.Demo.IO;
using VDI.Demo.OnlineBooking.Admin.Dto;
using VDI.Demo.OnlineBooking.Transaction.Dto;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;
using VDI.Demo.PropertySystemDB.OnlineBooking.ProjectInfo;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Hosting;
using System.IO;

namespace VDI.Demo.OnlineBooking.Admin
{
    public class AdminAppService : DemoAppServiceBase, IAdminAppService
    {
        
        #region fields
        private readonly IRepository<MS_ProjectInfo> _msProjectInfoRepo;
        private readonly IRepository<MS_ProjectKeyFeaturesCollection> _msProjectKeyFeaturesCollectionRepo;
        private readonly IRepository<MS_ProjectLocation> _msProjectLocationRepo;
        private readonly IRepository<MS_ProjectOLBooking> _msProjectOLBookingRepo;
        private readonly IRepository<MS_ProjectSocialMedia> _msProjectSocialMediaRepo;
        private readonly IRepository<MS_PromoOnlineBooking> _msPromoOnlineBookingRepo;
        private readonly IRepository<TR_ProjectImageGallery> _trProjectImageGalleryRepo;
        private readonly IRepository<TR_ProjectKeyFeatures> _trProjectKeyFeaturesRepo;
        private readonly IRepository<TR_ProjectSocialMedia> _trProjectSocialMediaRepo;
        private readonly IRepository<MS_Project> _msProjectRepo;
        private readonly IRepository<MS_Cluster> _msClusterRepo;
        private readonly IRepository<MS_Unit> _msUnit;
        private readonly IRepository<MS_Detail> _msDetail;
        private readonly FilesHelper _fileHelper;
        private readonly IHostingEnvironment _hostingEnvironment;
        #endregion

        #region constructor
        public AdminAppService(
          IRepository<MS_ProjectInfo> msProjectInfoRepo,
          IRepository<MS_ProjectKeyFeaturesCollection> msProjectKeyFeaturesCollectionRepo,
          IRepository<MS_ProjectLocation> msProjectLocationRepo,
          IRepository<MS_ProjectOLBooking> msProjectOLBookingRepo,
          IRepository<MS_ProjectSocialMedia> msProjectSocialMediaRepo,
          IRepository<MS_PromoOnlineBooking> msPromoOnlineBookingRepo,
          IRepository<TR_ProjectImageGallery> trProjectImageGalleryRepo,
          IRepository<TR_ProjectKeyFeatures> trProjectKeyFeaturesRepo,
          IRepository<TR_ProjectSocialMedia> trProjectSocialMediaRepo,
          IRepository<MS_Project> msProjectRepo,
          IRepository<MS_Cluster> msClusterRepo,
          IRepository<MS_Unit> msUnit,
          IRepository<MS_Detail> msDetail,
          FilesHelper filesHelper,
          IHostingEnvironment environment
        )
        {
            _msProjectInfoRepo = msProjectInfoRepo;
            _msProjectKeyFeaturesCollectionRepo = msProjectKeyFeaturesCollectionRepo;
            _msProjectLocationRepo = msProjectLocationRepo;
            _msProjectOLBookingRepo = msProjectOLBookingRepo;
            _msProjectSocialMediaRepo = msProjectSocialMediaRepo;
            _msPromoOnlineBookingRepo = msPromoOnlineBookingRepo;
            _trProjectImageGalleryRepo = trProjectImageGalleryRepo;
            _trProjectKeyFeaturesRepo = trProjectKeyFeaturesRepo;
            _trProjectSocialMediaRepo = trProjectSocialMediaRepo;
            _msProjectRepo = msProjectRepo;
            _msClusterRepo = msClusterRepo;
            _msUnit = msUnit;
            _msDetail = msDetail;
            _fileHelper = filesHelper;
            _hostingEnvironment = environment;
        }
        #endregion

        #region Project Information
        public ListResultDto<GetListAllProjectResultDto> GetListAllProject()
        {
            var allProjects = (from p in _msProjectRepo.GetAll()
                               select new GetListAllProjectResultDto
                               {
                                   projectId = p.Id,
                                   projectCode = p.projectCode,
                                   projectName = p.projectName
                               }).ToList();
            try
            {
                return new ListResultDto<GetListAllProjectResultDto>(allProjects);
            }
            // Handle data errors.
            catch (DataException exDb)
            {
                throw new UserFriendlyException("Database Error : {0}", exDb.Message);
            }
            // Handle all other exceptions.
            catch (Exception ex)
            {
                throw new UserFriendlyException("Error : {0}", ex.Message);
            }

        }

        private bool IsClusterExist(int projectId, int clusterId)
        {
            var checkCluster = (from msOlBooking in _msProjectOLBookingRepo.GetAll()
                                where msOlBooking.clusterID == clusterId && 
                                msOlBooking.projectID == projectId
                                select msOlBooking).ToList();
            if (checkCluster.Count > 0)
            {
                return true;
            }
            return false;
        }

        public ResultMessageDto CreateTowerOnDropdownChange(int projectId)
        {
            Logger.Info("checking tower cluster in ol booking");
            var checkTower = (from msProject in _msProjectRepo.GetAll()
                              join msCluster in _msClusterRepo.GetAll() on msProject.Id equals msCluster.projectID
                              where msProject.Id == projectId
                              select new
                              {
                                  projectId = msProject.Id,
                                  clusterId = msCluster.Id
                              }).ToList();
            Logger.Info("isi checkTower: "+checkTower);

            if (checkTower.Count > 0)
            {
                int insertCount = 0;
                int existCount = 0;
                foreach (var tower in checkTower)
                {
                    if (!IsClusterExist(tower.projectId, tower.clusterId))
                    {
                        Logger.Info("inserting MS_OLBooking");
                        var insertOLBooking = new MS_ProjectOLBooking
                        {
                            projectID = tower.projectId,
                            clusterID = tower.clusterId,
                            projectInfoID = null
                        };
                        _msProjectOLBookingRepo.Insert(insertOLBooking);
                        insertCount++;
                    }
                    else
                    {
                        existCount++;
                    }
                }
                Logger.Info("insertin MS_OLBooking done");
                return new ResultMessageDto
                {
                    message = insertCount+" inserted to olbooking with "+existCount+" already exist",
                    result = true
                };
            }
            else
            {
                return new ResultMessageDto
                {
                    message = "project id "+ projectId + " does not have any cluster",
                    result = false
                };
            }
        }

        public async Task CreateOrUpdateProjectInfo(CreateOrUpdateProjectInfoDto input)
        {
            Logger.Info("CreateProjectInfo() - Started.");
            var check = (from projectInfo in _msProjectInfoRepo.GetAll()
                         where projectInfo.projectID == input.projectID
                         select projectInfo).FirstOrDefault();

            if (check == null)
            {
                Logger.Info("CreateProjectInfo() - NotNull.");
                if (input.projectID != 0) //onClick Button Add
                {
                    var createKeyFet = new MS_ProjectKeyFeaturesCollection
                    {
                        CreationTime = DateTime.Now

                    };
                    var idKeyFet = await _msProjectKeyFeaturesCollectionRepo.InsertAndGetIdAsync(createKeyFet);

                    Logger.Info("CreateProjectInfo() - insertprojectinfo.");
                    var insertProjectInfo = new MS_ProjectInfo
                    {
                        projectID = input.projectID,
                        keyFeaturesCollectionID = idKeyFet,
                        projectDesc = input.projectDesc,
                        projectDeveloper = input.projectDeveloper,
                        projectWebsite = input.projectWebsite,
                        projectMarketingOffice = input.projectMarketingOffice,
                        projectMarketingPhone = input.projectMarketingPhone,
                    };

                    int projectInfoId = await _msProjectInfoRepo.InsertAndGetIdAsync(insertProjectInfo);
                }
                else
                {
                    throw new UserFriendlyException("create failed!");
                }
            }
            else
            {
                throw new UserFriendlyException("create failed!");
            }
        }

        public GetProjectInformationResultDto GetProjectInformationDetail(int projectId)
        {
            var projectInformation = (from projectInfo in _msProjectInfoRepo.GetAll()
                                      where projectInfo.projectID == projectId
                                      select new GetProjectInformationResultDto
                                      {
                                          projectId = projectInfo.projectID,
                                          projectDesc = projectInfo.projectDesc,
                                          projectDeveloper = projectInfo.projectDeveloper,
                                          projectWebsite = projectInfo.projectWebsite,
                                          projectMarketingOffice = projectInfo.projectMarketingOffice,
                                          projectMarketingPhone = projectInfo.projectMarketingPhone
                                      }).FirstOrDefault();
            if (projectInformation != null)
            {
                return projectInformation;
            }
            throw new UserFriendlyException("no data");
        }
        #endregion

        #region Tower/Cluster
        public ListResultDto<GetDetailTowerResultDto>GetListTower(int projectID)
        {
            Logger.Info("GetTowerDetail() - Dimulai.");
            var tower = (from cluster in _msClusterRepo.GetAll()
                         join olBooking in _msProjectOLBookingRepo.GetAll() on cluster.Id equals olBooking.clusterID
                         where cluster.projectID == projectID
                         select new GetDetailTowerResultDto
                         {
                             projectId = cluster.projectID,
                             towerId = cluster.Id,
                             clusterID = cluster.Id,
                             clusterCode = cluster.clusterCode,
                             clusterName = cluster.clusterName,
                             isActive = olBooking.isActive,
                             activeFrom = olBooking.activeFrom,
                             activeTo = olBooking.activeTo,
                             isRequiredPP = olBooking.isRequiredPP
                         }).ToList();

            try
            {
                return new ListResultDto<GetDetailTowerResultDto>(tower);
            }
            // Handle data errors.
            catch (DataException exDb)
            {
                throw new UserFriendlyException("Database Error : {0}", exDb.Message);
            }
            // Handle all other exceptions.
            catch (Exception ex)
            {
                throw new UserFriendlyException("Error : {0}", ex.Message);
            }
        }

        public GetDetailTowerResultDto GetTowerDetail(int projectID)
        {
            Logger.Info("GetTowerDetail() - Dimulai.");
            var tower = (from cluster in _msClusterRepo.GetAll()
                         join olBooking in _msProjectOLBookingRepo.GetAll() on cluster.Id equals olBooking.clusterID
                         where cluster.projectID == projectID
                         select new GetDetailTowerResultDto
                         {
                             projectId = cluster.projectID,
                             towerId = cluster.Id,
                             clusterID = cluster.Id,
                             clusterCode = cluster.clusterCode,
                             clusterName = cluster.clusterName,
                             isActive = olBooking.isActive,
                             activeFrom = olBooking.activeFrom,
                             activeTo = olBooking.activeTo

                         }).FirstOrDefault();

            return tower;
        }

        public GetTowerDetailEditResultDto GetTowerDetailEdit(int Id)
        {
            Logger.Info("GetTowerDetailEdit() - Dimulai.");
            var editTower = (from cluster in _msClusterRepo.GetAll()
                             join olBooking in _msProjectOLBookingRepo.GetAll() on cluster.Id equals olBooking.clusterID
                             where cluster.Id == Id
                             select new GetTowerDetailEditResultDto
                             {
                                 towerId = Id,
                                 clusterCode = cluster.clusterCode,
                                 clusterName = cluster.clusterName,
                                 activeFrom = olBooking.activeFrom,
                                 activeTo = olBooking.activeTo,
                                 isRequiredRP = olBooking.isRequiredPP,
                                 isActive = olBooking.isActive
                             }).FirstOrDefault();
            if (editTower is null)
            {
                throw new UserFriendlyException("edit cannot be done");
            }
            return editTower;
        }

        public async Task UpdateTowerDetailAsync(UpdateTowerDetailInputDto input)
        {
            Logger.Info("UpdateTowerDetailAync() - Dimulai.");
            var getTower = (from cluster in _msClusterRepo.GetAll()
                            join olBooking in _msProjectOLBookingRepo.GetAll() on cluster.Id equals olBooking.clusterID
                            where cluster.Id == input.towerId
                            select olBooking).FirstOrDefault();

            var updateTower = getTower.MapTo<MS_ProjectOLBooking>();
            if (getTower is null)
            {
                throw new UserFriendlyException("update tower data failed");
            }
            else
            {
                updateTower.isActive = input.isActive;
                updateTower.activeFrom = DateTime.Parse(input.activeFrom);
                updateTower.activeTo = DateTime.Parse(input.activeTo);
                updateTower.isRequiredPP = input.isRequiredPP;
                await _msProjectOLBookingRepo.UpdateAsync(updateTower);
            }
        }

        #endregion

        #region Key Feature

        //[UnitOfWork(isTransactional: false)]
        public ResultMessageDto CreateKeyFeature(CreateKeyFeatureInputDto input)
        {
            Logger.Info("CreateKeyFeature() - Started.");
            Logger.DebugFormat("Input Param Insert to MS_ProjectKeyFeaturesCollection. {0}" +
            "ProjectId                    = {1}{0}" +
            "keyFeatures                  = {2}{0}" +
            "status                       = {4}{0}",
            Environment.NewLine, input.ProjectId, input.keyFeatures,
            input.status);

            //get keyfetcollid
            var projectInfo = (from info in _msProjectInfoRepo.GetAll()
                               where info.projectID == input.ProjectId
                               select info);

            if (projectInfo.FirstOrDefault().keyFeaturesCollectionID == 0 ) //onClick Button Add
            {            
            //await _msProjectSocialMediaRepo.InsertAsync(dataLogo);
                var insertKeyFetColl = new MS_ProjectKeyFeaturesCollection
                {
                    CreationTime = DateTime.Now
                };

                //create creation time into projectKeyFeaturesColl
                var keyFetCollId = _msProjectKeyFeaturesCollectionRepo.InsertAndGetId(insertKeyFetColl);

                var insertKeyFet = new TR_ProjectKeyFeatures
                {
                    keyFeatures = input.keyFeatures,
                    keyFeaturesCollectionID = keyFetCollId, //insert keyFeaturesCollectionID into keyfeature
                    status = input.status
                };


                _trProjectKeyFeaturesRepo.Insert(insertKeyFet);

                return new ResultMessageDto
                {
                    message = "create keyfeature success + new keyfetcollid",
                    result = false
                };
            }
            else if(projectInfo.FirstOrDefault().keyFeaturesCollectionID > 0)
            {
                var insertKeyFet = new TR_ProjectKeyFeatures
                {
                    keyFeatures = input.keyFeatures,
                    keyFeaturesCollectionID = projectInfo.FirstOrDefault().keyFeaturesCollectionID, //insert keyFeaturesCollectionID into keyfeature
                    status = input.status
                };


                _trProjectKeyFeaturesRepo.Insert(insertKeyFet);

                return new ResultMessageDto
                {
                    message = "create keyfeature success",
                    result = false
                };
            }
            return new ResultMessageDto
            {
                message = "create failed",
                result = false
            };
        }

        public ListResultDto<GetKeyFeaturesResultDto> GetKeyFeatures(int projectId)
        {
            Logger.Info("getAllKeyFeatures() - Started.");



            var keyFeatures = (from projectInfo in _msProjectInfoRepo.GetAll()
                               join keyFetColl in _msProjectKeyFeaturesCollectionRepo.GetAll() on projectInfo.keyFeaturesCollectionID equals keyFetColl.Id
                               join keyFet in _trProjectKeyFeaturesRepo.GetAll() on keyFetColl.Id equals keyFet.keyFeaturesCollectionID
                               where projectInfo.projectID == projectId
                               select new GetKeyFeaturesResultDto
                               {
                                   ProjectId = projectId,
                                   Id = keyFet.Id,
                                   keyFeaturesCollectionID = keyFet.keyFeaturesCollectionID,
                                   keyFeatures = keyFet.keyFeatures,
                                   status = keyFet.status
            }).ToList();

            try
            {
                return new ListResultDto<GetKeyFeaturesResultDto>(keyFeatures);
            }
            // Handle data errors.
            catch (DataException exDb)
            {
                throw new UserFriendlyException("Database Error : {0}", exDb.Message);
            }
            // Handle all other exceptions.
            catch (Exception ex)
            {
                throw new UserFriendlyException("Error : {0}", ex.Message);
            }
        }

        public EditKeyFeatureResultDto EditKeyFeature(int keyFeatureId)
        {
            Logger.Info("EditKeyFeature() - Started.");
            var getKeyFeature = (from keyFet in _trProjectKeyFeaturesRepo.GetAll()
                                 where keyFet.Id == keyFeatureId
                                 select new EditKeyFeatureResultDto
                                 {
                                     id = keyFet.Id,
                                     keyFeatures = keyFet.keyFeatures,
                                     keyFeaturesCollectionID = keyFet.keyFeaturesCollectionID,
                                     status = keyFet.status
                                 }).FirstOrDefault();
            if (getKeyFeature is null)
            {
                throw new UserFriendlyException("something went wrong!");
            }
            else {
                return getKeyFeature;
            }
        }

        public async Task UpdateKeyFeatures(UpdateKeyFeaturesInputDto input)
        {
            Logger.Info("UpdateKeyFeatures() - started.");
            var getKeyFeature = (from keyFeature in _trProjectKeyFeaturesRepo.GetAll()
                                 where keyFeature.Id == input.Id
                                 select keyFeature).FirstOrDefault();
            var updateKeyFeature = getKeyFeature.MapTo<TR_ProjectKeyFeatures>();

            if (getKeyFeature is null)
            {
                throw new UserFriendlyException("UpdateKeyFeatures() went wrong!");
            }
            else
            {
                updateKeyFeature.keyFeatures = input.keyFeatures;
                updateKeyFeature.status = input.status;
                await _trProjectKeyFeaturesRepo.UpdateAsync(updateKeyFeature);
            }
        }
        #endregion

        #region social media project information

        public ResultMessageDto CreateOrUpdateSocialMediaProject(CreateOrUpdateSocialMediaProjectInputDto input)
        {
            Logger.Info("CreateOrUpdateSocialMediaProject() - Start.");

            int getProjectInfoID;

            if(input.projectID != 0)
            {
                getProjectInfoID = (from project in _msProjectRepo.GetAll()
                                    join projectInfo in _msProjectInfoRepo.GetAll() on project.Id equals projectInfo.projectID
                                    where project.Id == input.projectID
                                    select projectInfo.Id).FirstOrDefault();

                if (getProjectInfoID == 0)
                {
                    return new ResultMessageDto
                    {
                        message = "Project Info not Found",
                        result = false
                    };
                }

                if(input.sosialMediaID != 0)
                {
                    var checkSocialMedia = (from trSosmed in _trProjectSocialMediaRepo.GetAll()
                                            where trSosmed.projectInfoID == getProjectInfoID &&
                                            trSosmed.sosialMediaID == input.sosialMediaID
                                            select trSosmed).ToList();

                    if (checkSocialMedia != null && input.socialMediaLinkID != 0)
                    {
                        var getSocialMediaLink = (from trSosmed in _trProjectSocialMediaRepo.GetAll()
                                                where trSosmed.projectInfoID == getProjectInfoID &&
                                                trSosmed.sosialMediaID == input.sosialMediaID && trSosmed.Id == input.socialMediaLinkID
                                                select trSosmed).FirstOrDefault();

                        Logger.Info("CreateOrUpdateSocialMediaProject() - diUpdate.");

                        var updateSosmed = getSocialMediaLink.MapTo<TR_ProjectSocialMedia>();

                        updateSosmed.socialMediaLink = input.socialMediaLink;
                        updateSosmed.isActive = input.isActive;
                        updateSosmed.sosialMediaID = input.sosialMediaID;

                        _trProjectSocialMediaRepo.Update(updateSosmed);

                        return new ResultMessageDto
                        {
                            message = "update success",
                            result = true
                        };

                    }
                    else if (checkSocialMedia != null && input.socialMediaLinkID == 0)
                    {
                        Logger.Info("CreateOrUpdateSocialMediaProject() - Insert.");
                        var insertTrSosmed = new TR_ProjectSocialMedia
                        {
                            projectInfoID = getProjectInfoID,
                            socialMediaLink = input.socialMediaLink,
                            sosialMediaID = input.sosialMediaID,
                            isActive = input.isActive
                        };

                        _trProjectSocialMediaRepo.Insert(insertTrSosmed);

                        return new ResultMessageDto
                        {
                            message = "create success",
                            result = true
                        };
                    }
                    else
                    {
                        Logger.Info("CreateOrUpdateSocialMediaProject() - Insert.");
                        var insertTrSosmed = new TR_ProjectSocialMedia
                        {
                            projectInfoID = getProjectInfoID,
                            socialMediaLink = input.socialMediaLink,
                            sosialMediaID = input.sosialMediaID,
                            isActive = input.isActive
                        };

                        _trProjectSocialMediaRepo.Insert(insertTrSosmed);

                        return new ResultMessageDto
                        {
                            message = "create success",
                            result = true
                        };
                    }
                }
                else
                {
                    return new ResultMessageDto
                    {
                        message = "Social Media is not valid",
                        result = false
                    };
                }
            }
            else
            {
                return new ResultMessageDto
                {
                    message = "Project is not valid",
                    result = false
                };
            }
             
        }

        public ListResultDto<GetListSocialMediaProjectResultDto> GetListSocialMediaProject(int projectId)
        {
            var getListSosmed = (from projectInfo in _msProjectInfoRepo.GetAll()
                                 join trProjectSosmed in _trProjectSocialMediaRepo.GetAll() on projectInfo.Id equals trProjectSosmed.projectInfoID
                                 join sosmeds in _msProjectSocialMediaRepo.GetAll() on trProjectSosmed.sosialMediaID equals sosmeds.Id
                                 where projectInfo.projectID == projectId
                                 select new GetListSocialMediaProjectResultDto
                                 {
                                     projectId = projectInfo.projectID,
                                     socialMediaId = trProjectSosmed.sosialMediaID,
                                     socialMediaName = sosmeds.socialMediaName,
                                     socialMediaLink = trProjectSosmed.socialMediaLink,
                                     socialMediaLinkID = trProjectSosmed.Id,
                                     isActive = trProjectSosmed.isActive
                                 }).ToList();
            try
            {
                return new ListResultDto<GetListSocialMediaProjectResultDto>(getListSosmed);
            }
            // Handle data errors.
            catch (DataException exDb)
            {
                throw new UserFriendlyException("Database Error : {0}", exDb.Message);
            }
            // Handle all other exceptions.
            catch (Exception ex)
            {
                throw new UserFriendlyException("Error : {0}", ex.Message);
            }
        }

        public ResultMessageDto DeleteSocialMediaProject(int ProjectId, int sosmedId)
        {
            var getSosmed = (from sosmed in _trProjectSocialMediaRepo.GetAll()
                             join projectInfo in _msProjectInfoRepo.GetAll() on sosmed.projectInfoID equals projectInfo.Id
                             join sos in _msProjectSocialMediaRepo.GetAll() on sosmed.sosialMediaID equals sos.Id
                             where sosmed.sosialMediaID == sosmedId && projectInfo.projectID == ProjectId
                             select sosmed).FirstOrDefault();
            
            if (getSosmed is null)
            {
                return new ResultMessageDto
                {
                    message = "delete failed",
                    result = false
                };
            }
            else
            {
                _trProjectSocialMediaRepo.Delete(getSosmed.Id);
                return new ResultMessageDto
                {
                    message = getSosmed.socialMediaLink+" was deleted",
                    result = true
                };
            }
        }

        #endregion

        #region Project Location

        public ResultMessageDto CreateProjectLocation(CreateProjectLocationInputDto input)
        {
            Logger.Info("CreateProjectLocation(CreateProjectLocationInputDto input) - Started.");

            string fileUrl;
            if (input.locationImageURL == null)
            {
                fileUrl = "-";
            }
            else
            {
                fileUrl = MoveProjectLocationFile(input.locationImageURL);

                Logger.InfoFormat(" ---FileUrl : " + fileUrl);
            }

            var checkProject = (from projectInfo in _msProjectInfoRepo.GetAll()
                                where projectInfo.projectID == input.projectID
                                select projectInfo).FirstOrDefault();
            if (checkProject is null)
            {
                return new ResultMessageDto
                {
                    message = "create failed",
                    result = false
                };
            }
            else
            {
                var insertProjectLoc = new MS_ProjectLocation
                {
                    projectInfoID = checkProject.Id,
                    latitude = input.latitude,
                    longitude = input.longitude,
                    projectAddress = input.projectAddress,
                    locationImageURL = fileUrl
                };

                _msProjectLocationRepo.Insert(insertProjectLoc);
                return new ResultMessageDto
                {
                    message = "create location success",
                    result = true
                };
            }
        }

        public GetDetailProjectLocationResultDto GetDetailProjectLocation(int projectId)
        {
            var detailProjectLocation = (from projectInfo in _msProjectInfoRepo.GetAll()
                                         join projectLoc in _msProjectLocationRepo.GetAll() on projectInfo.Id equals projectLoc.projectInfoID
                                         where projectInfo.projectID == projectId
                                         select new GetDetailProjectLocationResultDto
                                         {
                                             id = projectLoc.Id,
                                             latitude = projectLoc.latitude,
                                             longitude = projectLoc.longitude,
                                             projectAddress = projectLoc.projectAddress,
                                             locationImageURL = _fileHelper.getAbsoluteUri()+projectLoc.locationImageURL,
                                             projectDesc = projectInfo.projectDesc
                                         }).FirstOrDefault();
            if (detailProjectLocation != null)
            {
                return detailProjectLocation;
            }
            throw new UserFriendlyException("no data");
        }

        public JObject UpdateDetailProjectLocation(UpdateDetailProjectLocationInputDto input)
        {
            JObject obj = new JObject();

            var getDetailProjectLoc = (from detailProject in _msProjectLocationRepo.GetAll()
                                       where detailProject.Id == input.Id
                                       select detailProject
                                       ).FirstOrDefault();

            var getProjectInfo = (from projectInfo in _msProjectInfoRepo.GetAll()
                                  where projectInfo.Id == getDetailProjectLoc.projectInfoID
                                  select projectInfo).FirstOrDefault();



            //update
            if (getDetailProjectLoc != null && getProjectInfo != null)
            {
                var updateDetailProjectLoc = getDetailProjectLoc.MapTo<MS_ProjectLocation>();
                var updateProjectInfo = getProjectInfo.MapTo<MS_ProjectInfo>();

                var getOldImage = getDetailProjectLoc.locationImageURL;
                if (input.imageStatus == "updated")
                {
                    Logger.InfoFormat("Input.imageStatus : updated");

                    var imageUrl = MoveProjectLocationFile(input.locationImageURL);

                    Logger.InfoFormat("imageUrl : " + imageUrl);

                    updateDetailProjectLoc.locationImageURL = imageUrl;

                    if (!string.IsNullOrWhiteSpace(getOldImage))
                    {
                        DeleteProjectLocationFile(getOldImage);
                    }
                }
                else if (input.imageStatus == "removed")
                {
                    Logger.InfoFormat("Input.imageStatus : removed");

                    DeleteProjectLocationFile(getOldImage);
                    updateDetailProjectLoc.locationImageURL = "-";
                }
                else
                {
                    updateDetailProjectLoc.locationImageURL = getDetailProjectLoc.locationImageURL;
                }
                updateDetailProjectLoc.latitude = input.latitude;
                updateDetailProjectLoc.longitude = input.longitude;
                updateDetailProjectLoc.projectAddress = input.projectAddress;
                //updateDetailProjectLoc.locationImageURL = input.locationImageURL;
                updateProjectInfo.projectDesc = input.projectDesc;

                _msProjectLocationRepo.Update(updateDetailProjectLoc);
                _msProjectInfoRepo.Update(updateProjectInfo);

                obj.Add("message", "Update Successfull");

                return obj;
            }
            else
            {
                obj.Add("message", "Update failed");

                return obj;
            }

            //else //insert
            //{
            //    var getOldImage = getDetailProjectLoc.locationImageURL;
            //    if (input.imageStatus == "updated")
            //    {
            //        Logger.InfoFormat("Input.imageStatus : updated");

            //        var imageUrl = MoveProjectLocationFile(input.locationImageURL);

            //        Logger.InfoFormat("imageUrl : " + imageUrl);

            //        updateDetailProjectLoc.locationImageURL = imageUrl;

            //        if (!string.IsNullOrWhiteSpace(getOldImage))
            //        {
            //            DeleteProjectLocationFile(getOldImage);
            //        }
            //    }
            //    else if (input.imageStatus == "removed")
            //    {
            //        Logger.InfoFormat("Input.imageStatus : removed");

            //        DeleteProjectLocationFile(getOldImage);
            //        updateDetailProjectLoc.locationImageURL = "-";
            //    }
            //    else
            //    {
            //        updateDetailProjectLoc.locationImageURL = getDetailProjectLoc.locationImageURL;
            //    }

            //    var projecLoc = new MS_ProjectLocation
            //    {
            //        projectInfoID = input.projectId,
            //        latitude = input.latitude,
            //        longitude = input.longitude,
            //        projectAddress = input.projectAddress,
            //        locationImageURL = input.locationImageURL
            //    };
            //    _msProjectLocationRepo.Insert(projecLoc);

            //    updateProjectInfo.projectDesc = input.projectDesc;
            //    _msProjectInfoRepo.Update(updateProjectInfo);
            //}

        }

        private string MoveProjectLocationFile(string fileName)
        {
            try
            {
                var newPath = _fileHelper.MoveFiles(fileName, @"Temp\Downloads\ProjectLocationImage\", @"Assets\Upload\OnlineBooking\Admin\ProjectLocationImage\");

                string result = @"Assets\Upload\OnlineBooking\Admin\ProjectLocationImage\m-" + fileName;
                return result;
            }
            catch (Exception ex)
            {
                Logger.DebugFormat("test() - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Error : {0}", ex.Message);
            }
        }

        private void DeleteProjectLocationFile(string filename)
        {
            var imageToDelete = filename;
            var filenameToDelete = imageToDelete.Split(@"/");
            var nameFileToDelete = filenameToDelete[filenameToDelete.Count() - 1];
            var deletePath = @"\Assets\Upload\OnlineBooking\Admin\ProjectLocationImage\";

            var deleteImage = _hostingEnvironment.WebRootPath + deletePath + nameFileToDelete;

            if (File.Exists(deleteImage))
            {
                var file = new FileInfo(deleteImage);
                file.Delete();
            }

        }
        #endregion

        #region Site Plan

        public GetDetailSitePlanResultDto GetDetailSitePlan(int projectId)
        {
            var getDetailSitePlan = (from sitePlan in _msProjectInfoRepo.GetAll()
                                     where sitePlan.projectID == projectId
                                     select new GetDetailSitePlanResultDto
                                     {
                                         projectId = projectId,
                                         sitePlansImageUrl = _fileHelper.getAbsoluteUri()+sitePlan.sitePlansImageUrl,
                                         sitePlansLegend = sitePlan.sitePlansLegend
                                     }).FirstOrDefault();
            if (getDetailSitePlan != null)
            {
                return getDetailSitePlan;
            }
            throw new UserFriendlyException("no data");
        }

        public JObject CreateOrUpdateSitePlan(CreateOrUpdateDetailSitePlanInputDto input)
        {
            JObject obj = new JObject();

            var getDetailSitePlan = (from sitePlan in _msProjectInfoRepo.GetAll()
                                     where sitePlan.projectID == input.ProjectId
                                     select sitePlan).FirstOrDefault();

            var updateDetailSitePlan = getDetailSitePlan.MapTo<MS_ProjectInfo>();

            if (getDetailSitePlan != null) //update
            {
                var getOldImage = getDetailSitePlan.sitePlansImageUrl;

                var updateSitePlan = getDetailSitePlan.MapTo<MS_ProjectInfo>();


                if (input.imageStatus == "updated")
                {
                    Logger.InfoFormat("Input.imageStatus : updated");

                    var imageUrl = MoveSitePlanFile(input.sitePlansImageUrl);

                    Logger.InfoFormat("imageUrl : " + imageUrl);

                    updateSitePlan.sitePlansImageUrl = imageUrl;

                    if (!string.IsNullOrWhiteSpace(getOldImage))
                    {
                        DeleteSitePlanFile(getOldImage);
                    }
                }
                else if (input.imageStatus == "removed")
                {
                    Logger.InfoFormat("Input.imageStatus : removed");

                    DeleteSitePlanFile(getOldImage);
                    updateSitePlan.sitePlansImageUrl = "-";
                }
                else
                {
                    updateSitePlan.sitePlansImageUrl = getDetailSitePlan.sitePlansImageUrl;
                }


                //updateDetailSitePlan.sitePlansImageUrl = input.sitePlansImageUrl;
                updateDetailSitePlan.sitePlansLegend = input.sitePlansLegend;

                _msProjectInfoRepo.Update(updateSitePlan);
                obj.Add("message", "Update Successfull");

                return obj;
            }
            else //create
            {
                obj.Add("message", "update failed");
                return obj;
            }
        }

        private string MoveSitePlanFile(string fileName)
        {
            try
            {
                var newPath = _fileHelper.MoveFiles(fileName, @"Temp/Downloads/SitePlanImage/", @"Assets/Upload/OnlineBooking/Admin/SitePlanImage/");

                string result = @"Assets/Upload/OnlineBooking/Admin/SitePlanImage/m-" + fileName;
                Logger.Info("MoveSitePlanFile return: "+result);
                return result;
            }
            catch (Exception ex)
            {
                Logger.DebugFormat("test() - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Error : {0}", ex.Message);
            }
        }

        private void DeleteSitePlanFile(string filename)
        {
            var imageToDelete = filename;
            var filenameToDelete = imageToDelete.Split(@"/");
            var nameFileToDelete = filenameToDelete[filenameToDelete.Count() - 1];
            var deletePath = @"\Assets\Upload\OnlineBooking\Admin\SitePlanImage\";

            var deleteImage = _hostingEnvironment.WebRootPath + deletePath + nameFileToDelete;

            if (File.Exists(deleteImage))
            {
                var file = new FileInfo(deleteImage);
                file.Delete();
            }

        }

        #endregion

        #region Project Gallery

        public ListResultDto<GetListProjectGalleryResultDto> GetGalleryProjectList(int projectId)
        {
            Logger.Info("GetGalleryProjectList(int projectId) - Started.");
            var projectInfo = (from info in _msProjectInfoRepo.GetAll()
                               where info.projectID == projectId
                               select info);

            var getList = (from gallery in _trProjectImageGalleryRepo.GetAll()
                           where gallery.projectInfoID == projectInfo.FirstOrDefault().Id
            select new GetListProjectGalleryResultDto
            {
                ProjectInfoID = gallery.projectInfoID,
                GalleryId = gallery.Id,
                ImageURL = _fileHelper.getAbsoluteUri() + gallery.imageURL,
                ImageAlt = gallery.imageAlt,
                ImageStatus = gallery.imageStatus,
                FileTypeId = gallery.fileTypeID
            }).ToList();

            try
            {
                return new ListResultDto<GetListProjectGalleryResultDto>(getList);
            }

            catch (DataException exDb)
            {
                throw new UserFriendlyException("Database Error : {0}", exDb.Message);
            }

            catch (Exception ex)
            {
                throw new UserFriendlyException("Error : {0}", ex.Message);
            }
        }

        //public ResultMessageDto AddProjectImageInput(AddProjectImageInputDto input)
        //{
        //    Logger.Info("AddProjectImageInput(AddProjectImageInputDto input) - Started.");
        //    var check = (from pInfo in _msProjectInfoRepo.GetAll()
        //                 where pInfo.projectID == input.ProjectId
        //                 select pInfo);
        //    if(check.FirstOrDefault() != null) 
        //    {
        //        var image = new TR_ProjectImageGallery
        //        {
        //            projectInfoID = input.ProjectInfoId,
        //            imageURL = input.ImageURL,
        //            imageAlt = input.ImageAlt,
        //            imageStatus = input.ImageStatus,
        //            fileTypeID = 2
        //        };
        //        _trProjectImageGalleryRepo.Insert(image);
        //        return new ResultMessageDto
        //        {
        //            message = "create project image success",
        //            result = true
        //        };
        //    }
        //    return new ResultMessageDto
        //    {
        //        message = "create project image failed",
        //        result = false
        //    };
        //}

        public GetDetailProjectImageResultDto GetDetailProjectImage(int Id)
        {
            Logger.Info("GetDetailProjectImage(int Id) - Started.");
            var GetDetailProjectImage = (from ProjectImage in _trProjectImageGalleryRepo.GetAll()
                                         where ProjectImage.Id == Id
                                         select new GetDetailProjectImageResultDto
                                         {
                                             Id = ProjectImage.Id,
                                             imageURL = ProjectImage.imageURL,
                                             imageAlt = ProjectImage.imageAlt,
                                             projectInfoID = ProjectImage.projectInfoID,
                                             FileTypeId = ProjectImage.fileTypeID
                                         }).FirstOrDefault();

            if (GetDetailProjectImage != null)
            {
                return GetDetailProjectImage;
            }

            throw new UserFriendlyException("no data");
        }

        //upload image
        public async Task UpdateProjectImage(UpdateProjectImageInputDto input)
        {
            Logger.Info("UpdateProjectImage(UpdateProjectImageInputDto input) - Started.");
            var getProjectImage = (from projectImage in _trProjectImageGalleryRepo.GetAll()
                                    where projectImage.Id == input.Id
                                    select projectImage).FirstOrDefault();
            var updateProjectImage = getProjectImage.MapTo<TR_ProjectImageGallery>();

            if (getProjectImage != null)
            {
                updateProjectImage.imageURL = input.imageURL;
                updateProjectImage.imageAlt = input.imageAlt;
                updateProjectImage.imageStatus = input.imageStatus;

                await _trProjectImageGalleryRepo.UpdateAsync(updateProjectImage);
            }
            else
            {
                throw new UserFriendlyException("update failed");
            }
        }

        public ResultMessageDto DeleteProjectImage(int Id)
        {
            Logger.Info("DeleteProjectImage(int Id) - Started.");
            var getImage = (from image in _trProjectImageGalleryRepo.GetAll()
            where image.Id == Id
            select image).FirstOrDefault();

            if (getImage is null)
            {
                return new ResultMessageDto
                {
                    message = "delete failed",
                    result = false
                };
            }
            else
            {
                _trProjectSocialMediaRepo.Delete(getImage.Id);
                return new ResultMessageDto
                {
                    message = "image deleted",
                    result = true
                };
            }
        }

        #endregion

        #region upload media hanan
        private string UploadFileSocialMedia(string logoName)
        {
            try
            {
                var newPath = _fileHelper.MoveFiles(logoName, @"Temp\Downloads\SosialMediaIcon\", @"Assets\Upload\OnlineBooking\Admin\SocialMedia\");

                string result = @"Assets\Upload\OnlineBooking\Admin\SocialMedia\m-" + logoName;
                return result;
            }
            catch (Exception ex)
            {
                Logger.DebugFormat("test() - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Error : {0}", ex.Message);
            }
        }

        private void DeleteFileSocialMediaIcon(string filename)
        {
            var imageToDelete = filename;
            Logger.InfoFormat("imageToDelete : " + imageToDelete);


            var filenameToDelete = imageToDelete.Split(@"/");
            Logger.InfoFormat("filenameToDelete : " + filenameToDelete);

            var nameFileToDelete = filenameToDelete[filenameToDelete.Count() - 1];
            Logger.InfoFormat("nameFileToDelete : " + nameFileToDelete);

            var deleteImage =  Path.Combine(_hostingEnvironment.WebRootPath, nameFileToDelete);
            Logger.InfoFormat("deleteImage : " + deleteImage);

            if (File.Exists(deleteImage))
            {
                Logger.InfoFormat("File.Exists(deleteImage)");

                var file = new FileInfo(deleteImage);
                file.Delete();
            }
            Logger.InfoFormat("DeleteSocialMediaIcon - End");

        }



        public async Task CreateSosialMedia(AddSocialMediaInputDto input)
        {
            Logger.InfoFormat(" ---Insert to MS_ProjectSocialMedia - Start.");

            Logger.DebugFormat("Input Param Insert to MS_ProjectSocialMedia. {0}" +
                "socialMediaIcon                  = {1}{0}" +
                "socialMediaName                  = {2}{0}",

            Environment.NewLine, input.socialMediaIcon, input.socialMediaName);

            var checkSosmed = (from a in _msProjectSocialMediaRepo.GetAll()
                               where a.socialMediaName == input.socialMediaName
                               select a).FirstOrDefault();
            if(checkSosmed != null)
            {
                throw new UserFriendlyException("Social Media Name Already Exist");
            }

            string fileUrl;
            if (input.socialMediaIcon == null)
            {
                fileUrl = "-";
            }
            else
            {
                fileUrl = UploadFileSocialMedia(input.socialMediaIcon);

                Logger.InfoFormat(" ---FileUrl : " + fileUrl);
            }

            Logger.DebugFormat("Param Insert MS_ProjectSocialMedia. {0}" +
               "socialMediaIcon             = {1}{0}" +
               "socialMediaName             = {2}{0}" +
               "isActive                    = {3}{0}",

           Environment.NewLine, fileUrl, input.socialMediaName, true);

            var dataLogo = new MS_ProjectSocialMedia
                {
                    isActive = input.isActive,
                    socialMediaIcon = fileUrl,
                    socialMediaName = input.socialMediaName
                };

            Logger.InfoFormat(" ---Insert to MS_ProjectSocialMedia - End.");

            await _msProjectSocialMediaRepo.InsertAsync(dataLogo);
        }

        public JObject UpdateSosialMedia(UpdateSocialMediaInputDto input)
        {
            JObject obj = new JObject();

            var getSosmed = (from sosmed in _msProjectSocialMediaRepo.GetAll()
                             where sosmed.Id == input.Id
                             select sosmed).FirstOrDefault();

            if (getSosmed == null)
            {
                throw new UserFriendlyException("Data Not Found");
            }
            else
            {
                var checkSosmed = (from a in _msProjectSocialMediaRepo.GetAll()
                                   where a.socialMediaName == input.socialMediaName
                                   && a.isActive == true && a.Id != input.Id
                                   select a).Any();
                if(checkSosmed)
                {
                    throw new UserFriendlyException("data already exist");
                }
                else
                {
                    var getOldImage = getSosmed.socialMediaIcon;

                    var updateMsSocialMedia = getSosmed.MapTo<MS_ProjectSocialMedia>();

                    if (input.iconStatus == "updated")
                    {
                        Logger.InfoFormat("Input.iconStatus : updated");

                        var imageUrl = MoveSosmedIcon(input.socialMediaIcon);

                        Logger.InfoFormat("imageUrl : " + imageUrl);

                        updateMsSocialMedia.socialMediaIcon = imageUrl;

                        if (!string.IsNullOrWhiteSpace(getOldImage))
                        {
                            DeleteFileSocialMediaIcon(getOldImage);
                        }
                    }
                    else if (input.iconStatus == "removed")
                    {
                        Logger.InfoFormat("Input.iconStatus : removed");

                        DeleteFileSocialMediaIcon(getOldImage);
                        updateMsSocialMedia.socialMediaIcon = "-";
                    }
                    else
                    {
                        updateMsSocialMedia.socialMediaIcon = getSosmed.socialMediaIcon;
                    }

                    updateMsSocialMedia.isActive = true;
                    updateMsSocialMedia.socialMediaName = input.socialMediaName;

                    _msProjectSocialMediaRepo.Update(updateMsSocialMedia);

                    obj.Add("message", "Edit Successfully");

                    return obj;
                }
            }
        }

        public JObject DeleteSosialMedia(int socialMediaID)
        {
            JObject obj = new JObject();

            var getSosmed = (from a in _msProjectSocialMediaRepo.GetAll()
                             where a.Id == socialMediaID
                             select a).FirstOrDefault();
            if(getSosmed == null)
            {
                throw new UserFriendlyException("Data Not Found");
            }
            else
            {
                _msProjectSocialMediaRepo.Delete(getSosmed.Id);

                DeleteFileSocialMediaIcon(getSosmed.socialMediaIcon);

                obj.Add("message", "Delete Successfully");

                return obj;
            }
        }


        #endregion

        #region Social Media
        private string MoveSosmedIcon(string socialMediaIcon)
        {
            try
            {
                var newPath = _fileHelper.MoveFiles(socialMediaIcon, @"Temp\Downloads\SosialMediaIcon\", @"Assets\Upload\OnlineBooking\Admin\SocialMedia\");

                var result = @"Assets\Upload\OnlineBooking\Admin\SocialMedia\m-" + socialMediaIcon;

                return result;
            }
            catch (Exception ex)
            {
                Logger.DebugFormat("test() - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Error : {0}", ex.Message);
            }
        }

        public ListResultDto<GetListSocialMediaResultDto> GetListSocialMedia()
        {
            var getAllData = (from sosmed in _msProjectSocialMediaRepo.GetAll()
                              orderby sosmed.CreationTime descending
                              select new GetListSocialMediaResultDto
                              {
                                  socialMediaID = sosmed.Id,
                                  socialMediaName = sosmed.socialMediaName,
                                  socialMediaIcon = _fileHelper.getAbsoluteUri() + sosmed.socialMediaIcon,
                                  isActive = sosmed.isActive,
                                  
                              }).ToList();
            try
            {
                return new ListResultDto<GetListSocialMediaResultDto>(getAllData);
            }
            // Handle data errors.
            catch (DataException exDb)
            {
                throw new UserFriendlyException("Database Error : {0}", exDb.Message);
            }
            // Handle all other exceptions.
            catch (Exception ex)
            {
                throw new UserFriendlyException("Error : {0}", ex.Message);
            }
        }

        public EditSocialMediaResultDto EditSocialMedia(int Id)
        {
            var getsosmed = (from sosmed in _msProjectSocialMediaRepo.GetAll()
                             where sosmed.Id == Id
                             select new EditSocialMediaResultDto
                             {
                                 Id = sosmed.Id,
                                 socialMediaIcon = sosmed.socialMediaIcon,
                                 socialMediaName = sosmed.socialMediaName,
                                 isActive = sosmed.isActive
                             }).FirstOrDefault();
            if (getsosmed is null)
            {
                throw new UserFriendlyException("Error while getting data");
            }
            else
            {
                return getsosmed;
            }
        }

        public JObject UpdateSocialMedia(UpdateSocialMediaInputDto input)
        {
            Logger.Info("UpdateSocialMedia() - Started.");
            JObject obj = new JObject();

            var getSosmed = (from sosmed in _msProjectSocialMediaRepo.GetAll()
                             where sosmed.Id == input.Id
                             select sosmed).FirstOrDefault();

            

            if (getSosmed is null)
            {
                throw new UserFriendlyException("something went wrong");
            }
            else
            {
                var chekSosmed = (from a in _msProjectSocialMediaRepo.GetAll()
                                    where a.socialMediaIcon == input.socialMediaIcon
                                    select a).Any();

                if (chekSosmed)
                {
                    throw new UserFriendlyException("sosmed icon already exist!");
                }
                else
                {
                    var getOldImage = getSosmed.socialMediaIcon;
                    var updateSosmed = getSosmed.MapTo<MS_ProjectSocialMedia>();
                    if (input.iconStatus == "updated")
                    {
                        Logger.InfoFormat("Input.iconStatus : updated");

                        //var imageUrl = MoveSosmedIcon(input.socialMediaIcon);

                        //Logger.InfoFormat("imageUrl : " + imageUrl);

                        //updateSosmed.socialMediaIcon = imageUrl;

                        if (!string.IsNullOrWhiteSpace(getOldImage))
                        {
                            DeleteMainPageImageFile(getOldImage);
                        }
                    }
                    else if (input.iconStatus == "removed")
                    {
                        Logger.InfoFormat("Input.imageUrl : removed");

                        DeleteMainPageImageFile(getOldImage);
                        updateSosmed.socialMediaIcon = "-";
                    }
                    else
                    {
                        updateSosmed.socialMediaIcon = getSosmed.socialMediaIcon;
                    }

                    updateSosmed.socialMediaName = input.socialMediaName;
                    _msProjectSocialMediaRepo.Update(updateSosmed);
                    return obj;
                }
            }
        }

        public ResultMessageDto DeleteSocialMedia(int Id)
        {
            var getSosmed = (from sosmed in _msProjectSocialMediaRepo.GetAll()
                             where sosmed.Id == Id
                             select sosmed).FirstOrDefault();
            if (getSosmed is null)
            {
                return new ResultMessageDto
                {
                    message = "delete failed",
                    result = false
                };
            }
            else
            {
                _msProjectSocialMediaRepo.Delete(Id);
                return new ResultMessageDto
                {
                    message = "${getSosmed.socialMediaName} was deleted",
                    result = true
                };
            }
        }        
        #endregion

        #region Main Page

        public ListResultDto<GetListMainPageImagesResultDto> GetListImageMainPage(int projectId)
        {
            var mainPageImages = (from projectInfo in _msProjectInfoRepo.GetAll()
                                  join image in _trProjectImageGalleryRepo.GetAll() on 
                                  projectInfo.projectID equals projectId
                                  select new GetListMainPageImagesResultDto
                                  {
                                    Id = image.Id,
                                    ProjectId = projectInfo.projectID,
                                    ImageURL = _fileHelper.getAbsoluteUri()+image.imageURL,
                                    ImageStatus = image.imageStatus,
                                    ImageAlt = image.imageAlt,
                                    FileTypeId = image.fileTypeID,
                                  }).ToList();
            try
            {
                return new ListResultDto<GetListMainPageImagesResultDto>(mainPageImages);
            }
            // Handle data errors.
            catch (DataException exDb)
            {
                throw new UserFriendlyException("Database Error : {0}", exDb.Message);
            }
            // Handle all other exceptions.
            catch (Exception ex)
            {
                throw new UserFriendlyException("Error : {0}", ex.Message);
            }
        }

        private string MoveImageMainPageFile(string imageName)
        {
            try
            {
                var newPath = _fileHelper.MoveFiles(imageName, @"Temp\Downloads\ImageMainPage\", @"Assets\Upload\OnlineBooking\Admin\ImageMainPage\");

                string result = @"Assets\Upload\OnlineBooking\Admin\ImageMainPage\m-" + imageName;
                return result;
            }
            catch (Exception ex)
            {
                Logger.DebugFormat("test() - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Error : {0}", ex.Message);
            }
        }

        public JObject CreateMainPage(CreateMainPageDto input)
        {
            Logger.InfoFormat(" ---Insert to TR_ProjectImageGallery - Start.");
            JObject obj = new JObject();

            string fileUrl;
            if (input.ImageURL == null)
            {
                fileUrl = "-";
            }
            else
            {
                fileUrl = MoveImageMainPageFile(input.ImageURL);

                Logger.InfoFormat(" ---FileUrl : " + fileUrl);
            }
            
            var info = (from projectInfo in _msProjectInfoRepo.GetAll()
                        where projectInfo.projectID == input.ProjectId
                        select projectInfo).FirstOrDefault();
        
            if(info != null)
            {
                var data = new TR_ProjectImageGallery
                    {
                        projectInfoID = info.Id,
                        imageURL = fileUrl,
                        imageAlt = input.ImageAlt,
                        fileTypeID = input.FileTypeId
                    };
                _trProjectImageGalleryRepo.Insert(data);
                obj.Add("message", "success");

                return obj;
            }
            else
            {
                Logger.InfoFormat(" ---Insert to TR_ProjectImageGallery - End.");
                obj.Add("message", "failed");

                return obj;
            }
        }

        public JObject UpdateMainPage(UpdateMainPageDto input)
        {
            Logger.Info("UpdateMainPage() - Started.");
            JObject obj = new JObject();

            var getMainPage = (from mainPage in _trProjectImageGalleryRepo.GetAll()
                               where mainPage.Id == input.MainPageId
                               select mainPage).FirstOrDefault();
            
            if (getMainPage is null)
            {
                throw new UserFriendlyException("something went wrong");
            }
            else
            {
                var chekMainPage = (from a in _trProjectImageGalleryRepo.GetAll()
                where a.imageAlt == input.ImageURL && a.imageStatus == true && a.Id == input.MainPageId
                select a).Any();

                if(chekMainPage)
                {
                    throw new UserFriendlyException(" already exist!");
                }
                else
                {
                var getOldImage = getMainPage.imageURL;
                
                var updateMainPage = getMainPage.MapTo<TR_ProjectImageGallery>();

                if(input.imageStatus == "updated")
                {
                    Logger.InfoFormat("Input.imageStatus : updated");

                    var imageUrl = MoveImageMainPageFile(input.ImageURL);

                    Logger.InfoFormat("imageUrl : " + imageUrl);

                    updateMainPage.imageURL = imageUrl;

                    if (!string.IsNullOrWhiteSpace(getOldImage))
                    {
                        DeleteMainPageImageFile(getOldImage);
                    }
                }
                else if (input.imageStatus == "removed")
                {
                    Logger.InfoFormat("Input.imageUrl : removed");
                    
                    DeleteMainPageImageFile(getOldImage);
                    updateMainPage.imageURL = "-";
                }
                else
                {
                    updateMainPage.imageURL = getMainPage.imageURL;
                }
                
                //updateMainPage.imageURL = url;
                updateMainPage.imageAlt = input.AltName;
                updateMainPage.fileTypeID = input.FileTypeId;
                
                _trProjectImageGalleryRepo.Update(updateMainPage);
                
                obj.Add("message", "Edit Successfully");
                
                return obj;
                }
            }
        }

        private void DeleteMainPageImageFile(string filename)
        {
            var imageToDelete = filename;
            var filenameToDelete = imageToDelete.Split(@"/");
            var nameFileToDelete = filenameToDelete[filenameToDelete.Count() - 1];
            var deletePath = @"\Assets\Upload\OnlineBooking\Admin\ImageMainPage\";

            var deleteImage = _hostingEnvironment.WebRootPath + deletePath + nameFileToDelete;

            if (File.Exists(deleteImage))
            {
                var file = new FileInfo(deleteImage);
                file.Delete();
            }
        }

        public JObject DeleteMainPage(int MainPageId)
        {
            JObject obj = new JObject();

            var getGallery = (from a in _trProjectImageGalleryRepo.GetAll()
                              where a.Id == MainPageId
                              select a).FirstOrDefault();

            if (getGallery == null)
            {
                throw new UserFriendlyException("Data Not Found");
            }
            else
            {
                _trProjectImageGalleryRepo.Delete(getGallery.Id);

                DeleteMainPageImageFile(getGallery.imageURL);

                obj.Add("message", "Delete Successfully");

                return obj;
            }
        }
        #endregion

        #region Manage Project

        public string CreateManageProject(CreateManageProjectDto input)
        {
            Logger.Info("CreateManageProject() - dimulai.");

            var activeFromConv = Convert.ToDateTime(input.activeFrom);

            var activeToConv = Convert.ToDateTime(input.activeTo);

            var getProject = (from project in _msProjectOLBookingRepo.GetAll()
                              where project.Id == input.projectID
                              select project).FirstOrDefault();
            
            if (getProject is null)
            {
                string fileUrl;
                if (input.projectImageLogo == null)
                {
                    fileUrl = "-";
                }
                else
                {
                    fileUrl = UploadFileSocialMedia(input.projectImageLogo);

                    Logger.InfoFormat(" ---FileUrl : " + fileUrl);
                }
                //insert image
                var image = new MS_ProjectOLBooking
                {
                    imgLogo = fileUrl,
                    projectID = input.projectID,
                    projectName = input.projectName,
                    isActive = input.projectStatus,
                    activeFrom = activeFromConv,
                    activeTo = activeToConv
                };
                _msProjectOLBookingRepo.Insert(image);

                return "create manage project success";
            }

            return "project already exist! insert failed!";
        }

        private string MoveManageProjectImage(string fileName)
        {
            try
            {
                var newPath = _fileHelper.MoveFiles(fileName, @"Temp\Downloads\ManageProjectImage\", @"Assets\Upload\OnlineBooking\Admin\ManageProjectImage\");

                string result = @"Assets\Upload\OnlineBooking\Admin\ManageProjectImage\m-" + fileName;
                return result;
            }
            catch (Exception ex)
            {
                Logger.DebugFormat("test() - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Error : {0}", ex.Message);
            }
        }

        public ListResultDto<GetManageProjectListResultDto> GetManageProjectList()
        {
            var projectList = (from projectOl in _msProjectOLBookingRepo.GetAll()
                               select new GetManageProjectListResultDto
                               {
                                    projectID = projectOl.projectID,
                                    Id = projectOl.Id,
                                    imgLogo = projectOl.imgLogo,
                                    projectName = projectOl.projectName,
                                    isActive = projectOl.isActive,
                                    activeFrom = projectOl.activeFrom,
                                    activeTo = projectOl.activeTo
                                }).ToList();
            try
            {
                return new ListResultDto<GetManageProjectListResultDto>(projectList);
            }
            // Handle data errors.
            catch (DataException exDb)
            {
                throw new UserFriendlyException("Database Error : {0}", exDb.Message);
            }
            // Handle all other exceptions.
            catch (Exception ex)
            {
                throw new UserFriendlyException("Error : {0}", ex.Message);
            }

        }

        public GetDetailProjectResultDto GetDetailProject(int manageProjectId)
        {
            var detailProject = (from a in _msProjectOLBookingRepo.GetAll()
                                 join project in _msProjectRepo.GetAll() on a.projectID equals project.Id
                                 where a.Id == manageProjectId
                                 select new GetDetailProjectResultDto
            
            {
                ProjectID = a.projectID,
                id = a.Id,
                projectCode = project.projectCode,
                ProjectName = project.projectName,
                ProjectStatus = a.isActive,
                ActiveFrom = a.activeFrom,
                ActiveTo = a.activeTo
            }).FirstOrDefault();
            
            if (detailProject != null)
            {
                return detailProject;
            }
            throw new UserFriendlyException("no data");
        }

        public JObject UpdateManageProject(UpdateManageProjectInputDto input)
        {
            JObject obj = new JObject();

            var getProject = (from project in _msProjectOLBookingRepo.GetAll()
                              where project.Id == input.manageId
                              select project).FirstOrDefault();

            //var getPromo = (from promo in _msPromoOnlineBookingRepo.GetAll()
            //                where promo.projectID == input.projectId
            //                select promo).FirstOrDefault();

            if (getProject != null)
            {                 
                var updateProject = getProject.MapTo<MS_ProjectOLBooking>();

                updateProject.isActive = input.projectStatus;
                updateProject.activeFrom = DateTime.Parse(input.activeFrom);
                updateProject.activeTo = DateTime.Parse(input.activeTo);

                //var updatePromo = getPromo.MapTo<MS_PromoOnlineBooking>();
                //updatePromo.isActive = input.projectStatus;

                _msProjectOLBookingRepo.Update(updateProject);
                //_msPromoOnlineBookingRepo.Update(updatePromo);

                return obj;
            }
            else
            {
                throw new UserFriendlyException("update failed");
            }
        }
        
        private void DeleteManageProjectIcon(string filename)
        {
            var imageToDelete = filename;
            Logger.InfoFormat("imageToDelete : " + imageToDelete);


            var filenameToDelete = imageToDelete.Split(@"/");
            Logger.InfoFormat("filenameToDelete : " + filenameToDelete);

            var nameFileToDelete = filenameToDelete[filenameToDelete.Count() - 1];
            Logger.InfoFormat("nameFileToDelete : " + nameFileToDelete);

            var deleteImage =  Path.Combine(_hostingEnvironment.WebRootPath, nameFileToDelete);
            Logger.InfoFormat("deleteImage : " + deleteImage);

            if (File.Exists(deleteImage))
            {
                Logger.InfoFormat("File.Exists(deleteImage)");

                var file = new FileInfo(deleteImage);
                file.Delete();
            }
            Logger.InfoFormat("DeleteManageProjectIcon - End");

        }
        
        #endregion

        private string MoveImageGallery(string imageUrl)
        {
            try
            {
                var newPath = _fileHelper.MoveFiles(imageUrl, @"Temp\Downloads\ImageGallery\", @"Assets\Upload\OnlineBooking\Admin\ImageGallery\");

                var result = @"Assets\Upload\OnlineBooking\Admin\ImageGallery\m-" + imageUrl;

                return result;
            }
            catch (Exception ex)
            {
                Logger.DebugFormat("test() - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Error : {0}", ex.Message);
            }
        }

        private void DeleteImageGallery(string filename)
        {
            var imageToDelete = filename;
            Logger.InfoFormat("imageToDelete : " + imageToDelete);


            var filenameToDelete = imageToDelete.Split(@"/");
            Logger.InfoFormat("filenameToDelete : " + filenameToDelete);

            var nameFileToDelete = filenameToDelete[filenameToDelete.Count() - 1];
            Logger.InfoFormat("nameFileToDelete : " + nameFileToDelete);

            var deleteImage = Path.Combine(_hostingEnvironment.WebRootPath, nameFileToDelete);
            Logger.InfoFormat("deleteImage : " + deleteImage);

            if (File.Exists(deleteImage))
            {
                Logger.InfoFormat("File.Exists(deleteImage)");

                var file = new FileInfo(deleteImage);
                file.Delete();
            }
            Logger.InfoFormat("DeleteImageGallery - End");

        }

        public JObject UpdateImageGallery(UpdateImageGalleryInputDto input)
        {
            JObject obj = new JObject();

            var getGallery = (from gallery in _trProjectImageGalleryRepo.GetAll()
                             where gallery.Id == input.galleryID
                             select gallery).FirstOrDefault();

            if (getGallery == null)
            {
                throw new UserFriendlyException("Data Not Found");
            }
            else
            {
                var getOldImage = getGallery.imageURL;

                var updateGallery = getGallery.MapTo<TR_ProjectImageGallery>();

                if (input.imageStatus == "updated")
                {
                    Logger.InfoFormat("Input.imageStatus : updated");

                    var imageUrl = MoveImageGallery(input.imageUrl);

                    Logger.InfoFormat("imageUrl : " + imageUrl);

                    updateGallery.imageURL = imageUrl;

                    if (!string.IsNullOrWhiteSpace(getOldImage))
                    {
                        DeleteImageGallery(getOldImage);
                    }
                }
                else if (input.imageStatus == "removed")
                {
                    Logger.InfoFormat("Input.imageStatus : removed");

                    DeleteImageGallery(getOldImage);
                    updateGallery.imageURL = "-";
                }
                else
                {
                    updateGallery.imageURL = getGallery.imageURL;
                }

                updateGallery.imageAlt = input.imageAlt;

                _trProjectImageGalleryRepo.Update(updateGallery);

                obj.Add("message", "Edit Successfully");

                return obj;
            }
        }

        public async Task CreateGalery(CreateGalleryInputDto input)
        {
            var projectInfo = (from info in _msProjectInfoRepo.GetAll()
                               where info.projectID == input.ProjectId
                               select info).FirstOrDefault();

            Logger.InfoFormat(" ---Insert to MS_ProjectSocialMedia - Start.");

            //Logger.DebugFormat("Input Param Insert to TR_ProjectImageGallery. {0}" +
            //    "imageURL                  = {1}{0}" +
            //    "projectInfoID                  = {2}{0}",

            //Environment.NewLine, input.ImageURL, input.ProjectInfoId);


            string fileUrl;
            if (input.ImageURL == null)
            {
                fileUrl = "-";
            }
            else
            {
                //Logger.InfoFormat("fileUrl");
                fileUrl = MoveImageGalleryFile(input.ImageURL);

                Logger.InfoFormat("FileUrl : " + fileUrl);
            }

           // Logger.DebugFormat("Param Insert TR_ProjectImageGallery. {0}" +
           //    "imageURL             = {1}{0}" +
           //    "projectInfoID             = {2}{0}" +
           //    "imageStatus                    = {3}{0}",

           //Environment.NewLine, fileUrl, input.ProjectInfoId, true);

            var dataLogo = new TR_ProjectImageGallery
            {
                imageStatus = true,
                imageURL = fileUrl,
                projectInfoID = projectInfo.Id,
                fileTypeID = 1,
                imageAlt = input.ImageAlt
            };

            Logger.InfoFormat(" ---Insert to MS_ProjectSocialMedia - End.");

            await _trProjectImageGalleryRepo.InsertAsync(dataLogo);
        }

        public JObject DeleteImageGallery(int galleryID)
        {
            JObject obj = new JObject();

            var getGallery = (from a in _trProjectImageGalleryRepo.GetAll()
                             where a.Id == galleryID
                             select a).FirstOrDefault();

            if (getGallery == null)
            {
                throw new UserFriendlyException("Data Not Found");
            }
            else
            {
                _trProjectImageGalleryRepo.Delete(getGallery.Id);

                DeleteImageGallery(getGallery.imageURL);

                obj.Add("message", "Delete Successfully");

                return obj;
            }
        }

        //private string MoveGalleryFile(string fileName)
        //{
        //    try
        //    {
        //        var newPath = _fileHelper.MoveFiles(fileName, @"Temp\Downloads\ImageGallery\", @"Assets\Upload\OnlineBooking\Admin\ImageGallery\");

        //        string result = @"Assets\Upload\OnlineBooking\Admin\ImageGallery\m-" + fileName;
        //        return result;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.InfoFormat(" ---whywhywhwywhy.");
        //        Logger.DebugFormat("test() - ERROR Exception. Result = {0}", ex.Message);
        //        throw new UserFriendlyException("Error : {0}", ex.Message);
        //    }
        //}

        private string MoveImageGalleryFile(string socialMediaIcon)
        {
            try
            {
                var newPath = _fileHelper.MoveFiles(socialMediaIcon, @"Temp\Downloads\ImageGallery\", @"Assets\Upload\OnlineBooking\Admin\ImageGallery\");

                var result = @"Assets\Upload\OnlineBooking\Admin\ImageGallery\m-" + socialMediaIcon;

                return result;
            }
            catch (Exception ex)
            {
                Logger.DebugFormat("test() - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Error : {0}", ex.Message);
            }
        }
    }
}
