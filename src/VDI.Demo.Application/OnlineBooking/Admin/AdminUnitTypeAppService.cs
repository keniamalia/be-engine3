﻿using System;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.UI;
using VDI.Demo.Files;
using VDI.Demo.IO;
using VDI.Demo.OnlineBooking.Admin.Dto;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;
using VDI.Demo.PropertySystemDB.OnlineBooking.ProjectInfo;
using Microsoft.AspNetCore.Hosting;
using System.IO;

namespace VDI.Demo.OnlineBooking.Admin
{
    public class AdminUnitTypeAppService : DemoAppServiceBase, IAdminUnitTypeAppService
    {
        private readonly IRepository<MS_ProjectInfo> _msProjectInfoRepo;
        private readonly IRepository<MS_ProjectKeyFeaturesCollection> _msProjectKeyFeaturesCollectionRepo;
        private readonly IRepository<MS_ProjectLocation> _msProjectLocationRepo;
        private readonly IRepository<MS_ProjectOLBooking> _msProjectOLBookingRepo;
        private readonly IRepository<MS_ProjectSocialMedia> _msProjectSocialMediaRepo;
        private readonly IRepository<MS_PromoOnlineBooking> _msPromoOnlineBookingRepo;
        private readonly IRepository<TR_ProjectImageGallery> _trProjectImageGalleryRepo;
        private readonly IRepository<TR_ProjectKeyFeatures> _trProjectKeyFeaturesRepo;
        private readonly IRepository<TR_ProjectSocialMedia> _trProjectSocialMediaRepo;
        private readonly IRepository<MS_Project> _msProjectRepo;
        private readonly IRepository<MS_Cluster> _msClusterRepo;
        private readonly IRepository<MS_Unit> _msUnit;
        private readonly IRepository<MS_Detail> _msDetail;
        private readonly FilesHelper _fileHelper;
        private readonly IHostingEnvironment _hostingEnvironment;

        public AdminUnitTypeAppService(
          IRepository<MS_ProjectInfo> msProjectInfoRepo,
          IRepository<MS_ProjectKeyFeaturesCollection> msProjectKeyFeaturesCollectionRepo,
          IRepository<MS_ProjectLocation> msProjectLocationRepo,
          IRepository<MS_ProjectOLBooking> msProjectOLBookingRepo,
          IRepository<MS_ProjectSocialMedia> msProjectSocialMediaRepo,
          IRepository<MS_PromoOnlineBooking> msPromoOnlineBookingRepo,
          IRepository<TR_ProjectImageGallery> trProjectImageGalleryRepo,
          IRepository<TR_ProjectKeyFeatures> trProjectKeyFeaturesRepo,
          IRepository<TR_ProjectSocialMedia> trProjectSocialMediaRepo,
          IRepository<MS_Project> msProjectRepo,
          IRepository<MS_Cluster> msClusterRepo,
          IRepository<MS_Unit> msUnit,
          IRepository<MS_Detail> msDetail,
          FilesHelper filesHelper,
          IHostingEnvironment hostingEnvironment
        )
        {
            _msProjectInfoRepo = msProjectInfoRepo;
            _msProjectKeyFeaturesCollectionRepo = msProjectKeyFeaturesCollectionRepo;
            _msProjectLocationRepo = msProjectLocationRepo;
            _msProjectOLBookingRepo = msProjectOLBookingRepo;
            _msProjectSocialMediaRepo = msProjectSocialMediaRepo;
            _msPromoOnlineBookingRepo = msPromoOnlineBookingRepo;
            _trProjectImageGalleryRepo = trProjectImageGalleryRepo;
            _trProjectKeyFeaturesRepo = trProjectKeyFeaturesRepo;
            _trProjectSocialMediaRepo = trProjectSocialMediaRepo;
            _msProjectRepo = msProjectRepo;
            _msClusterRepo = msClusterRepo;
            _msUnit = msUnit;
            _msDetail = msDetail;
            _fileHelper = filesHelper;
            _hostingEnvironment = hostingEnvironment;
        }

    
        public ListResultDto<GetListUnitTypeResultDto> GetListUnitType(int projectId)
        {
            //var units = (from unit in _msUnit.GetAll()
            //             join cluster in _msClusterRepo.GetAll() on unit.clusterID equals cluster.Id
            //             //join detail in _msDetail.GetAll() on cluster.Id equals detail.clus
            //             where unit.projectID == projectId
            //             //group cluster by new
            //             //{
            //             //    cluster.Id
            //} into c
            var units =(from cluster in _msClusterRepo.GetAll()
                       join msunit in _msUnit.GetAll() on cluster.Id equals msunit.clusterID
                       join msdetail in _msDetail.GetAll() on msunit.detailID equals msdetail.Id
                       where cluster.projectID == projectId

            select new GetListUnitTypeResultDto
                         {
                             projectID = projectId,
                             detailID = msdetail.Id,
                             clusterName = cluster.clusterName,
                             detailImage = _fileHelper.getAbsoluteUri() + msdetail.detailImage,
                             detailCode = cluster.clusterCode
                         }).Distinct().ToList();
            try
            {
                return new ListResultDto<GetListUnitTypeResultDto>(units);
            }
            // Handle data errors.
            catch (DataException exDb)
            {
                throw new UserFriendlyException("Database Error : {0}", exDb.Message);
            }
            // Handle all other exceptions.
            catch (Exception ex)
            {
                throw new UserFriendlyException("Error : {0}", ex.Message);
            }
        }

        public void UpdateUnitType(UpdateUnitTypeInputDto input)
        {
            Logger.Info("UpdateUnitType() - Started.");

            Logger.Info("detailID : " + input.detailID + ", imageStatus" + input.imageStatus);

            var checkUnittype = (from A in _msDetail.GetAll()
                                 where A.Id == input.detailID
                                 select A).FirstOrDefault();

            if (checkUnittype != null)
            {
                var getOldImage = checkUnittype.detailImage;

                var updateunitType = checkUnittype.MapTo<MS_Detail>();

                if (input.imageStatus == "updated")
                {
                    Logger.InfoFormat("Input.statusImage : updated");

                    var imageUrl = MoveDetailImage(input.detailImage);

                    Logger.InfoFormat("imageUrl : " + imageUrl);

                    updateunitType.detailImage = imageUrl;

                    if (!string.IsNullOrWhiteSpace(getOldImage))
                    {
                        DeleteFileDetailImage(getOldImage);
                    }
                }
                else if (input.imageStatus == "removed")
                {
                    Logger.InfoFormat("Input.iconStatus : removed");

                    DeleteFileDetailImage(getOldImage);
                    updateunitType.detailImage = "-";
                }
                else
                {
                    updateunitType.detailImage = checkUnittype.detailImage;
                }
                _msDetail.Update(updateunitType);
            }
            else
            {
                throw new UserFriendlyException("Data Not Exits");
            }
            Logger.Info("UpdateMsCategory() - Finished.");
        }

        private string MoveDetailImage(string detailImage)
        {
            try
            {
                var newPath = _fileHelper.MoveFiles(detailImage, @"Temp\Downloads\DetailImage\", @"Assets\Upload\OnlineBooking\Admin\DetailImage\");

                var result = @"Assets\Upload\OnlineBooking\Admin\DetailImage\m-" + detailImage;

                return result;
            }
            catch (Exception ex)
            {
                Logger.DebugFormat("test() - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Error : {0}", ex.Message);
            }
        }

        private void DeleteFileDetailImage(string filename)
        {
            var imageToDelete = filename;
            Logger.InfoFormat("imageToDelete : " + imageToDelete);


            var filenameToDelete = imageToDelete.Split(@"/");
            Logger.InfoFormat("filenameToDelete : " + filenameToDelete);

            var nameFileToDelete = filenameToDelete[filenameToDelete.Count() - 1];
            Logger.InfoFormat("nameFileToDelete : " + nameFileToDelete);

            var deleteImage = Path.Combine(_hostingEnvironment.WebRootPath, nameFileToDelete);
            Logger.InfoFormat("deleteImage : " + deleteImage);

            if (File.Exists(deleteImage))
            {
                Logger.InfoFormat("File.Exists(deleteImage)");

                var file = new FileInfo(deleteImage);
                file.Delete();
            }
            Logger.InfoFormat("DeleteFileDetailImage - End");
        }
    }
}