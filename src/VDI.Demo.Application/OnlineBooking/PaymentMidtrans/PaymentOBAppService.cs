﻿using Abp.AspNetZeroCore.Net;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.UI;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.NewCommDB;
using VDI.Demo.OnlineBooking.Email;
using VDI.Demo.OnlineBooking.Email.Dto;
using VDI.Demo.OnlineBooking.PaymentMidtrans.Dto;
using VDI.Demo.OnlineBooking.Transaction.Dto;
using VDI.Demo.PersonalsDB;
using VDI.Demo.PropertySystemDB.LippoMaster;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;
using VDI.Demo.PropertySystemDB.OnlineBooking.PropertySystem;
using Visionet_Backend_NetCore.Komunikasi;

namespace VDI.Demo.OnlineBooking.PaymentMidtrans
{
    public class PaymentOBAppService : DemoAppServiceBase, IPaymentOBAppService, ITransientDependency
    {
        private readonly PaymentOBConfiguration _configuration;
        private readonly IPaymentMidtransHelper _paymentMidtranshelper;
        private readonly IRepository<TR_UnitOrderHeader> _trUnitOrderHeader;
        private readonly IRepository<TR_UnitOrderDetail> _trUnitOrderDetail;
        private readonly IRepository<MS_Unit> _msUnit;
        private readonly IRepository<MS_UnitCode> _msUnitCode;
        private readonly IRepository<PERSONALS, string> _personalsRepo;
        private readonly IRepository<TR_BookingHeader> _bookingHeaderRepo;
        private readonly IRepository<MS_TujuanTransaksi> _tujuanTransaksiRepo;
        private readonly IRepository<MS_SumberDana> _sumberDanaRepo;
        private readonly IRepository<TR_ID, string> _trIDRepo;
        private readonly IRepository<MS_Schema, string> _msSchemaRepo;
        private readonly IRepository<PERSONALS_MEMBER, string> _personalsMemberRepo;
        private readonly IRepository<TR_Phone, string> _trPhoneRepo;
        private readonly PersonalsNewDbContext _personalContext;
        private readonly PropertySystemDbContext _propertySystemContext;
        private readonly NewCommDbContext _newCommContext;
        private readonly IEmailAppService _emailAppService;

        public PaymentOBAppService(PaymentOBConfiguration configuration,
            IPaymentMidtransHelper paymentMidtranshelper,
            IRepository<TR_UnitOrderHeader> trUnitOrderHeader,
            IRepository<TR_UnitOrderDetail> trUnitOrderDetail,
            IRepository<MS_Unit> msUnit,
            IRepository<MS_UnitCode> msUnitCode,
            IRepository<PERSONALS, string> personalsRepo,
            IRepository<TR_BookingHeader> bookingHeaderRepo,
            IRepository<MS_TujuanTransaksi> tujuanTransaksiRepo,
            IRepository<MS_SumberDana> sumberDanaRepo,
            IRepository<TR_ID, string> trIDRepo,
            IRepository<MS_Schema, string> msSchemaRepo,
            IRepository<PERSONALS_MEMBER, string> personalsMemberRepo,
            IRepository<TR_Phone, string> trPhoneRepo,
            PersonalsNewDbContext personalContext,
            PropertySystemDbContext propertySystemContext,
            NewCommDbContext newCommContext,
            IEmailAppService emailAppService)
        {
            _newCommContext = newCommContext;
            _propertySystemContext = propertySystemContext;
            _personalContext = personalContext;
            _trUnitOrderHeader = trUnitOrderHeader;
            _trUnitOrderDetail = trUnitOrderDetail;
            _msUnit = msUnit;
            _msUnitCode = msUnitCode;
            _personalsRepo = personalsRepo;
            _bookingHeaderRepo = bookingHeaderRepo;
            _tujuanTransaksiRepo = tujuanTransaksiRepo;
            _sumberDanaRepo = sumberDanaRepo;
            _trIDRepo = trIDRepo;
            _msSchemaRepo = msSchemaRepo;
            _personalsMemberRepo = personalsMemberRepo;
            _trPhoneRepo = trPhoneRepo;
            _paymentMidtranshelper = paymentMidtranshelper;
            _configuration = configuration;
            _emailAppService = emailAppService;
        }

        public Task<PaymentOnlineBookingResponse> CreatePayment(PaymentOnlineBookingRequest input)
        {
            SendConsole("Testing");
            Logger.InfoFormat("CreatePaymentOnlineBooking() - Start.");

            Logger.InfoFormat(JsonConvert.SerializeObject(input));

            var url = _configuration.BaseUrl.EnsureEndsWith('/') + "charge";
            Logger.DebugFormat("url create payment : {0}", url);

            string ignored = JsonConvert.SerializeObject(input, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });

            try
            {
                ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(AcceptAllCertifications);

                var data = Encoding.UTF8.GetBytes(ignored);

                HttpWebRequest myRequest;
                myRequest = WebRequest.CreateHttp(url);

                string autorization = _configuration.ServerKey + ":" + "";
                byte[] binaryAuthorization = Encoding.UTF8.GetBytes(autorization);
                autorization = Convert.ToBase64String(binaryAuthorization);
                autorization = "Basic " + autorization;
                myRequest.Headers.Add("Authorization", autorization);

                myRequest.Method = "POST";
                myRequest.UserAgent = "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0; WOW64; " +
                                        "Trident/4.0; SLCC1; .NET CLR 2.0.50727; Media Center PC 5.0; " +
                                        ".NET CLR 3.5.21022; .NET CLR 3.5.30729; .NET CLR 3.0.30618; " +
                                        "InfoPath.2; OfficeLiveConnector.1.3; OfficeLivePatch.0.0)";

                myRequest.ContentType = "application/json";
                myRequest.Accept = "application/json";
                myRequest.ContentLength = data.Length;
                myRequest.KeepAlive = false;
                myRequest.ProtocolVersion = HttpVersion.Version10;
                myRequest.Timeout = 25000;

                return ReadResponse<PaymentOnlineBookingResponse>(data, myRequest);
            }
            catch (Exception e)
            {
                SendConsole("" + e.Message + " " + e.StackTrace);
            }
            return null;
        }

        private bool AcceptAllCertifications(object sender, X509Certificate certification, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        public Task<PaymentOnlineBookingResponse> CreatePaymentLite(PaymentOnlineBookingRequest input)
        {
            SendConsole("Testing");
            Logger.InfoFormat("CreatePaymentOnlineBooking() - Start.");

            var url = _configuration.BaseUrlLite.EnsureEndsWith('/') + "transactions";
            Logger.DebugFormat("url create payment : {0}", url);

            string ignored = JsonConvert.SerializeObject(input, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });

            try
            {
                ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(AcceptAllCertifications);

                var data = Encoding.UTF8.GetBytes(ignored);

                HttpWebRequest myRequest;
                myRequest = WebRequest.CreateHttp(url);

                string autorization = _configuration.ServerKey + ":" + "";
                byte[] binaryAuthorization = Encoding.UTF8.GetBytes(autorization);
                autorization = Convert.ToBase64String(binaryAuthorization);
                autorization = "Basic " + autorization;
                myRequest.Headers.Add("Authorization", autorization);

                myRequest.Method = "POST";
                myRequest.UserAgent = "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0; WOW64; " +
                                        "Trident/4.0; SLCC1; .NET CLR 2.0.50727; Media Center PC 5.0; " +
                                        ".NET CLR 3.5.21022; .NET CLR 3.5.30729; .NET CLR 3.0.30618; " +
                                        "InfoPath.2; OfficeLiveConnector.1.3; OfficeLivePatch.0.0)";

                myRequest.ContentType = "application/json; charset=utf-8";
                myRequest.Accept = "application/json; charset=utf-8";
                myRequest.ContentLength = data.Length;
                myRequest.KeepAlive = false;
                myRequest.ProtocolVersion = HttpVersion.Version10;
                myRequest.Timeout = 25000;

                return ReadResponse<PaymentOnlineBookingResponse>(data, myRequest);
            }
            catch (Exception e)
            {
                SendConsole("" + e.Message + " " + e.StackTrace);
            }
            return null;
        }

        private async Task<PaymentOnlineBookingResponse> ReadResponse<T>(byte[] data, HttpWebRequest myRequest)
        {
            Logger.InfoFormat("ReadResponsePayment() - Start.");
            PaymentOnlineBookingResponse responseData = null;
            try
            {
                using (var stream = await myRequest.GetRequestStreamAsync())
                {
                    stream.Write(data, 0, data.Length);
                    stream.Close();
                }

                using (var theResponse = await myRequest.GetResponseAsync())
                {
                    var dataStream = theResponse.GetResponseStream();

                    StreamReader reader = new StreamReader(dataStream);
                    object objResponse = reader.ReadToEnd();

                    SendConsole("Pesan:" + objResponse.ToString());

                    responseData = _paymentMidtranshelper.ValidateResponseStatus(JsonConvert.DeserializeObject<PaymentOnlineBookingResponse>(objResponse.ToString()));

                    Logger.DebugFormat("response payment midtrans. {0}" +
                    "status code            = {1}{0}" +
                    "status message         = {2}{0}" +
                    "transaction id         = {3}{0}" +
                    "transaction time       = {4}{0}" +
                    "transaction status     = {5}{0}" +
                    "payment type           = {6}{0}" +
                    "gross amount           = {7}{0}" +
                    "order id               = {8}{0}" +
                    "error message          = {9}{0}" +
                    "fraud status           = {10}{0}" +
                    "signature key          = {11}{0}" +
                    "approval code          = {12}{0}" +
                    "billerKey|billerCode   = {13}{0}",
                    Environment.NewLine, responseData.status_code, responseData.status_message,
                    responseData.transaction_id, responseData.transaction_time, responseData.transaction_status,
                    responseData.payment_type, responseData.gross_amount, responseData.order_id,
                    responseData.error_messages, responseData.fraud_status, responseData.signature_key,
                    responseData.approval_code, responseData.bill_key + "|" + responseData.biller_code);

                    dataStream.Close();
                    theResponse.Close();
                }
            }
            catch (Exception e)
            {
                SendConsole("" + e.Message + " " + e.StackTrace);
            }

            Logger.InfoFormat("ReadResponsePayment() - End.");

            return responseData;
        }


        public async Task<RequestTokenResultDto> RequestToken(RequestTokenInputDto input)
        {
            Logger.InfoFormat("RequestToken() - Start.");
            var url = _configuration.reqToken;
            var authToken = Convert.ToBase64String(Encoding.UTF8.GetBytes(_configuration.ServerKey));
            Logger.DebugFormat("url create payment : {0}", url);

            using (var client = new HttpClient())
            {
                client.Timeout = TimeSpan.FromMinutes(10);
                var request = CreateRequest(url, "Basic", authToken, client, HttpMethod.Post);
                request.Content = new StringContent(JsonConvert.SerializeObject(input), Encoding.UTF8, MimeTypeNames.ApplicationJson);

                Logger.InfoFormat("RequestToken() - End.");
                return await ReadResponseReqToken<RequestTokenResultDto>(url, client, request);
            }
        }

        private async Task<RequestTokenResultDto> ReadResponseReqToken<T>(string url, HttpClient client, HttpRequestMessage request)
        {
            Logger.InfoFormat("ReadResponsePayment() - Start.");
            var response = await client.SendAsync(request);
            Logger.InfoFormat("status payment midtrans : success ==> {0}", response.IsSuccessStatusCode);

            if (!response.IsSuccessStatusCode)
            {
                var error = await response.Content.ReadAsStringAsync();
                Logger.Error($"Could not complete Paypal payment (url: {url}). Error: {error}");
                throw new UserFriendlyException(L("PaymentCouldNotCompleted"));
            }

            var success = await response.Content.ReadAsStringAsync();
            var responseData = _paymentMidtranshelper.ValidateResponseToken(JsonConvert.DeserializeObject<RequestTokenResultDto>(success));

            Logger.InfoFormat("ReadResponsePayment() - End.");

            return responseData;
        }

        private async Task<PaymentOnlineBookingResponse> ReadResponse<T>(string url, HttpClient client, HttpRequestMessage request)
        {
            Logger.InfoFormat("ReadResponsePayment() - Start.");
            var response = await client.SendAsync(request);
            Logger.InfoFormat("status payment midtrans : success ==> {0}", response.IsSuccessStatusCode);

            if (!response.IsSuccessStatusCode)
            {
                var error = await response.Content.ReadAsStringAsync();
                Logger.Error($"Could not complete Paypal payment (url: {url}). Error: {error}");
                throw new UserFriendlyException(L("PaymentCouldNotCompleted"));
            }

            var success = await response.Content.ReadAsStringAsync();
            var responseData = _paymentMidtranshelper.ValidateResponseStatus(JsonConvert.DeserializeObject<PaymentOnlineBookingResponse>(success));
            Logger.DebugFormat("response payment midtrans. {0}" +
                "status code            = {1}{0}" +
                "status message         = {2}{0}" +
                "transaction id         = {3}{0}" +
                "transaction time       = {4}{0}" +
                "transaction status     = {5}{0}" +
                "payment type           = {6}{0}" +
                "gross amount           = {7}{0}" +
                "order id               = {8}{0}" +
                "error message          = {9}{0}" +
                "fraud status           = {10}{0}" +
                "signature key          = {11}{0}" +
                "approval code          = {12}{0}" +
                "billerKey|billerCode   = {13}{0}",
            Environment.NewLine, responseData.status_code, responseData.status_message,
                responseData.transaction_id, responseData.transaction_time, responseData.transaction_status,
                responseData.payment_type, responseData.gross_amount, responseData.order_id,
                responseData.error_messages, responseData.fraud_status, responseData.signature_key,
                responseData.approval_code, responseData.bill_key + "|" + responseData.biller_code);

            Logger.InfoFormat("ReadResponsePayment() - End.");

            return responseData;
        }

        private static HttpRequestMessage CreateRequest(string url, string authSchema, string authToken, HttpClient client, HttpMethod method)
        {
            var request = new HttpRequestMessage(method, url);
            request.Headers.Authorization = new AuthenticationHeaderValue(authSchema, authToken);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(MimeTypeNames.ApplicationJson));
            return request;
        }

        public async Task<PaymentOnlineBookingResponse> CheckPaymentStatus(string orderCode)
        {
            Logger.InfoFormat("CheckPaymentStatus() - Start.");
            var url = _configuration.BaseUrl.EnsureEndsWith('/') + orderCode + "/status";
            var authToken = Convert.ToBase64String(Encoding.UTF8.GetBytes(_configuration.ServerKey));
            Logger.DebugFormat("url create payment : {0}", url);
            Logger.DebugFormat("CheckPaymentStatus() : Param Sent: orderCode = {0}", orderCode);

            using (var client = new HttpClient())
            {
                client.Timeout = TimeSpan.FromMinutes(10);
                var request = CreateRequest(url, "Basic", authToken, client, HttpMethod.Get);

                Logger.InfoFormat("CheckPaymentStatus() - End.");
                return await ReadResponse<PaymentOnlineBookingResponse>(url, client, request);
            }
        }

        public void PaymentNotification(PaymentOnlineBookingResponse data)
        {
            Logger.InfoFormat("PaymentNotification() - Start.");
            //var responseData = _paymentMidtranshelper.ValidateResponseStatus(JsonConvert.DeserializeObject<PaymentOnlineBookingResponse>(data));

            Logger.DebugFormat("response payment midtrans. {0}" +
                "status code            = {1}{0}" +
                "status message         = {2}{0}" +
                "transaction id         = {3}{0}" +
                "transaction time       = {4}{0}" +
                "transaction status     = {5}{0}" +
                "payment type           = {6}{0}" +
                "gross amount           = {7}{0}" +
                "order id               = {8}{0}" +
                "error message          = {9}{0}" +
                "fraud status           = {10}{0}" +
                "signature key          = {11}{0}" +
                "approval code          = {12}{0}" +
                "billerKey|billerCode   = {13}{0}",
            Environment.NewLine, data.status_code, data.status_message,
                data.transaction_id, data.transaction_time, data.transaction_status,
                data.payment_type, data.gross_amount, data.order_id,
                data.error_messages, data.fraud_status, data.signature_key,
                data.approval_code, data.bill_key + "|" + data.biller_code);
            Logger.InfoFormat("PaymentNotification() - End.");

        }

        //public async Task<ReqTokenResultDto> ReqToken(ReqTokenInputDto input)
        //{
        //    var url = "https://app.sandbox.midtrans.com/snap/v1/transactions";
        //    var authToken = Convert.ToBase64String(Encoding.UTF8.GetBytes(_configuration.ServerKey));


        //    using (var client = new HttpClient())
        //    {
        //        client.Timeout = TimeSpan.FromMinutes(10);
        //        var request = CreateRequest(url, "Basic", authToken, client, HttpMethod.Post);
        //        request.Content = new StringContent(JsonConvert.SerializeObject(input), Encoding.UTF8, MimeTypeNames.ApplicationJson);

        //        Logger.InfoFormat("CreatePaymentOnlineBooking() - End.");
        //        return await ReadResponse<PaymentOnlineBookingResponse>(url, client, request);
        //    }
        //}

        public void PaymentError(PaymentOnlineBookingResponse data)
        {
            Logger.InfoFormat("PaymentNotification() - Start.");
            //var responseData = _paymentMidtranshelper.ValidateResponseStatus(JsonConvert.DeserializeObject<PaymentOnlineBookingResponse>(data));

            Logger.DebugFormat("response payment midtrans. {0}" +
                "status code            = {1}{0}" +
                "status message         = {2}{0}" +
                "transaction id         = {3}{0}" +
                "transaction time       = {4}{0}" +
                "transaction status     = {5}{0}" +
                "payment type           = {6}{0}" +
                "gross amount           = {7}{0}" +
                "order id               = {8}{0}" +
                "error message          = {9}{0}" +
                "fraud status           = {10}{0}" +
                "signature key          = {11}{0}" +
                "approval code          = {12}{0}" +
                "billerKey|billerCode   = {13}{0}",
            Environment.NewLine, data.status_code, data.status_message,
                data.transaction_id, data.transaction_time, data.transaction_status,
                data.payment_type, data.gross_amount, data.order_id,
                data.error_messages, data.fraud_status, data.signature_key,
                data.approval_code, data.bill_key + "|" + data.biller_code);
            Logger.InfoFormat("PaymentError() - End.");
        }
        //public KonfirmasiPesananDto PaymentFinish(PaymentOnlineBookingResponse data)
        //{
        //    Logger.InfoFormat("PaymentFinish() - Start.");

        //    var dataKP = (from x in _propertySystemContext.TR_UnitOrderHeader
        //                  join a in _propertySystemContext.TR_UnitOrderDetail on x.Id equals a.UnitOrderHeaderID
        //                  join b in _propertySystemContext.MS_Unit on a.unitID equals b.Id
        //                  join d in _personalContext.PERSONAL.ToList() on x.psCode equals d.psCode
        //                  join e in _propertySystemContext.TR_BookingHeader on a.bookingHeaderID equals e.Id
        //                  join f in _propertySystemContext.MS_TujuanTransaksi on e.tujuanTransaksiID equals f.Id
        //                  join g in _propertySystemContext.MS_SumberDana on e.sumberDanaID equals g.Id
        //                  join h in _personalContext.TR_ID.ToList() on x.psCode equals h.psCode into iden
        //                  from h in iden.DefaultIfEmpty()
        //                  join j in _personalContext.PERSONALS_MEMBER.ToList() on new { e.memberCode, x.scmCode } equals new { j.memberCode, j.scmCode }
        //                  join k in _personalContext.PERSONAL.ToList() on j.psCode equals k.psCode
        //                  join l in _personalContext.TR_Phone.ToList() on k.psCode equals l.psCode into phone
        //                  from l in phone.DefaultIfEmpty()
        //                  join m in _propertySystemContext.MS_Project on b.projectID equals m.Id into project
        //                  from m in project.DefaultIfEmpty()
        //                  join p in _propertySystemContext.MS_Detail on b.detailID equals p.Id
        //                  join s in _propertySystemContext.MS_Term on e.termID equals s.Id
        //                  where x.Id == Convert.ToInt32(data.order_id) && new string[] { "1", "5", "7" }.Contains(h.idType)
        //                  select new KonfirmasiPesananDto
        //                  {
        //                      orderCode = x.orderCode,
        //                      kodePelanggan = x.psCode,
        //                      //tanggalBooking = String.Format("{0: dd MMM yyyy}", e.bookDate),
        //                      psName = x.psName,
        //                      birthDate = String.Format("{0: dd MMM yyyy}", d.birthDate),
        //                      noHpPembeli = x.psPhone,
        //                      noIdentitas = (h == null ? "-" : h.idNo),
        //                      noNPWP = d.NPWP,
        //                      email = x.psEmail,
        //                      BookCode = e.bookCode,
        //                      hargaJual = a.sellingPrice.ToString(),
        //                      bfAmount = a.BFAmount.ToString(),
        //                      imageProject = m.image,
        //                      noHp = l.number,
        //                      noDealCloser = k.psCode,
        //                      namaDealCloser = k.name,
        //                      caraPembayaran = s.remarks,
        //                      tujuanTransaksi = f.tujuanTransaksiName,
        //                      sumberDanaPembelian = g.sumberDanaName,
        //                      namaBank = e.bankRekeningPemilik,
        //                      noRekening = e.nomorRekeningPemilik,
        //                      unitID = e.unitID
        //                  }).FirstOrDefault();

        //var dataUnit = (from a in _propertySystemContext.MS_Unit
        //                join b in _propertySystemContext.MS_Project on a.projectID equals b.Id
        //                join c in _propertySystemContext.MS_Cluster on a.clusterID equals c.Id
        //                join d in _propertySystemContext.MS_UnitItem on a.Id equals d.unitID
        //                join e in _propertySystemContext.MS_Detail on a.detailID equals e.Id
        //                join f in _propertySystemContext.MS_Category on a.categoryID equals f.Id
        //                join g in _propertySystemContext.MS_UnitCode on a.unitCodeID equals g.Id
        //                join h in _propertySystemContext.TR_BookingHeader on a.Id equals h.unitID
        //                join i in _propertySystemContext.TR_BookingItemPrice on new { bookingHeaderID = h.Id, itemID = d.itemID } equals new { bookingHeaderID = i.bookingHeaderID, itemID = i.itemID }
        //                join j in _propertySystemContext.MS_Renovation on i.renovCode equals j.renovationCode into renov
        //                from j in renov.DefaultIfEmpty()
        //                join k in _propertySystemContext.TR_UnitOrderDetail on new { h.unitID, renovID = j.Id } equals new { k.unitID, k.renovID }
        //                group d by new
        //                {
        //                    d.unitID,
        //                    a.unitNo,
        //                    g.unitCode,
        //                    c.clusterName,
        //                    f.categoryName,
        //                    e.detailName,
        //                    j.renovationName
        //                } into G
        //                where G.Key.unitID == dataKP.unitID
        //                select new ListunitDto
        //                {
        //                    UnitNo = G.Key.unitNo,
        //                    UnitCode = G.Key.unitCode.Contains("-") ? G.Key.unitCode : null,
        //                    category = G.Key.categoryName,
        //                    cluster = G.Key.clusterName,
        //                    luas = G.Sum(d => d.area).ToString(),
        //                    renovation = G.Key.renovationName,
        //                    tipe = G.Key.detailName
        //                }).ToList();

        //    dataKP.listUnit = dataUnit;

        //    var dataBank = (from bank in _propertySystemContext.MS_BankOLBooking
        //                    join unit in _propertySystemContext.MS_Unit on new { bank.projectID, bank.clusterID } equals new { unit.projectID, unit.clusterID }
        //                    join header in _propertySystemContext.TR_BookingHeader on unit.Id equals header.unitID
        //                    where unit.Id == dataKP.unitID
        //                    select new listBankDto
        //                    {
        //                        bankName = bank.bankName,
        //                        noVA = bank.bankRekNo
        //                    }).ToList();

        //    dataKP.listBank = dataBank;

        //    var arrTerm = new List<listIlustrasiPembayaran>();

        //    var Cicilan = new List<listCicilan>();

        //    var getTerm = (from a in _propertySystemContext.TR_UnitOrderHeader
        //                   join b in _propertySystemContext.TR_UnitOrderDetail on a.Id equals b.UnitOrderHeaderID
        //                   join c in _propertySystemContext.TR_BookingHeader on b.bookingHeaderID equals c.Id
        //                   join d in _propertySystemContext.TR_BookingHeaderTerm on new { bookingHeaderID = c.Id, c.termID } equals new { bookingHeaderID = d.bookingHeaderID, d.termID }
        //                   where a.Id == Convert.ToInt32(data.order_id)
        //                   select new listIlustrasiPembayaran
        //                   {
        //                       termID = c.termID,
        //                       termName = d.remarks,
        //                       bookingFee = b.BFAmount,
        //                       sellingPrice = b.sellingPrice,
        //                       tglJatuhTempo = c.bookDate,
        //                       //orderHeaderID = a.Id
        //                   }).ToList();

        //    var getDPTerm = (from a in getTerm
        //                     join e in _propertySystemContext.MS_TermDP on a.termID equals e.termID
        //                     where a.orderHeaderID == Convert.ToInt32(data.order_id)
        //                     select new
        //                     {
        //                         a.sellingPrice,
        //                         e.DPNo,
        //                         e.DPPct,
        //                         e.daysDue,
        //                         a.tglJatuhTempo,
        //                         a.termID
        //                     }).ToList();

        //    var arrDPNo = new List<DPNoDto>();

        //    var dataTerm = new List<DPNoDtos>();

        //    foreach (var item in getDPTerm)
        //    {

        //        var dataListDPNos = new DPNoDtos
        //        {
        //            DPNo = item.DPNo,
        //            tglJatuhTempo = item.tglJatuhTempo.AddDays(item.daysDue),
        //            termID = item.termID
        //        };
        //        dataTerm.Add(dataListDPNos);
        //    }

        //    var datas = new DPNoDto
        //    {
        //        listDPNos = dataTerm,
        //        countDP = dataTerm.Count
        //    };

        //    arrDPNo.Add(datas);

        //    arrTerm.Add(getTerm.FirstOrDefault());

        //    var getTop5 = (from a in _propertySystemContext.TR_UnitOrderHeader
        //                   join b in _propertySystemContext.TR_UnitOrderDetail on a.Id equals b.UnitOrderHeaderID
        //                   join c in _propertySystemContext.TR_BookingHeader on b.bookingHeaderID equals c.Id
        //                   join d in _propertySystemContext.TR_BookingHeaderTerm on c.Id equals d.bookingHeaderID
        //                   where a.Id == Convert.ToInt32(data.order_id) && d.termID != c.termID
        //                   orderby d.Id ascending
        //                   select new listIlustrasiPembayaran
        //                   {
        //                       termID = d.termID,
        //                       termName = d.remarks,
        //                       bookingFee = b.BFAmount,
        //                       sellingPrice = b.sellingPrice,
        //                       tglJatuhTempo = c.bookDate,
        //                       orderHeaderID = a.Id
        //                   }).Take(5).ToList();

        //    foreach (var dto in getTop5)
        //    {
        //        var dataDPNo = new List<DPNoDtos>();

        //        var getDPTop5 = (from a in getTop5
        //                         join b in _propertySystemContext.MS_TermDP on a.termID equals b.termID
        //                         where a.orderHeaderID == Convert.ToInt32(data.order_id) && a.termID == dto.termID
        //                         select new
        //                         {
        //                             b.DPNo,
        //                             a.tglJatuhTempo,
        //                             b.daysDue,
        //                             a.termID
        //                         }).ToList();


        //        foreach (var item in getDPTop5)
        //        {
        //            var dataDP = new DPNoDtos
        //            {
        //                DPNo = item.DPNo,
        //                tglJatuhTempo = item.tglJatuhTempo.AddDays(item.daysDue),
        //                termID = item.termID
        //            };
        //            dataDPNo.Add(dataDP);
        //        }

        //        var dataListDPNo = new DPNoDto
        //        {
        //            listDPNos = dataDPNo,
        //            countDP = dataDPNo.Count
        //        };

        //        arrDPNo.Add(dataListDPNo);

        //        arrTerm.Add(dto);
        //    }

        //    var lala = (from a in arrDPNo
        //                orderby a.countDP descending
        //                select a.listDPNos);

        //    var DPNO = lala.FirstOrDefault();

        //    var allAmount = new List<listAllAmount>();

        //    foreach (var item in DPNO)
        //    {
        //        var amountList = new List<listAmountDto>();

        //        foreach (var dto in arrTerm)
        //        {
        //            var getDPAmountTop5 = (from a in arrTerm
        //                                   join b in _propertySystemContext.MS_TermDP on a.termID equals b.termID
        //                                   where a.orderHeaderID == Convert.ToInt32(data.order_id) && a.termID == dto.termID
        //                                         && item.tglJatuhTempo == a.tglJatuhTempo.AddDays(b.daysDue)
        //                                   select new
        //                                   {
        //                                       b.DPPct,
        //                                       b.daysDue
        //                                   });

        //            if (getDPAmountTop5.FirstOrDefault() != null)
        //            {
        //                var dataAmount = new listAmountDto
        //                {
        //                    amount = Math.Round(dto.sellingPrice * (decimal)getDPAmountTop5.FirstOrDefault().DPPct, 0),
        //                    termID = dto.termID,
        //                    tglJatuhTempo = item.tglJatuhTempo
        //                };
        //                var dataAllAmount = new listAllAmount
        //                {
        //                    amount = Math.Round(dto.sellingPrice * (decimal)getDPAmountTop5.FirstOrDefault().DPPct, 0),
        //                    termID = dto.termID,
        //                    tglJatuhTempo = item.tglJatuhTempo
        //                };
        //                amountList.Add(dataAmount);
        //                allAmount.Add(dataAllAmount);
        //            }
        //            else
        //            {
        //                var dataAmount = new listAmountDto
        //                {
        //                    amount = 0,
        //                    termID = dto.termID
        //                };
        //                var dataAllAmount = new listAllAmount
        //                {
        //                    amount = 0,
        //                    termID = dto.termID
        //                };
        //                allAmount.Add(dataAllAmount);
        //                amountList.Add(dataAmount);
        //            }
        //        }
        //        item.listAmount = amountList;
        //    }

        //    var totalDP = (from a in allAmount
        //                   group a by
        //                   a.termID
        //                   into G
        //                   select new
        //                   {
        //                       termID = G.Key,
        //                       total = G.Sum(a => a.amount)
        //                   }).ToList();

        //    var listFin = new List<listFinTimesDto>();

        //    foreach (var item in arrTerm)
        //    {
        //        var dataTotalAmount = totalDP.Where(a => a.termID == item.termID).FirstOrDefault().total;

        //        item.totalDP = dataTotalAmount;

        //        var lastSellingPrice = item.sellingPrice - (item.bookingFee + item.totalDP);

        //        item.leftSellingPrice = lastSellingPrice;

        //        var getFin = (from a in _propertySystemContext.MS_TermPmt
        //                      join b in _propertySystemContext.LK_FinType on a.finTypeID equals b.Id
        //                      where a.termID == item.termID
        //                      select new listFinTimesDto
        //                      {
        //                          termID = a.termID,
        //                          finStartDue = a.finStartDue,
        //                          finTimes = b.finTimes
        //                      }).FirstOrDefault();

        //        listFin.Add(getFin);
        //    }

        //    var finTimesTerbersar = listFin.OrderByDescending(a => a.finTimes).FirstOrDefault();

        //    var listCicilan = new List<listCicilans>();

        //    for(var i = 1; i <= finTimesTerbersar.finTimes; i++)
        //    {
        //        var getTgl = DPNO.OrderByDescending(a => a.tglJatuhTempo).FirstOrDefault();

        //        var getTglJatuhTempo = getTgl.tglJatuhTempo.AddDays(finTimesTerbersar.finStartDue);

        //        if (i == 1)
        //        {
        //            var dataCicilan = new listCicilans
        //            {
        //                cicilanNo = i,
        //                tglJatuhTempo = getTglJatuhTempo
        //            };

        //            listCicilan.Add(dataCicilan);

        //        }
        //        else
        //        {
        //            var dataCicilan = new listCicilans
        //            {
        //                cicilanNo = i,
        //                tglJatuhTempo = getTglJatuhTempo.AddMonths(i)
        //            };
        //            listCicilan.Add(dataCicilan);
        //        }
        //    }

        //    var listPelunasan2 = new List<listPelunasan2>();
        //    var listPelunasan1 = new List<listPelunasan1dto>();

        //    for (var x = 1; x <= arrTerm.Count; x++)
        //    {
        //        var listCicil = new List<listCicilanDto>();

        //        var getFinTerm = (from b in _propertySystemContext.MS_TermPmt
        //                          join c in _propertySystemContext.LK_FinType on b.finTypeID equals c.Id
        //                          where b.termID == arrTerm[x-1].termID
        //                          select new
        //                          {
        //                              c.finTimes,
        //                              b.termID,
        //                              b.finStartDue
        //                          }).FirstOrDefault();

        //        var cicilanAmount = arrTerm[x - 1].leftSellingPrice / getFinTerm.finTimes;

        //        if (getFinTerm.finTimes != 1)
        //        {
        //            for (var i = 1; i <= getFinTerm.finTimes; i++)
        //            {
        //                var dataCicil = new listCicilanDto
        //                {
        //                    amount = cicilanAmount,
        //                    cicilanNo = i
        //                };
        //                listCicil.Add(dataCicil);
        //            }

        //            arrTerm[x - 1].listCicilan = listCicil;
        //        }
        //        else
        //        {
        //            if(x == 1)
        //            {
        //                var dataPelunasan1 = new listPelunasan1dto
        //                {
        //                    amount = cicilanAmount,
        //                    termID = arrTerm[x - 1].termID,
        //                    finStartDue = getFinTerm.finStartDue
        //                };

        //                listPelunasan1.Add(dataPelunasan1);

        //                var dataPelunasan = new listPelunasan2
        //                {
        //                    amount = 0,
        //                    termID = arrTerm[x - 1].termID
        //                };
        //                listPelunasan2.Add(dataPelunasan);
        //            }
        //            else
        //            {
        //                var dataPelunasan = new listPelunasan2
        //                {
        //                    amount = cicilanAmount,
        //                    termID = arrTerm[x - 1].termID,
        //                    finStartDue = getFinTerm.finStartDue
        //                };
        //                listPelunasan2.Add(dataPelunasan);

        //                var dataPelunasan1 = new listPelunasan1dto
        //                {
        //                    amount = 0,
        //                    termID = arrTerm[x - 1].termID
        //                };

        //                listPelunasan1.Add(dataPelunasan1);
        //            }
        //        }
        //    }

        //    foreach (var item in listCicilan)
        //    {
        //        var amtCicilan = new List<listCicilanAmount>();

        //        foreach (var dto in arrTerm)
        //        {
        //            if(dto.listCicilan != null)
        //            {
        //                var getAmount = (from a in dto.listCicilan
        //                                 where a.cicilanNo == item.cicilanNo
        //                                 select a.amount).FirstOrDefault();


        //                var amountCicilan = new listCicilanAmount
        //                {
        //                    amount = getAmount != 0 ? getAmount : 0
        //                };
        //                amtCicilan.Add(amountCicilan);
        //            }
        //            else
        //            {
        //                var amountCicilan = new listCicilanAmount
        //                {
        //                    amount =  0
        //                };
        //                amtCicilan.Add(amountCicilan);
        //            }
        //        }
        //        item.listCicilanAmount = amtCicilan;
        //    }

        //    foreach (var item in DPNO)
        //    {
        //        if(listPelunasan1 != null)
        //        {
        //            foreach (var dto in listPelunasan1)
        //            {
        //                var getDP = (from a in item.listAmount
        //                             where a.termID == dto.termID && a.amount != 0
        //                             orderby item.tglJatuhTempo descending
        //                             select new
        //                             {
        //                                tgl = item.tglJatuhTempo
        //                             }).FirstOrDefault();

        //                if(getDP != null)
        //                {
        //                    dto.tglJatuhTempo = getDP.tgl.AddDays(dto.finStartDue);
        //                }
        //                else
        //                {
        //                    dto.tglJatuhTempo = null;
        //                }
        //            }
        //        }

        //        if(listPelunasan2 != null)
        //        {
        //            foreach (var dto in listPelunasan2)
        //            {
        //                var getDP = (from a in item.listAmount
        //                             where a.termID == dto.termID && a.amount != 0
        //                             orderby item.tglJatuhTempo descending
        //                             select new
        //                             {
        //                                 tgl = item.tglJatuhTempo
        //                             }).FirstOrDefault();

        //                if (getDP != null)
        //                {
        //                    dto.tglJatuhTempo = getDP.tgl.AddDays(dto.finStartDue);
        //                }
        //                else
        //                {
        //                    dto.tglJatuhTempo = null;
        //                }
        //            }
        //        }
        //    }

        //    dataKP.listPelunasan = listPelunasan2;
        //    dataKP.listPelunasan1 = listPelunasan1;
        //    dataKP.listCicilan = listCicilan;
        //    dataKP.listDPNo = DPNO;
        //    dataKP.ilustrasiPembayaran = arrTerm;

        //Logger.InfoFormat(json);

        //var getProjectInfo = (from project in _propertySystemContext.MS_Project
        //                      join info in _propertySystemContext.MS_ProjectInfo on project.Id equals info.projectID into a
        //                      from projectInfo in a.DefaultIfEmpty()
        //                      join unit in _propertySystemContext.MS_Unit on project.Id equals unit.projectID
        //                      where unit.Id == dataKP.unitID
        //                      orderby projectInfo.CreationTime descending
        //                      select new
        //                      {
        //                          project.projectName,
        //                          projectInfo.projectMarketingOffice,
        //                          projectInfo.projectMarketingPhone
        //                      }).FirstOrDefault();

        //Logger.InfoFormat(JsonConvert.SerializeObject(getProjectInfo));


        //var sendEmail = new BookingSuccessInputDto
        //{
        //    bookDate = DateTime.Now,
        //    customerName = dataKP.psName,
        //    devPhone = getProjectInfo.projectMarketingPhone != null ? getProjectInfo.projectMarketingPhone : "-",
        //    memberName = dataKP.namaDealCloser,
        //    memberPhone = dataKP.noHp,
        //    projectImage = dataKP.imageProject,
        //    projectName = getProjectInfo.projectName
        //};
        //var body = _emailAppService.bookingSuccess(sendEmail);

        //using (var client = new HttpClient())
        //{
        //    client.Timeout = TimeSpan.FromMinutes(10);
        //    var url = _configuration.ApiPdfUrl.EnsureEndsWith('/') + "api/Pdf/KonfirmasiPesananPdf";

        //    var request = new HttpRequestMessage(HttpMethod.Post, url);
        //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(MimeTypeNames.ApplicationJson));
        //    request.Content = new StringContent(JsonConvert.SerializeObject(dataKP), Encoding.UTF8, MimeTypeNames.ApplicationJson);

        //    var response = await ReadResponse(url, client, request);
        //    //response.Replace(@"\", null);
        //    //response.Replace(@"\\", null);

        //    Logger.InfoFormat(response);

        //    var email = new SendEmailInputDto
        //    {
        //        body = body,
        //        toAddress = dataKP.email,
        //        subject = "Konfirmasi Pemesanan Unit" + dataUnit.FirstOrDefault().UnitCode + " " + dataUnit.FirstOrDefault().UnitNo,
        //        pathKP = response
        //    };

        //    _emailAppService.ConfigurationEmail(email);
        //}

        //using (var client = new WebClient())
        //{
        //    var url = _configuration.ApiPdfUrl.EnsureEndsWith('/') + "api/Pdf/KonfirmasiPesananPdf";
        //    client.Headers.Add("Content-Type:application/json");
        //    client.Headers.Add("Accept:application/json");
        //    var result = client.UploadString(url, JsonConvert.SerializeObject(dataKP));
        //    var trimResult = result.Replace(@"\\", @"\").Trim(new char[1] { '"' });
        //    Logger.InfoFormat(trimResult);

        //    var email = new SendEmailInputDto
        //    {
        //        body = body,
        //        toAddress = dataKP.email,
        //        subject = "Konfirmasi Pemesanan Unit" + dataUnit.FirstOrDefault().UnitCode + " " + dataUnit.FirstOrDefault().UnitNo,
        //        pathKP = trimResult
        //    };

        //    _emailAppService.ConfigurationEmail(email);
        //}

        //    return dataKP;

        //}

        private async Task<string> ReadResponse(string url, HttpClient client, HttpRequestMessage request)
        {
            var response = await client.SendAsync(request);
            var success = await response.Content.ReadAsStringAsync();
            var trimString = success.Replace(@"\\", @"\").Trim(new char[1] { '"' });

            return trimString;
        }

        #region debug console
        protected void SendConsole(string msg)
        {
            if (Setting_variabel.enable_tcp_debug == true)
            {
                if (Setting_variabel.Komunikasi_TCPListener == null)
                {
                    Setting_variabel.Komunikasi_TCPListener = new Visionet_Backend_NetCore.Komunikasi.Komunikasi_TCPListener(17000);
                    Task.Run(() => StartListenerLokal());
                }

                if (Setting_variabel.Komunikasi_TCPListener != null)
                {
                    if (Setting_variabel.Komunikasi_TCPListener.IsRunning())
                    {
                        if (Setting_variabel.ConsoleBayangan != null)
                        {
                            Setting_variabel.ConsoleBayangan.Send("[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "] " + msg);
                        }
                    }
                }
            }

        }
        #endregion

        #region listener tcp debug
        protected void StartListenerLokal()
        {
            if (Setting_variabel.Komunikasi_TCPListener != null)
                Setting_variabel.Komunikasi_TCPListener.StartListener();
        }

        protected void StopListenerLokal()
        {
            if (Setting_variabel.Komunikasi_TCPListener != null)
                Setting_variabel.Komunikasi_TCPListener.StopListener();
        }
        #endregion

    }
}
