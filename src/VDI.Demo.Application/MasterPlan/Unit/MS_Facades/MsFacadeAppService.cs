﻿using System;
using System.Linq;
using System.Data;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.UI;
using VDI.Demo.Authorization;
using VDI.Demo.MasterPlan.Unit.MS_Facades.Dto;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;
using VDI.Demo.PropertySystemDB.LippoMaster;

namespace VDI.Demo.MasterPlan.Unit.MS_Facades
{
    public class MsFacadeAppService : DemoAppServiceBase, IMsFacadeAppService
    {
        private readonly IRepository<MS_Facade> _msFacadeRepo;
        private readonly IRepository<MS_Detail> _msDetailRepo;
        private readonly IRepository<MS_Unit> _msUnitRepo;
        private readonly IRepository<MS_Cluster> _msClusterRepo;
        private readonly IRepository<MS_Project> _msProjectRepo;
        private readonly IRepository<TR_BookingHeader> _trBookingHeaderRepo;

        public MsFacadeAppService(
            IRepository<MS_Facade> msFacadeRepo,
            IRepository<MS_Detail> msDetailRepo,
            IRepository<MS_Unit> msUnitRepo,
            IRepository<MS_Cluster> msClusterRepo,
            IRepository<MS_Project> msProjectRepo,
            IRepository<TR_BookingHeader> trBookingHeaderRepo
            )
        {
            _msFacadeRepo = msFacadeRepo;
            _msDetailRepo = msDetailRepo;
            _msUnitRepo = msUnitRepo;
            _msClusterRepo = msClusterRepo;
            _msProjectRepo = msProjectRepo;
            _trBookingHeaderRepo = trBookingHeaderRepo;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_MasterFacade_Create)]
        public void CreateMsFacade(CreateMsFacadeInput input)
        {
            Logger.InfoFormat("CreateMsFacade() - Started.");

            Logger.DebugFormat("CreateMsFacade() - Start checking existing facadeCode. Parameters sent: {0} " +
                "facadeCode = {1}{0}" +
                "facadeName = {2}{0}", Environment.NewLine, input.facadeCode, input.facadeName);
            
            var checkCode = (from facade in _msFacadeRepo.GetAll()
                             where facade.facadeCode == input.facadeCode || facade.facadeName == input.facadeName
                             select facade.Id).Any();

            Logger.DebugFormat("CreateMsFacade() - End checking existing facadeCode. Result = {0}", checkCode);

            if (!checkCode)
            {
                var data = new MS_Facade
                {
                    entityID = 1,
                    facadeCode = input.facadeCode,
                    facadeName = input.facadeName,
                    projectID = input.projectID,
                    clusterID = input.clusterID,
                    detailID = input.detailID
                };

                try
                {
                    Logger.DebugFormat("CreateMsFacade() - Start insert Facade. Parameters sent: {0} " +
                        "entityID = {1}{0}" +
                        "facadeCode = {2}{0}" +
                        "facadeName = {3}{0}" +
                        "projectID = {4}{0}" +
                        "clusterID = {5}{0}" +
                        "detailID  = {6}{0}" , Environment.NewLine, 1, input.facadeCode, input.facadeName, input.projectID, input.clusterID, input.detailID);
                    _msFacadeRepo.Insert(data);
                    CurrentUnitOfWork.SaveChanges(); //execution saved inside try
                    Logger.DebugFormat("CreateMsFacade() - End insert Facade");
                }
                catch (DataException ex)
                {
                    Logger.DebugFormat("CreateMsFacade() - ERROR DataException. Result = {0}", ex.Message);
                    throw new UserFriendlyException("Db Error: " + ex.Message);
                }
                catch (Exception ex)
                {
                    Logger.DebugFormat("CreateMsFacade() - ERROR Exception. Result = {0}", ex.Message);
                    throw new UserFriendlyException("Error: " + ex.Message);
                }
            }
            else
            {
                Logger.DebugFormat("CreateMsFacade() - ERROR. Result = {0}", "Facade Code or Name already exist!");
                throw new UserFriendlyException("Facade Code or Name already exist!");
            }
            Logger.InfoFormat("CreateMsFacade() - Finished.");
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_MasterFacade_Delete)]
        public void DeleteMsFacade(int Id)
        {
            Logger.InfoFormat("DeleteMsFacade() - Started.");

            var checkBookingHeader = (from x in _trBookingHeaderRepo.GetAll()
                                      where x.facadeID == Id
                                      select x.Id).Any();

            if (!checkBookingHeader)
            {
                try
                {
                    Logger.DebugFormat("DeleteMsFacade() - Start Delete msFacade. Parameters sent: {0} " +
                        "facadeID = {1}{0}", Environment.NewLine, Id);
                    _msFacadeRepo.Delete(Id);
                    CurrentUnitOfWork.SaveChanges(); //execution saved inside try
                    Logger.DebugFormat("DeleteMsFacade() - End Delete msFacade.");
                }
                catch (DataException ex)
                {
                    Logger.DebugFormat("DeleteMsFacade() - ERROR DataException. Result = {0}", ex.Message);
                    throw new UserFriendlyException("Db Error: " + ex.Message);
                }
                catch (Exception ex)
                {
                    Logger.DebugFormat("DeleteMsFacade() - ERROR Exception. Result = {0}", ex.Message);
                    throw new UserFriendlyException("Error: " + ex.Message);
                }
            }
            else
            {
                throw new UserFriendlyException("This Facade is used!");
            }
            
            Logger.InfoFormat("DeleteMsFacade() - Finished.");
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_MasterFacade)]
        public ListResultDto<GetMsFacadeListDto> GetAllMsFacadeByProject(int projectID)
        {
            var result = (from facade in _msFacadeRepo.GetAll()
                          join project in _msProjectRepo.GetAll() on facade.projectID equals project.Id
                          join cluster in _msClusterRepo.GetAll() on facade.clusterID equals cluster.Id
                          join detail in _msDetailRepo.GetAll() on facade.detailID equals detail.Id
                          select new GetMsFacadeListDto
                          {
                              Id = facade.Id,
                              facadeCode = facade.facadeCode,
                              facadeName = facade.facadeName,
                              projectID = project.Id,
                              clusterID = cluster.Id,
                              detailID = detail.Id,
                              detailCode = detail.detailCode,
                              detailName = detail.detailName
                          }).ToList();

            return new ListResultDto<GetMsFacadeListDto>(result);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_MasterFacade)]
        public ListResultDto<GetUnitDetailByProjectClusterListDto> GetUnitDetailByProjectCluster(int projectID, int clusterID)
        {
            var getDetailsFromMsUnit = (from x in _msUnitRepo.GetAll()
                                         where x.projectID == projectID
                                         select x.detailID).Distinct().ToList();

            if (getDetailsFromMsUnit == null)
            {
                throw new UserFriendlyException("No Detail data found in Unit");
            }

            var getDataDetails = (from y in _msDetailRepo.GetAll()
                                   where getDetailsFromMsUnit.Contains(y.Id)
                                   select new GetUnitDetailByProjectClusterListDto
                                   {
                                       detailID = y.Id,
                                       detailName = y.detailName,
                                       detailCode = y.detailCode
                                   }).ToList();

            return new ListResultDto<GetUnitDetailByProjectClusterListDto>(getDataDetails);
        }

        private void UpdateMsFacade(UpdateMsFacadeInput input)
        {
            Logger.InfoFormat("UpdateMsFacade() - Started.");

            Logger.DebugFormat("UpdateMsFacade() - Start checking existing facadeCode. Parameters sent: {0} " +
                "facadeCode = {1}{0}" +
                "Id = {2}{0}", Environment.NewLine, input.facadeCode, input.Id);
            var checkCode = (from facade in _msFacadeRepo.GetAll()
                             where facade.facadeCode == input.facadeCode &&
                             (facade.Id != input.Id)
                             select facade).Any();
            Logger.DebugFormat("UpdateMsFacade() - End checking existing facadeCode. Result = {0}", checkCode);

            if (!checkCode)
            {
                Logger.DebugFormat("UpdateMsFacade() - Start get data face for update. Parameters sent: {0} " +
                    "facadeID = {1}{0}", Environment.NewLine, input.Id);
                var getFacade = (from facade in _msFacadeRepo.GetAll()
                                 where facade.Id == input.Id
                                 select facade).FirstOrDefault();
                Logger.DebugFormat("UpdateMsFacade() - End get data face for update. Result = {0}", getFacade);

                var data = getFacade.MapTo<MS_Facade>();

                data.entityID = 1;
                data.facadeCode = input.facadeCode;
                data.facadeName = input.facadeName;
                data.projectID = input.projectID;
                data.clusterID = input.clusterID;
                data.detailID = input.detailID;

                try
                {
                    Logger.DebugFormat("UpdateMsFacade() - Start Update msFacade. Parameters sent: {0} " +
                       "entityID = {1}{0}" +
                       "facadeCode = {2}{0}" +
                       "facadeName = {3}{0}" +
                       "projectID = {4}{0}" +
                       "clusterID = {5}{0}" +
                       "detailID = {6}{0}"
                       , Environment.NewLine, 1, input.facadeCode, input.facadeName, input.projectID, input.clusterID, input.detailID);
                    _msFacadeRepo.Update(data);
                    CurrentUnitOfWork.SaveChanges(); //execution saved inside try
                    Logger.DebugFormat("UpdateMsFacade() - End Update msFacade.");
                }
                /*catch (DbEntityValidationException ex)
                {
                    var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);
                    string fullErrorMessage = string.Join("; ", errorMessages);
                    string exceptionMessage = string.Concat("Validation Error: ", fullErrorMessage);
                    Logger.DebugFormat("UpdateMsFacade() - ERROR DbEntityValidationException. Result = {0}", exceptionMessage);
                    throw new UserFriendlyException(exceptionMessage);
                }*/
                catch (DataException ex)
                {
                    Logger.DebugFormat("UpdateMsFacade() - ERROR DataException. Result = {0}", ex.Message);
                    throw new UserFriendlyException("Db Error: " + ex.Message);
                }
                catch (Exception ex)
                {
                    Logger.DebugFormat("UpdateMsFacade() - ERROR Exception. Result = {0}", ex.Message);
                    throw new UserFriendlyException("Error: " + ex.Message);
                }
            }
            else
            {
                Logger.DebugFormat("UpdateMsFacade() - ERROR. Result = {0}", "Facade Code already exist!");
                throw new UserFriendlyException("Facade Code already exist!");
            }
            Logger.InfoFormat("UpdateMsFacade() - Finished.");
        }
    }
}
