﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.UI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json.Linq;
using VDI.Demo.Authorization;
using VDI.Demo.Files;
using VDI.Demo.MasterPlan.Unit.MS_Clusters.Dto;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;

namespace VDI.Demo.MasterPlan.Unit.MS_Clusters
{
    public class MsClusterAppService : DemoAppServiceBase, IMsClusterAppService
    {
        private readonly IRepository<MS_Cluster> _msClusterRepo;
        private readonly IRepository<MS_Unit> _msUnitRepo;
        private readonly IRepository<MS_Project> _msProjectRepo;
        private readonly IRepository<MS_ClusterPenalty> _msClusterPenaltyRepo;
        private readonly IRepository<MP_ClusterHandOverPeriode> _mpClusterHandOverPeriodeRepo;
        private static IHttpContextAccessor _HttpContextAccessor;
        private readonly FilesHelper _filesHelper;
        private readonly IHostingEnvironment _hostingEnvironment;

        public MsClusterAppService(
            IRepository<MS_Cluster> msClusterRepo,
            IRepository<MS_Project> msProjectRepo,
            IRepository<MS_Unit> msUnitRepo,
            IRepository<MS_ClusterPenalty> msClusterPenaltyRepo,
            IRepository<MP_ClusterHandOverPeriode> mpClusterHandOverPeriodeRepo,
            IHttpContextAccessor httpContextAccessor,
            FilesHelper filesHelper,
            IHostingEnvironment environment
        )
        {
            _msProjectRepo = msProjectRepo;
            _msClusterRepo = msClusterRepo;
            _msUnitRepo = msUnitRepo;
            _msClusterPenaltyRepo = msClusterPenaltyRepo;
            _mpClusterHandOverPeriodeRepo = mpClusterHandOverPeriodeRepo;
            _HttpContextAccessor = httpContextAccessor;
            _filesHelper = filesHelper;
            _hostingEnvironment = environment;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_MasterCluster_Create)]
        public void CreateMsCluster(CreateOrUpdateMsClusterInputDto input)
        {
            Logger.Info("CreateMsCluster() Started.");

            Logger.DebugFormat("CreateMsCluster() - Start checking existing cluster code. Parameters sent:{0}" +
                            "clusterCode     = {1}{0}" +
                            "clusterName     = {2}"
                            , Environment.NewLine, input.clusterCode, input.clusterName);

            var checkCode = (from x in _msClusterRepo.GetAll()
                             where x.clusterCode == input.clusterCode
                             select x).Any();

            Logger.DebugFormat("CreateMsCluster() - End checking existing cluster code. Result:{0}", checkCode);

            if (!checkCode)
            {
                string fileUrl;
                if (input.termConditionFile.IsNullOrWhiteSpace())
                {
                    fileUrl = "-";
                }
                else
                {
                    fileUrl = MoveFile(input.termConditionFile);
                    GetURLWithoutHost(fileUrl, out fileUrl);
                }

                var data = new MS_Cluster
                {
                    entityID = 1,
                    clusterCode = input.clusterCode,
                    clusterName = input.clusterName,
                    projectID = input.projectID,
                    sortNo = 0, // hardcode
                    penaltyRate = 0, // hardcode
                    startPenaltyDay = 0, // hardcode
                    termConditionFile = fileUrl,
                    website = input.website
                };
                Logger.DebugFormat("CreateMsCluster() - Start insert cluster. Parameters sent:{0}" +
                "	entityID	    = {1}{0}" +
                "	clusterCode	    = {2}{0}" +
                "	clusterName	    = {3}{0}" +
                "	projectID	    = {4}{0}" +
                "	sortNo      	= 0{0}" +
                "	penaltyRate 	= 0{0}" +
                "	startPenaltyDay = 0{0}" +
                "   termConditionFile = {5}{0}" +
                "   website         = {6}{0}"
                , Environment.NewLine, 1, input.clusterCode, input.clusterName, input.projectID, fileUrl, input.website);

                try
                {
                    var clusterID = _msClusterRepo.InsertAndGetId(data);
                    CreateOrUpdateMappingClusterHandOverPeriode(input.CreateOrUpdateHandOverPeriodeDto, clusterID);
                }
                catch (DataException exDb)
                {
                    Logger.ErrorFormat("CreateMsCluster() ERROR DbException. Result = {0}", exDb.Message);
                    throw new UserFriendlyException("Database Error : {0}", exDb.Message);
                }
                // Handle all other exceptions.
                catch (Exception ex)
                {
                    Logger.ErrorFormat("CreateMsCluster() ERROR Exception. Result = {0}", ex.Message);
                    throw new UserFriendlyException("Error : {0}", ex.Message);
                }

                Logger.DebugFormat("CreateMsCluster() - End insert cluster.");
            }
            else
            {
                Logger.ErrorFormat("CreateMsCluster() ERROR. Result = {0}", "Cluster Code Already Exist !");
                throw new UserFriendlyException("Cluster Code Already Exist !");
            }
            Logger.Info("CreateMsCluster() Finished.");
        }

        private void CreateOrUpdateMappingClusterHandOverPeriode(List<CreateOrUpdateHandOverPeriodeDto> input, int clusterID)
        {
            Logger.Info("CreateOrUpdateMappingClusterHandOverPeriode() Started.");

            DeleteMpClusterHandOverPeriode(clusterID);

            foreach (var item in input)
            {
                var dataToInsert = new MP_ClusterHandOverPeriode
                {
                    entityID = 1,
                    clusterID = clusterID,
                    handOverStart = item.handOverStart,
                    handOverEnd = item.handOverEnd,
                    handOverDue = item.handOverDue,
                    graceDue = item.graceDue
                };

                Logger.DebugFormat("CreateOrUpdateMappingClusterHandOverPeriode() - Start insert MP_ClusterHandOverPeriode. Parameters sent:{0}" +
                " entityID        = {1}{0}" +
                " clusterID       = {2}{0}" +
                " handOverStart   = {3}{0}" +
                " handOverEnd     = {4}{0}" +
                " handOverDue     = {5}{0}" +
                " graceDue        = {6}{0}"
                , Environment.NewLine, 1, clusterID, item.handOverStart, item.handOverEnd, item.handOverDue, item.graceDue);

                try
                {
                    _mpClusterHandOverPeriodeRepo.Insert(dataToInsert);
                    Logger.DebugFormat("CreateOrUpdateMappingClusterHandOverPeriode() - End insert MP_ClusterHandOverPeriode");
                }
                // Handle data errors.
                catch (DataException exDb)
                {
                    Logger.ErrorFormat("CreateOrUpdateMappingClusterHandOverPeriode() ERROR DbException. Result = {0}", exDb.Message);
                    throw new UserFriendlyException("Database Error : {0}", exDb.Message);
                }
                // Handle all other exceptions.
                catch (Exception ex)
                {
                    Logger.ErrorFormat("CreateOrUpdateMappingClusterHandOverPeriode() ERROR Exception. Result = {0}", ex.Message);
                    throw new UserFriendlyException("Error : {0}", ex.Message);
                }
            }
            Logger.Info("CreateOrUpdateMappingClusterHandOverPeriode() Finished.");
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_MasterCluster_Delete)]
        public void DeleteMsCluster(int Id)
        {
            Logger.Info("DeleteMsCluster() Started.");

            Logger.DebugFormat("DeleteMsCluster() - Start checking Cluster Code with Id: {0}.", Id);

            var checkUnit = (from unit in _msUnitRepo.GetAll()
                              where unit.clusterID == Id
                              select unit.clusterID);

            Logger.DebugFormat("DeleteMsCluster() - End checking Cluster Code. Result: {0}.", checkUnit);

            if (!checkUnit.Any())
            {
                try
                {
                    Logger.DebugFormat("DeleteMsCluster() - Start delete Cluster. Parameters sent: {0}", Id);
                    _msClusterRepo.Delete(Id);
                    DeleteMsClusterPenalty(Id);
                    DeleteMpClusterHandOverPeriode(Id);
                    Logger.DebugFormat("DeleteMsCluster() - End delete Cluster");
                }
                // Handle data errors.
                catch (DataException exDb)
                {
                    Logger.ErrorFormat("DeleteMsCluster() ERROR DbException. Result = {0}", exDb.Message);
                    throw new UserFriendlyException("Database Error : {0}", exDb.Message);
                }
                // Handle all other exceptions.
                catch (Exception ex)
                {
                    Logger.ErrorFormat("DeleteMsCluster() ERROR Exception. Result = {0}", ex.Message);
                    throw new UserFriendlyException("Error : {0}", ex.Message);
                }
            }
            else
            {
                Logger.ErrorFormat("DeleteMsCluster() ERROR Exception. Result = {0}", "This Cluster is used by another master!");
                throw new UserFriendlyException("This Cluster is used by another master!");
            }
            Logger.Info("DeleteMsCluster() Finished.");
        }

        private void DeleteMpClusterHandOverPeriode(int clusterID)
        {
            var getMappingClusterHandOverPeriode = (from x in _mpClusterHandOverPeriodeRepo.GetAll()
                                                    where x.clusterID == clusterID
                                                    select x.Id);

            if (getMappingClusterHandOverPeriode.Any())
            {
                foreach (var mappingClusterID in getMappingClusterHandOverPeriode.ToList())
                {
                    try
                    {
                        Logger.DebugFormat("DeleteMpClusterHandOverPeriode() - Start delete Cluster Hand Over Periode. Parameters sent: {0}", mappingClusterID);
                        _mpClusterHandOverPeriodeRepo.Delete(mappingClusterID);
                        Logger.DebugFormat("DeleteMpClusterHandOverPeriode() - End delete Cluster Hand Over Periode");
                    }
                    // Handle data errors.
                    catch (DataException exDb)
                    {
                        Logger.ErrorFormat("DeleteMpClusterHandOverPeriode() ERROR DbException. Result = {0}", exDb.Message);
                        throw new UserFriendlyException("Database Error : {0}", exDb.Message);
                    }
                    // Handle all other exceptions.
                    catch (Exception ex)
                    {
                        Logger.ErrorFormat("DeleteMpClusterHandOverPeriode() ERROR Exception. Result = {0}", ex.Message);
                        throw new UserFriendlyException("Error : {0}", ex.Message);
                    }
                }
            }
        }

        private void DeleteMsClusterPenalty(int clusterID)
        {
            var getClusterPenalty = (from x in _msClusterPenaltyRepo.GetAll()
                                     where x.clusterID == clusterID
                                     select x.Id);

            if (getClusterPenalty.Any())
            {
                try
                {
                    Logger.DebugFormat("DeleteMsClusterPenalty() - Start delete Cluster Penalty. Parameters sent: {0}", getClusterPenalty);
                    _msClusterPenaltyRepo.Delete(getClusterPenalty.FirstOrDefault());
                    Logger.DebugFormat("DeleteMsClusterPenalty() - End delete Cluster Penalty");
                }
                // Handle data errors.
                catch (DataException exDb)
                {
                    Logger.ErrorFormat("DeleteMsClusterPenalty() ERROR DbException. Result = {0}", exDb.Message);
                    throw new UserFriendlyException("Database Error : {0}", exDb.Message);
                }
                // Handle all other exceptions.
                catch (Exception ex)
                {
                    Logger.ErrorFormat("DeleteMsClusterPenalty() ERROR Exception. Result = {0}", ex.Message);
                    throw new UserFriendlyException("Error : {0}", ex.Message);
                }
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_MasterCluster)]
        public ListResultDto<GetAllMsClusterListDto> GetAllMsCluster(int projectID)
        {
            var result = (from x in _msClusterRepo.GetAll()
                          where x.projectID == projectID
                          select new GetAllMsClusterListDto
                          {
                              Id = x.Id,
                              clusterCode = x.clusterCode,
                              clusterName = x.clusterName,
                          }).ToList();

            return new ListResultDto<GetAllMsClusterListDto>(result);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_MappingItem, AppPermissions.Pages_Tenant_GeneratePrice, AppPermissions.Pages_Tenant_MasterFacade, AppPermissions.Pages_Tenant_CreditManagement_CreditAgreement)]
        public ListResultDto<GetClusterDropdownListDto> GetMsClusterByProjectDropdown(int projectID)
        {
            var getData = (from A in _msUnitRepo.GetAll()
                           join B in _msClusterRepo.GetAll() on A.clusterID equals B.Id
                           where A.projectID == projectID
                           orderby B.Id descending
                           select new GetClusterDropdownListDto
                           {
                               clusterID = B.Id,
                               clusterCode = B.clusterCode,
                               clusterName = B.clusterName
                           }).Distinct().ToList();

            return new ListResultDto<GetClusterDropdownListDto>(getData);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_GenerateUnit)]
        public ListResultDto<GetClusterDropdownListDto> GetMsClusterDropdownPerProject(int projectID)
        {
            var getData = (from A in _msClusterRepo.GetAll()
                           where A.projectID == projectID
                           orderby A.Id descending
                           select new GetClusterDropdownListDto
                           {
                               clusterID = A.Id,
                               clusterCode = A.clusterCode,
                               clusterName = A.clusterName
                           }).ToList();

            return new ListResultDto<GetClusterDropdownListDto>(getData);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_MasterCluster_Edit)]
        public JObject UpdateMsCluster(CreateOrUpdateMsClusterInputDto input)
        {
            JObject obj = new JObject();

            Logger.DebugFormat("UpdateMsCluster() - Start checking existing cluster. Parameters sent:{0}" +
                "clusterCode     = {1}{0}" +
                "clusterName     = {2}"
                , Environment.NewLine, input.clusterCode, input.clusterName);

            var checkCode = (from x in _msClusterRepo.GetAll()
                             where x.Id != input.Id && x.clusterCode == input.clusterCode
                             select x).Any();

            Logger.DebugFormat("UpdateMsCluster() - End checking existing cluster. Result: {0}", checkCode);

            var getMsCluster = (from x in _msClusterRepo.GetAll()
                                 where x.Id == input.Id
                                 select x).FirstOrDefault();
            if (!checkCode)
            {
                var updateMsCluster = getMsCluster.MapTo<MS_Cluster>();

                var checkUnit = (from unit in _msUnitRepo.GetAll()
                                 where unit.clusterID == input.Id
                                 select unit).Any();

                updateMsCluster.website = input.website;

                if (input.termConditionFileStatus == "updated")
                {
                    var fileToDelete = getMsCluster.termConditionFile;
                    DeleteFile(fileToDelete);

                    var fileUrl = MoveFile(input.termConditionFile);
                    GetURLWithoutHost(fileUrl, out fileUrl);
                    updateMsCluster.termConditionFile = fileUrl;
                }
                else if (input.termConditionFileStatus == "removed")
                {
                    updateMsCluster.termConditionFile = "-";
                    DeleteFile(getMsCluster.termConditionFile);
                }
                else
                {
                    updateMsCluster.termConditionFile = getMsCluster.termConditionFile;
                }

                if (!checkUnit)
                {
                    updateMsCluster.clusterCode = input.clusterCode;
                    updateMsCluster.clusterName = input.clusterName;
                    updateMsCluster.projectID = input.projectID;
                    obj.Add("message", "Edit Successfully");
                }
                else
                {
                    obj.Add("message", "Edit Successfully, but can't change cluster Code, Name & Floor");
                }

                Logger.DebugFormat("UpdateMsCluster() - Start update MS_Cluster. Parameters sent:{0}" +
                " entityID	        = {1}{0}" +
                " clusterCode	    = {2}{0}" +
                " clusterName	    = {3}{0}" +
                " projectID	        = {4}{0}" +
                " sortNo      	    = 0{0}" +
                " penaltyRate 	    = 0{0}" +
                " startPenaltyDay   = 0{0}" +
                " termConditionFile = {5}{0}" +
                " website           = {6}{0}"
                , Environment.NewLine, 1, input.clusterCode, input.clusterName, input.projectID, updateMsCluster.termConditionFile, input.website);

                try
                {
                    _msClusterRepo.Update(updateMsCluster);
                    CreateOrUpdateMappingClusterHandOverPeriode(input.CreateOrUpdateHandOverPeriodeDto , getMsCluster.Id);
                    Logger.DebugFormat("UpdateMsCluster() - End update MS_Cluster");
                }
                catch (DataException exDb)
                {
                    Logger.ErrorFormat("UpdateMsCluster() ERROR DbException. Result = {0}", exDb.Message);
                    throw new UserFriendlyException("Database Error : {0}", exDb.Message);
                }
                // Handle all other exceptions.
                catch (Exception ex)
                {
                    Logger.ErrorFormat("UpdateMsCluster() ERROR Exception. Result = {0}", ex.Message);
                    throw new UserFriendlyException("Error : {0}", ex.Message);
                }
            }
            else
            {
                Logger.ErrorFormat("UpdateMsCluster() Cluster Code already exist. Result = {0}", checkCode);
                throw new UserFriendlyException("Cluster Code already exist!");
            }
            return obj;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_MasterCluster_Edit)]
        public CreateOrUpdateMsClusterInputDto GetMsClusterDetail(int clusterID)
        {
            var result = (from x in _msClusterRepo.GetAll()
                          join y in _msProjectRepo.GetAll() on x.projectID equals y.Id
                          where x.Id == clusterID
                          select new CreateOrUpdateMsClusterInputDto
                          {
                              Id = x.Id,
                              clusterCode = x.clusterCode,
                              clusterName = x.clusterName,
                              projectID = x.projectID,
                              projectName = y.projectName,
                              termConditionFile = (x != null && x.termConditionFile != null) ? (!x.termConditionFile.Equals("-")) ? getAbsoluteUriWithoutTail() + GetURLWithoutHost(x.termConditionFile) : null : null, //TODO link + ip host
                              website = x.website,
                              CreateOrUpdateHandOverPeriodeDto = (from y in _mpClusterHandOverPeriodeRepo.GetAll()
                                                                  where y.clusterID == clusterID
                                                                  select new CreateOrUpdateHandOverPeriodeDto
                                                                  {
                                                                      handOverStart = y.handOverStart,
                                                                      handOverEnd = y.handOverEnd,
                                                                      handOverDue = y.handOverDue,
                                                                      graceDue = y.graceDue
                                                                  }).ToList()
                          }).FirstOrDefault();

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_MasterCluster)]
        public ListResultDto<GetAvailableMsProjectDropdownForClusterDto> GetAvailableMsProjectDropdownForCluster()
        {
            var getProjectID = (from x in _msClusterRepo.GetAll()
                                select x.projectID).ToList();

            var result = (from x in _msProjectRepo.GetAll()
                          where !getProjectID.Contains(x.Id)
                          select new GetAvailableMsProjectDropdownForClusterDto
                          {
                              projectID = x.Id,
                              projectCode = x.projectCode,
                              projectName = x.projectName
                          }).ToList();

            return new ListResultDto<GetAvailableMsProjectDropdownForClusterDto>(result);
        }

        private static Uri getAbsolutUri()
        {
            var request = _HttpContextAccessor.HttpContext.Request;
            UriBuilder uriBuilder = new UriBuilder();
            uriBuilder.Scheme = request.Scheme;
            uriBuilder.Host = request.Host.ToString();
            uriBuilder.Path = request.Path.ToString();
            uriBuilder.Query = request.QueryString.ToString();
            return uriBuilder.Uri;
        }

        private string MoveFile(string filename)
        {
            try
            {
                return _filesHelper.MoveFiles(filename, @"Temp\Downloads\ClusterFiles\", @"Assets\Upload\ClusterFiles\");
            }
            catch (Exception ex)
            {
                Logger.DebugFormat("test() - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Error : {0}", ex.Message);
            }
        }

        private void DeleteFile(string filename)
        {
            var imageToDelete = filename;
            var filenameToDelete = imageToDelete.Split(@"/");
            var nameFileToDelete = filenameToDelete[filenameToDelete.Count() - 1];
            var deletePath = @"\Assets\Upload\ClusterFiles\";

            var deleteImage = _hostingEnvironment.WebRootPath + deletePath + nameFileToDelete;

            if (File.Exists(deleteImage))
            {
                var file = new FileInfo(deleteImage);
                file.Delete();
            }
        }

        private void GetURLWithoutHost(string path, out string finalpath)
        {
            finalpath = path;
            try
            {
                Regex RegexObj = new Regex("[\\w\\W]*([\\/]Assets[\\w\\W\\s]*)");
                if (RegexObj.IsMatch(path))
                {
                    finalpath = RegexObj.Match(path).Groups[1].Value;
                }
            }
            catch (ArgumentException ex)
            {
                Logger.DebugFormat("test() - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Error : {0}", ex.Message);
            }
        }

        private string GetURLWithoutHost(string path)
        {
            string finalpath = path;
            try
            {
                Regex RegexObj = new Regex("[\\w\\W]*([\\/]Assets[\\w\\W\\s]*)");
                if (RegexObj.IsMatch(path))
                {
                    finalpath = RegexObj.Match(path).Groups[1].Value;
                }
            }
            catch (ArgumentException ex)
            {
                Logger.DebugFormat("test() - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Error : {0}", ex.Message);
            }
            return finalpath;
        }

        private string getAbsoluteUriWithoutTail()
        {
            var request = _HttpContextAccessor.HttpContext.Request;
            UriBuilder uriBuilder = new UriBuilder();
            uriBuilder.Scheme = request.Scheme;
            uriBuilder.Host = request.Host.ToString();
            var test = uriBuilder.ToString();
            var result = test.Replace("[", "").Replace("]", "");
            int position = result.LastIndexOf('/');
            if (position > -1)
                result = result.Substring(0, result.Length - 1);

            if (request.PathBase != null)
            {
                if (!string.IsNullOrWhiteSpace(request.PathBase.Value))
                {
                    result += request.PathBase.Value;
                }
            }
            return result;
        }
    }
}
