﻿using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Extensions;
using Abp.UI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using VDI.Demo.Authorization;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.Files;
using VDI.Demo.MasterPlan.Project.Setting_Penalties.Dto;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;

namespace VDI.Demo.MasterPlan.Project.Setting_Penalties
{
    public class SettingPenaltyAppService : DemoAppServiceBase, ISettingPenaltyAppService
    {
        private readonly PropertySystemDbContext _contextPropertySystem;
        private readonly PersonalsNewDbContext _contextPersonals;
        private readonly DemoDbContext _contextEngine3;
        private static IHttpContextAccessor _httpContextAccessor;
        private readonly FilesHelper _filesHelper;
        private readonly IHostingEnvironment _hostingEnvironment;

        public SettingPenaltyAppService(
            PropertySystemDbContext contextPropertySystem,
            PersonalsNewDbContext contextPersonals,
            DemoDbContext contextEngine3,
            IHttpContextAccessor httpContextAccessor,
            FilesHelper filesHelper,
            IHostingEnvironment environment
            )
        {
            _contextPropertySystem = contextPropertySystem;
            _contextPersonals = contextPersonals;
            _contextEngine3 = contextEngine3;
            _httpContextAccessor = httpContextAccessor;
            _filesHelper = filesHelper;
            _hostingEnvironment = environment;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_SettingPenalty_Create, AppPermissions.Pages_Tenant_SettingPenalty_Edit)]
        public void CreateOrUpdateMSClusterPenalty(GetDataMSClusterPenaltyByIDListDto input)
        {
            Logger.Info("CreateOrUpdateMSClusterPenalty() - Started.");
            
            if (input.clusterPenaltyID == 0)
            {

                Logger.DebugFormat("CreateOrUpdateMSClusterPenalty() - Start checking before insert or update Cluster Penalty. Parameters sent:{0}" +
                           "bookingPeriodStart = {1}{0}" +
                           "bookingPeriodEnd = {2}{0}"
                           , Environment.NewLine, input.bookingPeriodStart, input.bookingPeriodEnd);

                var checkBookingPeriodCreate = (from a in _contextPropertySystem.MS_ClusterPenalty
                                                where a.clusterID == input.clusterID && ((input.bookingPeriodStart >= a.bookingPeriodStart && input.bookingPeriodStart <= a.bookingPeriodEnd) || (input.bookingPeriodEnd >= a.bookingPeriodStart && input.bookingPeriodEnd <= a.bookingPeriodEnd))
                                                select a).Any();

                Logger.DebugFormat("CreateOrUpdateMSClusterPenalty() - Ended checking before insert or update Cluster Penalty. Result = {0}", checkBookingPeriodCreate);
                if (!checkBookingPeriodCreate)
                {
                    string fileUrl;
                    if (input.templateName.IsNullOrWhiteSpace())
                    {
                        fileUrl = "-";
                    }
                    else
                    {
                        fileUrl = MoveFile(input.templateName);
                        GetURLWithoutHost(fileUrl, out fileUrl);
                    }

                    //create
                    var dataCreate = new MS_ClusterPenalty
                    {
                        entityID = input.entityID,
                        clusterID = input.clusterID,
                        penaltyBaseRate = Convert.ToInt32(input.penaltyBaseRate),
                        penaltyFreq = input.penaltyFreq,
                        penaltyRate = input.penaltyRate,
                        bookingPeriodStart = input.bookingPeriodStart,
                        bookingPeriodEnd = input.bookingPeriodEnd,
                        isAutoWave = input.isAutoWaive,
                        autoWavePeriod = input.autoWaivePeriod,
                        penaltyDocUrl = fileUrl,
                        penaltyDocName = "m-" + input.templateName
                    };

                    try
                    {
                        Logger.DebugFormat("CreateOrUpdateMSClusterPenalty() - Start insert Cluster Penalty. Parameters sent:{0}" +
                            "entityID = {1}{0}" +
                            "clusterID = {2}{0}" +
                            "penaltyBaseRate = {3}{0}" +
                            "penaltyRate = {4}{0}" +
                            "bookingPeriodeStart = {5}{0}" +
                            "bookingPeriodeEnd = {6}{0}" +
                            "isAutoWave = {7}{0}" +
                            "autoWavePeriode = {8}{0}" +
                            "penaltyFreq = {9}{0}"
                            , Environment.NewLine, input.entityID, input.clusterID, input.penaltyBaseRate, input.penaltyRate
                            , input.bookingPeriodStart, input.bookingPeriodEnd, input.isAutoWaive, input.autoWaivePeriod, input.penaltyFreq);

                        _contextPropertySystem.MS_ClusterPenalty.Add(dataCreate);

                        Logger.DebugFormat("CreateOrUpdateMSClusterPenalty() - Ended insert Account.");
                    }
                    catch (DataException ex)
                    {
                        Logger.ErrorFormat("CreateOrUpdateMSClusterPenalty() - ERROR DataException. Result = {0}", ex.Message);
                        throw new UserFriendlyException("Db Error: " + ex.Message);
                    }
                    catch (Exception ex)
                    {
                        Logger.ErrorFormat("CreateOrUpdateMSClusterPenalty() - ERROR Exception. Result = {0}", ex.Message);
                        throw new UserFriendlyException("Error: " + ex.Message);
                    }
                }
                else
                {
                    Logger.ErrorFormat("CreateOrUpdateMSClusterPenalty() - ERROR Result = {0}.", "Booking Period Start or Booking Period End is between the other period setting penalty!");
                    throw new UserFriendlyException("Booking Period Start or Booking Period End is between the other period setting penalty!");
                }
            }
            else
            {
                Logger.DebugFormat("CreateOrUpdateMSClusterPenalty() - Start checking before insert or update Cluster Penalty. Parameters sent:{0}" +
                           "bookingPeriodStart = {1}{0}" +
                           "bookingPeriodEnd = {2}{0}"
                           , Environment.NewLine, input.bookingPeriodStart, input.bookingPeriodEnd);

                var checkBookingPeriodCreate = (from a in _contextPropertySystem.MS_ClusterPenalty
                                                where a.clusterID == input.clusterID && a.Id != input.clusterPenaltyID && (input.bookingPeriodEnd >= a.bookingPeriodStart && input.bookingPeriodEnd <= a.bookingPeriodEnd)
                                                select a).Any();

                Logger.DebugFormat("CreateOrUpdateMSClusterPenalty() - Ended checking before insert or update Cluster Penalty. Result = {0}", checkBookingPeriodCreate);
                if (!checkBookingPeriodCreate)
                {
                    //edit
                    Logger.DebugFormat("CreateOrUpdateMSClusterPenalty() - Start get data before update Cluster Penalty. Parameters sent:{0}" +
                   "bookingPeriodStart = {1}{0}"
                   , Environment.NewLine, input.bookingPeriodStart, input.bookingPeriodEnd);

                    var getDataClusterPenalty = (from a in _contextPropertySystem.MS_ClusterPenalty
                                                 where a.Id == input.clusterPenaltyID
                                                 select a).FirstOrDefault();

                    Logger.DebugFormat("CreateOrUpdateMSClusterPenalty() - Ended get data before update Cluster Penalty. Result = {0}", getDataClusterPenalty);

                    var updateMsClusterPenalty = getDataClusterPenalty.MapTo<MS_ClusterPenalty>();

                    updateMsClusterPenalty.penaltyBaseRate = Convert.ToInt32(input.penaltyBaseRate);
                    updateMsClusterPenalty.penaltyFreq = input.penaltyFreq;
                    updateMsClusterPenalty.penaltyRate = input.penaltyRate;
                    updateMsClusterPenalty.bookingPeriodEnd = input.bookingPeriodEnd;
                    updateMsClusterPenalty.isAutoWave = input.isAutoWaive;
                    updateMsClusterPenalty.autoWavePeriod = input.autoWaivePeriod;

                    if (input.templateStatus == "updated")
                    {
                        var templateToDelete = updateMsClusterPenalty.penaltyDocUrl;
                        DeleteFile(templateToDelete);

                        var fileUrl = MoveFile(input.templateName);
                        GetURLWithoutHost(fileUrl, out fileUrl);
                        updateMsClusterPenalty.penaltyDocUrl = fileUrl;
                        updateMsClusterPenalty.penaltyDocName = "m-" + input.templateName;
                    }
                    else if (input.templateStatus == "removed")
                    {
                        updateMsClusterPenalty.penaltyDocUrl = "-";
                        updateMsClusterPenalty.penaltyDocName = "-";
                        DeleteFile(getDataClusterPenalty.penaltyDocUrl);
                    }
                    else
                    {
                        updateMsClusterPenalty.penaltyDocUrl = getDataClusterPenalty.penaltyDocUrl;
                        updateMsClusterPenalty.penaltyDocName = getDataClusterPenalty.penaltyDocName;
                    }

                    try
                    {
                        Logger.DebugFormat("CreateOrUpdateMSClusterPenalty() - Start insert Cluster Penalty. Parameters sent:{0}" +
                            "penaltyBaseRate = {1}{0}" +
                            "penaltyRate = {2}{0}" +
                            "bookingPeriodeEnd = {3}{0}" +
                            "isAutoWave = {4}{0}" +
                            "autoWavePeriode = {5}{0}" +
                            "penaltyFreq = {6}{0}"
                            , Environment.NewLine, input.penaltyBaseRate, input.penaltyRate
                            , input.bookingPeriodEnd, input.isAutoWaive, input.autoWaivePeriod, input.penaltyFreq);

                        _contextPropertySystem.MS_ClusterPenalty.Update(updateMsClusterPenalty);

                        Logger.DebugFormat("CreateOrUpdateMSClusterPenalty() - Ended insert Account.");
                    }
                    catch (DataException ex)
                    {
                        Logger.ErrorFormat("CreateOrUpdateMSClusterPenalty() - ERROR DataException. Result = {0}", ex.Message);
                        throw new UserFriendlyException("Db Error: " + ex.Message);
                    }
                    catch (Exception ex)
                    {
                        Logger.ErrorFormat("CreateOrUpdateMSClusterPenalty() - ERROR Exception. Result = {0}", ex.Message);
                        throw new UserFriendlyException("Error: " + ex.Message);
                    }
                }
                else
                {
                    Logger.ErrorFormat("CreateOrUpdateMSClusterPenalty() - ERROR Result = {0}.", "Booking Period Start or Booking Period End is between the other period setting penalty!");
                    throw new UserFriendlyException("Booking Period Start or Booking Period End is between the other period setting penalty!");
                }
            }

            _contextPropertySystem.SaveChanges();
            Logger.Info("CreateOrUpdateMSClusterPenalty() - Finished.");
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_SettingPenalty)]
        public List<GetDataMSClusterPenaltyListDto> GetDataMSClusterPenalty(int clusterID)
        {
            List<GetDataMSClusterPenaltyListDto> listResult = new List<GetDataMSClusterPenaltyListDto>();
            var result = new GetDataMSClusterPenaltyListDto();
            var getDataClusterPenalty = (from a in _contextPropertySystem.MS_ClusterPenalty
                                        join b in _contextPropertySystem.MS_Cluster on a.clusterID equals b.Id
                                        join c in _contextPropertySystem.MS_Project on b.projectID equals c.Id
                                        where a.clusterID == clusterID
                                        select new 
                                        {
                                            clusterPenaltyID = a.Id,
                                            bookingPeriodStart = a.bookingPeriodStart,
                                            bookingPeriodEnd = a.bookingPeriodEnd,
                                            penaltyRate = a.penaltyRate,
                                            projectID = c.Id,
                                            project = c.projectName,
                                            clusterID = b.Id,
                                            cluster = b.clusterName,
                                            a.penaltyBaseRate,
                                            a.penaltyFreq,
                                            a.penaltyDocUrl,
                                            a.penaltyDocName
                                        }).ToList();

            foreach(var item in getDataClusterPenalty)
            {
                if(item.penaltyBaseRate == 100)
                {
                    var getParamPercent = (from a in _contextPropertySystem.MS_Parameter
                                           where a.code == item.penaltyBaseRate.ToString()
                                           select a.value).FirstOrDefault();
                    result = new GetDataMSClusterPenaltyListDto
                    {
                        clusterPenaltyID = item.clusterPenaltyID,
                        projectID = item.projectID,
                        project = item.project,
                        clusterID = item.clusterID,
                        cluster = item.cluster,
                        bookingPeriodStart = item.bookingPeriodStart,
                        bookingPeriodEnd = item.bookingPeriodEnd,
                        penaltyRate = item.penaltyRate + " " + getParamPercent + " (" + item.penaltyFreq + ")",
                        penaltyDocUrl = item.penaltyDocUrl,
                        penaltyDocName = item.penaltyDocName
                    };
                }
                else
                {
                    var getParamPermill = (from a in _contextPropertySystem.MS_Parameter
                                           where a.code == item.penaltyBaseRate.ToString()
                                           select a.value).FirstOrDefault();
                    result = new GetDataMSClusterPenaltyListDto
                    {
                        clusterPenaltyID = item.clusterPenaltyID,
                        projectID = item.projectID,
                        project = item.project,
                        clusterID = item.clusterID,
                        cluster = item.cluster,
                        bookingPeriodStart = item.bookingPeriodStart,
                        bookingPeriodEnd = item.bookingPeriodEnd,
                        penaltyRate = item.penaltyRate + " " + getParamPermill + " (" + item.penaltyFreq + ")",
                        penaltyDocUrl = item.penaltyDocUrl,
                        penaltyDocName = item.penaltyDocName
                    };
                }
                listResult.Add(result);
            }

            return listResult;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_SettingPenalty)]
        public GetDataMSClusterPenaltyByIDListDto GetDataMSClusterPenaltyByID(int clusterPenaltyID)
        {
            var result = new GetDataMSClusterPenaltyByIDListDto();
            var getDataClusterPenalty = (from a in _contextPropertySystem.MS_ClusterPenalty
                                         join b in _contextPropertySystem.MS_Cluster on a.clusterID equals b.Id
                                         join c in _contextPropertySystem.MS_Project on b.projectID equals c.Id
                                         where a.Id == clusterPenaltyID
                                         select new
                                         {
                                             clusterPenaltyID = a.Id,
                                             bookingPeriodStart = a.bookingPeriodStart,
                                             bookingPeriodEnd = a.bookingPeriodEnd,
                                             penaltyRate = a.penaltyRate,
                                             projectID = c.Id,
                                             project = c.projectName,
                                             clusterID = b.Id,
                                             cluster = b.clusterName,
                                             a.penaltyBaseRate,
                                             a.isAutoWave,
                                             a.autoWavePeriod,
                                             a.penaltyFreq,
                                             a.penaltyDocUrl,
                                             a.penaltyDocName
                                         }).FirstOrDefault();

            var getParamPercent = (from a in _contextPropertySystem.MS_Parameter
                                   where a.code == getDataClusterPenalty.penaltyBaseRate.ToString()
                                   select a.value).FirstOrDefault();
            result = new GetDataMSClusterPenaltyByIDListDto
            {
                clusterPenaltyID = getDataClusterPenalty.clusterPenaltyID,
                projectID = getDataClusterPenalty.projectID,
                project = getDataClusterPenalty.project,
                clusterID = getDataClusterPenalty.clusterID,
                cluster = getDataClusterPenalty.cluster,
                bookingPeriodStart = getDataClusterPenalty.bookingPeriodStart,
                bookingPeriodEnd = getDataClusterPenalty.bookingPeriodEnd,
                penaltyRate = getDataClusterPenalty.penaltyRate,
                penaltyBaseRate = getParamPercent,
                penaltyFreq = getDataClusterPenalty.penaltyFreq,
                autoWaivePeriod = getDataClusterPenalty.autoWavePeriod,
                isAutoWaive = getDataClusterPenalty.isAutoWave,
                penaltyDocUrl = (getDataClusterPenalty != null && getDataClusterPenalty.penaltyDocUrl != null) ? (!getDataClusterPenalty.penaltyDocUrl.Equals("-")) ? getAbsoluteUriWithoutTail() + GetURLWithoutHost(getDataClusterPenalty.penaltyDocUrl) : null : null,
                penaltyDocName = getDataClusterPenalty.penaltyDocName
            };
            return result;
        }

        #region helper function
        private string MoveFile(string filename)
        {
            try
            {
                return _filesHelper.MoveFiles(filename, @"Temp\Downloads\Penalty\", @"Assets\HtmlTemplates\Penalty\");
            }
            catch (Exception ex)
            {
                Logger.DebugFormat("test() - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Error : {0}", ex.Message);
            }
        }

        private void DeleteFile(string filename)
        {
            var imageToDelete = filename;
            var filenameToDelete = imageToDelete.Split(@"/");
            var nameFileToDelete = filenameToDelete[filenameToDelete.Count() - 1];
            var deletePath = @"Assets\HtmlTemplates\SP\PenaltySP\";

            var deleteImage = _hostingEnvironment.WebRootPath + deletePath + nameFileToDelete;

            if (File.Exists(deleteImage))
            {
                var file = new FileInfo(deleteImage);
                file.Delete();
            }
        }

        private void GetURLWithoutHost(string path, out string finalpath)
        {
            finalpath = path;
            try
            {
                Regex RegexObj = new Regex("[\\w\\W]*([\\/]Assets[\\w\\W\\s]*)");
                if (RegexObj.IsMatch(path))
                {
                    finalpath = RegexObj.Match(path).Groups[1].Value;
                }
            }
            catch (ArgumentException ex)
            {
                Logger.DebugFormat("test() - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Error : {0}", ex.Message);
            }
        }

        private string GetURLWithoutHost(string path)
        {
            string finalpath = path;
            try
            {
                Regex RegexObj = new Regex("[\\w\\W]*([\\/]Assets[\\w\\W\\s]*)");
                if (RegexObj.IsMatch(path))
                {
                    finalpath = RegexObj.Match(path).Groups[1].Value;
                }
            }
            catch (ArgumentException ex)
            {
                Logger.DebugFormat("test() - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Error : {0}", ex.Message);
            }
            return finalpath;
        }

        private string getAbsoluteUriWithoutTail()
        {
            var request = _httpContextAccessor.HttpContext.Request;
            UriBuilder uriBuilder = new UriBuilder();
            uriBuilder.Scheme = request.Scheme;
            uriBuilder.Host = request.Host.ToString();
            var test = uriBuilder.ToString();
            var result = test.Replace("[", "").Replace("]", "");
            int position = result.LastIndexOf('/');
            if (position > -1)
                result = result.Substring(0, result.Length - 1);

            if (request.PathBase != null)
            {
                if (!string.IsNullOrWhiteSpace(request.PathBase.Value))
                {
                    result += request.PathBase.Value;
                }
            }
            return result;
        }
        #endregion

    }
}
