﻿using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.UI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using VDI.Demo.Authorization;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.Files;
using VDI.Demo.MasterPlan.Project.Setting_PeriodSPs.Dto;
using VDI.Demo.PropertySystemDB.LippoMaster;

namespace VDI.Demo.MasterPlan.Project.Setting_PeriodSPs
{
    public class SettingPeriodSPAppService : DemoAppServiceBase, ISettingPeriodSPAppService
    {
        private readonly PropertySystemDbContext _contextPropertySystem;
        private readonly AccountingDbContext _contextAccounting;
        private readonly TAXDbContext _contextTAX;
        private readonly PersonalsNewDbContext _contextPersonals;
        private readonly DemoDbContext _contextEngine3;
        private readonly IRepository<MS_SPPeriod> _msSPPeriodRepo;
        private static IHttpContextAccessor _HttpContextAccessor;
        private readonly FilesHelper _filesHelper;
        private readonly IHostingEnvironment _hostingEnvironment;

        public SettingPeriodSPAppService(
            PropertySystemDbContext contextPropertySystem,
            AccountingDbContext contextAccounting,
            TAXDbContext contextTAX,
            PersonalsNewDbContext contextPersonals,
            DemoDbContext contextEngine3,
            IRepository<MS_SPPeriod> msSPPeriodRepo,
            IHttpContextAccessor httpContextAccessor,
            FilesHelper filesHelper,
            IHostingEnvironment environment
            )
        {
            _contextPropertySystem = contextPropertySystem;
            _contextAccounting = contextAccounting;
            _contextTAX = contextTAX;
            _contextPersonals = contextPersonals;
            _contextEngine3 = contextEngine3;
            _msSPPeriodRepo = msSPPeriodRepo;
            _HttpContextAccessor = httpContextAccessor;
            _filesHelper = filesHelper;
            _hostingEnvironment = environment;
        }

        //[AbpAuthorize(AppPermissions.Pages_Tenant_SettingPeriodSP_Create)]
        public void CreateMsSPPeriod(CreateOrUpdateMsSPPeriodInputDto input)
        {
            Logger.Info("CreateMsSPPeriod() Started.");

            string fileUrl;
            if (input.templateName.IsNullOrWhiteSpace())
            {
                fileUrl = "-";
            }
            else
            {
                fileUrl = MoveFile(input.templateName);
                GetURLWithoutHost(fileUrl, out fileUrl);
            }

            var getDataCluster = (from a in _contextPropertySystem.MS_Cluster
                                  join b in _contextPropertySystem.MS_Project on a.projectID equals b.Id
                                  where b.isPublish == true
                                  select new
                                  {
                                      clusterID = a.Id,
                                      projectID = b.Id
                                  })
                                  .Distinct()
                                  .ToList();

            foreach(var item in getDataCluster)
            {
                var checkData = (from a in _contextPropertySystem.MS_SPPeriod
                                 where a.clusterID == item.clusterID && a.letterTypeID == input.letterTypeID
                                 select a).Any();
                if (checkData) continue;

                var getDataAccount = (from a in _contextPropertySystem.MS_Account
                                      where a.projectID == item.projectID
                                      orderby a.Id descending
                                      select a).FirstOrDefault(); //masih ngambil yang terakhir diinsert bukan default

                if (getDataAccount == null) continue;

                var data = new MS_SPPeriod
                {
                    entityID = 1,
                    clusterID = item.clusterID,//get
                    daysDue = input.daysDue,
                    letterTypeID = input.letterTypeID,
                    kuasaDir1 = input.kuasaDir1,
                    kuasaDir2 = input.kuasaDir2,
                    SPGenerateID = input.SPGenerateID,
                    templateName = "m-" + input.templateName,
                    templateUrl = fileUrl,
                    isDefault = true,
                    accountID = getDataAccount.Id //get
                };


                try
                {
                    Logger.DebugFormat("CreateMsSPPeriod() - Start insert SP Period. Parameters sent:{0}" +
                        "	entityID	    = {1}{0}" +
                        "	clusterID	    = {2}{0}" +
                        "	daysDue	        = {3}{0}" +
                        "	letterTypeID	= {4}{0}" +
                        "	SPGenerateID    = {5}{0}" +
                        "	templateName 	= {6}{0}" +
                        "   templateUrl     = {7}{0}" +
                        "   kuasaDir1       = {8}{0}" +
                        "   kuasaDir2       = {9}{0}" +
                        "   accountID       = {10}{0}"
                        , Environment.NewLine, 1, item.clusterID, input.daysDue, input.letterTypeID, input.SPGenerateID, input.templateName, fileUrl, input.kuasaDir1, input.kuasaDir2, getDataAccount.Id);

                    _contextPropertySystem.MS_SPPeriod.Add(data);
                }
                catch (DataException exDb)
                {
                    Logger.ErrorFormat("CreateMsSPPeriod() ERROR DbException. Result = {0}", exDb.Message);
                    throw new UserFriendlyException("Database Error : {0}", exDb.Message);
                }
                // Handle all other exceptions.
                catch (Exception ex)
                {
                    Logger.ErrorFormat("CreateMsSPPeriod() ERROR Exception. Result = {0}", ex.Message);
                    throw new UserFriendlyException("Error : {0}", ex.Message);
                }
            }
            _contextPropertySystem.SaveChanges();

            Logger.Info("CreateMsSPPeriod() Finished.");
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_SettingPeriodSP_Delete)]
        public void DeleteMsSPPeriod(int letterTypeID)
        {
            Logger.Info("DeleteMsSPPeriod() started.");
            var checkSettingDefault = (from a in _contextPropertySystem.MS_SPPeriod
                                        where a.letterTypeID == letterTypeID && a.isDefault == true
                                        select a);

            if (checkSettingDefault.Any())
            {
                try
                {
                    Logger.DebugFormat("DeleteMsSPPeriod() - Start delete SP Period. Parameters sent: {0}", checkSettingDefault);
                    _contextPropertySystem.RemoveRange(checkSettingDefault.ToList());
                    _contextPropertySystem.SaveChanges();
                    Logger.DebugFormat("DeleteMsSPPeriod() - End delete SP Period");
                }
                // Handle data errors.
                catch (DataException exDb)
                {
                    Logger.ErrorFormat("DeleteMsSPPeriod() ERROR DbException. Result = {0}", exDb.Message);
                    throw new UserFriendlyException("Database Error : {0}", exDb.Message);
                }
                // Handle all other exceptions.
                catch (Exception ex)
                {
                    Logger.ErrorFormat("DeleteMsSPPeriod() ERROR Exception. Result = {0}", ex.Message);
                    throw new UserFriendlyException("Error : {0}", ex.Message);
                }
            }
            else
            {
                Logger.ErrorFormat("DeleteMsSPPeriod() ERROR Exception. Result = {0}", "This Cluster is used by another master!");
                throw new UserFriendlyException("This Cluster has been booked!");
            }
            Logger.Info("DeleteMsSPPeriod() Finished.");
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_SettingPeriodSP)]
        public List<GetAllMsSPPeriodByClusterListDto> GetAllMsSPPeriodByCluster(int clusterID)
        {
            var result = (from SPPeriode in _contextPropertySystem.MS_SPPeriod
                          join cluster in _contextPropertySystem.MS_Cluster on SPPeriode.clusterID equals cluster.Id
                          join letterType in _contextPropertySystem.LK_LetterType on SPPeriode.letterTypeID equals letterType.Id
                          join SPGenerate in _contextPropertySystem.LK_SPGenerate on SPPeriode.SPGenerateID equals SPGenerate.Id
                          join account in _contextPropertySystem.MS_Account on SPPeriode.accountID equals account.Id
                          where SPPeriode.clusterID == clusterID
                          select new GetAllMsSPPeriodByClusterListDto
                          {
                              Id = SPPeriode.Id,
                              clusterID = SPPeriode.clusterID,
                              clusterName = cluster.clusterName,
                              letterTypeID = SPPeriode.letterTypeID,
                              letterType = letterType.letterType,
                              daysDue = SPPeriode.daysDue,
                              SPGenerateID = SPPeriode.SPGenerateID,
                              SPGenerateDesc = SPGenerate.SPGenerateDesc,
                              templateName = SPPeriode.templateName,
                              templateUrl = (SPPeriode != null && SPPeriode.templateUrl != null) ? (!SPPeriode.templateUrl.Equals("-")) ? getAbsoluteUriWithoutTail() + GetURLWithoutHost(SPPeriode.templateUrl) : null : null, //TODO link + ip host
                              kuasaDir1 = SPPeriode.kuasaDir1,
                              kuasaDir2 = SPPeriode.kuasaDir2,
                              accountID = SPPeriode.accountID,
                              accountCode = account.accCode
                          }).ToList();
            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_SettingPeriodSP)]
        public List<GetDropdownLKLetterTypeListDto> GetAvailableDropdownLKLetterType(bool flag)
        {
            var getUnavailableLetterType = (from SPPeriode in _contextPropertySystem.MS_SPPeriod
                                            where SPPeriode.isDefault == true
                                            select SPPeriode.letterTypeID).ToList();

            var getDataDropdown = (from a in _contextPropertySystem.LK_LetterType
                                   where !getUnavailableLetterType.Contains(a.Id)
                                   select new GetDropdownLKLetterTypeListDto
                                   {
                                       Id = a.Id,
                                       letterType = a.letterType,
                                       letterDesc = a.letterDesc,
                                       duration = a.duration
                                   })
                                   .ToList();
            //true = kebutuhan edit setting default SP
            if (flag == true)
            {
                getDataDropdown = (from a in _contextPropertySystem.LK_LetterType
                                       select new GetDropdownLKLetterTypeListDto
                                       {
                                           Id = a.Id,
                                           letterType = a.letterType,
                                           letterDesc = a.letterDesc,
                                           duration = a.duration
                                       })
                                   .ToList();
            }

            if (getDataDropdown == null)
            {
                throw new UserFriendlyException("All Letter Type has been set");
            }
            else
            {
                return getDataDropdown;
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_SettingPeriodSP)]
        public List<GetDropdownLKSPGenerateListDto> GetDropdownLKSPGenerate()
        {
            var getDataDropdown = (from a in _contextPropertySystem.LK_SPGenerate
                                   select new GetDropdownLKSPGenerateListDto
                                   {
                                       Id = a.Id,
                                       SPGenerateDesc = a.SPGenerateDesc
                                   }).ToList();

            return getDataDropdown;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_SettingPeriodSP)]
        public List<GetDropdownKuasaDireksiDto> GetDropdownKuasaDireksiManagerPenalty()
        {
            var PSASCode = (from x in _contextPropertySystem.MS_Parameter
                            where x.code == "PSAS"
                            select x.value).FirstOrDefault();

            var ManagerCode = (from x in _contextPropertySystem.MS_Parameter
                               where x.code == "MGR"
                               select x.value).FirstOrDefault();

            var getDataManagerPSAS = (from a in _contextPropertySystem.MS_Officer
                                      join b in _contextPropertySystem.MS_Department on a.departmentID equals b.Id
                                      join c in _contextPropertySystem.MS_Position on a.positionID equals c.Id
                                      where b.departmentCode == PSASCode && c.positionName.ToLower().Contains(ManagerCode)
                                      select new GetDropdownKuasaDireksiDto
                                      {
                                          Id = a.Id,
                                          officerName = a.officerName,
                                          isDefault = false
                                      }).ToList();

            return getDataManagerPSAS;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_SettingPeriodSP)]
        public List<GetDropdownKuasaDireksiDto> GetDropdownKuasaDireksiStaffPenalty()
        {
            var PSASCode = (from x in _contextPropertySystem.MS_Parameter
                            where x.code == "PSAS"
                            select x.value).FirstOrDefault();

            var StaffCode = (from x in _contextPropertySystem.MS_Parameter
                             where x.code == "STF"
                             select x.value).FirstOrDefault();
            

            var getDataStaffPSAS = (from a in _contextPropertySystem.MS_Officer
                                      join b in _contextPropertySystem.MS_Department on a.departmentID equals b.Id
                                      join c in _contextPropertySystem.MS_Position on a.positionID equals c.Id
                                      where b.departmentCode == PSASCode && c.positionName.ToLower().Contains(StaffCode)
                                      select new GetDropdownKuasaDireksiDto
                                      {
                                          Id = a.Id,
                                          officerName = a.officerName,
                                          isDefault = false
                                      }).ToList();

            return getDataStaffPSAS;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_SettingPeriodSP_Edit)]
        public void UpdateMsSPPeriod(CreateOrUpdateMsSPPeriodInputDto input)
        {
            Logger.Info("UpdateMsSPPeriod() Started.");

            //get setting default
            var getData = (from x in _contextPropertySystem.MS_SPPeriod
                           where x.letterTypeID == input.letterTypeID
                           select x).ToList();
            if (input.Id != 0)
            {
                getData = (from x in _contextPropertySystem.MS_SPPeriod
                               where x.Id == input.Id
                               select x).ToList();
            }
            
            foreach (var item in getData)
            {
                var data = item.MapTo<MS_SPPeriod>();
                data.daysDue = input.daysDue;
                data.SPGenerateID = input.SPGenerateID;
                data.kuasaDir1 = input.kuasaDir1;
                data.kuasaDir2 = input.kuasaDir2;

                if (input.Id != 0)
                {
                    data.isDefault = false;
                    data.accountID = input.accountID;
                }

                if (input.templateStatus == "updated")
                {
                    var templateToDelete = item.templateUrl;
                    DeleteFile(templateToDelete);

                    var fileUrl = MoveFile(input.templateName);
                    GetURLWithoutHost(fileUrl, out fileUrl);
                    data.templateUrl = fileUrl;
                    data.templateName = "m-" + input.templateName;
                }
                else if (input.templateStatus == "removed")
                {
                    data.templateUrl = "-";
                    data.templateName = "-";
                    DeleteFile(item.templateUrl);
                }
                else
                {
                    data.templateUrl = item.templateUrl;
                    data.templateName = item.templateName;
                }

                Logger.DebugFormat("UpdateMsSPPeriod() - Start update SP Period. Parameters sent:{0}" +
                    "	entityID	    = {1}{0}" +
                    "	clusterID	    = {2}{0}" +
                    "	daysDue	        = {3}{0}" +
                    "	letterTypeID	= {4}{0}" +
                    "	SPGenerateID    = {5}{0}" +
                    "	templateName 	= {6}{0}" +
                    "   templateUrl     = {7}{0}" +
                    "   kuasaDir1       = {8}{0}" +
                    "   kuasaDir2       = {9}{0}"
                    , Environment.NewLine, 1, item.clusterID, input.daysDue, item.letterTypeID, input.SPGenerateID, input.templateName, data.templateUrl, input.kuasaDir1, input.kuasaDir2);

                try
                {
                    _contextPropertySystem.MS_SPPeriod.Update(data);
                    _contextPropertySystem.SaveChanges();
                }
                catch (DataException exDb)
                {
                    Logger.ErrorFormat("UpdateMsSPPeriod() ERROR DbException. Result = {0}", exDb.Message);
                    throw new UserFriendlyException("Database Error : {0}", exDb.Message);
                }
                // Handle all other exceptions.
                catch (Exception ex)
                {
                    Logger.ErrorFormat("UpdateMsSPPeriod() ERROR Exception. Result = {0}", ex.Message);
                    throw new UserFriendlyException("Error : {0}", ex.Message);
                }
            }
            
            Logger.Info("UpdateMsSPPeriod() Finished.");
        }

        #region helper function
        private string MoveFile(string filename)
        {
            try
            {
                return _filesHelper.MoveFiles(filename, @"Temp\Downloads\PenaltySP\", @"Assets\HtmlTemplates\SP\PenaltySP\");
            }
            catch (Exception ex)
            {
                Logger.DebugFormat("test() - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Error : {0}", ex.Message);
            }
        }

        private void DeleteFile(string filename)
        {
            var imageToDelete = filename;
            var filenameToDelete = imageToDelete.Split(@"/");
            var nameFileToDelete = filenameToDelete[filenameToDelete.Count() - 1];
            var deletePath = @"Assets\HtmlTemplates\SP\PenaltySP\";

            var deleteImage = _hostingEnvironment.WebRootPath + deletePath + nameFileToDelete;

            if (File.Exists(deleteImage))
            {
                var file = new FileInfo(deleteImage);
                file.Delete();
            }
        }

        private void GetURLWithoutHost(string path, out string finalpath)
        {
            finalpath = path;
            try
            {
                Regex RegexObj = new Regex("[\\w\\W]*([\\/]Assets[\\w\\W\\s]*)");
                if (RegexObj.IsMatch(path))
                {
                    finalpath = RegexObj.Match(path).Groups[1].Value;
                }
            }
            catch (ArgumentException ex)
            {
                Logger.DebugFormat("test() - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Error : {0}", ex.Message);
            }
        }

        private string GetURLWithoutHost(string path)
        {
            string finalpath = path;
            try
            {
                Regex RegexObj = new Regex("[\\w\\W]*([\\/]Assets[\\w\\W\\s]*)");
                if (RegexObj.IsMatch(path))
                {
                    finalpath = RegexObj.Match(path).Groups[1].Value;
                }
            }
            catch (ArgumentException ex)
            {
                Logger.DebugFormat("test() - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Error : {0}", ex.Message);
            }
            return finalpath;
        }

        private string getAbsoluteUriWithoutTail()
        {
            var request = _HttpContextAccessor.HttpContext.Request;
            UriBuilder uriBuilder = new UriBuilder();
            uriBuilder.Scheme = request.Scheme;
            uriBuilder.Host = request.Host.ToString();
            var test = uriBuilder.ToString();
            var result = test.Replace("[", "").Replace("]", "");
            int position = result.LastIndexOf('/');
            if (position > -1)
                result = result.Substring(0, result.Length - 1);

            if (request.PathBase != null)
            {
                if (!string.IsNullOrWhiteSpace(request.PathBase.Value))
                {
                    result += request.PathBase.Value;
                }
            }
            return result;
        }

        public List<GetAllMsSPPeriodByClusterListDto> GetAllSettingDefaultSP()
        {
            var result = (from SPPeriode in _contextPropertySystem.MS_SPPeriod
                          join letterType in _contextPropertySystem.LK_LetterType on SPPeriode.letterTypeID equals letterType.Id
                          join SPGenerate in _contextPropertySystem.LK_SPGenerate on SPPeriode.SPGenerateID equals SPGenerate.Id
                          where SPPeriode.isDefault == true
                          group SPPeriode by new
                          {
                              SPPeriode.letterTypeID,
                              letterType.letterType,
                              SPPeriode.daysDue,
                              SPPeriode.SPGenerateID,
                              SPGenerate.SPGenerateDesc,
                              SPPeriode.templateName,
                              SPPeriode.templateUrl,
                              SPPeriode.kuasaDir1,
                              SPPeriode.kuasaDir2
                          } into G
                          select new GetAllMsSPPeriodByClusterListDto
                          {
                              letterTypeID      = G.Key.letterTypeID,
                              letterType        = G.Key.letterType,
                              daysDue           = G.Key.daysDue,
                              SPGenerateID      = G.Key.SPGenerateID,
                              SPGenerateDesc    = G.Key.SPGenerateDesc,
                              templateName      = G.Key.templateName,
                              templateUrl       = (G != null && G.Key.templateUrl != null) ? (!G.Key.templateUrl.Equals("-")) ? getAbsoluteUriWithoutTail() + GetURLWithoutHost(G.Key.templateUrl) : null : null, //TODO link + ip host
                              kuasaDir1         = G.Key.kuasaDir1,
                              kuasaDir2         = G.Key.kuasaDir2
                          })
                          .ToList();
            return result;
        }
        #endregion
    }
}
