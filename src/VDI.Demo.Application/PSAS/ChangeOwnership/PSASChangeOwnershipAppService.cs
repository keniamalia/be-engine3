﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.PropertySystemDB.LippoMaster;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;
using VDI.Demo.PSAS.Dto;
using System.Linq;
using VDI.Demo.PSAS.Price.Dto;
using Abp.UI;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;
using System.Threading.Tasks;
using Abp.AutoMapper;
using System.Data;
using VDI.Demo.PSAS.Term.Dto;
using System.Diagnostics;
using Newtonsoft.Json;
using Visionet_Backend_NetCore.Komunikasi;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.PSAS.ChangeOwnership.Dto;
using VDI.Demo.PSAS.Price;
using VDI.Demo.PropertySystemDB.Pricing;

namespace VDI.Demo.PSAS.ChangeOwnership
{
    public class PSASChangeOwnershipAppService : DemoAppServiceBase, IChangeOwnershipPSASAppService
    {
        private readonly IRepository<TR_BookingHeader> _trBookingHeaderRepo;
        private readonly IRepository<TR_BookingDetail> _trBookingDetailRepo;
        private readonly IRepository<TR_BookingDetailAddDisc> _trBookingDetailAddDiscRepo;
        private readonly IRepository<MS_Unit> _msUnitRepo;
        private readonly IRepository<MS_UnitCode> _msUnitCodeRepo;
        private readonly IRepository<MS_Project> _msProjectRepo;
        private readonly IRepository<TR_BookingItemPrice> _trBookingItemPriceRepo;
        private readonly IRepository<TR_BookingSalesDisc> _trBookingSalesDiscRepo;
        private readonly PropertySystemDbContext _contextProp;
        private readonly IRepository<TR_BookingDetailAddDiscHistory> _trBookingDetailAddDiscHistory;
        private readonly IRepository<TR_MKTAddDisc> _trMktAddDiscRepo;
        private readonly IRepository<TR_CommAddDisc> _trCommAddDiscRepo;
        private readonly IRepository<TR_BookingDetailHistory> _trBookingDetailHistoryRepo;
        private readonly IRepository<LK_Item> _lkItemRepo;
        private readonly PersonalsNewDbContext _contextPers;
        private readonly IPSASPriceAppService _iPriceAppService;
        private readonly IRepository<TR_BookingChangeOwner> _trBookingChangeOwnerRepo;
        private readonly IRepository<MS_Bank> _msBankRepo;
        private readonly IRepository<TR_BookingHeaderHistory> _trBookingHeaderHistoryRepo;
        private readonly IRepository<MS_Parameter> _msParameterRepo;
        private readonly IRepository<MS_Term> _msTermRepo;
        private readonly IRepository<MS_TermPmt> _msTermPmtRepo;
        private readonly IRepository<LK_FinType> _lkFinTypeRepo;

        public PSASChangeOwnershipAppService(
            IRepository<TR_BookingHeader> trBookingHeaderRepo,
            IRepository<TR_BookingDetail> trBookingDetailRepo,
            IRepository<TR_BookingDetailAddDisc> trBookingDetailAddDiscRepo,
            IRepository<MS_Unit> msUnitRepo,
            IRepository<MS_UnitCode> msUnitCodeRepo,
            IRepository<MS_Project> msProjectRepo,
            IRepository<TR_BookingItemPrice> trBookingItemPriceRepo,
            IRepository<TR_BookingSalesDisc> trBookingSalesDiscRepo,
            PropertySystemDbContext contextProp,
            IRepository<TR_BookingDetailAddDiscHistory> trBookingDetailAddDiscHistoryRepo,
            IRepository<TR_MKTAddDisc> trMktAddDiscRepo,
            IRepository<TR_CommAddDisc> trCommAddDiscRepo,
            IRepository<TR_BookingDetailHistory> trBookingDetailHistoryRepo,
            IRepository<LK_Item> lkItemRepo,
            PersonalsNewDbContext contextPers,
            IPSASPriceAppService iPriceAppService,
            IRepository<TR_BookingChangeOwner> trBookingChangeOwnerRepo,
            IRepository<MS_Bank> msBankRepo,
            IRepository<TR_BookingHeaderHistory> trBookingHeaderHistoryRepo,
            IRepository<MS_Parameter> msParameterRepo,
            IRepository<MS_Term> msTermRepo,
            IRepository<MS_TermPmt> msTermPmtRepo,
            IRepository<LK_FinType> lkFinTypeRepo
            )
        {
            _trBookingHeaderRepo = trBookingHeaderRepo;
            _trBookingDetailRepo = trBookingDetailRepo;
            _msUnitRepo = msUnitRepo;
            _msUnitCodeRepo = msUnitCodeRepo;
            _trBookingDetailAddDiscRepo = trBookingDetailAddDiscRepo;
            _msProjectRepo = msProjectRepo;
            _trBookingItemPriceRepo = trBookingItemPriceRepo;
            _trBookingSalesDiscRepo = trBookingSalesDiscRepo;
            _contextProp = contextProp;
            _trBookingDetailAddDiscHistory = trBookingDetailAddDiscHistoryRepo;
            _trMktAddDiscRepo = trMktAddDiscRepo;
            _trCommAddDiscRepo = trCommAddDiscRepo;
            _trBookingDetailHistoryRepo = trBookingDetailHistoryRepo;
            _lkItemRepo = lkItemRepo;
            _contextPers = contextPers;
            _iPriceAppService = iPriceAppService;
            _trBookingChangeOwnerRepo = trBookingChangeOwnerRepo;
            _msBankRepo = msBankRepo;
            _trBookingHeaderHistoryRepo = trBookingHeaderHistoryRepo;
            _msParameterRepo = msParameterRepo;
            _msTermRepo = msTermRepo;
            _msTermPmtRepo = msTermPmtRepo;
            _lkFinTypeRepo = lkFinTypeRepo;

        }

        public void ChangeOwnershipSave(ChangeOwnershipInputDto input)
        {
            Logger.Info("ChangeOwnershipSave() - Started.");

            //update Booking Header
            var getDataUpdateBookingHeader = (from bh in _trBookingHeaderRepo.GetAll()
                                              where bh.Id == input.bookingHeaderID
                                              select bh).FirstOrDefault();

            //history

            var checkHistory = (from A in _trBookingHeaderHistoryRepo.GetAll()
                                orderby A.Id descending
                                where A.bookCode == getDataUpdateBookingHeader.bookCode
                                select A).FirstOrDefault();

            var dataToInsertHistory = new TR_BookingHeaderHistory
            {
                bankName = getDataUpdateBookingHeader.bankName,
                bankNo = getDataUpdateBookingHeader.bankNo,
                bankRekeningPemilik = getDataUpdateBookingHeader.bankRekeningPemilik,
                BFPayTypeCode = getDataUpdateBookingHeader.BFPayTypeCode,
                bookCode = getDataUpdateBookingHeader.bookCode,
                bookDate = getDataUpdateBookingHeader.bookDate,
                discBFCalcType = getDataUpdateBookingHeader.discBFCalcType,
                cancelDate = getDataUpdateBookingHeader.cancelDate,
                DPCalcType = getDataUpdateBookingHeader.DPCalcType,
                entityID = getDataUpdateBookingHeader.entityID,
                eventID = getDataUpdateBookingHeader.eventID,
                facadeID = getDataUpdateBookingHeader.facadeID,
                SADStatusID = getDataUpdateBookingHeader.SADStatusID,
                scmCode = getDataUpdateBookingHeader.scmCode,
                shopBusinessID = getDataUpdateBookingHeader.shopBusinessID,
                isPenaltyStop = getDataUpdateBookingHeader.isPenaltyStop,
                promotionID = getDataUpdateBookingHeader.promotionID,
                isSK = getDataUpdateBookingHeader.isSK,
                isSMS = getDataUpdateBookingHeader.isSMS,
                KPRBankCode = getDataUpdateBookingHeader.KPRBankCode,
                memberCode = getDataUpdateBookingHeader.memberCode,
                sumberDanaID = getDataUpdateBookingHeader.sumberDanaID,
                memberName = getDataUpdateBookingHeader.memberName,
                nomorRekeningPemilik = getDataUpdateBookingHeader.nomorRekeningPemilik,
                PPJBDue = getDataUpdateBookingHeader.PPJBDue,
                psCode = getDataUpdateBookingHeader.psCode,
                netPriceComm = getDataUpdateBookingHeader.netPriceComm,
                NUP = getDataUpdateBookingHeader.NUP,
                remarks = getDataUpdateBookingHeader.remarks,
                termID = getDataUpdateBookingHeader.termID,
                transID = getDataUpdateBookingHeader.transID,
                termRemarks = getDataUpdateBookingHeader.termRemarks,
                tujuanTransaksiID = getDataUpdateBookingHeader.tujuanTransaksiID,
                unitID = getDataUpdateBookingHeader.unitID,
                historyNo = checkHistory == null ? (short)0 : (short)(checkHistory.historyNo + 1)
            };

            var update = getDataUpdateBookingHeader.MapTo<TR_BookingHeader>();
            update.psCode = input.newPsCode;
            update.KPRBankCode = input.newBankCode;

            var checkBookingChangeOwner = (from A in _trBookingChangeOwnerRepo.GetAll()
                                           where A.bookingHeaderID == input.bookingHeaderID
                                           orderby A.CreationTime descending
                                           select A).FirstOrDefault();

            var finType = (from bh in _trBookingHeaderRepo.GetAll()
                           join t in _msTermRepo.GetAll() on bh.termID equals t.Id into l2
                           from t in l2.DefaultIfEmpty()
                           join pt in _msTermPmtRepo.GetAll() on t.Id equals pt.termID into l3
                           from pt in l3.DefaultIfEmpty()
                           join ft in _lkFinTypeRepo.GetAll() on pt.finTypeID equals ft.Id into l4
                           from ft in l4.DefaultIfEmpty()
                           where bh.Id == input.bookingHeaderID
                           select ft.finTypeCode).FirstOrDefault();

            var entityID = (from bh in _trBookingHeaderRepo.GetAll()
                            join u in _msUnitRepo.GetAll() on bh.unitID equals u.Id 
                            join p in _msProjectRepo.GetAll() on u.projectID equals p.Id
                            where bh.Id == input.bookingHeaderID
                            select p.entityID).FirstOrDefault();

            var dataToInsertChangeOwner = new ChangeOwnershipInputDto
            {
                addnDate = input.addnDate,
                addnNo = input.addnNo,
                costAmt = input.costAmt,
                costPct = input.costPct,
                bookingHeaderID = input.bookingHeaderID,
                docNo = input.docNo,
                seqNo = checkBookingChangeOwner == null ? 1 : checkBookingChangeOwner.seqNo + 1,
                entityID = entityID == 0 ? 1 : entityID,
                jumlahSetoran = input.jumlahSetoran,
                newBankCode = input.newBankCode,
                oldBankCode = input.oldBankCode,
                newFinType = finType == null ? "-" : finType,
                oldFinType = finType == null ? "-" : finType,
                oldPsCode = input.oldPsCode,
                newPsCode = input.newPsCode,
                NJOPBangunan = input.NJOPBangunan,
                NJOPTanah = input.NJOPTanah,
                NOP = input.NOP,
                nilaiPengalihan = input.nilaiPengalihan,
                NTPN = input.NTPN,
                tanggalPenyetoran = input.tanggalPenyetoran,
                remarks = input.remarks,
                changeOwnerType = input.changeOwnerType
            };

            try
            {
                _trBookingHeaderHistoryRepo.Insert(dataToInsertHistory);
                _trBookingHeaderRepo.Update(update);
                CreateTrBookingChangeOwner(dataToInsertChangeOwner);
                Logger.Info("ChangeOwnershipSave() - Finished.");
            }
            catch (DataException ex)
            {
                Logger.ErrorFormat("ChangeOwnershipSave() - ERROR DataException. Result = {0}", ex.Message);
                Logger.Info("ChangeOwnershipSave() - Finished.");
                throw new UserFriendlyException("Db Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("ChangeOwnershipSave() - ERROR Exception. Result = {0}", ex.Message);
                Logger.Info("ChangeOwnershipSave() - Finished.");
                throw new UserFriendlyException("Error: " + ex.Message);
            }
        }

        public void CreateTrBookingChangeOwner(ChangeOwnershipInputDto input)
        {
            Logger.Info("CreateTrBookingChangeOwner() - Started.");

            var dataToInsert = new TR_BookingChangeOwner
            {
                ADDNDate = input.addnDate,
                ADDNNo = input.addnNo,
                costAmt = input.costAmt,
                costPct = input.costPct,
                bookingHeaderID = input.bookingHeaderID,
                docNo = input.docNo,
                seqNo = input.seqNo,
                entityID = input.entityID,
                jumlahSetoran = input.jumlahSetoran,
                newBankCode = input.newBankCode,
                oldBankCode = input.oldBankCode,
                newFinType = input.newFinType,
                oldFinType = input.oldFinType,
                oldPsCode = input.oldPsCode,
                newPsCode = input.newPsCode,
                nilaiJualObjekPajakBangunan = input.NJOPBangunan,
                nilaiJualObjekPajakTanah = input.NJOPTanah,
                noObjekPajak = input.NOP,
                nilaiPengalihan = input.nilaiPengalihan,
                noTandaPenerimaanNegara = input.NTPN,
                tanggalPenyetoran = input.tanggalPenyetoran,
                remarks = input.remarks,
                changeOwnerType = input.changeOwnerType
            };

            try
            {
                Logger.DebugFormat("CreateTrBookingChangeOwner() - Start insert into TR_BookingChangeOwner. Params sent:{9}" +
                "ADDNDate   = {1}{0}" +
                "ADDNNo = {2}{0}" +
                "costAmt = {3}{0}" +
                "costPct = {4}{0}" +
                "bookingHeaderID = {5}{0}" +
                "docNo  = {6}{0}" +
                "seqNo  = {7}{0}" +
                "entityID   = {8}{0}" +
                "jumlahSetoran  = {9}{0}" +
                "newBankCode    = {10}{0}" +
                "oldBankCode    = {11}{0}" +
                "newFinType = {12}{0}" +
                "oldFinType = {13}{0}" +
                "oldPsCode  = {14}{0}" +
                "newPsCode  = {15}{0}" +
                "nilaiJualObjekPajakBangunan = {16}{0}" +
                "nilaiJualObjekPajakTanah   = {17}{0}" +
                "noObjekPajak   = {18}{0}" +
                "nilaiPengalihan    = {19}{0}" +
                "noTandaPenerimaanNegara    ={20}{0}" +
                "tanggalPenyetoran  = {21}{0}" +
                "remarks    = {22}{0}"
                ,Environment.NewLine, input.addnDate, input.addnNo, input.costAmt, input.costPct, input.bookingHeaderID, input.docNo, input.seqNo, input.entityID
                , input.jumlahSetoran, input.newBankCode, input.oldBankCode, input.newFinType, input.oldFinType, input.oldPsCode, input.newPsCode
                , input.NJOPBangunan, input.NJOPTanah, input.NOP, input.nilaiPengalihan, input.NTPN, input.tanggalPenyetoran, input.remarks);

                _trBookingChangeOwnerRepo.Insert(dataToInsert);

                Logger.DebugFormat("CreateTrBookingChangeOwner() - End insert into TR_BookingChangeOwner.");
            }
            catch (DataException ex)
            {
                Logger.ErrorFormat("CreateTrBookingChangeOwner() - ERROR DataException. Result = {0}", ex.Message);
                Logger.Info("CreateTrBookingChangeOwner() - Finished.");
                throw new UserFriendlyException("Db Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("CreateTrBookingChangeOwner() - ERROR Exception. Result = {0}", ex.Message);
                Logger.Info("CreateTrBookingChangeOwner() - Finished.");
                throw new UserFriendlyException("Error: " + ex.Message);
            }
        }

        public List<GetMsBankDropdownListDto> GetBankDropdown()
        {
            var getBankDropDown = (from A in _msBankRepo.GetAll()
                                   orderby A.bankCode descending
                                   select new GetMsBankDropdownListDto
                                   {
                                       bankID = A.Id,
                                       bankCode = A.bankCode,
                                       bankName = A.bankName
                                   }).ToList();

            return getBankDropDown;
        }

        public List<string> GetChangeOwnerType()
        {
            var getDataChangeOwnerType = (from A in _msParameterRepo.GetAll()
                                          where A.code == "DRCOW"
                                          orderby A.value
                                          select A.value).ToList();

            return getDataChangeOwnerType;
        }

        public GetDataUnitChangeOwnerListDto GetDataUnitChangeOwnership(GetPSASParamsDto input)
        {
            var bookingHeaderId = _iPriceAppService.GetParameter(input).bookingHeaderID;
            var getDataUnit = (from bh in _trBookingHeaderRepo.GetAll()
                               join u in _msUnitRepo.GetAll() on bh.unitID equals u.Id
                               join uc in _msUnitCodeRepo.GetAll() on u.unitCodeID equals uc.Id
                               join p in _contextPers.PERSONAL.ToList() on bh.psCode equals p.psCode
                               join b in _msBankRepo.GetAll() on bh.KPRBankCode equals b.bankCode into l1
                               from b in l1.DefaultIfEmpty()
                               where bh.Id == bookingHeaderId
                               select new GetDataUnitChangeOwnerListDto
                               {
                                   bookingHeaderID = bh.Id,
                                   bookCode = bh.bookCode,
                                   unitNo = u.unitNo,
                                   unitCode = uc.unitCode,
                                   psCode = p.psCode,
                                   psName = p.name,
                                   bankCode = b == null ? "-" : b.bankCode,
                                   bankName = b == null ? "-" : b.bankName
                               }).FirstOrDefault();

            return getDataUnit;
        }

        public List<GetHistoryOwnerListDto> GetHistoryOwnership(GetPSASParamsDto input)
        {
            var bookingHeaderId = _iPriceAppService.GetParameter(input).bookingHeaderID;

            var getDataHistory = (from co in _trBookingChangeOwnerRepo.GetAll()
                                  join p in _contextPers.PERSONAL.ToList() on co.newPsCode equals p.psCode
                                  orderby co.CreationTime descending
                                  where co.bookingHeaderID == bookingHeaderId
                                  select new GetHistoryOwnerListDto
                                  {
                                      bookingHeaderID = co.bookingHeaderID,
                                      finType = co.newFinType,
                                      Remarks = co.remarks,
                                      docDate = DateTime.Now,//hardcode
                                      addnNo = co.ADDNNo,
                                      psName = p.name,
                                      docNo = co.docNo
                                  }).ToList();

            return getDataHistory;
        }

        public List<GetSearchPersonalListDto> SearchPersonal(string filter)
        {
            var getDataPersonals = (from p in _contextPers.PERSONAL
                                    join id in _contextPers.TR_ID on p.psCode equals id.psCode into l1
                                    from id in l1.DefaultIfEmpty()
                                    join it in _contextPers.LK_IDType on id.idType equals it.idType
                                    where p.name.Contains(filter) || p.psCode.Contains(filter) || id.idNo.Contains(filter)
                                    orderby p.psCode
                                    select new GetSearchPersonalListDto
                                    {
                                        psCode = p.psCode,
                                        name = p.name,
                                        idNo = id == null ? "-" : it.idTypeName + " - " + id.idNo
                                    })
                                    .OrderBy(x => x.name)
                                    .ToList();

            return getDataPersonals;
        }
    }
}
