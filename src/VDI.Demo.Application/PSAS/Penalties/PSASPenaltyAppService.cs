﻿using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using VDI.Demo.AccountingDB;
using VDI.Demo.Authorization;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.Payment.BulkPayment;
using VDI.Demo.Payment.BulkPayment.Dto;
using VDI.Demo.PropertySystemDB.LippoMaster;
using VDI.Demo.PSAS.Dto;
using VDI.Demo.PSAS.Penalties.Dto;
using VDI.Demo.PSAS.Price;

namespace VDI.Demo.PSAS.Penalties
{
    public class PSASPenaltyAppService : DemoAppServiceBase, IPSASPenaltyAppService
    {
        private readonly PropertySystemDbContext _contextPropertySystem;
        private readonly PersonalsNewDbContext _contextPersonals;
        private readonly AccountingDbContext _contextAccounting;
        private readonly IPSASPriceAppService _ipriceAppService;
        private readonly IRepository<TR_BookingDetailSchedule> _trBookingDetailScheduleRepo;
        private readonly IRepository<TR_PenaltySchedule> _trPenaltyScheduleRepo;
        private readonly IBulkPaymentAppService _iBulkAppService;

        public PSASPenaltyAppService(
            PropertySystemDbContext contextPropertySystem,
            PersonalsNewDbContext contextPersonals,
            AccountingDbContext contextAccounting,
            IPSASPriceAppService ipriceAppService,
            IRepository<TR_BookingDetailSchedule> trBookingDetailScheduleRepo,
            IRepository<TR_PenaltySchedule> trPenaltyScheduleRepo,
            IBulkPaymentAppService iBulkAppService
            )
        {
            _contextPropertySystem = contextPropertySystem;
            _contextPersonals = contextPersonals;
            _contextAccounting = contextAccounting;
            _ipriceAppService = ipriceAppService;
            _trBookingDetailScheduleRepo = trBookingDetailScheduleRepo;
            _trPenaltyScheduleRepo = trPenaltyScheduleRepo;
            _iBulkAppService = iBulkAppService;
        }

        public void CreatePaymentPenaltyWaive()
        {
            var getDataWaiveToInsert = (from a in _contextPropertySystem.TR_PenaltySchedule
                                        join b in _contextPropertySystem.TR_BookingHeader on a.bookingHeaderID equals b.Id
                                        join c in _contextPropertySystem.MS_Unit on b.unitID equals c.Id
                                        join d in _contextPropertySystem.MS_ClusterPenalty on c.clusterID equals d.clusterID
                                        join e in _contextPropertySystem.MS_UnitCode on c.unitCodeID equals e.Id
                                        join f in _contextPropertySystem.TR_BookingDetail on b.Id equals f.bookingHeaderID
                                        join g in _contextPropertySystem.MS_Account on c.projectID equals g.projectID
                                        where d.autoWavePeriod != 0 && DateTime.Now.Date <= DateTime.Now.AddDays((double)d.autoWavePeriod).Date
                                        group a by new
                                        {
                                            a,
                                            c.projectID,
                                            b.psCode,
                                            bookingHeaderID = b.Id,
                                            b.bookCode,
                                            d.autoWavePeriod,
                                            e.unitCode,
                                            b.unitID,
                                            c.unitNo,
                                            f.pctTax,
                                            accID = g.Id
                                        } into x
                                        select new
                                        {
                                            x.Key.a,
                                            x.Key.projectID,
                                            x.Key.psCode,
                                            x.Key.bookingHeaderID,
                                            x.Key.bookCode,
                                            x.Key.autoWavePeriod,
                                            x.Key.unitCode,
                                            x.Key.unitID,
                                            x.Key.unitNo,
                                            x.Key.pctTax,
                                            x.Key.accID
                                        }).ToList();

            var totalPaymentDetail = (from x in getDataWaiveToInsert
                                      group x by new
                                      {
                                          x.accID
                                      } into G
                                      select new
                                      {
                                          G.Key.accID,
                                      }).ToList();


            var journalCodeStart = "";
            var journalCode = "";
            var runningNumber = "";
            foreach (var item in totalPaymentDetail)
            {
                var total = (from x in getDataWaiveToInsert
                             where x.accID == item.accID
                             select x).ToList();

                var code = (from A in _contextPropertySystem.MS_Account where A.Id == item.accID select new { A.devCode, A.accCode }).FirstOrDefault();

                var getTempJournalCode = (from x in _contextAccounting.SYS_JournalCounter
                                          where x.accCode == code.accCode
                                          select x).FirstOrDefault();

                if (getTempJournalCode != null)
                {
                    var dataExisting = getTempJournalCode.journalCode.Split('.');

                    string coCodeExisting = dataExisting[1];
                    string accCodeExisting = dataExisting[2];
                    string yearExisting = dataExisting[3];
                    string monthExisting = dataExisting[4];
                    int runningNumberExisting = Convert.ToInt32(dataExisting[5]) + total.Count;
                    runningNumber = runningNumberExisting.ToString("D5");

                    if (code.devCode == coCodeExisting && code.accCode == accCodeExisting && DateTime.Now.Year.ToString() == yearExisting && DateTime.Now.Month.ToString() == monthExisting)
                    {
                        journalCodeStart = getTempJournalCode.journalCode;
                        journalCode = "PRS." + code.devCode + "." + code.accCode + "." + yearExisting + "." + monthExisting + "." + runningNumber;
                    }
                    else
                    {
                        int i = 0;
                        var runningNumberStart = i.ToString("D5");
                        runningNumber = total.Count.ToString("D5");
                        journalCodeStart = "PRS." + code.devCode + "." + code.accCode + "." + DateTime.Now.Year.ToString() + "." + DateTime.Now.Month.ToString() + "." + runningNumberStart;
                        journalCode = "PRS." + code.devCode + "." + code.accCode + "." + DateTime.Now.Year.ToString() + "." + DateTime.Now.Month.ToString() + "." + runningNumber;
                    }

                    var dataJournalUpdate = getTempJournalCode.MapTo<SYS_JournalCounter>();
                    dataJournalUpdate.journalCode = journalCode;
                    _contextAccounting.SYS_JournalCounter.Update(dataJournalUpdate);
                }
                else
                {
                    int i = 0;
                    runningNumber = total.Count.ToString("D5");
                    var startRunningNumber = i.ToString("D5");
                    journalCode = "PRS." + code.devCode + "." + code.accCode + "." + DateTime.Now.Year.ToString() + "." + DateTime.Now.Month.ToString() + "." + runningNumber;
                    journalCodeStart = "PRS." + code.devCode + "." + code.accCode + "." + DateTime.Now.Year.ToString() + "." + DateTime.Now.Month.ToString() + "." + startRunningNumber;

                    var dataJournalInsert = new SYS_JournalCounter
                    {
                        accCode = code.accCode,
                        journalCode = journalCode
                    };
                    _contextAccounting.SYS_JournalCounter.Add(dataJournalInsert);
                }
                _contextAccounting.SaveChanges();
            }

            foreach (var data in getDataWaiveToInsert)
            {
                var getPayForPen = (from a in _contextPropertySystem.MS_Parameter
                                   join b in _contextPropertySystem.LK_Alloc on a.value equals b.allocCode
                                   join c in _contextPropertySystem.LK_PayFor on b.payForID equals c.Id
                                   where a.code == "EXCPN"
                                   select new
                                   {
                                       b.Id,
                                       payForID = c.Id
                                   }).FirstOrDefault();

                var getPayTypePen = (from a in _contextPropertySystem.MS_Parameter
                                     join b in _contextPropertySystem.LK_PayType on a.value equals b.payTypeCode
                                     where a.code == "PTPW"
                                     select b.Id).FirstOrDefault();

                var getOthersTypePen = (from a in _contextPropertySystem.MS_Parameter
                                        join b in _contextPropertySystem.LK_OthersType on a.value equals b.othersTypeCode
                                        where a.code == "OTPW"
                                        select new
                                        {
                                            b.Id,
                                            b.othersTypeCode
                                        }).FirstOrDefault();

                var getAccount = (from a in _contextPropertySystem.MS_Account
                                  where a.projectID == data.projectID
                                  select a).FirstOrDefault();

                var getSchednoMax = (from a in _contextPropertySystem.TR_BookingDetailSchedule
                                     join b in _contextPropertySystem.TR_BookingDetail on a.bookingDetailID equals b.Id
                                     where b.bookingHeaderID == data.bookingHeaderID
                                     orderby a.schedNo descending
                                     select a.schedNo).FirstOrDefault();

                #region waivePenalty

                if (data.autoWavePeriod != 0 && DateTime.Now.Date <= DateTime.Now.AddDays((double)data.autoWavePeriod).Date)
                {
                    var dataScheduleList = new List<DataAlloc>();
                    var dataPaymentList = new List<DataPayment>();
                    //get personal by bookCode
                    var getPersonal = (from a in _contextPersonals.PERSONAL
                                       join c in _contextPersonals.TR_Address on a.psCode equals c.psCode into address
                                       from e in address.DefaultIfEmpty()
                                       join b in _contextPersonals.LK_AddrType on new { addrType = (e == null ? "" : e.addrType), addrName = "Corress" } equals new { addrType = b.addrType, addrName = b.addrTypeName } into addrType
                                       from f in addrType.DefaultIfEmpty()
                                       where a.psCode == data.psCode
                                       select new
                                       {
                                           a.name,
                                           a.NPWP,
                                           address = e == null ? null : e.address
                                       }).FirstOrDefault();

                    var dataSchedule = new DataAlloc
                    {
                        allocID = getPayForPen.Id,
                        schedNo = (short)getSchednoMax,
                        amount = data.a.penaltyAmount - data.a.penaltyAmount, //amount setelah dikurangi
                        amountPerSchedNo = data.a.penaltyAmount //jumlah yang harus dibayar
                    };
                    dataScheduleList.Add(dataSchedule);

                    var dataPayment = new DataPayment
                    {
                        payTypeID = getPayTypePen,
                        othersTypeID = getOthersTypePen.Id,
                        othersTypeCode = getOthersTypePen.othersTypeCode,
                        amount = data.a.penaltyAmount, //bayar berapa
                        dataAllocList = dataScheduleList
                    };
                    dataPaymentList.Add(dataPayment);

                    var journalData = journalCodeStart.Split(".");
                    var formatJournal = journalData[0] + "." + journalData[1] + "." + journalData[2] + "." + journalData[3] + "." + journalData[4];
                    var runningJournal = Convert.ToInt32(journalData[5]);

                    var dataCreatePayment = new CreateUniversalBulkPaymentInputDto
                    {
                        accID = getAccount.Id,
                        bookingHeaderID = data.bookingHeaderID,
                        bookCode = data.bookCode,
                        bankName = "-",//belum dikasih tau
                        clearDate = DateTime.Now,
                        keterangan = "[Auto] Waive penalty Developer",
                        unitCode = data.unitCode,
                        unitNo = data.unitNo,
                        unitID = data.unitID,
                        name = getPersonal.name,
                        psCode = data.psCode,
                        journalCode = formatJournal,
                        runningJournal = runningJournal,
                        address = getPersonal.address,
                        paymentDate = DateTime.Now,
                        payForID = getPayForPen.payForID,
                        pctTax = data.pctTax,
                        NPWP = getPersonal.NPWP,
                        flag = 3,
                        dataForPayment = dataPaymentList,
                        dataScheduleList = dataScheduleList
                    };
                    _iBulkAppService.CreateBulkPayment(dataCreatePayment);

                    var countInsertedjournal = dataPaymentList.Count();

                    int runningNumberExisting = runningJournal + countInsertedjournal;
                    runningNumber = runningNumberExisting.ToString("D5");

                    journalCodeStart = formatJournal + "." + runningNumber;
                }
                #endregion
            }

        }

        public void CreatePenaltyScheduler()
        {
            var getDPINSPen = (from a in _contextPropertySystem.MS_Parameter
                               where a.code == "DPPN" || a.code == "INSPN"
                               select a.value).ToList();
            //get data yang lebih dari duedate booking schedule
            var getBookingAfterDueDate = (from a in _contextPropertySystem.TR_BookingDetailSchedule
                                          join b in _contextPropertySystem.TR_BookingDetail on a.bookingDetailID equals b.Id
                                          join c in _contextPropertySystem.TR_BookingHeader on b.bookingHeaderID equals c.Id
                                          join d in _contextPropertySystem.MS_Unit on c.unitID equals d.Id
                                          join e in _contextPropertySystem.MS_ClusterPenalty on d.clusterID equals e.clusterID
                                          join f in _contextPropertySystem.LK_Alloc on a.allocID equals f.Id
                                          join g in _contextPropertySystem.MS_UnitCode on d.unitCodeID equals g.Id
                                          where DateTime.Now.Date > a.dueDate.Date && a.netOut != 0 && a.vatOut != 0 && (c.bookDate.Date >= e.bookingPeriodStart.Date && c.bookDate.Date <= e.bookingPeriodEnd.Date) && getDPINSPen.Contains(f.allocCode)
                                          group a by new
                                          {
                                              a.schedNo,
                                              bookingHeaderID = c.Id,
                                              f.allocCode,
                                              e.penaltyBaseRate,
                                              e.penaltyFreq,
                                              e.penaltyRate,
                                              e.autoWavePeriod,
                                              a.allocID,
                                              d.projectID,
                                              c.bookCode,
                                              d.unitNo,
                                              unitID = d.Id,
                                              g.unitCode,
                                              c.psCode,
                                              b.pctTax
                                          } into x
                                          select new
                                          {
                                              x.Key.schedNo,
                                              x.Key.bookingHeaderID,
                                              x.Key.allocCode,
                                              totalOut = x.Sum(g => g.netOut) + x.Sum(g => g.vatOut),
                                              totalAmt = x.Sum(g => g.netAmt) + x.Sum(g => g.vatAmt),
                                              netOut = x.Sum(g => g.netOut),
                                              vatOut = x.Sum(g => g.vatOut),
                                              x.Key.penaltyBaseRate,
                                              x.Key.penaltyFreq,
                                              x.Key.penaltyRate,
                                              x.Key.autoWavePeriod,
                                              x.Key.allocID,
                                              x.Key.projectID,
                                              x.Key.bookCode,
                                              x.Key.unitNo,
                                              x.Key.unitID,
                                              x.Key.unitCode,
                                              x.Key.psCode,
                                              x.Key.pctTax
                                          })
                                          .OrderBy(x => x.bookingHeaderID).ThenBy(n => n.schedNo)
                                          .ToList();

            var bookingHeaderBefore = 0;
            var listDataUpdateBooking = new List<GetBookingDetailUpdateListDto>();
            foreach (var data in getBookingAfterDueDate)
            {
                int i = 1;
                // pengecekan untuk insert atau update
                var cekPenaltySchedule = (from a in _contextPropertySystem.TR_PenaltySchedule
                                          where a.bookingHeaderID == data.bookingHeaderID && a.ScheduleAllocCode == data.allocCode
                                          select a);

                var getTotalAmount = (from x in _contextPropertySystem.TR_BookingDetail
                                      where x.bookingHeaderID == data.bookingHeaderID
                                      group x by new { x.bookingHeaderID } into G
                                      select new
                                      {
                                          bookHeaderID = G.Key.bookingHeaderID,
                                          TotalNetNetPrice = G.Sum(d => d.netNetPrice)
                                      }).FirstOrDefault();

                var getBookingDetail = (from a in _contextPropertySystem.TR_BookingDetail
                                        join b in _contextPropertySystem.LK_Item on a.itemID equals b.Id
                                        where a.bookingHeaderID == data.bookingHeaderID
                                        select new { a.Id, b.itemCode }).ToList();

                var getAllocPen = (from a in _contextPropertySystem.MS_Parameter
                                   join b in _contextPropertySystem.LK_Alloc on a.value equals b.allocCode
                                   join c in _contextPropertySystem.LK_PayFor on b.payForID equals c.Id
                                   where a.code == "EXCPN"
                                   select new
                                   {
                                       b.Id,
                                       payForID = c.Id
                                   }).FirstOrDefault();

                
                if (!cekPenaltySchedule.Any())
                {
                    //insert TR Penalty Schedule TR Booking Schedule

                    #region createTRPenaltySchedule

                    //cek book code di tr penalty schedule kebutuhan seqNo per book code
                    var cekBookCodeInPenaltySchedule = (from a in _contextPropertySystem.TR_PenaltySchedule
                                                        where a.bookingHeaderID == data.bookingHeaderID
                                                        orderby a.SeqNo descending
                                                        select a).FirstOrDefault();


                    var dataCreate = new TR_PenaltySchedule
                    {
                        bookingHeaderID = data.bookingHeaderID,
                        entityID = 1,
                        ScheduleAllocCode = data.allocCode,
                        ScheduleTerm = data.schedNo,
                        ScheduleNetAmount = data.totalAmt,
                        SeqNo = cekBookCodeInPenaltySchedule == null ? i : cekBookCodeInPenaltySchedule.SeqNo + 1,
                        penaltyAging = 1,
                        penaltyAmount = data.penaltyFreq == "Daily" ? (((data.totalOut * (decimal)(data.penaltyRate / data.penaltyBaseRate)) / 30) * 1) : (data.totalOut * (decimal)(Math.Pow((1 + (data.penaltyRate / data.penaltyBaseRate)), 1) - 1)),
                        scheduleStatus = "Outstanding"
                    };
                    _trPenaltyScheduleRepo.Insert(dataCreate);
                    #endregion

                    #region createTRBookingScheduleForPEN


                    var getSchednoMax = (from a in _contextPropertySystem.TR_BookingDetailSchedule
                                         join b in _contextPropertySystem.TR_BookingDetail on a.bookingDetailID equals b.Id
                                         where b.bookingHeaderID == data.bookingHeaderID
                                         orderby a.schedNo descending
                                         select a.schedNo).FirstOrDefault();


                    if (bookingHeaderBefore != data.bookingHeaderID)
                    {
                        listDataUpdateBooking = new List<GetBookingDetailUpdateListDto>();
                    }

                    foreach (var item in getBookingDetail)
                    {
                        var getPercentage = (from x in _contextPropertySystem.TR_BookingDetail
                                             where x.bookingHeaderID == data.bookingHeaderID && x.Id == item.Id
                                             select new
                                             {
                                                 x.bookingHeaderID,
                                                 netNetPrice = x.netNetPrice / getTotalAmount.TotalNetNetPrice
                                             }).FirstOrDefault();

                        decimal netOut = data.penaltyFreq == "Daily" ? (((data.netOut * (decimal)(data.penaltyRate / data.penaltyBaseRate)) / 30) * 1) * getPercentage.netNetPrice : (data.netOut * (decimal)(Math.Pow((1 + (data.penaltyRate / data.penaltyBaseRate)), 1) - 1)) * getPercentage.netNetPrice;

                        decimal vatOut = data.penaltyFreq == "Daily" ? (((data.vatOut * (decimal)(data.penaltyRate / data.penaltyBaseRate)) / 30) * 1) * getPercentage.netNetPrice : (data.vatOut * (decimal)(Math.Pow((1 + (data.penaltyRate / data.penaltyBaseRate)), 1) - 1)) * getPercentage.netNetPrice;

                        if (bookingHeaderBefore != 0 && bookingHeaderBefore == data.bookingHeaderID)
                        {
                            var idForUpdate = (from a in listDataUpdateBooking
                                               where a.itemCode == item.itemCode
                                               select a.bookingDetailID).FirstOrDefault();
                            var getDataForUpdate = (from a in _trBookingDetailScheduleRepo.GetAll()
                                                    where a.Id == idForUpdate
                                                    select a).FirstOrDefault();

                            var updateBookingDetailSchedule = getDataForUpdate.MapTo<TR_BookingDetailSchedule>();
                            updateBookingDetailSchedule.netAmt = getDataForUpdate.netAmt + netOut;
                            updateBookingDetailSchedule.vatAmt = getDataForUpdate.vatAmt + vatOut;
                            updateBookingDetailSchedule.netOut = getDataForUpdate.netOut + netOut;
                            updateBookingDetailSchedule.vatOut = getDataForUpdate.vatOut + vatOut;

                            _trBookingDetailScheduleRepo.Update(updateBookingDetailSchedule);
                        }
                        else
                        {
                            var dataCreateBooking = new TR_BookingDetailSchedule
                            {
                                allocID = getAllocPen.Id,
                                bookingDetailID = item.Id,
                                entityID = 1,
                                netAmt = netOut,
                                vatAmt = vatOut,
                                netOut = netOut,
                                vatOut = vatOut,
                                schedNo = (short)(getSchednoMax + 1),
                                remarks = "-",
                                dueDate = DateTime.Now
                            };
                            try
                            {
                                Logger.DebugFormat("CreatePenaltyScheduler() - Start insert TR Booking Schedule. Parameters sent:{0}" +
                                    "orToAdjust = {1}{0}" +
                                    "controlNo = {2}{0}"
                                    , Environment.NewLine);

                                var Id = _trBookingDetailScheduleRepo.InsertAndGetId(dataCreateBooking);
                                var dataGetToUpdate = new GetBookingDetailUpdateListDto
                                {
                                    bookingDetailID = Id,
                                    itemCode = item.itemCode
                                };
                                listDataUpdateBooking.Add(dataGetToUpdate);

                                Logger.DebugFormat("CreatePenaltyScheduler() - Ended insert TR Booking Schedule.");
                            }
                            catch (DataException ex)
                            {
                                Logger.ErrorFormat("CreatePenaltyScheduler() - ERROR DataException. Result = {0}", ex.Message);
                                throw new UserFriendlyException("Db Error: " + ex.Message);
                            }
                            catch (Exception ex)
                            {
                                Logger.ErrorFormat("CreatePenaltyScheduler() - ERROR Exception. Result = {0}", ex.Message);
                                throw new UserFriendlyException("Error: " + ex.Message);
                            }

                        }
                    }
                    bookingHeaderBefore = data.bookingHeaderID;
                    i++;
                    #endregion
                }
                else
                {
                    //update TR Penalty Schedule TR Booking Schedule

                    //update TR Penalty Schedule
                    var getPenalty = cekPenaltySchedule.FirstOrDefault();
                    
                    var updatePenaltySchedule = getPenalty.MapTo<TR_PenaltySchedule>();
                    updatePenaltySchedule.penaltyAmount = data.penaltyFreq == "Daily" ? (((data.totalOut * (decimal)(data.penaltyRate / data.penaltyBaseRate)) / 30) * (getPenalty.penaltyAging + 1)) : (data.totalOut * (decimal)(Math.Pow((1 + (data.penaltyRate / data.penaltyBaseRate)), (getPenalty.penaltyAging + 1)) - 1));
                    updatePenaltySchedule.penaltyAging = getPenalty.penaltyAging + 1;
                    
                    _trPenaltyScheduleRepo.Update(updatePenaltySchedule);

                    //update TR Booking Detail Schedule
                    foreach (var item in getBookingDetail)
                    {
                        var getPercentage = (from x in _contextPropertySystem.TR_BookingDetail
                                             where x.bookingHeaderID == data.bookingHeaderID && x.Id == item.Id
                                             select new
                                             {
                                                 x.bookingHeaderID,
                                                 netNetPrice = x.netNetPrice / getTotalAmount.TotalNetNetPrice
                                             }).FirstOrDefault();


                        decimal netOut = data.penaltyFreq == "Daily" ? (((data.netOut * (decimal)(data.penaltyRate / data.penaltyBaseRate)) / 30) * (getPenalty.penaltyAging + 1)) * getPercentage.netNetPrice : (data.netOut * (decimal)(Math.Pow((1 + (data.penaltyRate / data.penaltyBaseRate)), (getPenalty.penaltyAging + 1)) - 1)) * getPercentage.netNetPrice;

                        decimal vatOut = data.penaltyFreq == "Daily" ? (((data.vatOut * (decimal)(data.penaltyRate / data.penaltyBaseRate)) / 30) * (getPenalty.penaltyAging + 1)) * getPercentage.netNetPrice : (data.vatOut * (decimal)(Math.Pow((1 + (data.penaltyRate / data.penaltyBaseRate)), (getPenalty.penaltyAging + 1)) - 1)) * getPercentage.netNetPrice;

                        var getDataForUpdate = (from a in _trBookingDetailScheduleRepo.GetAll()
                                                where a.bookingDetailID == item.Id && a.allocID == getAllocPen.Id
                                                select a).FirstOrDefault();

                        var updateBookingDetailSchedule = getDataForUpdate.MapTo<TR_BookingDetailSchedule>();
                        updateBookingDetailSchedule.netAmt = getDataForUpdate.netAmt + netOut;
                        updateBookingDetailSchedule.vatAmt = getDataForUpdate.vatAmt + vatOut;
                        updateBookingDetailSchedule.netOut = getDataForUpdate.netOut + netOut;
                        updateBookingDetailSchedule.vatOut = getDataForUpdate.vatOut + vatOut;

                        _trBookingDetailScheduleRepo.Update(updateBookingDetailSchedule);
                    }
                }

            }
            CurrentUnitOfWork.SaveChanges();
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_PSAS_Penalty)]
        public GetDataPenaltyViewListDto GetDataPenaltyView(GetPSASParamsDto input)
        {
            var dataBookingId = _ipriceAppService.GetParameter(input);

            var getDataPenaltySchedule = (from x in _contextPropertySystem.TR_PenaltySchedule
                                          join y in _contextPropertySystem.TR_BookingDetail on x.bookingHeaderID equals y.bookingHeaderID
                                          join z in _contextPropertySystem.TR_BookingDetailSchedule on new { bookingDetailID = y.Id, schedNo = (short) x.ScheduleTerm } equals new { bookingDetailID = z.bookingDetailID, schedNo = z.schedNo }
                                          join w in _contextPropertySystem.TR_PaymentHeader on x.bookingHeaderID equals w.bookingHeaderID into ww
                                          from w in ww.DefaultIfEmpty()
                                          where x.bookingHeaderID == dataBookingId.bookingHeaderID
                                          select new GetPenaltyScheduleDto
                                          {
                                              penaltyScheduleID = x.Id,
                                              schedNo = x.ScheduleTerm,
                                              allocCode = x.ScheduleAllocCode,
                                              dueDate = z.dueDate,
                                              amountNo = x.ScheduleNetAmount,
                                              aging = x.penaltyAging,
                                              penAmount = x.penaltyAmount,
                                              penPaymentDate = w == null ? default(DateTime) : w.paymentDate,
                                              statusPenalty = w == null ? "Outstanding" : "Fully Paid"
                                          }).Distinct().ToList();

            var getDataPenalty = (from a in _contextPropertySystem.TR_BookingHeader
                                  join b in _contextPropertySystem.MS_Unit on a.unitID equals b.Id
                                  join c in _contextPropertySystem.MS_Project on b.projectID equals c.Id
                                  join d in _contextPropertySystem.MS_Cluster on b.clusterID equals d.Id
                                  join e in _contextPropertySystem.MS_UnitCode on b.unitCodeID equals e.Id
                                  join f in _contextPersonals.PERSONAL.ToList() on a.psCode equals f.psCode
                                  where a.Id == dataBookingId.bookingHeaderID
                                  select new GetDataPenaltyViewListDto
                                  {
                                      projectID = b.projectID,
                                      project = c.projectName,
                                      clusterID = b.clusterID,
                                      cluster = d.clusterName,
                                      unitID = a.unitID,
                                      unitCode = e.unitCode,
                                      unitNo = b.unitNo,
                                      psCode = a.psCode,
                                      custName = f.name,
                                      dataPenaltySchedule = getDataPenaltySchedule
                                  }).FirstOrDefault();

            return getDataPenalty;
        }
    }
}
