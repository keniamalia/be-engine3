﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Abp.Extensions;
using Abp.Linq.Extensions;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;
using VDI.Demo.Files;
using VDI.Demo.PropertySystemDB.LippoMaster;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;
using VDI.Demo.PSAS.LegalDocument.MappingTemplate.Dto;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System.Data;
using Abp.UI;
using Abp.Authorization;
using VDI.Demo.Authorization;
using VDI.Demo.EntityFrameworkCore;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Abp.Collections.Extensions;

namespace VDI.Demo.PSAS.LegalDocument.MappingTemplate
{
    //[AbpAuthorize(AppPermissions.Pages_Tenant_PSAS_MasterLegalDocument_MappingTemplate)]
    public class PSASMappingTemplateAppService : DemoAppServiceBase, IPSASMappingTemplateAppService
    {
        private readonly IRepository<MS_MappingTemplate> _msMappingTemplateRepo;
        private readonly IRepository<MS_DocumentPS> _msDocumentPsRepo;
        private readonly IRepository<MS_DocTemplate> _msDocTemplateRepo;
        private readonly IRepository<MS_Project> _msProjectRepo;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IFilesHelper _iFilesHelper;
        private readonly DemoDbContext _contextDemo;

        public PSASMappingTemplateAppService(
            IRepository<MS_MappingTemplate> msMappingTemplateRepo,
            IRepository<MS_DocumentPS> msDocumentPsRepo,
            IRepository<MS_DocTemplate> msDocTemplateRepo,
            IRepository<MS_Project> msProjectRepo,
            IHttpContextAccessor httpContextAccessor,
            IFilesHelper iFilesHelper,
            DemoDbContext contextDemo
            )
        {
            _msMappingTemplateRepo = msMappingTemplateRepo;
            _msDocumentPsRepo = msDocumentPsRepo;
            _msDocTemplateRepo = msDocTemplateRepo;
            _msProjectRepo = msProjectRepo;
            _httpContextAccessor = httpContextAccessor;
            _iFilesHelper = iFilesHelper;
            _contextDemo = contextDemo;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_PSAS_MasterLegalDocument_MappingTemplate_Create)]
        public void CreateMappingTemplate(CreateMappingTemplateInputDto input)
        {
            Logger.InfoFormat("CreateMappingTemplate() Started.");

            Logger.DebugFormat("CreateMappingTemplate() - Start Preparation Data For Insert MS_MappingTemplate. Parameters sent: {0} " +
            "   entityID = 1" +
            "   docID = {1}{0}" +
            "   docTemplateID = {2}{0}" +
            "   activeFrom = {3}{0}" +
            "   activeTo = {4}{0}" +
            "   isActive = {5}{0}" +
            "   projectID = {6}{0}"
            , Environment.NewLine, input.docID, input.docTemplateID, input.activeFrom, input.activeTo, input.isActive, input.projectID);

            if (input.isTandaTerimaFile != null && input.isTandaTerimaFile != "")
            {
                if (input.isTandaTerimaFile == "Default-Surat-Terima.html")
                {
                    Logger.Info("Surat-Terima.html - /LegalDocument/TandaTerima/");
                    input.isTandaTerimaFile = "/Assets/Upload/LegalDocument/TandaTerima/Default-Surat-Terima.html";
                }
                else
                {
                    var isTandaTerimaFile = uploadFile(input.isTandaTerimaFile, @"Temp\Downloads\LegalDocument\TandaTerima\", @"Assets\Upload\LegalDocument\TandaTerima\" + input.docTemplateID + @"\");
                    GetURLWithoutHost(isTandaTerimaFile, out isTandaTerimaFile);
                    input.isTandaTerimaFile = isTandaTerimaFile;
                }
            }

            var dataMappingTemplate = new MS_MappingTemplate
            {
                entityID = 1,
                projectID = input.projectID,
                docID = input.docID,
                docTemplateID = input.docTemplateID,
                activeFrom = input.activeFrom,
                activeTo = input.activeTo,
                isActive = input.isActive,
                isTandaTerima = input.isTandaTerima,
                isTandaTerimaFile = input.isTandaTerimaFile
            };
            Logger.DebugFormat("CreateMappingTemplate() - End Preparation Data For Insert MS_MappingTemplate.");
            
            Logger.InfoFormat("CheckDataIsExist() Start check Data is exist.");
            /*
            Logger.DebugFormat("CheckDataInsertIsExist() - Start Checking Data. ");
            var dataIsExist = CheckDataInsertIsExist(input.projectID, input.docID, input.docTemplateID, input.activeFrom, input.activeTo);
            Logger.DebugFormat("CheckDataInsertIsExist() - End Checking Data. ");
            
            if (dataIsExist.isExist)
            {
                //if (dataIsExist.isActiveUpdated != false)
                //{
                    UpdateDataIsExist(dataIsExist.mappingTemplateID.GetValueOrDefault(), dataIsExist.activeToOldData.GetValueOrDefault());
                //}
               
                /*
                if (!input.activeTo.HasValue)
                {
                    dataMappingTemplate.activeTo = dataIsExist.activeTo;
                }
                
            }
            */

            Logger.DebugFormat("CreateMappingTemplate() - Start Insert MS_MappingTemplate. ");
            _msMappingTemplateRepo.Insert(dataMappingTemplate);
            Logger.DebugFormat("CreateMappingTemplate() - End Insert MS_MappingTemplate. ");

            Logger.InfoFormat("CreateMappingTemplate() Finished.");
        }


        public CheckDataUpdateIsExistListDto CheckDataInsertIsExist(int projectID, int docID, int docTemplateID, DateTime activeFrom, DateTime? activeTo)
        {
            CheckDataUpdateIsExistListDto returnDataUpdate = new CheckDataUpdateIsExistListDto();

            var GetMappingTemplate = (from A in _msMappingTemplateRepo.GetAll()
                                      join B in _msDocTemplateRepo.GetAll() on A.docTemplateID equals B.Id
                                      join C in _msProjectRepo.GetAll() on A.projectID equals C.Id
                                      join D in _msDocumentPsRepo.GetAll() on A.docID equals D.Id
                                      where A.projectID == projectID && A.docID == docID                                                                         
                                      && A.isActive
                                      && B.isActive.GetValueOrDefault()
                                      select new UpdateMappingTemplateInputDto
                                      {
                                          entityID = A.entityID,
                                          isTandaTerima = A.isTandaTerima,
                                          mappingTemplateID = A.Id,
                                          docID = A.docID,
                                          docTemplateID = B.Id,
                                          projectID = A.projectID,
                                          isActive = A.isActive,
                                          activeFrom = A.activeFrom,
                                          activeTo = A.activeTo
                                      })
                                      .OrderByDescending(x => x.activeFrom)
                                      .ThenByDescending(x => x.activeTo);

            if (GetMappingTemplate.Any())
            {   
                //Validation

                if (GetMappingTemplate.Any(x => x.activeFrom >= activeFrom ))
                {
                    throw new UserFriendlyException("Newer period is exist!");
                }

                if (GetMappingTemplate.Any(x => x.activeTo != null && (x.activeFrom <= activeFrom && x.activeTo >= activeFrom)))
                {
                    throw new UserFriendlyException("Data with such active time period already exists! Please enter an active date outside of an existing period");
                }
                
                if (activeTo.HasValue && GetMappingTemplate.Any(x => x.activeTo != null && ((x.activeFrom <= activeFrom && x.activeTo >= activeFrom) && (x.activeFrom <= activeTo && x.activeTo >= activeTo))))
                {
                    throw new UserFriendlyException("Data with such active time period already exists! Please enter an active date outside of an existing period");
                }
              
                //Return Value
                if (GetMappingTemplate.Any(x => x.activeTo == null && x.activeFrom < activeFrom && x.isActive))
                {
                    var singleData = GetMappingTemplate.Where(x => x.activeTo == null && x.activeFrom < activeFrom && x.isActive).OrderByDescending(x => x.activeFrom).FirstOrDefault();
                    returnDataUpdate.isExist = true;
                    returnDataUpdate.mappingTemplateID = singleData.mappingTemplateID;
                    returnDataUpdate.activeToOldData = activeFrom.AddDays(-1);
                    returnDataUpdate.isConfirmation = true;
                }
                else
                {
                    returnDataUpdate.isConfirmation = false;
                }
            }
            return returnDataUpdate;
        }

        public void UpdateDataIsExist(int mappingTemplateID, DateTime activeTo)
        {
            Logger.InfoFormat("UpdateDataIsExist() - Started.");
            Logger.DebugFormat("UpdateDataIsExist() - Start get MS_MappingTemplate for update. Parameters sent: {0} " +
                "mappingTemplateID = {1}{0}", Environment.NewLine, mappingTemplateID);
            var getMappingTemplate = (from x in _msMappingTemplateRepo.GetAll()
                                      where x.Id == mappingTemplateID
                                      select x).FirstOrDefault();
            Logger.DebugFormat("UpdateDataIsExist() - End get MS_MappingTemplate for update. Result = {0} ", getMappingTemplate);

            var updateMappingTemplate = getMappingTemplate.MapTo<MS_MappingTemplate>();
            updateMappingTemplate.activeTo = activeTo;
            
            Logger.DebugFormat("UpdateDataIsExist() - Start Update MS_MappingTemplate. Parameters sent: {0} " +
                "projectID = {1}{0}" +
                "docID = {2}{0}" +
                "docTemplateID = {3}{0}" +
                "activeFrom = {4}{0}" +
                "activeTo = {5}{0}" +
                "isTandaTerima = {6}{0}" +
                "isActive = {7}{0}"
                , Environment.NewLine, updateMappingTemplate.projectID, updateMappingTemplate.docID
                , updateMappingTemplate.docTemplateID, updateMappingTemplate.activeFrom, activeTo
                , updateMappingTemplate.isTandaTerima, updateMappingTemplate.isActive);
            _msMappingTemplateRepo.Update(updateMappingTemplate);
            Logger.DebugFormat("UpdateDataIsExist() - End Update MS_MappingTemplate.");
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_PSAS_MasterLegalDocument_MappingTemplate_Delete)]
        public void DeleteMappingTemplate(int mappingTemplateID)
        {
            Logger.InfoFormat("DeleteMappingTemplate() Started.");

            var getMappingTemplate = (from x in _msMappingTemplateRepo.GetAll()
                                         where x.Id == mappingTemplateID
                                         select x).FirstOrDefault();
            var updateMappingTemplate = getMappingTemplate.MapTo<MS_MappingTemplate>();
            updateMappingTemplate.isActive = false;

            Logger.DebugFormat("DeleteMappingTemplate() - Start Update isActive MS_MappingTemplate where id: "+ mappingTemplateID);
            _msMappingTemplateRepo.Update(updateMappingTemplate);
            Logger.DebugFormat("DeleteMappingTemplate() - Start Update isActive MS_MappingTemplate ");

            Logger.InfoFormat("DeleteMappingTemplate() Finished.");
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_PSAS_MasterLegalDocument_MappingTemplate)]
        public PagedResultDto<GetMappingTemplateListDto> GetMappingTemplate(GetMappingTemplateInputDto input)
        {
            var queryMappingTemplate = (from A in _msMappingTemplateRepo.GetAll()
                                        join B in _msDocTemplateRepo.GetAll() on A.docTemplateID equals B.Id
                                        join C in _msProjectRepo.GetAll() on A.projectID equals C.Id
                                        join D in _msDocumentPsRepo.GetAll() on A.docID equals D.Id
                                        select new GetMappingTemplateListDto
                                        {
                                            entityID = A.Id,
                                            mappingTemplateID = A.Id,
                                            mappingTemplateCode = _iFilesHelper.ConvertIdToCode(A.Id),
                                            docID = A.docID,
                                            docCode = D.docCode,
                                            docTemplateID = B.Id,
                                            docTemplateName = B.templateName,
                                            projectID = A.projectID,
                                            projectName = C.projectName,
                                            isActive = A.isActive,
                                            isTandaTerima = A.isTandaTerima,
                                            isTandaTerimaFile = (A != null && A.isTandaTerimaFile != null) ?
                                            (A.isTandaTerimaFile == "Default-Surat-Terima.html" ?
                                            getAbsoluteUriWithoutTail() + GetURLWithoutHost((@"Assets\Upload\LegalDocument\TandaTerima\" + @"\" + A.isTandaTerimaFile)) :
                                            getAbsoluteUriWithoutTail() + GetURLWithoutHost((@"Assets\Upload\LegalDocument\TandaTerima\" + B.Id + @"\" + A.isTandaTerimaFile)))
                                            : null,
                                            activeFrom = A.activeFrom,
                                            activeTo = A.activeTo,
                                            creationTime = A.CreationTime.ToString("dd/MM/yyyy"),
                                            creatorUserID = A.CreatorUserId,
                                            period = A.activeTo != null ? A.activeFrom.Date.ToString() + " - " + A.activeTo.Value.Date.ToString() : A.activeFrom.Date.ToString()
                                        });

            var dataCountMapping = queryMappingTemplate.Count();
            List<GetMappingTemplateListDto> getMappingTemplate = new List<GetMappingTemplateListDto>();
            if (dataCountMapping != 0)
            {
                getMappingTemplate = queryMappingTemplate
                                    .OrderBy(input.Sorting)
                                    .PageBy(input)
                                    .ToList();

                foreach (var X in getMappingTemplate)
                {
                    if (X.creatorUserID != null)
                    {
                        var getName = (from A in _contextDemo.Users
                                       where A.Id == X.creatorUserID.GetValueOrDefault()
                                       select A.Name).FirstOrDefault();
                        X.creationBy = getName;
                    }
                }
            }
            var getMappingTemplateFilter = getMappingTemplate.WhereIf(!input.Filter.IsNullOrWhiteSpace(),
                                            u =>
                                                u.docTemplateName.ToLower().Contains(input.Filter.ToLower()) ||
                                                u.projectName.ToLower().Contains(input.Filter.ToLower()) ||
                                                u.docCode.ToLower().Contains(input.Filter.ToLower()) ||
                                                u.mappingTemplateCode.ToLower().Contains(input.Filter.ToLower()) ||
                                                u.activeFrom.ToString().Contains(input.Filter) ||
                                                u.activeTo.ToString().Contains(input.Filter) ||
                                                u.period.Contains(input.Filter.ToLower()) ||
                                                u.creationBy.ToLower().Contains(input.Filter.ToLower())
                                        );
            var dataCount = getMappingTemplateFilter.Count();

            return new PagedResultDto<GetMappingTemplateListDto>(
                dataCount,
                getMappingTemplateFilter.ToList()
                );
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_PSAS_MasterLegalDocument_MappingTemplate_Edit)]
        public void UpdateMappingTemplate(CreateMappingTemplateInputDto input)
        {
            Logger.InfoFormat("UpdateMappingTemplate() - Started.");
            if (input.mappingTemplateID.HasValue)
            {
                Logger.DebugFormat("UpdateMappingTemplate() - Start get MS_MappingTemplate for update. Parameters sent: {0} " +
                    "mappingTemplateID = {1}{0}", Environment.NewLine, input.mappingTemplateID);
                var getMappingTemplate = (from x in _msMappingTemplateRepo.GetAll()
                                          where x.Id == input.mappingTemplateID
                                          select x).FirstOrDefault();
                Logger.DebugFormat("UpdateMappingTemplate() - End get MS_MappingTemplate for update. Result = {0} ", getMappingTemplate);

               

                var getMappingTemplateWithSameDate = (from x in _msMappingTemplateRepo.GetAll()
                                                      where x.activeFrom == input.activeFrom &&
                                                      x.activeTo == input.activeTo
                                                      select x).FirstOrDefault();
                if (getMappingTemplateWithSameDate == null)
                {
                    Logger.DebugFormat("CheckDataInsertIsExist() - Start Checking Data. ");
                    CheckDataUpdateIsExist(input.mappingTemplateID.GetValueOrDefault(), input.projectID, input.docID, input.docTemplateID, input.activeFrom, input.activeTo);
                    Logger.DebugFormat("CheckDataInsertIsExist() - End Checking Data. ");
                }

                var updateMappingTemplate = getMappingTemplate.MapTo<MS_MappingTemplate>();
                updateMappingTemplate.projectID = input.projectID;
                updateMappingTemplate.docID = input.docID;
                updateMappingTemplate.docTemplateID = input.docTemplateID;
                updateMappingTemplate.activeFrom = input.activeFrom;
                updateMappingTemplate.activeTo = input.activeTo;
                updateMappingTemplate.isTandaTerima = input.isTandaTerima;
                updateMappingTemplate.isActive = input.isActive;

                if (input.isTandaTerimaFile == "updated")
                {
                    var imageUrl = uploadFile(input.isTandaTerimaFileNew, @"Temp\Downloads\LegalDocument\TandaTerima\", @"Assets\Upload\LegalDocument\TandaTerima\" + input.docTemplateID + @"\");
                    GetURLWithoutHost(imageUrl, out imageUrl);
                    updateMappingTemplate.isTandaTerimaFile= imageUrl;
                }
                else
                {
                    updateMappingTemplate.isTandaTerimaFile = input.isTandaTerimaFile;
                }

                //if (checkDataUpdateIsExist.isExist)
                //{
                //    if (checkDataUpdateIsExist.mappingTemplateID != null)
                //    {
                //        UpdateDataIsExist(checkDataUpdateIsExist.mappingTemplateID.GetValueOrDefault(), checkDataUpdateIsExist.activeToOldData.GetValueOrDefault());
                //    }

                //    if (!input.activeTo.HasValue)
                //    {
                //        updateMappingTemplate.activeTo = checkDataUpdateIsExist.activeTo;
                //    }
                //}

                Logger.DebugFormat("UpdateMappingTemplate() - Start Update MS_MappingTemplate. Parameters sent: {0} " +
                        "projectID = {1}{0}" +
                        "docID = {2}{0}" +
                        "docTemplateID = {3}{0}" +
                        "activeFrom = {4}{0}" +
                        "activeTo = {5}{0}" +
                        "isTandaTerima = {6}{0}" +
                        "isActive = {7}{0}"
                        , Environment.NewLine, input.projectID, input.docID, input.docTemplateID, input.activeFrom, input.activeTo
                        , input.isTandaTerima, input.isActive);
                _msMappingTemplateRepo.Update(updateMappingTemplate);
                CurrentUnitOfWork.SaveChanges(); //execution saved inside try
            }
            else
            {
                throw new UserFriendlyException("Data not found !");
            }
            Logger.DebugFormat("UpdateMappingTemplate() - End Update MS_MappingTemplate.");
        }

        private void CheckDataUpdateIsExist(int mappingTemplateID, int projectID, int docID, int docTemplateID, DateTime activeFrom, DateTime? activeTo)
        {
            //CheckDataUpdateIsExistListDto returnDataUpdate = new CheckDataUpdateIsExistListDto();

            var GetMappingTemplate = (from A in _msMappingTemplateRepo.GetAll()
                                      join B in _msDocTemplateRepo.GetAll() on A.docTemplateID equals B.Id
                                      join C in _msProjectRepo.GetAll() on A.projectID equals C.Id
                                      join D in _msDocumentPsRepo.GetAll() on A.docID equals D.Id
                                      where A.Id != mappingTemplateID && A.projectID == projectID && A.docID == docID
                                      && A.docTemplateID == docTemplateID
                                      && A.isActive
                                      select new UpdateMappingTemplateInputDto
                                      {
                                          entityID = A.entityID,
                                          isTandaTerima = A.isTandaTerima,
                                          mappingTemplateID = A.Id,
                                          docID = A.docID,
                                          docTemplateID = B.Id,
                                          projectID = A.projectID,
                                          isActive = A.isActive,
                                          activeFrom = A.activeFrom,
                                          activeTo = A.activeTo
                                      })
                                      .OrderByDescending(x => x.activeFrom)
                                      .ThenByDescending(x => x.activeTo)
                                      .ToList();

            if (GetMappingTemplate.Any())
            {
                //Validation

                if (GetMappingTemplate.Any(x => x.activeFrom >= activeFrom))
                {
                    throw new UserFriendlyException("Newer period is exist!");
                }

                if (GetMappingTemplate.Any(x => x.activeTo != null && (x.activeFrom <= activeFrom && x.activeTo >= activeFrom)))
                {
                    throw new UserFriendlyException("Data with such active time period already exists! Please enter an active date outside of an existing period");
                }

                if (activeTo.HasValue && GetMappingTemplate.Any(x => x.activeTo != null && ((x.activeFrom <= activeFrom && x.activeTo >= activeFrom) && (x.activeFrom <= activeTo && x.activeTo >= activeTo))))
                {
                    throw new UserFriendlyException("Data with such active time period already exists! Please enter an active date outside of an existing period");
                }

                ////Return Value
                //if (GetMappingTemplate.Any(x => x.activeTo == null && x.activeFrom < activeFrom && x.isActive))
                //{
                //    var singleData = GetMappingTemplate.Where(x => x.activeTo == null && x.activeFrom < activeFrom && x.isActive).OrderByDescending(x => x.activeFrom).FirstOrDefault();
                //    returnDataUpdate.isExist = true;
                //    returnDataUpdate.mappingTemplateID = singleData.mappingTemplateID;
                //    returnDataUpdate.activeToOldData = activeFrom.AddDays(-1);
                //    returnDataUpdate.isConfirmation = true;
                //}
                //else
                //{
                //    returnDataUpdate.isConfirmation = false;
                //}
            }
            //return returnDataUpdate;
        }

        public void SchedulerUpdateDataExpired()
        {
            Logger.InfoFormat("SchedulerUpdateDataExpired() - Started.");
            var getDataExpired = (from A in _msMappingTemplateRepo.GetAll()
                                  where A.activeTo != null &&
                                  A.activeTo < DateTime.Now &&
                                  A.isActive
                                  select A.Id).ToList();

            foreach (var dataExpired in getDataExpired)
            {
                Logger.InfoFormat("DeleteMappingTemplate() - Started.");
                DeleteMappingTemplate(dataExpired);
                Logger.InfoFormat("DeleteMappingTemplate() - Ended.");
            }
            Logger.InfoFormat("SchedulerUpdateDataExpired() - Ended.");
        }

        private string uploadFile(string filename, string pathTemp, string pathAsset)
        {
            try
            {
                return _iFilesHelper.MoveFilesLegalDoc(filename, pathTemp, pathAsset, null);
            }
            catch (Exception ex)
            {
                Logger.DebugFormat("test() - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Upload Error !", ex.Message);
            }
        }

        private void GetURLWithoutHost(string path, out string finalpath)
        {
            finalpath = path;
            try
            {
                Regex RegexObj = new Regex("[\\w\\W]*([\\/]Assets[\\w\\W\\s]*)");
                if (RegexObj.IsMatch(path))
                {
                    finalpath = RegexObj.Match(path).Groups[1].Value;
                }
            }
            catch (ArgumentException ex)
            {
            }
        }

        private string GetURLWithoutHost(string path)
        {
            string finalpath = path;
            try
            {
                Regex RegexObj = new Regex("[\\w\\W]*([\\/]Assets[\\w\\W\\s]*)");
                if (RegexObj.IsMatch(path))
                {
                    finalpath = RegexObj.Match(path).Groups[1].Value;
                }
            }
            catch (ArgumentException ex)
            {
            }
            return finalpath;
        }

        private string getAbsoluteUriWithoutTail()
        {
            var request = _httpContextAccessor.HttpContext.Request;
            UriBuilder uriBuilder = new UriBuilder();
            uriBuilder.Scheme = request.Scheme;
            uriBuilder.Host = request.Host.ToString();
            var test = uriBuilder.ToString();
            var result = test.Replace("[", "").Replace("]", "");
            int position = result.LastIndexOf('/');
            if (position > -1)
                result = result.Substring(0, result.Length - 1);

            if (request.PathBase != null)
            {
                if (!string.IsNullOrWhiteSpace(request.PathBase.Value))
                {
                    result += request.PathBase.Value;
                }
            }
            return result;
        }
    }
}
