﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using VDI.Demo.PropertySystemDB.LippoMaster;
using VDI.Demo.PSAS.LegalDocument.DocTemplate.Dto;
using VDI.Demo.Files;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using VDI.Demo.Configuration;
using Microsoft.Extensions.Configuration;
using System.IO;
using VDI.Demo.Authorization.Users;
using VDI.Demo.EntityFrameworkCore;
using Abp.AutoMapper;
using Abp.UI;
using VDI.Demo.Files.Dto;
using VDI.Demo.Authorization;
using Abp.Authorization;
using VDI.Demo.PSAS.Dto;
using VDI.Demo.PSAS.Price;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;

namespace VDI.Demo.PSAS.LegalDocument.DocTemplate
{
    //[AbpAuthorize(AppPermissions.Pages_Tenant_PSAS_MasterLegalDocument_MasterTemplate)]
    public class PSASDocTemplateAppService : DemoAppServiceBase, IPSASDocTemplateAppService
    {
        private readonly IRepository<MS_DocumentPS> _msDocumentRepo;
        private readonly IRepository<MS_DocTemplate> _msDocTemplateRepo;
        private readonly IRepository<MS_MappingTemplate> _msMappingTemplateRepo;
        private readonly IFilesHelper _iFilesHelper;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly UserManager _userManager;
        private readonly DemoDbContext _contextDemo;
        private readonly IPSASPriceAppService _ipriceAppService;
        private readonly IRepository<MS_Unit> _msUnitRepo;
        private readonly IRepository<MS_UnitCode> _msUnitCodeRepo;
        private readonly IRepository<MS_Project> _msProjectRepo;

        private readonly IConfigurationRoot _appConfiguration;

        public PSASDocTemplateAppService(
            IRepository<MS_DocTemplate> msDocTemplateRepo,
            IRepository<MS_MappingTemplate> msMappingTemplateRepo,
            IRepository<MS_DocumentPS> msDocumentRepo,
            IFilesHelper iFilesHelper,
            IHttpContextAccessor httpContextAccessor,
            IHostingEnvironment hostingEnvironment,
            UserManager userManager,
            DemoDbContext contextDemo,
            IPSASPriceAppService ipriceAppService,
            IRepository<MS_Unit> msUnitRepo,
            IRepository<MS_UnitCode> msUnitCodeRepo,
            IRepository<MS_Project> msProjectRepo
            )
        {
            _msDocTemplateRepo = msDocTemplateRepo;
            _msMappingTemplateRepo = msMappingTemplateRepo;
            _msDocumentRepo = msDocumentRepo;
            _iFilesHelper = iFilesHelper;
            _httpContextAccessor = httpContextAccessor;
            _hostingEnvironment = hostingEnvironment;
            _userManager = userManager;
            _contextDemo = contextDemo;
            _msUnitRepo = msUnitRepo;
            _msUnitCodeRepo = msUnitCodeRepo;
            _msProjectRepo = msProjectRepo;
            _ipriceAppService = ipriceAppService;

            _appConfiguration = hostingEnvironment.GetAppConfiguration();
        }

        public List<GetDocTemplateListDto> GetDocTemplate()
        {
            var getDocTemplate = (from A in _msDocTemplateRepo.GetAll()
                                  join B in _msDocumentRepo.GetAll() on A.docID equals B.Id
                                  where A.isActive.GetValueOrDefault()
                                  select new GetDocTemplateListDto
                                  {
                                      docTemplateID = A.Id,
                                      docTemplateCode = _iFilesHelper.ConvertIdToCode(A.Id),
                                      docTemplateName = A.templateName,
                                      copyNo = A.copyNo,
                                      linkFileDoc = A.templateFile,
                                      docCode = B.docCode,
                                      creationTime = A.CreationTime.ToString("dd/MM/yyyy"),
                                      creatorUserId = A.CreatorUserId,
                                      isActive = A.isActive.GetValueOrDefault(),
                                      isMaster = A.isMaster.GetValueOrDefault()
                                  }).ToList();

            var getName = (from A in getDocTemplate
                           join B in _contextDemo.Users on A.creatorUserId equals B.Id into BB
                           from B in BB.DefaultIfEmpty()
                           select new GetDocTemplateListDto
                           {
                               docTemplateID = A.docTemplateID,
                               docTemplateCode = A.docTemplateCode,
                               docTemplateName = A.docTemplateName,
                               copyNo = A.copyNo,
                               linkFileDoc = A.linkFileDoc,
                               docCode = A.docCode,
                               creationTime = A.creationTime,
                               creatorUserId = A.creatorUserId,
                               isActive = A.isActive,
                               isMaster = A.isMaster,
                               creationBy = B.Name
                           }).ToList();

            return getName;
        }

        public List<GetDocTemplateListDto> GetDocTemplateByDocID(int docID)
        {
            var getDocTemplate = (from A in _msDocTemplateRepo.GetAll()
                                  join B in _msDocumentRepo.GetAll() on A.docID equals B.Id
                                  where A.docID == docID  && A.isActive.GetValueOrDefault()
                                  select new GetDocTemplateListDto
                                  {
                                      docTemplateID = A.Id,
                                      docTemplateCode = _iFilesHelper.ConvertIdToCode(A.Id),
                                      docTemplateName = A.templateName,
                                      copyNo = A.copyNo,
                                      linkFileDoc = A.templateFile,
                                      isActive = A.isActive.GetValueOrDefault(),
                                      docCode = B.docCode,
                                      creationTime = A.CreationTime.ToString("dd/MM/yyyy"),
                                      creatorUserId = A.CreatorUserId,
                                      isMaster = A.isMaster.GetValueOrDefault()
                                      //creationBy = A.CreatorUserId != null ? UserManager.GetUserByIdAsync(A.CreatorUserId.GetValueOrDefault()).Result.UserName.ToString() : null
                                  }).ToList();
            if (getDocTemplate.Any())
            {
                foreach (var docTemplate in getDocTemplate)
                {
                    var getName = (from A in _contextDemo.Users
                                   where A.Id == docTemplate.creatorUserId
                                   select A.Name).FirstOrDefault();

                    if (docTemplate.linkFileDoc != null)
                    {
                        var webRootPath = _hostingEnvironment.WebRootPath;
                        var path = (webRootPath + docTemplate.linkFileDoc).Replace("/", @"\");
                        var getDocFileTemplate = _iFilesHelper.GetBase64FileByPhysicalPath(docTemplate.linkFileDoc);
                        var getDocFile = (from A in getDocFileTemplate
                                          select new LinkPathListDto
                                          {
                                              linkFile = A.linkFile,
                                              filename = A.filename
                                          }).ToList();

                        docTemplate.path = getDocFile;
                        docTemplate.filePath = getDocFileTemplate.Select(x => path + @"\" + x).ToList();
                    }

                    docTemplate.creationBy = getName;
                }

                return getDocTemplate;
            }
            else
            {
                var docCode = (from A in _msDocumentRepo.GetAll()
                               where A.Id == docID
                               select A.docCode).FirstOrDefault();

                throw new UserFriendlyException("Doc Template with DocCode: " + docCode + "is not found.");
            }
        }

        public GetDocTemplateListDto GetDocTemplateByDocTemplateID(int docTemplateID)
        {
            var getDocTemplate = (from A in _msDocTemplateRepo.GetAll()
                                  join B in _msDocumentRepo.GetAll() on A.docID equals B.Id
                                  where A.Id == docTemplateID && A.isActive.GetValueOrDefault()
                                  select new GetDocTemplateListDto
                                  {
                                      docTemplateID = A.Id,
                                      docTemplateCode = _iFilesHelper.ConvertIdToCode(A.Id),
                                      docTemplateName = A.templateName,
                                      copyNo = A.copyNo,
                                      linkFileDoc = A.templateFile,
                                      isActive = A.isActive.GetValueOrDefault(),
                                      docCode = B.docCode,
                                      creationTime = A.CreationTime.ToString("dd/MM/yyyy"),
                                      creatorUserId = A.CreatorUserId,
                                      isMaster = A.isMaster.GetValueOrDefault()
                                      //creationBy = A.CreatorUserId != null ? UserManager.GetUserByIdAsync(A.CreatorUserId.GetValueOrDefault()).Result.UserName.ToString() : null
                                  }).FirstOrDefault();

            var getName = (from A in _contextDemo.Users
                           where A.Id == getDocTemplate.creatorUserId
                           select A.Name).FirstOrDefault();

            if (getDocTemplate.linkFileDoc != null)
            {
                var webRootPath = _hostingEnvironment.WebRootPath;
                var path = (webRootPath + getDocTemplate.linkFileDoc).Replace("/", @"\");
                var getDocFileTemplate = _iFilesHelper.GetBase64FileByPhysicalPath(getDocTemplate.linkFileDoc);
                var getDocFile = (from A in getDocFileTemplate
                                  select new LinkPathListDto
                                  {
                                      linkFile = A.linkFile,
                                      filename = A.filename,
                                      linkServerFile = A.linkServerFile,
                                      filePhysicalPath = A.filePhysicalPath
                                  }).ToList();
                //getDocTemplate.linkPath = getDocFileTemplate.Select(x => (getAbsoluteUriWithoutTail() + getDocTemplate.linkFileDoc + "/" + GetURLWithoutHost(x)).Replace(@"\", "/")).ToList();
                getDocTemplate.path = getDocFile;
                getDocTemplate.filePath = getDocFileTemplate.Select(x => path + @"\" + x).ToList();
            }

            getDocTemplate.creationBy = getName;

            return getDocTemplate;
        }

        private void GetURLWithoutHost(string path, out string finalpath)
        {
            finalpath = path;
            try
            {
                Regex RegexObj = new Regex("[\\w\\W]*([\\/]Assets[\\w\\W\\s]*)");
                if (RegexObj.IsMatch(path))
                {
                    finalpath = RegexObj.Match(path).Groups[1].Value;
                }
            }
            catch (ArgumentException ex)
            {
            }
        }

        private string GetURLWithoutHost(string path)
        {
            string finalpath = path;
            try
            {
                Regex RegexObj = new Regex("[\\w\\W]*([\\/]Assets[\\w\\W\\s]*)");
                if (RegexObj.IsMatch(path))
                {
                    finalpath = RegexObj.Match(path).Groups[1].Value;
                }
            }
            catch (ArgumentException ex)
            {
            }
            return finalpath;
        }

        private string getAbsoluteUriWithoutTail()
        {
            var request = _httpContextAccessor.HttpContext.Request;
            UriBuilder uriBuilder = new UriBuilder();
            uriBuilder.Scheme = request.Scheme;
            uriBuilder.Host = request.Host.ToString();
            var test = uriBuilder.ToString();
            var result = test.Replace("[", "").Replace("]", "");
            int position = result.LastIndexOf('/');
            if (position > -1)
                result = result.Substring(0, result.Length - 1);

            if (request.PathBase != null)
            {
                if (!string.IsNullOrWhiteSpace(request.PathBase.Value))
                {
                    result += request.PathBase.Value;
                }
            }
            return result;
        }

        //[AbpAuthorize(AppPermissions.Pages_Tenant_PSAS_MasterLegalDocument_MasterTemplate_Delete)]
        public void UpdateIsActiveDocTemplate(int docTemplateID)
        {
            Logger.InfoFormat("UpdateIsActiveDocTemplate() Started.");
            var getDocTemplate = (from A in _msDocTemplateRepo.GetAll()
                                  where A.Id == docTemplateID
                                  select A).FirstOrDefault();

            var getDocTemplateInMapping = (from A in _msMappingTemplateRepo.GetAll()
                                          where A.docTemplateID == getDocTemplate.Id
                                          select A).ToList();
            if (getDocTemplateInMapping.Any())
            {
                throw new UserFriendlyException("Template already used by another mapping!");
            }
            else
            {
                var updateDocTemplate = getDocTemplate.MapTo<MS_DocTemplate>();
                updateDocTemplate.isActive = false;

                Logger.DebugFormat("UpdateIsActiveDocTemplate() - Update isActive MS_DocTemplate. Parameters sent: {0} " +
                    "   docTemplateID = {1}{0}"
                    , Environment.NewLine, docTemplateID);
                _msDocTemplateRepo.Update(updateDocTemplate);
            }
            Logger.InfoFormat("UpdateIsActiveDocTemplate() Ended.");
        }

        private void UpdateIsMasterDocTemplate(int docTemplateID)
        {
            Logger.InfoFormat("UpdateIsMasterDocTemplate() Started.");
            var getDocTemplate = (from A in _msDocTemplateRepo.GetAll()
                                  where A.Id == docTemplateID
                                  select A).FirstOrDefault();
            var updateDocTemplate = getDocTemplate.MapTo<MS_DocTemplate>();
            updateDocTemplate.isMaster = false;

            Logger.DebugFormat("UpdateIsMasterDocTemplate() - Update master MS_DocTemplate. Parameters sent: {0} " +
                "   docTemplateID = {1}{0}"
                , Environment.NewLine, docTemplateID);
            _msDocTemplateRepo.Update(updateDocTemplate);

            Logger.InfoFormat("UpdateIsMasterDocTemplate() Ended.");
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_PSAS_MasterLegalDocument_MasterTemplate_Create)]
        public void CreateDocTemplate(CreateDocTemplateInputDto input)
        {
            Logger.InfoFormat("CreateDocTemplate() Started.");

            Logger.InfoFormat("checkDocTemplate() Check MS_DocTemplate is there any data with DocID: " + input.docID);
            var checkDocTemplate = (from A in _msDocTemplateRepo.GetAll()
                                    where A.isActive.GetValueOrDefault() &&
                                    A.entityID == 1 &&
                                    A.docID == input.docID &&
                                    A.templateName == input.templateName
                                    select A);

            if (checkDocTemplate.Any())
            {
                throw new UserFriendlyException("Doc Id: "+input.docID+" With Template Name:" + input.templateName + " is already exist.");
            }

            var checkIsMaster = CheckIsMasterDocID(input.docID, input.isMaster);
            if (checkIsMaster.isMaster)
            {
                UpdateIsMasterDocTemplate(checkIsMaster.docTemplateID);
            }

            Logger.InfoFormat("checkDocTemplate() End Check MS_DocTemplate.");

            var param = new MS_DocTemplate
            {
                entityID = 1,
                isActive = true,
                isMaster = input.isMaster,
                docID = input.docID,
                templateName = input.templateName,
                copyNo = input.copyNo
            };

            if (input.templateFileDto.Any())
            {
                var dateTimeFormat = DateTime.Now.ToString().Replace("/", "").Replace(":","").Replace(" ", "_");
                var newLinkPath = @"Assets\Upload\LegalDocument\MasterTemplate\" + input.docID + "-" + dateTimeFormat + "-" + input.templateName.Replace(" ", "_") + @"\";
                Logger.Info("New Path - " + newLinkPath);
                foreach (var file in input.templateFileDto)
                {
                    if (file.templateFile == "KP.html")
                    {
                        uploadKP(file.templateFile, @"Assets\Upload\LegalDocument\MasterTemplate\", newLinkPath, file.orderNumber);
                    }
                    else if (file.templateFile == "FileKadaster.html")
                    {
                        uploadKP(file.templateFile, @"Assets\Upload\LegalDocument\MasterTemplate\", newLinkPath, file.orderNumber);
                    }
                    else
                    {
                        uploadFile(file.templateFile, @"Temp\Downloads\LegalDocument\MasterTemplate\", newLinkPath, file.orderNumber);
                    }
                }

                param.templateFile = "/" + newLinkPath.Replace(@"\", "/").Remove(newLinkPath.Length - 1);
                Logger.InfoFormat("Link template: " + param.templateFile);
            }

            Logger.InfoFormat("Start insert MS_DocTemplate.");
            _msDocTemplateRepo.Insert(param);
            Logger.InfoFormat("End insert MS_DocTemplate.");
            Logger.InfoFormat("CreateDocTemplate() Ended.");
        }

        private string uploadFile(string filename, string pathTemp, string pathAsset, int orderNumber)
        {
            try
            {
                return _iFilesHelper.MoveFilesLegalDoc(filename, pathTemp, pathAsset, orderNumber);
            }
            catch (Exception ex)
            {
                Logger.DebugFormat("test() - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Upload Error !", ex.Message);
            }
        }

        private string uploadKP(string filename, string pathTemp, string pathAsset, int orderNumber)
        {
            try
            {
                return _iFilesHelper.CopyKPFile(filename, pathTemp, pathAsset, orderNumber);
            }
            catch (Exception ex)
            {
                Logger.DebugFormat("test() - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Upload Error !", ex.Message);
            }
        }

        public CheckIsMasterDocIdListDto CheckIsMasterDocID(int docID, bool isMaster)
        {
            CheckIsMasterDocIdListDto returnIsMaster = new CheckIsMasterDocIdListDto();

            if (isMaster)
            {
                var checkMsDocTemplate = (from A in _msDocTemplateRepo.GetAll()
                                          where A.docID == docID &&
                                          A.isMaster.GetValueOrDefault()
                                          select A).FirstOrDefault();
                if (checkMsDocTemplate != null)
                {
                    returnIsMaster = new CheckIsMasterDocIdListDto
                    {
                        isMaster = true,
                        docTemplateID = checkMsDocTemplate.Id
                    };

                    
                }
                else
                {
                    returnIsMaster = new CheckIsMasterDocIdListDto
                    {
                        isMaster = false,
                        docTemplateID = 0
                    };
                }
            }
            else
            {
                returnIsMaster = new CheckIsMasterDocIdListDto
                {
                    isMaster = false,
                    docTemplateID = 0
                };
            }

            return returnIsMaster;
        }

        public bool CheckIsMaster(int docID)
        {
            bool isMaster;
            var getIsMaster = (from x in _msDocTemplateRepo.GetAll()
                               where x.docID == docID 
                               && x.entityID == 1
                               && x.isMaster.GetValueOrDefault() 
                               && x.isActive.GetValueOrDefault()
                               select x).FirstOrDefault();
            if (getIsMaster != null)
            {
                isMaster = true;
            }
            else
            {
                isMaster = false;
            }
            return isMaster;
        }
    }
}
