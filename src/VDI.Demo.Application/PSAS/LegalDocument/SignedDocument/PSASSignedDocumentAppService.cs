﻿using Abp.Domain.Repositories;
using Abp.UI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using VDI.Demo.Configuration;
using VDI.Demo.Files;
using VDI.Demo.PropertySystemDB.LippoMaster;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;
using VDI.Demo.PSAS.LegalDocument.SignedDocument.Dto;
using VDI.Demo.PSAS.Price;
using Abp.AutoMapper;
using System.Text.RegularExpressions;
using Abp.Authorization;
using VDI.Demo.Authorization;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;
using VDI.Demo.NewCommDB;

namespace VDI.Demo.PSAS.LegalDocument.SignedDocument
{
    //[AbpAuthorize(AppPermissions.Pages_Tenant_PSAS_SignedDocument)]
    public class PSASSignedDocumentAppService : DemoAppServiceBase, IPSASSignedDocumentAppService
    {
        private readonly IRepository<MS_KuasaDireksi> _msKuasaDireksiRepo;
        private readonly IRepository<MS_KuasaDireksiPeople> _msKuasaDireksiPeopleRepo;
        private readonly IRepository<MS_MappingTemplate> _msMappingTemplateRepo;
        private readonly IRepository<MS_DocumentPS> _msDocumentPsRepo;
        private readonly IRepository<MS_DocTemplate> _msDocTemplateRepo;
        private readonly IRepository<MS_Project> _msProjectRepo;
        private readonly IFilesHelper _iFilesHelper;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IRepository<TR_BookingHeader> _trBookingHeaderRepo;
        private readonly IRepository<TR_BookingDetail> _trBookingDetailRepo;
        private readonly IRepository<TR_BookingDocument> _trBookingDocumentRepo;
        private readonly IRepository<MS_Company> _msCompanyRepo;       
        private readonly IRepository<DocNo_Counter> _docNoCounterRepo;
        private readonly IPSASPriceAppService _ipriceAppService;
        private readonly NewCommDbContext _newCommDbContext;
        private readonly IRepository<MS_Unit> _msUnitRepo;
        private readonly IRepository<MS_UnitCode> _msUnitCodeRepo;
        private readonly IRepository<TR_SoldUnit, string> _trSoldUnit;
        private readonly PropertySystemDbContext _propertySystemDB;
        private readonly IConfigurationRoot _appConfiguration;

        public PSASSignedDocumentAppService(
            IRepository<MS_KuasaDireksi> msKuasaDireksiRepo,
            IRepository<MS_KuasaDireksiPeople> msKuasaDireksiPeopleRepo,
            IRepository<MS_MappingTemplate> msMappingTemplateRepo,
            IRepository<MS_DocumentPS> msDocumentPsRepo,
            IRepository<MS_DocTemplate> msDocTemplateRepo,
            IRepository<MS_Project> msProjectRepo,
            IFilesHelper iFilesHelper,
            IHttpContextAccessor httpContextAccessor,
            IHostingEnvironment hostingEnvironment,
            IRepository<TR_BookingHeader> trBookingHeaderRepo,
            IRepository<TR_BookingDetail> trBookingDetailRepo,
            IRepository<TR_BookingDocument> trBookingDocumentRepo,
            IRepository<MS_Company> msCompanyRepo,
            IRepository<DocNo_Counter> docNoCounterRepo,
            IPSASPriceAppService ipriceAppService,
            IRepository<TR_BookingDetailSchedule> trBookingDetailScheduleRepo,
            NewCommDbContext newCommDbContext,
            IRepository<MS_Unit> msUnitRepo,
            IRepository<MS_UnitCode> msUnitCodeRepo,
            IRepository<TR_SoldUnit, string> trSoldUnit,
            PropertySystemDbContext propertySystemDB
            )
        {
            _msKuasaDireksiRepo = msKuasaDireksiRepo;
            _msKuasaDireksiPeopleRepo = msKuasaDireksiPeopleRepo;
            _trBookingDocumentRepo = trBookingDocumentRepo;
            _trBookingHeaderRepo = trBookingHeaderRepo;
            _trBookingDetailRepo = trBookingDetailRepo;
            _ipriceAppService = ipriceAppService;
            _msMappingTemplateRepo = msMappingTemplateRepo;
            _msDocumentPsRepo = msDocumentPsRepo;
            _msDocTemplateRepo = msDocTemplateRepo;
            _msProjectRepo = msProjectRepo;
            _msCompanyRepo = msCompanyRepo;
            _docNoCounterRepo = docNoCounterRepo;
            _iFilesHelper = iFilesHelper;
            _httpContextAccessor = httpContextAccessor;
            _hostingEnvironment = hostingEnvironment;
            _newCommDbContext = newCommDbContext;
            _msUnitRepo = msUnitRepo;
            _msUnitCodeRepo = msUnitCodeRepo;
            _trSoldUnit = trSoldUnit;
            _propertySystemDB = propertySystemDB;
            _appConfiguration = hostingEnvironment.GetAppConfiguration();
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_PSAS_SignedDocument_Create)]
        public void UploadSignedDocument(UploadSignedDocumentInputDto input)
        {
            var getBookingDocument = (from x in _trBookingDocumentRepo.GetAll()
                                      where x.Id == input.trBookingDocID
                                      select x).FirstOrDefault();
            var updateBookingDocument = getBookingDocument.MapTo<TR_BookingDocument>();

            //var getBookCode = (from x in _trBookingHeaderRepo.GetAll()
            //                   where x.Id == getBookingDocument.bookingHeaderID
            //                   select x).FirstOrDefault();

            //var getUnit = (from x in _msUnitRepo.GetAll()
            //               where x.Id == getBookCode.unitID
            //               select x).FirstOrDefault();

            //var getUnitCode = (from x in _msUnitCodeRepo.GetAll()
            //                   where x.Id == getUnit.unitCodeID
            //                   select x).FirstOrDefault();

            //var getSoldUnit = (from x in _newCommDbContext.TR_SoldUnit
            //                   where x.roadCode == getUnitCode.unitCode &&
            //                   x.unitNo == getUnit.unitNo
            //                   select x).FirstOrDefault();
                             

            if (getBookingDocument != null)
            {
                var getBookingCode = (from x in _trBookingHeaderRepo.GetAll()
                                      where x.Id == getBookingDocument.bookingHeaderID
                                      select x).FirstOrDefault();
                if (getBookingCode != null)
                {
                    if (getBookingDocument.signedDocumentFile != null)
                    {
                        throw new UserFriendlyException("You already have file uploaded.");
                    }
                    else
                    {
                        if (input.signedDocumentFile != null && input.signedDocumentFile != "")
                        {
                            var fileSignatured = uploadFile(input.signedDocumentFile, @"Temp\Downloads\LegalDocument\PPPU\SignedDocument", @"Assets\Upload\LegalDocument\PPPU\" + getBookingCode.bookCode + @"\");
                            GetURLWithoutHost(fileSignatured, out fileSignatured);
                            updateBookingDocument.signedDocumentFile = fileSignatured;
                            updateBookingDocument.signatureDate = input.signatureDate;
                            updateBookingDocument.signedDocumentDate = DateTime.Now;

                            _trBookingDocumentRepo.Update(updateBookingDocument);

                            //if(getSoldUnit != null)
                            //{
                            //    var updateSoldUnit = getSoldUnit.MapTo<TR_SoldUnit>();
                            //    updateSoldUnit.PPJBDate = updateBookingDocument.signedDocumentDate;
                            //    _newCommDbContext.TR_SoldUnit.Update(updateSoldUnit);
                            //    _newCommDbContext.SaveChanges();
                            //}
                        }
                        else
                        {
                            throw new UserFriendlyException("Cannot upload empty file.");
                        }
                    }
                }
                else
                {
                    throw new UserFriendlyException("BookCode is not valid.");
                }
            }
        }

        public List<GetDocNoDropdownListDto> GetDocNoDropdown(string docNo)
        {
            var getBookingDoc = (from A in _trBookingDocumentRepo.GetAll()
                                 join B in _msDocumentPsRepo.GetAll() on A.docID equals B.Id
                                 select new GetDocNoDropdownListDto
                                 {
                                     docNo = A.docNo,
                                     docDate = A.docDate.ToString("dd/MM/yyyy"),
                                     trBookingDocID = A.Id,
                                     docType = B.docName
                                 })
                                 .Where(x => x.docNo.Contains(docNo))
                                 .ToList();

            return getBookingDoc;
        }

        public GetDetailBookingDocumentListDto GetDetailBookingDocument(int bookingDocumentID)
        {
            var getBookingDocumentData = (from x in _trBookingDocumentRepo.GetAll()
                                          join a in _msDocumentPsRepo.GetAll() on x.docID equals a.Id
                                          where x.Id == bookingDocumentID
                                          select new GetDetailBookingDocumentListDto
                                          {
                                              bookingDocumentId = x.Id,
                                              docCode = a.docCode,
                                              docName = a.docName,
                                              docNo = x.docNo,
                                              docDate = x.docDate,
                                              remarks = x.remarks,
                                              fileTandaTerima = (x != null && x.tandaTerimaFile != null) ? getAbsoluteUriWithoutTail() + GetURLWithoutHost(x.tandaTerimaFile) : null,
                                              signatureDate = x.signatureDate != null ? x.signatureDate.Value.ToString("dd/MM/yyyy") : null,
                                              signatureFile = (x != null && x.signedDocumentFile != null) ? getAbsoluteUriWithoutTail() + GetURLWithoutHost(x.signedDocumentFile) : null,
                                              signedDocumentDate = x.signedDocumentDate != null ? x.signedDocumentDate.Value.ToString("dd/MM/yyyy") : null
                                          })
                                          .FirstOrDefault();

            return getBookingDocumentData;
        }

        private string uploadFile(string filename, string pathTemp, string pathAsset)
        {
            try
            {
                return _iFilesHelper.MoveFilesLegalDoc(filename, pathTemp, pathAsset, null);
            }
            catch (Exception ex)
            {
                Logger.DebugFormat("test() - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Upload Error !", ex.Message);
            }
        }

        private void GetURLWithoutHost(string path, out string finalpath)
        {
            finalpath = path;
            try
            {
                Regex RegexObj = new Regex("[\\w\\W]*([\\/]Assets[\\w\\W\\s]*)");
                if (RegexObj.IsMatch(path))
                {
                    finalpath = RegexObj.Match(path).Groups[1].Value;
                }
            }
            catch (ArgumentException ex)
            {
            }
        }

        private string GetURLWithoutHost(string path)
        {
            string finalpath = path;
            try
            {
                Regex RegexObj = new Regex("[\\w\\W]*([\\/]Assets[\\w\\W\\s]*)");
                if (RegexObj.IsMatch(path))
                {
                    finalpath = RegexObj.Match(path).Groups[1].Value;
                }
            }
            catch (ArgumentException ex)
            {
            }
            return finalpath;
        }

        private string getAbsoluteUriWithoutTail()
        {
            var request = _httpContextAccessor.HttpContext.Request;
            UriBuilder uriBuilder = new UriBuilder();
            uriBuilder.Scheme = request.Scheme;
            uriBuilder.Host = request.Host.ToString();
            var test = uriBuilder.ToString();
            var result = test.Replace("[", "").Replace("]", "");
            int position = result.LastIndexOf('/');
            if (position > -1)
                result = result.Substring(0, result.Length - 1);

            if (request.PathBase != null)
            {
                if (!string.IsNullOrWhiteSpace(request.PathBase.Value))
                {
                    result += request.PathBase.Value;
                }
            }
            return result;
        }

        public List<GetDetailBookingDocumentListDto> GetBookingDocumentWithSignature()
        {
            var getBookingDocumentData = (from x in _trBookingDocumentRepo.GetAll()
                                          join a in _msDocumentPsRepo.GetAll() on x.docID equals a.Id
                                          where x.signedDocumentFile != null && x.signedDocumentDate != null && x.signatureDate != null
                                          select new GetDetailBookingDocumentListDto
                                          {
                                              bookingDocumentId = x.Id,
                                              docCode = a.docCode,
                                              docName = a.docName,
                                              docNo = x.docNo,
                                              docDate = x.docDate,
                                              remarks = x.remarks,
                                              fileTandaTerima = (x != null && x.tandaTerimaFile != null) ? getAbsoluteUriWithoutTail() + GetURLWithoutHost(x.tandaTerimaFile) : null,
                                              signatureDate = x.signatureDate != null ? x.signatureDate.Value.ToString("dd/MM/yyyy") : null,
                                              signatureFile = (x != null && x.signedDocumentFile != null) ? getAbsoluteUriWithoutTail() + GetURLWithoutHost(x.signedDocumentFile) : null,
                                              signedDocumentDate = x.signedDocumentDate != null ? x.signedDocumentDate.Value.ToString("dd/MM/yyyy") : null
                                          })
                                          .ToList();

            return getBookingDocumentData;
        }
    }
}
