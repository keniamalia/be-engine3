﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Abp.Extensions;
using Abp.Linq.Extensions;
using VDI.Demo.Files;
using VDI.Demo.PropertySystemDB.LippoMaster;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;
using VDI.Demo.PSAS.LegalDocument.MappingKadaster.Dto;
using Abp.AutoMapper;
using Abp.UI;
using Abp.Application.Services.Dto;

using VDI.Demo.PropertySystemDB.MasterPlan.Project;
using Abp.Collections.Extensions;
using System.Linq.Dynamic.Core;
using Abp.Authorization;
using VDI.Demo.Authorization;
using Newtonsoft.Json.Linq;
using System.IO;

namespace VDI.Demo.PSAS.LegalDocument.MappingKadaster
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_PSAS_LegalDocument_MasterKadaster)]
    public class PSASMappingKadasterAppService : DemoAppServiceBase, IPSASMappingKadasterAppService
    {
        private readonly IRepository<MS_MappingKadaster> _msMappingKadasterRepo;
        private readonly IRepository<MS_MappingTemplate> _msMappingTemplateRepo;
        private readonly IRepository<MS_Unit> _msUnitRepo;
        private readonly IRepository<MS_Project> _msProjectRepo;
        private readonly IRepository<MS_Category> _msCategoryRepo;
        private readonly IRepository<MS_UnitCode> _msUnitCodeRepo;
        private readonly IRepository<MS_Cluster> _msClusterRepo;
        private readonly IFilesHelper _iFilesHelper;

        public PSASMappingKadasterAppService(
            IRepository<MS_MappingKadaster> msMappingKadasterRepo,
            IRepository<MS_MappingTemplate> msMappingTemplateRepo,
            IRepository<MS_Unit> msUnitRepo,
            IRepository<MS_Project> msProjectRepo,
            IRepository<MS_Category> msCategoryRepo,
            IRepository<MS_UnitCode> msUnitCodeRepo,
            IRepository<MS_Cluster> msClusterRepo,
            IFilesHelper iFilesHelper
            )
        {
            _msMappingKadasterRepo = msMappingKadasterRepo;
            _msMappingTemplateRepo = msMappingTemplateRepo;
            _msCategoryRepo = msCategoryRepo;
            _msUnitRepo = msUnitRepo;
            _msProjectRepo = msProjectRepo;
            _iFilesHelper = iFilesHelper;
            _msUnitCodeRepo = msUnitCodeRepo;
            _msClusterRepo = msClusterRepo;
        }

        public bool CheckCategoryIsExist(int projectID, int categoryID)
        {
            Logger.InfoFormat("CheckCategoryisExist() with projectID :" + projectID + " and categoryID:" + categoryID + " Started.");

            Logger.InfoFormat("CheckCategoryisExist() start to check projectID and categoryID");
            var checkMsMappingKadaster = (from A in _msMappingKadasterRepo.GetAll()
                                          where A.projectID == projectID && A.categoryID == categoryID
                                          && A.isActive
                                          select A);
            Logger.InfoFormat("CheckCategoryisExist() start to check projectID and categoryID");

            var isExist = checkMsMappingKadaster.Any();
            if (isExist)
            {
                throw new UserFriendlyException("Project and Category you choose already mapped");
            }
            Logger.InfoFormat("CheckCategoryisExist() End.");
            return isExist;
        }

        public void UpdateMappingKadasterByCode(CreateMappingKadasterInputDto input)
        {
            Logger.InfoFormat("UpdateMappingKadasterByCode() Started.");

            foreach(var unit in input.codeKadaster)
            {
                var getUnitExist = (from y in _msMappingKadasterRepo.GetAll()
                                    where y.unitID == unit.unitID
                                    && y.isActive
                                    select y).ToList();
                if (getUnitExist.Any())
                {
                    var getKadasterByUnitNo = (from x in _msMappingKadasterRepo.GetAll()
                                               where x.unitID == unit.unitID
                                               && x.isActive
                                               select x).FirstOrDefault();
                    _msMappingKadasterRepo.Delete(getKadasterByUnitNo.Id);

                    Logger.DebugFormat("UpdateMappingKadasterByCode() - delete old Kadaster" +
                          "   unitID = {1}{0}"
                           , Environment.NewLine, getKadasterByUnitNo.unitID);
                }
            }           

            foreach (var unit in input.codeKadaster)
            {
                var paramPerCode = new MS_MappingKadaster
                {
                    projectID = input.projectID,
                    categoryID = input.categoryID,
                    kadasterLocation = input.url,
                    isGeneratedBySystem = input.isGenerateSystem,
                    kadasterCode = unit.kadasterCodeName,
                    unitID = unit.unitID,
                    isActive = true
                };
                
                Logger.DebugFormat("UpdateMappingKadasterByCode() - Insert Updated Kadaster" +
                       "   projectID = {1}{0}" +
                       "   categoryID = {2}{0}" +
                       "   kadasterLocation = {3}{0}" +
                       "   isGeneratedBySystem = {4}{0}" +
                       "   kadasterCode = {5}{0}" +
                       "   unitID = {6}{0}" +
                       "   isActive = {7}{0}"
                       , Environment.NewLine, input.projectID, input.categoryID,
                       input.url, input.isGenerateSystem, unit.kadasterCodeName, unit.unitID, true);

                _msMappingKadasterRepo.Insert(paramPerCode);
            }

            Logger.InfoFormat("UpdateMappingKadasterByCode() End.");
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_PSAS_LegalDocument_MasterKadaster_Create)]
        public void CreateMappingKadaster(CreateMappingKadasterInputDto input)
        {
            Logger.InfoFormat("CreateMappingKadaster() Started.");

            //Logger.InfoFormat("CreateMappingKadaster() check category started.");
            ////var isExist = CheckCategoryIsExist(input.projectID, input.categoryID);
            //Logger.InfoFormat("CreateMappingKadaster() check category end.");

            if (input.isGenerateSystem)
            {
                Logger.InfoFormat("CreateMappingKadaster() choose isGeneratedBySytem.");
                //Highrise
                if (input.codeKadaster != null)
                {
                    Logger.InfoFormat("CreateMappingKadaster() choose byCode.");
                    //By Code
                    foreach (var inputPerCodeKadaster in input.codeKadaster)
                    {
                        var paramPerCode = new MS_MappingKadaster
                        {
                            projectID = input.projectID,
                            categoryID = input.categoryID,
                            kadasterLocation = input.url,
                            isGeneratedBySystem = input.isGenerateSystem,
                            kadasterCode = inputPerCodeKadaster.kadasterCodeName,
                            unitID = inputPerCodeKadaster.unitID,
                            isActive = true
                        };
                        Logger.DebugFormat("CreateMappingKadaster() - Start Insert MS_MappingKadaster" +
                        "   projectID = {1}{0}" +
                        "   categoryID = {2}{0}" +
                        "   kadasterLocation = {3}{0}" +
                        "   isGeneratedBySystem = {4}{0}" +
                        "   kadasterCode = {5}{0}" +
                        "   unitID = {6}{0}" +
                        "   isActive = {7}{0}"
                        , Environment.NewLine, input.projectID, input.categoryID,
                        input.url, input.isGenerateSystem, inputPerCodeKadaster.kadasterCodeName, inputPerCodeKadaster.unitID, true);
                        _msMappingKadasterRepo.Insert(paramPerCode);
                        Logger.DebugFormat("CreateMappingKadaster() - End Insert MS_MappingKadaster.");
                    }
                }
                else
                {
                    Logger.InfoFormat("CreateMappingKadaster() choose byUnit.");
                    var paramByUnit = new MS_MappingKadaster
                    {
                        projectID = input.projectID,
                        categoryID = input.categoryID,
                        kadasterLocation = input.url,
                        isGeneratedBySystem = input.isGenerateSystem,
                        isActive = true
                    };
                    Logger.DebugFormat("CreateMappingKadaster() - Start Insert MS_MappingKadaster" +
                            "   projectID = {1}{0}" +
                            "   categoryID = {2}{0}" +
                            "   kadasterLocation = {3}{0}" +
                            "   isGeneratedBySystem = {4}{0}" +
                            "   isActive = {7}{0}"
                    , Environment.NewLine, input.projectID, input.categoryID,
                    input.url, input.isGenerateSystem, true);
                    _msMappingKadasterRepo.Insert(paramByUnit);
                    Logger.DebugFormat("CreateMappingKadaster() - End Insert MS_MappingKadaster.");
                }
            }
            else
            {
                Logger.InfoFormat("CreateMappingKadaster() choose noMappingByUnit.");
                //NoMapping
                var paramNoMapping = new MS_MappingKadaster
                {
                    projectID = input.projectID,
                    categoryID = input.categoryID,
                    kadasterLocation = input.url,
                    isGeneratedBySystem = input.isGenerateSystem,
                    isActive = true
                };

                Logger.DebugFormat("CreateMappingKadaster() - Start Insert MS_MappingKadaster" +
                            "   projectID = {1}{0}" +
                            "   categoryID = {2}{0}" +
                            "   kadasterLocation = {3}{0}" +
                            "   isGeneratedBySystem = {4}{0}" +
                            "   isActive = {7}{0}"
                , Environment.NewLine, input.projectID, input.categoryID,
                input.url, input.isGenerateSystem, true);
                _msMappingKadasterRepo.Insert(paramNoMapping);
            }

        }

        public List<GetDropdownCategoryByUnitListDto> GetDropdownCategoryByProjectUnit(int projectID)
        {
            Logger.DebugFormat("GetDropdownCategoryByProjectUnit() - Start with " + "projectID = {1}{0}"
                , Environment.NewLine, projectID);

            var getUnit = (from A in _msUnitRepo.GetAll()
                           where A.projectID == projectID
                           group A by A.categoryID
                           into G
                           select G.Key).ToList();



            var getCategory = (from A in getUnit
                               join B in _msCategoryRepo.GetAll() on A equals B.Id
                               select new GetDropdownCategoryByUnitListDto
                               {
                                   categoryCode = B.categoryCode,
                                   categoryName = B.categoryName,
                                   categoryID = A,
                                   kadasterType = B.kadasterType                                
                               }).ToList();

            Logger.DebugFormat("GetDropdownCategoryByProjectUnit() - End");
            return getCategory;
        }

        public PagedResultDto<GetMappingKadasterListDto> GetMappingKadaster(ListOfMappingKadasterInputDto input)
        {
            Logger.InfoFormat("GetMappingKadaster() start");
            
            var queryMappingKadaster = (from A in _msMappingKadasterRepo.GetAll()
                                        join B in _msProjectRepo.GetAll() on A.projectID equals B.Id
                                        join C in _msCategoryRepo.GetAll() on A.categoryID equals C.Id
                                        select new GetMappingKadasterListDto
                                        {
                                            projectID = B.Id,
                                            projectName = B.projectName,
                                            categoryID = C.Id,
                                            categoryName = C.categoryName,
                                            url = A.kadasterLocation,
                                            isActive = A.isActive,
                                            criteriaKadaster = A.isGeneratedBySystem ? A.kadasterCode != null ? "By Code" : "By Unit" : "No Mapping By Unit",
                                            isGenerateBySystem = A.isGeneratedBySystem
                                        })
                                        .Distinct()
                                        .WhereIf(!input.Filter.IsNullOrWhiteSpace(),
                                            x =>
                                                x.projectName.ToLower().Contains(input.Filter.ToLower()) ||
                                                x.categoryName.ToLower().Contains(input.Filter.ToLower()) ||
                                                x.url.ToLower().Contains(input.Filter.ToLower()) ||
                                                x.criteriaKadaster.ToLower().Contains(input.Filter.ToLower())
                                         );
            
            var dataCount = queryMappingKadaster.Count();
            var getListOfMappingKadaster = queryMappingKadaster
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToList();

            foreach (var X in getListOfMappingKadaster)
            {
                if (X.isGenerateBySystem && X.criteriaKadaster == "By Code")
                {
                    var getUnitCounted = GetDataUnit(X.projectID, X.categoryID, X.isActive);
                    X.kadasterCodeAndDetailUnit = getUnitCounted;
                }
            }

            Logger.InfoFormat("GetMappingKadaster() end");

            return new PagedResultDto<GetMappingKadasterListDto>(
                dataCount,
                getListOfMappingKadaster
                );
        }

        public List<UnitByClusterListDto> GetUnitByCluster(int clusterID, int projectID, int categoryID)
        {
            Logger.DebugFormat("GetDropdownCategoryByProjectUnit() - Start with " +
                "clusterID = {1}{0}" +
                "projectID = {2}{0}" +
                "categoryID = {3}{0}"
            , Environment.NewLine, clusterID, projectID, categoryID);

            List<UnitByClusterListDto> listUnitNoByCluster = new List<UnitByClusterListDto>();
            var query = (from A in _msUnitRepo.GetAll()
                         join B in _msUnitCodeRepo.GetAll() on A.unitCodeID equals B.Id
                         where A.clusterID == clusterID
                         && A.projectID == projectID
                         && A.categoryID == categoryID
                         select new
                         {
                             unitCode = B.unitCode,
                             unitCodeID = B.Id,
                             unitID = A.Id,
                             unitNo = A.unitNo
                         }).ToList();

            var unitCodes = (from A in query
                             group A by A.unitCode into G
                             select G.Key).ToList();

            foreach (var unitCode in unitCodes)
            {
                UnitByClusterListDto unitByCluster = new UnitByClusterListDto();
                var unitNo = query.Where(x => x.unitCode == unitCode).Select(x => new UnitDesc { unitNo = x.unitNo, unitID = x.unitID }).ToList();
                unitByCluster.unitCode = unitCode;
                unitByCluster.unit = unitNo;
                listUnitNoByCluster.Add(unitByCluster);
            }
            
            return listUnitNoByCluster;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_PSAS_LegalDocument_MasterKadaster_Delete)]
        public void UpdateIsActiveMappingKadaster(int projectID, int categoryID)
        {
            Logger.DebugFormat("DeleteMappingKadaster() - Start Update IsActive mappingKadaster " +
                "   projectID = {1}{0}" +
                "   categoryID = {2}{0}"
                , Environment.NewLine, projectID, categoryID);

            var getMappingKadaster = (from x in _msMappingKadasterRepo.GetAll()
                                           where x.projectID == projectID &&
                                           x.categoryID == categoryID &&
                                           x.isActive 
                                      select x).ToList();
            foreach (var MappingKadaster in getMappingKadaster)
            {
                var updateMappingKadaster = MappingKadaster.MapTo<MS_MappingKadaster>();
                updateMappingKadaster.isActive = false;
                Logger.DebugFormat("DeleteMappingKadaster() - Start Update isActive MS_MappingKadaster with ID = " + MappingKadaster.Id);
                _msMappingKadasterRepo.Update(updateMappingKadaster);
                Logger.DebugFormat("DeleteMappingKadaster() - End Update isActive MS_MappingKadaster ");
                
            }

            Logger.DebugFormat("DeleteMappingKadaster() - End Delete MS_MappingKadaster ");
        }

        //[AbpAuthorize(AppPermissions.Pages_Tenant_PSAS_LegalDocument_MasterKadaster_View)]
        ////criteriaKadaster 1 = By Code
        ////criteriaKadaster 2 = By Unit
        ////criteriaKadaster 3 / else = No Mapping By Unit
        //public GetMappingKadasterListDto ViewMappingKadaster(int projectID, int categoryID, bool status, int criteriaKadaster)
        //{
        //        var queryMappingKadaster = (from A in _msMappingKadasterRepo.GetAll()
        //                                join B in _msProjectRepo.GetAll() on A.projectID equals B.Id
        //                                join C in _msCategoryRepo.GetAll() on A.categoryID equals C.Id
        //                                where 
        //                                A.isActive == status &&
        //                                A.projectID == projectID && 
        //                                A.categoryID == categoryID &&
        //                                (criteriaKadaster == 1 ? (A.isGeneratedBySystem && A.kadasterCode != null) : //ByCode
        //                                criteriaKadaster == 2 ? (A.isGeneratedBySystem && A.kadasterCode == null) : //ByUnit
        //                                !A.isGeneratedBySystem) //NoMappingByUnut
        //                                select new GetMappingKadasterListDto
        //                                {
        //                                    projectID = B.Id,
        //                                    projectName = B.projectName,
        //                                    categoryID = C.Id,
        //                                    categoryName = C.categoryName,
        //                                    url = A.kadasterLocation,
        //                                    isActive = A.isActive,
        //                                    criteriaKadaster = A.isGeneratedBySystem ? A.kadasterCode != null ? "By Code" : "By Unit" : "No Mapping By Unit",
        //                                    isGenerateBySystem = A.isGeneratedBySystem
        //                                })
        //                                .Distinct()
        //                                .FirstOrDefault();

        //    var getUnitCounted = GetDataUnit(projectID, categoryID, status);
        //    queryMappingKadaster.kadasterCodeAndDetailUnit = getUnitCounted;

        //    return queryMappingKadaster;
        //}

        private List<UnitCountedDto> GetDataUnit(int projectID, int categoryID, bool isActive)
        {
            List<UnitCountedDto> unitCountedList = new List<UnitCountedDto>();
            var getKadasterCode = (from x in _msMappingKadasterRepo.GetAll()
                                   where x.projectID == projectID && x.categoryID == categoryID
                                   && x.isActive == isActive
                                   select x).ToList();

            var group = (from y in getKadasterCode
                         group y by y.kadasterCode into G
                         select new UnitCountedDto
                         {
                             kadasterCode = G.Key,
                             unitAmount = G.Count()
                         }).ToList();

            foreach (var X in group)
            {
                var unitDetail = GetDataDetailUnit(X.kadasterCode, isActive);
                X.unitDetail = unitDetail;
            }

            unitCountedList.AddRange(group);
            return unitCountedList;
        }

        private List<UnitDetail> GetDataDetailUnit(string kadasterCode, bool isActive)
        {
            List<UnitDetail> unitDetailList = new List<UnitDetail>();
            var getUnitDetail = (from a in _msMappingKadasterRepo.GetAll()
                                 where a.kadasterCode == kadasterCode && a.isActive == isActive
                                 join b in _msUnitRepo.GetAll() on a.unitID equals b.Id
                                 join c in _msUnitCodeRepo.GetAll() on b.unitCodeID equals c.Id
                                 join d in _msClusterRepo.GetAll() on b.clusterID equals d.Id
                                 select new UnitDetail
                                 {
                                     clusterName = d.clusterName,
                                     clusterCode = d.clusterCode,
                                     unitCode = c.unitCode,
                                     unitNo = b.unitNo
                                 }).ToList();
            unitDetailList.AddRange(getUnitDetail);
            return unitDetailList;
        }

        public List<UnitByClusterListDto> GetUnitByClusterForEdit(int clusterID, int projectID, int categoryID)
        {
            List<UnitByClusterListDto> listUnitNoByCluster = new List<UnitByClusterListDto>();

            var query = (from a in _msUnitRepo.GetAll()
                         join b in _msUnitCodeRepo.GetAll() on a.unitCodeID equals b.Id
                         where a.clusterID == clusterID
                         && a.projectID == projectID
                         && a.categoryID == categoryID
                         select new UnitSelectedDto
                         {
                             unitCode = b.unitCode,
                             unitCodeID = b.Id,
                             unitID = a.Id,
                             unitNo = a.unitNo,
                             kadasterCodeSelected = false
                         }).ToList();

            var getUnitWithKadaster = (from x in _msMappingKadasterRepo.GetAll()
                                       where x.projectID == projectID && x.categoryID == categoryID
                                       && x.isActive
                                       select x.unitID).ToList();

            foreach (var unitWithKadaster in getUnitWithKadaster)
            {
                foreach (var allUnit in query)
                {
                    if (allUnit.unitID == unitWithKadaster)
                    {
                        allUnit.kadasterCodeSelected = true;
                    }
                }
            }

            var unitCodes = (from a in query
                             select a.unitCode).Distinct();

            foreach (var unitCode in unitCodes)
            {
                UnitByClusterListDto unitByCluster = new UnitByClusterListDto();
                var unitNo = query.Where(x => x.unitCode == unitCode)
                    .Select(x => new UnitDesc { unitNo = x.unitNo, unitID = x.unitID, isSelected = x.kadasterCodeSelected}).ToList();
                unitByCluster.unitCode = unitCode;
                unitByCluster.unit = unitNo;
                listUnitNoByCluster.Add(unitByCluster);
            }

            Logger.DebugFormat("GetDropdownCategoryByProjectUnit() - End");
            return listUnitNoByCluster;

        }

        //public List<UnitCountedDto> GetUnitCounted(int projectID, int categoryID, bool isActive)
        //{
        //    List<UnitCountedDto> unitCountedList = new List<UnitCountedDto>();
        //    var getKadasterCode = (from x in _msMappingKadasterRepo.GetAll()
        //                           where x.projectID == projectID && x.categoryID == categoryID
        //                           && x.isActive == isActive
        //                           select x).ToList();

        //    var group = (from y in getKadasterCode        
        //                group y by y.kadasterCode into G
        //                select new UnitCountedDto
        //                {
        //                    kadasterCode = G.Key,
        //                    unitAmount = G.Count()
        //                 }).ToList();

        //    unitCountedList.AddRange(group);
        //    return unitCountedList;
        //}

        //public List<UnitDetail> GetDetailUnitMappingKadaster(string kadasterCode, bool isActive)
        //{
        //    List<UnitDetail> unitDetailList = new List<UnitDetail>();
        //    var getUnitDetail = (from a in _msMappingKadasterRepo.GetAll()
        //                         where a.kadasterCode == kadasterCode && a.isActive == isActive
        //                         join b in _msUnitRepo.GetAll() on a.unitID equals b.Id
        //                         join c in _msUnitCodeRepo.GetAll() on b.unitCodeID equals c.Id
        //                         join d in _msClusterRepo.GetAll() on b.clusterID equals d.Id
        //                         select new UnitDetail
        //                         {
        //                             clusterName = d.clusterName,
        //                             clusterCode = d.clusterCode,
        //                             unitCode = c.unitCode,
        //                             unitNo = b.unitNo
        //                         }).ToList();
        //    unitDetailList.AddRange(getUnitDetail);
        //    return unitDetailList;
        //}

        public JObject GetDefaultMappingKadasterLocation()
        {
            JObject obj = new JObject();

            var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
            var webConfigApp = (JObject)appsettingsjson["App"];

            var pathKadasterLocation = webConfigApp.Property("KadasterLocation").Value.ToString();

            obj.Add("message", pathKadasterLocation);
            return obj;
        }
    }
}
