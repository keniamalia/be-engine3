﻿using Abp.Domain.Repositories;
using Abp.UI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using VDI.Demo.Files;
using VDI.Demo.PropertySystemDB.LippoMaster;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;
using VDI.Demo.PSAS.Dto;
using VDI.Demo.PSAS.LegalDocument.BookingDocument.Dto;
using VDI.Demo.PSAS.Price;
using System.Linq;
using Abp.Linq.Extensions;
using Abp.AutoMapper;
using System.Globalization;
using VDI.Demo.PersonalsDB;
using VDI.Demo.PropertySystemDB.Pricing;
using VDI.Demo.PropertySystemDB.OnlineBooking.PropertySystem;
using VDI.Demo.EntityFrameworkCore;
using static VDI.Demo.PSAS.LegalDocument.BookingDocument.Dto.KonfirmasiPesananLegalDocDto;
using System.Net.Http;
using System.Net.Http.Headers;
using Abp.AspNetZeroCore.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using VDI.Demo.Configuration;
using VDI.Demo.OnlineBooking.PaymentMidtrans.Dto;
using VDI.Demo.Authorization;
using Abp.Authorization;
using VDI.Demo.OnlineBooking.Transaction.Dto;
using VDI.Demo.DataExporting.Pdf.LegalDocument;
using VDI.Demo.OnlineBooking.Transaction;

namespace VDI.Demo.PSAS.LegalDocument.BookingDocument
{
    //[AbpAuthorize(AppPermissions.Pages_Tenant_PSAS_LegalDocument_BookingDocument)]
    public class PSASBookingDocumentAppService : DemoAppServiceBase, IPSASBookingDocumentAppService
    {
        private readonly IRepository<MS_KuasaDireksi> _msKuasaDireksiRepo;
        private readonly IRepository<MS_KuasaDireksiPeople> _msKuasaDireksiPeopleRepo;
        private readonly IRepository<MS_MappingTemplate> _msMappingTemplateRepo;
        private readonly IRepository<MS_DocumentPS> _msDocumentPsRepo;
        private readonly IRepository<MS_DocTemplate> _msDocTemplateRepo;
        private readonly IRepository<MS_Project> _msProjectRepo;
        private readonly IFilesHelper _iFilesHelper;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IRepository<TR_BookingHeader> _trBookingHeaderRepo;
        private readonly IRepository<TR_BookingDetail> _trBookingDetailRepo;
        private readonly IRepository<MS_Unit> _msUnitRepo;
        private readonly IRepository<MS_Cluster> _msClusterRepo;
        private readonly IRepository<MS_UnitCode> _msUnitCodeRepo;
        private readonly IRepository<TR_BookingDocument> _trBookingDocumentRepo;
        private readonly IRepository<MS_Company> _msCompanyRepo;
        private readonly IRepository<DocNo_Counter> _docNoCounterRepo;
        private readonly IRepository<PERSONALS, string> _personalsRepo;
        private readonly IRepository<TR_Address, string> _trAddressRepo;
        private readonly IRepository<TR_Phone, string> _trPhoneRepo;
        private readonly IRepository<TR_ID, string> _trIdRepo;
        private readonly IRepository<TR_Email, string> _trEmailRepo;
        private readonly IPSASPriceAppService _ipriceAppService;

        private readonly IRepository<MS_Term> _msTermRepo;
        private readonly IRepository<MS_TermMain> _msTermMainRepo;
        private readonly IRepository<TR_UnitOrderDetail> _trUnitOrderDetailRepo;
        private readonly IRepository<TR_UnitOrderHeader> _trUnitOrderHeaderRepo;
        private readonly IRepository<LK_AddrType, string> _lkAddrTypeRepo;
        private readonly IRepository<LK_IDType, string> _lkIdTypeRepo;
        private readonly IRepository<TR_BookingDetailSchedule> _trBookingDetailScheduleRepo;
        private readonly IRepository<MS_UnitItem> _msUnitItemRepo;
        private readonly IRepository<MS_Facade> _msFacadeRepo;
        private readonly IRepository<MS_Category> _msCategoryRepo;
        private readonly IRepository<MS_MappingKadaster> _msMappingKadasterRepo;

        private readonly PersonalsNewDbContext _personalContext;
        private readonly PropertySystemDbContext _propertySystemContext;
        private readonly NewCommDbContext _newCommContext;

        private readonly IConfigurationRoot _appConfiguration;
        private readonly ILegalDocumentGeneratePdfExporter _legalDocumentGeneratePdfExporter;
        private readonly ITransactionAppService _transactionAppService;

        public PSASBookingDocumentAppService(
            IRepository<MS_KuasaDireksi> msKuasaDireksiRepo,
            IRepository<MS_KuasaDireksiPeople> msKuasaDireksiPeopleRepo,
            IRepository<MS_MappingTemplate> msMappingTemplateRepo,
            IRepository<MS_DocumentPS> msDocumentPsRepo,
            IRepository<MS_DocTemplate> msDocTemplateRepo,
            IRepository<MS_Project> msProjectRepo,
            IFilesHelper iFilesHelper,
            IHttpContextAccessor httpContextAccessor,
            IHostingEnvironment hostingEnvironment,
            IRepository<TR_BookingHeader> trBookingHeaderRepo,
            IRepository<TR_BookingDetail> trBookingDetailRepo,
            IRepository<MS_Unit> msUnitRepo,
            IRepository<MS_Cluster> msClusterRepo,
            IRepository<MS_UnitCode> msUnitCodeRepo,
            IRepository<TR_BookingDocument> trBookingDocumentRepo,
            IRepository<MS_Company> msCompanyRepo,
            IRepository<DocNo_Counter> docNoCounterRepo,
            IRepository<PERSONALS, string> personalsRepo,
            IRepository<TR_Address, string> trAddressRepo,
            IRepository<TR_Phone, string> trPhoneRepo,
            IRepository<TR_ID, string> trIdRepo,
            IRepository<TR_Email, string> trEmailRepo,
            IPSASPriceAppService ipriceAppService,

            IRepository<MS_Term> msTermRepo,
            IRepository<MS_TermMain> msTermMainRepo,
            IRepository<TR_UnitOrderDetail> trUnitOrderDetailRepo,
            IRepository<TR_UnitOrderHeader> trUnitOrderHeaderRepo,
            IRepository<LK_AddrType, string> lkAddrTypeRepo,
            IRepository<LK_IDType, string> lkIdTypeRepo,
            IRepository<TR_BookingDetailSchedule> trBookingDetailScheduleRepo,
            IRepository<MS_UnitItem> msUnitItemRepo,
            IRepository<MS_Facade> msFacadeRepo,
            IRepository<MS_Category> msCategoryRepo,
            IRepository<MS_MappingKadaster> msMappingKadasterRepo,

        PersonalsNewDbContext personalContext,
            PropertySystemDbContext propertySystemContext,
            NewCommDbContext newCommContext,
            ILegalDocumentGeneratePdfExporter legalDocumentGeneratePdfExporter,
            ITransactionAppService transactionAppService
            )
        {
            _msKuasaDireksiRepo = msKuasaDireksiRepo;
            _msKuasaDireksiPeopleRepo = msKuasaDireksiPeopleRepo;
            _trBookingDocumentRepo = trBookingDocumentRepo;
            _msUnitRepo = msUnitRepo;
            _msClusterRepo = msClusterRepo;
            _msUnitCodeRepo = msUnitCodeRepo;
            _trBookingHeaderRepo = trBookingHeaderRepo;
            _trBookingDetailRepo = trBookingDetailRepo;
            _ipriceAppService = ipriceAppService;
            _msMappingTemplateRepo = msMappingTemplateRepo;
            _msDocumentPsRepo = msDocumentPsRepo;
            _msDocTemplateRepo = msDocTemplateRepo;
            _msProjectRepo = msProjectRepo;
            _msCompanyRepo = msCompanyRepo;
            _docNoCounterRepo = docNoCounterRepo;
            _personalsRepo = personalsRepo;
            _trAddressRepo = trAddressRepo;
            _trPhoneRepo = trPhoneRepo;
            _trIdRepo = trIdRepo;
            _iFilesHelper = iFilesHelper;
            _httpContextAccessor = httpContextAccessor;
            _hostingEnvironment = hostingEnvironment;

            _msTermRepo = msTermRepo;
            _msTermMainRepo = msTermMainRepo;
            _trUnitOrderDetailRepo = trUnitOrderDetailRepo;
            _trUnitOrderHeaderRepo = trUnitOrderHeaderRepo;
            _lkAddrTypeRepo = lkAddrTypeRepo;
            _lkIdTypeRepo = lkIdTypeRepo;
            _trBookingDetailScheduleRepo = trBookingDetailScheduleRepo;
            _msUnitItemRepo = msUnitItemRepo;
            _msFacadeRepo = msFacadeRepo;
            _msCategoryRepo = msCategoryRepo;
            _msMappingKadasterRepo = msMappingKadasterRepo;

            _newCommContext = newCommContext;
            _propertySystemContext = propertySystemContext;
            _personalContext = personalContext;

            _appConfiguration = hostingEnvironment.GetAppConfiguration();
            _legalDocumentGeneratePdfExporter = legalDocumentGeneratePdfExporter;
            _transactionAppService = transactionAppService;
        }

        public GetBookingDocumentListDto GetBookingDocument(GetPSASParamsDto input)
        {
            GetBookingDocumentListDto BookingDocument = new GetBookingDocumentListDto();

            var UnitID = _ipriceAppService.GetParameter(input);
            var getBookingDocument = (from x in _trBookingHeaderRepo.GetAll()
                                      join a in _msUnitRepo.GetAll() on x.unitID equals a.Id
                                      join b in _msUnitCodeRepo.GetAll() on a.unitCodeID equals b.Id
                                      join c in _msProjectRepo.GetAll() on a.projectID equals c.Id
                                      join d in _msClusterRepo.GetAll() on a.clusterID equals d.Id
                                      where x.Id == UnitID.bookingHeaderID
                                      select new GetBookingDocumentListDto
                                      {
                                          bookCode = x.bookCode,
                                          projectID = c.Id,
                                          projectName = c.projectName,
                                          clusterName = d.clusterName,
                                          unitCode = b.unitCode,
                                          unitNo = a.unitNo,
                                          bookingDate = x.bookDate
                                      }).FirstOrDefault();

            //var getBookingDocumentData = (from x in _trBookingDocumentRepo.GetAll()
            //                              where x.bookingHeaderID == UnitID.bookingHeaderID
            //                              join a in _msDocumentPsRepo.GetAll() on x.docID equals a.Id
            //                              join b in _msMappingTemplateRepo.GetAll() on new
            //                              {
            //                                  docID = a.Id,
            //                                  projectID = getBookingDocument.projectID,
            //                                  isActive = true
            //                              } equals new
            //                              {
            //                                  docID = b.docID,
            //                                  projectID = b.projectID,
            //                                  isActive = b.isActive,                                            
            //                              } into bb
            //                              from b in bb.DefaultIfEmpty()
            //                              where b.Id > 0 ? (b.activeTo != null ? x.docDate <= b.activeTo && x.docDate >= b.activeFrom : x.docDate >= b.activeFrom) : true
            //                              select new BookingDocumentListDto
            //                              {
            //                                  bookingDocumentId = x.Id,
            //                                  docCode = a.docCode,
            //                                  docName = a.docName,
            //                                  docNo = x.docNo,
            //                                  docDate = x.docDate,
            //                                  remarks = x.remarks,
            //                                  isTandaTerima = b != null ? b.isTandaTerima : false,
            //                                  tandaTerimaNo = x.tandaTerimaNo,
            //                                  fileTandaTerima = (x != null && x.tandaTerimaFile != null) ? getAbsoluteUriWithoutTail() + GetURLWithoutHost(x.tandaTerimaFile) : null,
            //                                  isNewest = false,
            //                                  latestDate = x.CreationTime
            //                              })
            //                              .ToList();

            var getBookingDocumentData = (from x in _trBookingDocumentRepo.GetAll()
                                          where x.bookingHeaderID == UnitID.bookingHeaderID
                                          join y in _msDocumentPsRepo.GetAll() on x.docID equals y.Id
                                          select new BookingDocumentListDto
                                          {
                                              bookingDocumentId = x.Id,
                                              docCode = y.docCode,
                                              docName = y.docName,
                                              docNo = x.docNo,
                                              docDate = x.docDate,
                                              remarks = x.remarks,
                                              tandaTerimaNo = x.tandaTerimaNo,
                                              fileTandaTerima = (x != null && x.tandaTerimaFile != null) ? getAbsoluteUriWithoutTail() + GetURLWithoutHost(x.tandaTerimaFile) : null,
                                              isNewest = false,
                                              latestDate = x.CreationTime
                                          }).ToList();



            var latest = getBookingDocumentData.OrderByDescending(x => x.latestDate).FirstOrDefault();
            foreach (var A in getBookingDocumentData)
            {
                if (A.bookingDocumentId == latest.bookingDocumentId)
                {
                    A.isNewest = true;
                }
            }

            BookingDocument.bookCode = getBookingDocument.bookCode;
            BookingDocument.projectID = getBookingDocument.projectID;
            BookingDocument.projectName = getBookingDocument.projectName;
            BookingDocument.clusterName = getBookingDocument.clusterName;
            BookingDocument.unitCode = getBookingDocument.unitCode;
            BookingDocument.unitNo = getBookingDocument.unitNo;
            BookingDocument.bookingDate = getBookingDocument.bookingDate;

            BookingDocument.bookingDoc = getBookingDocumentData;

            return BookingDocument;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_PSAS_MasterLegalDocument_MappingTemplate_Create)]
        public JObject CreateBookingDocument(CreateBookingDocumentInputDto input)
        {
            JObject obj = new JObject();

            #region Insert TrBookingDocument
            //Check BookCode
            GetPSASParamsDto inputPSAS = new GetPSASParamsDto();
            inputPSAS.bookCode = input.bookCode;
            inputPSAS.unitCode = input.unitCode;
            inputPSAS.unitNo = input.unitNo;

            var docDate = DateTime.Now;
            var UnitID = _ipriceAppService.GetParameter(inputPSAS);
            var getTrBookingHeader = (from A in _trBookingHeaderRepo.GetAll()
                                      join B in _msFacadeRepo.GetAll() on A.facadeID equals B.Id into BB
                                      from B in BB.DefaultIfEmpty()
                                      where A.Id == UnitID.bookingHeaderID
                                      select new
                                      {
                                          bookingHeader = A,
                                          facade = B
                                      }).FirstOrDefault();

            if (getTrBookingHeader == null)
            {
                throw new UserFriendlyException("TR_BookingHeader with Id: " + UnitID.bookingHeaderID + " not found !");
            }

            //??
            var checkTrBookingDocumentDocDate = (from A in _trBookingDocumentRepo.GetAll()
                                                 where A.entityID == 1 &&
                                                 A.docID == input.docID &&
                                                 A.bookingHeaderID == UnitID.bookingHeaderID &&
                                                 A.docDate.Date == docDate.Date
                                                 select A).OrderByDescending(a => a.LastModificationTime != null ? a.LastModificationTime : a.CreationTime);

            //Cek Company ada tidak
            var getProjectID = (from A in _msUnitRepo.GetAll()
                                where A.Id == UnitID.unitID
                                select A.projectID).FirstOrDefault();
            var getBookingDetail = (from A in _trBookingDetailRepo.GetAll()
                                    where A.bookingHeaderID == UnitID.bookingHeaderID && A.refNo == 1
                                    select A).FirstOrDefault();
            var getCoID = (from A in _msCompanyRepo.GetAll()
                           where A.coCode == getBookingDetail.coCode
                           select A).FirstOrDefault();
            if (getCoID.Id == 0)
            {
                throw new UserFriendlyException("Company with CoCode: " + getBookingDetail.coCode + " not found !");
            }

            //Set Parameter Insert
            var param = new TR_BookingDocument
            {
                entityID = 1,
                bookingHeaderID = (int)UnitID.bookingHeaderID,
                docID = input.docID,
                oldDocNo = input.oldDocNo,
                remarks = input.remarks,
                tandaTerimaDate = input.tandaTerimaDate,
                tandaTerimaFile = input.tandaTerimaFile,
                docDate = docDate
            };

            //Get Unit
            var getUnit = (from A in _msUnitRepo.GetAll()
                           join B in _msUnitCodeRepo.GetAll() on A.unitCodeID equals B.Id
                           join C in _msProjectRepo.GetAll() on A.projectID equals C.Id
                           join D in _msCategoryRepo.GetAll() on A.categoryID equals D.Id
                           where A.Id == UnitID.unitID
                           select new
                           {
                               unit = A,
                               unitCode = B,
                               project = C,
                               category = D
                           }).FirstOrDefault();

            GetMappingPathListDto physicalFolderPath = new GetMappingPathListDto();

            var getMappingTemplateDoc = (from a in _msMappingTemplateRepo.GetAll()
                                         join b in _msDocTemplateRepo.GetAll() on a.docTemplateID equals b.Id
                                         where a.entityID == 1 &&
                                         a.projectID == getProjectID &&
                                         a.docID == input.docID &&
                                         //input.docDate >= a.activeFrom &&
                                         (a.activeTo != null ? docDate <= a.activeTo && docDate >= a.activeFrom : docDate >= a.activeFrom) &&
                                         a.isActive &&
                                         b.isActive.GetValueOrDefault()
                                         orderby a.activeFrom, a.activeTo descending
                                         select new GetMappingPathListDto
                                         {
                                             mappingTemplateID = a.Id,
                                             isTandaTerima = a.isTandaTerima,
                                             templateFile = b.templateFile,
                                             templateName = b.templateName,
                                             copyNo = b.copyNo.GetValueOrDefault(),
                                             tandaTerimaFile = a.isTandaTerimaFile
                                         })
                                      .FirstOrDefault();

            if (getMappingTemplateDoc == null)
            {
                //Get Default Template
                var checkIsMaster = (from A in _msDocTemplateRepo.GetAll()
                                     where A.entityID == 1 && A.isActive.GetValueOrDefault() && A.isMaster.GetValueOrDefault() && A.docID == input.docID
                                     select A).FirstOrDefault();

                if (checkIsMaster != null)
                {
                    physicalFolderPath.templateFile = checkIsMaster.templateFile;
                    physicalFolderPath.templateName = checkIsMaster.templateName;
                    physicalFolderPath.copyNo = checkIsMaster.copyNo.GetValueOrDefault();
                }
                else
                {
                    throw new UserFriendlyException("DocTemplate is empty. (Template Master Not Exist)");
                }
            }
            else
            {
                //Get Document yang akan di generate
                physicalFolderPath.mappingTemplateID = getMappingTemplateDoc.mappingTemplateID;
                physicalFolderPath.isTandaTerima = getMappingTemplateDoc.isTandaTerima;
                physicalFolderPath.templateFile = getMappingTemplateDoc.templateFile;
                physicalFolderPath.templateName = getMappingTemplateDoc.templateName;
                physicalFolderPath.copyNo = getMappingTemplateDoc.copyNo;
            }
            Logger.DebugFormat("physicalFolderPath : {0}", physicalFolderPath);

            var docCounter = CounterDocNo(input.projectID, input.docID, getCoID.Id);
            param.docNo = docCounter.docNo;

            if (physicalFolderPath.isTandaTerima)
            {
                param.tandaTerimaNo = docCounter.tandaTerimaNo;
            }

            //Cek dulu jika TR BookingDocument ada data digenerate dihari yg sama.
            if (!checkTrBookingDocumentDocDate.Any())
            {
                //Cek TrBookingDocument tanpa kondisi docDate
                var checkTrBookingDocument = (from A in _trBookingDocumentRepo.GetAll()
                                              where A.entityID == 1 &&
                                              A.docID == input.docID &&
                                              A.bookingHeaderID == UnitID.bookingHeaderID
                                              select A).OrderByDescending(a => a.LastModificationTime != null ? a.LastModificationTime : a.CreationTime);

                //TODO: OldDocNo jika kena validasi tgl tanda terima lebih dr doc date, throw error, update remarks jadi invalid, docno.
                if (checkTrBookingDocument.Any())
                {
                    var checkRemarks = (from A in _trBookingDocumentRepo.GetAll()
                                        where A.entityID == 1 &&
                                        A.bookingHeaderID == UnitID.bookingHeaderID &&
                                        A.docID == input.docID
                                        select A).OrderByDescending(a => a.LastModificationTime != null ? a.LastModificationTime : a.CreationTime).FirstOrDefault();

                    Logger.InfoFormat("getMappingTemplate() Not null.");

                    if (physicalFolderPath.isTandaTerima)
                    {
                        if (checkTrBookingDocument.FirstOrDefault().tandaTerimaFile != null)
                        {
                            throw new UserFriendlyException("Transaction already exist !");
                        }
                        else
                        {
                            param.oldDocNo = checkRemarks.docNo;
                        }
                    }
                    else
                    {
                        Logger.InfoFormat("physicalFolderPath.isTandaTerima - false.");
                    }
                }

                //Insert TR_BookingDocument
                Logger.DebugFormat("CreateBookingDocument() - Start Preparation Data For Insert TR_BookingDocument. Parameters sent: {0} " +
                    "   entityID = 1" +
                    "   bookingHeaderID = {1}{0}" +
                    "   docID = {2}{0}" +
                    "   docNo = {3}{0}" +
                    "   oldDocNo = {4}{0}" +
                    "   remarks = {5}{0}" +
                    "   tandaTerimaBy = {5}{0}" +
                    "   tandaTerimaDate = {5}{0}" +
                    "   tandaTerimaNo = {5}{0}" +
                    "   tandaTerimaFile = {5}{0}"
                    , Environment.NewLine, UnitID.bookingHeaderID, input.docID, input.docNo, input.oldDocNo, input.remarks, input.tandaTerimaBy, input.tandaTerimaDate, input.tandaTerimaNo, input.tandaTerimaFile);
                _trBookingDocumentRepo.Insert(param);
                Logger.DebugFormat("CreateBookingDocument() - End Insert TR_BookingDocument");

            }
            else //Jika ada data lama, replace pdf
            {
                if (physicalFolderPath.isTandaTerima)
                {
                    if (checkTrBookingDocumentDocDate.FirstOrDefault().tandaTerimaFile != null)
                    {
                        throw new UserFriendlyException("Transaction already exist !");
                    }
                }
                else
                {
                    Logger.InfoFormat("physicalFolderPath.isTandaTerima - false.");
                }

                var oldData = checkTrBookingDocumentDocDate.FirstOrDefault();
                param.docNo = oldData.docNo;
                param.tandaTerimaNo = oldData.tandaTerimaNo;
                if (oldData != null)
                { 
                    //Update DocDate jadi yang terbaru
                    var updateBookingDocument = oldData.MapTo<TR_BookingDocument>();
                    updateBookingDocument.docDate = DateTime.Now;
                    _trBookingDocumentRepo.Update(updateBookingDocument);
                    Logger.InfoFormat("Update DocDate TR_BookingDocument with id: " + updateBookingDocument.Id);
                }

            }
            #endregion Insert TrBookingDocument

            #region Generate PPPU
            //Generate PPPU
            Logger.DebugFormat("GeneratePPPU() - Start Generate. Parameters sent: {0} " +
                "   docNo = {1}{0}" +
                "   bookingHeaderID = {2}{0}" +
                "   procurationName = {3}{0}" +
                "   procurationType = {4}{0}"
                , Environment.NewLine, param.docNo, param.bookingHeaderID, input.procurationName, input.procurationType);

            #region Prep Signature Image
            //Prepare Signature Image
            var base64Image = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAgEBLAEsAAD/7QaCUGhvdG9zaG9wIDMuMAA4QklNA+0KUmVzb2x1dGlvbgAAAAAQASwAAAABAAEBLAAAAAEAAThCSU0EDRhGWCBHbG9iYWwgTGlnaHRpbmcgQW5nbGUAAAAABAAAAB44QklNBBkSRlggR2xvYmFsIEFsdGl0dWRlAAAAAAQAAAAeOEJJTQPzC1ByaW50IEZsYWdzAAAACQAAAAAAAAAAAQA4QklNBAoOQ29weXJpZ2h0IEZsYWcAAAAAAQAAOEJJTScQFEphcGFuZXNlIFByaW50IEZsYWdzAAAAAAoAAQAAAAAAAAACOEJJTQP1F0NvbG9yIEhhbGZ0b25lIFNldHRpbmdzAAAASAAvZmYAAQBsZmYABgAAAAAAAQAvZmYAAQChmZoABgAAAAAAAQAyAAAAAQBaAAAABgAAAAAAAQA1AAAAAQAtAAAABgAAAAAAAThCSU0D+BdDb2xvciBUcmFuc2ZlciBTZXR0aW5ncwAAAHAAAP////////////////////////////8D6AAAAAD/////////////////////////////A+gAAAAA/////////////////////////////wPoAAAAAP////////////////////////////8D6AAAOEJJTQQIBkd1aWRlcwAAAAAQAAAAAQAAAkAAAAJAAAAAADhCSU0EHg1VUkwgb3ZlcnJpZGVzAAAABAAAAAA4QklNBBoGU2xpY2VzAAAAAGkAAAAGAAAAAAAAAAAAAACWAAAA7QAAAAQAQwBDAEIAMgAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAA7QAAAJYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOEJJTQQREUlDQyBVbnRhZ2dlZCBGbGFnAAAAAQEAOEJJTQQUF0xheWVyIElEIEdlbmVyYXRvciBCYXNlAAAABAAAAAE4QklNBAwVTmV3IFdpbmRvd3MgVGh1bWJuYWlsAAAC5QAAAAEAAABwAAAARwAAAVAAAF0wAAACyQAYAAH/2P/gABBKRklGAAECAQBIAEgAAP/uAA5BZG9iZQBkgAAAAAH/2wCEAAwICAgJCAwJCQwRCwoLERUPDAwPFRgTExUTExgRDAwMDAwMEQwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwBDQsLDQ4NEA4OEBQODg4UFA4ODg4UEQwMDAwMEREMDAwMDAwRDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDP/AABEIAEcAcAMBIgACEQEDEQH/3QAEAAf/xAE/AAABBQEBAQEBAQAAAAAAAAADAAECBAUGBwgJCgsBAAEFAQEBAQEBAAAAAAAAAAEAAgMEBQYHCAkKCxAAAQQBAwIEAgUHBggFAwwzAQACEQMEIRIxBUFRYRMicYEyBhSRobFCIyQVUsFiMzRygtFDByWSU/Dh8WNzNRaisoMmRJNUZEXCo3Q2F9JV4mXys4TD03Xj80YnlKSFtJXE1OT0pbXF1eX1VmZ2hpamtsbW5vY3R1dnd4eXp7fH1+f3EQACAgECBAQDBAUGBwcGBTUBAAIRAyExEgRBUWFxIhMFMoGRFKGxQiPBUtHwMyRi4XKCkkNTFWNzNPElBhaisoMHJjXC0kSTVKMXZEVVNnRl4vKzhMPTdePzRpSkhbSVxNTk9KW1xdXl9VZmdoaWprbG1ub2JzdHV2d3h5ent8f/2gAMAwEAAhEDEQA/APVUkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklP/0PVUkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklP/0fVUkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklP/0vVUkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklP/0/VUl8qpJKfqpJfKqSSn6qSXyqkkp+qkl8qpJKfqpJfKqSSn6qSXyqkkp+qkl8qpJKf/2QA4QklNBCEaVmVyc2lvbiBjb21wYXRpYmlsaXR5IGluZm8AAAAAVQAAAAEBAAAADwBBAGQAbwBiAGUAIABQAGgAbwB0AG8AcwBoAG8AcAAAABMAQQBkAG8AYgBlACAAUABoAG8AdABvAHMAaABvAHAAIAA2AC4AMAAAAAEAOEJJTQQGDEpQRUcgUXVhbGl0eQAAAAAHAAgBAQABAQD/7gAhQWRvYmUAZEAAAAABAwAQAwIDBgAAAAAAAAAAAAAAAP/bAIQAAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQICAgICAgICAgICAwMDAwMDAwMDAwEBAQEBAQEBAQEBAgIBAgIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMD/8IAEQgAlgDtAwERAAIRAQMRAf/EAF0AAQEAAAAAAAAAAAAAAAAAAAAKAQEAAAAAAAAAAAAAAAAAAAAAEAEAAAAAAAAAAAAAAAAAAACQEQEAAAAAAAAAAAAAAAAAAACQEgEAAAAAAAAAAAAAAAAAAACQ/9oADAMBAQIRAxEAAAC/gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAH//2gAIAQIAAQUADr//2gAIAQMAAQUADr//2gAIAQEAAQUADr//2gAIAQICBj8ADr//2gAIAQMCBj8ADr//2gAIAQEBBj8ADr//2Q==";
            StringBuilder base64SignatureImage1 = new StringBuilder();
            if ((input.signatureImage1 != null && input.signatureImage1 != "") && input.signatureImage1 != "null")
            {
                var fileSignature = _hostingEnvironment.ContentRootPath + @"\wwwroot" + input.signatureImage1.Replace("/", @"\");
                Logger.DebugFormat("Link fileSignature : {0}", fileSignature);
                Byte[] bytesSignatureImage = File.ReadAllBytes(fileSignature);
                var convertingBase64 = Convert.ToBase64String(bytesSignatureImage);
                var getTypeImage = new FileInfo(input.signatureImage1.Split('/')[5]);
                base64SignatureImage1.Append("data:image/" + getTypeImage.Extension.Replace(".", "") + ";base64," + convertingBase64);
            }
            else
            {
                base64SignatureImage1.Append(base64Image);
            }

            StringBuilder base64SignatureImage2 = new StringBuilder();
            if ((input.signatureImage2 != null && input.signatureImage2 != "") && input.signatureImage2 != "null")
            {
                var fileSignature = _hostingEnvironment.ContentRootPath + @"\wwwroot" + input.signatureImage2.Replace("/", @"\");
                Logger.DebugFormat("Link fileSignature 2 : {0}", fileSignature);
                Byte[] bytesSignatureImage2 = File.ReadAllBytes(fileSignature);
                var convertingBase64 = Convert.ToBase64String(bytesSignatureImage2);
                var getTypeImage = new FileInfo(input.signatureImage2.Split('/')[5]);
                base64SignatureImage2.Append("data:image/" + getTypeImage.Extension.Replace(".", "") + ";base64," + convertingBase64);
            }
            else
            {
                base64SignatureImage2.Append(base64Image);
            }
            #endregion Prep Signature Image

            #region GetDataGenerate
            //Start GetDataGenerate - Melengkapi data dto P3UDocumentDto
            Logger.DebugFormat("GetDataGenerate() - Start Preparation Generate");
            var getDataGenerate = GetDataGenerate(param.docNo, param.bookingHeaderID, input.procurationName, input.procurationType);

            //Sebuah fungsi untuk mengambil seluruh file didalam folder yg tersimpan di templateFile. Dan diurutkan berdasarkan creationTime file tsb

            var webRootPath = _hostingEnvironment.WebRootPath;

            string path = physicalFolderPath.templateFile;
            //getDataGenerate.path = 
            var pathReturn = path != null ? _iFilesHelper.GetBase64FileByPhysicalPath(path).ToList() : null;

            List<fileExtensionGenerateListDto> pathSorting = new List<fileExtensionGenerateListDto>();
            foreach (var pathFile in pathReturn)
            {
                Logger.Info("pathFile : " + pathFile.filePhysicalPath + " Order Number : " + pathFile.filename.Split('-')[0]);

                fileExtensionGenerateListDto newPathFile = new fileExtensionGenerateListDto();
                newPathFile.phisicalPath = pathFile.filePhysicalPath;
                newPathFile.orderNumber = Int32.Parse(pathFile.filename.Split('-')[0]);

                pathSorting.Add(newPathFile);

                Logger.Info("Path Order number : " + pathFile + " - " + newPathFile.orderNumber);
            }
            getDataGenerate.physicalPath = pathSorting.OrderBy(x => x.orderNumber).Select(x => x.phisicalPath).ToList();

            //Prepare file kadaster
            //step 1 - Looping getDataGenerate.path ambil kadaster untuk diganti
            var indexOfKadasterFile = getDataGenerate.physicalPath.FindIndex(x => x.Contains("FileKadaster.html"));

            var blankFloorHtml = Path.Combine(webRootPath, @"Assets\Upload\LegalDocument\MasterTemplate\", "Blank-Floor-Plan-Warning.html");
            
            if (indexOfKadasterFile != -1)
            {
                var getMappingKadaster = (from A in _msMappingKadasterRepo.GetAll()
                                          where A.categoryID == getUnit.category.Id
                                          && A.projectID == input.projectID
                                          && A.isActive
                                          select A).FirstOrDefault();

                if (getMappingKadaster != null)
                {
                    if (getMappingKadaster.isGeneratedBySystem)
                    {
                        getDataGenerate.isGenerateBySystem = true;
                        Logger.Info("Prepare FileKadaster() isGenerateBySystem : true");

                        var getKadasterCode = (from A in _msMappingKadasterRepo.GetAll()
                                               where A.isActive
                                               && A.isGeneratedBySystem
                                               && A.projectID == input.projectID
                                               && A.categoryID == getUnit.category.Id
                                               select new
                                               {
                                                   A.unitID,
                                                   A.kadasterCode,
                                                   A.kadasterLocation
                                               }).ToList();

                        if (getKadasterCode.Any()) //Highrise
                        {
                            Logger.Info("Prepare FileKadaster() getKadasterCode have data");
                            string directoryKadaster;

                            if (getKadasterCode.FirstOrDefault().kadasterCode != null) //By Code
                            {
                                Logger.Info("Prepare FileKadaster() getKadasterCode By Code");
                                var getKadasterCodeByCode = (from A in getKadasterCode
                                                             where A.unitID == UnitID.unitID
                                                             select new
                                                             {
                                                                 A.kadasterCode,
                                                                 A.kadasterLocation
                                                             }).FirstOrDefault();

                                var kadasterLocation = getKadasterCodeByCode.kadasterLocation;
                                directoryKadaster = kadasterLocation.Substring(kadasterLocation.Length - 1) != @"\" ? kadasterLocation + @"\" + getKadasterCodeByCode.kadasterCode : kadasterLocation + getKadasterCodeByCode.kadasterCode;
                                Logger.Info("Prepare FileKadaster() directoryKadaster : " + directoryKadaster);
                            }
                            else //ByUnit
                            {
                                Logger.Info("Prepare FileKadaster() getKadasterCode By Unit");
                                var getKadasterCodeByUnit = (from A in getKadasterCode
                                                             select new
                                                             {
                                                                 A.kadasterLocation
                                                             }).FirstOrDefault();
                                var kadasterLocation = getKadasterCodeByUnit.kadasterLocation;
                                directoryKadaster = kadasterLocation.Substring(kadasterLocation.Length - 1) != @"\" ? kadasterLocation + @"\" + getUnit.project.projectCode + "-" + getUnit.unitCode.unitCode + "#" + getUnit.unit.unitNo : kadasterLocation + getUnit.project.projectCode + "-" + getUnit.unitCode.unitCode + "#" + getUnit.unit.unitNo;
                                Logger.Info("Prepare FileKadaster() directoryKadaster : " + directoryKadaster);
                            }

                            //Check File
                            var fileKKadasterJpg = directoryKadaster + ".jpg";
                            var fileKKadasterJpeg = directoryKadaster + ".jpeg";
                            var fileKKadasterPng = directoryKadaster + ".png";
                            Logger.Info("Prepare FileKadaster() - " + fileKKadasterJpg);
                            Logger.Info("Prepare FileKadaster() - " + fileKKadasterJpeg);
                            Logger.Info("Prepare FileKadaster() - " + fileKKadasterPng);
                            if (File.Exists(fileKKadasterJpg))
                            {
                                Logger.Info("Prepare FileKadaster() - jpg");
                                getDataGenerate.imageKadaster = Convert.ToBase64String(File.ReadAllBytes(directoryKadaster + ".jpg"));
                            }
                            else if (File.Exists(fileKKadasterJpeg))
                            {
                                Logger.Info("Prepare FileKadaster() - jpeg");
                                getDataGenerate.imageKadaster = Convert.ToBase64String(File.ReadAllBytes(directoryKadaster + ".jpeg"));
                            }
                            else if (File.Exists(fileKKadasterPng))
                            {
                                Logger.Info("Prepare FileKadaster() - png");
                                getDataGenerate.imageKadaster = Convert.ToBase64String(File.ReadAllBytes(directoryKadaster + ".png"));
                            }
                            else
                            {
                                getDataGenerate.physicalPath[indexOfKadasterFile] = blankFloorHtml;
                                obj.Add("message", "Can not find File Kadaster with Unit Code: " + getUnit.unitCode.unitCode + " and Unit No: " + getUnit.unit.unitNo + ".");
                                Logger.InfoFormat("Prepare FileKadaster() File Kadaster extension not found.");
                            }
                        }
                        else
                        {
                            getDataGenerate.physicalPath[indexOfKadasterFile] = blankFloorHtml;
                            obj.Add("message", "Can not find File Kadaster with Unit Code: " + getUnit.unitCode.unitCode + " and Unit No: " + getUnit.unit.unitNo + ".");
                            Logger.InfoFormat("Prepare FileKadaster() File Kadaster not found.");
                        }
                    }
                    else //Landed
                    {
                        var categoryUnit = getUnit.category.kadasterType == "Landed";

                        getDataGenerate.isGenerateBySystem = false;
                        var kadasterLocation = getMappingKadaster.kadasterLocation;
                        var filename = getUnit.project.projectCode + "-" + getUnit.unitCode.unitCode + "#" + getUnit.unit.unitNo + ".pdf";
                        var filenameFullPath = kadasterLocation.Substring(kadasterLocation.Length - 1) != @"\" ? Path.Combine(webRootPath, getMappingKadaster.kadasterLocation + @"\", filename) : Path.Combine(webRootPath, getMappingKadaster.kadasterLocation, filename);

                        Logger.InfoFormat("Prepare FileKadaster() Filename: " + filenameFullPath);

                        var facadeName = getTrBookingHeader.facade != null ? getTrBookingHeader.facade.facadeCode != null ? getTrBookingHeader.facade.facadeCode : null : null;
                        var filenameFacade = facadeName != null ? getUnit.project.projectCode + "-" + getUnit.unitCode.unitCode + "#" + getUnit.unit.unitNo + "-" + facadeName + ".pdf" : null;
                        var filenameFacadeFullPath = filenameFacade != null ?
                            kadasterLocation.Substring(kadasterLocation.Length - 1) != @"\" ?
                            Path.Combine(webRootPath, getMappingKadaster.kadasterLocation + @"\", filenameFacade) : Path.Combine(webRootPath, getMappingKadaster.kadasterLocation, filenameFacade) : null;
                        Logger.InfoFormat("Prepare FileKadaster() Filename: " + filenameFacadeFullPath);

                        Logger.InfoFormat("Prepare FileKadaster() Started.");
                        if (File.Exists(filenameFullPath))
                        {
                            getDataGenerate.physicalPath[indexOfKadasterFile] = filenameFullPath;

                            if (categoryUnit)
                            {
                                if (File.Exists(filenameFacadeFullPath))
                                {
                                    getDataGenerate.physicalPath.Insert(indexOfKadasterFile + 1, filenameFacadeFullPath);
                                    Logger.InfoFormat("Prepare FileKadaster() File Kadasater was found .");
                                }
                                else
                                {
                                    //Menambahkan blank page jika 1B tidak ketemu, Mba Jess Requirement. Only for LANDED category
                                    var blankKadaster1BHtml = Path.Combine(webRootPath, @"Assets\Upload\LegalDocument\MasterTemplate\", "Blank-Kadaster-1B.html");

                                    getDataGenerate.physicalPath.Insert(indexOfKadasterFile + 1, blankKadaster1BHtml);
                                    obj.Add("message", "Can not find File Kadaster with Unit Code: " + getUnit.unitCode.unitCode + ", Unit No: " + getUnit.unit.unitNo + " and FacadeCode: " + facadeName);
                                    Logger.InfoFormat("Prepare FileKadaster() File Kadasater was not found .");
                                }
                            }
                        }
                        else
                        {
                            getDataGenerate.physicalPath[indexOfKadasterFile] = blankFloorHtml;
                            obj.Add("message", "Can not find File Kadaster with Unit Code: " + getUnit.unitCode.unitCode + " and Unit No: " + getUnit.unit.unitNo + "");
                            Logger.InfoFormat("Prepare FileKadaster() File Kadaster not found.");
                            Logger.InfoFormat("Prepare FileKadaster() File Kadasater was not found .");
                        }
                    }
                }
                else
                {
                    getDataGenerate.physicalPath[indexOfKadasterFile] = blankFloorHtml;
                    obj.Add("message", "Can not find File Kadaster with Unit Code: " + getUnit.unitCode.unitCode + " and Unit No: " + getUnit.unit.unitNo + "");
                    Logger.InfoFormat("Prepare FileKadaster() File Kadasater was not found .");
                }
            }

            var indexOfKPFile = getDataGenerate.physicalPath.FindIndex(x => x.Contains("KP.html"));
            Logger.Info("indexOfKPFile : " + indexOfKPFile);
            if (indexOfKPFile != -1)
            {
                Logger.Info("Cek File KP.");
                var getKpFile = _transactionAppService.GenerateKP(getDataGenerate.OrderID, getDataGenerate.UnitID);

                if (getKpFile.pathKP != "")
                {
                    Logger.Info("KP ada.");
                    if (File.Exists(getKpFile.pathKP))
                    {
                        Logger.InfoFormat("Prepare KP() File KP was found - " + getKpFile.pathKP);
                        getDataGenerate.physicalPath[indexOfKPFile] = getKpFile.pathKP;
                    }
                    else
                    {
                        if (obj.HasValues)
                        {
                            obj.Property("message").Remove();
                        }
                        Logger.InfoFormat("Prepare KP() File KP was not found - " + getKpFile.pathKP);
                        obj.Add("message", "FIle KP not found");
                    }
                }
            }
            else
            {
                if (obj.HasValues)
                {
                    obj.Property("message").Remove();
                }
                else
                {
                    Logger.InfoFormat("Prepare KP() File KP was not found .");
                    //obj.Add("message", "File KP not found");
                }
                //obj.Property("message").Remove();
                
            }

            List<string> setDoc = new List<string>();
            //List<string> setDoc = new List<string>();
            //step 2 - Cek rangkap (Number of copy)
            if (physicalFolderPath != null)
            {
                if (physicalFolderPath.copyNo > 0)
                {
                    for (var copyNoInc = 0; copyNoInc < physicalFolderPath.copyNo; copyNoInc++)
                    {
                        setDoc.AddRange(getDataGenerate.physicalPath);
                    }

                    getDataGenerate.physicalPath = setDoc;
                }
            }
            else
            {
                Logger.InfoFormat("Prepare FileKadaster() Physical Path is null.");
            }

            Logger.DebugFormat("GetDataGenerate() - isTandaTerima start");
            //Ambil isTandaTerima
            getDataGenerate.isTandaTerima = physicalFolderPath != null ? physicalFolderPath.isTandaTerima : false;
            if (getDataGenerate.isTandaTerima)
            {
                //var suratTerimaPath = Path.Combine(webRootPath, @"Assets\Upload\LegalDocument\MasterTemplate\", "Surat-Terima.html");
                var suratTerimaPath = webRootPath + getMappingTemplateDoc.tandaTerimaFile;
                Logger.Info("suratTerimaPath :" + suratTerimaPath);
               
                getDataGenerate.physicalPath.Add(suratTerimaPath);
            }
            Logger.DebugFormat("GetDataGenerate() - isTandaTerima end");

            getDataGenerate.DocDate = param.docDate.ToString("dd/MM/yyyy");
            getDataGenerate.PPDate = DateTime.Now.ToString("dd/MM/yyyy");
            getDataGenerate.PrintDay = DateTime.Now.ToString("dddd", new System.Globalization.CultureInfo("id-ID"));
            getDataGenerate.PrintDate = DateTime.Now.ToString("dd/MM/yyyy");
            getDataGenerate.NameOfProcuration = input.procurationName;
            getDataGenerate.TypeOfProcuration = input.procurationType == 1 ? "selaku wali dari anak yang masih di bawah umur "
                                                : input.procurationType == 2 ? "selaku kuasa dari"
                                                : input.procurationType == 3 ? "" : null;
            getDataGenerate.TandaTerimaNo = param.tandaTerimaNo;
            getDataGenerate.KadasterErrorMessage = "File Kadaster with UnitCode: " + getDataGenerate.UnitCode + " and UnitNo: " + getDataGenerate.UnitNo;
            Logger.DebugFormat("GetDataGenerate() - Unit Code: " + getDataGenerate.UnitCode);
            getDataGenerate.Block = getDataGenerate.UnitCode != null ?
                getDataGenerate.UnitCode.Contains("-") ?
                getDataGenerate.UnitCode.Split('-')[0] :
                getDataGenerate.UnitCode.Substring(0, getDataGenerate.UnitCode.Length - 2)
                : null;
            Logger.DebugFormat("GetDataGenerate() - Block: " + getDataGenerate.Block);
            getDataGenerate.Tower = getDataGenerate.UnitCode != null ?
                getDataGenerate.UnitCode.Contains("-") ?
                getDataGenerate.UnitCode.Split('-')[1] :
                getDataGenerate.UnitCode.Substring(getDataGenerate.UnitCode.Length - 2)
                : null;
            Logger.DebugFormat("GetDataGenerate() - Tower: " + getDataGenerate.Tower);
            getDataGenerate.signatureImage1 = base64SignatureImage1.ToString();
            getDataGenerate.signatureImage2 = base64SignatureImage2.ToString();
            getDataGenerate.kuasaDireksiPeopleName1 = input.kuasaDireksiPeopleName1;
            getDataGenerate.kuasaDireksiPeopleName2 = input.kuasaDireksiPeopleName2;
            Logger.DebugFormat("GetDataGenerate() - End Preparation Generate");
            //End Prep GetDataGenerate
            #endregion GetDataGenerate

            //Returning byte
            var responseFile = _legalDocumentGeneratePdfExporter.PrintP3UGeneratePdf(getDataGenerate);

            var oldLinkPath = @"Assets\Upload\LegalDocument\PPPU\" + getDataGenerate.BookCode;
            var oldPath = Path.Combine(webRootPath, oldLinkPath);
            var filePath = oldPath + @"\" + param.docNo.Replace("/", "-") + ".pdf";

            if (!Directory.Exists(oldPath))
            {
                Directory.CreateDirectory(oldPath);
            }
            System.IO.File.WriteAllBytes(filePath, responseFile);

            //#region Get From API generate PDF 
            //var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
            //var webConfigApp = (JObject)appsettingsjson["App"];
            //var configuration = webConfigApp.Property("apiPdfUrl").Value.ToString();
            //using (var client = new HttpClient())
            //{
            //    client.Timeout = TimeSpan.FromMinutes(10);
            //    var url = configuration + "api/Pdf/P3UPdf";

            //    var request = new HttpRequestMessage(HttpMethod.Post, url);
            //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(MimeTypeNames.ApplicationJson));
            //    request.Content = new StringContent(JsonConvert.SerializeObject(getDataGenerate), Encoding.UTF8, MimeTypeNames.ApplicationJson);

            //    var responseFile = ReadResponse(url, client, request);

            //    var oldLinkPath = @"Assets\Upload\LegalDocument\PPPU\" + getDataGenerate.BookCode;
            //    var oldPath = Path.Combine(webRootPath, oldLinkPath);
            //    var filePath = oldPath + @"\" + param.docNo.Replace("/", "-") + ".pdf";

            //    if (!Directory.Exists(oldPath))
            //    {
            //        Directory.CreateDirectory(oldPath);
            //    }
            //    var getBytes = Convert.FromBase64String(responseFile.Result);
            //    System.IO.File.WriteAllBytes(filePath, getBytes);
            //}

            //#endregion Get From API generate PDF 

            #endregion Generate PPPU
            Logger.InfoFormat("CreateBookingDocument() Ended.");
            return obj;
        }

        private async Task<string> ReadResponse(string url, HttpClient client, HttpRequestMessage request)
        {
            var response = await client.SendAsync(request);

            var success = await response.Content.ReadAsStringAsync();
            var indexFirstSlash = success.IndexOf(@"\");
            var trimString = success.Substring(indexFirstSlash + 1, success.Length).Trim(new char[1] { '"' });

            return trimString;
        }

        public void UploadTandaTerima(UpdateBookingDocumentInputDto input)
        {
            Logger.InfoFormat("UploadTandaTerima() Started.");
            var getDate = DateTime.Now;
            var getBookingDocument = (from x in _trBookingDocumentRepo.GetAll()
                                      where x.Id == input.trBookingDocumentID
                                      select x).FirstOrDefault();
            var updateBookingDocument = getBookingDocument.MapTo<TR_BookingDocument>();
            if (getDate.Date > getBookingDocument.docDate.Date)
            {
                //TODO: Update remarks to invalid
                updateBookingDocument.remarks = "Invalid";
                Logger.DebugFormat("UploadTandaTerima() - Start Update Remarks TR_BookingDocument. Parameters sent: {0} "
                , Environment.NewLine, updateBookingDocument.remarks);
                _trBookingDocumentRepo.Update(updateBookingDocument);
                Logger.DebugFormat("UploadTandaTerima() - End Update Remarks TR_BookingDocument");

                Logger.InfoFormat("UploadTandaTerima() Tanda Terima Date must be the same with the Document Date. Please re-generate the document.");
                throw new UserFriendlyException("Tanda Terima Date must be the same with the Document Date. Please re-generate the document.");
            }
            else
            {
                if (input.tandaTerimaFile != null)
                {
                    var tandaTerimaFile = uploadFile(input.tandaTerimaFile, @"Temp\Downloads\LegalDocument\TandaTerima\", @"Assets\Upload\LegalDocument\TandaTerima\");
                    GetURLWithoutHost(tandaTerimaFile, out tandaTerimaFile);
                    updateBookingDocument.tandaTerimaFile = tandaTerimaFile;
                }

                updateBookingDocument.tandaTerimaNo = input.tandaTerimaNo;
                updateBookingDocument.tandaTerimaDate = getDate;
                //updateBookingDocument.tandaTerimaBy = input.tandaTerimaBy;

                Logger.DebugFormat("UploadTandaTerima() - Start Preparation Data For Update TR_BookingDocument. Parameters sent: {0} " +
                "   entityID = 1" +
                "   bookingDocumentID = {1}{0}" +
                "   tandaTerimaFile = {2}{0}" +
                "   tandaTerimaNo = {3}{0}" +
                "   tandaTerimaDate = {4}{0}" +
                "   tandaTerimaBy = {5}{0}"
                , Environment.NewLine, input.trBookingDocumentID, updateBookingDocument.tandaTerimaFile, input.tandaTerimaNo, input.tandaTerimaDate, input.tandaTerimaBy);
                _trBookingDocumentRepo.Update(updateBookingDocument);
                Logger.DebugFormat("UploadTandaTerima() - End Update TR_BookingDocument");
            }
            Logger.InfoFormat("UploadTandaTerima() Ended.");
        }

        private CounterDocNoListDto CounterDocNo(int projectID, int docID, int coID)
        {
            CounterDocNoListDto counterNo = new CounterDocNoListDto();

            string docCodeNumber = null;
            int docIncInt;
            var getCounter = (from A in _docNoCounterRepo.GetAll()
                              where A.projectID == projectID && A.docID == docID && A.coID == coID
                              select A).FirstOrDefault();

            var getLastDate = (from A in _trBookingDocumentRepo.GetAll()
                               where A.entityID == 1 && A.docID == docID
                               orderby A.CreationTime descending
                               select A).FirstOrDefault();

            var getDocCode = (from A in _msDocumentPsRepo.GetAll()
                              where A.Id == docID
                              select A).FirstOrDefault();

            var getProjectCode = (from A in _msProjectRepo.GetAll()
                                  where A.Id == projectID
                                  select A).FirstOrDefault();

            if (getDocCode != null && getProjectCode != null)
            {
                var monthCompare = DateTime.Now.ToString("MM");
                var month = getLastDate != null ? getLastDate.docNo.Split('/')[2] : null;
                if (getCounter != null && getLastDate != null && month == monthCompare)
                {
                    var lastInc = Int32.Parse(getLastDate.docNo.Split("/")[0].TrimStart(new Char[] { '0' }));
                    var inc = lastInc + 1;
                    docIncInt = inc;
                    docCodeNumber = _iFilesHelper.ConvertDocIdToDocCode(inc);
                }
                else
                {
                    docIncInt = 1;
                    docCodeNumber = _iFilesHelper.ConvertDocIdToDocCode(1);
                }
                var docNo = docCodeNumber + "/" + getDocCode.docCode + "-" + getProjectCode.projectCode + "/" + DateTime.Now.ToString("MM") + "/" + DateTime.Now.Year.ToString();
                var tandaTerimaNo = docCodeNumber + "/" + getDocCode.docCode + "-" + getProjectCode.projectCode + "-TandaTerima/" + DateTime.Now.ToString("MM") + "/" + DateTime.Now.Year.ToString();
                counterNo.docNo = docNo;
                counterNo.tandaTerimaNo = tandaTerimaNo;

                var param = new DocNo_Counter
                {
                    docID = docID,
                    docNo = docIncInt,
                    coID = coID,
                    projectID = projectID
                };

                Logger.DebugFormat("CreateBookingDocument() - Start Insert DocNo_Counter. Parameters sent: {0} " +
                    "   docID = 1" +
                    "   docNo = {1}{0}" +
                    "   coID = {2}{0}" +
                    "   projectID = {3}{0}"
                    , Environment.NewLine, docID, docIncInt, coID, projectID);
                _docNoCounterRepo.Insert(param);
                Logger.DebugFormat("CreateBookingDocument() - End Inserting data");

                return counterNo;
            }
            else
            {
                throw new UserFriendlyException("Project Or Document is not found !");
            }
        }

        private string uploadFile(string filename, string pathTemp, string pathAsset)
        {
            try
            {
                return _iFilesHelper.MoveFilesLegalDoc(filename, pathTemp, pathAsset, null);
            }
            catch (Exception ex)
            {
                Logger.DebugFormat("test() - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Upload Error !", ex.Message);
            }
        }

        private void GetURLWithoutHost(string path, out string finalpath)
        {
            finalpath = path;
            try
            {
                Regex RegexObj = new Regex("[\\w\\W]*([\\/]Assets[\\w\\W\\s]*)");
                if (RegexObj.IsMatch(path))
                {
                    finalpath = RegexObj.Match(path).Groups[1].Value;
                }
            }
            catch (ArgumentException ex)
            {
            }
        }

        private string GetURLWithoutHost(string path)
        {
            string finalpath = path;
            try
            {
                Regex RegexObj = new Regex("[\\w\\W]*([\\/]Assets[\\w\\W\\s]*)");
                if (RegexObj.IsMatch(path))
                {
                    finalpath = RegexObj.Match(path).Groups[1].Value;
                }
            }
            catch (ArgumentException ex)
            {
            }
            return finalpath;
        }

        private string getAbsoluteUriWithoutTail()
        {
            var request = _httpContextAccessor.HttpContext.Request;
            UriBuilder uriBuilder = new UriBuilder();
            uriBuilder.Scheme = request.Scheme;
            uriBuilder.Host = request.Host.ToString();
            var test = uriBuilder.ToString();
            var result = test.Replace("[", "").Replace("]", "");
            int position = result.LastIndexOf('/');
            if (position > -1)
                result = result.Substring(0, result.Length - 1);

            if (request.PathBase != null)
            {
                if (!string.IsNullOrWhiteSpace(request.PathBase.Value))
                {
                    result += request.PathBase.Value;
                }
            }
            return result;
        }

        public P32DocumentDto GetDataGenerate(string docNo, int bookingHeaderID, string atasNama, int procurationType)
        {
            //The KTP address or if null, any 
            //provided address.
            StringBuilder dynamicIdAddress = new StringBuilder();
            StringBuilder dynamicIdNo = new StringBuilder();

            //The corespondence address or if null, any 
            //provided address.
            StringBuilder correspondenceAddress = new StringBuilder();

            var trbooking = (from A in _trBookingHeaderRepo.GetAll()
                             join B in _msUnitRepo.GetAll() on A.unitID equals B.Id
                             join C in _msUnitCodeRepo.GetAll() on B.unitCodeID equals C.Id
                             join D in _msTermMainRepo.GetAll() on B.termMainID equals D.Id
                             join E in _msTermRepo.GetAll() on D.termCode equals E.termCode
                             join F in _trBookingDetailRepo.GetAll() on A.Id equals F.bookingHeaderID
                             where A.entityID == 1
                             && A.Id == bookingHeaderID
                             && A.cancelDate == null
                             && F.itemID == 1
                             select new {
                                 bookingHeaderID = A.Id,
                                 bookCode = A.bookCode,
                                 bookDate = A.bookDate,
                                 unitCode = C.unitCode,
                                 unitNo = B.unitNo,
                                 psCode = A.psCode,
                                 termCode = D.termCode,
                                 termNo = E.termNo,
                                 coCode = F.coCode,
                                 unitID = B.Id,
                                 unitCodeID = C.Id,
                                 projectID = B.projectID
                             }).FirstOrDefault();

            if (trbooking == null)
            {
                throw new UserFriendlyException("Could not find booking with bookCode : " + trbooking.bookCode);
            }

            var marketingFloor = trbooking.unitCode.Split('-').Last();

            var luasArea = (from A in _trBookingHeaderRepo.GetAll()
                            join B in _trBookingDetailRepo.GetAll() on A.Id equals B.bookingHeaderID
                             where A.entityID == 1
                             && A.Id == bookingHeaderID
                             && A.cancelDate == null
                             group B by A.Id into BB
                             select BB.Sum(x => x.area)).FirstOrDefault();

            var unitOrder = (from A in _trUnitOrderDetailRepo.GetAll()
                             join B in _trUnitOrderHeaderRepo.GetAll() on A.UnitOrderHeaderID equals B.Id
                             where A.bookingHeaderID == trbooking.bookingHeaderID
                             orderby A.CreationTime descending
                             select new
                             {
                                 bookCode = trbooking.bookCode,
                                 orderCode = B.orderCode,
                                 orderID = B.Id,
                                 unitID = A.unitID,
                                 renovID = A.renovID,
                                 termID = A.termID,
                                 bfAmount = A.BFAmount,
                                 sellingPrice = A.sellingPrice,
                                 ppNo = A.PPNo,
                                 remarks = A.remarks,
                                 disc1 = A.disc1,
                                 disc2 = A.disc2,
                                 specialDiscType = A.specialDiscType,
                                 groupBU = A.groupBU
                             }).FirstOrDefault();

            if (unitOrder == null)
            {
                throw new UserFriendlyException("Could not find UnitOrderDetail with bookCode : " + trbooking.bookCode);
            }

            string PPNo = unitOrder.ppNo;
            string orderCode = unitOrder.orderCode;

            var PPDate = trbooking.bookDate.ToString("dd/MM/yyyy");

            //TODO: EntityID ?
            var personalInfo = (from A in _personalContext.PERSONAL
                                join B in _personalContext.TR_Address on A.psCode equals B.psCode into BB
                                from B in BB.DefaultIfEmpty()
                                join LkAddr in _personalContext.LK_AddrType on B.addrType equals LkAddr.addrType into LkAddrr
                                from LkAddr in LkAddrr.DefaultIfEmpty()
                                join C in _personalContext.TR_Phone on A.psCode equals C.psCode into CC
                                from C in CC.DefaultIfEmpty()
                                join D in _personalContext.TR_ID on A.psCode equals D.psCode into DD
                                from D in DD.DefaultIfEmpty()
                                join LkID in _personalContext.LK_IDType on D.idType equals LkID.idType into LkIDD
                                from LkID in LkIDD.DefaultIfEmpty()
                                join E in _personalContext.TR_Email on A.psCode equals E.psCode into EE
                                from E in EE.DefaultIfEmpty()
                                where A.psCode == trbooking.psCode //&& B.refID == 1
                                select new
                                {
                                    personalName = A.name,
                                    noIdentitas = D.idNo,
                                    personalAddress = B.address,
                                    personalNpwp = A.NPWP,
                                    personalEmail = E.email,
                                    personalPhone = C.number,
                                    bookCode = trbooking.bookCode,
                                    bookingDate = trbooking.bookDate.Date,
                                    tanggalGenerate = DateTime.Now.Date,
                                    addrType = B.addrType,
                                    addrName = LkAddr.addrTypeName,
                                    idType = D.idType,
                                    idName = LkID.idTypeName
                                }).ToList();

            if (personalInfo != null)
            {
                var personalAddressId = personalInfo.Any(x => x.addrName != null) ? personalInfo.Any(x => x.addrName.Equals("ID")) : false;
                var personalAddressCorress = personalInfo.Any(x => x.addrName != null) ? personalInfo.Any(x => x.addrName.Equals("Corress")) : false;
                if (personalAddressId)
                {
                    var dataAddr = personalInfo.Where(x => x.addrName.Equals("ID")).Select(x => x.personalAddress).FirstOrDefault();
                    dynamicIdAddress.Append(dataAddr);
                }
                else if (personalAddressCorress)
                {
                    var dataAddr = personalInfo.Where(x => x.addrName.Equals("Corress")).Select(x => x.personalAddress).FirstOrDefault();
                    dynamicIdAddress.Append(dataAddr);
                }
                else
                {
                    dynamicIdAddress.Append("-");
                }

                if (personalAddressCorress)
                {
                    var dataAddr = personalInfo.Where(x => x.addrName.Equals("Corress")).Select(x => x.personalAddress).FirstOrDefault();
                    correspondenceAddress.Append(dataAddr);
                }
                else if (personalAddressId)
                {
                    var dataAddr = personalInfo.Where(x => x.addrName.Equals("ID")).Select(x => x.personalAddress).FirstOrDefault();
                    correspondenceAddress.Append(dataAddr);
                }
                else
                {
                    correspondenceAddress.Append("-");
                }

                var personalIdKtp = personalInfo.Any(x => x.idName != null) ? personalInfo.Any(x => x.idName.Equals("KTP")) : false;
                var personalIdSim = personalInfo.Any(x => x.idName != null) ? personalInfo.Any(x => x.idName.Equals("SIM")) : false;
                var personalIdKitas = personalInfo.Any(x => x.idName != null) ? personalInfo.Any(x => x.idName.Equals("KITAS")) : false;
                if (personalIdKtp)
                {
                    var dataIdentitas = personalInfo.Where(x => x.idName.Equals("KTP")).Select(x => x.noIdentitas).FirstOrDefault();
                    dynamicIdNo.Append(dataIdentitas);
                }
                else if (personalIdSim)
                {
                    var dataIdentitas = personalInfo.Where(x => x.idName.Equals("SIM")).Select(x => x.noIdentitas).FirstOrDefault();
                    dynamicIdNo.Append(dataIdentitas);
                }
                else if (personalIdKitas)
                {
                    var dataIdentitas = personalInfo.Where(x => x.idName.Equals("KITAS")).Select(x => x.noIdentitas).FirstOrDefault();
                    dynamicIdNo.Append(dataIdentitas);
                }
                else
                {
                    dynamicIdNo.Append("-");
                }
            }

            //var msUnitVA
            //TODO: Ambil data company
            var msCompany = _msCompanyRepo
                .GetAll()
                .Where(p => p.coCode == trbooking.coCode)
                .OrderBy(p => p.coCode)
                .ThenBy(p => p.coName)
                .FirstOrDefault();

            var msProject = _msProjectRepo
                .GetAll()
                .Where(p => p.Id == trbooking.projectID)
                .FirstOrDefault();

            var msTerm = _msTermRepo
                .GetAll()
                .Where(p => p.termCode == trbooking.termCode && p.termNo == trbooking.termNo)
                .ToList();

            var trBookingDetails = (from A in _trBookingDetailRepo.GetAll()
                                    join B in _trBookingHeaderRepo.GetAll() on A.bookingHeaderID equals B.Id
                                    where B.bookCode == trbooking.bookCode
                                    select A);

            var bookingDetails = trBookingDetails.Sum(x => (double)x.netNetPrice + (x.pctTax * (double)x.netNetPrice));

            var bfAmount = trBookingDetails.Sum(x => x.BFAmount);

            var clusterCode = (from A in _msUnitRepo.GetAll()
                               join C in _msClusterRepo.GetAll() on A.clusterID equals C.Id
                               where A.Id == trbooking.unitID
                               select C.clusterCode).FirstOrDefault();

            var msCluster = _msClusterRepo
                .GetAll()
                .Where(p => p.clusterCode == clusterCode)
                .ToList();

            //var bookingDetailID = (from A in _trBookingDetailRepo.GetAll()
            //                       join B in _trBookingHeaderRepo.GetAll() on A.bookingHeaderID equals B.Id
            //                       where B.bookCode == trbooking.bookCode
            //                       select B.Id).FirstOrDefault();

            //var listBookingSchedule = _trBookingDetailScheduleRepo.GetAll().Where(x => x.bookingDetailID == bookingDetailID).ToList();

            //var groupingHeaders = listBookingSchedule
            //    .GroupBy(x => new { x.schedNo, x.allocID, x.dueDate });

            //if (!(msTerm[0].remarks.Contains("KPA") || msTerm[0].remarks.Contains("CASH")))
            //{
            //    groupingHeaders = listBookingSchedule
            //   .Where(x => x.allocID != 4)
            //   .GroupBy(x => new { x.schedNo, x.allocID, x.dueDate });
            //}

            //var groupingDetails = listBookingSchedule
            //        .Where(x => x.allocID != 4)
            //        .GroupBy(x => new { x.schedNo, x.allocID, x.dueDate });

            //var headers = groupingHeaders.Select(header =>
            //{
            //    var jenisPembayaran = "";

            //    if (header.Key.allocID == 2)
            //    {
            //        jenisPembayaran = "Booking Fee";
            //    }
            //    else if (header.Key.allocID == 3)
            //    {
            //        jenisPembayaran = "Down Payment";
            //    }
            //    else if (header.Key.allocID == 4 && msTerm[0].remarks.Contains("KPA"))
            //    {
            //        jenisPembayaran = "Pencairan KPA";
            //    }
            //    else if (header.Key.allocID == 4 && msTerm[0].remarks.Contains("CASH"))
            //    {
            //        jenisPembayaran = "Pelunasan";
            //    }

            //    return new
            //    {
            //        AllocationCode = jenisPembayaran,
            //        DueDate = header.Key.dueDate.ToString("dd/MM/yyyy"),
            //        TotalAmount = header.Sum(v => (v.netAmt + v.vatAmt)).ToString("#,###")
            //    };
            //});

            //var nomorAngsuran = 1;
            //var installments = groupingDetails.OrderBy(x => x.Key.schedNo).Select(x =>
            //{
            //    var jenisAngsuran = "";

            //    if (!(msTerm[0].remarks.Contains("KPA") || msTerm[0].remarks.Contains("CASH")))
            //    {
            //        jenisAngsuran = "Ke-" + nomorAngsuran;
            //        nomorAngsuran++;

            //        return new
            //        {
            //            AllocationCode = jenisAngsuran,
            //            DueDate = x.Key.dueDate.ToString("dd/MM/yyyy"),
            //            TotalAmount = x.Sum(v => (v.netAmt + v.vatAmt)).ToString("#,###")
            //        };
            //    }

            //    return new
            //    {
            //        AllocationCode = "",
            //        DueDate = "",
            //        TotalAmount = ""
            //    };
            //});

            //var unitItem = _msUnitItemRepo.FirstOrDefault(x => x.unitID == trbooking.unitID && x.itemID == 2);

            KonfirmasiPesananDto getKP = null; //GetKP(orderCode);
            if (getKP != null)
            {
                getKP.bfAmount = bfAmount.ToString();
            }

            P32DocumentDto returningData = new P32DocumentDto();
            returningData.DocNo = docNo;
            returningData.DocDate = DateTime.Now.Date.ToString();
            returningData.Nama = personalInfo.Any() ? personalInfo.FirstOrDefault().personalName : "";
            returningData.AlamatKTP = dynamicIdAddress.ToString();
            returningData.Email = personalInfo.Any() ? personalInfo.FirstOrDefault().personalEmail : "";
            returningData.NoKTP = dynamicIdNo.ToString();
            returningData.NoNPWP = personalInfo.Any() ? personalInfo.FirstOrDefault().personalNpwp : "";
            returningData.Handphone = personalInfo.Any() ? personalInfo.FirstOrDefault().personalPhone : "";
            returningData.BookCode = trbooking.bookCode;
            returningData.UnitCode = trbooking.unitCode;
            returningData.UnitNo = trbooking.unitNo;
            //returningData.VirtualAccountNumber = msUnitVA.Items.Any() ? msUnitVA.Items[0].VA_BankAccNo : "";
            CompanyDto CompanyParam = new CompanyDto();
            CompanyParam.companyName = msCompany != null ? msCompany.coName.ToUpper() : null;
            CompanyParam.SADEmail = msProject != null ? msProject.SADEmail : null;
            CompanyParam.companyAddress = msCompany != null ? msCompany.address : null;
            CompanyParam.companyPhone = msCompany != null ? msCompany.phoneNo : null;
            CompanyParam.companyWebsite = msCompany != null ? msCompany.website : null;

            returningData.Company = CompanyParam;

            returningData.SpecialCicilan = msTerm.Any() ? msTerm.FirstOrDefault().remarks : "";
            returningData.SpecialCicilanAmount = bookingDetails.ToString("#;###");
            returningData.HandOverPeriod = msCluster.Any() ? msCluster.FirstOrDefault().handOverPeriod : "";
            returningData.GracePeriod = msCluster.Any() ? msCluster.FirstOrDefault().gracePeriod : "";
            returningData.NameOfProcuration = atasNama != "" ? atasNama + " " : "";
            returningData.TypeOfProcuration = procurationType != 0 ? procurationType == 1 ? " selaku wali dari anak dibawah umur bernama " : " selaku kuasa dari " : "";
            returningData.PPNo = PPNo;
            returningData.PPDate = PPDate;
            returningData.OrderCode = orderCode;
            returningData.AlamatCorrespondence = correspondenceAddress.ToString();
            returningData.KP = getKP;
            returningData.UnitID = trbooking.unitID;
            returningData.OrderID = unitOrder.orderID;
            returningData.komplek = msProject != null ? msProject.projectName : null;
            returningData.marketingFloor = marketingFloor;
            returningData.luas = luasArea.ToString();

            return returningData;
        }

        private KonfirmasiPesananDto GetKP(string orderCode)
        {
            //var dataKP = (from x in _propertySystemContext.TR_UnitOrderHeader
            //              join a in _propertySystemContext.TR_UnitOrderDetail on x.Id equals a.UnitOrderHeaderID
            //              join b in _propertySystemContext.MS_Unit on a.unitID equals b.Id
            //              join d in _personalContext.PERSONAL.ToList() on x.psCode equals d.psCode
            //              join e in _propertySystemContext.TR_BookingHeader on a.bookingHeaderID equals e.Id
            //              join f in _propertySystemContext.MS_TujuanTransaksi on e.tujuanTransaksiID equals f.Id
            //              join g in _propertySystemContext.MS_SumberDana on e.sumberDanaID equals g.Id
            //              join h in _personalContext.TR_ID.ToList() on x.psCode equals h.psCode into iden
            //              from h in iden.DefaultIfEmpty()
            //              //join i in _newCommContext.MS_Schema.ToList() on e.scmCode equals i.scmCode
            //              join j in _personalContext.PERSONALS_MEMBER.ToList() on new { e.memberCode, e.scmCode } equals new { j.memberCode, j.scmCode }
            //              join k in _personalContext.PERSONAL.ToList() on j.psCode equals k.psCode
            //              join l in _personalContext.TR_Phone.ToList() on k.psCode equals l.psCode into phone
            //              from l in phone.DefaultIfEmpty()
            //              join m in _propertySystemContext.MS_Project on b.projectID equals m.Id
            //              join p in _propertySystemContext.MS_Detail on b.detailID equals p.Id
            //              join s in _propertySystemContext.MS_Term on e.termID equals s.Id
            //              where x.orderCode == orderCode && new string[] { "1", "5", "7" }.Contains(h.idType)
            //              select new KonfirmasiPesananDto
            //              {
            //                  orderCode = x.orderCode,
            //                  kodePelanggan = x.psCode,
            //                  tanggalBooking = e.bookDate.ToString(),
            //                  psName = x.psName,
            //                  birthDate = d.birthDate.ToString(),
            //                  noHpPembeli = x.psPhone,
            //                  noIdentitas = (h == null ? "-" : h.idNo),
            //                  noNPWP = d.NPWP,
            //                  email = x.psEmail,
            //                  BookCode = e.bookCode,
            //                  hargaJual = a.sellingPrice.ToString(),
            //                  bfAmount = a.BFAmount.ToString(),
            //                  imageProject = m.image,
            //                  noHp = l.number,
            //                  noDealCloser = k.psCode,
            //                  namaDealCloser = k.name,
            //                  caraPembayaran = s.remarks,
            //                  tujuanTransaksi = f.tujuanTransaksiName,
            //                  sumberDanaPembelian = g.sumberDanaName,
            //                  unitID = e.unitID
            //              }).FirstOrDefault();

            var dataOrder = (from a in _propertySystemContext.TR_UnitOrderHeader
                             join b in _propertySystemContext.TR_UnitOrderDetail on a.Id equals b.UnitOrderHeaderID
                             join trBookingHeader in _propertySystemContext.TR_BookingHeader on b.bookingHeaderID equals trBookingHeader.Id
                             join c in _propertySystemContext.MS_SumberDana on a.sumberDanaID equals c.Id into sumda
                             from c in sumda.DefaultIfEmpty()
                             join d in _propertySystemContext.MS_TujuanTransaksi on a.tujuanTransaksiID equals d.Id into tutran
                             from d in tutran.DefaultIfEmpty()
                             join e in _msUnitRepo.GetAll() on b.unitID equals e.Id
                             join f in _msProjectRepo.GetAll() on e.projectID equals f.Id
                             where a.orderCode == orderCode
                             select new KonfirmasiPesananDto
                             {
                                 orderCode = a.orderCode,
                                 unitID = b.unitID,
                                 sumberDanaPembelian = c.sumberDanaName,
                                 tujuanTransaksi = d.tujuanTransaksiName,
                                 imageProject = f.image != null ? f.image : "-",
                                 kodePelanggan = a.psCode,
                                 psName = a.psName,
                                 noHpPembeli = a.psPhone,
                                 email = a.psEmail,
                                 noDealCloser = a.memberCode,
                                 namaDealCloser = a.memberName,
                                 namaBank = a.bankRekeningPemilik,
                                 noRekening = a.nomorRekeningPemilik,
                                 bfAmount = b.BFAmount.ToString(),
                                 hargaJual = b.sellingPrice.ToString(),
                                 tanggalBooking = trBookingHeader.bookDate.ToString(),
                                 renovID = b.renovID,
                                 caraPembayaran = trBookingHeader.termRemarks,
                                 termBookingHeaderID = trBookingHeader.termID
                             }).FirstOrDefault();

            var getCustomerInfo = (from a in _personalContext.PERSONAL
                                   join b in _personalContext.TR_ID on a.psCode equals b.psCode into idno
                                   from b in idno.DefaultIfEmpty()
                                   where a.psCode == dataOrder.kodePelanggan
                                   select new
                                   {
                                       a.birthDate,
                                       a.NPWP,
                                       idNo = b.idNo != null ? b.idNo : "-"
                                   }).FirstOrDefault();

            dataOrder.birthDate = getCustomerInfo.birthDate.ToString();
            dataOrder.noNPWP = getCustomerInfo.NPWP;
            dataOrder.noIdentitas = getCustomerInfo.idNo;

            var getMember = (from a in _personalContext.PERSONALS_MEMBER
                             join b in _personalContext.TR_Phone on a.psCode equals b.psCode into phone
                             from b in phone.DefaultIfEmpty()
                             where a.memberCode == dataOrder.noDealCloser
                             select b.number).FirstOrDefault();

            dataOrder.noHp = getMember;

            var getDataUnit = (from a in _propertySystemContext.MS_Unit
                               join b in _propertySystemContext.MS_UnitCode on a.unitCodeID equals b.Id
                               join c in _propertySystemContext.MS_Cluster on a.clusterID equals c.Id
                               join d in _propertySystemContext.MS_Category on a.categoryID equals d.Id
                               join e in _propertySystemContext.MS_UnitItem on a.Id equals e.unitID
                               join h in _propertySystemContext.MS_Detail on a.detailID equals h.Id
                               group e by new
                               {
                                   a.unitNo,
                                   b.unitCode,
                                   c.clusterName,
                                   d.categoryName,
                                   h.detailName,
                                   a.Id
                               } into G
                               where G.Key.Id == dataOrder.unitID
                               select new ListunitDto
                               {
                                   UnitNo = G.Key.unitNo,
                                   UnitCode = G.Key.unitCode,
                                   cluster = G.Key.clusterName,
                                   category = G.Key.categoryName,
                                   tipe = G.Key.detailName,
                                   luas = G.Sum(a => a.area).ToString()
                               }).ToList();

            var getRenov = (from a in _propertySystemContext.MS_Renovation
                            where a.Id == dataOrder.renovID
                            select a.renovationName).FirstOrDefault();

            getDataUnit.FirstOrDefault().renovation = getRenov;

            dataOrder.listUnit = getDataUnit;

            var dataBank = (from bank in _propertySystemContext.MS_BankOLBooking
                            join unit in _propertySystemContext.MS_Unit on new { bank.projectID, bank.clusterID } equals new { unit.projectID, unit.clusterID }
                            where unit.Id == dataOrder.unitID
                            select new OnlineBooking.PaymentMidtrans.Dto.listBankDto
                            {
                                bankName = bank.bankName,
                                noVA = bank.bankRekNo
                            }).ToList();

            dataOrder.listBank = dataBank;

            Logger.InfoFormat(JsonConvert.SerializeObject(dataBank));

            var arrTerm = new List<listIlustrasiPembayaran>();

            var getTerm = (from a in _propertySystemContext.TR_UnitOrderHeader
                           join b in _propertySystemContext.TR_UnitOrderDetail on a.Id equals b.UnitOrderHeaderID
                           where dataOrder.unitID == b.unitID
                           select new
                           {
                               termID = b.termID,
                               bookingFee = b.BFAmount,
                               sellingPrice = b.sellingPrice,
                               orderHeaderID = a.Id
                           }).FirstOrDefault();

            var getBookdate = DateTime.Parse(dataOrder.tanggalBooking);

            //var getHeaderTerm = insertTrBookingHeaderTerm.Where(a => a.termID == getTerm.termID).FirstOrDefault();

            var dataIlusTerm = new listIlustrasiPembayaran
            {
                bookingFee = getTerm.bookingFee,
                sellingPrice = getTerm.sellingPrice,
                orderHeaderID = getTerm.orderHeaderID,
                termID = dataOrder.termBookingHeaderID,
                //termName = getHeaderTerm.remarks,
                tglJatuhTempo = getBookdate
            };

            arrTerm.Add(dataIlusTerm);

            var getDPTerm = (from a in _propertySystemContext.MS_TermDP
                             where a.termID == dataIlusTerm.termID
                             select new
                             {
                                 a.DPNo,
                                 a.DPPct,
                                 a.daysDue,
                                 tglJatuhTempo = getBookdate,
                                 a.termID
                             }).ToList();

            var arrDPNo = new List<DPNoDto>();

            var dataTerm = new List<DPNoDtos>();

            foreach (var dto in getDPTerm)
            {
                var dataListDPNos = new DPNoDtos
                {
                    DPNo = dto.DPNo,
                    tglJatuhTempo = dto.tglJatuhTempo.AddDays(dto.daysDue),
                    termID = dto.termID
                };
                dataTerm.Add(dataListDPNos);
            }

            var datas = new DPNoDto
            {
                listDPNos = dataTerm,
                termID = dataIlusTerm.termID,
                countDP = dataTerm.Count
            };
            arrDPNo.Add(datas);

            //var getTop5 = (from a in insertTrBookingHeaderTerm
            //               where a.termID != getTerm.termID
            //               select new listIlustrasiPembayaran
            //               {
            //                   termID = a.termID,
            //                   termName = a.remarks,
            //                   tglJatuhTempo = getBookdate,
            //                   bookingFee = getTerm.bookingFee
            //               }).Take(5).ToList();

            //foreach (var top5 in getTop5)
            //{
            //    var listGP = new List<ListPriceDto>();

            //    var getGrossprice = (from a in insertTrBookingItemPrice
            //                         join b in _propertySystemContext.MS_UnitItem on new { a.itemID, item.unitID } equals new { b.itemID, b.unitID }
            //                         where a.termID == top5.termID
            //                         select new ListPriceDto
            //                         {
            //                             grossprice = a.grossPrice,
            //                             pctTax = b.pctTax,
            //                             pctDisc = b.pctDisc
            //                         }).ToList();

            //    foreach (var items in getGrossprice)
            //    {
            //        items.grossprice = items.grossprice * (1 - (decimal)items.pctDisc);

            //        items.grossprice = items.grossprice * (1 - (decimal)items.pctTax);
            //    }

            //    listGP = getGrossprice;

            //    top5.sellingPrice = listGP.Sum(a => a.grossprice);

            //    var getDisc = (from a in _propertySystemContext.MS_TermAddDisc
            //                   where a.termID == top5.termID
            //                   select new
            //                   {
            //                       a.addDiscPct
            //                   }).ToList();

            //    foreach (var disc in getDisc)
            //    {
            //        top5.sellingPrice = Math.Round(top5.sellingPrice * (1 - (decimal)disc.addDiscPct));
            //    }

            //    var dataDPNo = new List<DPNoDtos>();

            //    var getDPTop5 = (from b in _propertySystemContext.MS_TermDP
            //                     where b.termID == top5.termID
            //                     select new
            //                     {
            //                         b.DPNo,
            //                         top5.tglJatuhTempo,
            //                         b.daysDue,
            //                         top5.termID
            //                     }).ToList();

            //    foreach (var itm in getDPTop5)
            //    {
            //        var dataDP = new DPNoDtos
            //        {
            //            DPNo = itm.DPNo,
            //            tglJatuhTempo = itm.tglJatuhTempo.AddDays(itm.daysDue),
            //            termID = itm.termID
            //        };
            //        dataDPNo.Add(dataDP);
            //    }

            //    var dataListDPNo = new DPNoDto
            //    {
            //        listDPNos = dataDPNo,
            //        countDP = dataDPNo.Count,
            //        termID = top5.termID
            //    };

            //    arrDPNo.Add(dataListDPNo);

            //    var dataTop5 = new listIlustrasiPembayaran
            //    {
            //        termID = top5.termID,
            //        termName = top5.termName,
            //        tglJatuhTempo = top5.tglJatuhTempo,
            //        sellingPrice = top5.sellingPrice,
            //        bookingFee = top5.bookingFee
            //    };

            //    arrTerm.Add(dataTop5);
            //}

            var DPNO = arrDPNo.OrderByDescending(a => a.countDP).FirstOrDefault();

            var allAmount = new List<listAllAmount>();

            foreach (var listDP in DPNO.listDPNos)
            {
                var amountList = new List<listAmountDto>();

                foreach (var dto in arrTerm)
                {
                    var getDPAmountTop5 = (from a in arrTerm
                                           join b in _propertySystemContext.MS_TermDP on a.termID equals b.termID
                                           where a.termID == dto.termID
                                                 && listDP.tglJatuhTempo == a.tglJatuhTempo.AddDays(b.daysDue)
                                           select new
                                           {
                                               b.DPPct,
                                               b.daysDue
                                           });

                    if (getDPAmountTop5.FirstOrDefault() != null)
                    {
                        var dataAmount = new listAmountDto
                        {
                            amount = Math.Round(dto.sellingPrice * (decimal)getDPAmountTop5.FirstOrDefault().DPPct, 0),
                            termID = dto.termID,
                            tglJatuhTempo = listDP.tglJatuhTempo
                        };
                        var dataAllAmount = new listAllAmount
                        {
                            amount = Math.Round(dto.sellingPrice * (decimal)getDPAmountTop5.FirstOrDefault().DPPct, 0),
                            termID = dto.termID,
                            tglJatuhTempo = listDP.tglJatuhTempo
                        };

                        amountList.Add(dataAmount);
                        allAmount.Add(dataAllAmount);
                    }
                    else
                    {
                        var dataAmount = new listAmountDto
                        {
                            amount = 0,
                            termID = dto.termID
                        };
                        var dataAllAmount = new listAllAmount
                        {
                            amount = 0,
                            termID = dto.termID
                        };
                        allAmount.Add(dataAllAmount);
                        amountList.Add(dataAmount);
                    }
                }
                listDP.listAmount = amountList;
            }

            var totalDP = (from a in allAmount
                           group a by a.termID into G
                           select new
                           {
                               termID = G.Key,
                               total = G.Sum(a => a.amount)
                           }).ToList();

            var listFin = new List<listFinTimesDto>();

            foreach (var term in arrTerm)
            {
                var dataTotalAmount = totalDP.Where(a => a.termID == dataOrder.termBookingHeaderID).FirstOrDefault().total;

                term.totalDP = dataTotalAmount;

                var lastSellingPrice = term.sellingPrice - (term.bookingFee + term.totalDP);

                term.leftSellingPrice = lastSellingPrice;

                var getFin = (from a in _propertySystemContext.MS_TermPmt
                              join b in _propertySystemContext.LK_FinType on a.finTypeID equals b.Id
                              where a.termID == dataOrder.termBookingHeaderID
                              select new listFinTimesDto
                              {
                                  termID = a.termID,
                                  finStartDue = a.finStartDue,
                                  finTimes = b.finTimes
                              }).FirstOrDefault();

                listFin.Add(getFin);
            }

            var finTimesTerbersar = listFin.OrderByDescending(a => a.finTimes).FirstOrDefault();

            var listCicilan = new List<listCicilans>();

            for (var i = 1; i <= finTimesTerbersar.finTimes; i++)
            {
                var getTgl = DPNO.listDPNos.OrderByDescending(a => a.tglJatuhTempo).FirstOrDefault();

                var getTglJatuhTempo = getTgl.tglJatuhTempo.AddDays(finTimesTerbersar.finStartDue);

                if (i == 1)
                {
                    var dataCicilan = new listCicilans
                    {
                        cicilanNo = i,
                        tglJatuhTempo = getTglJatuhTempo
                    };

                    listCicilan.Add(dataCicilan);

                }
                else
                {
                    var dataCicilan = new listCicilans
                    {
                        cicilanNo = i,
                        tglJatuhTempo = getTglJatuhTempo.AddMonths(i)
                    };
                    listCicilan.Add(dataCicilan);
                }
            }

            var listPelunasan2 = new List<ListPenulasanDto>();
            var listPelunasan1 = new List<ListPenulasanDto>();

            for (var x = 1; x <= arrTerm.Count; x++)
            {
                var listCicil = new List<listCicilanDto>();

                var getFinTerm = (from b in _propertySystemContext.MS_TermPmt
                                  join c in _propertySystemContext.LK_FinType on b.finTypeID equals c.Id
                                  where b.termID == arrTerm[x - 1].termID
                                  select new
                                  {
                                      c.finTimes,
                                      b.termID,
                                      b.finStartDue
                                  }).FirstOrDefault();

                var cicilanAmount = arrTerm[x - 1].leftSellingPrice / getFinTerm.finTimes;

                if (getFinTerm.finTimes != 1)
                {
                    for (var i = 1; i <= getFinTerm.finTimes; i++)
                    {
                        var dataCicil = new listCicilanDto
                        {
                            amount = cicilanAmount,
                            cicilanNo = i
                        };
                        listCicil.Add(dataCicil);
                    }

                    arrTerm[x - 1].listCicilan = listCicil;
                }
                else
                {
                    if (x == 1)
                    {
                        var dataPelunasan1 = new ListPenulasanDto
                        {
                            amount = cicilanAmount,
                            termID = arrTerm[x - 1].termID,
                            finStartDue = getFinTerm.finStartDue
                        };

                        listPelunasan1.Add(dataPelunasan1);

                        var dataPelunasan = new ListPenulasanDto
                        {
                            amount = 0,
                            termID = arrTerm[x - 1].termID
                        };
                        listPelunasan2.Add(dataPelunasan);
                    }
                    else
                    {
                        var dataPelunasan = new ListPenulasanDto
                        {
                            amount = cicilanAmount,
                            termID = arrTerm[x - 1].termID,
                            finStartDue = getFinTerm.finStartDue
                        };
                        listPelunasan2.Add(dataPelunasan);

                        var dataPelunasan1 = new ListPenulasanDto
                        {
                            amount = 0,
                            termID = arrTerm[x - 1].termID
                        };

                        listPelunasan1.Add(dataPelunasan1);
                    }
                }
            }

            foreach (var cicilan in listCicilan)
            {
                var amtCicilan = new List<listCicilanAmount>();

                foreach (var dto in arrTerm)
                {
                    if (dto.listCicilan != null)
                    {
                        var getAmount = (from a in dto.listCicilan
                                         where a.cicilanNo == cicilan.cicilanNo
                                         select a.amount).FirstOrDefault();


                        var amountCicilan = new listCicilanAmount
                        {
                            amount = getAmount != 0 ? getAmount : 0
                        };
                        amtCicilan.Add(amountCicilan);
                    }
                }
                cicilan.listCicilanAmount = amtCicilan;
            }

            foreach (var dp in DPNO.listDPNos)
            {
                if (listPelunasan1 != null)
                {
                    foreach (var dto in listPelunasan1)
                    {
                        var getDP = (from a in dp.listAmount
                                     where a.termID == dto.termID && a.amount != 0
                                     orderby dp.tglJatuhTempo descending
                                     select new
                                     {
                                         tgl = dp.tglJatuhTempo
                                     }).FirstOrDefault();

                        if (getDP != null)
                        {
                            dto.tglJatuhTempo = getDP.tgl.AddDays(dto.finStartDue);
                        }
                        else
                        {
                            dto.tglJatuhTempo = null;
                        }
                    }
                }

                if (listPelunasan2 != null)
                {
                    foreach (var dto in listPelunasan2)
                    {
                        var getDP = (from a in dp.listAmount
                                     where a.termID == dto.termID && a.amount != 0
                                     orderby dp.tglJatuhTempo descending
                                     select new
                                     {
                                         tgl = dp.tglJatuhTempo
                                     }).FirstOrDefault();

                        if (getDP != null)
                        {
                            dto.tglJatuhTempo = getDP.tgl.AddDays(dto.finStartDue);
                        }
                        else
                        {
                            dto.tglJatuhTempo = null;
                        }
                    }
                }
            }


            dataOrder.listPelunasan2 = listPelunasan2;
            dataOrder.listPelunasan1 = listPelunasan1;
            dataOrder.listCicilan = listCicilan;
            dataOrder.listDPNo = DPNO.listDPNos;
            dataOrder.ilustrasiPembayaran = arrTerm;

            return dataOrder;
        }

        public GetFilePPPUListDto GetFilePPPU(GetFilePPPUInputDto input)
        {
            GetFilePPPUListDto getFilePPPU = new GetFilePPPUListDto();

            var getBookingDocumentData = (from A in _trBookingDocumentRepo.GetAll()
                                          where A.Id == input.bookingDocumentID
                                          select A).FirstOrDefault();

            if (getBookingDocumentData != null)
            {
                var cekLinkFilePPPU = _hostingEnvironment.WebRootPath + @"\Assets\Upload\LegalDocument\PPPU\" + input.bookCode + @"\" + getBookingDocumentData.docNo.Replace("/", "-") + ".pdf";

                StringBuilder linkFileBuild = new StringBuilder();
                linkFileBuild.Append("/Assets/Upload/LegalDocument/PPPU/" + input.bookCode + "/" + getBookingDocumentData.docNo.Replace("/", "-") + ".pdf");
                var linkFilePPPU = getAbsoluteUriWithoutTail() + GetURLWithoutHost(linkFileBuild.ToString());

                if (File.Exists(cekLinkFilePPPU))
                {
                    getFilePPPU.linkFilePPPU = linkFilePPPU;
                }
                else
                {
                    throw new UserFriendlyException("File is not found!");
                }

                return getFilePPPU;
            }
            else
            {
                throw new UserFriendlyException("File is not found!");
            }
        }

        public List<DocCodeDto> GetDocCodeDropDown()
        {
            List<DocCodeDto> docCodeData = new List<DocCodeDto>();
            var getDocId = (from x in _msMappingTemplateRepo.GetAll()
                            group x by x.docID into G
                            select G.Key).ToList();

            var getDocMaster = (from x in _msDocTemplateRepo.GetAll()
                                join y in _msDocumentPsRepo.GetAll() on x.docID equals y.Id
                                where !getDocId.Contains(x.docID) && x.isMaster.GetValueOrDefault()
                                select new DocCodeDto
                                {
                                    docID = y.Id,
                                    docCode = y.docCode
                                }).ToList();

            docCodeData.AddRange(getDocMaster);

            foreach (var docID in getDocId)
            {
                var getDocCode = (from y in _msDocumentPsRepo.GetAll()
                                  where y.Id == docID
                                  select new DocCodeDto
                                  {
                                      docID = y.Id,
                                      docCode = y.docCode
                                  }).FirstOrDefault();

                docCodeData.Add(getDocCode);
            } 

            return docCodeData;
        }
    }
}
