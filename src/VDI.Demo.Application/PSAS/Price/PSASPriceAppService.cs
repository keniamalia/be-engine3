﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.PropertySystemDB.LippoMaster;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;
using VDI.Demo.PSAS.Dto;
using System.Linq;
using VDI.Demo.PSAS.Price.Dto;
using Abp.UI;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;
using System.Threading.Tasks;
using Abp.AutoMapper;
using System.Data;
using VDI.Demo.PSAS.Term.Dto;
using System.Diagnostics;
using Newtonsoft.Json;
using Visionet_Backend_NetCore.Komunikasi;
using VDI.Demo.EntityFrameworkCore;

namespace VDI.Demo.PSAS.Price
{
    public class PSASPriceAppService : DemoAppServiceBase, IPSASPriceAppService
    {
        private readonly IRepository<TR_BookingHeader> _trBookingHeaderRepo;
        private readonly IRepository<TR_BookingDetail> _trBookingDetailRepo;
        private readonly IRepository<TR_BookingDetailAddDisc> _trBookingDetailAddDiscRepo;
        private readonly IRepository<MS_Unit> _msUnitRepo;
        private readonly IRepository<MS_UnitCode> _msUnitCodeRepo;
        private readonly IRepository<MS_Project> _msProjectRepo;
        private readonly IRepository<TR_BookingItemPrice> _trBookingItemPriceRepo;
        private readonly IRepository<TR_BookingSalesDisc> _trBookingSalesDiscRepo;
        private readonly PropertySystemDbContext _contextProp;
        private readonly IRepository<TR_BookingDetailAddDiscHistory> _trBookingDetailAddDiscHistory;
        private readonly IRepository<TR_MKTAddDisc> _trMktAddDiscRepo;
        private readonly IRepository<TR_CommAddDisc> _trCommAddDiscRepo;
        private readonly IRepository<TR_BookingDetailHistory> _trBookingDetailHistoryRepo;
        private readonly IRepository<LK_Item> _lkItemRepo;

        public PSASPriceAppService(
            IRepository<TR_BookingHeader> trBookingHeaderRepo,
            IRepository<TR_BookingDetail> trBookingDetailRepo,
            IRepository<TR_BookingDetailAddDisc> trBookingDetailAddDiscRepo,
            IRepository<MS_Unit> msUnitRepo,
            IRepository<MS_UnitCode> msUnitCodeRepo,
            IRepository<MS_Project> msProjectRepo,
            IRepository<TR_BookingItemPrice> trBookingItemPriceRepo,
            IRepository<TR_BookingSalesDisc> trBookingSalesDiscRepo,
            PropertySystemDbContext contextProp,
            IRepository<TR_BookingDetailAddDiscHistory> trBookingDetailAddDiscHistoryRepo,
            IRepository<TR_MKTAddDisc> trMktAddDiscRepo,
            IRepository<TR_CommAddDisc> trCommAddDiscRepo,
            IRepository<TR_BookingDetailHistory> trBookingDetailHistoryRepo,
            IRepository<LK_Item> lkItemRepo
            )
        {
            _trBookingHeaderRepo = trBookingHeaderRepo;
            _trBookingDetailRepo = trBookingDetailRepo;
            _msUnitRepo = msUnitRepo;
            _msUnitCodeRepo = msUnitCodeRepo;
            _trBookingDetailAddDiscRepo = trBookingDetailAddDiscRepo;
            _msProjectRepo = msProjectRepo;
            _trBookingItemPriceRepo = trBookingItemPriceRepo;
            _trBookingSalesDiscRepo = trBookingSalesDiscRepo;
            _contextProp = contextProp;
            _trBookingDetailAddDiscHistory = trBookingDetailAddDiscHistoryRepo;
            _trMktAddDiscRepo = trMktAddDiscRepo;
            _trCommAddDiscRepo = trCommAddDiscRepo;
            _trBookingDetailHistoryRepo = trBookingDetailHistoryRepo;
            _lkItemRepo = lkItemRepo;
        }

        public void CreateUpdateDiscountPrice(CreateUpdatePriceParamsDto input)
        {
            Logger.Info("CreateUpdateDiscountPrice() - Started.");
            var unitID = GetParameter(input.paramsCheck);

            Logger.DebugFormat("CreateUpdateDiscountPrice() - Start get data Booking Detail. Parameters sent:{0}" +
                        "bookingHeaderID = {1}{0}" +
                        "unitID = {2}{0}"
                        , Environment.NewLine, unitID.bookingHeaderID, unitID.unitID);
            //REPAIR THIS
            var getbookingDetail = (from x in _trBookingDetailRepo.GetAll()
                                    join c in _trBookingHeaderRepo.GetAll()
                                    on x.bookingHeaderID equals c.Id
                                    where c.Id == unitID.bookingHeaderID
                                    && c.unitID == unitID.unitID
                                    select x).ToList();

            Logger.DebugFormat("CreateUpdateDiscountPrice() - Ended get data Booking Detail.");

            foreach (var item in input.DiscountList)
            {
                Logger.DebugFormat("CreateUpdateDiscountPrice() - Start checking before update TR Booking Detail Add Disc. Parameters sent:{0}" +
                            "discNo = {1}{0}"
                            , Environment.NewLine, item.discNo);

                var checkExists = (from a in _trBookingDetailAddDiscRepo.GetAll()
                                   join x in getbookingDetail
                                   on a.bookingDetailID equals x.Id
                                   where item.discNo == a.addDiscNo
                                   select a);

                Logger.DebugFormat("CreateUpdateDiscountPrice() - Ended checking before insert Account. Result = {0}", checkExists.Count());

                if (checkExists.Any())
                {
                    foreach (var disc in checkExists.ToList())
                    {
                        Logger.DebugFormat("CreateUpdateDiscountPrice() - Start checking table history before insert TR Booking Detail Add Disc History. Parameters sent:{0}" +
                            "bookingDetailID = {1}{0}"
                            , Environment.NewLine, disc.bookingDetailID);

                        var checkHistory = (from A in _trBookingDetailAddDiscHistory.GetAll()
                                            orderby A.Id descending
                                            where A.bookingDetailID == disc.bookingDetailID && A.priceType == "PSAS"
                                            select A).FirstOrDefault();

                        //insert into tb history
                        var dataToInsertHistory = new TR_BookingDetailAddDiscHistory
                        {
                            addDiscDesc = disc.addDiscDesc,
                            addDiscNo = disc.addDiscNo,
                            amtAddDisc = disc.amtAddDisc,
                            isAmount = disc.isAmount,
                            pctAddDisc = disc.pctAddDisc,
                            bookingDetailID = disc.bookingDetailID,
                            entityID = disc.entityID,
                            priceType = "PSAS",
                            historyNo = checkHistory == null ? (short)0 : (short)(checkHistory.historyNo + 1)
                        };

                        var update = disc.MapTo<TR_BookingDetailAddDisc>();

                        update.pctAddDisc = input.isAmount == true ? 0 : item.pctDisc;
                        update.amtAddDisc = input.isAmount == true ? item.amountDisc : 0;
                        update.isAmount = input.isAmount;
                        try
                        {
                            _trBookingDetailAddDiscHistory.Insert(dataToInsertHistory);
                            CurrentUnitOfWork.SaveChanges();

                            Logger.DebugFormat("CreateUpdateDiscountPrice() - Start update TR Booking Detail Add Disc. Parameters sent:{0}" +
                                "pctAddDisc = {1}{0}" +
                                "amtAddDisc = {2}{0}" +
                                "isAmount = {3}{0}"
                                , Environment.NewLine, item.pctDisc, item.amountDisc, input.isAmount);

                            _trBookingDetailAddDiscRepo.Update(update);
                            CurrentUnitOfWork.SaveChanges();

                            Logger.DebugFormat("CreateUpdateDiscountPrice() - Ended update TR Booking Detail Add Disc.");
                        }
                        catch (DataException ex)
                        {
                            Logger.ErrorFormat("CreateUpdateDiscountPrice() - ERROR DataException. Result = {0}", ex.Message);
                            throw new UserFriendlyException("Db Error: " + ex.Message);
                        }
                        catch (Exception ex)
                        {
                            Logger.ErrorFormat("CreateUpdateDiscountPrice() - ERROR Exception. Result = {0}", ex.Message);
                            throw new UserFriendlyException("Error: " + ex.Message);
                        }

                    }
                }
                else
                {
                    foreach (var detail in getbookingDetail)
                    {
                        var insert = new TR_BookingDetailAddDisc
                        {
                            addDiscDesc = item.addDiscDesc,
                            addDiscNo = item.discNo,
                            amtAddDisc = input.isAmount == true ? item.amountDisc : 0,
                            isAmount = input.isAmount,
                            pctAddDisc = input.isAmount == true ? 0 : item.pctDisc,
                            bookingDetailID = detail.Id,
                            entityID = 1
                        };

                        try
                        {
                            Logger.DebugFormat("CreateUpdateDiscountPrice() - Start insert TR Booking Detail Add Disc. Parameters sent:{0}" +
                                "entityID = {1}{0}" +
                                "addDiscDesc = {2}{0}" +
                                "addDiscNo = {3}{0}" +
                                "amtAddDisc = {4}{0}" +
                                "isAmount = {5}{0}" +
                                "pctAddDisc = {6}{0}" +
                                "bookingDetailID = {7}{0}"
                                , Environment.NewLine, 1, item.addDiscDesc, item.discNo, item.amountDisc, input.isAmount, item.pctDisc, detail.Id);

                            _trBookingDetailAddDiscRepo.Insert(insert);
                            CurrentUnitOfWork.SaveChanges();

                            Logger.DebugFormat("CreateUpdateDiscountPrice() - Ended insert TR Booking Detail Add Disc.");
                        }
                        catch (DataException ex)
                        {
                            Logger.ErrorFormat("CreateUpdateDiscountPrice() - ERROR DataException. Result = {0}", ex.Message);
                            throw new UserFriendlyException("Db Error: " + ex.Message);
                        }
                        catch (Exception ex)
                        {
                            Logger.ErrorFormat("CreateUpdateDiscountPrice() - ERROR Exception. Result = {0}", ex.Message);
                            throw new UserFriendlyException("Error: " + ex.Message);
                        }
                    }
                }
            }
            Logger.Info("CreateUpdateDiscountPrice() - Finished.");
        }

        public GetPSASPriceListDto GetCommisionPrice(GetPSASParamsDto input)
        {
            var unitID = GetParameter(input);

            var projectCode = (from a in _msUnitRepo.GetAll()
                               join b in _msProjectRepo.GetAll() on a.projectID equals b.Id
                               select b.projectCode).FirstOrDefault();

            var getAreaGrossPriceList = (from a in _trBookingDetailRepo.GetAll()
                                         join i in _lkItemRepo.GetAll() on a.itemID equals i.Id
                                         where a.bookingHeaderID == unitID.bookingHeaderID
                                         group a by new
                                         {
                                             a.bookingHeaderID,
                                             a.itemID,
                                             i.itemCode,
                                             a.amountComm,
                                             a.area
                                         } into G
                                         select new
                                         {
                                             item = G.Key.itemID,
                                             G.Key.itemCode,
                                             area = G.Key.area,
                                             G.Key.amountComm
                                         }).ToList();

            var getArea = new GetAreaLlistDto
            {
                tanah = getAreaGrossPriceList.Where(x => x.itemCode == "01").Select(x => x.area).FirstOrDefault(),
                bangunan = getAreaGrossPriceList.Where(x => x.itemCode == "02").Select(x => x.area).FirstOrDefault(),
                renov = getAreaGrossPriceList.Where(x => x.itemCode != "01" && x.itemCode != "02").Select(x => x.area).FirstOrDefault(),
                total = getAreaGrossPriceList.Sum(x => x.area)
            };

            var getGrossPrice = new GetGrosspriceLlistDto
            {
                tanah = getAreaGrossPriceList.Where(x => x.itemCode == "01").Select(x => x.amountComm).FirstOrDefault(),
                bangunan = getAreaGrossPriceList.Where(x => x.itemCode == "02").Select(x => x.amountComm).FirstOrDefault(),
                renov = getAreaGrossPriceList.Where(x => x.itemCode != "01" && x.itemCode != "02").Select(x => x.amountComm).FirstOrDefault(),
                total = getAreaGrossPriceList.Sum(x => x.amountComm)
            };

            var getDiscountList = (from a in _trBookingDetailRepo.GetAll()
                                   join b in _trBookingHeaderRepo.GetAll() on a.bookingHeaderID equals b.Id
                                   join i in _lkItemRepo.GetAll() on a.itemID equals i.Id
                                   where b.unitID == unitID.unitID
                                   && a.bookingHeaderID == unitID.bookingHeaderID
                                   group a by new
                                   {
                                       b.unitID,
                                       a.pctDisc,
                                       a.itemID,
                                       i.itemCode,
                                       a.amount
                                   } into G
                                   select new
                                   {
                                       G.Key.itemCode,
                                       discount = G.Key.pctDisc,
                                       discountItem = Math.Round((double)G.Key.amount * G.Key.pctDisc)
                                   }).ToList();

            var getDiscount = new GetDiscountLlistDto
            {
                discount = getDiscountList.FirstOrDefault().discount,
                tanah = getDiscountList.Where(x => x.itemCode == "01").Select(x => x.discountItem).FirstOrDefault(),
                bangunan = getDiscountList.Where(x => x.itemCode == "02").Select(x => x.discountItem).FirstOrDefault(),
                renov = getDiscountList.Where(x => x.itemCode != "01" && x.itemCode != "02").Select(x => x.discountItem).FirstOrDefault(),
                total = getDiscountList.Sum(x => x.discountItem)

            };

            var getNetPrice = new GetNetpriceLlistDto
            {
                tanah = Convert.ToDouble(getGrossPrice.tanah) - Convert.ToDouble(getDiscount.tanah),
                bangunan = Convert.ToDouble(getGrossPrice.bangunan) - Convert.ToDouble(getDiscount.bangunan),
                renov = Convert.ToDouble(getGrossPrice.renov) - Convert.ToDouble(getDiscount.renov),
                total = Convert.ToDouble(getGrossPrice.total) - Convert.ToDouble(getDiscount.total),
            };

            //ToDo: fixing addDisc
            var getDiscountA = (from a in _trCommAddDiscRepo.GetAll()
                                join b in _trBookingDetailRepo.GetAll()
                                on a.bookingDetailID equals b.Id
                                join c in _trBookingHeaderRepo.GetAll()
                                on b.bookingHeaderID equals c.Id
                                orderby a.addDiscNo
                                where c.Id == unitID.bookingHeaderID
                                && c.unitID == unitID.unitID
                                && b.itemID == 2
                                select new
                                {
                                    bookingDetailID = b.Id,
                                    bookingDetailAddDiscID = a.Id,
                                    discountName = a.addDiscDesc,
                                    discount = a.isAmount == true ? (double)a.amtAddDisc : a.pctAddDisc,
                                    isAmount = a.isAmount,
                                    tanah = a.isAmount == true ? (double)a.amtAddDisc : Math.Round(a.pctAddDisc * (double)getNetPrice.tanah),
                                    bangunan = a.isAmount == true ? (double)a.amtAddDisc : Math.Round(a.pctAddDisc * (double)getNetPrice.bangunan),
                                    renov = a.isAmount == true ? (double)a.amtAddDisc : Math.Round(a.pctAddDisc * (double)getNetPrice.renov),
                                    //total = a.isAmount == true ? (double)a.amtAddDisc : Math.Round(a.pctAddDisc * (double)getNetPrice.bangunan)
                                }).ToList();

            double dataNetPriceBangunan = (double)getNetPrice.bangunan;
            double dataNetPriceTanah = (double)getNetPrice.tanah;
            double dataNetPriceRenov = (double)getNetPrice.renov;
            double netPriceDiscBangunan = 0;
            double netPriceDiscTanah = 0;
            double netPriceDiscRenov = 0;

            List<GetDiscountAlistDto> discDto = new List<GetDiscountAlistDto>();
            foreach (var dataDisc in getDiscountA)
            {
                var discountAddBangunan = dataDisc.isAmount == true ? (double)dataDisc.discount : Math.Round(dataDisc.discount * dataNetPriceBangunan);
                var discountAddTanah = dataDisc.isAmount == true ? (double)dataDisc.discount : Math.Round(dataDisc.discount * dataNetPriceTanah);
                var discountAddRenov = dataDisc.isAmount == true ? (double)dataDisc.discount : Math.Round(dataDisc.discount * dataNetPriceRenov);
                dataNetPriceBangunan = dataNetPriceBangunan - discountAddBangunan;
                dataNetPriceTanah = dataNetPriceTanah - discountAddTanah;
                dataNetPriceRenov = dataNetPriceRenov - discountAddRenov;
                netPriceDiscBangunan = netPriceDiscBangunan + discountAddBangunan;
                netPriceDiscTanah = netPriceDiscTanah + discountAddTanah;
                netPriceDiscRenov = netPriceDiscRenov + discountAddRenov;
                discDto.Add(new GetDiscountAlistDto
                {
                    bookingDetailID = dataDisc.bookingDetailID,
                    bookingDetailAddDiscID = dataDisc.bookingDetailAddDiscID,
                    discountName = dataDisc.discountName,
                    discount = dataDisc.discount,
                    isAmount = dataDisc.isAmount,
                    tanah = discountAddTanah,
                    bangunan = discountAddBangunan,
                    renov = discountAddRenov,
                    total = discountAddTanah + discountAddBangunan + discountAddRenov
                });
            }

            var getAddDisc = new GetAddDiscLlistDto
            {
                tanah = Math.Round(netPriceDiscTanah),
                bangunan = Math.Round(netPriceDiscBangunan),
                renov = Math.Round(netPriceDiscRenov),
                total = Math.Round(netPriceDiscTanah) + Math.Round(netPriceDiscBangunan) + Math.Round(netPriceDiscRenov)
            };

            var getNetNetPrice = new GetNetNetPriceLlistDto
            {
                tanah = Math.Round(Convert.ToDouble(getNetPrice.tanah) - Convert.ToDouble(getAddDisc.tanah)),
                bangunan = Math.Round(Convert.ToDouble(getNetPrice.bangunan) - Convert.ToDouble(getAddDisc.bangunan)),
                renov = Math.Round(Convert.ToDouble(getNetPrice.renov) - Convert.ToDouble(getAddDisc.renov)),
                total = Math.Round(Convert.ToDouble(getNetPrice.total) - Convert.ToDouble(getAddDisc.total))
            };

            var getVAT = new GetVATLlistDto
            {
                discount = 0.1,
                tanah = Math.Round(Convert.ToDouble(getNetNetPrice.tanah) * 0.1),
                bangunan = Math.Round(Convert.ToDouble(getNetNetPrice.bangunan) * 0.1),
                renov = Math.Round(Convert.ToDouble(getNetNetPrice.renov) * 0.1),
                total = Math.Round(Convert.ToDouble(getNetNetPrice.total) * 0.1)
            };

            var checkInterest = (from a in _msUnitRepo.GetAll()
                                 join b in _msProjectRepo.GetAll()
                                 on a.projectID equals b.Id
                                 where a.Id == unitID.unitID
                                 && b.projectCode == "SDH"
                                 select a).Any();

            var getInterest = new GetInterestLlistDto();

            if (checkInterest)
            {
                getInterest = new GetInterestLlistDto
                {
                    tanah = 0,
                    bangunan = 0,
                    renov = 0,
                    total = 0
                };
            }
            else
            {
                //Nothing
            }

            var getTotal = new GetTotalLlistDto
            {
                tanah = Math.Round(Convert.ToDouble(getNetNetPrice.tanah) + Convert.ToDouble(getVAT.tanah) + Convert.ToDouble(getInterest.tanah)),
                bangunan = Math.Round(Convert.ToDouble(getNetNetPrice.bangunan) + Convert.ToDouble(getVAT.bangunan) + Convert.ToDouble(getInterest.bangunan)),
                renov = Math.Round(Convert.ToDouble(getNetNetPrice.renov) + Convert.ToDouble(getVAT.renov) + Convert.ToDouble(getInterest.renov)),
                total = Math.Round(Convert.ToDouble(getNetNetPrice.total) + Convert.ToDouble(getVAT.total) + Convert.ToDouble(getInterest.total))
            };

            var result = new GetPSASPriceListDto
            {
                bookingHeaderID = unitID.bookingHeaderID,
                projectCode = projectCode,
                area = getArea,
                grossPrice = getGrossPrice,
                discount = getDiscount,
                netPrice = getNetPrice,
                addDisc = getAddDisc,
                discountA = discDto,
                netNetPrice = getNetNetPrice,
                VATPrice = getVAT,
                interest = getInterest,
                total = getTotal
            };

            return result;
        }


        public GetPSASPriceListDto GetMarketingPrice(GetPSASParamsDto input)
        {
            var unitID = GetParameter(input);

            var projectCode = (from a in _msUnitRepo.GetAll()
                               join b in _msProjectRepo.GetAll() on a.projectID equals b.Id
                               select b.projectCode).FirstOrDefault();

            var getAreaGrossPriceList = (from a in _trBookingDetailRepo.GetAll()
                                         join i in _lkItemRepo.GetAll() on a.itemID equals i.Id
                                         where a.bookingHeaderID == unitID.bookingHeaderID
                                         group a by new
                                         {
                                             a.bookingHeaderID,
                                             a.itemID,
                                             i.itemCode,
                                             a.amountMKT,
                                             a.area
                                         } into G
                                         select new
                                         {
                                             item = G.Key.itemID,
                                             G.Key.itemCode,
                                             G.Key.area,
                                             G.Key.amountMKT
                                         }).ToList();

            var getArea = new GetAreaLlistDto
            {
                tanah = getAreaGrossPriceList.Where(x => x.itemCode == "01").Select(x => x.area).FirstOrDefault(),
                bangunan = getAreaGrossPriceList.Where(x => x.itemCode == "02").Select(x => x.area).FirstOrDefault(),
                renov = getAreaGrossPriceList.Where(x => x.itemCode != "01" && x.itemCode != "02").Select(x => x.area).FirstOrDefault(),
                total = getAreaGrossPriceList.Sum(x => x.area)
            };

            var getGrossPrice = new GetGrosspriceLlistDto
            {
                tanah = getAreaGrossPriceList.Where(x => x.itemCode == "01").Select(x => x.amountMKT).FirstOrDefault(),
                bangunan = getAreaGrossPriceList.Where(x => x.itemCode == "02").Select(x => x.amountMKT).FirstOrDefault(),
                renov = getAreaGrossPriceList.Where(x => x.itemCode != "01" && x.itemCode != "02").Select(x => x.amountMKT).FirstOrDefault(),
                total = getAreaGrossPriceList.Sum(x => x.amountMKT)
            };

            var getDiscountList = (from a in _trBookingDetailRepo.GetAll()
                                   join b in _trBookingHeaderRepo.GetAll() on a.bookingHeaderID equals b.Id
                                   join i in _lkItemRepo.GetAll() on a.itemID equals i.Id
                                   where b.unitID == unitID.unitID
                                   && a.bookingHeaderID == unitID.bookingHeaderID
                                   group a by new
                                   {
                                       b.unitID,
                                       a.pctDisc,
                                       a.itemID,
                                       i.itemCode,
                                       a.amount
                                   } into G
                                   select new
                                   {
                                       G.Key.itemCode,
                                       discount = G.Key.pctDisc,
                                       discountItem = Math.Round((double)G.Key.amount * G.Key.pctDisc)
                                   }).ToList();

            var getDiscount = new GetDiscountLlistDto
            {
                discount = getDiscountList.FirstOrDefault().discount,
                tanah = getDiscountList.Where(x => x.itemCode == "01").Select(x => x.discountItem).FirstOrDefault(),
                bangunan = getDiscountList.Where(x => x.itemCode == "02").Select(x => x.discountItem).FirstOrDefault(),
                renov = getDiscountList.Where(x => x.itemCode != "01" && x.itemCode != "02").Select(x => x.discountItem).FirstOrDefault(),
                total = getDiscountList.Sum(x => x.discountItem)

            };

            var getNetPrice = new GetNetpriceLlistDto
            {
                tanah = Convert.ToDouble(getGrossPrice.tanah) - Convert.ToDouble(getDiscount.tanah),
                bangunan = Convert.ToDouble(getGrossPrice.bangunan) - Convert.ToDouble(getDiscount.bangunan),
                renov = Convert.ToDouble(getGrossPrice.renov) - Convert.ToDouble(getDiscount.renov),
                total = Convert.ToDouble(getGrossPrice.total) - Convert.ToDouble(getDiscount.total),
            };

            //ToDo: fixing addDisc
            var getDiscountA = (from a in _trMktAddDiscRepo.GetAll()
                                join b in _trBookingDetailRepo.GetAll()
                                on a.bookingDetailID equals b.Id
                                join c in _trBookingHeaderRepo.GetAll()
                                on b.bookingHeaderID equals c.Id
                                orderby a.addDiscNo
                                where c.Id == unitID.bookingHeaderID
                                && c.unitID == unitID.unitID
                                && b.itemID == 2
                                select new
                                {
                                    bookingDetailID = b.Id,
                                    bookingDetailAddDiscID = a.Id,
                                    discountName = a.addDiscDesc,
                                    discount = a.isAmount == true ? (double)a.amtAddDisc : a.pctAddDisc,
                                    isAmount = a.isAmount,
                                    tanah = a.isAmount == true ? (double)a.amtAddDisc : Math.Round(a.pctAddDisc * (double)getNetPrice.tanah),
                                    bangunan = a.isAmount == true ? (double)a.amtAddDisc : Math.Round(a.pctAddDisc * (double)getNetPrice.bangunan),
                                    renov = a.isAmount == true ? (double)a.amtAddDisc : Math.Round(a.pctAddDisc * (double)getNetPrice.renov),
                                    //total = a.isAmount == true ? (double)a.amtAddDisc : Math.Round(a.pctAddDisc * (double)getNetPrice.bangunan)
                                }).ToList();

            double dataNetPriceBangunan = (double)getNetPrice.bangunan;
            double dataNetPriceTanah = (double)getNetPrice.tanah;
            double dataNetPriceRenov = (double)getNetPrice.renov;
            double netPriceDiscBangunan = 0;
            double netPriceDiscTanah = 0;
            double netPriceDiscRenov = 0;

            List<GetDiscountAlistDto> discDto = new List<GetDiscountAlistDto>();
            foreach (var dataDisc in getDiscountA)
            {
                var discountAddBangunan = dataDisc.isAmount == true ? (double)dataDisc.discount : Math.Round(dataDisc.discount * dataNetPriceBangunan);
                var discountAddTanah = dataDisc.isAmount == true ? (double)dataDisc.discount : Math.Round(dataDisc.discount * dataNetPriceTanah);
                var discountAddRenov = dataDisc.isAmount == true ? (double)dataDisc.discount : Math.Round(dataDisc.discount * dataNetPriceRenov);
                dataNetPriceBangunan = dataNetPriceBangunan - discountAddBangunan;
                dataNetPriceTanah = dataNetPriceTanah - discountAddTanah;
                dataNetPriceRenov = dataNetPriceRenov - discountAddRenov;
                netPriceDiscBangunan = netPriceDiscBangunan + discountAddBangunan;
                netPriceDiscTanah = netPriceDiscTanah + discountAddTanah;
                netPriceDiscRenov = netPriceDiscRenov + discountAddRenov;
                discDto.Add(new GetDiscountAlistDto
                {
                    bookingDetailID = dataDisc.bookingDetailID,
                    bookingDetailAddDiscID = dataDisc.bookingDetailAddDiscID,
                    discountName = dataDisc.discountName,
                    discount = dataDisc.discount,
                    isAmount = dataDisc.isAmount,
                    tanah = discountAddTanah,
                    bangunan = discountAddBangunan,
                    renov = discountAddRenov,
                    total = discountAddTanah + discountAddBangunan + discountAddRenov
                });
            }

            var getAddDisc = new GetAddDiscLlistDto
            {
                tanah = Math.Round(netPriceDiscTanah),
                bangunan = Math.Round(netPriceDiscBangunan),
                renov = Math.Round(netPriceDiscRenov),
                total = Math.Round(netPriceDiscTanah) + Math.Round(netPriceDiscBangunan) + Math.Round(netPriceDiscRenov)
            };

            var getNetNetPrice = new GetNetNetPriceLlistDto
            {
                tanah = Math.Round(Convert.ToDouble(getNetPrice.tanah) - Convert.ToDouble(getAddDisc.tanah)),
                bangunan = Math.Round(Convert.ToDouble(getNetPrice.bangunan) - Convert.ToDouble(getAddDisc.bangunan)),
                renov = Math.Round(Convert.ToDouble(getNetPrice.renov) - Convert.ToDouble(getAddDisc.renov)),
                total = Math.Round(Convert.ToDouble(getNetPrice.total) - Convert.ToDouble(getAddDisc.total))
            };

            var getVAT = new GetVATLlistDto
            {
                discount = 0.1,
                tanah = Math.Round(Convert.ToDouble(getNetNetPrice.tanah) * 0.1),
                bangunan = Math.Round(Convert.ToDouble(getNetNetPrice.bangunan) * 0.1),
                renov = Math.Round(Convert.ToDouble(getNetNetPrice.renov) * 0.1),
                total = Math.Round(Convert.ToDouble(getNetNetPrice.total) * 0.1)
            };

            var checkInterest = (from a in _msUnitRepo.GetAll()
                                 join b in _msProjectRepo.GetAll()
                                 on a.projectID equals b.Id
                                 where a.Id == unitID.unitID
                                 && b.projectCode == "SDH"
                                 select a).Any();

            var getInterest = new GetInterestLlistDto();

            if (checkInterest)
            {
                getInterest = new GetInterestLlistDto
                {
                    tanah = 0,
                    bangunan = 0,
                    renov = 0,
                    total = 0
                };
            }
            else
            {
                //Nothing
            }

            var getTotal = new GetTotalLlistDto
            {
                tanah = Math.Round(Convert.ToDouble(getNetNetPrice.tanah) + Convert.ToDouble(getVAT.tanah) + Convert.ToDouble(getInterest.tanah)),
                bangunan = Math.Round(Convert.ToDouble(getNetNetPrice.bangunan) + Convert.ToDouble(getVAT.bangunan) + Convert.ToDouble(getInterest.bangunan)),
                renov = Math.Round(Convert.ToDouble(getNetNetPrice.renov) + Convert.ToDouble(getVAT.renov) + Convert.ToDouble(getInterest.renov)),
                total = Math.Round(Convert.ToDouble(getNetNetPrice.total) + Convert.ToDouble(getVAT.total) + Convert.ToDouble(getInterest.total))
            };

            var result = new GetPSASPriceListDto
            {
                bookingHeaderID = unitID.bookingHeaderID,
                projectCode = projectCode,
                area = getArea,
                grossPrice = getGrossPrice,
                discount = getDiscount,
                netPrice = getNetPrice,
                addDisc = getAddDisc,
                discountA = discDto,
                netNetPrice = getNetNetPrice,
                VATPrice = getVAT,
                interest = getInterest,
                total = getTotal
            };

            return result;
        }


        public GetPSASResultDto GetParameter(GetPSASParamsDto input)
        {
            GetPSASResultDto getunitID;

            if (input.bookCode != null && input.unitNo == null && input.unitCode == null)
            {
                getunitID = (from a in _trBookingHeaderRepo.GetAll()
                             where a.bookCode == input.bookCode
                             orderby a.CreationTime descending
                             select new GetPSASResultDto
                             {
                                 unitID = a.unitID,
                                 bookingHeaderID = a.Id
                             }).FirstOrDefault();

                if (getunitID == null)
                {
                    throw new UserFriendlyException("bookCode '" + input.bookCode + "' not exists");
                }
            }
            else if (input.unitNo != null && input.unitCode != null && input.bookCode == null)
            {
                getunitID = (from a in _msUnitRepo.GetAll()
                             join b in _msUnitCodeRepo.GetAll()
                             on a.unitCodeID equals b.Id
                             join c in _trBookingHeaderRepo.GetAll()
                             on a.Id equals c.unitID
                             where a.unitNo == input.unitNo
                             && b.unitCode == input.unitCode
                             orderby a.CreationTime descending
                             select new GetPSASResultDto
                             {
                                 unitID = a.Id,
                                 bookingHeaderID = c.Id
                             }).FirstOrDefault();

                if (getunitID == null)
                {
                    throw new UserFriendlyException("unitCode '" + input.unitCode + "', unitNo '" + input.unitNo + "' not exists");
                }
            }
            else
            {
                throw new UserFriendlyException("Invalid Parameter");
            }
            return getunitID;
        }

        public GetPSASPriceListDto GetPSASPrice(GetPSASParamsDto input)
        {
            var unitID = GetParameter(input);

            var projectCode = (from a in _msUnitRepo.GetAll()
                               join b in _msProjectRepo.GetAll() on a.projectID equals b.Id
                               select b.projectCode).FirstOrDefault();

            var getAreaGrossPriceList = (from a in _trBookingDetailRepo.GetAll()
                                         join i in _lkItemRepo.GetAll() on a.itemID equals i.Id
                                         where a.bookingHeaderID == unitID.bookingHeaderID
                                         group a by new
                                         {
                                             a.bookingHeaderID,
                                             a.itemID,
                                             i.itemCode,
                                             a.amount,
                                             a.area
                                         } into G
                                         select new
                                         {
                                             item = G.Key.itemID,
                                             G.Key.itemCode,
                                             area = G.Key.area,
                                             G.Key.amount
                                         }).ToList();

            var getArea = new GetAreaLlistDto
            {
                tanah = getAreaGrossPriceList.Where(x => x.itemCode == "01").Select(x => x.area).FirstOrDefault(),
                bangunan = getAreaGrossPriceList.Where(x => x.itemCode == "02").Select(x => x.area).FirstOrDefault(),
                renov = getAreaGrossPriceList.Where(x => x.itemCode != "01" && x.itemCode != "02").Select(x => x.area).FirstOrDefault(),
                total = getAreaGrossPriceList.Sum(x => x.area)
            };
            
            var getGrossPrice = new GetGrosspriceLlistDto
            {
                tanah = getAreaGrossPriceList.Where(x => x.itemCode == "01").Select(x => x.amount).FirstOrDefault(),
                bangunan = getAreaGrossPriceList.Where(x => x.itemCode == "02").Select(x => x.amount).FirstOrDefault(),
                renov = getAreaGrossPriceList.Where(x => x.itemCode != "01" && x.itemCode != "02").Select(x => x.amount).FirstOrDefault(),
                total = getAreaGrossPriceList.Sum(x => x.amount)
            };

            var getDiscountList = (from a in _trBookingDetailRepo.GetAll()
                                   join b in _trBookingHeaderRepo.GetAll() on a.bookingHeaderID equals b.Id
                                   join i in _lkItemRepo.GetAll() on a.itemID equals i.Id
                                   where b.unitID == unitID.unitID
                                   && a.bookingHeaderID == unitID.bookingHeaderID
                                   group a by new
                                   {
                                       b.unitID,
                                       a.pctDisc,
                                       a.itemID,
                                       i.itemCode,
                                       a.amount
                                   } into G
                                   select new
                                   {
                                       G.Key.itemCode,
                                       discount = G.Key.pctDisc,
                                       discountItem = Math.Round((double)G.Key.amount * G.Key.pctDisc)
                                   }).ToList();

            var getDiscount = new GetDiscountLlistDto
            {
                discount = getDiscountList.FirstOrDefault().discount,
                tanah = getDiscountList.Where(x => x.itemCode == "01").Select(x => x.discountItem).FirstOrDefault(),
                bangunan = getDiscountList.Where(x => x.itemCode == "02").Select(x => x.discountItem).FirstOrDefault(),
                renov = getDiscountList.Where(x => x.itemCode != "01" && x.itemCode != "02").Select(x => x.discountItem).FirstOrDefault(),
                total = getDiscountList.Sum(x => x.discountItem)

            };

            var getNetPrice = new GetNetpriceLlistDto
            {
                tanah = Convert.ToDouble(getGrossPrice.tanah) - Convert.ToDouble(getDiscount.tanah),
                bangunan = Convert.ToDouble(getGrossPrice.bangunan) - Convert.ToDouble(getDiscount.bangunan),
                renov = Convert.ToDouble(getGrossPrice.renov) - Convert.ToDouble(getDiscount.renov),
                total = Convert.ToDouble(getGrossPrice.total) - Convert.ToDouble(getDiscount.total),
            };

            //ToDo: fixing addDisc
            var getDiscountA = (from a in _trBookingDetailAddDiscRepo.GetAll()
                                join b in _trBookingDetailRepo.GetAll()
                                on a.bookingDetailID equals b.Id
                                join c in _trBookingHeaderRepo.GetAll()
                                on b.bookingHeaderID equals c.Id
                                orderby a.addDiscNo
                                where c.Id == unitID.bookingHeaderID
                                && c.unitID == unitID.unitID
                                && b.itemID == 2
                                select new 
                                {
                                    bookingDetailID = b.Id,
                                    bookingDetailAddDiscID = a.Id,
                                    discountName = a.addDiscDesc,
                                    discount = a.isAmount == true ? (double)a.amtAddDisc : a.pctAddDisc,
                                    isAmount = a.isAmount,
                                    tanah = a.isAmount == true ? (double)a.amtAddDisc : Math.Round(a.pctAddDisc * (double)getNetPrice.tanah),
                                    bangunan = a.isAmount == true ? (double)a.amtAddDisc : Math.Round(a.pctAddDisc * (double)getNetPrice.bangunan),
                                    renov = a.isAmount == true ? (double)a.amtAddDisc : Math.Round(a.pctAddDisc * (double)getNetPrice.renov),
                                    //total = a.isAmount == true ? (double)a.amtAddDisc : Math.Round(a.pctAddDisc * (double)getNetPrice.bangunan)
                                }).ToList();

            double dataNetPriceBangunan = (double)getNetPrice.bangunan;
            double dataNetPriceTanah = (double)getNetPrice.tanah;
            double dataNetPriceRenov = (double)getNetPrice.renov;
            double netPriceDiscBangunan = 0;
            double netPriceDiscTanah = 0;
            double netPriceDiscRenov = 0;

            List<GetDiscountAlistDto> discDto = new List<GetDiscountAlistDto>();
            foreach (var dataDisc in getDiscountA)
            {
                var discountAddBangunan = dataDisc.isAmount == true ? (double)dataDisc.discount : Math.Round(dataDisc.discount * dataNetPriceBangunan);
                var discountAddTanah = dataDisc.isAmount == true ? (double)dataDisc.discount : Math.Round(dataDisc.discount * dataNetPriceTanah);
                var discountAddRenov = dataDisc.isAmount == true ? (double)dataDisc.discount : Math.Round(dataDisc.discount * dataNetPriceRenov);
                dataNetPriceBangunan = dataNetPriceBangunan - discountAddBangunan;
                dataNetPriceTanah = dataNetPriceTanah - discountAddTanah;
                dataNetPriceRenov = dataNetPriceRenov - discountAddRenov;
                netPriceDiscBangunan = netPriceDiscBangunan + discountAddBangunan;
                netPriceDiscTanah = netPriceDiscTanah + discountAddTanah;
                netPriceDiscRenov = netPriceDiscRenov + discountAddRenov;
                discDto.Add(new GetDiscountAlistDto
                {
                    bookingDetailID = dataDisc.bookingDetailID,
                    bookingDetailAddDiscID = dataDisc.bookingDetailAddDiscID,
                    discountName = dataDisc.discountName,
                    discount = dataDisc.discount,
                    isAmount = dataDisc.isAmount,
                    tanah = discountAddTanah,
                    bangunan = discountAddBangunan,
                    renov = discountAddRenov,
                    total = discountAddTanah + discountAddBangunan + discountAddRenov
                });
            }

            var getAddDisc = new GetAddDiscLlistDto
            {
                tanah = Math.Round(netPriceDiscTanah),
                bangunan = Math.Round(netPriceDiscBangunan),
                renov = Math.Round(netPriceDiscRenov),
                total = Math.Round(netPriceDiscTanah) + Math.Round(netPriceDiscBangunan) + Math.Round(netPriceDiscRenov)
            };

            var getNetNetPrice = new GetNetNetPriceLlistDto
            {
                tanah = Math.Round(Convert.ToDouble(getNetPrice.tanah) - Convert.ToDouble(getAddDisc.tanah)),
                bangunan = Math.Round(Convert.ToDouble(getNetPrice.bangunan) - Convert.ToDouble(getAddDisc.bangunan)),
                renov = Math.Round(Convert.ToDouble(getNetPrice.renov) - Convert.ToDouble(getAddDisc.renov)),
                total = Math.Round(Convert.ToDouble(getNetPrice.total) - Convert.ToDouble(getAddDisc.total))
            };

            var getVAT = new GetVATLlistDto
            {
                discount = 0.1,
                tanah = Math.Round(Convert.ToDouble(getNetNetPrice.tanah) * 0.1),
                bangunan = Math.Round(Convert.ToDouble(getNetNetPrice.bangunan) * 0.1),
                renov = Math.Round(Convert.ToDouble(getNetNetPrice.renov) * 0.1),
                total = Math.Round(Convert.ToDouble(getNetNetPrice.total) * 0.1)
            };

            var checkInterest = (from a in _msUnitRepo.GetAll()
                                 join b in _msProjectRepo.GetAll()
                                 on a.projectID equals b.Id
                                 where a.Id == unitID.unitID
                                 && b.projectCode == "SDH"
                                 select a).Any();

            var getInterest = new GetInterestLlistDto();

            if (checkInterest)
            {
                getInterest = new GetInterestLlistDto
                {
                    tanah = 0,
                    bangunan = 0,
                    renov = 0,
                    total = 0
                };
            }
            else
            {
                //Nothing
            }

            var getTotal = new GetTotalLlistDto
            {
                tanah = Math.Round(Convert.ToDouble(getNetNetPrice.tanah) + Convert.ToDouble(getVAT.tanah) + Convert.ToDouble(getInterest.tanah)),
                bangunan = Math.Round(Convert.ToDouble(getNetNetPrice.bangunan) + Convert.ToDouble(getVAT.bangunan) + Convert.ToDouble(getInterest.bangunan)),
                renov = Math.Round(Convert.ToDouble(getNetNetPrice.renov) + Convert.ToDouble(getVAT.renov) + Convert.ToDouble(getInterest.renov)),
                total = Math.Round(Convert.ToDouble(getNetNetPrice.total) + Convert.ToDouble(getVAT.total) + Convert.ToDouble(getInterest.total))
            };

            var result = new GetPSASPriceListDto
            {
                bookingHeaderID = unitID.bookingHeaderID,
                projectCode = projectCode,
                area = getArea,
                grossPrice = getGrossPrice,
                discount = getDiscount,
                netPrice = getNetPrice,
                addDisc = getAddDisc,
                discountA = discDto,
                netNetPrice = getNetNetPrice,
                VATPrice = getVAT,
                interest = getInterest,
                total = getTotal
            };

            return result;
        }

        public GetUniversalListDto GetUniversalPrice(GetPSASParamsDto input)
        {
            var getPSASPrice = GetPSASPrice(input);
            var getMarketingPrice = GetMarketingPrice(input);
            var getCommisionPrice = GetCommisionPrice(input);

            var result = new GetUniversalListDto
            {
                PSASPrice = getPSASPrice,
                MarketingPrice = getMarketingPrice,
                CommisionPrice = getCommisionPrice
            };
            return result;
        }

        public List<GetAddDiscListDto> GetDiscountPrice(GetPSASParamsDto input)
        {
            var unitID = GetParameter(input);

            var getSingleDetailID = (from a in _trBookingDetailRepo.GetAll()
                                     join c in _trBookingHeaderRepo.GetAll()
                                     on a.bookingHeaderID equals c.Id
                                     where c.Id == unitID.bookingHeaderID
                                     && c.unitID == unitID.unitID
                                     select a.Id).FirstOrDefault();

            var getDetailAddDisc = (from a in _trBookingDetailAddDiscRepo.GetAll()
                                    where a.bookingDetailID == getSingleDetailID
                                    select new GetAddDiscListDto
                                    {
                                        addDiscDesc = a.addDiscDesc,
                                        addDiscNo = a.addDiscNo,
                                        isAmount = a.isAmount,
                                        pctAddDisc = a.pctAddDisc,
                                        amtAddDisc = a.amtAddDisc
                                    }).ToList();

            return getDetailAddDisc;
        }

        public void UpdateCommisionPrice(UpdatePSASParamsDto input)
        {
            Logger.Info("UpdateCommisionPrice() - Started.");

            var unitID = GetParameter(input.paramsCheck);

            Logger.DebugFormat("UpdateCommisionPrice() - Start get data Booking Detail for update. Parameters sent:{0}" +
                        "bookingHeaderID = {1}{0}"
                        , Environment.NewLine, unitID.bookingHeaderID);

            var getDetail = (from x in _trBookingDetailRepo.GetAll()
                             join i in _lkItemRepo.GetAll() on x.itemID equals i.Id
                             where x.bookingHeaderID == unitID.bookingHeaderID
                             select new {
                                 detail = x,
                                 dataitem = i
                             }).ToList();

            Logger.DebugFormat("UpdateCommisionPrice() - Ended get data Booking Detail.");

            var dataPriceOri = UpdatePriceUniversal(input);

            decimal finalGrossPriceTanah = ((decimal)dataPriceOri.netNetPrice.tanah + (decimal)dataPriceOri.VATPrice.tanah);
            decimal finalGrossPriceBangunan = ((decimal)dataPriceOri.netNetPrice.bangunan + (decimal)dataPriceOri.VATPrice.tanah);
            decimal finalGrossPriceRenov = ((decimal)dataPriceOri.netNetPrice.renov + (decimal)dataPriceOri.VATPrice.tanah);

            //(NetNetPrice + VAT) / (1 - AddDiscount) / (1 - SalesDiscount)

            foreach (var pctAddDiscList in dataPriceOri.pctAddDisc)
            {
                finalGrossPriceTanah = finalGrossPriceTanah / (decimal)(1 - pctAddDiscList);
                finalGrossPriceBangunan = finalGrossPriceBangunan / (decimal)(1 - pctAddDiscList);
                finalGrossPriceRenov = finalGrossPriceRenov / (decimal)(1 - pctAddDiscList);
            }

            var inputData = new UpdatePSASParamsDto
            {
                paramsCheck = input.paramsCheck,
                grossPriceTanah = input.isEffectGross ? finalGrossPriceTanah / (1 - (decimal)dataPriceOri.discount.discount) : input.grossPriceTanah,
                grossPriceBangunan = input.isEffectGross ? finalGrossPriceBangunan / (1 - (decimal)dataPriceOri.discount.discount) : input.grossPriceBangunan,
                grossPriceRenov = input.isEffectGross ? finalGrossPriceRenov / (1 - (decimal)dataPriceOri.discount.discount) : input.grossPriceRenov,
            };

            var dataPrice = UpdatePriceUniversal(inputData);

            var checkHistory = (from A in _trBookingDetailHistoryRepo.GetAll()
                                orderby A.Id descending
                                where A.bookingHeaderID == unitID.bookingHeaderID
                                select A).FirstOrDefault();

            foreach (var item in getDetail)
            {
                var dataHistory = new TR_BookingDetailHistory
                {
                    adjArea = item.detail.adjArea,
                    adjPrice = item.detail.adjPrice,
                    amount = item.detail.amount,
                    amountComm = item.detail.amountComm,
                    amountMKT = item.detail.amountMKT,
                    area = item.detail.area,
                    BFAmount = item.detail.BFAmount,
                    bookingHeaderID = item.detail.bookingHeaderID,
                    bookingTrTypeID = item.detail.bookingTrTypeID,
                    bookNo = item.detail.bookNo,
                    coCode = item.detail.coCode,
                    combineCode = item.detail.combineCode,
                    finTypeID = item.detail.finTypeID,
                    itemID = item.detail.itemID,
                    netNetPrice = item.detail.netNetPrice,
                    netPrice = item.detail.netPrice,
                    netPriceCash = item.detail.netPriceCash,
                    netPriceComm = item.detail.netPriceComm,
                    netPriceMKT = item.detail.netPriceMKT,
                    pctDisc = item.detail.pctDisc,
                    pctTax = item.detail.pctTax,
                    refNo = item.detail.refNo,
                    entityID = 1,
                    historyNo = checkHistory == null ? (short)0 : (short)(checkHistory.historyNo + 1)
                };

                _trBookingDetailHistoryRepo.Insert(dataHistory);

                decimal getGrossPrice = 0;
                decimal netPrice = 0;
                decimal netNetPrice = 0;

                if (item.dataitem.itemCode == "01")
                {
                    getGrossPrice = Math.Round(dataPrice.grossPrice.tanah);
                    netPrice = Math.Round((Decimal)dataPrice.netPrice.tanah);
                    netNetPrice = Math.Round((Decimal)dataPrice.netNetPrice.tanah);
                }
                else if (item.dataitem.itemCode == "02")
                {
                    getGrossPrice = Math.Round(dataPrice.grossPrice.bangunan);
                    netPrice = Math.Round((Decimal)dataPrice.netPrice.bangunan);
                    netNetPrice = Math.Round((Decimal)dataPrice.netNetPrice.bangunan);
                }
                else
                {
                    getGrossPrice = Math.Round(dataPrice.grossPrice.renov);
                    netPrice = Math.Round((Decimal)dataPrice.netPrice.renov);
                    netNetPrice = Math.Round((Decimal)dataPrice.netNetPrice.renov);
                }

                var update = item.detail.MapTo<TR_BookingDetail>();

                update.amountComm = getGrossPrice;
                update.netPriceComm = netPrice;

                try
                {
                    Logger.DebugFormat("UpdateCommisionPrice() - Start update TR Booking Detail. Parameters sent:{0}" +
                        "amountComm = {1}{0}"
                        , Environment.NewLine, getGrossPrice);

                    _trBookingDetailRepo.Update(update);
                    CurrentUnitOfWork.SaveChanges();

                    Logger.DebugFormat("UpdateCommisionPrice() - Ended update TR Booking Detail");
                }
                catch (DataException ex)
                {
                    Logger.ErrorFormat("UpdateCommisionPrice() - ERROR DataException. Result = {0}", ex.Message);
                    throw new UserFriendlyException("Db Error: " + ex.Message);
                }
                catch (Exception ex)
                {
                    Logger.ErrorFormat("UpdateCommisionPrice() - ERROR Exception. Result = {0}", ex.Message);
                    throw new UserFriendlyException("Error: " + ex.Message);
                }
            }
            Logger.Info("UpdateCommisionPrice() - Finished.");
        }

        public void UpdateMarketingPrice(UpdatePSASParamsDto input)
        {
            Logger.Info("UpdateMarketingPrice() - Started.");

            var unitID = GetParameter(input.paramsCheck);

            Logger.DebugFormat("UpdateMarketingPrice() - Start get data Booking Detail for update. Parameters sent:{0}" +
                        "bookingHeaderID = {1}{0}"
                        , Environment.NewLine, unitID.bookingHeaderID);

            var getDetail = (from x in _trBookingDetailRepo.GetAll()
                             join i in _lkItemRepo.GetAll() on x.itemID equals i.Id
                             where x.bookingHeaderID == unitID.bookingHeaderID
                             select new
                             {
                                 detail = x,
                                 dataitem = i
                             }).ToList();

            Logger.DebugFormat("UpdateMarketingPrice() - Ended get data Booking Detail.");

            var dataPriceOri = UpdatePriceUniversal(input);

            decimal finalGrossPriceTanah = ((decimal)dataPriceOri.netNetPrice.tanah + (decimal)dataPriceOri.VATPrice.tanah);
            decimal finalGrossPriceBangunan = ((decimal)dataPriceOri.netNetPrice.bangunan + (decimal)dataPriceOri.VATPrice.tanah);
            decimal finalGrossPriceRenov = ((decimal)dataPriceOri.netNetPrice.renov + (decimal)dataPriceOri.VATPrice.tanah);

            //(NetNetPrice + VAT) / (1 - AddDiscount) / (1 - SalesDiscount)

            foreach (var pctAddDiscList in dataPriceOri.pctAddDisc)
            {
                finalGrossPriceTanah = finalGrossPriceTanah / (decimal)(1 - pctAddDiscList);
                finalGrossPriceBangunan = finalGrossPriceBangunan / (decimal)(1 - pctAddDiscList);
                finalGrossPriceRenov = finalGrossPriceRenov / (decimal)(1 - pctAddDiscList);
            }

            var inputData = new UpdatePSASParamsDto
            {
                paramsCheck = input.paramsCheck,
                grossPriceTanah = input.isEffectGross ? finalGrossPriceTanah / (1 - (decimal)dataPriceOri.discount.discount) : input.grossPriceTanah,
                grossPriceBangunan = input.isEffectGross ? finalGrossPriceBangunan / (1 - (decimal)dataPriceOri.discount.discount) : input.grossPriceBangunan,
                grossPriceRenov = input.isEffectGross ? finalGrossPriceRenov / (1 - (decimal)dataPriceOri.discount.discount) : input.grossPriceRenov,
            };

            var dataPrice = UpdatePriceUniversal(inputData);

            var checkHistory = (from A in _trBookingDetailHistoryRepo.GetAll()
                                orderby A.Id descending
                                where A.bookingHeaderID == unitID.bookingHeaderID
                                select A).FirstOrDefault();

            foreach (var item in getDetail)
            {
                var dataHistory = new TR_BookingDetailHistory
                {
                    adjArea = item.detail.adjArea,
                    adjPrice = item.detail.adjPrice,
                    amount = item.detail.amount,
                    amountComm = item.detail.amountComm,
                    amountMKT = item.detail.amountMKT,
                    area = item.detail.area,
                    BFAmount = item.detail.BFAmount,
                    bookingHeaderID = item.detail.bookingHeaderID,
                    bookingTrTypeID = item.detail.bookingTrTypeID,
                    bookNo = item.detail.bookNo,
                    coCode = item.detail.coCode,
                    combineCode = item.detail.combineCode,
                    finTypeID = item.detail.finTypeID,
                    itemID = item.detail.itemID,
                    netNetPrice = item.detail.netNetPrice,
                    netPrice = item.detail.netPrice,
                    netPriceCash = item.detail.netPriceCash,
                    netPriceComm = item.detail.netPriceComm,
                    netPriceMKT = item.detail.netPriceMKT,
                    pctDisc = item.detail.pctDisc,
                    pctTax = item.detail.pctTax,
                    refNo = item.detail.refNo,
                    entityID = 1,
                    historyNo = checkHistory == null ? (short)0 : (short)(checkHistory.historyNo + 1)
                };

                _trBookingDetailHistoryRepo.Insert(dataHistory);

                decimal getGrossPrice = 0;
                decimal netPrice = 0;
                decimal netNetPrice = 0;

                if (item.dataitem.itemCode == "01")
                {
                    getGrossPrice = Math.Round(dataPrice.grossPrice.tanah);
                    netPrice = Math.Round((Decimal)dataPrice.netPrice.tanah);
                    netNetPrice = Math.Round((Decimal)dataPrice.netNetPrice.tanah);
                }
                else if (item.dataitem.itemCode == "02")
                {
                    getGrossPrice = Math.Round(dataPrice.grossPrice.bangunan);
                    netPrice = Math.Round((Decimal)dataPrice.netPrice.bangunan);
                    netNetPrice = Math.Round((Decimal)dataPrice.netNetPrice.bangunan);
                }
                else
                {
                    getGrossPrice = Math.Round(dataPrice.grossPrice.renov);
                    netPrice = Math.Round((Decimal)dataPrice.netPrice.renov);
                    netNetPrice = Math.Round((Decimal)dataPrice.netNetPrice.renov);
                }

                var update = item.detail.MapTo<TR_BookingDetail>();

                update.amountMKT = getGrossPrice;
                update.netPriceMKT = netPrice;

                try
                {
                    Logger.DebugFormat("UpdateMarketingPrice() - Start update TR Booking Detail. Parameters sent:{0}" +
                        "amountMKT = {1}{0}"
                        , Environment.NewLine, getGrossPrice);

                    _trBookingDetailRepo.Update(update);
                    CurrentUnitOfWork.SaveChanges();

                    Logger.DebugFormat("UpdateMarketingPrice() - Ended update TR Booking Detail");
                }
                catch (DataException ex)
                {
                    Logger.ErrorFormat("UpdateMarketingPrice() - ERROR DataException. Result = {0}", ex.Message);
                    throw new UserFriendlyException("Db Error: " + ex.Message);
                }
                catch (Exception ex)
                {
                    Logger.ErrorFormat("UpdateMarketingPrice() - ERROR Exception. Result = {0}", ex.Message);
                    throw new UserFriendlyException("Error: " + ex.Message);
                }
            }
            Logger.Info("UpdateMarketingPrice() - Finished.");
        }

        public void UpdatePSASPrice(UpdatePSASParamsDto input)
        {
            Logger.Info("UpdatePSASPrice() - Started.");

            var unitID = GetParameter(input.paramsCheck);

            Logger.DebugFormat("UpdatePSASPrice() - Start get data Booking Detail for update. Parameters sent:{0}" +
                        "bookingHeaderID = {1}{0}"
                        , Environment.NewLine, unitID.bookingHeaderID);

            var getDetail = (from x in _trBookingDetailRepo.GetAll()
                             join i in _lkItemRepo.GetAll() on x.itemID equals i.Id
                             where x.bookingHeaderID == unitID.bookingHeaderID
                             select new
                             {
                                 detail = x,
                                 dataitem = i
                             }).ToList();

            Logger.DebugFormat("UpdatePSASPrice() - Ended get data Booking Detail.");

            var dataPriceOri = UpdatePriceUniversal(input);

            decimal finalGrossPriceTanah = ((decimal)dataPriceOri.netNetPrice.tanah + (decimal)dataPriceOri.VATPrice.tanah);
            decimal finalGrossPriceBangunan = ((decimal)dataPriceOri.netNetPrice.bangunan + (decimal)dataPriceOri.VATPrice.tanah);
            decimal finalGrossPriceRenov = ((decimal)dataPriceOri.netNetPrice.renov + (decimal)dataPriceOri.VATPrice.tanah);

            //(NetNetPrice + VAT) / (1 - AddDiscount) / (1 - SalesDiscount)

            foreach ( var pctAddDiscList in dataPriceOri.pctAddDisc)
            {
                finalGrossPriceTanah = finalGrossPriceTanah / (decimal)(1 - pctAddDiscList);
                finalGrossPriceBangunan = finalGrossPriceBangunan / (decimal)(1 - pctAddDiscList);
                finalGrossPriceRenov = finalGrossPriceRenov / (decimal)(1 - pctAddDiscList);
            }

            var inputData = new UpdatePSASParamsDto
            {
                paramsCheck = input.paramsCheck,
                grossPriceTanah = input.isEffectGross ? finalGrossPriceTanah / (1 - (decimal)dataPriceOri.discount.discount) : input.grossPriceTanah,
                grossPriceBangunan = input.isEffectGross ? finalGrossPriceBangunan / (1 - (decimal)dataPriceOri.discount.discount) : input.grossPriceBangunan,
                grossPriceRenov = input.isEffectGross ? finalGrossPriceRenov / (1 -(decimal)dataPriceOri.discount.discount) : input.grossPriceRenov,
            };

            var dataPrice = UpdatePriceUniversal(inputData);

            var checkHistory = (from A in _trBookingDetailHistoryRepo.GetAll()
                                orderby A.Id descending
                                where A.bookingHeaderID == unitID.bookingHeaderID
                                select A).FirstOrDefault();

            foreach (var item in getDetail)
            {
                var dataHistory = new TR_BookingDetailHistory
                {
                    adjArea = item.detail.adjArea,
                    adjPrice = item.detail.adjPrice,
                    amount = item.detail.amount,
                    amountComm = item.detail.amountComm,
                    amountMKT = item.detail.amountMKT,
                    area = item.detail.area,
                    BFAmount = item.detail.BFAmount,
                    bookingHeaderID = item.detail.bookingHeaderID,
                    bookingTrTypeID = item.detail.bookingTrTypeID,
                    bookNo = item.detail.bookNo,
                    coCode = item.detail.coCode,
                    combineCode = item.detail.combineCode,
                    finTypeID = item.detail.finTypeID,
                    itemID = item.detail.itemID,
                    netNetPrice = item.detail.netNetPrice,
                    netPrice = item.detail.netPrice,
                    netPriceCash = item.detail.netPriceCash,
                    netPriceComm = item.detail.netPriceComm,
                    netPriceMKT = item.detail.netPriceMKT,
                    pctDisc = item.detail.pctDisc,
                    pctTax = item.detail.pctTax,
                    refNo = item.detail.refNo,
                    entityID = 1,
                    historyNo = checkHistory == null ? (short)0 : (short)(checkHistory.historyNo + 1)
                };

                _trBookingDetailHistoryRepo.Insert(dataHistory);

                decimal getGrossPrice = 0;
                decimal netPrice = 0;
                decimal netNetPrice = 0;


                if (item.dataitem.itemCode == "01")
                {
                    getGrossPrice = Math.Round(dataPrice.grossPrice.tanah);
                    netPrice = Math.Round((Decimal)dataPrice.netPrice.tanah);
                    netNetPrice = Math.Round((Decimal)dataPrice.netNetPrice.tanah);
                }
                else if (item.dataitem.itemCode == "02")
                {
                    getGrossPrice = Math.Round(dataPrice.grossPrice.bangunan);
                    netPrice = Math.Round((Decimal)dataPrice.netPrice.bangunan);
                    netNetPrice = Math.Round((Decimal)dataPrice.netNetPrice.bangunan);
                }
                else
                {
                    getGrossPrice = Math.Round(dataPrice.grossPrice.renov);
                    netPrice = Math.Round((Decimal)dataPrice.netPrice.renov);
                    netNetPrice = Math.Round((Decimal)dataPrice.netNetPrice.renov);
                }


                var update = item.detail.MapTo<TR_BookingDetail>();

                update.amount = getGrossPrice;
                update.netPrice = netPrice;
                update.netNetPrice = netNetPrice;

                try
                {
                    Logger.DebugFormat("UpdatePSASPrice() - Start update TR Booking Detail. Parameters sent:{0}" +
                        "amount = {1}{0}"
                        , Environment.NewLine, getGrossPrice);

                    _trBookingDetailRepo.Update(update);
                    CurrentUnitOfWork.SaveChanges();

                    Logger.DebugFormat("UpdatePSASPrice() - Ended update TR Booking Detail");
                }
                catch (DataException ex)
                {
                    Logger.ErrorFormat("UpdatePSASPrice() - ERROR DataException. Result = {0}", ex.Message);
                    throw new UserFriendlyException("Db Error: " + ex.Message);
                }
                catch (Exception ex)
                {
                    Logger.ErrorFormat("UpdatePSASPrice() - ERROR Exception. Result = {0}", ex.Message);
                    throw new UserFriendlyException("Error: " + ex.Message);
                }
            }
            Logger.Info("UpdatePSASPrice() - Finished.");
        }

        public void GeneratePrice(UpdateTermInputDto input)
        {
            #region performance test
            SendConsole("start stopwatch");
            var watch = System.Diagnostics.Stopwatch.StartNew();
            #endregion

            try
            {
                #region old
                /*
                //get gross price
                var grossPrice = (from A in _trBookingHeaderRepo.GetAll()
                                  join B in _trBookingItemPriceRepo.GetAll() on new { A = A.Id, B = A.termID } equals new { A = B.bookingHeaderID, B = (int)B.termID }
                                  where B.termID == input.termID && A.unitID == input.unitID && A.cancelDate != null
                                  group B by new
                                  {
                                      A.Id
                                  } into G
                                  select new
                                  {
                                      bookingHeaderId = G.Key.Id,
                                      grossPrice = G.Sum(x => x.grossPrice)
                                  }).FirstOrDefault();

                //to get pct and booking detailID
                var getData = (from A in _trBookingHeaderRepo.GetAll()
                               join B in _trBookingItemPriceRepo.GetAll() on new { A = A.Id, B = A.termID } equals new { A = B.bookingHeaderID, B = (int)B.termID }
                               join C in _trBookingDetailRepo.GetAll() on new { A = B.itemID, B = A.Id } equals new { A = C.itemID, B = C.bookingHeaderID }
                               where A.Id == grossPrice.bookingHeaderId && B.termID == input.termID
                               select new
                               {
                                   bookingDetailID = C.Id,
                                   grossPricePerItem = B.grossPrice,
                                   itemID = B.termID,
                                   percentage = B.grossPrice / grossPrice.grossPrice
                               });
                
                var percentages = getData.ToList();

                var pctDisc = (from A in _trBookingHeaderRepo.GetAll()
                               join B in _trBookingSalesDiscRepo.GetAll() on A.Id equals B.bookingHeaderID
                               where A.Id == grossPrice.bookingHeaderId
                               select B.pctDisc).FirstOrDefault();

                var disc = grossPrice.grossPrice * (decimal)pctDisc;

                var netPrice = grossPrice.grossPrice - disc;

                var addDisc = (from A in _trBookingDetailAddDiscRepo.GetAll()
                               where A.bookingDetailID == getData.FirstOrDefault().bookingDetailID
                               select new
                               {
                                   pctDisc = A.pctAddDisc,
                                   //disc = A.pctAddDisc * (double)netPrice
                               }).ToList();

                decimal totalAddDisc = 0;
                foreach (var pctAddDisc in addDisc)
                {
                    var addDiscount = (decimal)pctAddDisc.pctDisc * netPrice;

                    totalAddDisc += addDiscount;
                }

                var netNetPrice = netPrice - totalAddDisc;

                //update
                foreach (var percentage in percentages)
                {
                    var dataToUpdate = (from A in _trBookingDetailRepo.GetAll()
                                        join B in _trBookingHeaderRepo.GetAll() on A.bookingHeaderID equals B.Id
                                        where A.bookingHeaderID == grossPrice.bookingHeaderId && A.Id == percentage.bookingDetailID
                                        select A).FirstOrDefault();

                    var update = dataToUpdate.MapTo<TR_BookingDetail>();

                    update.amount = grossPrice.grossPrice * percentage.percentage;
                    update.netPrice = netPrice * percentage.percentage;
                    update.netNetPrice = netNetPrice * percentage.percentage;
                    update.pctDisc = pctDisc;
                }
                */
                #endregion

                #region old sesuai sql
                /*
                var getDataNetPrice = (
                             from bh in _contextProp.TR_BookingHeader
                             join bgross in
                                 (from bh in _contextProp.TR_BookingHeader
                                  join bip in _contextProp.TR_BookingItemPrice on bh.termID equals bip.termID
                                  where bh.unitID == input.unitID && bh.termID == input.termID && bh.Id == bip.bookingHeaderID
                                   && bh.termID == bip.termID
                                  group new { bh, bip } by new { bh.unitID, bip.termID } into grp
                                  select new
                                  {
                                      JmlGrossPrice = (double)grp.Sum(x => x.bip.grossPrice),
                                      grp.Key.unitID,
                                      grp.Key.termID
                                  })
                              on bh.unitID equals bgross.unitID
                             join bip in _contextProp.TR_BookingItemPrice on bh.Id equals bip.bookingHeaderID
                             join bd in _contextProp.TR_BookingDetail on bip.itemID equals bd.itemID
                             join bsd in _contextProp.TR_BookingSalesDisc on bh.Id equals bsd.bookingHeaderID
                             join addDisc in (
                                from bh in _contextProp.TR_BookingHeader
                                join bip in _contextProp.TR_BookingItemPrice on bh.termID equals bip.termID
                                join bd in _contextProp.TR_BookingDetail on bh.Id equals bd.bookingHeaderID
                                join bdad in _contextProp.TR_BookingDetailAddDisc on bd.Id equals bdad.bookingDetailID
                                where bip.itemID == bd.itemID && bh.unitID == input.unitID && bh.termID == input.termID && bh.Id == bip.bookingHeaderID
                                group new { bdad } by new { bdad.bookingDetailID } into grp2
                                select new
                                {
                                    JmlDisc = (double)grp2.Sum(x => x.bdad.pctAddDisc),
                                    grp2.Key.bookingDetailID
                                }
                             ) on bd.Id equals addDisc.bookingDetailID
                             where bh.termID == bgross.termID && bh.termID == bip.termID && bh.Id == bd.bookingHeaderID && bip.itemID == bsd.itemID

                             select new
                             {
                                 bd.Id,
                                 bip.grossPrice,
                                 bip.termID,
                                 PersenGrossPrice = ((double)bip.grossPrice / bgross.JmlGrossPrice),
                                 bsd.pctDisc,
                                 Disc = ((double)bip.grossPrice * bsd.pctDisc),
                                 NetPrice = ((double)bip.grossPrice - ((double)bip.grossPrice * bsd.pctDisc)),
                                 TotalAddDisc = (addDisc.JmlDisc * ((double)bip.grossPrice - ((double)bip.grossPrice * bsd.pctDisc))),
                                 NetNetPrice = (((double)bip.grossPrice - ((double)bip.grossPrice * bsd.pctDisc)) - (addDisc.JmlDisc * ((double)bip.grossPrice - ((double)bip.grossPrice * bsd.pctDisc))))
                             });
                             */
                #endregion
                Logger.Info("GeneratePrice() - Started.");

                var getDataNetPrice = (
                             from bh in _contextProp.TR_BookingHeader
                             join bgross in
                                 (from bh in _contextProp.TR_BookingHeader
                                  join bip in _contextProp.TR_BookingItemPrice on bh.termID equals bip.termID
                                  where bh.unitID == input.unitID && bh.cancelDate == null && bh.termID == input.termID && bh.Id == bip.bookingHeaderID
                                   && bh.termID == bip.termID
                                  group new { bip } by new { bip.termID } into grp
                                  select new
                                  {
                                      JmlGrossPrice = (double)grp.Sum(x => x.bip.grossPrice)
                                  })
                             on 1 equals 1
                             join bip in _contextProp.TR_BookingItemPrice on bh.Id equals bip.bookingHeaderID
                             join bd in _contextProp.TR_BookingDetail on bip.itemID equals bd.itemID
                             join bsd in _contextProp.TR_BookingSalesDisc on bh.Id equals bsd.bookingHeaderID
                             join addDisc in (
                                from bh in _contextProp.TR_BookingHeader
                                join bip in _contextProp.TR_BookingItemPrice on bh.termID equals bip.termID
                                join bd in _contextProp.TR_BookingDetail on bh.Id equals bd.bookingHeaderID
                                join bdad in _contextProp.TR_BookingDetailAddDisc on bd.Id equals bdad.bookingDetailID
                                where bip.itemID == bd.itemID && bh.unitID == input.unitID && bh.cancelDate == null && bh.termID == input.termID && bh.Id == bip.bookingHeaderID
                                group new { bdad } by new { bdad.bookingDetailID } into grp2
                                select new
                                {
                                    JmlDisc = (double)grp2.Sum(x => x.bdad.pctAddDisc),
                                    grp2.Key.bookingDetailID
                                }
                             ) on bd.Id equals addDisc.bookingDetailID
                             where bh.termID == input.termID && bh.termID == bip.termID && bh.Id == bd.bookingHeaderID && bip.itemID == bsd.itemID
                             && bh.unitID == input.unitID
                             select new
                             {
                                 bd.Id,
                                 bip.grossPrice,
                                 bip.termID,
                                 PersenGrossPrice = ((double)bip.grossPrice / bgross.JmlGrossPrice),
                                 bsd.pctDisc,
                                 Disc = ((double)bip.grossPrice * bsd.pctDisc),
                                 NetPrice = ((double)bip.grossPrice - ((double)bip.grossPrice * bsd.pctDisc)),
                                 TotalAddDisc = (addDisc.JmlDisc * ((double)bip.grossPrice - ((double)bip.grossPrice * bsd.pctDisc))),
                                 NetNetPrice = (((double)bip.grossPrice - ((double)bip.grossPrice * bsd.pctDisc)) - (addDisc.JmlDisc * ((double)bip.grossPrice - ((double)bip.grossPrice * bsd.pctDisc))))
                             }).ToList();

                var dataNetNetPrice = getDataNetPrice.Sum(x => x.NetPrice);

                //per jumlah add disc
                var getAddDisc = (from a in _trBookingDetailAddDiscRepo.GetAll()
                                  join b in _trBookingDetailRepo.GetAll()
                                  on a.bookingDetailID equals b.Id
                                  join c in _trBookingHeaderRepo.GetAll()
                                  on b.bookingHeaderID equals c.Id
                                  orderby a.addDiscNo
                                  where c.unitID == input.unitID && c.cancelDate == null
                                  select new
                                  {
                                      discountName = a.addDiscDesc,
                                      discount = a.isAmount == true ? (double)a.amtAddDisc : a.pctAddDisc,
                                      a.isAmount
                                  }).Distinct().ToList();

                foreach( var dataAddDisc in getAddDisc)
                {
                    var discountAdd = dataAddDisc.isAmount == true ? (double)dataAddDisc.discount : Math.Round(dataAddDisc.discount * dataNetNetPrice);

                    dataNetNetPrice = dataNetNetPrice - discountAdd;
                }

                //foreach (var dataAddDisc in getAddDisc)
                //{
                //    var discountAdd = dataAddDisc.isAmount == true ? (double)dataAddDisc.discount : Math.Round(dataAddDisc.discount * totalNetPrice);
                //    dataNetPrice = dataNetPrice - discountAdd;
                //    netPriceDisc = netPriceDisc + discountAdd;
                //    discDto.Add(new GetDiscountAlistDto
                //    {
                //        bookingDetailID = dataDisc.bookingDetailID,
                //        bookingDetailAddDiscID = dataDisc.bookingDetailAddDiscID,
                //        discountName = dataDisc.discountName,
                //        discount = dataDisc.discount,
                //        isAmount = dataDisc.isAmount,
                //        bangunan = discountAdd,
                //        total = discountAdd
                //    });
                //}


                if (getDataNetPrice.Any())
                {
                    //update
                    foreach (var data in getDataNetPrice)
                    {
                        var dataToUpdate = (from A in _trBookingDetailRepo.GetAll()
                                            join B in getDataNetPrice on A.Id equals B.Id
                                            where A.Id == data.Id
                                            select A).FirstOrDefault();

                        var update = dataToUpdate.MapTo<TR_BookingDetail>();

                        update.amount = data.grossPrice;
                        update.netPrice = (decimal)data.NetPrice;
                        update.netNetPrice = (decimal)dataNetNetPrice * (decimal)data.PersenGrossPrice;
                        update.pctDisc = data.pctDisc;

                        _trBookingDetailRepo.Update(update);
                        CurrentUnitOfWork.SaveChanges();
                    }

                    //SendConsole("getDataNetPrice:" + JsonConvert.SerializeObject(getDataNetPrice));
                }
                else
                {
                    throw new UserFriendlyException("Data Not Found!");
                }

                Logger.Info("GeneratePrice() - Finished.");

            }
            catch (Exception e)
            {
                SendConsole("" + e.Message + " " + e.StackTrace);
            }

            #region performance test
            watch.Stop();
            var elapsedMs = watch.ElapsedMilliseconds;
            SendConsole("stop stopwatch: " + elapsedMs + " ms");
            #endregion

        }

        public GetPSASPriceUniversalListDto UpdatePriceUniversal(UpdatePSASParamsDto input)
        {
            var unitID = GetParameter(input.paramsCheck);

            var projectCode = (from a in _msUnitRepo.GetAll()
                               join b in _msProjectRepo.GetAll() on a.projectID equals b.Id
                               select b.projectCode).FirstOrDefault();

            var getGrossPrice = new GetGrosspriceLlistDto
            {
                tanah = input.grossPriceTanah,
                bangunan = input.grossPriceBangunan,
                renov = input.grossPriceRenov,
                total = input.grossPriceTanah + input.grossPriceBangunan + input.grossPriceRenov
            };

            var getPctDisc = (from x in _trBookingDetailRepo.GetAll()
                              join a in _trBookingHeaderRepo.GetAll() on x.bookingHeaderID equals a.Id
                              where a.unitID == unitID.unitID && x.bookingHeaderID == unitID.bookingHeaderID
                              select x.pctDisc).FirstOrDefault();

            var getDiscount = new GetDiscountLlistDto
            {
                discount = getPctDisc,
                tanah = (double)input.grossPriceTanah * getPctDisc,
                bangunan = (double)input.grossPriceBangunan * getPctDisc,
                renov = (double)input.grossPriceRenov * getPctDisc,
                total = (double)getGrossPrice.total * getPctDisc
            };

            var getNetPrice = new GetNetpriceLlistDto
            {
                tanah = Convert.ToDouble(getGrossPrice.tanah) - Convert.ToDouble(getDiscount.tanah),
                bangunan = Convert.ToDouble(getGrossPrice.bangunan) - Convert.ToDouble(getDiscount.bangunan),
                renov = Convert.ToDouble(getGrossPrice.renov) - Convert.ToDouble(getDiscount.renov),
                total = Convert.ToDouble(getGrossPrice.total) - Convert.ToDouble(getDiscount.total),
            };

            //ToDo: fixing addDisc
            var getDiscountA = (from a in _trBookingDetailAddDiscRepo.GetAll()
                                join b in _trBookingDetailRepo.GetAll()
                                on a.bookingDetailID equals b.Id
                                join c in _trBookingHeaderRepo.GetAll()
                                on b.bookingHeaderID equals c.Id
                                orderby a.addDiscNo
                                where c.Id == unitID.bookingHeaderID
                                && c.unitID == unitID.unitID
                                && b.itemID == 2
                                select new
                                {
                                    bookingDetailID = b.Id,
                                    bookingDetailAddDiscID = a.Id,
                                    discountName = a.addDiscDesc,
                                    discount = a.isAmount == true ? (double)a.amtAddDisc : a.pctAddDisc,
                                    isAmount = a.isAmount,
                                    tanah = a.isAmount == true ? (double)a.amtAddDisc : Math.Round(a.pctAddDisc * (double)getNetPrice.tanah),
                                    bangunan = a.isAmount == true ? (double)a.amtAddDisc : Math.Round(a.pctAddDisc * (double)getNetPrice.bangunan),
                                    renov = a.isAmount == true ? (double)a.amtAddDisc : Math.Round(a.pctAddDisc * (double)getNetPrice.renov),
                                    //total = a.isAmount == true ? (double)a.amtAddDisc : Math.Round(a.pctAddDisc * (double)getNetPrice.bangunan)
                                }).ToList();

            var pctAddDisc = getDiscountA.Select(x => x.discount).ToList();

            double dataNetPriceBangunan = (double)getNetPrice.bangunan;
            double dataNetPriceTanah = (double)getNetPrice.tanah;
            double dataNetPriceRenov = (double)getNetPrice.renov;
            double netPriceDiscBangunan = 0;
            double netPriceDiscTanah = 0;
            double netPriceDiscRenov = 0;

            List<GetDiscountAlistDto> discDto = new List<GetDiscountAlistDto>();
            foreach (var dataDisc in getDiscountA)
            {
                var discountAddBangunan = dataDisc.isAmount == true ? (double)dataDisc.discount : Math.Round(dataDisc.discount * dataNetPriceBangunan);
                var discountAddTanah = dataDisc.isAmount == true ? (double)dataDisc.discount : Math.Round(dataDisc.discount * dataNetPriceTanah);
                var discountAddRenov = dataDisc.isAmount == true ? (double)dataDisc.discount : Math.Round(dataDisc.discount * dataNetPriceRenov);
                dataNetPriceBangunan = dataNetPriceBangunan - discountAddBangunan;
                dataNetPriceTanah = dataNetPriceTanah - discountAddTanah;
                dataNetPriceRenov = dataNetPriceRenov - discountAddRenov;
                netPriceDiscBangunan = netPriceDiscBangunan + discountAddBangunan;
                netPriceDiscTanah = netPriceDiscTanah + discountAddTanah;
                netPriceDiscRenov = netPriceDiscRenov + discountAddRenov;
                discDto.Add(new GetDiscountAlistDto
                {
                    bookingDetailID = dataDisc.bookingDetailID,
                    bookingDetailAddDiscID = dataDisc.bookingDetailAddDiscID,
                    discountName = dataDisc.discountName,
                    discount = dataDisc.discount,
                    isAmount = dataDisc.isAmount,
                    tanah = discountAddTanah,
                    bangunan = discountAddBangunan,
                    renov = discountAddRenov,
                    total = discountAddTanah + discountAddBangunan + discountAddRenov
                });
            }

            var getAddDisc = new GetAddDiscLlistDto
            {
                tanah = Math.Round(netPriceDiscTanah),
                bangunan = Math.Round(netPriceDiscBangunan),
                renov = Math.Round(netPriceDiscRenov),
                total = Math.Round(netPriceDiscTanah) + Math.Round(netPriceDiscBangunan) + Math.Round(netPriceDiscRenov)
            };

            var getNetNetPrice = new GetNetNetPriceLlistDto
            {
                tanah = Math.Round(Convert.ToDouble(getNetPrice.tanah) - Convert.ToDouble(getAddDisc.tanah)),
                bangunan = Math.Round(Convert.ToDouble(getNetPrice.bangunan) - Convert.ToDouble(getAddDisc.bangunan)),
                renov = Math.Round(Convert.ToDouble(getNetPrice.renov) - Convert.ToDouble(getAddDisc.renov)),
                total = Math.Round(Convert.ToDouble(getNetPrice.total) - Convert.ToDouble(getAddDisc.total))
            };

            var getVAT = new GetVATLlistDto
            {
                discount = 0.1,
                tanah = Math.Round(Convert.ToDouble(getNetNetPrice.tanah) * 0.1),
                bangunan = Math.Round(Convert.ToDouble(getNetNetPrice.bangunan) * 0.1),
                renov = Math.Round(Convert.ToDouble(getNetNetPrice.renov) * 0.1),
                total = Math.Round(Convert.ToDouble(getNetNetPrice.total) * 0.1)
            };

            var checkInterest = (from a in _msUnitRepo.GetAll()
                                 join b in _msProjectRepo.GetAll()
                                 on a.projectID equals b.Id
                                 where a.Id == unitID.unitID
                                 && b.projectCode == "SDH"
                                 select a).Any();

            var getInterest = new GetInterestLlistDto();

            if (checkInterest)
            {
                getInterest = new GetInterestLlistDto
                {
                    tanah = 0,
                    bangunan = 0,
                    renov = 0,
                    total = 0
                };
            }
            else
            {
                //Nothing
            }

            var getTotal = new GetTotalLlistDto
            {
                bangunan = Math.Round(Convert.ToDouble(getNetNetPrice.bangunan) + Convert.ToDouble(getVAT.bangunan) + Convert.ToDouble(getInterest.bangunan)),
                total = Math.Round(Convert.ToDouble(getNetNetPrice.total) + Convert.ToDouble(getVAT.total) + Convert.ToDouble(getInterest.total))
            };

            var result = new GetPSASPriceUniversalListDto
            {
                bookingHeaderID = unitID.bookingHeaderID,
                projectCode = projectCode,
                grossPrice = getGrossPrice,
                discount = getDiscount,
                netPrice = getNetPrice,
                addDisc = getAddDisc,
                discountA = discDto,
                netNetPrice = getNetNetPrice,
                VATPrice = getVAT,
                interest = getInterest,
                total = getTotal,
                pctAddDisc = pctAddDisc
            };

            return result;
        }


        #region debug console
        protected void SendConsole(string msg)
        {
            if (Setting_variabel.enable_tcp_debug == true)
            {
                if (Setting_variabel.Komunikasi_TCPListener == null)
                {
                    Setting_variabel.Komunikasi_TCPListener = new Visionet_Backend_NetCore.Komunikasi.Komunikasi_TCPListener(17000);
                    Task.Run(() => StartListenerLokal());
                }

                if (Setting_variabel.Komunikasi_TCPListener != null)
                {
                    if (Setting_variabel.Komunikasi_TCPListener.IsRunning())
                    {
                        if (Setting_variabel.ConsoleBayangan != null)
                        {
                            Setting_variabel.ConsoleBayangan.Send("[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "] " + msg);
                        }
                    }
                }
            }

        }
        #endregion

        #region listener tcp debug
        protected void StartListenerLokal()
        {
            if (Setting_variabel.Komunikasi_TCPListener != null)
                Setting_variabel.Komunikasi_TCPListener.StartListener();
        }

        protected void StopListenerLokal()
        {
            if (Setting_variabel.Komunikasi_TCPListener != null)
                Setting_variabel.Komunikasi_TCPListener.StopListener();
        }

        public List<GetAddDiscListDto> GetDiscountPriceMKT(GetPSASParamsDto input)
        {
            var unitID = GetParameter(input);

            var getSingleDetailID = (from a in _trBookingDetailRepo.GetAll()
                                     join c in _trBookingHeaderRepo.GetAll()
                                     on a.bookingHeaderID equals c.Id
                                     where c.Id == unitID.bookingHeaderID
                                     && c.unitID == unitID.unitID
                                     select a.Id).FirstOrDefault();

            var getDetailAddDisc = (from a in _trMktAddDiscRepo.GetAll()
                                    where a.bookingDetailID == getSingleDetailID
                                    select new GetAddDiscListDto
                                    {
                                        addDiscDesc = a.addDiscDesc,
                                        addDiscNo = a.addDiscNo,
                                        isAmount = a.isAmount,
                                        pctAddDisc = a.pctAddDisc,
                                        amtAddDisc = a.amtAddDisc
                                    }).ToList();

            return getDetailAddDisc;
        }

        public List<GetAddDiscListDto> GetDiscountPriceComm(GetPSASParamsDto input)
        {
            var unitID = GetParameter(input);

            var getSingleDetailID = (from a in _trBookingDetailRepo.GetAll()
                                     join c in _trBookingHeaderRepo.GetAll()
                                     on a.bookingHeaderID equals c.Id
                                     where c.Id == unitID.bookingHeaderID
                                     && c.unitID == unitID.unitID
                                     select a.Id).FirstOrDefault();

            var getDetailAddDisc = (from a in _trCommAddDiscRepo.GetAll()
                                    where a.bookingDetailID == getSingleDetailID
                                    select new GetAddDiscListDto
                                    {
                                        addDiscDesc = a.addDiscDesc,
                                        addDiscNo = a.addDiscNo,
                                        isAmount = a.isAmount,
                                        pctAddDisc = a.pctAddDisc,
                                        amtAddDisc = a.amtAddDisc
                                    }).ToList();

            return getDetailAddDisc;
        }

        public void CreateUpdateDiscountPriceMKT(CreateUpdatePriceParamsDto input)
        {
            Logger.Info("CreateUpdateDiscountPriceMKT() - Started.");
            var unitID = GetParameter(input.paramsCheck);

            Logger.DebugFormat("CreateUpdateDiscountPriceMKT() - Start get data Booking Detail. Parameters sent:{0}" +
                        "bookingHeaderID = {1}{0}" +
                        "unitID = {2}{0}"
                        , Environment.NewLine, unitID.bookingHeaderID, unitID.unitID);
            //REPAIR THIS
            var getbookingDetail = (from x in _trBookingDetailRepo.GetAll()
                                    join c in _trBookingHeaderRepo.GetAll()
                                    on x.bookingHeaderID equals c.Id
                                    where c.Id == unitID.bookingHeaderID
                                    && c.unitID == unitID.unitID
                                    select x).ToList();

            Logger.DebugFormat("CreateUpdateDiscountPriceMKT() - Ended get data Booking Detail.");

            foreach (var item in input.DiscountList)
            {
                Logger.DebugFormat("CreateUpdateDiscountPriceMKT() - Start checking before update TR MKT Add Disc. Parameters sent:{0}" +
                            "discNo = {1}{0}"
                            , Environment.NewLine, item.discNo);

                var checkExists = (from a in _trMktAddDiscRepo.GetAll()
                                   join x in getbookingDetail
                                   on a.bookingDetailID equals x.Id
                                   where item.discNo == a.addDiscNo
                                   select a);

                Logger.DebugFormat("CreateUpdateDiscountPriceMKT() - Ended checking before insert TR MKT Add Disc. Result = {0}", checkExists.Count());

                if (checkExists.Any())
                {
                    foreach (var disc in checkExists.ToList())
                    {
                        Logger.DebugFormat("CreateUpdateDiscountPriceMKT() - Start checking table history before insert TR MKT Add Disc History. Parameters sent:{0}" +
                            "bookingDetailID = {1}{0}"
                            , Environment.NewLine, disc.bookingDetailID);

                        var checkHistory = (from A in _trBookingDetailAddDiscHistory.GetAll()
                                            orderby A.Id descending
                                            where A.bookingDetailID == disc.bookingDetailID && A.priceType == "MKT"
                                            select A).FirstOrDefault();

                        //insert into tb history
                        var dataToInsertHistory = new TR_BookingDetailAddDiscHistory
                        {
                            addDiscDesc = disc.addDiscDesc,
                            addDiscNo = disc.addDiscNo,
                            amtAddDisc = disc.amtAddDisc,
                            isAmount = disc.isAmount,
                            pctAddDisc = disc.pctAddDisc,
                            bookingDetailID = disc.bookingDetailID,
                            entityID = disc.entityID,
                            priceType = "MKT",
                            historyNo = checkHistory == null ? (short)0 : (short)(checkHistory.historyNo + 1)
                        };

                        var update = disc.MapTo<TR_MKTAddDisc>();

                        update.pctAddDisc = input.isAmount == true ? 0 : item.pctDisc;
                        update.amtAddDisc = input.isAmount == true ? item.amountDisc : 0;
                        update.isAmount = input.isAmount;
                        try
                        {
                            _trBookingDetailAddDiscHistory.Insert(dataToInsertHistory);
                            CurrentUnitOfWork.SaveChanges();

                            Logger.DebugFormat("CreateUpdateDiscountPriceMKT() - Start update TR MKT Add Disc. Parameters sent:{0}" +
                                "pctAddDisc = {1}{0}" +
                                "amtAddDisc = {2}{0}" +
                                "isAmount = {3}{0}"
                                , Environment.NewLine, item.pctDisc, item.amountDisc, input.isAmount);

                            _trMktAddDiscRepo.Update(update);
                            CurrentUnitOfWork.SaveChanges();

                            Logger.DebugFormat("CreateUpdateDiscountPriceMKT() - Ended update TR MKT Add Disc.");
                        }
                        catch (DataException ex)
                        {
                            Logger.ErrorFormat("CreateUpdateDiscountPriceMKT() - ERROR DataException. Result = {0}", ex.Message);
                            throw new UserFriendlyException("Db Error: " + ex.Message);
                        }
                        catch (Exception ex)
                        {
                            Logger.ErrorFormat("CreateUpdateDiscountPriceMKT() - ERROR Exception. Result = {0}", ex.Message);
                            throw new UserFriendlyException("Error: " + ex.Message);
                        }

                    }
                }
                else
                {
                    foreach (var detail in getbookingDetail)
                    {
                        var insert = new TR_MKTAddDisc
                        {
                            addDiscDesc = item.addDiscDesc,
                            addDiscNo = item.discNo,
                            amtAddDisc = input.isAmount == true ? item.amountDisc : 0,
                            isAmount = input.isAmount,
                            pctAddDisc = input.isAmount == true ? 0 : item.pctDisc,
                            bookingDetailID = detail.Id,
                            entityID = 1
                        };

                        try
                        {
                            Logger.DebugFormat("CreateUpdateDiscountPriceMKT() - Start insert TR MKT Add Disc. Parameters sent:{0}" +
                                "entityID = {1}{0}" +
                                "addDiscDesc = {2}{0}" +
                                "addDiscNo = {3}{0}" +
                                "amtAddDisc = {4}{0}" +
                                "isAmount = {5}{0}" +
                                "pctAddDisc = {6}{0}" +
                                "bookingDetailID = {7}{0}"
                                , Environment.NewLine, 1, item.addDiscDesc, item.discNo, item.amountDisc, input.isAmount, item.pctDisc, detail.Id);

                            _trMktAddDiscRepo.Insert(insert);
                            CurrentUnitOfWork.SaveChanges();

                            Logger.DebugFormat("CreateUpdateDiscountPriceMKT() - Ended insert TR MKT Add Disc.");
                        }
                        catch (DataException ex)
                        {
                            Logger.ErrorFormat("CreateUpdateDiscountPriceMKT() - ERROR DataException. Result = {0}", ex.Message);
                            throw new UserFriendlyException("Db Error: " + ex.Message);
                        }
                        catch (Exception ex)
                        {
                            Logger.ErrorFormat("CreateUpdateDiscountPriceMKT() - ERROR Exception. Result = {0}", ex.Message);
                            throw new UserFriendlyException("Error: " + ex.Message);
                        }
                    }
                }
            }
            Logger.Info("CreateUpdateDiscountPriceMKT() - Finished.");
        }

        public void CreateUpdateDiscountPriceComm(CreateUpdatePriceParamsDto input)
        {
            Logger.Info("CreateUpdateDiscountPriceComm() - Started.");
            var unitID = GetParameter(input.paramsCheck);

            Logger.DebugFormat("CreateUpdateDiscountPriceComm() - Start get data Booking Detail. Parameters sent:{0}" +
                        "bookingHeaderID = {1}{0}" +
                        "unitID = {2}{0}"
                        , Environment.NewLine, unitID.bookingHeaderID, unitID.unitID);
            //REPAIR THIS
            var getbookingDetail = (from x in _trBookingDetailRepo.GetAll()
                                    join c in _trBookingHeaderRepo.GetAll()
                                    on x.bookingHeaderID equals c.Id
                                    where c.Id == unitID.bookingHeaderID
                                    && c.unitID == unitID.unitID
                                    select x).ToList();

            Logger.DebugFormat("CreateUpdateDiscountPriceComm() - Ended get data Booking Detail.");

            foreach (var item in input.DiscountList)
            {
                Logger.DebugFormat("CreateUpdateDiscountPriceComm() - Start checking before update TR Booking Detail Add Disc. Parameters sent:{0}" +
                            "discNo = {1}{0}"
                            , Environment.NewLine, item.discNo);

                var checkExists = (from a in _trCommAddDiscRepo.GetAll()
                                   join x in getbookingDetail
                                   on a.bookingDetailID equals x.Id
                                   where item.discNo == a.addDiscNo
                                   select a);

                Logger.DebugFormat("CreateUpdateDiscountPriceComm() - Ended checking before insert TR Booking Detail Add Disc. Result = {0}", checkExists.Count());

                if (checkExists.Any())
                {
                    foreach (var disc in checkExists.ToList())
                    {
                        Logger.DebugFormat("CreateUpdateDiscountPriceComm() - Start checking table history before insert TR Comm Add Disc History. Parameters sent:{0}" +
                            "bookingDetailID = {1}{0}"
                            , Environment.NewLine, disc.bookingDetailID);

                        var checkHistory = (from A in _trBookingDetailAddDiscHistory.GetAll()
                                            orderby A.Id descending
                                            where A.bookingDetailID == disc.bookingDetailID && A.priceType == "COMM"
                                            select A).FirstOrDefault();

                        //insert into tb history
                        var dataToInsertHistory = new TR_BookingDetailAddDiscHistory
                        {
                            addDiscDesc = disc.addDiscDesc,
                            addDiscNo = disc.addDiscNo,
                            amtAddDisc = disc.amtAddDisc,
                            isAmount = disc.isAmount,
                            pctAddDisc = disc.pctAddDisc,
                            bookingDetailID = disc.bookingDetailID,
                            entityID = disc.entityID,
                            priceType = "COMM",
                            historyNo = checkHistory == null ? (short)0 : (short)(checkHistory.historyNo + 1)
                        };

                        var update = disc.MapTo<TR_CommAddDisc>();

                        update.pctAddDisc = input.isAmount == true ? 0 : item.pctDisc;
                        update.amtAddDisc = input.isAmount == true ? item.amountDisc : 0;
                        update.isAmount = input.isAmount;
                        try
                        {
                            _trBookingDetailAddDiscHistory.Insert(dataToInsertHistory);
                            CurrentUnitOfWork.SaveChanges();

                            Logger.DebugFormat("CreateUpdateDiscountPriceComm() - Start update TR Comm Add Disc. Parameters sent:{0}" +
                                "pctAddDisc = {1}{0}" +
                                "amtAddDisc = {2}{0}" +
                                "isAmount = {3}{0}"
                                , Environment.NewLine, item.pctDisc, item.amountDisc, input.isAmount);

                            _trCommAddDiscRepo.Update(update);
                            CurrentUnitOfWork.SaveChanges();

                            Logger.DebugFormat("CreateUpdateDiscountPriceComm() - Ended update TR Comm Add Disc.");
                        }
                        catch (DataException ex)
                        {
                            Logger.ErrorFormat("CreateUpdateDiscountPriceComm() - ERROR DataException. Result = {0}", ex.Message);
                            throw new UserFriendlyException("Db Error: " + ex.Message);
                        }
                        catch (Exception ex)
                        {
                            Logger.ErrorFormat("CreateUpdateDiscountPriceComm() - ERROR Exception. Result = {0}", ex.Message);
                            throw new UserFriendlyException("Error: " + ex.Message);
                        }

                    }
                }
                else
                {
                    foreach (var detail in getbookingDetail)
                    {
                        var insert = new TR_CommAddDisc
                        {
                            addDiscDesc = item.addDiscDesc,
                            addDiscNo = item.discNo,
                            amtAddDisc = input.isAmount == true ? item.amountDisc : 0,
                            isAmount = input.isAmount,
                            pctAddDisc = input.isAmount == true ? 0 : item.pctDisc,
                            bookingDetailID = detail.Id,
                            entityID = 1
                        };

                        try
                        {
                            Logger.DebugFormat("CreateUpdateDiscountPriceComm() - Start insert TR Comm Add Disc. Parameters sent:{0}" +
                                "entityID = {1}{0}" +
                                "addDiscDesc = {2}{0}" +
                                "addDiscNo = {3}{0}" +
                                "amtAddDisc = {4}{0}" +
                                "isAmount = {5}{0}" +
                                "pctAddDisc = {6}{0}" +
                                "bookingDetailID = {7}{0}"
                                , Environment.NewLine, 1, item.addDiscDesc, item.discNo, item.amountDisc, input.isAmount, item.pctDisc, detail.Id);

                            _trCommAddDiscRepo.Insert(insert);
                            CurrentUnitOfWork.SaveChanges();

                            Logger.DebugFormat("CreateUpdateDiscountPriceComm() - Ended insert TR Comm Add Disc.");
                        }
                        catch (DataException ex)
                        {
                            Logger.ErrorFormat("CreateUpdateDiscountPriceComm() - ERROR DataException. Result = {0}", ex.Message);
                            throw new UserFriendlyException("Db Error: " + ex.Message);
                        }
                        catch (Exception ex)
                        {
                            Logger.ErrorFormat("CreateUpdateDiscountPriceComm() - ERROR Exception. Result = {0}", ex.Message);
                            throw new UserFriendlyException("Error: " + ex.Message);
                        }
                    }
                }
            }
            Logger.Info("CreateUpdateDiscountPriceComm() - Finished.");
        }
        #endregion
    }
}
