﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.PropertySystemDB.LippoMaster;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;
using VDI.Demo.PSAS.Dto;
using System.Linq;
using VDI.Demo.PSAS.Price.Dto;
using Abp.UI;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;
using System.Threading.Tasks;
using Abp.AutoMapper;
using System.Data;
using VDI.Demo.PSAS.Term.Dto;
using System.Diagnostics;
using Newtonsoft.Json;
using Visionet_Backend_NetCore.Komunikasi;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.PSAS.ChangeOwnership.Dto;
using VDI.Demo.PSAS.Price;
using VDI.Demo.PSAS.AccountStatement.Dto;
using VDI.Demo.PropertySystemDB.Pricing;
using Abp.Collections.Extensions;
using Abp.Extensions;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using System.IO;
using System.Net.Http.Headers;
using Abp.AspNetZeroCore.Net;
using Microsoft.AspNetCore.Hosting;
using VDI.Demo.Dto;
using VDI.Demo.DataExporting.Pdf.Exporter.AccountStatement;

namespace VDI.Demo.PSAS.AccountStatement
{
    public class PSASAccountStatementAppService : DemoAppServiceBase, IAccountStatementPSASAppService
    {
        private readonly IRepository<TR_BookingHeader> _trBookingHeaderRepo;
        private readonly IRepository<TR_BookingDetail> _trBookingDetailRepo;
        private readonly IRepository<TR_BookingDetailAddDisc> _trBookingDetailAddDiscRepo;
        private readonly IRepository<MS_Unit> _msUnitRepo;
        private readonly IRepository<MS_UnitCode> _msUnitCodeRepo;
        private readonly IRepository<MS_Project> _msProjectRepo;
        private readonly IRepository<TR_BookingItemPrice> _trBookingItemPriceRepo;
        private readonly IRepository<TR_BookingSalesDisc> _trBookingSalesDiscRepo;
        private readonly PropertySystemDbContext _contextProp;
        private readonly IRepository<TR_BookingDetailAddDiscHistory> _trBookingDetailAddDiscHistory;
        private readonly IRepository<TR_MKTAddDisc> _trMktAddDiscRepo;
        private readonly IRepository<TR_CommAddDisc> _trCommAddDiscRepo;
        private readonly IRepository<TR_BookingDetailHistory> _trBookingDetailHistoryRepo;
        private readonly IRepository<LK_Item> _lkItemRepo;
        private readonly PersonalsNewDbContext _contextPers;
        private readonly IPSASPriceAppService _iPriceAppService;
        private readonly IRepository<TR_BookingChangeOwner> _trBookingChangeOwnerRepo;
        private readonly IRepository<MS_Bank> _msBankRepo;
        private readonly IRepository<TR_BookingHeaderHistory> _trBookingHeaderHistoryRepo;
        private readonly IRepository<MS_Parameter> _msParameterRepo;
        private readonly IRepository<TR_PaymentHeader> _trPaymentHeaderRepo;
        private readonly IRepository<TR_PaymentDetail> _trPaymentDetailRepo;
        private readonly IRepository<TR_PaymentDetailAlloc> _trPaymentDetailAllocRepo;
        private readonly IRepository<LK_PayType> _lkPayTypeRepo;
        private readonly IRepository<LK_PayFor> _lkPayForRepo;
        private readonly IRepository<TR_BookingDetailSchedule> _trBookingDetailScheduleRepo;
        private readonly IRepository<LK_Alloc> _lkAllocRepo;
        private readonly IRepository<MS_Term> _msTermRepo;
        private readonly IRepository<MS_TermPmt> _msTermPmtRepo;
        private readonly IRepository<LK_FinType> _lkFinTypeRepo;
        private readonly IRepository<MS_Company> _msCompanyRepo;
        private readonly IRepository<MS_Account> _msAccountRepo;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IGenerateAccountStatementExporter _generateAccountStatementExporter;

        public PSASAccountStatementAppService(
            IRepository<TR_BookingHeader> trBookingHeaderRepo,
            IRepository<TR_BookingDetail> trBookingDetailRepo,
            IRepository<TR_BookingDetailAddDisc> trBookingDetailAddDiscRepo,
            IRepository<MS_Unit> msUnitRepo,
            IRepository<MS_UnitCode> msUnitCodeRepo,
            IRepository<MS_Project> msProjectRepo,
            IRepository<TR_BookingItemPrice> trBookingItemPriceRepo,
            IRepository<TR_BookingSalesDisc> trBookingSalesDiscRepo,
            PropertySystemDbContext contextProp,
            IRepository<TR_BookingDetailAddDiscHistory> trBookingDetailAddDiscHistoryRepo,
            IRepository<TR_MKTAddDisc> trMktAddDiscRepo,
            IRepository<TR_CommAddDisc> trCommAddDiscRepo,
            IRepository<TR_BookingDetailHistory> trBookingDetailHistoryRepo,
            IRepository<LK_Item> lkItemRepo,
            PersonalsNewDbContext contextPers,
            IPSASPriceAppService iPriceAppService,
            IRepository<TR_BookingChangeOwner> trBookingChangeOwnerRepo,
            IRepository<MS_Bank> msBankRepo,
            IRepository<TR_BookingHeaderHistory> trBookingHeaderHistoryRepo,
            IRepository<MS_Parameter> msParameterRepo,
            IRepository<TR_PaymentHeader> trPaymentHeaderRepo,
            IRepository<TR_PaymentDetail> trPaymentDetailRepo,
            IRepository<TR_PaymentDetailAlloc> trPaymentDetailAllocRepo,
            IRepository<LK_PayType> lkPayTypeRepo,
            IRepository<LK_PayFor> lkPayForRepo,
            IRepository<TR_BookingDetailSchedule> trBookingDetailScheduleRepo,
            IRepository<LK_Alloc> lkAllocRepo,
            IRepository<MS_Term> msTermRepo,
            IRepository<MS_TermPmt> msTermPmtRepo,
            IRepository<LK_FinType> lkFinTypeRepo,
            IRepository<MS_Company> msCompanyRepo,
            IHostingEnvironment hostingEnvironment,
            IGenerateAccountStatementExporter generateAccountStatementExporter
            )
        {
            _trBookingHeaderRepo = trBookingHeaderRepo;
            _trBookingDetailRepo = trBookingDetailRepo;
            _msUnitRepo = msUnitRepo;
            _msUnitCodeRepo = msUnitCodeRepo;
            _trBookingDetailAddDiscRepo = trBookingDetailAddDiscRepo;
            _msProjectRepo = msProjectRepo;
            _trBookingItemPriceRepo = trBookingItemPriceRepo;
            _trBookingSalesDiscRepo = trBookingSalesDiscRepo;
            _contextProp = contextProp;
            _trBookingDetailAddDiscHistory = trBookingDetailAddDiscHistoryRepo;
            _trMktAddDiscRepo = trMktAddDiscRepo;
            _trCommAddDiscRepo = trCommAddDiscRepo;
            _trBookingDetailHistoryRepo = trBookingDetailHistoryRepo;
            _lkItemRepo = lkItemRepo;
            _contextPers = contextPers;
            _iPriceAppService = iPriceAppService;
            _trBookingChangeOwnerRepo = trBookingChangeOwnerRepo;
            _msBankRepo = msBankRepo;
            _trBookingHeaderHistoryRepo = trBookingHeaderHistoryRepo;
            _msParameterRepo = msParameterRepo;
            _trPaymentHeaderRepo = trPaymentHeaderRepo;
            _trPaymentDetailRepo = trPaymentDetailRepo;
            _trPaymentDetailAllocRepo = trPaymentDetailAllocRepo;
            _lkPayTypeRepo = lkPayTypeRepo;
            _lkPayForRepo = lkPayForRepo;
            _trBookingDetailScheduleRepo = trBookingDetailScheduleRepo;
            _lkAllocRepo = lkAllocRepo;
            _msTermRepo = msTermRepo;
            _msTermPmtRepo = msTermPmtRepo;
            _lkFinTypeRepo = lkFinTypeRepo;
            _msCompanyRepo = msCompanyRepo;
            _hostingEnvironment = hostingEnvironment;
            _generateAccountStatementExporter = generateAccountStatementExporter;
        }

        public FileDto GetAccountStatementToExport(AccountStatementInputDto input)
        {
            //Logger.DebugFormat("Manda BookCode|"+input.bookCode+"|");
            //Logger.DebugFormat("Manda PsCode|" + input.psCode + "|");
            var webRootPath = _hostingEnvironment.WebRootPath;

            List<GetAccountStatementToExportDto> dataResult = new List<GetAccountStatementToExportDto>();

            List<DataSchedulePayment> dataTabel = new List<DataSchedulePayment>();

            var getDataPersonals = _contextPers.PERSONAL.ToList().Join(_contextPers.PERSONALS_MEMBER, x => x.psCode, y => y.psCode, (query_personalRepo, query_personalMemberRepo) => new { query_personalRepo, query_personalMemberRepo })
                .Where(s => s.query_personalRepo.psCode.Equals(s.query_personalMemberRepo.psCode));

            var getDataUnitCust = (from bh in _contextProp.TR_BookingHeader
                                   join u in _contextProp.MS_Unit on bh.unitID equals u.Id
                                   join uc in _contextProp.MS_UnitCode on u.unitCodeID equals uc.Id
                                   join pr in _contextProp.MS_Project on u.projectID equals pr.Id
                                   join t in _contextProp.MS_Term on bh.termID equals t.Id
                                   join tp in _contextProp.MS_TermPmt on t.Id equals tp.termID
                                   join ft in _contextProp.LK_FinType on tp.finTypeID equals ft.Id
                                   //join p in _contextPers.PERSONAL.ToList() on bh.psCode equals p.psCode into l1
                                   //from p in l1.DefaultIfEmpty()
                                   //join pm in _contextPers.PERSONALS_MEMBER.ToList() on new { A = bh.memberCode, B = p.psCode } equals new { A = pm.memberCode, B = pm.psCode } into l2
                                   //from pm in l2.DefaultIfEmpty()
                                   join px in getDataPersonals on bh.psCode equals px.query_personalRepo.psCode into l2
                                   from px in l2.DefaultIfEmpty()
                                   select new
                                   {
                                       bookingHeaderID = bh.Id,
                                       bh.bookCode,
                                       bh.bookDate,
                                       u.unitNo,
                                       uc.unitCode,
                                       pr.projectName,
                                       projectImage = pr.image,
                                       paymentTerm = ft.finTypeDesc,
                                       psCode = px == null ? "-" : px.query_personalRepo == null ? "-" : px.query_personalRepo.psCode,
                                       psName = px == null ? "-" : px.query_personalRepo == null ? "-" : px.query_personalRepo.name
                                   })
                                   .WhereIf(input.flag == "psCode", x => x.psCode == input.value)
                                   .WhereIf(input.flag == "bookCode", x => x.bookCode == input.value)
                                   .ToList();

            if (!getDataUnitCust.Any()) throw new UserFriendlyException("Data Not Found!");

            foreach (var dataBooking in getDataUnitCust)
            {
                var getCompanyImage = (from cp in _contextProp.MS_Company
                                       join bd in _contextProp.TR_BookingDetail on cp.coCode equals bd.coCode
                                       where bd.bookingHeaderID == dataBooking.bookingHeaderID
                                       orderby bd.itemID ascending
                                       select cp.image).FirstOrDefault();

                var getDataSellingPrice = (from bd in _contextProp.TR_BookingDetail
                                           join bh in _contextProp.TR_BookingHeader on bd.bookingHeaderID equals bh.Id
                                           where bh.Id == dataBooking.bookingHeaderID
                                           select bd.netNetPrice).Sum(x => x);

                var getDataSales = (from bh in _contextProp.TR_BookingHeader.ToList()
                                    join pm in _contextPers.PERSONALS_MEMBER on bh.memberCode equals pm.memberCode into pers
                                    from pm in pers.DefaultIfEmpty()
                                    join p in _contextPers.PERSONAL on pm.psCode equals p.psCode into l1
                                    from p in l1.DefaultIfEmpty()
                                    join tp in _contextPers.TR_Phone on p.psCode equals tp.psCode into l2
                                    from tp in l2.DefaultIfEmpty()
                                    join e in _contextPers.TR_Email on p.psCode equals e.psCode into l3
                                    from e in l3.DefaultIfEmpty()
                                    where bh.Id == dataBooking.bookingHeaderID
                                    select new
                                    {
                                        salesCode = p == null ? "-" : p.psCode,
                                        salesName = p == null ? "-" : p.name,
                                        salesPhone = tp == null ? "-" : tp.number,
                                        salesEmail = e == null ? "-" : e.email
                                    }).FirstOrDefault();

                var getDataPayment = (from pda in _contextProp.TR_PaymentDetailAlloc
                                      join pd in _contextProp.TR_PaymentDetail on pda.paymentDetailID equals pd.Id
                                      join ph in _contextProp.TR_PaymentHeader on pd.paymentHeaderID equals ph.Id
                                      join pf in _contextProp.LK_PayFor on ph.payForID equals pf.Id
                                      join pt in _contextProp.LK_PayType on pd.payTypeID equals pt.Id
                                      where ph.bookingHeaderID == dataBooking.bookingHeaderID
                                      select new DataSchedulePayment
                                      {
                                          paymentAmount = NumberHelper.IndoFormat(pda.netAmt + pda.vatAmt),
                                          totalSchedulePayment = pda.vatAmt + pda.netAmt,
                                          paymentDate = ph.paymentDate.ToString("dd/mm/yyyy"),
                                          transactionDesc = pf.payForName + " - " + pt.payTypeDesc,
                                          orderDate = ph.paymentDate,
                                          type = 'P'
                                      }).ToList();

                dataTabel.AddRange(getDataPayment);

                var getDataSchedule = (from x in _contextProp.TR_BookingDetailSchedule
                                       join a in _contextProp.TR_BookingDetail on x.bookingDetailID equals a.Id
                                       join b in _contextProp.TR_BookingHeader on a.bookingHeaderID equals b.Id
                                       join c in _contextProp.LK_Alloc on x.allocID equals c.Id
                                       where b.Id == dataBooking.bookingHeaderID
                                       select new DataSchedulePayment
                                       {
                                           scheduleAmount = NumberHelper.IndoFormat(x.vatAmt + x.netAmt),
                                           totalSchedulePayment = x.vatAmt + x.netAmt,
                                           dueDate = x.dueDate.ToString("dd/mm/yyyy"),
                                           transactionDesc = c.allocDesc,
                                           allocCode = c.allocCode,
                                           orderDate = x.dueDate,
                                           type = 'S'
                                       }).ToList();

                dataTabel.AddRange(getDataSchedule);

                var companyImage = GetImageChecker(getCompanyImage);
                var projectImage = GetImageChecker(getCompanyImage);

                var totalScheduleAmount = getDataSchedule.Sum(x => x.totalSchedulePayment);
                var totalPaymentAmount = getDataPayment.Sum(x => x.totalSchedulePayment);
                var outstandingAmount = totalScheduleAmount - totalPaymentAmount;
                var outstandingPenalty = getDataSchedule.Where(x => x.allocCode == "PEN").Select(x => x.totalSchedulePayment).Sum();
                var totalBalance = outstandingAmount + outstandingPenalty;

                var dataToPush = new GetAccountStatementToExportDto
                {
                    projectName = dataBooking == null ? "-" : dataBooking.projectName,
                    projectImage = projectImage,
                    publishDate = DateTime.Now.ToString("MM dd yyyy"),
                    companyImage = companyImage,
                    bookCode = dataBooking == null ? "-" : dataBooking.bookCode,
                    bookDate = dataBooking == null ? "-" : dataBooking.bookDate.ToString("dd/mm/yyyy"),
                    unitCode = dataBooking == null ? "-" : dataBooking.unitCode,
                    unitNo = dataBooking == null ? "-" : dataBooking.unitNo,
                    psCode = dataBooking == null ? "-" : dataBooking.psCode,
                    psName = dataBooking == null ? "-" : dataBooking.psName,
                    salesName = getDataSales == null ? "-" : getDataSales.salesName,
                    salesEmail = getDataSales == null ? "-" : getDataSales.salesEmail,
                    salesPhone = getDataSales == null ? "-" : getDataSales.salesPhone,
                    paymentTerm = dataBooking == null ? "-" : dataBooking.paymentTerm,
                    sellingPrice = NumberHelper.IndoFormat(getDataSellingPrice),
                    totalScheduleAmount = NumberHelper.IndoFormat(totalScheduleAmount),
                    totalPaymentAmount = NumberHelper.IndoFormat(totalPaymentAmount),
                    outstandingAmount = NumberHelper.IndoFormat(outstandingAmount),
                    outstandingPenalty = NumberHelper.IndoFormat(outstandingPenalty),
                    totalBalance = NumberHelper.IndoFormat(totalBalance),
                    getDataSchedulePayment = dataTabel.OrderBy(x => x.orderDate).ToList()
                };
                dataResult.Add(dataToPush);
            }
            return _generateAccountStatementExporter.GeneratePdfAccountStatement(dataResult);
        }
        
        private string GetImageChecker(string getImage)
        {
            var image = "";
            if (getImage != null && getImage != "-")
            {
                var fileImage = "";
                if (getImage[0].Equals('/'))
                {
                    fileImage = getImage.Substring(1);
                }
                else
                {
                    fileImage = getImage;
                }
                var imagePath = Path.Combine(_hostingEnvironment.WebRootPath, fileImage);

                image = Convert.ToBase64String(File.ReadAllBytes(imagePath));
            }
            else
            {
                image = Convert.ToBase64String(File.ReadAllBytes(@"wwwroot\Assets\Upload\CompanyImage\imageDefault.jpeg"));
            }

            return image;
        }

        private async Task<string> ReadResponse(string url, HttpClient client, HttpRequestMessage request)
        {
            var response = await client.SendAsync(request);

            var success = await response.Content.ReadAsStringAsync();
            var indexFirstSlash = success.IndexOf(@"\");
            var trimString = success.Substring(indexFirstSlash + 1, success.Length).Trim(new char[1] { '"' });

            return trimString;
        }

        public List<GetMsBankDropdownListDto> GetBankDropdown()
        {
            var getBankDropDown = (from A in _msBankRepo.GetAll()
                                   orderby A.bankCode descending
                                   select new GetMsBankDropdownListDto
                                   {
                                       bankID = A.Id,
                                       bankCode = A.bankCode,
                                       bankName = A.bankName
                                   }).ToList();

            return getBankDropDown;
        }

        public List<string> GetChangeOwnerType()
        {
            var getDataChangeOwnerType = (from A in _msParameterRepo.GetAll()
                                          where A.code == "DRCOW"
                                          orderby A.value
                                          select A.value).ToList();

            return getDataChangeOwnerType;
        }

    }
}