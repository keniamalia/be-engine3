﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.NewCommDB;
using VDI.Demo.Payment.InputPayment;
using VDI.Demo.Payment.InputPayment.Dto;
using VDI.Demo.PersonalsDB;
using VDI.Demo.PropertySystemDB.LippoMaster;
using VDI.Demo.PropertySystemDB.MasterPlan.Project;
using VDI.Demo.PropertySystemDB.MasterPlan.Unit;
using VDI.Demo.PropertySystemDB.Pricing;
using VDI.Demo.PSAS.CancelUnit.Dto;
using VDI.Demo.PSAS.ChangeRenov.Dto;
using VDI.Demo.PSAS.Dto;
using VDI.Demo.PSAS.Main.Dto;
using VDI.Demo.PSAS.Price;
using VDI.Demo.PSAS.Schedule;
using VDI.Demo.PSAS.Schedule.Dto;

namespace VDI.Demo.PSAS.CancelUnit
{
    public class PSASCancelUnitAppService : DemoAppServiceBase, ICancelUnitPSASAppService
    {
        private readonly IRepository<MS_Unit> _msUnitRepo;
        private readonly IRepository<MS_UnitCode> _msUnitCodeRepo;
        private readonly IRepository<MS_UnitItem> _msUnitItemRepo;
        private readonly IRepository<LK_Item> _lkItemRepo;
        private readonly IRepository<TR_BookingHeader> _trBookingHeaderRepo;
        private readonly IRepository<MS_Area> _msAreaRepo;
        private readonly IRepository<MS_Project> _msProjectRepo;
        private readonly IRepository<MS_Product> _msProductRepo;
        private readonly IRepository<MS_Cluster> _msClusterRepo;
        private readonly IRepository<MS_Category> _msCategoryRepo;
        private readonly IRepository<MS_Detail> _msDetailRepo;
        private readonly IRepository<MS_Term> _msTermRepo;
        private readonly IRepository<LK_UnitStatus> _lkUnitStatusRepo;
        private readonly IRepository<PERSONALS, string> _personalsRepo;
        private readonly IRepository<LK_PayType> _lkPayTypeRepo;
        private readonly IRepository<MS_TransFrom> _msTransformRepo;
        private readonly IRepository<MS_Schema, string> _msSchemaRepo;
        private readonly IRepository<MS_ShopBusiness> _msShopBusinessRepo;
        private readonly IRepository<MS_SalesEvent> _msSalesEventRepo;
        private readonly IRepository<LK_SADStatus> _lkSADStatusRepo;
        private readonly IRepository<LK_Promotion> _lkPromotionRepo;
        private readonly IRepository<TR_BookingDetail> _trBookingDetailRepo;
        private readonly IRepository<MS_Company> _msCompanyRepo;
        private readonly IRepository<TR_Address, string> _trAddressRepo;
        private readonly IRepository<PERSONALS_MEMBER, string> _personalMemberRepo;
        private readonly IRepository<TR_Phone, string> _trPhoneRepo;
        private readonly IRepository<LK_Reason> _lkReasonRepo;
        private readonly IPSASPriceAppService _iPriceAppService;
        private readonly PersonalsNewDbContext _contextPers;
        private readonly NewCommDbContext _contextNew;
        private readonly PropertySystemDbContext _contextProp;
        private readonly IRepository<TR_BookingItemPrice> _trBookingItemPriceRepo;
        private readonly IRepository<TR_BookingDetailAddDisc> _trBookingDetailAddDiscRepo;
        private readonly IRepository<TR_PaymentDetailAlloc> _trPaymentDetailAllocRepo;
        private readonly IRepository<TR_PaymentHeader> _trPaymentHeaderRepo;
        private readonly IRepository<TR_PaymentDetail> _trPaymentDetailRepo;
        private readonly IRepository<LK_PayFor> _lkPayForRepo;
        private readonly IRepository<TR_BookingCancel> _trBookingCancelRepo;
        private readonly IInputPaymentAppService _iInputPaymentAppService;
        private readonly IPSASScheduleAppService _iPSASScheduleAppService;
        private readonly IRepository<TR_BookingHeaderHistory> _trBookingHeaderHistoryRepo;
        private readonly IRepository<MS_Bank> _msBankRepo;
        private readonly IRepository<MS_Account> _msAccountRepo;
        private readonly AccountingDbContext _contextAccounting;

        public PSASCancelUnitAppService(
            IRepository<MS_UnitItem> msUnitItem,
            IRepository<LK_Item> lkItem,
            IRepository<MS_Unit> msUnit,
            IRepository<MS_UnitCode> msUnitCode,
            IRepository<TR_BookingHeader> trBookingHeader,
            IRepository<MS_Area> msArea,
            IRepository<MS_Project> msProject,
            IRepository<MS_Product> msProduct,
            IRepository<MS_Cluster> msCluster,
            IRepository<MS_Category> msCategory,
            IRepository<MS_Detail> msDetail,
            IRepository<MS_Term> msTerm,
            IRepository<LK_UnitStatus> lkUnitStatus,
            IRepository<PERSONALS, string> personals,
            IRepository<LK_PayType> lkPayType,
            IRepository<MS_TransFrom> msTransform,
            IRepository<MS_Schema, string> msSchema,
            IRepository<MS_ShopBusiness> msShopBusiness,
            IRepository<MS_SalesEvent> msSalesEvent,
            IRepository<LK_SADStatus> lkSADStatus,
            IRepository<LK_Promotion> lkPromotion,
            IRepository<TR_BookingDetail> trBookingDetail,
            IRepository<MS_Company> msCompanyRepo,
            IRepository<TR_Address, string> trAddressRepo,
            IRepository<PERSONALS_MEMBER, string> personalMemberRepo,
            IRepository<TR_Phone, string> trPhoneRepo,
            IRepository<LK_Reason> lkReasonRepo,
            IPSASPriceAppService iPriceAppService,
            PersonalsNewDbContext contextPers,
            NewCommDbContext contextNew,
            PropertySystemDbContext contextProp,
            IRepository<TR_BookingItemPrice> trBookingItemPriceRepo,
            IRepository<TR_BookingDetailAddDisc> trBookingDetailAddDiscRepo,
            IRepository<TR_PaymentDetailAlloc> trPaymentDetailAllocRepo,
            IRepository<TR_PaymentHeader> trPaymentHeaderRepo,
            IRepository<TR_PaymentDetail> trPaymentDetailRepo,
            IRepository<LK_PayFor> lkPayForRepo,
            IRepository<TR_BookingCancel> trBookingCancelRepo,
            IInputPaymentAppService iInputPaymentAppService,
            IPSASScheduleAppService IPSASScheduleAppService,
            IRepository<TR_BookingHeaderHistory> trBookingHeaderHistoryRepo,
            IRepository<MS_Bank> msBankRepo,
            IRepository<MS_Account> msAccountRepo,
            AccountingDbContext contextAccounting
        )
        {
            _contextProp = contextProp;
            _msUnitItemRepo = msUnitItem;
            _lkItemRepo = lkItem;
            _msUnitRepo = msUnit;
            _msUnitCodeRepo = msUnitCode;
            _trBookingHeaderRepo = trBookingHeader;
            _msAreaRepo = msArea;
            _msProjectRepo = msProject;
            _msProductRepo = msProduct;
            _msClusterRepo = msCluster;
            _msCategoryRepo = msCategory;
            _msDetailRepo = msDetail;
            _msTermRepo = msTerm;
            _lkUnitStatusRepo = lkUnitStatus;
            _personalsRepo = personals;
            _lkPayTypeRepo = lkPayType;
            _msTransformRepo = msTransform;
            _msSchemaRepo = msSchema;
            _msShopBusinessRepo = msShopBusiness;
            _msSalesEventRepo = msSalesEvent;
            _lkSADStatusRepo = lkSADStatus;
            _lkPromotionRepo = lkPromotion;
            _trBookingDetailRepo = trBookingDetail;
            _msCompanyRepo = msCompanyRepo;
            _trAddressRepo = trAddressRepo;
            _personalMemberRepo = personalMemberRepo;
            _trPhoneRepo = trPhoneRepo;
            _lkReasonRepo = lkReasonRepo;
            _iPriceAppService = iPriceAppService;
            _contextPers = contextPers;
            _contextNew = contextNew;
            _trBookingItemPriceRepo = trBookingItemPriceRepo;
            _trBookingDetailAddDiscRepo = trBookingDetailAddDiscRepo;
            _trPaymentDetailAllocRepo = trPaymentDetailAllocRepo;
            _trPaymentHeaderRepo = trPaymentHeaderRepo;
            _trPaymentDetailRepo = trPaymentDetailRepo;
            _lkPayForRepo = lkPayForRepo;
            _trBookingCancelRepo = trBookingCancelRepo;
            _iInputPaymentAppService = iInputPaymentAppService;
            _iPSASScheduleAppService = IPSASScheduleAppService;
            _trBookingHeaderHistoryRepo = trBookingHeaderHistoryRepo;
            _msBankRepo = msBankRepo;
            _msAccountRepo = msAccountRepo;
            _contextAccounting = contextAccounting;
        }

        public void CancelUnitSave(CancelUnitInputDto input)
        {
            var checkBooking = (from bh in _trBookingHeaderRepo.GetAll()
                                where bh.Id == input.bookingHeaderID && bh.cancelDate == null
                                select bh).FirstOrDefault();

            if (checkBooking != null)
            {
                #region insert booking cancel
                var dataInsertBookingCancel = new CreateTrBookingCancelInputDto
                {
                    reasonID = input.reasonID,
                    bookingHeaderID = input.bookingHeaderID,
                    cancelDate = DateTime.Now,
                    entityID = 1,
                    lostAmount = input.lostAmount,
                    refundAmount = input.refundAmount,
                    newBookCode = "-",
                    remarks = input.remarks == null ? "-" : input.remarks,
                };

                CreateTrBookingCancel(dataInsertBookingCancel);
                #endregion

                #region update booking header and booking header history

                var getDataBookingHeader = (from bh in _trBookingHeaderRepo.GetAll()
                                            where bh.Id == input.bookingHeaderID
                                            select bh
                                            ).FirstOrDefault();

                var updateBookingHeader = getDataBookingHeader.MapTo<TR_BookingHeader>();
                updateBookingHeader.cancelDate = DateTime.Now;
                _trBookingHeaderRepo.Update(updateBookingHeader);

                //history
                var checkHistory = (from A in _trBookingHeaderHistoryRepo.GetAll()
                                    orderby A.Id descending
                                    where A.bookCode == input.bookCode
                                    select A).FirstOrDefault();

                var dataToInsertHistory = new TR_BookingHeaderHistory
                {
                    bankName = getDataBookingHeader.bankName,
                    bankNo = getDataBookingHeader.bankNo,
                    bankRekeningPemilik = getDataBookingHeader.bankRekeningPemilik,
                    BFPayTypeCode = getDataBookingHeader.BFPayTypeCode,
                    bookCode = getDataBookingHeader.bookCode,
                    bookDate = getDataBookingHeader.bookDate,
                    discBFCalcType = getDataBookingHeader.discBFCalcType,
                    cancelDate = DateTime.Now,
                    DPCalcType = getDataBookingHeader.DPCalcType,
                    entityID = getDataBookingHeader.entityID,
                    eventID = getDataBookingHeader.eventID,
                    facadeID = getDataBookingHeader.facadeID,
                    SADStatusID = getDataBookingHeader.SADStatusID,
                    scmCode = getDataBookingHeader.scmCode,
                    shopBusinessID = getDataBookingHeader.shopBusinessID,
                    isPenaltyStop = getDataBookingHeader.isPenaltyStop,
                    promotionID = getDataBookingHeader.promotionID,
                    isSK = getDataBookingHeader.isSK,
                    isSMS = getDataBookingHeader.isSMS,
                    KPRBankCode = getDataBookingHeader.KPRBankCode,
                    memberCode = getDataBookingHeader.memberCode,
                    sumberDanaID = getDataBookingHeader.sumberDanaID,
                    memberName = getDataBookingHeader.memberName,
                    nomorRekeningPemilik = getDataBookingHeader.nomorRekeningPemilik,
                    PPJBDue = getDataBookingHeader.PPJBDue,
                    psCode = getDataBookingHeader.psCode,
                    netPriceComm = getDataBookingHeader.netPriceComm,
                    NUP = getDataBookingHeader.NUP,
                    remarks = input.remarks == null ? "-" : input.remarks,
                    termID = getDataBookingHeader.termID,
                    transID = getDataBookingHeader.transID,
                    termRemarks = getDataBookingHeader.termRemarks,
                    tujuanTransaksiID = getDataBookingHeader.tujuanTransaksiID,
                    unitID = getDataBookingHeader.unitID,
                    historyNo = checkHistory == null ? (short)0 : (short)(checkHistory.historyNo + 1)
                };

                _trBookingHeaderHistoryRepo.Insert(dataToInsertHistory);
                #endregion

                #region payment
                var getDataPaymentHeaderExist = (from ph in _trPaymentHeaderRepo.GetAll()
                                                 where ph.bookingHeaderID == input.bookingHeaderID
                                                 group ph by new
                                                 {
                                                     ph.accountID,
                                                     ph.combineCode
                                                 } into G
                                                 select new
                                                 {
                                                     G.Key.accountID,
                                                     G.Key.combineCode,
                                                     CreationTime = G.Max(x => x.CreationTime)
                                                 }).OrderByDescending(x => x.CreationTime).ToList();

                if (getDataPaymentHeaderExist.Any())
                {
                    decimal totalRefundLost = input.refundAmount + input.lostAmount;

                    var lastSchedNo = 1;

                    foreach (var paymentHeaderExist in getDataPaymentHeaderExist)
                    {
                        List<RefundLostListDto> listRefundLost = new List<RefundLostListDto>();

                        var refundData = new RefundLostListDto
                        {
                            payNo = 1,
                            pct = totalRefundLost == 0 ? 0 : input.refundAmount / totalRefundLost
                        };

                        listRefundLost.Add(refundData);

                        var lostData = new RefundLostListDto
                        {
                            payNo = 2,
                            pct = totalRefundLost == 0 ? 0 : input.lostAmount / totalRefundLost
                        };

                        listRefundLost.Add(lostData);

                        listRefundLost.OrderBy(x => x.payNo);

                        #region paymentrefund

                        int payForID = _lkPayForRepo.GetAll().Where(x => x.payForCode == "PMT").Select(x => x.Id).FirstOrDefault();

                        var accCode = (from A in _msAccountRepo.GetAll() where A.Id == paymentHeaderExist.accountID select A.accCode).FirstOrDefault();
                        var coCode = (from A in _msAccountRepo.GetAll() where A.Id == paymentHeaderExist.accountID select A.devCode).FirstOrDefault();

                        //alloc
                        var getDataAllocExist = (from pa in _trPaymentDetailAllocRepo.GetAll()
                                                 join pd in _trPaymentDetailRepo.GetAll() on pa.paymentDetailID equals pd.Id
                                                 join ph in _trPaymentHeaderRepo.GetAll() on pd.paymentHeaderID equals ph.Id
                                                 join bh in _trBookingHeaderRepo.GetAll() on ph.bookingHeaderID equals bh.Id
                                                 join pf in _lkPayForRepo.GetAll() on ph.payForID equals pf.Id
                                                 where ph.bookingHeaderID == input.bookingHeaderID && ph.accountID == paymentHeaderExist.accountID && pa.netAmt != 0
                                                 && pf.payForCode != "OTP"
                                                 group pa by new
                                                 {
                                                     pa.schedNo
                                                 } into G
                                                 //orderby pa.schedNo
                                                 select new
                                                 {
                                                     netAmt = G.Sum(x => x.netAmt),
                                                     vatAmt = G.Sum(x => x.vatAmt),
                                                     totalAlloc = G.Sum(x => x.netAmt) + G.Sum(x => x.vatAmt),
                                                     G.Key.schedNo
                                                 }
                                                     ).OrderBy(x => x.schedNo).ToList();


                        decimal totalVatPerDetail = getDataAllocExist.Sum(x => x.vatAmt);
                        decimal totalNetPerDetail = getDataAllocExist.Sum(x => x.netAmt);
                        decimal totalAllocPerDetail = getDataAllocExist.Sum(x => x.totalAlloc);

                        foreach (var itemRefundLost in listRefundLost)
                        {
                            var inputGenerateTransNo = new GenerateTransNoInputDto
                            {
                                accID = paymentHeaderExist.accountID,
                                entityID = 1
                            };

                            var transNo = _iPSASScheduleAppService.GenerateTransNo(inputGenerateTransNo);

                            //insert payment header
                            var dataInputPaymentHeader = new CreatePaymentHeaderInputDto
                            {
                                entityID = 1,
                                accountID = paymentHeaderExist.accountID,
                                bookingHeaderID = input.bookingHeaderID,
                                clearDate = DateTime.Now,
                                combineCode = paymentHeaderExist.combineCode, //wait
                                ket = input.remarks == null ? "-" : input.remarks,
                                payForID = payForID,
                                paymentDate = DateTime.Now,
                                transNo = transNo.GetValue("transNo").ToString(),
                                controlNo = transNo.GetValue("transNo").ToString(),
                                isSms = false,
                                hadMail = false
                            };

                            int paymentHeaderId = _iInputPaymentAppService.CreateTrPaymentHeader(dataInputPaymentHeader);

                            var getBankName = (from bh in _trBookingHeaderRepo.GetAll()
                                               join b in _msBankRepo.GetAll() on bh.KPRBankCode equals b.bankCode
                                               select b.bankName).FirstOrDefault();
                            var dataInsertPaymentDetail = new CreatePaymentDetailInputDto
                            {
                                bankName = getBankName == null ? "-" : getBankName,
                                chequeNo = "0",
                                dueDate = DateTime.Now,
                                entityID = 1,
                                ket = input.remarks == null ? "-" : input.remarks,
                                othersTypeCode = itemRefundLost.payNo == 1 ? "AD4" : "AD7",
                                paymentHeaderID = paymentHeaderId,
                                payNo = itemRefundLost.payNo,
                                payTypeID = _lkPayTypeRepo.GetAll().Where(x => x.payTypeCode == "ADJ").Select(x => x.Id).FirstOrDefault(),
                                status = "C"
                            };

                            int paymentDetailId = _iInputPaymentAppService.CreateTrPaymentDetail(dataInsertPaymentDetail);

                            decimal sisaNominal = totalRefundLost - (itemRefundLost.pct * totalAllocPerDetail);

                            bool isLooping = true;
                            foreach (var paymentAllocExist in getDataAllocExist)
                            {
                                if (sisaNominal > 0 && isLooping && lastSchedNo <= paymentAllocExist.schedNo)
                                {
                                    sisaNominal = sisaNominal - paymentAllocExist.totalAlloc;
                                    var dataInputDetailAlloc = new CreatePaymentDetailAllocInputDto
                                    {
                                        entityID = 1,
                                        netAmt = paymentAllocExist.netAmt * -1,
                                        vatAmt = paymentAllocExist.vatAmt * -1,
                                        paymentDetailID = paymentDetailId,
                                        schedNo = paymentAllocExist.schedNo
                                    };
                                    _iInputPaymentAppService.CreateTrPaymentDetailAlloc(dataInputDetailAlloc);

                                }
                                else if (sisaNominal < 0 && isLooping && lastSchedNo <= paymentAllocExist.schedNo)
                                {
                                    //sisaNominal = sisaNominal - paymentAllocExist.totalAlloc;
                                    var dataInputDetailAlloc = new CreatePaymentDetailAllocInputDto
                                    {
                                        entityID = 1,
                                        netAmt = paymentAllocExist.netAmt,
                                        vatAmt = paymentAllocExist.vatAmt,
                                        paymentDetailID = paymentDetailId,
                                        schedNo = paymentAllocExist.schedNo
                                    };
                                    _iInputPaymentAppService.CreateTrPaymentDetailAlloc(dataInputDetailAlloc);

                                }

                                if (sisaNominal == 0) isLooping = false;

                                lastSchedNo = paymentAllocExist.schedNo + 1;
                            }
                            #endregion

                            #region accountrefund
                            //insert payment detail journal

                            var dataInputJournalCode = new GenerateJurnalInputDto
                            {
                                accCode = accCode,
                                bookCode = input.bookCode,
                                coCode = coCode,
                                transNo = dataInputPaymentHeader.transNo,
                                totalPaymentDetail = 1
                                //payNo = itemRefundLost.payNo
                            };

                            var journalCode = _iInputPaymentAppService.GenerateJurnalCode(dataInputJournalCode);

                            var dataToInsertTRPaymentDetailJournal = new CreateAccountingTrPaymentDetailJournalInputDto
                            {
                                accCode = accCode,
                                bookCode = input.bookCode,
                                entityCode = "1",
                                payNo = itemRefundLost.payNo,
                                transNo = dataInputPaymentHeader.transNo,
                                journalCode = journalCode.GetValue("journalCode").ToString()
                            };

                            _iInputPaymentAppService.CreateAccountingTrPaymentDetailJournal(dataToInsertTRPaymentDetailJournal);

                            var getMsJournal = (from a in _contextAccounting.MS_JournalType.ToList()
                                                where a.journalTypeCode == "PMT.ADJ.AD4"
                                                select new
                                                {
                                                    a.COACodeFIN,
                                                    a.amtTypeCode,
                                                    a.ACCAlloc
                                                }).ToList();
                            if (getMsJournal.Any())
                            {
                                foreach (var dataJournal in getMsJournal)
                                {
                                    decimal debit = 0;
                                    decimal kredit = 0;

                                    if (dataJournal.ACCAlloc * (totalAllocPerDetail * -1) < 0)
                                    {
                                        if (dataJournal.amtTypeCode == "1")
                                        {
                                            debit = 0;
                                            kredit = totalAllocPerDetail;
                                        }

                                        else if (dataJournal.amtTypeCode == "2")
                                        {
                                            debit = 0;
                                            kredit = totalNetPerDetail;
                                        }

                                        else if (dataJournal.amtTypeCode == "3")
                                        {
                                            debit = 0;
                                            kredit = totalVatPerDetail;
                                        }
                                    }
                                    else
                                    {
                                        if (dataJournal.amtTypeCode == "1")
                                        {
                                            debit = totalAllocPerDetail;
                                            kredit = 0;
                                        }

                                        if (dataJournal.amtTypeCode == "2")
                                        {
                                            debit = totalNetPerDetail;
                                            kredit = 0;
                                        }

                                        if (dataJournal.amtTypeCode == "3")
                                        {
                                            debit = totalVatPerDetail;
                                            kredit = 0;
                                        }
                                    }

                                    var dataToInsertTrJournal = new CreateTrJournalInputDto
                                    {
                                        COACodeAcc = "-",
                                        COACodeFIN = dataJournal.COACodeFIN,
                                        entityCode = "1",
                                        journalCode = journalCode.GetValue("journalCode").ToString(),
                                        journalDate = DateTime.Now,
                                        debit = debit,
                                        kredit = kredit,
                                        remarks = "-",

                                    };

                                    _iInputPaymentAppService.CreateTrJournal(dataToInsertTrJournal);
                                }
                            }
                            #endregion
                        }

                    }
                }


                #endregion

                #region unit

                var getUnitForUpdate = (from u in _msUnitRepo.GetAll()
                                        where u.Id == getDataBookingHeader.unitID
                                        select u).FirstOrDefault();

                var updateMsUnit = getUnitForUpdate.MapTo<MS_Unit>();

                updateMsUnit.unitStatusID = (from us in _lkUnitStatusRepo.GetAll() where us.unitStatusCode == "A" select us.Id).FirstOrDefault();
                if (input.reasonCode == "7")
                {
                    updateMsUnit.unitStatusID = (from us in _lkUnitStatusRepo.GetAll() where us.unitStatusCode == "P" select us.Id).FirstOrDefault();
                }

                _msUnitRepo.Update(updateMsUnit);

                #endregion
                #region komisi

                var unitCode = (from u in _msUnitRepo.GetAll()
                                join uc in _msUnitCodeRepo.GetAll() on u.unitCodeID equals uc.Id
                                where u.Id == getDataBookingHeader.unitID
                                select uc.unitCode).FirstOrDefault();

                var getDataKomisi = (from su in _contextNew.TR_SoldUnit.ToList()
                                     where su.roadCode == unitCode && su.unitNo == getUnitForUpdate.unitNo
                                     orderby su.bookDate descending
                                     select su).FirstOrDefault();

                if (getDataKomisi != null)
                {
                    var updateKomisi = getDataKomisi.MapTo<TR_SoldUnit>();
                    updateKomisi.cancelDate = DateTime.Now;
                    updateKomisi.Remarks = "# Auto Cancel by PSAS Cancel Unit";

                    _contextNew.TR_SoldUnit.Update(getDataKomisi);
                }

                #endregion

                _contextNew.SaveChanges();
                _contextAccounting.SaveChanges();
            }
            else
            {
                throw new UserFriendlyException("This Unit Already Cancelled!");
            }
        }

        public void CreateTrBookingCancel(CreateTrBookingCancelInputDto input)
        {
            Logger.Info("CreateTrBookingCancel() - Started.");

            var dataToInsert = new TR_BookingCancel
            {
                reasonID = input.reasonID,
                bookingHeaderID = input.bookingHeaderID,
                cancelDate = input.cancelDate,
                entityID = input.entityID,
                lostAmount = input.lostAmount,
                refundAmount = input.refundAmount,
                newBookCode = input.newBookCode,
                remarks = input.remarks
            };

            try
            {
                _trBookingCancelRepo.Insert(dataToInsert);
            }
            catch (DataException ex)
            {
                Logger.ErrorFormat("CreateTrBookingCancel() - ERROR DataException. Result = {0}", ex.Message);
                Logger.Info("CreateTrBookingCancel() - Finished.");
                throw new UserFriendlyException("Db Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("CreateTrBookingCancel() - ERROR Exception. Result = {0}", ex.Message);
                Logger.Info("CreateTrBookingCancel() - Finished.");
                throw new UserFriendlyException("Error: " + ex.Message);
            }

        }

        public GetDataCancelUnitListDto GetDataCancelUnit(GetPSASParamsDto input)
        {
            var bookingHeaderId = _iPriceAppService.GetParameter(input).bookingHeaderID;

            var getDataUnit = (from bh in _trBookingHeaderRepo.GetAll()
                               join u in _msUnitRepo.GetAll() on bh.unitID equals u.Id
                               join uc in _msUnitCodeRepo.GetAll() on u.unitCodeID equals uc.Id
                               join p in _contextPers.PERSONAL.ToList() on bh.psCode equals p.psCode into l1
                               from p in l1.DefaultIfEmpty()
                               where bh.Id == bookingHeaderId
                               select new
                               {
                                   bookingHeaderID = bh.Id,
                                   bh.bookCode,
                                   bh.cancelDate,
                                   u.unitNo,
                                   uc.unitCode,
                                   psCode = p == null ? null : p.psCode,
                                   name = p == null ? null : p.name
                               }).FirstOrDefault();

            var getDataPayment = (from pda in _trPaymentDetailAllocRepo.GetAll()
                                  join pd in _trPaymentDetailRepo.GetAll() on pda.paymentDetailID equals pd.Id
                                  join ph in _trPaymentHeaderRepo.GetAll() on pd.paymentHeaderID equals ph.Id
                                  join pf in _lkPayForRepo.GetAll() on ph.payForID equals pf.Id
                                  where ph.bookingHeaderID == bookingHeaderId && pf.payForCode != "PEN" && pf.payForCode != "OTP"
                                  group pda by new
                                  {
                                      ph.bookingHeaderID
                                  } into G
                                  select new
                                  {
                                      netAmountPayment = G.Sum(x => x.netAmt),
                                      vatAmountPayment = G.Sum(x => x.vatAmt),
                                      totalAmountPayment = G.Sum(x => x.netAmt) + G.Sum(x => x.vatAmt)
                                  }).FirstOrDefault();

            var getDataPenalty = (from pda in _trPaymentDetailAllocRepo.GetAll()
                                  join pd in _trPaymentDetailRepo.GetAll() on pda.paymentDetailID equals pd.Id
                                  join ph in _trPaymentHeaderRepo.GetAll() on pd.paymentHeaderID equals ph.Id
                                  join pf in _lkPayForRepo.GetAll() on ph.payForID equals pf.Id
                                  where ph.bookingHeaderID == bookingHeaderId && pf.payForCode == "PEN"
                                  group pda by new
                                  {
                                      ph.bookingHeaderID
                                  } into G
                                  select new
                                  {
                                      netAmountPayment = G.Sum(x => x.netAmt),
                                      vatAmountPayment = G.Sum(x => x.vatAmt),
                                      totalAmountPayment = G.Sum(x => x.netAmt) + G.Sum(x => x.vatAmt)
                                  }).FirstOrDefault();

            var getDataAdjBankCharge = (from pda in _trPaymentDetailAllocRepo.GetAll()
                                        join pd in _trPaymentDetailRepo.GetAll() on pda.paymentDetailID equals pd.Id
                                        join ph in _trPaymentHeaderRepo.GetAll() on pd.paymentHeaderID equals ph.Id
                                        join pf in _lkPayForRepo.GetAll() on ph.payForID equals pf.Id
                                        where ph.bookingHeaderID == bookingHeaderId && pd.othersTypeCode == "AD1"
                                        group pda by new
                                        {
                                            ph.bookingHeaderID
                                        } into G
                                        select new
                                        {
                                            netAmountPayment = G.Sum(x => x.netAmt),
                                            vatAmountPayment = G.Sum(x => x.vatAmt),
                                            totalAmountPayment = G.Sum(x => x.netAmt) + G.Sum(x => x.vatAmt)
                                        }).FirstOrDefault();

            var dataResult = new GetDataCancelUnitListDto
            {
                bookingHeaderID = getDataUnit == null ? 0 : getDataUnit.bookingHeaderID,
                bookCode = getDataUnit == null ? null : getDataUnit.bookCode,
                unitCode = getDataUnit == null ? null : getDataUnit.unitCode,
                unitNo = getDataUnit == null ? null : getDataUnit.unitNo,
                customerName = getDataUnit == null ? null : getDataUnit.name,
                psCode = getDataUnit == null ? null : getDataUnit.psCode,
                totalPaymentAmt = getDataPayment == null ? 0 : getDataPayment.netAmountPayment,
                totalPaymentVat = getDataPayment == null ? 0 : getDataPayment.vatAmountPayment,
                totalPayment = getDataPayment == null ? 0 : getDataPayment.totalAmountPayment,
                totalPenalty = getDataPenalty == null ? 0 : getDataPenalty.totalAmountPayment,
                paymentAdjBankCharge = getDataAdjBankCharge == null ? 0 : getDataAdjBankCharge.totalAmountPayment,
                cancelDate = getDataUnit.cancelDate
            };

            return dataResult;
        }

        public List<GetLkReasonDropdownListDto> GetLkReasonDropdown()
        {
            var getDataDropdown = (from A in _lkReasonRepo.GetAll()
                                   where A.isActive == true
                                   orderby A.reasonDesc
                                   select new GetLkReasonDropdownListDto
                                   {
                                       reasonID = A.Id,
                                       reasonCode = A.reasonCode,
                                       reasonDesc = A.reasonDesc
                                   }).ToList();

            return getDataDropdown;
        }
    }
}
