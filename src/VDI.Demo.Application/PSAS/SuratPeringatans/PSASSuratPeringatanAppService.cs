﻿using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.PSAS.Dto;
using VDI.Demo.PSAS.Price;
using VDI.Demo.PSAS.SuratPeringatans.Dto;
using System.Linq;
using System.Net.Mail;
using Visionet_Backend_NetCore.Komunikasi;
using Abp.UI;
using Abp.Net.Mail;
using Newtonsoft.Json.Linq;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using VDI.Demo.DataExporting.Pdf.SP;
using VDI.Demo.PropertySystemDB.LippoMaster;
using VDI.Demo.Payment.InputPayment.Dto;
using VDI.Demo.Payment.InputPayment;
using System.Data;
using Abp.Domain.Repositories;
using Abp.AutoMapper;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Http;
using Abp.Authorization;
using VDI.Demo.Authorization;

namespace VDI.Demo.PSAS.SuratPeringatans
{
    public class PSASSuratPeringatanAppService : DemoAppServiceBase, IPSASSuratPeringatanAppService
    {
        private readonly PropertySystemDbContext _contextPropertySystem;
        private readonly PersonalsNewDbContext _contextPersonals;
        private readonly DemoDbContext _contextEngine3;
        private readonly IPSASPriceAppService _iPriceAppService;
        private readonly IEmailSender _emailSender;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly ISuratPeringatanGeneratePdfExporter _generatePdfExporter;
        private readonly IInputPaymentAppService _iInputPaymentAppService;
        private readonly IRepository<TR_ReminderLetter> _trReminderLetterRepo;
        private static IHttpContextAccessor _HttpContextAccessor;

        public PSASSuratPeringatanAppService(
            PropertySystemDbContext contextPropertySystem,
            PersonalsNewDbContext contextPersonals,
            DemoDbContext contextEngine3,
            IPSASPriceAppService iPriceAppService,
            IEmailSender emailSender,
            IHostingEnvironment hostingEnvironment,
            ISuratPeringatanGeneratePdfExporter generatePdfExporter,
            IInputPaymentAppService iInputPaymentAppService,
            IRepository<TR_ReminderLetter> trReminderLetterRepo,
            IHttpContextAccessor httpContextAccessor
            )
        {
            _contextPropertySystem = contextPropertySystem;
            _contextPersonals = contextPersonals;
            _contextEngine3 = contextEngine3;
            _iPriceAppService = iPriceAppService;
            _emailSender = emailSender;
            _hostingEnvironment = hostingEnvironment;
            _generatePdfExporter = generatePdfExporter;
            _iInputPaymentAppService = iInputPaymentAppService;
            _trReminderLetterRepo = trReminderLetterRepo;
            _HttpContextAccessor = httpContextAccessor;
        }

        public void CreateSuratPeringatanPenaltyScheduler()
        {
            Logger.InfoFormat("CreateSuratPeringatanPenaltyScheduler() Started.");

            Logger.DebugFormat("CreateSuratPeringatanPenaltyScheduler() - Start get data TE_PenaltySchedule. {0} ", Environment.NewLine);
            var getDataPenaltySchedule = (from a in _contextPropertySystem.TR_PenaltySchedule
                                          join b in _contextPropertySystem.TR_BookingHeader on a.bookingHeaderID equals b.Id
                                          join c in _contextPropertySystem.TR_BookingDetail on a.bookingHeaderID equals c.bookingHeaderID
                                          join d in _contextPropertySystem.TR_BookingDetailSchedule on new { bookingDetailID = c.Id, schedNo = (short)a.ScheduleTerm } equals new { bookingDetailID = d.bookingDetailID, schedNo = d.schedNo }
                                          join e in _contextPropertySystem.MS_Unit on b.unitID equals e.Id
                                          join f in _contextPropertySystem.MS_Cluster on e.clusterID equals f.Id
                                          join g in _contextPropertySystem.MS_SPPeriod on f.Id equals g.clusterID
                                          join h in _contextPropertySystem.MS_Project on e.projectID equals h.Id
                                          join i in _contextPropertySystem.LK_LetterType on g.letterTypeID equals i.Id
                                          join j in _contextPropertySystem.MS_UnitCode on e.unitCodeID equals j.Id
                                          group d by new
                                          {
                                              b.bookCode,
                                              c.bookingHeaderID,
                                              SPPeriodeID = g.Id,
                                              g.clusterID,
                                              g.SPGenerateID,
                                              g.letterTypeID,
                                              d.dueDate,
                                              g.daysDue,
                                              a.ScheduleTerm,
                                              h.entityID,
                                              h.projectCode,
                                              projectID = h.Id,
                                              i.letterType,
                                              totAmt = a.ScheduleNetAmount,
                                              penAge = a.penaltyAging,
                                              a.penaltyAmount,
                                              g.kuasaDir1,
                                              g.kuasaDir2,
                                              g.templateUrl,
                                              e.unitNo,
                                              j.unitCode,
                                              j.unitName,
                                              h.projectName,
                                              b.psCode,
                                              c.coCode
                                          } into G
                                          select new
                                          {
                                              G.Key.bookCode,
                                              G.Key.bookingHeaderID,
                                              G.Key.SPPeriodeID,
                                              G.Key.clusterID,
                                              G.Key.SPGenerateID,
                                              G.Key.letterTypeID,
                                              G.Key.dueDate,
                                              G.Key.daysDue,
                                              G.Key.ScheduleTerm,
                                              G.Key.entityID,
                                              G.Key.projectCode,
                                              G.Key.projectID,
                                              G.Key.letterType,
                                              G.Key.totAmt,
                                              payedAmt = G.Sum(x => x.netAmt) - G.Sum(x => x.netOut),
                                              netAmt = G.Sum(x => x.netOut),
                                              G.Key.penAge,
                                              G.Key.penaltyAmount,
                                              G.Key.kuasaDir1,
                                              G.Key.kuasaDir2,
                                              G.Key.templateUrl,
                                              G.Key.unitNo,
                                              G.Key.unitCode,
                                              G.Key.unitName,
                                              G.Key.projectName,
                                              G.Key.psCode,
                                              G.Key.coCode
                                          })
                                          .OrderBy(x => x.bookingHeaderID).ThenBy(n => n.SPGenerateID)
                                          .ToList();
            Logger.DebugFormat("CreateSuratPeringatanPenaltyScheduler() - Result = {0}", getDataPenaltySchedule.Count());

            int loop = 0;
            foreach (var data in getDataPenaltySchedule)
            {
                Logger.DebugFormat("CreateSuratPeringatanPenaltyScheduler() - Start get Payment Schedule. Parameters sent: {0} " +
                "   bookingHeaderID = {1}{0}" +
                "   ScheduleTerm    = {2}{0}"
                , Environment.NewLine, data.bookingHeaderID, data.ScheduleTerm);

                var checkScheduleStatus = (from x in _contextPropertySystem.TR_BookingDetailSchedule
                                           join y in _contextPropertySystem.TR_BookingDetail on x.bookingDetailID equals y.Id
                                           where x.schedNo == data.ScheduleTerm && y.bookingHeaderID == data.bookingHeaderID
                                           group x by new
                                           {
                                               x.schedNo,
                                               y.bookingHeaderID
                                           } into G
                                           select new
                                           {
                                               totalOut = G.Sum(x => x.netOut) + G.Sum(x => x.vatOut)
                                           }).FirstOrDefault();
                Logger.DebugFormat("CreateSuratPeringatanPenaltyScheduler() - End get Payment Schedule. Result = {0}", checkScheduleStatus);

                Logger.DebugFormat("CreateSuratPeringatanPenaltyScheduler() - Start checking existing TR_ReminderLetter. Parameters sent: {0} " +
                "   bookingHeaderID = {1}{0}" +
                "   SPPeriodID      = {2}{0}"
                , Environment.NewLine, data.bookingHeaderID, data.SPPeriodeID);

                var checkReminderLetter = (from x in _contextPropertySystem.TR_ReminderLetter
                                           where x.bookingHeaderID == data.bookingHeaderID && x.SPPeriodID == data.SPPeriodeID
                                           select x);

                Logger.DebugFormat("CreateSuratPeringatanPenaltyScheduler() - End checking existing TR_ReminderLetter. Result = {0}", checkReminderLetter);

                if (checkScheduleStatus != null)
                {
                    if (!checkReminderLetter.Any())
                    {
                        if (checkScheduleStatus.totalOut != 0)
                        {
                            Logger.DebugFormat("CreateSuratPeringatanPenaltyScheduler() - Start get EntityID from MS_Project. Parameters sent: {0} " +
                            "   projectID = {1}{0}"
                            , Environment.NewLine, data.projectID);

                            var getEntityProject = (from x in _contextPropertySystem.MS_Project
                                                    where x.Id == data.projectID
                                                    select x.entityID).FirstOrDefault();

                            Logger.DebugFormat("CreateSuratPeringatanPenaltyScheduler() - End get EntityID from MS_Project. Result -> EntityID = {0}", getEntityProject);

                            Logger.DebugFormat("CreateSuratPeringatanPenaltyScheduler() - Start get Officer And Position 1. Parameters sent: {0} " +
                            "   officerID = {1}{0}"
                            , Environment.NewLine, data.kuasaDir1);

                            var getKuasaDirPosition1 = (from x in _contextPropertySystem.MS_Officer
                                                        join y in _contextPropertySystem.MS_Position on x.positionID equals y.Id
                                                        where x.Id == data.kuasaDir1
                                                        select new
                                                        {
                                                            sadOfficer = x.officerName,
                                                            sadPosition = y.positionName
                                                        }).FirstOrDefault();

                            Logger.DebugFormat("CreateSuratPeringatanPenaltyScheduler() - End get Officer And Position 1. Result = {0}", getKuasaDirPosition1);

                            Logger.DebugFormat("CreateSuratPeringatanPenaltyScheduler() - Start get Officer And Position 2. Parameters sent: {0} " +
                            "   officerID = {1}{0}"
                            , Environment.NewLine, data.kuasaDir2);

                            var getKuasaDirPosition2 = (from x in _contextPropertySystem.MS_Officer
                                                        join y in _contextPropertySystem.MS_Position on x.positionID equals y.Id
                                                        where x.Id == data.kuasaDir2
                                                        select new
                                                        {
                                                            sadOfficer = x.officerName,
                                                            sadPosition = y.positionName
                                                        }).FirstOrDefault();

                            //Logger.DebugFormat("CreateSuratPeringatanPenaltyScheduler() - Start get RunningNumber TR_ReminderLetter. Parameters sent: {0} " +
                            //"   projectCode = {1}{0}" +
                            //"   Month       = {2}{0}" +
                            //"   Year        = {3}{0}"
                            //, Environment.NewLine, data.projectCode, DateTime.Now.Month, DateTime.Now.Year);

                            //var getCompanyBankBranch = (from x in _contextPropertySystem.MS_C
                            //                            join y in _contextPropertySystem.MS_Position on x.positionID equals y.Id
                            //                            where x.Id == data.kuasaDir2
                            //                            select new
                            //                            {
                            //                                sadOfficer = x.officerName,
                            //                                sadPosition = y.positionName
                            //                            }).FirstOrDefault();

                            //Logger.DebugFormat("CreateSuratPeringatanPenaltyScheduler() - End get CoCode & BankBranch. Result = {0}", getKuasaDirPosition2);

                            Logger.DebugFormat("CreateSuratPeringatanPenaltyScheduler() - Start get RunningNumber TR_ReminderLetter. Parameters sent: {0} " +
                            "   projectCode = {1}{0}" +
                            "   Month       = {2}{0}" +
                            "   Year        = {3}{0}"
                            , Environment.NewLine, data.projectCode, DateTime.Now.Month, DateTime.Now.Year);

                            var getRunningNumber = (from x in _contextPropertySystem.TR_ReminderLetter
                                                    where x.letterNo.Split(new[] { '/' })[0].Contains(data.projectCode) &&
                                                    x.letterNo.Split(new[] { '/' })[3].Contains(Convert.ToString(DateTime.Now.Month)) &&
                                                    x.letterNo.Split(new[] { '/' })[4].Contains(Convert.ToString(DateTime.Now.Year))
                                                    orderby x.Id descending
                                                    select x.letterNo).FirstOrDefault();

                            Logger.DebugFormat("CreateSuratPeringatanPenaltyScheduler() - End get RunningNumber TR_ReminderLetter. Result -> letterNo = {0}", getRunningNumber);

                            string runningNumber = getRunningNumber == null ? 1.ToString("D8") : (Convert.ToInt32(getRunningNumber.Split("/")[2]) + 1).ToString("D8");

                            var dataReminderLetter = new TR_ReminderLetter
                            {
                                entityID = getEntityProject,
                                bookingHeaderID = data.bookingHeaderID,
                                letterNo = data.projectCode + "/" + data.letterType + "/" + runningNumber + "/" + DateTime.Now.Month + "/" + DateTime.Now.Year,
                                letterDate = DateTime.Now,
                                SPPeriodID = data.SPPeriodeID,
                                dueDate = data.dueDate,
                                totAmt = data.totAmt,
                                payedAmt = data.payedAmt,
                                outAmt = data.netAmt,
                                overDue = 1, // konfirmasi ulang
                                penAge = data.penAge,
                                penAmt = data.penaltyAmount,
                                remarks = "-", // kosongi dulu
                                letterStatusID = 4, // Unknown Status
                                mailDate = null, // kosongin dulu
                                receiveDate = null, // kosongin dulu
                                clearDate = DateTime.Now, // bikin today dulu
                                printDate = null,
                                bankBranchCode = "1", //tidak tahu
                                coCode = data.coCode,
                                bankRekNo = "1", //tidak tahu
                                sadOfficer1 = getKuasaDirPosition1.sadOfficer,
                                sadPosition1 = getKuasaDirPosition1.sadPosition,
                                sadOfficer2 = getKuasaDirPosition2.sadOfficer,
                                sadPosition2 = getKuasaDirPosition2.sadPosition
                            };

                            if (data.SPGenerateID == 1) // days due harcode
                            {
                                if ((DateTime.Now.Date - data.dueDate.Date).TotalDays == data.daysDue)
                                {
                                    Logger.DebugFormat("CreateSuratPeringatanPenaltyScheduler() - Start insert TR_PenaltySchedule. Parameters sent:{0}" +
                                    "	entityID	    = {1}{0}" +
                                    "   bookingHeaderID = {2}{0}" +
                                    "   letterNo        = {3}{0}" +
                                    "   letterDate      = {4}{0}" +
                                    "   SPPeriodID      = {5}{0}" +
                                    "   dueDate         = {6}{0}" +
                                    "   totAmt          = {7}{0}" +
                                    "   payedAmt        = {8}{0}" +
                                    "   outAmt          = {9}{0}" +
                                    "   overDue         = {10}{0}" +
                                    "   penAge          = {11}{0}" +
                                    "   penAmt          = {12}{0}" +
                                    "   remarks         = {13}{0}" +
                                    "   letterStatusID  = {14}{0}" +
                                    "   mailDate        = {15}{0}" +
                                    "   receiveDate     = {16}{0}" +
                                    "   clearDate       = {17}{0}" +
                                    "   printDate       = {18}{0}" +
                                    "   bankBranchCode  = {19}{0}" +
                                    "   coCode          = {20}{0}" +
                                    "   bankRekNo       = {21}{0}" +
                                    "   sadOfficer1     = {22}{0}" +
                                    "   sadPosition1    = {23}{0}" +
                                    "   sadOfficer2     = {24}{0}" +
                                    "   sadPosition2    = {25}{0}"
                                    , Environment.NewLine,
                                    dataReminderLetter.entityID, dataReminderLetter.bookingHeaderID, dataReminderLetter.letterNo,
                                    dataReminderLetter.letterDate, dataReminderLetter.SPPeriodID, dataReminderLetter.dueDate,
                                    dataReminderLetter.totAmt, dataReminderLetter.payedAmt, dataReminderLetter.outAmt,
                                    dataReminderLetter.overDue, dataReminderLetter.penAge, dataReminderLetter.penAmt,
                                    dataReminderLetter.remarks, dataReminderLetter.letterStatusID, dataReminderLetter.mailDate,
                                    dataReminderLetter.receiveDate, dataReminderLetter.clearDate, dataReminderLetter.printDate,
                                    dataReminderLetter.bankBranchCode, dataReminderLetter.coCode, dataReminderLetter.bankRekNo,
                                    dataReminderLetter.sadOfficer1, dataReminderLetter.sadPosition1, dataReminderLetter.sadOfficer2, dataReminderLetter.sadPosition2);

                                    try
                                    {
                                        Logger.DebugFormat("CreateSuratPeringatanPenaltyScheduler() - Start insert TR_ReminderLetter.");
                                        _trReminderLetterRepo.Insert(dataReminderLetter);
                                        Logger.DebugFormat("CreateSuratPeringatanPenaltyScheduler() - End insert TR_ReminderLetter.");
                                    }
                                    catch (DataException exDb)
                                    {
                                        Logger.ErrorFormat("CreateSuratPeringatanPenaltyScheduler() ERROR DbException. Result = {0}", exDb.Message);
                                    }
                                    // Handle all other exceptions.
                                    catch (Exception ex)
                                    {
                                        Logger.ErrorFormat("CreateSuratPeringatanPenaltyScheduler() ERROR Exception. Result = {0}", ex.Message);
                                    }
                                }
                            }
                            else
                            {
                                var getReminderLetter = (from a in _contextPropertySystem.TR_ReminderLetter
                                                         join b in _contextPropertySystem.MS_SPPeriod on a.SPPeriodID equals b.Id
                                                         join c in _contextPropertySystem.LK_SPGenerate on b.SPGenerateID equals c.Id
                                                         where a.bookingHeaderID == getDataPenaltySchedule[loop].bookingHeaderID && c.Id == getDataPenaltySchedule[loop].SPGenerateID
                                                         select a).FirstOrDefault();

                                if (getReminderLetter != null)
                                {
                                    if ((DateTime.Now.Date - getReminderLetter.letterDate.Date).TotalDays == data.daysDue)
                                    {
                                        Logger.DebugFormat("CreateSuratPeringatanPenaltyScheduler() - Start insert TR_PenaltySchedule. Parameters sent:{0}" +
                                        "	entityID	    = {1}{0}" +
                                        "   bookingHeaderID = {2}{0}" +
                                        "   letterNo        = {3}{0}" +
                                        "   letterDate      = {4}{0}" +
                                        "   SPPeriodID      = {5}{0}" +
                                        "   dueDate         = {6}{0}" +
                                        "   totAmt          = {7}{0}" +
                                        "   payedAmt        = {8}{0}" +
                                        "   outAmt          = {9}{0}" +
                                        "   overDue         = {10}{0}" +
                                        "   penAge          = {11}{0}" +
                                        "   penAmt          = {12}{0}" +
                                        "   remarks         = {13}{0}" +
                                        "   letterStatusID  = {14}{0}" +
                                        "   mailDate        = {15}{0}" +
                                        "   receiveDate     = {16}{0}" +
                                        "   clearDate       = {17}{0}" +
                                        "   printDate       = {18}{0}" +
                                        "   bankBranchCode  = {19}{0}" +
                                        "   coCode          = {20}{0}" +
                                        "   bankRekNo       = {21}{0}" +
                                        "   sadOfficer1     = {22}{0}" +
                                        "   sadPosition1    = {23}{0}" +
                                        "   sadOfficer2     = {24}{0}" +
                                        "   sadPosition2    = {25}{0}"
                                        , Environment.NewLine,
                                        dataReminderLetter.entityID, dataReminderLetter.bookingHeaderID, dataReminderLetter.letterNo,
                                        dataReminderLetter.letterDate, dataReminderLetter.SPPeriodID, dataReminderLetter.dueDate,
                                        dataReminderLetter.totAmt, dataReminderLetter.payedAmt, dataReminderLetter.outAmt,
                                        dataReminderLetter.overDue, dataReminderLetter.penAge, dataReminderLetter.penAmt,
                                        dataReminderLetter.remarks, dataReminderLetter.letterStatusID, dataReminderLetter.mailDate,
                                        dataReminderLetter.receiveDate, dataReminderLetter.clearDate, dataReminderLetter.printDate,
                                        dataReminderLetter.bankBranchCode, dataReminderLetter.coCode, dataReminderLetter.bankRekNo,
                                        dataReminderLetter.sadOfficer1, dataReminderLetter.sadPosition1, dataReminderLetter.sadOfficer2, dataReminderLetter.sadPosition2);

                                        try
                                        {
                                            Logger.DebugFormat("CreateSuratPeringatanPenaltyScheduler() - Start insert TR_ReminderLetter.");
                                            _trReminderLetterRepo.Insert(dataReminderLetter);
                                            Logger.DebugFormat("CreateSuratPeringatanPenaltyScheduler() - End insert TR_ReminderLetter.");
                                        }
                                        catch (DataException exDb)
                                        {
                                            Logger.ErrorFormat("CreateSuratPeringatanPenaltyScheduler() ERROR DbException. Result = {0}", exDb.Message);
                                        }
                                        // Handle all other exceptions.
                                        catch (Exception ex)
                                        {
                                            Logger.ErrorFormat("CreateSuratPeringatanPenaltyScheduler() ERROR Exception. Result = {0}", ex.Message);
                                        }
                                    }
                                }
                            }
                            loop++;
                        }
                    }
                    else
                    {
                        var getDataOverdue = checkReminderLetter.FirstOrDefault();

                        if (getDataOverdue.overDue != checkScheduleStatus.totalOut)
                        {
                            var dataToUpdate = getDataOverdue.MapTo<TR_ReminderLetter>();

                            dataToUpdate.overDue = checkScheduleStatus.totalOut;

                            Logger.DebugFormat("CreateSuratPeringatanPenaltyScheduler() - Start update TR_ReminderLetter. Parameters sent: {0} " +
                            "   bookingHeaderID = {1}{0}" +
                            "   SPPeriodID      = {2}{0}" +
                            "   Overdue         = {3}{0}"
                            , Environment.NewLine, data.bookingHeaderID, data.SPPeriodeID, checkScheduleStatus.totalOut);
                            _trReminderLetterRepo.Update(dataToUpdate);
                            Logger.DebugFormat("CreateSuratPeringatanPenaltyScheduler() - End update TR_ReminderLetter. Result = {0}", dataToUpdate);
                        }
                    }
                }
            }
            Logger.InfoFormat("CreateSuratPeringatanPenaltyScheduler() - Finished.");
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_PSAS_SuratPeringatan)]
        public GetDataSuratPeringatanPenaltyViewListDto GetDataSuratPeringatanPenaltyView(GetPSASParamsDto input)
        {
            var dataBookingId = _iPriceAppService.GetParameter(input);

            var getDataReminderLetter = (from x in _contextPropertySystem.TR_ReminderLetter
                                         join y in _contextPropertySystem.MS_SPPeriod on x.SPPeriodID equals y.Id
                                         join z in _contextPropertySystem.LK_LetterStatus on x.letterStatusID equals z.Id
                                         join w in _contextPropertySystem.LK_LetterType on y.letterTypeID equals w.Id
                                         where x.bookingHeaderID == dataBookingId.bookingHeaderID
                                         select new GetDataReminderLetterDto
                                         {
                                             generateDate = x.letterDate,
                                             letterType = w.letterType,
                                             status = z.letterStatusDesc,
                                             sentDate = x.mailDate,
                                             documentUrl = (y != null && y.templateUrl != null) ? (!y.templateUrl.Equals("-")) ? getAbsoluteUriWithoutTail() + GetURLWithoutHost(y.templateUrl) : null : null, //TODO link + ip host,
                                             documentName = y.templateName
                                         }).ToList();

            var getDataSuratPeringatan = (from a in _contextPropertySystem.TR_BookingHeader
                                          join b in _contextPropertySystem.MS_Unit on a.unitID equals b.Id
                                          join c in _contextPropertySystem.MS_Project on b.projectID equals c.Id
                                          join d in _contextPropertySystem.MS_Cluster on b.clusterID equals d.Id
                                          join e in _contextPropertySystem.MS_UnitCode on b.unitCodeID equals e.Id
                                          join f in _contextPersonals.PERSONAL.ToList() on a.psCode equals f.psCode
                                          where a.Id == dataBookingId.bookingHeaderID
                                          select new GetDataSuratPeringatanPenaltyViewListDto
                                          {
                                              projectID = b.projectID,
                                              project = c.projectName,
                                              clusterID = b.clusterID,
                                              cluster = d.clusterName,
                                              unitID = a.unitID,
                                              unitCode = e.unitCode,
                                              unitNo = b.unitNo,
                                              psCode = a.psCode,
                                              custName = f.name,
                                              dataReminderLetter = getDataReminderLetter
                                          }).FirstOrDefault();
            return getDataSuratPeringatan;
        }

        public void SendEmailSPScheduler()
        {
            Logger.Info("SendEmailSPScheduler() - Started.");

            var getDataReminderLetters = (from a in _contextPropertySystem.TR_ReminderLetter
                                          join b in _contextPropertySystem.TR_BookingHeader on a.bookingHeaderID equals b.Id
                                          join c in _contextPropertySystem.MS_Unit on b.unitID equals c.Id
                                          join d in _contextPropertySystem.MS_Project on c.projectID equals d.Id
                                          join e in _contextPropertySystem.MS_UnitCode on c.unitCodeID equals e.Id
                                          join f in _contextPropertySystem.MS_SPPeriod on a.SPPeriodID equals f.Id
                                          join g in _contextPropertySystem.LK_LetterType on f.letterTypeID equals g.Id
                                          where a.letterDate.Date == DateTime.Now.Date
                                          select new
                                          {
                                              reminderLetterID = a.Id,
                                              b.psCode,
                                              d.projectName,
                                              c.unitNo,
                                              e.unitCode,
                                              e.unitName,
                                              templateHtml = f.templateUrl,
                                              g.letterType
                                          }).ToList();

            foreach (var item in getDataReminderLetters)
            {
                var filePath = _hostingEnvironment.WebRootPath + @"\Assets\HtmlTemplates\SP\TemplateEmailSP.html";
                var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
                var webConfigApp = (JObject)appsettingsjson["App"];
                var emailDev = webConfigApp.Property("emailDev").Value.ToString();

                var urlHtmlSP = item.templateHtml.Replace("/", @"\");
                var htmlSP = _hostingEnvironment.WebRootPath + urlHtmlSP;
                string html = File.ReadAllText(htmlSP);

                var getDataPersonal = (from a in _contextPersonals.PERSONAL
                                       where a.psCode == item.psCode
                                       select a.name).FirstOrDefault();

                var dataSP = new SendEmailSPInputDto
                {
                    projectName = item.projectName,
                    unitCode = item.unitCode,
                    letterType = item.letterType,
                    unitNo = item.unitNo,
                    unitName = item.unitName,
                    name = getDataPersonal
                };

                var destinationPath = _hostingEnvironment.WebRootPath + @"\Assets\HtmlTemplates\SP";
                var filePathResult = _generatePdfExporter.SPGeneratePdf(html, dataSP, destinationPath, @"\Assets\HtmlTemplates\SP");

                var attachmentSP = _hostingEnvironment.WebRootPath + filePathResult;

                using (StreamReader reader = new StreamReader(filePath))
                {
                    html = reader.ReadToEnd();
                }

                string htmlContent = Setting_variabel.GetHTMLMappedValue(html, dataSP);
                var ketSubject = "";
                if (dataSP.letterType == "SP1") ketSubject = "Pemberitahuan Keterlambatan Pembayaran";
                else if (dataSP.letterType == "SP2") ketSubject = "Peringatan Keterlambatan Pembayaran";
                else if (dataSP.letterType == "SPP") ketSubject = "Peringatan Pemutusan Pesanan";
                else if (dataSP.letterType == "DUD") ketSubject = "Pemberitahuan Pemutusan Pesanan";

                try
                {
                    MailMessage mail = new MailMessage();
                    mail.From = new MailAddress("denykalpar@gmail.com");
                    mail.To.Add(emailDev);
                    mail.Subject = dataSP.letterType + ": " + ketSubject + " " + dataSP.unitCode + "/" + dataSP.unitNo;
                    mail.Body = htmlContent;
                    Attachment data = new Attachment(attachmentSP);
                    mail.Attachments.Add(data);
                    mail.IsBodyHtml = true;

                    Logger.DebugFormat("SendEmailSPScheduler() - email data. {0}" +
                        "to             = {1}{0}" +
                        "subject        = {2}{0}" +
                        "body           = {3}{0}" +
                        "att            = {4}{0}",
                        Environment.NewLine, mail.To.ToString(), mail.Subject, mail.Body, attachmentSP);

                    _emailSender.Send(mail);

                    Logger.Info("SendEmailSPScheduler() - Success.");

                    var getReminderLetter = (from a in _contextPropertySystem.TR_ReminderLetter
                                             where a.Id == item.reminderLetterID
                                             select a).FirstOrDefault();

                    var updateReminderLetter = getReminderLetter.MapTo<TR_ReminderLetter>();
                    updateReminderLetter.mailDate = DateTime.Now;
                    updateReminderLetter.letterStatusID = 3;

                    _contextPropertySystem.TR_ReminderLetter.Update(updateReminderLetter);

                    _contextPropertySystem.SaveChanges();
                }
                catch (Exception ex)
                {
                    Logger.Info("SendEmailSPScheduler() - Error. " + ex);
                }
            }

            Logger.Info("SendEmailSPScheduler() - Finished.");
        }

        #region helper function

        private void GetURLWithoutHost(string path, out string finalpath)
        {
            finalpath = path;
            try
            {
                Regex RegexObj = new Regex("[\\w\\W]*([\\/]Assets[\\w\\W\\s]*)");
                if (RegexObj.IsMatch(path))
                {
                    finalpath = RegexObj.Match(path).Groups[1].Value;
                }
            }
            catch (ArgumentException ex)
            {
                Logger.DebugFormat("test() - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Error : {0}", ex.Message);
            }
        }

        private string GetURLWithoutHost(string path)
        {
            string finalpath = path;
            try
            {
                Regex RegexObj = new Regex("[\\w\\W]*([\\/]Assets[\\w\\W\\s]*)");
                if (RegexObj.IsMatch(path))
                {
                    finalpath = RegexObj.Match(path).Groups[1].Value;
                }
            }
            catch (ArgumentException ex)
            {
                Logger.DebugFormat("test() - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Error : {0}", ex.Message);
            }
            return finalpath;
        }

        private string getAbsoluteUriWithoutTail()
        {
            var request = _HttpContextAccessor.HttpContext.Request;
            UriBuilder uriBuilder = new UriBuilder();
            uriBuilder.Scheme = request.Scheme;
            uriBuilder.Host = request.Host.ToString();
            var test = uriBuilder.ToString();
            var result = test.Replace("[", "").Replace("]", "");
            int position = result.LastIndexOf('/');
            if (position > -1)
                result = result.Substring(0, result.Length - 1);

            if (request.PathBase != null)
            {
                if (!string.IsNullOrWhiteSpace(request.PathBase.Value))
                {
                    result += request.PathBase.Value;
                }
            }
            return result;
        }

        #endregion
    }
}