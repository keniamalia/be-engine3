﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Visionet_Backend_NetCore.Komunikasi;

namespace Visionet_Backend_NetCore.Komunikasi
{
    #region kumpulan enum
    public enum TipeConsole
    {
        Error,
        Warning,
        Info,
        None,
        NewTrans
    }

    public enum TipeServer
    {
        Mvc,
        NonMvc
    }
    #endregion

    public static class Setting_variabel
    {
        public static bool enable_debug = true;
        public static bool enable_tcp_debug = true;

        public static ConsoleBayangan ConsoleBayangan;
        public static Komunikasi_TCPListener Komunikasi_TCPListener;

        public static Object GetPropValue(this Object obj, String name)
        {
            foreach (String part in name.Split('.'))
            {
                if (obj == null) { return null; }

                Type type = obj.GetType();
                PropertyInfo info = type.GetProperty(part);
                if (info == null) { return null; }

                obj = info.GetValue(obj, null);
            }
            return obj;
        }

        public static T GetPropValue<T>(this Object obj, String name)
        {
            Object retval = GetPropValue(obj, name);
            if (retval == null) { return default(T); }

            // throws InvalidCastException if types are incompatible
            return (T)retval;
        }

        public static string GetHTMLMappedValue(string htmlContent, Object data)
        {
            if (string.IsNullOrEmpty(htmlContent))
            {
                return "";
            }
            else
            {
                try
                {
                    string tmphtmlContent = htmlContent;
                    Regex RegexObj = new Regex(@"[\{][\{]([a-zA-Z0-9\.]*)[\}][\}]");
                    Match MatchResults = RegexObj.Match(htmlContent);
                    while (MatchResults.Success)
                    {
                        for (int i = 1; i < MatchResults.Groups.Count; i++)
                        {
                            Group GroupObj = MatchResults.Groups[i];
                            if (GroupObj.Success)
                            {
                                Debug.WriteLine("Group: " + GroupObj.Value);
                                try
                                {
                                    //SendConsole("Properti:"+ Setting_variabel.GetPropValue<int>(data, GroupObj.Value)); //ini jika butuh cast
                                    var val = (string.IsNullOrEmpty("" + GetPropValue(data, GroupObj.Value))) ? "(Belum ada data)" : GetPropValue(data, GroupObj.Value);
                                    Debug.WriteLine("Properti:" + val);

                                    //replace
                                    tmphtmlContent = tmphtmlContent.Replace("{{" + GroupObj.Value + "}}", "" + val);
                                }
                                catch (Exception e)
                                {
                                    Debug.WriteLine("" + e.Message);
                                }
                            }
                        }
                        MatchResults = MatchResults.NextMatch();
                    }
                    htmlContent = tmphtmlContent;
                }
                catch (ArgumentException ex)
                {
                }
            }

            return htmlContent;
        }

        public static string ToString<T>(this Nullable<T> nullable, string format) where T : struct
        {
            return String.Format("{0:" + format + "}", nullable.GetValueOrDefault());
        }

        public static string ToString<T>(this Nullable<T> nullable, string format, string defaultValue) where T : struct
        {
            if (nullable.HasValue)
            {
                return String.Format("{0:" + format + "}", nullable.Value);
            }

            return defaultValue;
        }
        
    }
}
