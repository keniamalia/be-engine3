﻿using Abp.Collections.Extensions;
using Abp.Extensions;
using Abp.UI;
using Castle.Core.Logging;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using VDI.Demo.Files.Dto;

namespace VDI.Demo.Files
{
    public class FilesHelper : IFilesHelper
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly ILogger _logger;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public FilesHelper(
            IHostingEnvironment hostingEnvironment, 
            ILogger logger,
            IHttpContextAccessor httpContextAccessor)
        {
            _hostingEnvironment = hostingEnvironment;
            _logger = logger;
            _httpContextAccessor = httpContextAccessor;
        }

        public string MoveFiles(string filename, string oldPath, string newPath)
        {
            var webRootPath = _hostingEnvironment.WebRootPath;
            _logger.InfoFormat("webRootPath: " + webRootPath);

            var oldImagePath = Path.Combine(webRootPath, oldPath, filename);
            _logger.InfoFormat("oldImagePath: " + oldImagePath);

            var newImagePath = Path.Combine(webRootPath, newPath, "m-" + filename);
            _logger.InfoFormat("newImagePath: " + newImagePath);

            var oldFolderPath = Path.Combine(webRootPath, oldPath);
            _logger.InfoFormat("oldFolderPath: " + oldFolderPath);

            var newFolderPath = Path.Combine(webRootPath, newPath);
            _logger.InfoFormat("newFolderPath: " + newFolderPath);

            var newImageUrl = getAbsoluteUri() + newPath + "m-" + filename;
            _logger.InfoFormat("newImageUrl: " + newImageUrl);

            _logger.InfoFormat("uploadFile() Started.");
            try
            {
                if (!Directory.Exists(oldFolderPath))
                {
                    Directory.CreateDirectory(oldFolderPath);
                }

                if (!Directory.Exists(newFolderPath))
                {
                    Directory.CreateDirectory(newFolderPath);
                }

                if (!File.Exists(oldImagePath))
                    throw new FileNotFoundException();

                if (File.Exists(newImagePath))
                    File.Delete(newImagePath);

                _logger.InfoFormat("uploadFile() - Start Move Image to: {0}", newFolderPath);
                var file = new FileInfo(oldImagePath);
                file.MoveTo(newImagePath);
                _logger.InfoFormat("uploadFile() - End Move Image");

                _logger.InfoFormat("uploadFile() - Finished.");
                return newImageUrl.Replace(@"\", "/");
            }
            catch (FileNotFoundException exFn)
            {
                _logger.DebugFormat("MoveFiles() - ERROR FileNotFoundException. Result = {0}", exFn.Message);
                throw new UserFriendlyException("File Not Found !", exFn.Message);
            }
        }

        public string MoveFilesBarcode(string filename, string oldPath, string newPath)
        {
            var webRootPath = _hostingEnvironment.WebRootPath;
            _logger.InfoFormat("webRootPath: " + webRootPath);

            var oldImagePath = Path.Combine(oldPath, filename);
            _logger.InfoFormat("oldImagePath: " + oldImagePath);

            var newImagePath = Path.Combine(webRootPath, newPath, filename);
            _logger.InfoFormat("newImagePath: " + newImagePath);

            var oldFolderPath = Path.Combine(oldPath);
            _logger.InfoFormat("oldFolderPath: " + oldFolderPath);

            var newFolderPath = Path.Combine(webRootPath, newPath);
            _logger.InfoFormat("newFolderPath: " + newFolderPath);

            var newImageUrl = Path.Combine(webRootPath, newPath, filename);
            _logger.InfoFormat("newImageUrl: " + newImageUrl);

            _logger.InfoFormat("uploadFile() Started.");
            try
            {
                if (!Directory.Exists(oldFolderPath))
                {
                    Directory.CreateDirectory(oldFolderPath);
                }

                if (!Directory.Exists(newFolderPath))
                {
                    Directory.CreateDirectory(newFolderPath);
                }

                if (!File.Exists(oldImagePath))
                    throw new FileNotFoundException();

                if (File.Exists(newImagePath))
                    File.Delete(newImagePath);

                _logger.InfoFormat("uploadFile() - Start Move Image to: {0}", newFolderPath);
                var file = new FileInfo(oldImagePath);
                file.MoveTo(newImagePath);
                _logger.InfoFormat("uploadFile() - End Move Image");

                _logger.InfoFormat("uploadFile() - Finished.");
                return newImageUrl;
            }
            catch (FileNotFoundException exFn)
            {
                _logger.DebugFormat("MoveFiles() - ERROR FileNotFoundException. Result = {0}", exFn.Message);
                throw new UserFriendlyException("File Not Found !", exFn.Message);
            }
        }

        public string MoveFilesLegalDoc(string filename, string oldPath, string newPath, int? orderNumber)
        {
            var checkFilename = orderNumber != null ? orderNumber + "-" + filename : filename;

            var webRootPath = _hostingEnvironment.WebRootPath;
            var oldImagePath = Path.Combine(webRootPath, oldPath, filename);
            var newImagePath = Path.Combine(webRootPath, newPath, checkFilename);
            var oldFolderPath = Path.Combine(webRootPath, oldPath);
            var newFolderPath = Path.Combine(webRootPath, newPath);
            var newImageUrl = getAbsoluteUri() + newPath + checkFilename;

            _logger.InfoFormat("uploadFile() Started.");
            try
            {
                if (!Directory.Exists(oldFolderPath))
                {
                    Directory.CreateDirectory(oldFolderPath);
                }

                if (!Directory.Exists(newFolderPath))
                {
                    Directory.CreateDirectory(newFolderPath);
                }

                if (!File.Exists(oldImagePath))
                    throw new FileNotFoundException();

                if (File.Exists(newImagePath))
                    File.Delete(newImagePath);

                _logger.InfoFormat("uploadFile() - Start Move Image to: {0}", newFolderPath);
                var file = new FileInfo(oldImagePath);
                file.MoveTo(newImagePath);
                _logger.InfoFormat("uploadFile() - End Move Image");

                _logger.InfoFormat("uploadFile() - Finished.");
                return newImageUrl.Replace(@"\", "/");
            }
            catch (FileNotFoundException exFn)
            {
                _logger.DebugFormat("MoveFiles() - ERROR FileNotFoundException. Result = {0}", exFn.Message);
                throw new UserFriendlyException("File Not Found !", exFn.Message);
            }
        }

        public string CopyKPFile(string filename, string oldPath, string newPath, int? orderNumber)
        {
            var checkFilename = orderNumber != null ? orderNumber + "-" + filename : filename;

            var webRootPath = _hostingEnvironment.WebRootPath;
            var oldImagePath = Path.Combine(webRootPath, oldPath, filename);
            var newImagePath = Path.Combine(webRootPath, newPath, checkFilename);
            var oldFolderPath = Path.Combine(webRootPath, oldPath);
            var newFolderPath = Path.Combine(webRootPath, newPath);
            var newImageUrl = getAbsoluteUri() + newPath + checkFilename;

            _logger.InfoFormat("uploadFile() Started.");
            try
            {
                if (!Directory.Exists(oldFolderPath))
                {
                    Directory.CreateDirectory(oldFolderPath);
                }

                if (!Directory.Exists(newFolderPath))
                {
                    Directory.CreateDirectory(newFolderPath);
                }

                if (!File.Exists(oldImagePath))
                    throw new FileNotFoundException();

                if (File.Exists(newImagePath))
                    File.Delete(newImagePath);

                _logger.InfoFormat("uploadFile() - Start Move Image to: {0}", newFolderPath);
                var file = new FileInfo(oldImagePath);
                file.CopyTo(newImagePath);
                _logger.InfoFormat("uploadFile() - End Move Image");

                _logger.InfoFormat("uploadFile() - Finished.");
                return newImageUrl.Replace(@"\", "/");
            }
            catch (FileNotFoundException exFn)
            {
                _logger.DebugFormat("MoveFiles() - ERROR FileNotFoundException. Result = {0}", exFn.Message);
                throw new UserFriendlyException("File Not Found !", exFn.Message);
            }
        }

        public string getAbsoluteUri()
        {
            var request = _httpContextAccessor.HttpContext.Request;
            UriBuilder uriBuilder = new UriBuilder();
            uriBuilder.Scheme = request.Scheme;
            uriBuilder.Host = request.Host.ToString();
            var test = uriBuilder.ToString();
            var result = test.Replace("[", "").Replace("]", "");
            return result;
        }

        public string ConvertIdToCode(int? Id)
        {
            if (Id != null)
            {
                var mappingTemplateCode = Id.ToString();
                return string.Format("M{0}", mappingTemplateCode);
            }
            else
            {
                return null;
            }
        }

        public string ConvertDocIdToDocCode(int Id)
        {
            var docCode = Id.ToString().PadLeft(5, '0');
            if (docCode.Length > 5)
            {
                throw new UserFriendlyException("Maximum Code is 99999!");
            }
            else
            {
                return docCode;
            }
        }

        public List<LinkPathListDto> GetBase64FileByPhysicalPath(string physicalPath)
        {
            var webRootPath = _hostingEnvironment.WebRootPath;
            var path = (webRootPath + physicalPath).Replace("/", @"\");
            
            List<string> pdfFiles = Directory.GetFiles(path)
                                        .Select(Path.GetFileName)
                                        .ToList();
            var returnBase64 = pdfFiles.Select(x => new LinkPathListDto
            {
                linkFile = Convert.ToBase64String(File.ReadAllBytes(path + @"\" + x)),
                filePhysicalPath = path + @"\" + x,
                filename = x,
                linkServerFile = x != null ? getAbsoluteUriWithoutTail() + GetURLWithoutHost(physicalPath + "/" + x) : null
            }).ToList();

            return returnBase64;
        }

        private void GetURLWithoutHost(string path, out string finalpath)
        {
            finalpath = path;
            try
            {
                Regex RegexObj = new Regex("[\\w\\W]*([\\/]Assets[\\w\\W\\s]*)");
                if (RegexObj.IsMatch(path))
                {
                    finalpath = RegexObj.Match(path).Groups[1].Value;
                }
            }
            catch (ArgumentException ex)
            {
            }
        }

        private string GetURLWithoutHost(string path)
        {
            string finalpath = path;
            try
            {
                Regex RegexObj = new Regex("[\\w\\W]*([\\/]Assets[\\w\\W\\s]*)");
                if (RegexObj.IsMatch(path))
                {
                    finalpath = RegexObj.Match(path).Groups[1].Value;
                }
            }
            catch (ArgumentException ex)
            {
            }
            return finalpath;
        }

        private string getAbsoluteUriWithoutTail()
        {
            var request = _httpContextAccessor.HttpContext.Request;
            UriBuilder uriBuilder = new UriBuilder();
            uriBuilder.Scheme = request.Scheme;
            uriBuilder.Host = request.Host.ToString();
            var test = uriBuilder.ToString();
            var result = test.Replace("[", "").Replace("]", "");
            int position = result.LastIndexOf('/');
            if (position > -1)
                result = result.Substring(0, result.Length - 1);

            if (request.PathBase != null)
            {
                if (!string.IsNullOrWhiteSpace(request.PathBase.Value))
                {
                    result += request.PathBase.Value;
                }
            }
            return result;
        }

        public List<LinkPathListDto> GetBase64FileByPhysicalPathFilter(string physicalPath, string fileName)
        {
            var webRootPath = _hostingEnvironment.WebRootPath;
            var path = (webRootPath + physicalPath).Replace("/", @"\");

            List<string> pdfFiles = Directory.GetFiles(path, fileName)
                                        .Select(Path.GetFileName)
                                        .ToList();
            var returnBase64 = pdfFiles.Select(x => new LinkPathListDto
            {
                linkFile = Convert.ToBase64String(File.ReadAllBytes(path + @"\" + x)),
                filePhysicalPath = physicalPath + @"\" + x,
                filename = x
            }).ToList();

            return returnBase64;
        }
    }
}
