﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.DataExporting.ExceptionHandler
{
    public class BadTemplateException : Exception
    {
        public string BadTemplateName { get; set; }

        public BadTemplateException(string message, string badTemplateName)
            : base(message)
        {
            BadTemplateName = badTemplateName;
        }
    }
}
