﻿using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.PSAS.SuratPeringatans.Dto;

namespace VDI.Demo.DataExporting.Pdf.SP
{
    public interface ISuratPeringatanGeneratePdfExporter
    {
        string SPGeneratePdf(string htmlContent, SendEmailSPInputDto data, string destinationPath, string file);
    }
}
