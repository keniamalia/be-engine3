﻿using Abp.UI;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.DataExporting.Excel.EpPlus;
using VDI.Demo.PSAS.SuratPeringatans.Dto;
using Visionet_Backend_NetCore.Komunikasi;

namespace VDI.Demo.DataExporting.Pdf.SP
{
    public class SuratPeringatanGeneratePdfExporter : PdfExporterBase, ISuratPeringatanGeneratePdfExporter
    {
        public string SPGeneratePdf(string htmlContent, SendEmailSPInputDto data, string destinationPath, string file)
        {
            htmlContent = Setting_variabel.GetHTMLMappedValue(htmlContent, data);

            string filePdf = null;
            string fileName = data.letterType + "_" + data.unitCode + "_" + data.unitNo;

            try
            {
                filePdf = CreatePdfPackageNoFileDto(fileName + ".pdf", htmlContent, Mse.PDFSharp.PageSize.A4, destinationPath, file);
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }

            return filePdf;
        }
    }
}
