﻿using Abp.AspNetZeroCore.Net;
using Abp.Dependency;
using Mse.HtmlRenderer.PdfSharp;
using Mse.PDFSharp;
using Mse.PDFSharp.Fonts;
using Mse.PDFSharp.Pdf;
using Mse.PDFSharp.Pdf.IO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using VDI.Demo.Dto;
using Visionet_Backend_NetCore.Komunikasi;

namespace VDI.Demo.DataExporting.Excel.EpPlus
{
    public abstract class PdfExporterBase : DemoServiceBase, ITransientDependency
    {
        public IAppFolders AppFolders { get; set; }

        public PdfExporterBase()
        {
        }

        protected FileDto CreatePdfPackage(string fileName, string htmlContent)
        {
            if (GlobalFontSettings.FontResolver == null)
            {
                GlobalFontSettings.FontResolver = new VDI.Demo.Application.SegoeWpFontResolver();
                GlobalFontSettings.DefaultFontEncoding = PdfFontEncoding.WinAnsi;
            }

            FileDto file=null;
            try
            {
                file = new FileDto(fileName, MimeTypeNames.ApplicationPdf);
                var filePath = Path.Combine(AppFolders.TempFileDownloadFolder, file.FileToken);

                if (!Directory.Exists(AppFolders.TempFileDownloadFolder))
                {
                    Directory.CreateDirectory(AppFolders.TempFileDownloadFolder);
                }

                PdfDocument pdf = PdfGenerator.GeneratePdf(htmlContent, PageSize.A4);
                pdf.Save(filePath);
            }
            catch (Exception e) {
                SendConsole(""+e.Message+" "+e.StackTrace );
            }

            return file;
        }

        protected FileDto CreatePdfPackage(string fileName, string htmlContent, PageSize pageSize)
        {
            if (GlobalFontSettings.FontResolver == null)
            {
                GlobalFontSettings.FontResolver = new VDI.Demo.Application.SegoeWpFontResolver();
                GlobalFontSettings.DefaultFontEncoding = PdfFontEncoding.WinAnsi;
            }

            FileDto file = null;
            try
            {
                file = new FileDto(fileName, MimeTypeNames.ApplicationPdf);
                var filePath = Path.Combine(AppFolders.TempFileDownloadFolder, file.FileToken);

                if (!Directory.Exists(AppFolders.TempFileDownloadFolder))
                {
                    Directory.CreateDirectory(AppFolders.TempFileDownloadFolder);
                }

                PdfDocument pdf = PdfGenerator.GeneratePdf(htmlContent, pageSize);
                pdf.Save(filePath);
            }
            catch (Exception e)
            {
                SendConsole("" + e.Message + " " + e.StackTrace);
            }

            return file;
        }

        protected FileDto CreatePdfPackage(string fileName, string htmlContent, PageSize pageSize, string destinationPath)
        {
            if (GlobalFontSettings.FontResolver == null)
            {
                GlobalFontSettings.FontResolver = new VDI.Demo.Application.SegoeWpFontResolver();
                GlobalFontSettings.DefaultFontEncoding = PdfFontEncoding.WinAnsi;
            }

            FileDto file = null;
            try
            {
                file = new FileDto(fileName, MimeTypeNames.ApplicationPdf);
                var filePath = Path.Combine(destinationPath, file.FileToken);

                if (!Directory.Exists(destinationPath))
                {
                    Directory.CreateDirectory(destinationPath);
                }

                PdfDocument pdf = PdfGenerator.GeneratePdf(htmlContent, pageSize);
                pdf.Save(filePath);
            }
            catch (Exception e)
            {
                SendConsole("" + e.Message + " " + e.StackTrace);
            }

            return file;
        }

        protected string CreatePdfPackageNoFileDto(string fileName, string htmlContent, PageSize pageSize, string destinationPath, string filePathReturn)
        {
            if (GlobalFontSettings.FontResolver == null)
            {
                GlobalFontSettings.FontResolver = new VDI.Demo.Application.SegoeWpFontResolver();
                GlobalFontSettings.DefaultFontEncoding = PdfFontEncoding.WinAnsi;
            }

            var filePathResult = "";
            try
            {
                var filePath = Path.Combine(destinationPath, fileName);

                if (!Directory.Exists(destinationPath))
                {
                    Directory.CreateDirectory(destinationPath);
                }

                PdfDocument pdf = PdfGenerator.GeneratePdf(htmlContent, pageSize);
                pdf.Save(filePath);
                filePathResult = filePathReturn + @"\" + fileName;
            }
            catch (Exception e)
            {
                SendConsole("" + e.Message + " " + e.StackTrace);
            }

            return filePathResult;
        }

        protected string CreatePdfPackageNoFileDtoLandscape(string fileName, string htmlContent, PageSize pageSize, PageOrientation pageOrientation, string destinationPath, string filePathReturn)
        {
            if (GlobalFontSettings.FontResolver == null)
            {
                GlobalFontSettings.FontResolver = new VDI.Demo.Application.SegoeWpFontResolver();
                GlobalFontSettings.DefaultFontEncoding = PdfFontEncoding.WinAnsi;
            }

            var filePathResult = "";
            try
            {
                var filePath = Path.Combine(destinationPath, fileName);

                if (!Directory.Exists(destinationPath))
                {
                    Directory.CreateDirectory(destinationPath);
                }

                PdfDocument pdf = PdfGenerator.GeneratePdf(htmlContent, pageSize, pageOrientation);
                pdf.Save(filePath);
                filePathResult = filePathReturn + @"\" + fileName;
            }
            catch (Exception e)
            {
                SendConsole("" + e.Message + " " + e.StackTrace);
            }

            return filePathResult;
        }

        protected string CreatePdfFromListFile(string fileName, List<string> listPdf, string destinationPath, string filePathReturn)
        {
            if (GlobalFontSettings.FontResolver == null)
            {
                GlobalFontSettings.FontResolver = new VDI.Demo.Application.SegoeWpFontResolver();
                GlobalFontSettings.DefaultFontEncoding = PdfFontEncoding.WinAnsi;
            }

            var filePathResult = "";
            try
            {
                var filePath = Path.Combine(destinationPath, fileName);

                if (!Directory.Exists(destinationPath))
                {
                    Directory.CreateDirectory(destinationPath);
                }
                // Open the output document
                PdfDocument outputDocument = new PdfDocument();

                // Iterate files
                foreach (string file in listPdf)
                {
                    var fileCombine = Path.Combine(destinationPath, file);
                    // Open the document to import pages from it.
                    PdfDocument inputDocument = PdfReader.Open(fileCombine, PdfDocumentOpenMode.Import);

                    // Iterate pages
                    int count = inputDocument.PageCount;
                    for (int idx = 0; idx < count; idx++)
                    {
                        // Get the page from the external document...
                        PdfPage page = inputDocument.Pages[idx];
                        // ...and add it to the output document.
                        outputDocument.AddPage(page);
                    }
                }
                outputDocument.Save(filePath);
                filePathResult = filePathReturn + @"\" + fileName;
            }
            catch (Exception e)
            {
                SendConsole("" + e.Message + " " + e.StackTrace);
            }

            return filePathResult;
        }

        #region debug console
        private void SendConsole(string msg)
        {
            if (Setting_variabel.enable_tcp_debug == true)
            {
                if (Setting_variabel.Komunikasi_TCPListener == null)
                {
                    Setting_variabel.Komunikasi_TCPListener = new Visionet_Backend_NetCore.Komunikasi.Komunikasi_TCPListener(17000);
                    Task.Run(() => StartListenerLokal());
                }

                if (Setting_variabel.Komunikasi_TCPListener != null)
                {
                    if (Setting_variabel.Komunikasi_TCPListener.IsRunning())
                    {
                        if (Setting_variabel.ConsoleBayangan != null)
                        {
                            Setting_variabel.ConsoleBayangan.Send("[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "] " + msg);
                        }
                    }
                }
            }

        }
        #endregion

        #region listener tcp debug
        protected void StartListenerLokal()
        {
            if (Setting_variabel.Komunikasi_TCPListener != null)
                Setting_variabel.Komunikasi_TCPListener.StartListener();
        }

        protected void StopListenerLokal()
        {
            if (Setting_variabel.Komunikasi_TCPListener != null)
                Setting_variabel.Komunikasi_TCPListener.StopListener();
        }
        #endregion
    }
}