﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using Abp.UI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Mse.PDFSharp;
using Mse.PDFSharp.Pdf;
using Mse.PDFSharp.Pdf.IO;
using Newtonsoft.Json.Linq;
using VDI.Demo.DataExporting.Excel.EpPlus;
using VDI.Demo.DataExporting.ExceptionHandler;
using VDI.Demo.DataExporting.Pdf.Helper;
using VDI.Demo.Dto;
using VDI.Demo.PSAS.LegalDocument.BookingDocument.Dto;
using Visionet_Backend_NetCore.Komunikasi;

namespace VDI.Demo.DataExporting.Pdf.LegalDocument
{
    public class LegalDocumentGeneratePdfExporter : BasePdfService, ILegalDocumentGeneratePdfExporter
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IHostingEnvironment _hostingEnvironment;
        private string _outputFolder;

        public LegalDocumentGeneratePdfExporter(
            IHttpContextAccessor httpContextAccessor,
            IHostingEnvironment environment)
            : base()
        {
            _httpContextAccessor = httpContextAccessor;
            _hostingEnvironment = environment;
            //RegisterTemplates("P3U");
            _outputFolder = ConfigurationManager.AppSettings["PdfOutput"];
        }

        public byte[] PrintP3UGeneratePdf(P32DocumentDto dto)
        {
            try
            {
                //KonfirmasiPesananDocService kpService = new KonfirmasiPesananDocService();
                //byte[] kpPdf = null;
                //if (dto.KP != null)
                //{
                //    kpPdf = kpService.GetKPDocument(dto.KP);
                //}

                object[] templatesOrPdfs = new object[dto.physicalPath.Count];
                for (var i = 0; i < dto.physicalPath.Count; i++)
                {
                    if (dto.physicalPath[i].Contains("KP.html"))
                    {
                        //templatesOrPdfs[i] = kpPdf;
                        Logger.Info("KP di skip.");
                    }
                    else if (dto.physicalPath[i].Contains("FileKadaster.html") && dto.isGenerateBySystem)
                    {
                        var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
                        var webConfigApp = (JObject)appsettingsjson["App"];

                        templatesOrPdfs[i] = webConfigApp.Property("HeaderFooter").Value.ToString();
                    }
                    else if (new FileInfo(dto.physicalPath[i]).Extension == ".pdf")
                    {
                        byte[] bytesOfKadaster = File.ReadAllBytes(dto.physicalPath[i]);
                        templatesOrPdfs[i] = bytesOfKadaster;
                    }
                    else
                    {
                        templatesOrPdfs[i] = dto.physicalPath[i];
                    }
                }

                byte[] pdf = null;
                if (dto.physicalPath.Count > 0)
                {
                    pdf = WriteP3U(dto, PageSize.A4, 10,
                            templatesOrPdfs
                        );
                }

                return pdf;
            }
            catch (BadTemplateException ex)
            {
                throw new InvalidOperationException(String.Format("Could not load template '{0}' from template folder '{1}'.", ex.BadTemplateName, ""), ex);
            }
        }

    }
}
