﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.Dto;
using VDI.Demo.PSAS.LegalDocument.BookingDocument.Dto;

namespace VDI.Demo.DataExporting.Pdf.LegalDocument
{
    public interface ILegalDocumentGeneratePdfExporter : IApplicationService
    {
        byte[] PrintP3UGeneratePdf(P32DocumentDto data);
    }
}
