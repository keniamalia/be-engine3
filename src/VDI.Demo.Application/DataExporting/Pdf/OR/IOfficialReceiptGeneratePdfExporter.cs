﻿using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.Dto;
using VDI.Demo.Payment.InputPayment.Dto;

namespace VDI.Demo.DataExporting.Pdf.OR
{
    public interface IOfficialReceiptGeneratePdfExporter
    {
        string PrintORGeneratePdf(string htmlContent, PrintORDto data, string destinationPath, string file);
        string PrintORGeneratePdfFromListFile(List<string> listPdf, string destinationPath, string file);
    }
}
