﻿using Abp.Timing.Timezone;
using Abp.UI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using VDI.Demo.DataExporting.Excel.EpPlus;
using VDI.Demo.Dto;
using VDI.Demo.Payment.InputPayment.Dto;
using Visionet_Backend_NetCore.Komunikasi;

namespace VDI.Demo.DataExporting.Pdf.OR
{
    public class OfficialReceiptGeneratePdfExporter : PdfExporterBase, IOfficialReceiptGeneratePdfExporter
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IHostingEnvironment _hostingEnvironment;

        public OfficialReceiptGeneratePdfExporter(
            IHttpContextAccessor httpContextAccessor,
            IHostingEnvironment environment)
        {
            _httpContextAccessor = httpContextAccessor;
            _hostingEnvironment = environment;
        }

        public string PrintORGeneratePdf(string htmlContent, PrintORDto data, string destinationPath, string file)
        {
            htmlContent = Setting_variabel.GetHTMLMappedValue(htmlContent, data);
            
            string filePdf = null;
            var transNo = data.transNo.Replace('/', '_');
            var unitCode = data.unitCode.Replace('-', '_');
            var fileName = transNo + "-"+ unitCode + "-" + data.unitNo;
            //var fileNameNew = fileName.Replace('/', '_');

            try
            {
                filePdf = CreatePdfPackageNoFileDto(fileName + ".pdf", htmlContent, Mse.PDFSharp.PageSize.A5, destinationPath, file);
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }

            return filePdf;
        }

        public string PrintORGeneratePdfFromListFile(List<string> listPdf, string destinationPath, string file)
        {
            string filePdf = null;
            //var transNo = data.transNo.Replace('/', '_');
            //var unitCode = data.unitCode.Replace('-', '_');
            var fileName = "Gabungan" + "-" + 1 + "-" + DateTime.Now.ToString("dd-MM-yyy-hh-mm-ss");
            //var fileNameNew = fileName.Replace('/', '_');

            try
            {
                filePdf = CreatePdfFromListFile(fileName + ".pdf", listPdf, destinationPath, file);
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }

            return filePdf;
        }
    }
}
