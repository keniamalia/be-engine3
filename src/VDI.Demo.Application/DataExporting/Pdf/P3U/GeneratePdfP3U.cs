﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.DataExporting.Excel.EpPlus;

namespace VDI.Demo.DataExporting.Pdf.P3U
{
    public class GeneratePdfP3U : PdfExporterBase, IGeneratePdfP3U
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IHostingEnvironment _hostingEnvironment;

        public GeneratePdfP3U(
            IHttpContextAccessor httpContextAccessor,
            IHostingEnvironment environment)
        {
            _httpContextAccessor = httpContextAccessor;
            _hostingEnvironment = environment;
        }

    }
}
