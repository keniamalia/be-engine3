﻿using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using VDI.Demo.DataExporting.Excel.EpPlus;
using VDI.Demo.Dto;
using VDI.Demo.PSAS.AccountStatement.Dto;
using Visionet_Backend_NetCore.Komunikasi;

namespace VDI.Demo.DataExporting.Pdf.Exporter.AccountStatement
{
    public class GenerateAccountStatementExporter : PdfExporterBase, IGenerateAccountStatementExporter
    {
        private readonly IHostingEnvironment _hostingEnvironment;

        public GenerateAccountStatementExporter(IHostingEnvironment environment)
        {
            _hostingEnvironment = environment;
        }
        public FileDto GeneratePdfAccountStatement(List<GetAccountStatementToExportDto> data)
        {
            StringBuilder htmlContents = new StringBuilder();
            var headerPath = _hostingEnvironment.WebRootPath + @"\Assets\HtmlTemplates\AS\header.html";
            var footerPath = _hostingEnvironment.WebRootPath + @"\Assets\HtmlTemplates\AS\footer.html";

            foreach (var headerFooter in data)
            {
                // header
                string htmlHeader = File.ReadAllText(headerPath);
                htmlContents.Append(Setting_variabel.GetHTMLMappedValue(htmlHeader, headerFooter));

                var idx = 1;
                foreach (var paymentschedule in headerFooter.getDataSchedulePayment)
                {
                    //datatable
                    var bgColor = ((idx % 2 == 0) ? "bgcolor='#f2f2f2'" : "");
                    htmlContents.AppendLine("<tr>" );
                    htmlContents.AppendLine("    <td " + bgColor + ">" + paymentschedule.dueDate + "</td>");
                    htmlContents.AppendLine("    <td " + bgColor + ">" + paymentschedule.paymentDate + "</td>");

                    if (paymentschedule.type.Equals('S'))
                    {
                        htmlContents.AppendLine("    <td " + bgColor + ">"+ paymentschedule.transactionDesc + "</td>");
                        htmlContents.AppendLine("    <td class='pull-right' " + bgColor + ">" + paymentschedule.scheduleAmount + "</td>");
                        htmlContents.AppendLine("    <td " + bgColor + ">" +"</td>");
                    }
                    else
                    {
                        htmlContents.AppendLine("    <td " + bgColor + ">" + "><span class='tab'>" + paymentschedule.transactionDesc + "</td>");
                        htmlContents.AppendLine("    <td " + bgColor + ">" + "></td>");
                        htmlContents.AppendLine("    <td class='pull-right' " + bgColor + ">" + paymentschedule.paymentAmount + "</td>");
                    }

                    htmlContents.AppendLine("</tr>");
                    idx++;
                }

                //footer
                string htmlFooter = File.ReadAllText(footerPath);
                htmlContents.Append(Setting_variabel.GetHTMLMappedValue(htmlFooter, headerFooter));
            }

            FileDto filePdf = null;
            try
            {
                filePdf = CreatePdfPackage("AccountStatement-" + DateTime.Now.ToString("yyyyMMddhhmmssfff") + ".pdf", htmlContents.ToString());
            }
            catch (Exception e)
            {
                SendConsole("" + e.Message + " " + e.StackTrace);
            }

            return filePdf;
        }

        #region debug console
        private void SendConsole(string msg)
        {
            if (Setting_variabel.enable_tcp_debug == true)
            {
                if (Setting_variabel.Komunikasi_TCPListener == null)
                {
                    Setting_variabel.Komunikasi_TCPListener = new Visionet_Backend_NetCore.Komunikasi.Komunikasi_TCPListener(17000);
                    Task.Run(() => StartListenerLokal());
                }

                if (Setting_variabel.Komunikasi_TCPListener != null)
                {
                    if (Setting_variabel.Komunikasi_TCPListener.IsRunning())
                    {
                        if (Setting_variabel.ConsoleBayangan != null)
                        {
                            Setting_variabel.ConsoleBayangan.Send("[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "] " + msg);
                        }
                    }
                }
            }

        }
        #endregion
    }
}
