﻿using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.Dto;
using VDI.Demo.PSAS.AccountStatement.Dto;

namespace VDI.Demo.DataExporting.Pdf.Exporter.AccountStatement
{
    public interface IGenerateAccountStatementExporter
    {
        FileDto GeneratePdfAccountStatement(List<GetAccountStatementToExportDto> data);
    }
}
