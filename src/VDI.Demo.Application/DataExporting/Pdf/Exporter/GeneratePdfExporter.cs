﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using Abp.UI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Threading.Tasks;
using VDI.Demo.DataExporting.Excel.EpPlus;
using VDI.Demo.Dto;
using Visionet_Backend_NetCore.Komunikasi;
using VDI.Demo.Sessions.Dto;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.DataExporting.Pdf.Exporter
{
    public class GeneratePdfExporter : PdfExporterBase, IGeneratePdfExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IHostingEnvironment _hostingEnvironment;

        public GeneratePdfExporter(
            ITimeZoneConverter timeZoneConverter,
            IHttpContextAccessor httpContextAccessor,
            IAbpSession abpSession,
            IHostingEnvironment environment)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
            _httpContextAccessor = httpContextAccessor;
            _hostingEnvironment = environment;
        }

        public FileDto GeneratePdfSKLFinance(string htmlContent, List<GetOnlinePdfDto> data)
        {
            SendConsole("GeneratePdfSKLFinance");

            StringBuilder strb = new StringBuilder();
            foreach (var item in data)
            {
                strb.Append(Setting_variabel.GetHTMLMappedValue(htmlContent, item));
            }
            htmlContent = strb.ToString();

            SendConsole("htmlContent:"+ htmlContent);

            FileDto filePdf = null;
            try
            {
                filePdf = CreatePdfPackage("PdfTest-"+DateTime.Now.ToString("yyyyMMddhhmmssfff")+".pdf", htmlContent);
            }
            catch (Exception e)
            {
                SendConsole("" + e.Message + " " + e.StackTrace);
            }

            return filePdf;
        }

        #region debug console
        private void SendConsole(string msg)
        {
            if (Setting_variabel.enable_tcp_debug == true)
            {
                if (Setting_variabel.Komunikasi_TCPListener == null)
                {
                    Setting_variabel.Komunikasi_TCPListener = new Visionet_Backend_NetCore.Komunikasi.Komunikasi_TCPListener(17000);
                    Task.Run(() => StartListenerLokal());
                }

                if (Setting_variabel.Komunikasi_TCPListener != null)
                {
                    if (Setting_variabel.Komunikasi_TCPListener.IsRunning())
                    {
                        if (Setting_variabel.ConsoleBayangan != null)
                        {
                            Setting_variabel.ConsoleBayangan.Send("[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "] " + msg);
                        }
                    }
                }
            }

        }
        #endregion

        #region listener tcp debug
        protected void StartListenerLokal()
        {
            if (Setting_variabel.Komunikasi_TCPListener != null)
                Setting_variabel.Komunikasi_TCPListener.StartListener();
        }

        protected void StopListenerLokal()
        {
            if (Setting_variabel.Komunikasi_TCPListener != null)
                Setting_variabel.Komunikasi_TCPListener.StopListener();
        }
        #endregion
    }
}
