﻿using Abp.UI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.DataExporting.Excel.EpPlus;
using VDI.Demo.PPOnline.OrderPP.Dto;
using VDI.Demo.PPOnline.PaymentPP.Dto;
using VDI.Demo.PPOnline.RefundPP.Dto;
using Visionet_Backend_NetCore.Komunikasi;

namespace VDI.Demo.DataExporting.Pdf.PPOnline
{
    public class PPOnlineGeneratePdfExporter : PdfExporterBase, IPPOnlineGeneratePdfExporter
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IHostingEnvironment _hostingEnvironment;

        public PPOnlineGeneratePdfExporter(
            IHttpContextAccessor httpContextAccessor,
            IHostingEnvironment environment)
        {
            _httpContextAccessor = httpContextAccessor;
            _hostingEnvironment = environment;
        }

        public string PrintPreviewRefundGeneratePdf(string htmlContent, GeneratePreviewRefundInputDto data, string destinationPath, string file)
        {
            htmlContent = Setting_variabel.GetHTMLMappedValue(htmlContent, data);

            string filePdf = null;
            var fileName = "PR_" + data.customerCode + "_" + data.PPNumber;

            try
            {
                filePdf = CreatePdfPackageNoFileDtoLandscape(fileName + ".pdf", htmlContent, Mse.PDFSharp.PageSize.A4, Mse.PDFSharp.PageOrientation.Landscape, destinationPath, file);
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }

            return filePdf;
        }
        public string PrintTTBFGeneratePdf(string htmlContent, ListTTBFInputDto data, string destinationPath, string file)
        {
            var html = "";
            foreach (var item in data.inputDto)
            {
                var dataHtml = Setting_variabel.GetHTMLMappedValue(htmlContent, item.dto);

                html += dataHtml;
            }

            string filePdf = null;
            var fileName = data.name;

            try
            {
                filePdf = CreatePdfPackageNoFileDto(fileName + ".pdf", html, Mse.PDFSharp.PageSize.A4, destinationPath, file);
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }

            return filePdf;
        }
        public string PrintReportOrderPdf(string htmlContent, ListDataDto data, string destinationPath, string file, Mse.PDFSharp.PageSize pageSize, Mse.PDFSharp.PageOrientation pageOrientation)
        {
            htmlContent = Setting_variabel.GetHTMLMappedValue(htmlContent, data);
            
            string filePdf = null;
            var fileName = "Report_Order_" + String.Format("{0:ddMMyyyy_hhmmss}", DateTime.Now);

            try
            {
                filePdf = CreatePdfPackageNoFileDtoLandscape(fileName + ".pdf", htmlContent, pageSize, pageOrientation, destinationPath, file);
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }

            return filePdf;
        }
        public string PrintReportPaymentrPdf(string htmlContent, ListDataPaymentDto data, string destinationPath, string file, Mse.PDFSharp.PageSize pageSize, Mse.PDFSharp.PageOrientation pageOrientation)
        {
            htmlContent = Setting_variabel.GetHTMLMappedValue(htmlContent, data);

            string filePdf = null;
            var fileName = "Report_Payment_" + String.Format("{0:ddMMMyyyy_hhmmss}", DateTime.Now);

            try
            {
                filePdf = CreatePdfPackageNoFileDtoLandscape(fileName + ".pdf", htmlContent, pageSize, pageOrientation, destinationPath, file);
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }

            return filePdf;
        }
    }
}
