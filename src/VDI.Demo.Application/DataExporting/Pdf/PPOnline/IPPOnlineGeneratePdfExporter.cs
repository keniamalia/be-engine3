﻿using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.PPOnline.OrderPP.Dto;
using VDI.Demo.PPOnline.PaymentPP.Dto;
using VDI.Demo.PPOnline.RefundPP.Dto;

namespace VDI.Demo.DataExporting.Pdf.PPOnline
{
    public interface IPPOnlineGeneratePdfExporter
    {
        string PrintPreviewRefundGeneratePdf(string htmlContent, GeneratePreviewRefundInputDto data, string destinationPath, string file);

        string PrintTTBFGeneratePdf(string htmlContent, ListTTBFInputDto data, string destinationPath, string file);

        string PrintReportOrderPdf(string htmlContent, ListDataDto data, string destinationPath, string file, Mse.PDFSharp.PageSize pageSize, Mse.PDFSharp.PageOrientation pageOrientation);

        string PrintReportPaymentrPdf(string htmlContent, ListDataPaymentDto data, string destinationPath, string file, Mse.PDFSharp.PageSize pageSize, Mse.PDFSharp.PageOrientation pageOrientation);
    }
}
