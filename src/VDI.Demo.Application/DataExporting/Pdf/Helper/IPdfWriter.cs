﻿using Mse.PDFSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace VDI.Demo.DataExporting.Pdf.Helper
{
    public interface IPdfWriter
    {
        IDictionary<string, string> Templates { get; set; }
        byte[] Write<V>(V dto, PageSize pageSize, int margin, params object[] templates);
        byte[] WriteP3U<V>(V dto, PageSize pageSize, int margin, params object[] templatesOrPdfs);
    }
}
