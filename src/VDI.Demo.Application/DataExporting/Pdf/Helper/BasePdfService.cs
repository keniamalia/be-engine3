﻿using Abp;
using Abp.Dependency;
using Microsoft.AspNetCore.Identity;
using Mse.PDFSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text;
using VDI.Demo.Authorization.Users;
using Mustache;
using VDI.Demo.DataExporting.ExceptionHandler;
using Mse.PDFSharp.Pdf;
using System.Linq;
using Mse.HtmlRenderer.PdfSharp;
using Mse.PDFSharp.Pdf.IO;
using Mse.PDFSharp.Fonts;
using Abp.UI;

namespace VDI.Demo.DataExporting.Pdf.Helper
{
    public abstract class BasePdfService : DemoServiceBase, ITransientDependency, IPdfWriter
    {
        protected string AllTemplatesFolder { get; private set; }
        protected string DocTemplatesFolder { get; private set; }
        protected string TemplatesFolderFullPath
        {
            get
            {
                return Path.Combine(AllTemplatesFolder, DocTemplatesFolder);
            }
        }
        public IDictionary<string, string> Templates { get; set; }

        public BasePdfService(IPdfWriter pdfWriter, string allTemplatesFolder)
        {
            if (pdfWriter == null) throw new ArgumentNullException(nameof(pdfWriter));
            if (String.IsNullOrWhiteSpace(allTemplatesFolder)) throw new ArgumentNullException(nameof(allTemplatesFolder));

            Templates = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
            AllTemplatesFolder = allTemplatesFolder;
        }
        public BasePdfService() {
            Templates = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
            AllTemplatesFolder = ConfigurationManager.AppSettings["TemplateFolder"];
        }
        //public BasePdfService()
        //    : this(new PdfWriter(), ConfigurationManager.AppSettings["TemplateFolder"])
        //{

        //}

        /// <summary>
        /// Gets all templates saved in a template-folder
        /// and saves them in the PdfWriter.
        /// </summary>
        protected void RegisterTemplates(string docTemplatesFolder)
        {
            if (String.IsNullOrWhiteSpace(docTemplatesFolder)) throw new ArgumentNullException(nameof(docTemplatesFolder));

            var templatesFolderFullPath = Path.Combine(AllTemplatesFolder, docTemplatesFolder);

            if (!Directory.Exists(templatesFolderFullPath)) throw new InvalidOperationException(String.Format("Could not register the .html templates at '{0}' because the directory does not exist.", templatesFolderFullPath));

            var templatePaths = Directory.EnumerateFiles(templatesFolderFullPath, "*.html", SearchOption.TopDirectoryOnly);

            foreach (var templatePath in templatePaths)
            {
                try
                {
                    var templateName = Path.GetFileNameWithoutExtension(templatePath);
                    var template = File.ReadAllText(templatePath);

                    Templates.Add(templateName, template);
                }
                catch (Exception ex)
                {
                    throw new InvalidOperationException(String.Format("An error occured while trying to load template '{0}'. {1}", templatePath, ex.Message));
                }
            }

            DocTemplatesFolder = docTemplatesFolder;
        }

        public string WriteOn<T>(string templateName, T dto)
        {
            var compiler = new FormatCompiler();
            var generator = compiler.Compile(GetTemplate(templateName));
            return generator.Render(dto);
        }

        private string GetTemplate(string templateName)
        {
            if (Templates.ContainsKey(templateName))
            {
                return Templates[templateName];
            }
            else
            {
                var existingTemplateNames = String.Join(", ", Templates.Keys.Select(x => String.Format("'{0}'", x)));
                throw new BadTemplateException(String.Format("No template called '{0}' has been registered. The only templates available are {1}. Register templates with TemplateWriter.Templates.Add().",
                    templateName,
                    existingTemplateNames
                    ), templateName);
            }
        }

        public string WriteOnP3u<T>(string templateName, T dto)
        {
            var compiler = new FormatCompiler();
            var htmlTemplate = File.ReadAllText(templateName);
            var generator = compiler.Compile(htmlTemplate);
            return generator.Render(dto);
        }

        public byte[] Write<T>(T dto, PageSize pageSize, int margin, params string[] templates)
        {
            var output = new PdfDocument();

            foreach (var template in templates)
            {
                AppendTo(output, template, dto, pageSize, margin);
            }

            var outputStream = new MemoryStream();
            output.Save(outputStream);

            return outputStream.ToArray();
        }

        public byte[] WriteP3U<T>(T dto, PageSize pagesize, int margin, params object[] templatesOrPdfs)
        {
            if (GlobalFontSettings.FontResolver == null)
            {
                GlobalFontSettings.FontResolver = new VDI.Demo.Application.SegoeWpFontResolver();
                GlobalFontSettings.DefaultFontEncoding = PdfFontEncoding.WinAnsi;
            }

            var output = new PdfDocument();

            foreach (var item in templatesOrPdfs)
            {
                if (item == null)
                {
                    continue;
                }
                else if (item.GetType() == typeof(string))
                {
                    var template = (string)item;
                    AppendToP3U(output, template, dto, pagesize, margin);
                }
                else if (item.GetType() == typeof(byte[]))
                {
                    var pdf = (byte[])item;
                    AppendTo(output, pdf);
                }
            }

            var outputStream = new MemoryStream();
            output.Save(outputStream);

            return outputStream.ToArray();
        }

        public byte[] Write<T>(T dto, PageSize pagesize, int margin, params object[] templatesOrPdfs)
        {
            var output = new PdfDocument();

            foreach (var item in templatesOrPdfs)
            {
                if (item == null)
                {
                    continue;
                }
                else if (item.GetType() == typeof(string))
                {
                    var template = (string)item;
                    AppendTo(output, template, dto, pagesize, margin);
                }
                else if (item.GetType() == typeof(byte[]))
                {
                    var pdf = (byte[])item;
                    AppendTo(output, pdf);
                }
            }

            var outputStream = new MemoryStream();
            output.Save(outputStream);

            return outputStream.ToArray();
        }

        private void AppendTo<T>(PdfDocument targetPdf, string template, T dto, PageSize pageSize, int margin)
        {
            var html = WriteOn(template, dto);
            var inputPdf = PdfGenerator.GeneratePdf(html, pageSize, margin, null, null, null);

            // 'newPages' can't just be added since it was not opened with PdfDocumentOpenMode.Import. 
            // We thus have to go a roundabout way.
            var newPagesStream = new MemoryStream();
            try
            {
                inputPdf.Save(newPagesStream);
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Cannot save a PDF document with no pages")) return;
                else throw;
            }

            // .Save() closes the stream immediately, so we have to reopen it.
            var reOpenedNewPagesStream = new MemoryStream(newPagesStream.ToArray());
            var inputPdf2 = PdfReader.Open(reOpenedNewPagesStream, PdfDocumentOpenMode.Import);

            //Assume each HTML template is always a single page
            //and that additional pages are just overflow.
            targetPdf.AddPage(inputPdf2.Pages[0]);

            reOpenedNewPagesStream.Close();
        }

        private void AppendToP3U<T>(PdfDocument targetPdf, string template, T dto, PageSize pageSize, int margin)
        {
            var html = WriteOnP3u(template, dto);
            var inputPdf = PdfGenerator.GeneratePdf(html, pageSize, margin, null, null, null);

            // 'newPages' can't just be added since it was not opened with PdfDocumentOpenMode.Import. 
            // We thus have to go a roundabout way.
            var newPagesStream = new MemoryStream();
            try
            {
                inputPdf.Save(newPagesStream);
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Cannot save a PDF document with no pages")) return;
                else throw;
            }

            // .Save() closes the stream immediately, so we have to reopen it.
            var reOpenedNewPagesStream = new MemoryStream(newPagesStream.ToArray());
            var inputPdf2 = PdfReader.Open(reOpenedNewPagesStream, PdfDocumentOpenMode.Import);

            //Assume each HTML template is always a single page
            //and that additional pages are just overflow.
            targetPdf.AddPage(inputPdf2.Pages[0]);

            reOpenedNewPagesStream.Close();
        }

        private void AppendTo(PdfDocument targetPdf, byte[] pdf)
        {
            try
            {
                var stream = new MemoryStream(pdf);
                var pdfDoc = PdfReader.Open(stream, PdfDocumentOpenMode.Import);

                foreach (PdfPage page in pdfDoc.Pages)
                {
                    targetPdf.AddPage(page);
                }

                stream.Close();
            }
            catch (Exception ex)
            {
                Logger.DebugFormat("AppendTo() - ERROR Exception. Result = {0}", ex.Message);
                if (ex.Message.Contains("The file is not a valid PDF document"))
                {
                    throw new UserFriendlyException("Your Doc is corrupt.");
                }
                else
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
        }
    }
}
