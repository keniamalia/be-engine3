﻿//using Mse.HtmlRenderer.PdfSharp;
//using Mse.PDFSharp;
//using Mse.PDFSharp.Pdf;
//using Mse.PDFSharp.Pdf.IO;
//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Text;
//using Mustache;
//using System.Linq;
//using VDI.Demo.DataExporting.ExceptionHandler;

//namespace VDI.Demo.DataExporting.Pdf.Helper
//{
//    public class PdfWriter : IPdfWriter
//    {
//        public IDictionary<string, string> Templates { get; set; }

//        public PdfWriter()
//        {
//            Templates = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
//        }

//        public string WriteOn<T>(string templateName, T dto)
//        {
//            var compiler = new FormatCompiler();
//            var generator = compiler.Compile(GetTemplate(templateName));
//            return generator.Render(dto);
//        }

//        private string GetTemplate(string templateName)
//        {
//            if (Templates.ContainsKey(templateName))
//            {
//                return Templates[templateName];
//            }
//            else
//            {
//                var existingTemplateNames = String.Join(", ", Templates.Keys.Select(x => String.Format("'{0}'", x)));
//                throw new BadTemplateException(String.Format("No template called '{0}' has been registered. The only templates available are {1}. Register templates with TemplateWriter.Templates.Add().",
//                    templateName,
//                    existingTemplateNames
//                    ), templateName);
//            }
//        }

//        public string WriteOnP3u<T>(string templateName, T dto)
//        {
//            var compiler = new FormatCompiler();
//            var htmlTemplate = File.ReadAllText(templateName);
//            var generator = compiler.Compile(htmlTemplate);
//            return generator.Render(dto);
//        }

//        public byte[] Write<T>(T dto, PageSize pageSize, int margin, params string[] templates)
//        {
//            var output = new PdfDocument();

//            foreach (var template in templates)
//            {
//                AppendTo(output, template, dto, pageSize, margin);
//            }

//            var outputStream = new MemoryStream();
//            output.Save(outputStream);

//            return outputStream.ToArray();
//        }

//        public byte[] WriteP3U<T>(T dto, PageSize pagesize, int margin, params object[] templatesOrPdfs)
//        {
//            var output = new PdfDocument();

//            foreach (var item in templatesOrPdfs)
//            {
//                if (item == null)
//                {
//                    continue;
//                }
//                else if (item.GetType() == typeof(string))
//                {
//                    var template = (string)item;
//                    AppendToP3U(output, template, dto, pagesize, margin);
//                }
//                else if (item.GetType() == typeof(byte[]))
//                {
//                    var pdf = (byte[])item;
//                    AppendTo(output, pdf);
//                }
//            }

//            var outputStream = new MemoryStream();
//            output.Save(outputStream);

//            return outputStream.ToArray();
//        }

//        public byte[] Write<T>(T dto, PageSize pagesize, int margin, params object[] templatesOrPdfs)
//        {
//            var output = new PdfDocument();

//            foreach (var item in templatesOrPdfs)
//            {
//                if (item == null)
//                {
//                    continue;
//                }
//                else if (item.GetType() == typeof(string))
//                {
//                    var template = (string)item;
//                    AppendTo(output, template, dto, pagesize, margin);
//                }
//                else if (item.GetType() == typeof(byte[]))
//                {
//                    var pdf = (byte[])item;
//                    AppendTo(output, pdf);
//                }
//            }

//            var outputStream = new MemoryStream();
//            output.Save(outputStream);

//            return outputStream.ToArray();
//        }

//        private void AppendTo<T>(PdfDocument targetPdf, string template, T dto, PageSize pageSize, int margin)
//        {
//            var html = WriteOn(template, dto);
//            var inputPdf = PdfGenerator.GeneratePdf(html, pageSize, margin, null, null, null);

//            // 'newPages' can't just be added since it was not opened with PdfDocumentOpenMode.Import. 
//            // We thus have to go a roundabout way.
//            var newPagesStream = new MemoryStream();
//            try
//            {
//                inputPdf.Save(newPagesStream);
//            }
//            catch (Exception ex)
//            {
//                if (ex.Message.Contains("Cannot save a PDF document with no pages")) return;
//                else throw;
//            }

//            // .Save() closes the stream immediately, so we have to reopen it.
//            var reOpenedNewPagesStream = new MemoryStream(newPagesStream.ToArray());
//            var inputPdf2 = PdfReader.Open(reOpenedNewPagesStream, PdfDocumentOpenMode.Import);

//            //Assume each HTML template is always a single page
//            //and that additional pages are just overflow.
//            targetPdf.AddPage(inputPdf2.Pages[0]);

//            reOpenedNewPagesStream.Close();
//        }

//        private void AppendToP3U<T>(PdfDocument targetPdf, string template, T dto, PageSize pageSize, int margin)
//        {
//            var html = WriteOnP3u(template, dto);
//            var inputPdf = PdfGenerator.GeneratePdf(html, pageSize, margin, null, null, null);

//            // 'newPages' can't just be added since it was not opened with PdfDocumentOpenMode.Import. 
//            // We thus have to go a roundabout way.
//            var newPagesStream = new MemoryStream();
//            try
//            {
//                inputPdf.Save(newPagesStream);
//            }
//            catch (Exception ex)
//            {
//                if (ex.Message.Contains("Cannot save a PDF document with no pages")) return;
//                else throw;
//            }

//            // .Save() closes the stream immediately, so we have to reopen it.
//            var reOpenedNewPagesStream = new MemoryStream(newPagesStream.ToArray());
//            var inputPdf2 = PdfReader.Open(reOpenedNewPagesStream, PdfDocumentOpenMode.Import);

//            //Assume each HTML template is always a single page
//            //and that additional pages are just overflow.
//            targetPdf.AddPage(inputPdf2.Pages[0]);

//            reOpenedNewPagesStream.Close();
//        }

//        private void AppendTo(PdfDocument targetPdf, byte[] pdf)
//        {
//            var stream = new MemoryStream(pdf);
//            var pdfDoc = PdfReader.Open(stream, PdfDocumentOpenMode.Import);

//            foreach (PdfPage page in pdfDoc.Pages)
//            {
//                targetPdf.AddPage(page);
//            }

//            stream.Close();
//        }
//    }
//}
