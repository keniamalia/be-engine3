﻿using Abp.Domain.Repositories;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VDI.Demo.Authorization.Accounts;
using VDI.Demo.Authorization.Accounts.Dto;
using VDI.Demo.Authorization.Users;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.Files;
using VDI.Demo.OnlineBooking.CustomerMember;
using VDI.Demo.OnlineBooking.CustomerMember.Dto;
using VDI.Demo.PPOnlineDB;
using VDI.Demo.PropertySystemDB.OnlineBooking.DemoDB;
using VDI.Demo.PropertySystemDB.OnlineBooking.PPOnline;
using System.Linq;
using Abp.UI;
using VDI.Demo.PPOnline.CustomerPP.Dto;
using Abp.Domain.Uow;
using Abp.AutoMapper;
using Microsoft.AspNetCore.Mvc;
using VDI.Demo.OnlineBooking.Transaction.Dto;

namespace VDI.Demo.PPOnline.CustomerPP
{
    public class CustomerPPAppService : DemoAppServiceBase
    {
        private readonly IRepository<LK_PaymentType> _lkPaymentType;
        private readonly IRepository<MS_Projects> _msProject;
        private readonly IRepository<TR_Payment> _trPayment;
        private readonly IRepository<TR_PaymentDetailEDC> _trPaymentDetailEDC;
        private readonly IRepository<TR_PPOrder> _trPPOrder;
        private readonly PPOnlineDbContext _PPOnlinecontext;
        private readonly PersonalsNewDbContext _personalsContext;
        private readonly PropertySystemDbContext _propertySystemContext;
        private readonly ICustomerMemberAppService _customerMemberAppService;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly FilesHelper _fileHelper;
        private readonly IAccountAppService _accountAppService;
        private readonly IRepository<MP_UserPersonals> _mpUserPersonals;
        private readonly IRepository<User, long> _User;
        private readonly IRepository<MS_UserRegBy> _msUserRegBy;
        private readonly DemoDbContext _demoDbContext;
        
        public CustomerPPAppService
        (
            IRepository<LK_PaymentType> lkPaymentType,
            IRepository<MS_Projects> msProject,
            IRepository<TR_Payment> trPayment,
            IRepository<TR_PaymentDetailEDC> trPaymentDetailEDC,
            IRepository<TR_PPOrder> trPPOrder,
            PPOnlineDbContext PPOnlinecontext,
            PersonalsNewDbContext personalsContext,
            PropertySystemDbContext propertySystemContext,
            ICustomerMemberAppService customerMemberAppService,
            IHostingEnvironment hostingEnvironment,
            FilesHelper fileHelper,
            IAccountAppService accountAppService,
            IRepository<MP_UserPersonals> mpUserPersonals,
            IRepository<User, long> User,
            IRepository<MS_UserRegBy> msUserRegBy,
            DemoDbContext demoDbContext
        )
        {
            _lkPaymentType = lkPaymentType;
            _msProject = msProject;
            _trPayment = trPayment;
            _trPaymentDetailEDC = trPaymentDetailEDC;
            _trPPOrder = trPPOrder;
            _PPOnlinecontext = PPOnlinecontext;
            _personalsContext = personalsContext;
            _propertySystemContext = propertySystemContext;
            _customerMemberAppService = customerMemberAppService;
            _hostingEnvironment = hostingEnvironment;
            _fileHelper = fileHelper;
            _accountAppService = accountAppService;
            _mpUserPersonals = mpUserPersonals;
            _User = User;
            _msUserRegBy = msUserRegBy;
            _demoDbContext = demoDbContext;
        }

        [UnitOfWork(isTransactional: false)]
        public async Task<SignUpCustomerResultDto> RegisterCusttomer(SignUpCustomerInputDto input)
        {
            var getPsCode = _customerMemberAppService.SignUpCustomer(input);

            try
            {
                Random random = new Random();
                int length = 8;
                string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                var password = new string (Enumerable.Repeat(chars, length).Select(s => s[random.Next(s.Length)]).ToArray());
                
                var inputAccount = new RegisterCustomerInput
                {
                    name = input.name,
                    email = input.email,
                    password = password
                };

                await _accountAppService.RegisterCustomer(inputAccount);

                var getuserID = (from x in _User.GetAll()
                                 where x.UserName == input.email && x.EmailAddress == input.email
                                 select new
                                 {
                                     x.Id,
                                     x.CreatorUserId
                                 }).FirstOrDefault();

                var insertUserPersonals = new MP_UserPersonals
                {
                    psCode = getPsCode.Result.psCode,
                    userID = getuserID.Id,
                    userType = 2,
                    activeDate = null
                };
                _mpUserPersonals.Insert(insertUserPersonals);

                var insertUserReg = new MS_UserRegBy
                {
                    userID = getuserID.Id,
                    userRegBy = (long)getuserID.CreatorUserId
                };
                _msUserRegBy.Insert(insertUserReg);

                return new SignUpCustomerResultDto { psCode = getPsCode.Result.psCode };
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }
        }

        public List<GetListCustomerResultDto> GetListCustomerPP(GetListCustomerInputDto input)
        {
            var getCustomer = (from user in _demoDbContext.MP_UserPersonals.ToList()
                               join userReg in _demoDbContext.MS_UserRegBy.ToList() on user.userID equals userReg.userID
                               join personal in _personalsContext.PERSONAL on user.psCode equals personal.psCode
                               join address in _personalsContext.TR_Address on personal.psCode equals address.psCode into addr
                               from address in addr.DefaultIfEmpty()
                               join phone in _personalsContext.TR_Phone on personal.psCode equals phone.psCode into num
                               from phone in num.DefaultIfEmpty()
                               join email in _personalsContext.TR_Email on personal.psCode equals email.psCode into em
                               from email in em.DefaultIfEmpty()
                               join usr in _demoDbContext.Users.ToList() on user.CreatorUserId equals usr.Id
                               join member in _personalsContext.PERSONALS_MEMBER on usr.UserName equals member.memberCode
                               join ps in _personalsContext.PERSONAL on member.psCode equals ps.psCode
                               where address.refID == 1 && phone.phoneType == "3"
                               select new GetListCustomerResultDto
                               {
                                   activeDate = user.activeDate,
                                   address = address.address,
                                   birthInfo = personal.birthPlace + ", " + String.Format("{0: dd MMM yyyy}", personal.birthDate),
                                   city = address.city,
                                   customerCode = personal.psCode,
                                   customerName = personal.name,
                                   email = email.email,
                                   phoneNumber = phone.number,
                                   accountStatus = user.activeDate.ToString() == "" ? "Inactive" : "Active",
                                   memberCode = member.memberCode,
                                   memberName = ps.name,
                                   NPWP = personal.NPWP,
                                   regDate = String.Format("{0: dd MMM yyyy}", personal.CreationTime)
                               }).Distinct().ToList();

            if(getCustomer.Count() != 0)
            {
                if(input.customerName == null && input.customerName == "" && input.keyword == null && input.keyword == "" && input.member == null && input.member == "")
                {
                    if(getCustomer.Count >= 100)
                    {
                        getCustomer.Take(100).ToList();
                    }
                }
                else
                {
                    if(input.customerName != null)
                    {
                        getCustomer = (from data in getCustomer
                                       where data.customerName == input.customerName
                                       select data).ToList();
                    }
                    if(input.member != null)
                    {
                        getCustomer = (from data in getCustomer
                                       where data.memberName.Contains(input.member) || data.memberCode.Contains(input.member)
                                       select data).ToList();
                    }
                    if(input.keyword != null)
                    {
                        getCustomer = (from data in getCustomer
                                       where data.NPWP.Contains(input.keyword) || data.customerCode.Contains(input.keyword)
                                             || data.customerName.Contains(input.keyword) || data.address.Contains(input.keyword)
                                             || data.phoneNumber.Contains(input.keyword)
                                       select data).ToList();
                    }
                }
            }

            return getCustomer;
        }

        [HttpPost]
        [UnitOfWork(isTransactional: false)]
        public async Task<ResultMessageDto> UpdateCustomerPP(UpdateCustomerInputDto input)
        {
            await _customerMemberAppService.UpdateCustomer(input);

            try
            {
                var getEmail = (from user in _demoDbContext.Users
                                where user.UserName == input.email && user.EmailAddress == input.email
                                select user).FirstOrDefault();

                if (getEmail == null)
                {
                    var getUserPersonal = (from up in _demoDbContext.MP_UserPersonals
                                           where up.psCode == input.psCode
                                           select up).FirstOrDefault();

                    var user = getUserPersonal.Id;

                    if (getUserPersonal != null)
                    {
                        Random random = new Random();
                        int length = 8;
                        string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                        var password = new string(Enumerable.Repeat(chars, length).Select(s => s[random.Next(s.Length)]).ToArray());

                        var inputAccount = new RegisterCustomerInput
                        {
                            name = input.name,
                            email = input.email,
                            password = password
                        };

                        await _accountAppService.RegisterCustomer(inputAccount);

                        var getuserID = (from x in _User.GetAll()
                                         where x.UserName == input.email && x.EmailAddress == input.email
                                         select new
                                         {
                                             x.Id,
                                             x.CreatorUserId
                                         }).FirstOrDefault();

                        var updateMP = getUserPersonal.MapTo<MP_UserPersonals>();

                        updateMP.userID = getuserID.Id;

                        _mpUserPersonals.Update(updateMP);

                        var getUserReg = (from ug in _demoDbContext.MS_UserRegBy
                                          where ug.userID == user
                                          select ug).FirstOrDefault();

                        var updateUG = getUserReg.MapTo<MS_UserRegBy>();

                        updateUG.userID = getuserID.Id;
                        updateUG.userRegBy = (long)getuserID.CreatorUserId;

                        _msUserRegBy.Update(updateUG);

                    }
                }
                return new ResultMessageDto { result = true, message = "Success" };
            }
            catch(Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }
        }
    }
}
