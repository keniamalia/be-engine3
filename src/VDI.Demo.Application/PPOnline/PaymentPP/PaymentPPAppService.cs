﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.PPOnline.PaymentPP.Dto;
using VDI.Demo.PPOnlineDB;
using VDI.Demo.PropertySystemDB.OnlineBooking.PPOnline;
using System.Linq;
using Abp.UI;
using VDI.Demo.OnlineBooking.Transaction.Dto;
using Abp.AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using VDI.Demo.DataExporting.Pdf.PPOnline;
using Microsoft.AspNetCore.Hosting;
using VDI.Demo.Files;
using System.Globalization;
using VDI.Demo.PPOnline.RefundPP.Dto;
using ZXing.QrCode;
using Abp.Domain.Uow;
using VDI.Demo.Dto;
using VDI.Demo.PPOnline.OrderPP.ExportExcellReportOrder;
using VDI.Demo.PPOnline.OrderPP.Dto;

namespace VDI.Demo.PPOnline.PaymentPP
{
    public class PaymentPPAppService : DemoAppServiceBase, IPaymentPPAppService
    {
        private readonly IRepository<LK_PaymentType> _lkPaymentType;
        private readonly IRepository<MS_Projects> _msProject;
        private readonly IRepository<TR_Payment> _trPayment;
        private readonly IRepository<TR_PaymentDetailEDC> _trPaymentDetailEDC;
        private readonly IRepository<TR_PPOrder> _trPPOrder;
        private readonly PPOnlineDbContext _PPOnlinecontext;
        private readonly PersonalsNewDbContext _personalsContext;
        private readonly PropertySystemDbContext _propertySystemContext;
        private readonly IPPOnlineGeneratePdfExporter _PPonlineGeneratePdf;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly FilesHelper _fileHelper;
        private readonly IGenerateReportOrderExcelExporter _generateExcel;

        public PaymentPPAppService
        (
            IRepository<LK_PaymentType> lkPaymentType,
            IRepository<MS_Projects> msProject,
            IRepository<TR_Payment> trPayment,
            IRepository<TR_PaymentDetailEDC> trPaymentDetailEDC,
            IRepository<TR_PPOrder> trPPOrder,
            PPOnlineDbContext PPOnlinecontext,
            PersonalsNewDbContext personalsContext,
            PropertySystemDbContext propertySystemContext,
            IPPOnlineGeneratePdfExporter PPonlineGeneratePdf,
            IHostingEnvironment hostingEnvironment,
            FilesHelper fileHelper,
            IGenerateReportOrderExcelExporter generateExcel

        )
        {
            _lkPaymentType = lkPaymentType;
            _msProject = msProject;
            _trPayment = trPayment;
            _trPaymentDetailEDC = trPaymentDetailEDC;
            _trPPOrder = trPPOrder;
            _PPOnlinecontext = PPOnlinecontext;
            _personalsContext = personalsContext;
            _propertySystemContext = propertySystemContext;
            _PPonlineGeneratePdf = PPonlineGeneratePdf;
            _hostingEnvironment = hostingEnvironment;
            _fileHelper = fileHelper;
            _generateExcel = generateExcel;
        }

        public List<GetListPaymentTypeResultDto> GetListPaymentType()
        {
            var getPaymentType = (from lpt in _lkPaymentType.GetAll()
                                  select new GetListPaymentTypeResultDto
                                  {
                                      paymentType = lpt.paymentType,
                                      paymentTypeName = lpt.paymentTypeName
                                  }).Distinct().ToList();

            if(getPaymentType.Count() != 0)
            {
                return getPaymentType;
            }
            else
            {
                throw new UserFriendlyException("Data is not exist");
            }
        }

        public List<GetListProjectResultDto> GetListProject()
        {
            var getProject = (from mp in _msProject.GetAll()
                              select new GetListProjectResultDto
                              {
                                  projectID = mp.Id,
                                  projectName = mp.projectName
                              }).Distinct().ToList();

            if (getProject.Count() != 0)
            {
                return getProject;
            }
            else
            {
                throw new UserFriendlyException("Data is not exist");
            }
        }

        [HttpPost]
        public List<GetListHistoryPaymentResultDto> GetListPaymentHistory(GetListPaymentHistoryInputDto input)
        {
            var getPaymentHistory = (from trp in _PPOnlinecontext.TR_Payment
                                     join trpd in _PPOnlinecontext.TR_PaymentDetailEDC on trp.Id equals trpd.paymentID into detail
                                     from trpd in detail.DefaultIfEmpty()
                                     join trppo in _PPOnlinecontext.TR_PPOrder on trp.PPOrderID equals trppo.Id
                                     join msp in _PPOnlinecontext.MS_Projects on trppo.projectID equals msp.Id
                                     join trppop in _PPOnlinecontext.TR_PPOrderPPNo on trppo.Id equals trppop.PPOrderID
                                     join trpp in _PPOnlinecontext.TR_PriorityPass on trppop.PPNo equals trpp.PPNo
                                     join los in _PPOnlinecontext.LK_OrderStatus on trppo.orderStatusID equals los.Id
                                     select new GetListHistoryPaymentResultDto
                                     {
                                         orderStatusID = trppo.orderStatusID,
                                         projectID = trppo.projectID,
                                         orderCode = trppo.orderCode,
                                         projectName = msp.projectName,
                                         TID = trpd.TID,
                                         MID = trpd.MID,
                                         approvalCode = trpd.approvalCode,
                                         bankBranch = trp.bankBranch,
                                         bankAccNo = trp.bankAccNo,
                                         qty = trppo.qty,
                                         paymentAmt = trp.paymentAmt,
                                         paymentID = trp.Id,
                                         psCode = trpp.psCode,
                                         paymentType = trp.paymentType,
                                         orderStatus = los.orderStatusName,
                                         PPOrderID = trppo.Id
                                     }).ToList();

            var getData = (from data in getPaymentHistory
                           join personal in _personalsContext.PERSONAL on data.psCode equals personal.psCode
                           select new GetListHistoryPaymentResultDto
                           {
                               orderStatusID = data.orderStatusID,
                               paymentType = data.paymentType,
                               projectID = data.projectID,
                               orderCode = data.orderCode,
                               projectName = data.projectName,
                               TID = data.TID,
                               MID = data.MID,
                               approvalCode = data.approvalCode,
                               paymentTypeName = data.paymentTypeName,
                               bankAccNo = data.bankAccNo,
                               bankBranch = data.bankBranch,
                               qty = data.qty,
                               customerName = personal.name,
                               orderStatus = data.orderStatus,
                               paymentID = data.paymentID,
                               PPOrderID = data.PPOrderID
                           }).Distinct().ToList();

            var getPayment = (from data in getData
                              join lpt in _propertySystemContext.LK_PaymentType on data.paymentType equals lpt.paymentType
                              select new GetListHistoryPaymentResultDto
                              {
                                  orderStatusID = data.orderStatusID,
                                  paymentType = data.paymentType,
                                  projectID = data.projectID,
                                  orderCode = data.orderCode,
                                  projectName = data.projectName,
                                  TID = data.TID,
                                  MID = data.MID,
                                  approvalCode = data.approvalCode,
                                  paymentTypeName = lpt.paymentTypeName,
                                  bankAccNo = data.bankAccNo,
                                  bankBranch = data.bankBranch,
                                  qty = data.qty,
                                  customerName = data.customerName,
                                  paymentID = data.paymentID,
                                  orderStatus = data.orderStatus,
                                  PPOrderID = data.PPOrderID
                              }).Distinct().ToList();


            if (getPayment.Count() != 0)
            {
                if(input.projectID == null && input.projectID == 0 && input.orderStatusID == null && input.orderStatusID == 0 && input.paymentType == null && input.paymentType == 0 && input.keyword == null)
                {
                    if(getPayment.Count() >= 100)
                    {
                        getPayment.Take(100).ToList();
                    }
                }
                else
                {
                    if (input.projectID != null && input.projectID != 0)
                    {
                        getPayment = (from data in getPayment
                                      where data.projectID == input.projectID
                                      select data).Distinct().ToList();
                    }
                    if (input.orderStatusID != null && input.orderStatusID != 0)
                    {
                        getPayment = (from data in getPayment
                                      where data.orderStatusID == input.orderStatusID
                                      select data).Distinct().ToList();
                    }
                    if (input.paymentType != null && input.paymentType != 0)
                    {
                        getPayment = (from data in getPayment
                                      where data.paymentType == input.paymentType
                                      select data).Distinct().ToList();
                    }
                    if (input.keyword != null && input.keyword != "")
                    {
                        getPayment = (from data in getPayment
                                      where data.customerName.Contains(input.keyword) || data.orderCode.Contains(input.keyword)
                                            || data.projectName.Contains(input.keyword) || data.TID.Contains(input.keyword)
                                            || data.MID.Contains(input.keyword) || data.TID.Contains(input.keyword)
                                      select data).Distinct().ToList();
                    }

                }
                return getPayment;
            }
            else
            {
                throw new UserFriendlyException("Data is not exist");
            }
            
        }

        public List<GetLIstOrderStatusResultDto> GetListOrderStatus()
        {
            var getStatus = (from los in _PPOnlinecontext.LK_OrderStatus
                             where new[] { 1, 2 }.Contains(los.orderStatus)
                             select new GetLIstOrderStatusResultDto
                             {
                                 orderStatusID = los.Id,
                                 orderStatusName = los.orderStatusName
                             }).ToList();

            if(getStatus.Count() != 0)
            {
                return getStatus;
            }
            else
            {
                throw new UserFriendlyException("Data is not exist");
            }
        }

        public GetDetailPaymentInfoResultDto GetDetailPaymentInfo(GetDetailPaymentInfoInputtDto input)
        {
            var getHistory = (from trp in _PPOnlinecontext.TR_Payment
                             join lkb in _PPOnlinecontext.LK_Bank on trp.bankID equals lkb.Id
                             join trppo in _PPOnlinecontext.TR_PPOrder on trp.PPOrderID equals trppo.Id
                             join msp in _PPOnlinecontext.MS_Projects on trppo.projectID equals msp.Id
                             where trp.PPOrderID == input.PPOrderID
                             select new GetPaymentHistory
                             {
                                 orderCode = trppo.orderCode,
                                 projectName = msp.projectName,
                                 bankName = lkb.bankName,
                                 paymentID = trp.Id,
                                 paymentType = trp.paymentType,
                                 PPOrderID = trppo.Id,
                                 amount = trp.paymentAmt
                             }).ToList();

            var getPayment = (from data in getHistory
                              join lkp in _propertySystemContext.LK_PaymentType on data.paymentType equals lkp.paymentType
                              select new GetPaymentHistory
                              {
                                  orderCode = data.orderCode,
                                  paymentTypeName = lkp.paymentTypeName,
                                  paymentType = data.paymentType,
                                  paymentID = data.paymentID,
                                  bankName = data.bankName,
                                  projectName = data.projectName,
                                  PPOrderID = data.PPOrderID,
                                  amount = data.paymentID
                              }).ToList();

            var getDetail = (from data in getPayment
                             join trp in _PPOnlinecontext.TR_Payment on data.paymentID equals trp.Id
                             join trppo in _PPOnlinecontext.TR_PPOrder on trp.PPOrderID equals trppo.Id
                             join los in _PPOnlinecontext.LK_OrderStatus on trppo.orderStatusID equals los.Id
                             where trp.Id == input.paymentID
                             select new GetDetailPaymentInfoResultDto
                             {
                                 orderCode = data.orderCode,
                                 projectName = data.projectName,
                                 customerName = input.customerName,
                                 qty = input.qty,
                                 paymentDate = trp.paymentDate,
                                 bankName = data.bankName,
                                 paymentTypeName = data.paymentTypeName,
                                 location = trp.bankBranch,
                                 accountName = trp.bankAccName,
                                 accountNo = trp.bankAccNo,
                                 orderStatusName = los.orderStatusName,
                                 paymentHistory = getPayment,
                                 uploadReceipt = _fileHelper.getAbsoluteUri() + trp.docFile,
                             }).FirstOrDefault();

            var getAmount = (from trp in _PPOnlinecontext.TR_Payment
                             join trpo in _PPOnlinecontext.TR_PPOrder on trp.PPOrderID equals trpo.Id
                             join trppo in _PPOnlinecontext.TR_PPOrderPPNo on trpo.Id equals trppo.PPOrderID
                             join trpp in _PPOnlinecontext.TR_PriorityPass on trppo.PPNo equals trpp.PPNo
                             join msb in _PPOnlinecontext.MS_BatchEntry on trpp.batchID equals msb.Id
                             where trp.PPOrderID == input.PPOrderID
                             group msb by new 
                             {
                                 trp.Id
                             } into G
                             select new GetDetailPaymentInfoResultDto
                             {
                                 totalAmount = G.Sum(a => a.priorityPassPrice)
                             }).FirstOrDefault();

            getDetail.totalAmount = getAmount.totalAmount;

            var getPaidAmt = (from trp in _PPOnlinecontext.TR_Payment
                              where trp.PPOrderID == input.PPOrderID
                              group trp by new
                              {
                                  trp.PPOrderID
                              } into G
                              select new GetDetailPaymentInfoResultDto
                              {
                                  paidAmount = G.Sum(a => a.paymentAmt)
                              }).FirstOrDefault();

            getDetail.paidAmount = getPaidAmt.paidAmount;

            getDetail.outstanding = getDetail.totalAmount - (decimal)getDetail.paidAmount;
            
            if(getDetail != null)
            {

                return getDetail;
            }
            else
            {
                throw new UserFriendlyException("Data is not exist");
            }

        }

        public ResultMessageDto InputPaymentOffline(InsertPaymentOfflineInputDto input)
        {
            var fileUrl = "";

            if (input.docFile != null && input.docFile != "")
            {
                fileUrl = UploadReceipt(input.docFile);

                Logger.InfoFormat(" ---FileUrl : " + fileUrl);
            }

            var dataPayment = new TR_Payment
            {
                paymentAmt = input.paymentAmt,
                bankAccName = input.accountName,
                bankAccNo = input.accountNo,
                bankBranch = input.location,
                bankID = input.bankID,
                paymentDate = Convert.ToDateTime(input.paymentDate),
                paymentType = input.paymentType,
                PPOrderID = input.PPOrderID,
                docFile = fileUrl
            };
            
            _trPayment.Insert(dataPayment);

            try
            {
                var getData = (from trppo in _PPOnlinecontext.TR_PPOrder
                               join trp in _PPOnlinecontext.TR_Payment on trppo.Id equals trp.PPOrderID
                               where trppo.Id == input.PPOrderID
                               select new
                               {
                                   trp.Id,
                                   trp.paymentAmt
                               }).Distinct().ToList();

                double totalPayment;

                if (getData.Count() != 0)
                {
                    totalPayment = getData.Sum(a => a.paymentAmt);

                    totalPayment = totalPayment + dataPayment.paymentAmt;

                }
                else
                {
                    totalPayment = dataPayment.paymentAmt;
                }

                var getPPPrice = (from trppo in _PPOnlinecontext.TR_PPOrder
                                  join trppn in _PPOnlinecontext.TR_PPOrderPPNo on trppo.Id equals trppn.PPOrderID
                                  join trpp in _PPOnlinecontext.TR_PriorityPass on trppn.PPNo equals trpp.PPNo
                                  join msb in _PPOnlinecontext.MS_BatchEntry on trpp.batchID equals msb.Id
                                  where trppo.Id == input.PPOrderID
                                  select new
                                  {
                                      msb.priorityPassPrice
                                  }).Distinct().ToList();

                var totalPPPrice = (decimal?)0.00;

                if (getPPPrice.Count() != 0)
                {
                    totalPPPrice = getPPPrice.Sum(a => a.priorityPassPrice);
                }

                if (totalPPPrice == (decimal)totalPayment)
                {
                    var getStatus = (from lks in _PPOnlinecontext.LK_OrderStatus
                                     where lks.orderStatus == 2
                                     select lks.Id).FirstOrDefault();

                    var getPPOrder = (from trppo in _PPOnlinecontext.TR_PPOrder
                                      where trppo.Id == input.PPOrderID
                                      select trppo).FirstOrDefault();

                    var updateOrderStatus = getPPOrder.MapTo<TR_PPOrder>();

                    updateOrderStatus.orderStatusID = getStatus;

                    _trPPOrder.Update(updateOrderStatus);
                }

                return new ResultMessageDto { message = "Success", result = true };
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }
            
        }

        private string UploadReceipt(string logoName)
        {
            try
            {
                var newPath = _fileHelper.MoveFiles(logoName, @"Temp\Downloads\UploadReceipt\", @"Assets\Upload\PPOnline\Payment\UploadReceipt\");

                string result = @"Assets\Upload\PPOnline\Payment\UploadReceipt\m-" + logoName;
                return result;
            }
            catch (Exception ex)
            {
                Logger.DebugFormat("test() - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Error : {0}", ex.Message);

            }
        }

        public GenerateTTBFResultDto PrintTTBF(int PPOrderID)
        {
            var getData = (from trp in _PPOnlinecontext.TR_Payment
                           join trppo in _PPOnlinecontext.TR_PPOrder on trp.PPOrderID equals trppo.Id
                           join trppopp in _PPOnlinecontext.TR_PPOrderPPNo on trppo.Id equals trppopp.PPOrderID
                           join msp in _PPOnlinecontext.MS_Projects on trppo.projectID equals msp.Id
                           join trpp in _PPOnlinecontext.TR_PriorityPass on trppopp.PPNo equals trpp.PPNo
                           join msb in _PPOnlinecontext.MS_BatchEntry on trpp.batchID equals msb.Id
                           where trppo.Id == PPOrderID
                           select new
                           {
                               trppo.orderCode,
                               trppopp.PPNo,
                               trp.paymentType,
                               msp.banner,
                               msp.termAndCondition,
                               trpp.memberCode,
                               trpp.psCode,
                               msb.priorityPassPrice
                           }).Distinct().ToList();

            var result = new List<GenerateTTBFResultDto>();
            
            var dataTTBF = new ListTTBFInputDto();
            
            var file = "";
            
            var listTTBF = new List<TTBFInputDto>();

            var formatTanggal = String.Format("{0: yyyy/MM/dd}", DateTime.Now);

            formatTanggal = formatTanggal.Replace("/", "");

            formatTanggal = formatTanggal.Remove(0, 3);

            var formatWaktu = String.Format("{0: hh:mm:ss.fff}", DateTime.Now);

            formatWaktu = formatWaktu.Replace(":", "");

            formatWaktu = formatWaktu.Replace(".", "");

            var name = "TTBF_" + getData.FirstOrDefault().orderCode + "_" + formatTanggal + formatWaktu;

            foreach (var item in getData)
            {
                var dto = new List<GenerateTTBFInputDto>();

                var getPersonals = (from personal in _personalsContext.PERSONAL
                                    join trid in _personalsContext.TR_ID on personal.psCode equals trid.psCode into id
                                    from trid in id.DefaultIfEmpty()
                                    join tre in _personalsContext.TR_Email on personal.psCode equals tre.psCode into email
                                    from tre in email.DefaultIfEmpty()
                                    join trp in _personalsContext.TR_Phone on personal.psCode equals trp.psCode into phone
                                    from trp in phone.DefaultIfEmpty()
                                    where personal.psCode == item.psCode
                                    select new
                                    {
                                        personal.name,
                                        trid.idNo,
                                        trp.number,
                                        tre.email
                                    }).FirstOrDefault();

                var getMember = (from member in _personalsContext.PERSONALS_MEMBER
                                 join ps in _personalsContext.PERSONAL on member.psCode equals ps.psCode
                                 where member.memberCode == item.memberCode
                                 select ps.name).FirstOrDefault();

                var getPayementType = (from lkp in _propertySystemContext.LK_PaymentType
                                       where lkp.paymentType == item.paymentType
                                       select lkp.paymentTypeName).FirstOrDefault();

                CultureInfo ID = CultureInfo.CreateSpecificCulture("id-ID");
                var price = String.Format(ID, "{0:0,0}", item.priorityPassPrice);
                
                var getFileBarcode = GenerateBarcode(item.PPNo);

                getFileBarcode = Path.Combine(getFileBarcode);

                var imageBarcode = "";
 
                if (File.Exists(getFileBarcode))
                {
                    var files = File.ReadAllBytes(getFileBarcode);

                    imageBarcode = Convert.ToBase64String(files);
                    
                    File.Delete(getFileBarcode);
                }
                else
                {
                    imageBarcode = "-";
                }

                var pathBanner = Path.Combine(_hostingEnvironment.WebRootPath, item.banner == null ? ".jpg" : item.banner);

                var imageBanner = "";

                if (File.Exists(pathBanner))
                {
                    var files = File.ReadAllBytes(pathBanner);

                    imageBanner = Convert.ToBase64String(files);
                }
                else
                {
                    imageBanner = "-";
                }

                var pathTNC = Path.Combine(_hostingEnvironment.WebRootPath, item.termAndCondition == null ? ".jpg" : item.termAndCondition);

                var imageTNC = "";

                if (File.Exists(pathTNC))
                {
                    var files = File.ReadAllBytes(pathTNC);

                    imageTNC = Convert.ToBase64String(files);
                }
                else
                {
                    imageTNC = "-";
                }
                
                var data = new GenerateTTBFInputDto
                {
                    banner = imageBanner,
                    customerName = getPersonals.name,
                    email = getPersonals.email,
                    idNo = getPersonals.idNo,
                    memberCode = item.memberCode,
                    memberName = getMember,
                    metodePembayaran = getPayementType == null ? "-" : getPayementType,
                    noHp = getPersonals.number,
                    PPNO = item.PPNo,
                    PPPrice = price,
                    TNC = imageTNC,
                    tanggal = String.Format("{0: dd MMM yyyy}", DateTime.Now),
                    barcode = imageBarcode
                };

                dto.Add(data);

                foreach (var mbo in dto)
                {
                    var laaa = new TTBFInputDto
                    {
                        dto = mbo
                    };

                    listTTBF.Add(laaa);
                }
                
            }


            dataTTBF.inputDto = listTTBF;

            dataTTBF.name = name.Replace(" ", "");

            file = GenerateTTBFPP(dataTTBF);

            var fileP = Path.Combine(_fileHelper.getAbsoluteUri(), file);

            var path = Path.Combine(_hostingEnvironment.WebRootPath, file);

            return new GenerateTTBFResultDto { url = fileP, path = path};
        }

        private string GenerateTTBFPP(ListTTBFInputDto input)
        {
            var destinationPath = Path.Combine(_hostingEnvironment.WebRootPath, @"Assets\Upload\PPOnline\TTBF");
            var filePath = _hostingEnvironment.WebRootPath + @"\Assets\HtmlTemplates\PPOnline\TTBF.html";
            
            string html = File.ReadAllText(filePath);
            var filePathResult = _PPonlineGeneratePdf.PrintTTBFGeneratePdf(html, input, destinationPath, @"Assets\Upload\PPOnline\TTBF");

            return filePathResult;
        }

        public List<GetListDropdownBankResultDto> GetListDropdownBank()
        {
            var getBank = (from lkb in _PPOnlinecontext.LK_Bank
                           select new GetListDropdownBankResultDto
                           {
                               bankID = lkb.Id,
                               bankName = lkb.bankName
                           }).ToList();

            try
            {
                return getBank;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Error : {0}", ex.Message);
            }
        }

        public List<GetListPaymentCheckResultDto> GetListPaymentCheck(GetListPaymentCheckInputDto input)
        {
            var getOrder = (from trppo in _PPOnlinecontext.TR_PPOrder
                            join msp in _PPOnlinecontext.MS_Projects on trppo.projectID equals msp.Id
                            join trpop in _PPOnlinecontext.TR_PPOrderPPNo on trppo.Id equals trpop.PPOrderID
                            join trpp in _PPOnlinecontext.TR_PriorityPass on trpop.PPNo equals trpp.PPNo
                            join msb in _PPOnlinecontext.MS_BatchEntry on trpp.batchID equals msb.Id
                            join los in _PPOnlinecontext.LK_OrderStatus on trppo.orderStatusID equals los.Id
                            group msb by new
                            {
                                trppo.orderCode,
                                trppo.Id,
                                msp.projectCode,
                                trppo.qty,
                                trpp.psCode,
                                projectID = msp.Id,
                                los.orderStatusName
                            } into G
                            select new GetListPaymentCheckResultDto
                            {
                                orderCode = G.Key.orderCode,
                                PPOrderID = G.Key.Id,
                                projectCode = G.Key.projectCode,
                                qty = G.Key.qty,
                                psCode = G.Key.psCode,
                                totalAmount = G.Sum(a => a.priorityPassPrice),
                                projectID = G.Key.projectID,
                                orderStatusName = G.Key.orderStatusName
                            }).ToList();

            var getData = (from data in getOrder
                           join personal in _personalsContext.PERSONAL on data.psCode equals personal.psCode
                           select new GetListPaymentCheckResultDto
                           {
                               orderCode = data.orderCode,
                               PPOrderID = data.PPOrderID,
                               projectCode = data.projectCode,
                               qty = data.qty,
                               customerName = personal.name,
                               totalAmount = data.totalAmount,
                               projectID = data.projectID,
                               orderStatusName = data.orderStatusName
                           }).ToList();

            foreach (var item in getData)
            {
                var getPayment = (from trp in _PPOnlinecontext.TR_Payment
                                  where trp.PPOrderID == item.PPOrderID
                                  group trp by new
                                  {
                                      trp.PPOrderID
                                  } into G
                                  select new
                                  {
                                      totalPaid = G.Sum(a => a.paymentAmt),
                                      G.Key.PPOrderID
                                  }).FirstOrDefault();

                if(getPayment != null)
                {
                    item.paidAmount = getPayment.totalPaid;
                }
                else
                {
                    item.paidAmount = 0;
                }

                item.outstanding = item.totalAmount - (decimal)item.paidAmount;
            }
            if(getData.Count != 0)
            {
                if (input.projectID == 0 && input.projectID == null && input.orderCode == null && input.customerName == null)
                {
                    if (getData.Count() >= 100)
                    {
                        getData.Take(100).ToList();
                    }
                }
                else
                {
                    if (input.projectID != 0 && input.projectID != null)
                    {
                        getData = (from data in getData
                                   where data.projectID == input.projectID
                                   select data).ToList();
                    }
                    if (input.orderCode != null && input.orderCode != "")
                    {
                        getData = (from data in getData
                                   where data.orderCode.Contains(input.orderCode)
                                   select data).ToList();
                    }
                    if (input.customerName != null && input.customerName != "")
                    {
                        getData = (from data in getData
                                   where data.customerName.Contains(input.customerName)
                                   select data).ToList();
                    }
                }
                
                return getData;
            }
            else
            {
                throw new UserFriendlyException("Data not found");
            }
            
        }

        public string GenerateBarcode(string input)
        {
            var qrCodeWriter = new ZXing.BarcodeWriterPixelData
            {
                Format = ZXing.BarcodeFormat.CODE_128,
                Options = new QrCodeEncodingOptions
                {
                    Height = 100,
                    Width = 250,
                    Margin = 10
                }
            };

            var pixelData = qrCodeWriter.Write(input);

            var filePath = "";

            // creating a bitmap from the raw pixel data; if only black and white colors are used it makes no difference   
            // that the pixel data ist BGRA oriented and the bitmap is initialized with RGB   
            using (var bitmap = new System.Drawing.Bitmap(pixelData.Width, pixelData.Height, System.Drawing.Imaging.PixelFormat.Format32bppRgb))

            using (var ms = new MemoryStream())
            {
                var bitmapData = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, pixelData.Width, pixelData.Height), System.Drawing.Imaging.ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format32bppRgb);
                try
                {
                    // we assume that the row stride of the bitmap is aligned to 4 byte multiplied by the width of the image   
                    System.Runtime.InteropServices.Marshal.Copy(pixelData.Pixels, 0, bitmapData.Scan0, pixelData.Pixels.Length);
                }
                finally
                {
                    bitmap.UnlockBits(bitmapData);
                }
                
                var oldPath = $"C:/barcode/";

                if (!Directory.Exists(oldPath))
                {
                    Directory.CreateDirectory(oldPath);
                }

                var filename = input +".png";

                // save to stream as PNG   
                bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                bitmap.Save(oldPath + filename);

                var newPath = _fileHelper.MoveFilesBarcode(filename, oldPath, @"Assets\Upload\PPOnline\Payment\Barcode\");

                filePath = newPath;
            }

            return filePath;
        }
        
        [HttpPost]
        [UnitOfWork(isTransactional: false)]
        public List<GetReportPaymentResultDto> GetReportPayment(GetReportPaymentInputDto input)
        {
            var getPayment = (from pay in _PPOnlinecontext.TR_Payment
                              join order in _PPOnlinecontext.TR_PPOrder on pay.PPOrderID equals order.Id
                              join orderpp in _PPOnlinecontext.TR_PPOrderPPNo on order.Id equals orderpp.PPOrderID
                              join pp in _PPOnlinecontext.TR_PriorityPass on orderpp.PPNo equals pp.PPNo
                              join personal in _personalsContext.PERSONAL.ToList() on pp.psCode equals personal.psCode
                              join payType in _propertySystemContext.LK_PaymentType.ToList() on pay.paymentType equals payType.paymentType
                              join bank in _PPOnlinecontext.LK_Bank on pay.bankID equals bank.Id
                              join orstatus in _PPOnlinecontext.LK_OrderStatus on order.orderStatusID equals orstatus.Id
                              join member in _personalsContext.PERSONALS_MEMBER.ToList() on pp.memberCode equals member.memberCode
                              join ps in _personalsContext.PERSONAL.ToList() on member.psCode equals ps.psCode
                              select new GetReportPaymentResultDto
                              {
                                  year = String.Format("{0:yyyy}", pay.paymentDate),
                                  yearMonth = String.Format("{0:MMM yyyy}", pay.paymentDate),
                                  accName = pay.bankAccName,
                                  accNo = pay.bankAccNo,
                                  bankBranch = pay.bankBranch,
                                  bankName = bank.bankName,
                                  customerName = personal.name,
                                  orderCode = order.orderCode,
                                  paymentStatus = orstatus.orderStatusName,
                                  PPNo = pp.PPNo,
                                  amount = pay.paymentAmt,
                                  paymentDate = pay.paymentDate,
                                  clearDate = pay.clearDate,
                                  orderDate = order.CreationTime,
                                  bankID = pay.bankID,
                                  paymentStatusID = order.orderStatusID,
                                  paymentTypeName = payType.paymentTypeName,
                                  paymentType = pay.paymentType,
                                  memberName = ps.name
                              }).Distinct().ToList();

            try
            {
                var payDateFrom = Convert.ToDateTime(input.paymentDateFrom);

                var payDateTo = Convert.ToDateTime(input.paymentDateTo);

                var orderDateFrom = Convert.ToDateTime(input.orderDateFrom);

                var orderDateTo = Convert.ToDateTime(input.orderDateTo);

                var clearDateFrom = Convert.ToDateTime(input.clearDateFrom);

                var clearDateTo = Convert.ToDateTime(input.clearDateTo);

                if (input.paymentDateFrom != null && input.paymentDateTo != null)
                {
                    getPayment = (from data in getPayment
                                  where data.paymentDate >= payDateFrom && data.paymentDate <= payDateTo
                                  select data).ToList();
                }
                if (input.paymentDateFrom == null && input.paymentDateTo != null)
                {
                    getPayment = (from data in getPayment
                                  where data.paymentDate <= payDateTo
                                  select data).ToList();
                }
                if (input.paymentDateTo == null && input.paymentDateFrom != null)
                {
                    getPayment = (from data in getPayment
                                  where data.paymentDate >= payDateFrom
                                  select data).ToList();
                }
                if (input.orderDateFrom != null && input.orderDateTo != null)
                {
                    getPayment = (from data in getPayment
                                  where data.orderDate >= orderDateFrom && data.orderDate <= orderDateTo
                                  select data).ToList();
                }
                if (input.orderDateFrom == null && input.orderDateTo != null)
                {
                    getPayment = (from data in getPayment
                                  where data.orderDate >= orderDateFrom
                                  select data).ToList();
                }
                if (input.orderDateFrom != null && input.orderDateTo == null)
                {
                    getPayment = (from data in getPayment
                                  where data.orderDate <= orderDateTo
                                  select data).ToList();
                }
                if (input.orderStatusID != null)
                {
                    getPayment = (from data in getPayment
                                  where input.orderStatusID.Contains(data.paymentStatusID)
                                  select data).ToList();
                }
                if (input.clearDateFrom != null && input.clearDateTo != null)
                {
                    getPayment = (from data in getPayment
                                  where data.clearDate >= clearDateFrom && data.clearDate <= clearDateTo
                                  select data).ToList();
                }
                if (input.clearDateFrom == null && input.clearDateTo != null)
                {
                    getPayment = (from data in getPayment
                                  where data.clearDate <= clearDateTo
                                  select data).ToList();
                }
                if (input.clearDateFrom != null && input.clearDateTo == null)
                {
                    getPayment = (from data in getPayment
                                  where data.clearDate >= clearDateFrom
                                  select data).ToList();
                }
                if (input.paymentType != null)
                {
                    getPayment = (from data in getPayment
                                  where input.paymentType.Contains(data.paymentType)
                                  select data).ToList();
                }
                if (input.bankID != null)
                {
                    getPayment = (from data in getPayment
                                  where input.bankID.Contains(data.bankID)
                                  select data).ToList();
                }
                if (input.keyword != null)
                {
                    getPayment = (from data in getPayment
                                  where input.keyword.Contains(data.customerName) 
                                     || input.keyword.Contains(data.memberName) 
                                     || input.keyword.Contains(data.orderCode)
                                  select data).ToList();
                }

                getPayment.GroupBy(a => a.year);

                getPayment.GroupBy(a => a.yearMonth);

                getPayment.GroupBy(a => a.paymentStatus);

                return getPayment;
            }
            catch(Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }
        }

        public ResultMessageDto GenerateReportPaymentPdf(GetPaymentReportPdfInputDto input)
        {
            Mse.PDFSharp.PageSize paperSize;

            Mse.PDFSharp.PageOrientation orientation;

            if (input.paperSizeID == 1)
            {
                paperSize = Mse.PDFSharp.PageSize.A4;
            }
            else
            {
                paperSize = Mse.PDFSharp.PageSize.A3;
            }

            if (input.orientationID == 1)
            {
                orientation = Mse.PDFSharp.PageOrientation.Landscape;
            }
            else
            {
                orientation = Mse.PDFSharp.PageOrientation.Portrait;
            }

            string headerHtml = "";

            input.listData.listHeader.OrderBy(a => a.headerID);

            foreach (var item in input.listData.listHeader)
            {
                headerHtml += "<th style='border: 1px solid black;'><strong>" + item.header + "</strong></th>";
            }

            input.listData.headerHtml = headerHtml;

            string bodyHtml = "";

            foreach (var item in input.listData.listBody)
            {
                bodyHtml += "<tr>";

                if (item.year != null)
                {
                    bodyHtml += "<td style='border: 1px solid black;'>" + item.year + "</td>";
                }
                if (item.yearMonth != null)
                {
                    bodyHtml += "<td style='border: 1px solid black;'>" + item.yearMonth + "</td>";
                }
                if (item.paymentStatus != null)
                {
                    bodyHtml += "<td style='border: 1px solid black;'>" + item.paymentStatus + "</td>";
                }
                if (item.paymentDate != null)
                {
                    bodyHtml += "<td style='border: 1px solid black;'>" + String.Format("{0:dd MMM yyyy}", item.paymentDate) + "</td>";
                }
                if (item.clearDate != null)
                {
                    bodyHtml += "<td style='border: 1px solid black;'>" + String.Format("{0:dd MMM yyyy}", item.clearDate) + "</td>";
                }
                if (item.bankName != null)
                {
                    bodyHtml += "<td style='border: 1px solid black;'>" + item.bankName + "</td>";
                }
                if (item.paymentTypeName != null)
                {
                    bodyHtml += "<td style='border: 1px solid black;'>" + item.paymentTypeName + "</td>";
                }
                if (item.orderCode != null)
                {
                    bodyHtml += "<td style='border: 1px solid black;'>" + item.orderCode + "</td>";
                }
                if (item.PPNo != null)
                {
                    bodyHtml += "<td style='border: 1px solid black;'>" + item.PPNo + "</td>";
                }
                if (item.accNo != null)
                {
                    bodyHtml += "<td style='border: 1px solid black;'>" + item.accNo + "</td>";
                }
                if (item.accName != null)
                {
                    bodyHtml += "<td style='border: 1px solid black;'>" + item.accName + "</td>";
                }
                if (item.bankBranch != null)
                {
                    bodyHtml += "<td style='border: 1px solid black;'>" + item.bankBranch + "</td>";
                }
                if (item.amount != null)
                {
                    bodyHtml += "<td style='border: 1px solid black;'>" + item.amount.ToString() + "</td>";

                    input.listData.grandTotal = input.listData.listBody.Sum(a => a.amount);
                }
                else
                {
                    input.listData.grandTotal = 0;
                }

                bodyHtml += "</tr>";
            }

            input.listData.bodyHtml = bodyHtml;

            input.listData.colspanTbl = input.listData.listHeader.Count - 1;

            var destinationPath = Path.Combine(_hostingEnvironment.WebRootPath, @"Assets\Upload\PPOnline\ReportPayment");
            var filePath = _hostingEnvironment.WebRootPath + @"\Assets\HtmlTemplates\PPOnline\ReportOrder.html";

            string html = File.ReadAllText(filePath);
            var filePathResult = _PPonlineGeneratePdf.PrintReportPaymentrPdf(html, input.listData, destinationPath, @"Assets\Upload\PPOnline\ReportPayment", paperSize, orientation);

            return new ResultMessageDto { message = _fileHelper.getAbsoluteUri() + filePathResult, result = true };
        }

        public FileDto GenerateExcelReportOrder(GetPaymentReportPdfInputDto input)
        {
            return _generateExcel.GenerateExcelReportPayment(input);
        }

        public List<GetCheckBoxDisplayedFieldResultDto> GetCheckboxDisplayedFieldPayment()
        {
            var getData = new List<GetCheckBoxDisplayedFieldResultDto>();

            var name = new[] { "Year", "Year & Month", "Payment Status", "Payment Date", "Clear Date",
                               "Bank", "Payment Type", "Order No", "PP No", "Account No",
                               "Account Name", "Bank Branch", "Amount"};

            var displayedID = 1;

            foreach (var item in name)
            {
                var dataDisplay = new GetCheckBoxDisplayedFieldResultDto
                {
                    displayedID = displayedID,
                    displayedName = item
                };

                getData.Add(dataDisplay);

                displayedID++;
            }

            return getData;
        }

    }
}
