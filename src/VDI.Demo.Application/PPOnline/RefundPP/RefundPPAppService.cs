﻿using Abp.Domain.Repositories;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using VDI.Demo.DataExporting.Pdf.PPOnline;
using VDI.Demo.PPOnlineDB;
using System.Linq;
using VDI.Demo.PersonalsDB;
using VDI.Demo.PPOnline.RefundPP.Dto;
using Abp.UI;
using Abp.AutoMapper;
using Abp.Domain.Uow;
using VDI.Demo.Files;
using Newtonsoft.Json;
using VDI.Demo.OnlineBooking.Transaction.Dto;
using Abp.Application.Services.Dto;
using VDI.Demo.EntityFrameworkCore;

namespace VDI.Demo.PPOnline.RefundPP
{
    public class RefundPPAppService : DemoAppServiceBase
    {
        private readonly IPPOnlineGeneratePdfExporter _PPonlineGeneratePdf;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IRepository<TR_PriorityPass> _trPriorityPass;
        private readonly IRepository<LK_PPStatus> _lkPpStatus;
        private readonly IRepository<MS_BatchEntry> _msBatchEntry;
        private readonly IRepository<TR_PriorityPassRefund> _trPriorityPassRefund;
        private readonly IRepository<LK_RefundReason> _lkRefundReason;
        private readonly IRepository<PERSONALS, string> _personals;
        private readonly IRepository<PERSONALS_MEMBER, string> _personalsMember;
        private readonly PPOnlineDbContext _PPOnlinecontext;
        private readonly PersonalsNewDbContext _personalsContext;
        private readonly PropertySystemDbContext _propertySystemContext;
        private readonly FilesHelper _fileHelper;

        public RefundPPAppService
        (
            IPPOnlineGeneratePdfExporter PPOnlineGeneratePdf,
            IHostingEnvironment hostingEnvironment,
            IRepository<TR_PriorityPass> trPriorityPass,
            IRepository<LK_PPStatus> lkPpStatus,
            IRepository<PERSONALS, string> personals,
            IRepository<PERSONALS_MEMBER, string> personalsMember,
            IRepository<MS_BatchEntry> msBatchEntry,
            IRepository<TR_PriorityPassRefund> trPriorityPassRefund,
            IRepository<LK_RefundReason> lkRefundReason,
            PPOnlineDbContext PPOnlinecontext,
            PersonalsNewDbContext personalsContext,
            PropertySystemDbContext propertySystemContext,
            FilesHelper fileHelper
        )
        {
            _PPonlineGeneratePdf = PPOnlineGeneratePdf;
            _hostingEnvironment = hostingEnvironment;
            _trPriorityPass = trPriorityPass;
            _lkPpStatus = lkPpStatus;
            _personals = personals;
            _personalsMember = personalsMember;
            _msBatchEntry = msBatchEntry;
            _trPriorityPassRefund = trPriorityPassRefund;
            _lkRefundReason = lkRefundReason;
            _PPOnlinecontext = PPOnlinecontext;
            _personalsContext = personalsContext;
            _fileHelper = fileHelper;
            _propertySystemContext = propertySystemContext;
        }

        [UnitOfWork(isTransactional: false)]
        public GetDetailPPNoResultDto GetDetailPPNo(string PPNo)
        {
            Logger.InfoFormat("GetDetailPPNo() - Start.");

            Logger.DebugFormat("Param checkPPNo. {0}" +
                               "PPNo         = {1}{0}",
                               Environment.NewLine, PPNo);

            var checkPPNo = (from tpp in _trPriorityPass.GetAll()
                             join lpps in _lkPpStatus.GetAll() on tpp.PPStatusID equals lpps.Id
                             where tpp.PPNo == PPNo && (lpps.PPStatus.ToLowerInvariant() == "n" || lpps.PPStatus.ToLowerInvariant() == "z" || lpps.PPStatus.ToLowerInvariant() == "b")
                             select new
                             {
                                 tpp.Id,
                                 tpp.psCode,
                                 tpp.memberCode,
                                 tpp.batchID,
                                 lpps.PPStatus,
                                 tpp.PPNo,
                                 tpp.scmCode
                             }).ToList();

            if (checkPPNo.FirstOrDefault() != null)
            {
                Logger.InfoFormat("Check PPNo tidak NULL");

                Logger.DebugFormat("Param DetailPPNo. {0}" +
                                   "PPNoID         = {1}{0}",
                               Environment.NewLine, checkPPNo.FirstOrDefault().Id);

                if(checkPPNo.FirstOrDefault().PPStatus == "Z")
                {
                    var getDetailPP = (from ppno in checkPPNo
                                       join customer in _personals.GetAll() on ppno.psCode equals customer.psCode
                                       join psmember in _personalsMember.GetAll() on ppno.memberCode equals psmember.memberCode
                                       join member in _personals.GetAll() on psmember.psCode equals member.psCode
                                       join batch in _msBatchEntry.GetAll() on ppno.batchID equals batch.Id
                                       join refund in _trPriorityPassRefund.GetAll() on ppno.Id equals refund.priorityPassID
                                       where ppno.PPNo == PPNo
                                       select new GetDetailPPNoResultDto
                                       {
                                           customerCode = ppno.psCode,
                                           customerName = customer.name,
                                           memberCode = ppno.memberCode,
                                           memberName = member.name,
                                           PPPrice = batch.priorityPassPrice,
                                           scmCode = ppno.scmCode,
                                           PPNo = ppno.PPNo,
                                           PPNoID = ppno.Id,
                                           namaBank = refund.namaBank,
                                           noRek = refund.noRek,
                                           namaRek = refund.namaRek,
                                           refundReason = refund.refundReasonOthers,
                                           PPStatus = ppno.PPStatus
                                       }).Distinct().FirstOrDefault();

                    return getDetailPP;
                }
                else if(checkPPNo.FirstOrDefault().PPStatus == "N")
                {
                    var getDetailPP = (from ppno in checkPPNo
                                       join customer in _personals.GetAll() on ppno.psCode equals customer.psCode
                                       join psmember in _personalsMember.GetAll() on ppno.memberCode equals psmember.memberCode
                                       join member in _personals.GetAll() on psmember.psCode equals member.psCode
                                       join batch in _msBatchEntry.GetAll() on ppno.batchID equals batch.Id
                                       where ppno.PPNo == PPNo
                                       select new GetDetailPPNoResultDto
                                       {
                                           customerCode = ppno.psCode,
                                           customerName = customer.name,
                                           memberCode = ppno.memberCode,
                                           memberName = member.name,
                                           PPPrice = batch.priorityPassPrice,
                                           PPNo = ppno.PPNo,
                                           PPNoID = ppno.Id,
                                           PPStatus = ppno.PPStatus,
                                           scmCode = ppno.scmCode
                                       }).Distinct().FirstOrDefault();

                    Logger.DebugFormat("Result DetailPPNo. {0}" +
                                   "customerCode    = {1}{0}" +
                                   "customerName    = {2}{0}" +
                                   "memberCode      = {3}{0}" +
                                   "memberName      = {4}{0}" +
                                   "PPPrice         = {5}{0}" +
                                   "scmCode         = {6}{0}" +
                                   "PPNo            = {7}{0}" +
                                   "PPNoID          = {8}{0}",
                                   Environment.NewLine, getDetailPP.customerCode == null ? string.Empty : getDetailPP.customerCode,
                                   getDetailPP.customerName == null ? string.Empty : getDetailPP.customerName,
                                   getDetailPP.memberCode == null ? string.Empty : getDetailPP.memberCode,
                                   getDetailPP.memberName == null ? string.Empty : getDetailPP.memberName,
                                   getDetailPP.PPPrice == 0 ? 0 : getDetailPP.PPPrice, getDetailPP.scmCode == null ? string.Empty : getDetailPP.scmCode,
                                   getDetailPP.PPNo == null ? string.Empty : getDetailPP.PPNo, getDetailPP.PPNoID == 0 ? 0 : getDetailPP.PPNoID);

                    return getDetailPP;
                }
                else
                {
                    var getDetailPP = (from ppno in checkPPNo
                                       join customer in _personals.GetAll() on ppno.psCode equals customer.psCode
                                       join psmember in _personalsMember.GetAll() on ppno.memberCode equals psmember.memberCode
                                       join member in _personals.GetAll() on psmember.psCode equals member.psCode
                                       join batch in _msBatchEntry.GetAll() on ppno.batchID equals batch.Id
                                       where ppno.PPNo == PPNo
                                       select new GetDetailPPNoResultDto
                                       {
                                           customerCode = ppno.psCode,
                                           customerName = customer.name,
                                           memberCode = ppno.memberCode,
                                           memberName = member.name,
                                           PPPrice = batch.priorityPassPrice,
                                           PPNo = ppno.PPNo,
                                           PPNoID = ppno.Id,
                                           PPStatus = ppno.PPStatus,
                                           scmCode = ppno.scmCode
                                       }).Distinct().FirstOrDefault();

                    var getOrder = (from orderDetail in _propertySystemContext.TR_UnitOrderDetail
                                    join bookingHeader in _propertySystemContext.TR_BookingHeader on orderDetail.bookingHeaderID equals bookingHeader.Id
                                    join unit in _propertySystemContext.MS_Unit on bookingHeader.unitID equals unit.Id
                                    join unitCode in _propertySystemContext.MS_UnitCode on unit.unitCodeID equals unitCode.Id
                                    where orderDetail.PPNo == PPNo
                                    select new UnitInfoDto
                                    {
                                        unitCode = unitCode.unitCode == null ? string.Empty : unitCode.unitCode,
                                        unitNo = unit.unitNo == null ? string.Empty : unit.unitNo,
                                        cancelDate = bookingHeader.cancelDate,
                                        bookDate = bookingHeader.bookDate
                                    }).FirstOrDefault();

                    getDetailPP.unitInfo = getOrder;

                    return getDetailPP;
                }
                
            }
            else
            {
                Logger.InfoFormat("Check PPNo NULL");

                throw new UserFriendlyException("Data not found");
            }
        }

        public List<GetListRefundReasonResultDto> GetListRefundReason()
        {
            Logger.InfoFormat("GetListRefundReason(). Start");

            var getReason = (from lrr in _lkRefundReason.GetAll()
                             select new GetListRefundReasonResultDto
                             {
                                 refundReasonID = lrr.Id,
                                 refundReasonName = lrr.refundReasonName
                             }).ToList();

            Logger.InfoFormat("Result Get Reason: " + JsonConvert.SerializeObject(getReason));

            if (getReason.Count != 0)
            {
                return getReason;
            }
            else
            {
                throw new UserFriendlyException("Data not found");
            }
        }

        public ResultMessageDto CreateRefundPP(CreateRefundPPInputDto input)
        {
            Logger.InfoFormat("CreateRefundPP(). Start");

            Logger.DebugFormat("Param CreateRefundPP. {0}" +
                               "PPNoID         = {1}{0}"+
                               "refundReasonID = {2}{0}"+
                               "namaBank       = {3}{0}"+
                               "noRek          = {4}{0}"+
                               "namaRek        = {5}{0}"+
                               "refundReason   = {6}{0}"+
                               "customerName   = {7}{0}"+
                               "customerCode   = {8}{0}"+
                               "memberName     = {9}{0}",
                               Environment.NewLine, input.PPNoID == 0 ? 0 : input.PPNoID,
                               input.refundReasonID == 0 ? 0 : input.refundReasonID,
                               input.namaBank == null ? string.Empty : input.namaBank,
                               input.noRek == null ? string.Empty : input.noRek,
                               input.namaRek == null ? string.Empty : input.namaRek,
                               input.refundReason == null ? string.Empty : input.refundReason,
                               input.customerName == null ? string.Empty : input.customerName,
                               input.customerCode == null ? string.Empty : input.customerCode,
                               input.memberName == null ? string.Empty : input.memberName);

            var pathPR = "";

            if(input.namaBank != null && input.namaRek != null && input.noRek != null && input.PPNoID != 0)
            {
                var isSuccess = false;

                Logger.InfoFormat("isSuccess: False");

                var getStatus = (from tpp in _trPriorityPass.GetAll()
                                 join lpps in _lkPpStatus.GetAll() on tpp.PPStatusID equals lpps.Id
                                 where tpp.Id == input.PPNoID
                                 select new
                                 {
                                     tpp.PPStatusID,
                                     lpps.PPStatus,
                                     tpp.PPNo
                                 }).FirstOrDefault();

                Logger.InfoFormat("getStatus: " + JsonConvert.SerializeObject(getStatus));

                if (getStatus.PPStatus != "N")
                {
                    throw new UserFriendlyException("PP Number is already refunded");
                }

                var getLastRefundRef = (from tppr in _trPriorityPassRefund.GetAll()
                                        orderby tppr.refundRef descending
                                        select tppr.refundRef).FirstOrDefault();

                Logger.InfoFormat("Last RefundRef: " + getLastRefundRef);

                var getReasonName = (from lrr in _lkRefundReason.GetAll()
                                     where lrr.Id == input.refundReasonID
                                     select new
                                     {
                                         lrr.refundReasonName,
                                         lrr.refundReason
                                     }).FirstOrDefault();

                Logger.DebugFormat("getReasonName {0}" +
                                   "refundReasonName   = {1}{0}"+
                                   "refundReason       = {2}{0}",
                               Environment.NewLine, getReasonName.refundReason == null ? string.Empty : getReasonName.refundReason,
                               getReasonName.refundReasonName == null ? string.Empty : getReasonName.refundReasonName);

                var dataRefund = new TR_PriorityPassRefund
                {
                    refundRef = getLastRefundRef + 1,
                    priorityPassID = input.PPNoID,
                    refundTime = DateTime.Now,
                    namaBank = input.namaBank,
                    namaRek = input.namaRek,
                    noRek = input.noRek,
                    refundReasonID = input.refundReasonID
                };

                if(input.refundReason != null && getReasonName.refundReason == "5")
                {
                    dataRefund.refundReasonOthers = input.refundReason;
                }
                else
                {
                    dataRefund.refundReasonOthers = getReasonName.refundReasonName;
                }

                //Logger.DebugFormat("Data Refund {0}" +
                //                   "refundRef           = {1}{0}" +
                //                   "priorityPassID      = {2}{0}" +
                //                   "namaBank            = {3}{0}" +
                //                   "namaRek             = {4{0}" +
                //                   "noRek               = {5}{0}" +
                //                   "refundReasonID      = {6}{0}" +
                //                   "refundReasonOthers  = {7}{0}",
                //               Environment.NewLine, dataRefund.refundRef == 0 ? 0 : dataRefund.refundRef,
                //               dataRefund.priorityPassID == 0 ? 0 : dataRefund.priorityPassID,
                //               dataRefund.namaBank == null ? string.Empty : dataRefund.namaBank,
                //               dataRefund.namaRek == null ? string.Empty : dataRefund.namaRek,
                //               dataRefund.noRek == null ? string.Empty : dataRefund.noRek,
                //               dataRefund.refundReasonID == 0 ? 0 : dataRefund.refundReasonID,
                //               dataRefund.refundReasonOthers == null ? string.Empty : dataRefund.refundReasonOthers);

                var refundID = _trPriorityPassRefund.InsertAndGetId(dataRefund);

                isSuccess = true;

                Logger.InfoFormat("isSuccess: true");

                if (isSuccess == true)
                {
                    var getPP = (from tpp in _trPriorityPass.GetAll()
                                 where tpp.Id == input.PPNoID
                                 select tpp).FirstOrDefault();

                    //Logger.InfoFormat("getPP: " + JsonConvert.SerializeObject(getPP));

                    try
                    {
                        var getPPStatus = (from lps in _lkPpStatus.GetAll()
                                           where lps.PPStatus == "Z"
                                           select lps.Id).FirstOrDefault();
                        
                        var updatePP = getPP.MapTo<TR_PriorityPass>();

                        updatePP.PPStatusID = getPPStatus;

                        _trPriorityPass.Update(updatePP);

                        var dataPdf = new GeneratePreviewRefundInputDto
                        {
                            customerName = input.customerName,
                            bankName = input.namaBank,
                            noRek = input.noRek,
                            PPNumber = getStatus.PPNo,
                            refundReason = dataRefund.refundReasonOthers,
                            rekName = input.namaRek,
                            tanggal = String.Format("{0: dd MMM yyyy}", dataRefund.refundTime),
                            tglCetak = String.Format("{0: dd MMM yyyy}", getPP.buyDate),
                            tglTransfer = String.Format("{0: dd MMM yyyy}", getPP.buyDate),
                            noForm = refundID.ToString(),
                            customerCode = input.customerCode,
                            paymentType = getPP.paymentType

                        };

                        //Logger.InfoFormat("Request GeneratePreviewRefund. Start");

                        pathPR = GeneratePreviewRefund(dataPdf);

                        //Logger.InfoFormat("localPath: " + pathPR);

                        pathPR = _fileHelper.getAbsoluteUri() + pathPR;

                        //Logger.InfoFormat("AbsolutePatbh: " + pathPR);

                        Logger.InfoFormat("Request GeneratePreviewRefund. End");
                    }
                    catch (Exception e)
                    {
                        throw new UserFriendlyException(e.Message);
                    }
                }

                Logger.InfoFormat("Request CreateRefund. End");

                return new ResultMessageDto { message = pathPR, result = true };
            }
            else
            {
                Logger.InfoFormat("Request CreateRefund. End");
                throw new UserFriendlyException("Param cannot be null");
            }
        }

        public ResultMessageDto UpdateRefund(int PPNoID)
        {
            var getPPStatus = (from lpps in _lkPpStatus.GetAll()
                               select new
                               {
                                   lpps.Id,
                                   lpps.PPStatus
                               }).ToList();

            var PPStatusZ = getPPStatus.Where(a => a.PPStatus == "Z").Select(a => a.Id).FirstOrDefault();

            var PPStatusN = getPPStatus.Where(b => b.PPStatus == "N").Select(a => a.Id).FirstOrDefault();

            var getPPNo = (from tpp in _trPriorityPass.GetAll()
                           where tpp.Id == PPNoID
                           select tpp).FirstOrDefault();

            var getPPRefund = (from tppr in _trPriorityPassRefund.GetAll()
                               where tppr.priorityPassID == PPNoID
                               select tppr.Id).FirstOrDefault();

            if(getPPNo.PPStatusID != PPStatusZ)
            {
                throw new UserFriendlyException("Your PP status is not deleted");
            }
            else
            {
                try
                {
                    var updateRefund = getPPNo.MapTo<TR_PriorityPass>();

                    updateRefund.PPStatusID = PPStatusN;

                    _trPriorityPass.Update(updateRefund);

                    _trPriorityPassRefund.Delete(getPPRefund);

                    return new ResultMessageDto { message = "Update Success", result = true };
                }
                catch (Exception e)
                {
                    throw new UserFriendlyException(e.Message);
                }
            }
            
        }
        
        private string GeneratePreviewRefund(GeneratePreviewRefundInputDto input)
        {
            var destinationPath = Path.Combine(_hostingEnvironment.WebRootPath, @"Assets\Upload\PPOnline\PreviewRefund");
            var filePath = _hostingEnvironment.WebRootPath + @"\Assets\HtmlTemplates\PPOnline\PreviewRefund.html";
            string html = File.ReadAllText(filePath);
            var filePathResult = _PPonlineGeneratePdf.PrintPreviewRefundGeneratePdf(html, input , destinationPath, @"Assets\Upload\PPOnline\PreviewRefund");

            return filePathResult;
        }
        
        [UnitOfWork(isTransactional: false)]
        public List<GetListRefundHistoryResultDto> GetListRefundHistory()
        {
            var getHistoryRefund = (from tpp in _PPOnlinecontext.TR_PriorityPass
                                    join tppr in _PPOnlinecontext.TR_PriorityPassRefund on tpp.Id equals tppr.priorityPassID
                                    select new                                     {
                                        clientCode = tpp.psCode,
                                        PPNumber = tpp.PPNo,
                                        refundReason = tppr.refundReasonOthers,
                                        salesChannel = tpp.scmCode,
                                        memberCode = tpp.memberCode,
                                        PPRefundID = tppr.Id
                                    }).Distinct().ToList();

            var getName = (from history in getHistoryRefund
                           join personalmember in _personalsContext.PERSONALS_MEMBER on history.memberCode equals personalmember.memberCode
                           join personal in _personalsContext.PERSONAL on personalmember.psCode equals personal.psCode
                           select new GetListRefundHistoryResultDto
                           {
                               clientCode = history.clientCode,
                               PPNumber = history.PPNumber,
                               refundReason = history.refundReason,
                               salesChannel = history.salesChannel,
                               memberName = personal.name,
                               PPRefundID = history.PPRefundID
                           }).Distinct().ToList();

            if(getName.Count() >= 100)
            {
                return getName.Take(100).ToList();
            }
            else if (getName.Count() < 100)
            {
                return getName;
            }
            else
            {
                throw new UserFriendlyException("Data is empty");
            }
        }

        [UnitOfWork(isTransactional: false)]
        public List<GetListRefundHistoryResultDto> GetListRefundHistoryBySearch(string psOrPP)
        {
            var getHistory = (from tppr in _PPOnlinecontext.TR_PriorityPassRefund
                              join tpp in _PPOnlinecontext.TR_PriorityPass on tppr.priorityPassID equals tpp.Id
                              where tpp.psCode.Contains(psOrPP) || tpp.PPNo.Contains(psOrPP)
                              select new 
                              {
                                  clientCode = tpp.psCode,
                                  PPNumber = tpp.PPNo,
                                  refundReason = tppr.refundReasonOthers,
                                  salesChannel = tpp.scmCode,
                                  memberCode = tpp.memberCode,
                                  PPRefundID = tppr.Id
                              }).Distinct().ToList();

            var getName = (from history in getHistory
                           join personalmember in _personalsContext.PERSONALS_MEMBER on history.memberCode equals personalmember.memberCode
                           join personal in _personalsContext.PERSONAL on personalmember.psCode equals personal.psCode
                           where history.clientCode.Contains(psOrPP) || history.PPNumber.Contains(psOrPP)
                           select new GetListRefundHistoryResultDto
                           {
                               clientCode = history.clientCode,
                               PPNumber = history.PPNumber,
                               refundReason = history.refundReason,
                               salesChannel = history.salesChannel,
                               memberName = personal.name,
                               PPRefundID = history.PPRefundID
                           }).Distinct().ToList();

            if (getName != null)
            {
                return getName;
            }
            else
            {
                throw new UserFriendlyException("Data is empty");
            }
        }

        [UnitOfWork(isTransactional: false)]
        public ResultMessageDto PrintFormPembatalan(int PPRefundID)
        {
            var result = "";

            var getData = (from tpp in _PPOnlinecontext.TR_PriorityPass
                           join tppr in _PPOnlinecontext.TR_PriorityPassRefund on tpp.Id equals tppr.priorityPassID
                           where tppr.Id == PPRefundID
                           select new
                           {
                               tpp.PPNo,
                               tppr.refundReasonOthers,
                               tppr.namaBank,
                               tppr.noRek,
                               tppr.namaRek,
                               tpp.buyDate,
                               tppr.refundTime,
                               tpp.psCode,
                               tppr.Id,
                               tpp.paymentType
                           }).Distinct().ToList();

            var getAllData = (from data in getData
                              join personal in _personals.GetAll() on data.psCode equals personal.psCode
                              where data.Id == PPRefundID
                              select new GeneratePreviewRefundInputDto
                              {
                                  customerName = personal.name,
                                  bankName = data.namaBank,
                                  noRek = data.noRek,
                                  PPNumber = data.PPNo,
                                  refundReason = data.refundReasonOthers,
                                  rekName = data.namaRek,
                                  tanggal = String.Format("{0: dd MMM yyyy}", data.refundTime),
                                  tglCetak = String.Format("{0: dd MMM yyyy}", data.buyDate),
                                  tglTransfer = String.Format("{0: dd MMM yyyy}", data.buyDate),
                                  noForm = PPRefundID.ToString(),
                                  customerCode = data.psCode,
                                  paymentType = data.paymentType == null ? "-" : data.paymentType
                              }).FirstOrDefault();

            var file = @"Assets\Upload\PPOnline\PreviewRefund";

            var filepath = Path.Combine(_hostingEnvironment.WebRootPath, file);

            var filename = "PR_" + getAllData.customerCode + "_" + getAllData.PPNumber + ".pdf";

            var files = Path.Combine(filepath, filename);

            if (!File.Exists(files))
            {
                result = GeneratePreviewRefund(getAllData);
            }
            else
            {
                result = Path.Combine(file, filename);
            }

            return new ResultMessageDto { message = _fileHelper.getAbsoluteUri() + result, result = true };
        }

    }
}
