﻿using Abp.Domain.Repositories;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.DataExporting.Pdf.PPOnline;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.Files;
using VDI.Demo.PPOnline.OrderPP.Dto;
using VDI.Demo.PPOnlineDB;
using VDI.Demo.PropertySystemDB.OnlineBooking.PPOnline;
using System.Linq;
using Abp.UI;
using VDI.Demo.OnlineBooking.Transaction.Dto;
using System.IO;
using Visionet_Backend_NetCore.Komunikasi;
using System.Globalization;
using VDI.Demo.OnlineBooking.Email;
using VDI.Demo.OnlineBooking.Email.Dto;
using VDI.Demo.PPOnline.PaymentPP;
using Abp.Domain.Uow;
using Microsoft.AspNetCore.Mvc;
using VDI.Demo.Dto;
using VDI.Demo.PPOnline.OrderPP.ExportExcellReportOrder;

namespace VDI.Demo.PPOnline.OrderPP
{
    public class OrderPPAppService : DemoAppServiceBase
    {
        private readonly IRepository<LK_PaymentType> _lkPaymentType;
        private readonly IRepository<MS_Projects> _msProject;
        private readonly IRepository<TR_Payment> _trPayment;
        private readonly IRepository<TR_PaymentDetailEDC> _trPaymentDetailEDC;
        private readonly IRepository<TR_PPOrder> _trPPOrder;
        private readonly PPOnlineDbContext _PPOnlinecontext;
        private readonly PersonalsNewDbContext _personalsContext;
        private readonly PropertySystemDbContext _propertySystemContext;
        private readonly IPPOnlineGeneratePdfExporter _PPonlineGeneratePdf;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly FilesHelper _fileHelper;
        private readonly IEmailAppService _emailAppService;
        private readonly IPaymentPPAppService _paymentPPAppService;
        private readonly IGenerateReportOrderExcelExporter _generateExcel; 

        public OrderPPAppService
        (
            IRepository<LK_PaymentType> lkPaymentType,
            IRepository<MS_Projects> msProject,
            IRepository<TR_Payment> trPayment,
            IRepository<TR_PaymentDetailEDC> trPaymentDetailEDC,
            IRepository<TR_PPOrder> trPPOrder,
            PPOnlineDbContext PPOnlinecontext,
            PersonalsNewDbContext personalsContext,
            PropertySystemDbContext propertySystemContext,
            IPPOnlineGeneratePdfExporter PPonlineGeneratePdf,
            IHostingEnvironment hostingEnvironment,
            FilesHelper fileHelper,
            IEmailAppService emailAppService,
            IPaymentPPAppService paymentPPAppService,
            IGenerateReportOrderExcelExporter generateExcel

        )
        {
            _lkPaymentType = lkPaymentType;
            _msProject = msProject;
            _trPayment = trPayment;
            _trPaymentDetailEDC = trPaymentDetailEDC;
            _trPPOrder = trPPOrder;
            _PPOnlinecontext = PPOnlinecontext;
            _personalsContext = personalsContext;
            _propertySystemContext = propertySystemContext;
            _PPonlineGeneratePdf = PPonlineGeneratePdf;
            _hostingEnvironment = hostingEnvironment;
            _fileHelper = fileHelper;
            _emailAppService = emailAppService;
            _paymentPPAppService = paymentPPAppService;
            _generateExcel = generateExcel;
        }

        public List<GetListOrderHistoryResultDto> GetListOrderHistory(GetListOrderHistoryInputDto input)
        {
            var getOrder = (from trppo in _PPOnlinecontext.TR_PPOrder
                            join trpop in _PPOnlinecontext.TR_PPOrderPPNo on trppo.Id equals trpop.PPOrderID
                            join trpp in _PPOnlinecontext.TR_PriorityPass on trpop.PPNo equals trpp.PPNo
                            join los in _PPOnlinecontext.LK_OrderStatus on trppo.orderStatusID equals los.Id
                            join msp in _PPOnlinecontext.MS_Projects on trppo.projectID equals msp.Id
                            group trppo by new
                            {
                                trppo.orderCode,
                                los.orderStatusName,
                                trpp.PPNo,
                                PPOrderID = trppo.Id,
                                trpp.psCode,
                                trppo.orderStatusID,
                                trppo.projectID,
                                msp.projectName,
                                trppo.CreationTime
                            }into G
                            select new GetListOrderHistoryResultDto
                            {
                                orderCode = G.Key.orderCode,
                                orderStatusName = G.Key.orderStatusName,
                                PPNo = G.Key.PPNo,
                                PPOrderID = G.Key.PPOrderID,
                                psCode = G.Key.psCode,
                                orderStatusID = G.Key.orderStatusID,
                                projectID = G.Key.projectID,
                                projectName = G.Key.projectName,
                                creationTime = G.Key.CreationTime
                            }).Distinct().ToList();

            try
            {
                var getName = (from data in getOrder
                               join personal in _personalsContext.PERSONAL on data.psCode equals personal.psCode
                               select new GetListOrderHistoryResultDto
                               {
                                   orderCode = data.orderCode,
                                   customerName = personal.name,
                                   orderStatusName = data.orderStatusName,
                                   PPNo = data.PPNo,
                                   PPOrderID = data.PPOrderID,
                                   psCode = data.psCode,
                                   orderStatusID = data.orderStatusID,
                                   projectID = data.projectID,
                                   paymentType = data.paymentType,
                                   projectName = data.projectName,
                                   creationTime = data.creationTime
                               }).Distinct().ToList();

                foreach (var item in getName)
                {
                    var getPayment = (from trp in _PPOnlinecontext.TR_Payment
                                      where trp.PPOrderID == item.PPOrderID
                                      select trp).FirstOrDefault();

                    if(getPayment != null)
                    {
                        item.paymentType = getPayment.paymentType;
                    }
                }


                if (input.orderStatusID != null && input.orderStatusID != 0)
                {
                    getName = (from data in getName
                               where data.orderStatusID == input.orderStatusID
                               select data).Distinct().ToList();
                }
                if (input.projectID != 0 && input.projectID != null)
                {
                    getName = (from data in getName
                               where data.projectID == input.projectID
                               select data).Distinct().ToList();
                }
                if (input.paymentType != null && input.paymentType != 0)
                {
                    getName = (from data in getName
                               where data.paymentType == input.paymentType
                               select data).Distinct().ToList();
                }
                if (input.keyword != null && input.keyword != "")
                {
                    getName = (from data in getName
                               where data.customerName.Contains(input.keyword)
                                  || data.projectName.Contains(input.keyword)
                                  || data.orderCode.Contains(input.keyword)
                               select data).Distinct().ToList();
                }
                if((input.sortID != null && input.sortID !=0) && (input.byID != null && input.byID != 0))
                {
                    if(input.sortID == 1 && input.byID == 1)
                    {
                        getName = (from data in getName
                                   orderby data.creationTime ascending
                                   select data).ToList();
                    }
                    else if (input.sortID == 1 && input.byID == 2)
                    {
                        getName = getName.OrderByDescending(a => a.creationTime).ToList();
                    }
                    else if (input.sortID == 2 && input.byID == 1)
                    {
                        getName = (from data in getName
                                   orderby data.orderCode ascending
                                   select data).ToList();
                    }
                    else if (input.sortID == 2 && input.byID == 2)
                    {
                        getName = getName.OrderByDescending(a => a.orderCode).ToList();
                    }
                    else if (input.sortID == 3 && input.byID == 1)
                    {
                        getName = (from data in getName
                                   orderby data.projectName ascending
                                   select data).ToList();
                    }
                    else if (input.sortID == 3 && input.byID == 2)
                    {
                        getName = getName.OrderByDescending(a => a.projectName).ToList();
                    }
                    else if (input.sortID == 4 && input.byID == 1)
                    {
                        getName = (from data in getName
                                   orderby data.customerName ascending
                                   select data).ToList();
                    }
                    else
                    {
                        getName = getName.OrderByDescending(a => a.customerName).ToList();
                    }
                }

                return getName;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Error : {0}", ex.Message);
            }
        }

        public ResultMessageDto ResendEmail(int PPOrderID)
        {
            CultureInfo ID = CultureInfo.CreateSpecificCulture("id-ID");

            var getDataBody = (from trppo in _PPOnlinecontext.TR_PPOrder
                               join msp in _PPOnlinecontext.MS_Projects on trppo.projectID equals msp.Id
                               join trppop in _PPOnlinecontext.TR_PPOrderPPNo on trppo.Id equals trppop.PPOrderID
                               join trpp in _PPOnlinecontext.TR_PriorityPass on trppop.PPNo equals trpp.PPNo
                               where trppo.Id == PPOrderID
                               select new CreateBodyEmailPPCustomerInputDto
                               {
                                   orderCode = trppo.orderCode,
                                   productName = msp.projectName,
                                   logoProduct = msp.logo,
                                   qty = trppo.qty,
                                   PPOrderID = trppo.Id,
                                   PPNo = trppop.PPNo,
                                   psCode = trpp.psCode,
                                   memberCode = trpp.memberCode
                               }).Distinct().ToList();

            var getPP = (from pp in getDataBody
                         where pp.PPOrderID == PPOrderID
                         select new ListPPNo
                         {
                             PPNo = pp.PPNo
                         }).Distinct().ToList();

            var getData = (from body in getDataBody
                           join personal in _personalsContext.PERSONAL on body.psCode equals personal.psCode
                           join tremail in _personalsContext.TR_Email on personal.psCode equals tremail.psCode
                           join member in _personalsContext.PERSONALS_MEMBER on body.memberCode equals member.memberCode
                           join tre in _personalsContext.TR_Email on member.psCode equals tre.psCode
                           orderby Convert.ToDateTime(body.paymentDate) descending
                           select new CreateBodyEmailPPCustomerInputDto
                           {
                               orderCode = body.orderCode,
                               productName = body.productName,
                               paymentDate = body.paymentDate,
                               logoProduct = body.logoProduct,
                               PPPrice = body.PPPrice,
                               qty = body.qty,
                               PPOrderID = body.PPOrderID,
                               customerName = personal.name,
                               psCode = body.psCode,
                               memberCode = body.memberCode,
                               emailCustomer = tremail.email,
                               emailMember = tre.email
                           }).FirstOrDefault();

            var getPayement = (from payment in _PPOnlinecontext.TR_Payment
                              where payment.PPOrderID == PPOrderID
                              orderby payment.Id descending
                              group payment by new
                              {
                                  payment.paymentDate
                              } into G
                              select new 
                              {
                                  paymentDate = String.Format("{0: dd MMM yyyy}", G.Key.paymentDate),
                                  PPPrice = G.Sum(a=> a.paymentAmt)
                              }).FirstOrDefault();

            getData.listPPNo = getPP;
            getData.paymentDate = getPayement.paymentDate;
            getData.PPPrice = String.Format(ID, "{0:0,0}", getPayement.PPPrice);

            var bodyCustomer = CreateBodyEmailPPCustomer(getData);

            var ttbf = _paymentPPAppService.PrintTTBF(getData.PPOrderID);

            var emailCus = new SendEmailInputDto
            {
                body = bodyCustomer,
                toAddress = getData.emailCustomer,
                subject = "Konfirmasi Pemesanan PP Number",
                pathKP = ttbf.path
            };

            _emailAppService.ConfigurationEmail(emailCus);

            var bodyMember = CreateBodyEmailPPMember(getData);

            var emailMem = new SendEmailInputDto
            {
                body = bodyMember,
                toAddress = getData.emailMember,
                subject = "Konfirmasi Pemesanan PP Number"
            };

            _emailAppService.ConfigurationEmailBankTransfer(emailMem);

            return new ResultMessageDto { result = true, message = "Success" };
        }

        private string CreateBodyEmailPPCustomer(CreateBodyEmailPPCustomerInputDto input)
        {
            var fileHtml = Path.Combine(_hostingEnvironment.WebRootPath, @"Assets\HtmlTemplates\PPOnline\BodyEmailPP_Customer.html");

            string html = File.ReadAllText(fileHtml);

            var PPHtml = "";

            for(var i = 1; i <= input.listPPNo.Count; i++)
            {
                if(i == 1)
                {
                    PPHtml += "<td style='width: 390px; font-family: arial; font-size: 14px; line-height: 24px; color: #898989;' align= 'left' mc: label = 'the_copy' mc: edit = 'the_copy'>" + input.listPPNo[i - 1].PPNo + "</td></tr>";
                }
                else
                {
                    PPHtml += "<tr>" +
                              "<td style = 'width: 200px; white-space: nowrap; padding-left: 40px; font-family: arial; font-size: 14px; line-height: 24px; color: #898989;' align = 'left' mc: label = 'the_copy' mc: edit = 'the_copy' ></td>"+
                              "<td style = 'width: 10px; font-family: arial; font-size: 14px; line-height: 24px; color: #898989;' align = 'left' mc: label = 'the_copy' mc: edit = 'the_copy ></td>"+
                              "<td style = 'width: 390px; font-family: arial; font-size: 14px; line-height: 24px; color: #898989;' align = 'left' mc: label = 'the_copy' mc: edit = 'the_copy'>&nbsp;"+ input.listPPNo[i - 1].PPNo + "</td></tr>";
                              
                }
                
            }

            input.PPHtml = PPHtml;

            html = Setting_variabel.GetHTMLMappedValue(html, input);

            return html;
        }

        private string CreateBodyEmailPPMember(CreateBodyEmailPPCustomerInputDto input)
        {
            var fileHtml = Path.Combine(_hostingEnvironment.WebRootPath, @"Assets\HtmlTemplates\PPOnline\BodyEmailPP_Member.html");

            string html = File.ReadAllText(fileHtml);

            var PPHtml = "";

            for (var i = 1; i <= input.listPPNo.Count; i++)
            {
                if (i == 1)
                {
                    PPHtml += "<td style='width: 390px; font-family: arial; font-size: 14px; line-height: 24px; color: #898989;' align= 'left' mc: label = 'the_copy' mc: edit = 'the_copy'>" + input.listPPNo[i - 1].PPNo + "</td></tr>";
                }
                else
                {
                    PPHtml += "<tr>" +
                              "<td style = 'width: 200px; white-space: nowrap; padding-left: 40px; font-family: arial; font-size: 14px; line-height: 24px; color: #898989;' align = 'left' mc: label = 'the_copy' mc: edit = 'the_copy' ></td>" +
                              "<td style = 'width: 10px; font-family: arial; font-size: 14px; line-height: 24px; color: #898989;' align = 'left' mc: label = 'the_copy' mc: edit = 'the_copy ></td>" +
                              "<td style = 'width: 390px; font-family: arial; font-size: 14px; line-height: 24px; color: #898989;' align = 'left' mc: label = 'the_copy' mc: edit = 'the_copy'>&nbsp;" + input.listPPNo[i - 1].PPNo + "</td></tr>";

                }

            }

            input.PPHtml = PPHtml;

            html = Setting_variabel.GetHTMLMappedValue(html, input);

            return html;
        }

        public List<GetDropdownSortResultDto> GetDropdownSort()
        {
            var listData = new List<GetDropdownSortResultDto>();

            var sortID = 1;

            var array = new[] { "Input Time", "Ordercode", "Project", "Customer" };

            foreach (var item in array)
            {
                var data = new GetDropdownSortResultDto
                {
                    sortID = sortID,
                    sortName = item
                };

                listData.Add(data);

                sortID++;
            }

            return listData;
        }
        public List<GetDropdownSortResultDto> GetDropdownSortBy()
        {
            var listData = new List<GetDropdownSortResultDto>();

            var sortID = 1;

            var array = new[] { "Ascending", "Descending" };

            foreach (var item in array)
            {
                var data = new GetDropdownSortResultDto
                {
                    sortID = sortID,
                    sortName = item
                };

                listData.Add(data);

                sortID++;
            }

            return listData;
        }

        public List<GetDropdownTermResultDto> GetDropdownTerm()
        {
            var getTerm = (from term in _PPOnlinecontext.LK_TermRef
                           select new GetDropdownTermResultDto
                           {
                               termID = term.Id,
                               termName = term.termName
                           }).Distinct().ToList();

            try
            {
                return getTerm;
            }
            catch(Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }
        }

        public List<GetCheckBoxDisplayedFieldResultDto> GetCheckboxDisplayedField()
        {
            var getData = new List<GetCheckBoxDisplayedFieldResultDto>();

            var name = new[] { "Year", "Year & Month", "Ordercode", "Order Date", "Order Status",
                               "Project", "Customer", "Preffered Unit", "Term Payment", "PP Qty",
                               "Price", "Total Price"};

            var displayedID = 1;

            foreach (var item in name)
            {
                var dataDisplay = new GetCheckBoxDisplayedFieldResultDto
                {
                    displayedID = displayedID,
                    displayedName = item
                };

                getData.Add(dataDisplay);

                displayedID++;
            }

            return getData;
        }
        public List<GetPreferredTypeResultDto> GetPreferredType(int projectID)
        {
            var getData = (from category in _PPOnlinecontext.LK_CategoryTypePreferred
                           where category.projectID == projectID
                           select new GetPreferredTypeResultDto
                           {
                               prefferedTypeID = category.Id,
                               preferredTypeName = category.preferredName
                           }).ToList();

            return getData;
        }
        
        [UnitOfWork(isTransactional: false)]
        [HttpPost]
        public List<GetListReportOrderResultDto> GetListReportOrder(GetListReportOrderInputDto input)
        {
            CultureInfo ID = CultureInfo.CreateSpecificCulture("id-ID");

            var getData = (from trppo in _PPOnlinecontext.TR_PPOrder
                           join trppop in _PPOnlinecontext.TR_PPOrderPPNo on trppo.Id equals trppop.PPOrderID
                           join trpp in _PPOnlinecontext.TR_PriorityPass on trppop.PPNo equals trpp.PPNo
                           join personal in _personalsContext.PERSONAL.ToList() on trpp.psCode equals personal.psCode
                           join lkos in _PPOnlinecontext.LK_OrderStatus on trppo.orderStatusID equals lkos.Id
                           join msp in _PPOnlinecontext.MS_Projects on trppo.projectID equals msp.Id
                           join lcp in _PPOnlinecontext.LK_CategoryTypePreferred on trppo.preferredType equals lcp.preferredType
                           join lkt in _PPOnlinecontext.LK_TermRef on trppo.termRefID equals lkt.Id
                           join msb in _PPOnlinecontext.MS_BatchEntry on trpp.batchID equals msb.Id
                           join member in _personalsContext.PERSONALS_MEMBER.ToList() on trpp.memberCode equals member.memberCode
                           join ps in _personalsContext.PERSONAL.ToList() on member.psCode equals ps.psCode
                           select new GetListReportOrderResultDto
                           {
                               year = String.Format("{0:yyyy}", trppo.CreationTime),
                               yearMonth = String.Format("{0: MMM yyyy}", trppo.CreationTime),
                               orderCode = trppo.orderCode,
                               customerName = personal.name,
                               orderDate = String.Format("{0: dd MMM yyyy}", trppo.CreationTime),
                               orderStatus = lkos.orderStatusName,
                               preferredTypeName = lcp.preferredName,
                               projectName = msp.projectName,
                               qty = trppo.qty,
                               termPayment = lkt.termName,
                               price = msb.priorityPassPrice,
                               memberName = ps.name,
                               orderStatusID = trppo.orderStatusID,
                               prefferedType = trppo.preferredType,
                               projectID = trppo.projectID,
                               termID = trppo.termRefID
                           }).Distinct().ToList();

            foreach (var item in getData)
            {
                var hitung = item.price * (decimal?)item.qty;

                item.totalPrice = hitung;
            }

            var dateFrom = Convert.ToDateTime(input.orderDateFrom);

            var dateTo = Convert.ToDateTime(input.orderDateTo);

            if (input.orderDateFrom != null && input.orderDateTo != null)
            {
                getData = (from data in getData
                           where Convert.ToDateTime(data.orderDate) >= dateFrom && Convert.ToDateTime(data.orderDate) <= dateTo
                           select data).ToList();
            }
            if(input.orderDateFrom != null && input.orderDateTo == null)
            {
                getData = (from data in getData
                           where Convert.ToDateTime(data.orderDate) >= dateFrom
                           select data).ToList();
            }
            if (input.orderDateFrom == null && input.orderDateTo != null)
            {
                getData = (from data in getData
                           where Convert.ToDateTime(data.orderDate) <= dateTo
                           select data).ToList();
            }
            if(input.projectID != null)
            {
                getData = (from data in getData
                           where data.projectID == input.projectID
                           select data).ToList();
            }
            if (input.termID.Length != 0 && input.termID != null)
            {
                getData = (from data in getData
                           where input.termID.Contains(data.termID)
                           select data).ToList();
               
            }
            if (input.keyword != null)
            {
                getData = (from data in getData
                           where data.customerName.Contains(input.keyword) || data.memberName.Contains(input.keyword)
                                || data.orderCode.Contains(input.keyword)
                           select data).ToList();
            }
            if(input.orderStatusID.Length != 0 && input.orderStatusID != null)
            {
                getData = (from data in getData
                           where input.orderStatusID.Contains(data.orderStatusID)
                           select data).ToList();
            }
            if(input.preferredTypeID.Length != 0 && input.preferredTypeID != null)
            {
                getData = (from data in getData
                           where input.preferredTypeID.Contains(data.prefferedType)
                           select data).ToList();
            }

            getData.GroupBy(a => a.year).ToList();
            getData.GroupBy(a => a.yearMonth).ToList();
            getData.GroupBy(a => a.orderStatus).ToList();

            return getData;
        }
        public ResultMessageDto GenerateReportPdf(GetReportOrderPdfInputDto input)
        {
            Mse.PDFSharp.PageSize paperSize;

            Mse.PDFSharp.PageOrientation orientation;

            if(input.paperSizeID == 1)
            {
                paperSize = Mse.PDFSharp.PageSize.A4;
            }
            else
            {
                paperSize = Mse.PDFSharp.PageSize.A3;
            }
            if(input.orientationID == 1)
            {
                orientation = Mse.PDFSharp.PageOrientation.Landscape;
            }
            else
            {
                orientation = Mse.PDFSharp.PageOrientation.Portrait;
            }
            
            string headerHtml = "";

            input.listData.listHeader = input.listData.listHeader.OrderBy(a => a.headerID).ToList();

            var color = "class = 'bg'";

            foreach (var item in input.listData.listHeader)
            {
                headerHtml += "<th style='border: 1px solid black;'" + color +"><strong>" + item.header + "</strong></th>";
            }

            input.listData.headerHtml = headerHtml;

            string bodyHtml = "";

            foreach (var item in input.listData.listBody)
            {
                bodyHtml += "<tr>";

                if(item.year != null)
                {
                    bodyHtml += "<td style='border: 1px solid black;'>" + item.year + "</td>";
                }
                if (item.yearMonth != null)
                {
                    bodyHtml += "<td style='border: 1px solid black;'>" + item.yearMonth + "</td>";
                }
                if (item.orderCode != null)
                {
                    bodyHtml += "<td style='border: 1px solid black;'>" + item.orderCode + "</td>";
                }
                if (item.orderDate != null)
                {
                    bodyHtml += "<td style='border: 1px solid black;'>" + item.orderDate + "</td>";
                }
                if (item.orderStatus != null)
                {
                    bodyHtml += "<td style='border: 1px solid black;'>" + item.orderStatus + "</td>";
                }
                if (item.projectName != null)
                {
                    bodyHtml += "<td style='border: 1px solid black;'>" + item.projectName + "</td>";
                }
                if (item.customerName != null)
                {
                    bodyHtml += "<td style='border: 1px solid black;'>" + item.customerName + "</td>";
                }
                if (item.preferredTypeName != null)
                {
                    bodyHtml += "<td style='border: 1px solid black;'>" + item.preferredTypeName + "</td>";
                }
                if (item.termPayment != null)
                {
                    bodyHtml += "<td style='border: 1px solid black;'>" + item.termPayment + "</td>";
                }
                if (item.qty != 0)
                {
                    bodyHtml += "<td style='border: 1px solid black;'>" + item.qty.ToString() + "</td>";
                }
                if (item.price != null)
                {
                    bodyHtml += "<td style='border: 1px solid black;'>" + item.price.ToString() + "</td>";
                }
                if (item.totalPrice != null)
                {
                    bodyHtml += "<td style='border: 1px solid black;'>" + item.totalPrice.ToString() + "</td>";

                    input.listData.grandTotal = input.listData.listBody.Sum(a => a.totalPrice).ToString();
                }
                else
                {
                    input.listData.grandTotal = "0";
                }

                bodyHtml += "</tr>";
            }

            input.listData.bodyHtml = bodyHtml;

            input.listData.colspanTbl = input.listData.listHeader.Count() - 1;
            
            var destinationPath = Path.Combine(_hostingEnvironment.WebRootPath, @"Assets\Upload\PPOnline\ReportOrder");
            var filePath = _hostingEnvironment.WebRootPath + @"\Assets\HtmlTemplates\PPOnline\ReportOrder.html";

            string html = File.ReadAllText(filePath);
            var filePathResult = _PPonlineGeneratePdf.PrintReportOrderPdf(html, input.listData, destinationPath, @"Assets\Upload\PPOnline\ReportOrder", paperSize, orientation);

            return new ResultMessageDto { message = _fileHelper.getAbsoluteUri() + filePathResult, result = true };
        }

        public FileDto GenerateExcelReportOrder(GetReportOrderPdfInputDto input)
        {
            return _generateExcel.GenerateExcelReportOrder(input);
        }
    }
}
