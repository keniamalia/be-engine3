﻿using Abp.Extensions;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.DataExporting.Excel.EpPlus;
using VDI.Demo.Dto;
using VDI.Demo.PPOnline.OrderPP.Dto;
using VDI.Demo.PPOnline.PaymentPP.Dto;
using System.Linq;

namespace VDI.Demo.PPOnline.OrderPP.ExportExcellReportOrder
{
    public class GenerateReportOrderExcelExporter : EpPlusExcelExporterBase, IGenerateReportOrderExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IHostingEnvironment _hostingEnvironment;

        public GenerateReportOrderExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IHttpContextAccessor httpContextAccessor,
            IAbpSession abpSession,
            IHostingEnvironment environment)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
            _httpContextAccessor = httpContextAccessor;
            _hostingEnvironment = environment;
        }

        public FileDto GenerateExcelReportOrder(GetReportOrderPdfInputDto input)
        {
            return CreateExcelPackage(
                "Report_Order_"+ String.Format("{0:ddMMyyy_hhmmss}", DateTime.Now) + ".xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add("ReportOrder");
                    sheet.OutLineApplyStyle = true;
                    var headerCells = sheet.Cells[1, 1];
                    headerCells.Value = "Report Order";
                    var headerFont = headerCells.Style.Font;
                    headerFont.Size = 14;
                    headerFont.Bold = true;
                    
                    AddHeaders(
                        sheet,
                        3,
                        "Year",
                        "Year & Month",
                        "Order Status",
                        "Order Date",
                        "Customer",
                        "Project",
                        "Order Code",
                        "Preferred Unit",
                        "Term Payment",
                        "Quantity",
                        "Price",
                        "Total Price"
                    );

                    AddObjects(
                        sheet, 4, input.listData.listBody,
                        _ => _.year,
                        _ => _.yearMonth,
                        _ => _.orderStatus,
                        _ => _.orderDate,
                        _ => _.customerName,
                        _ => _.projectName,
                        _ => _.orderCode,
                        _ => _.preferredTypeName,
                        _ => _.termPayment,
                        _ => _.qty,
                        _ => _.price,
                        _ => _.totalPrice
                        );

                    var x = input.listData.listBody.Count + 5;

                    var grandTotal = sheet.Cells[x, 1];

                    grandTotal.Value = "Grand Total";

                    grandTotal.Merge = true;

                    var gtFont = grandTotal.Style.Font;
                    gtFont.Size = 14;
                    gtFont.Bold = true;

                    var grandTotalAmt = sheet.Cells[x, 14];

                    var amt = input.listData.listBody.Sum(a => a.totalPrice);

                    grandTotalAmt.Value = amt;


                    for (var i = 1; i <= 10; i++)
                    {
                        if (i.IsIn(5, 10)) //Don't AutoFit Parameters and Exception
                        {
                            continue;
                        }

                        sheet.Column(i).AutoFit();
                    }
                });
        }

        public FileDto GenerateExcelReportPayment(GetPaymentReportPdfInputDto input)
        {
            return CreateExcelPackage(
                "Report_Payment_" + String.Format("{0:ddMMyyy_hhmmss}", DateTime.Now) + ".xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add("ReportPayment");
                    sheet.OutLineApplyStyle = true;
                    var headerCells = sheet.Cells[1, 1];
                    headerCells.Value = "Report Payment";
                    var headerFont = headerCells.Style.Font;
                    headerFont.Size = 14;
                    headerFont.Bold = true;

                    AddHeaders(
                        sheet,
                        3,
                        "Year",
                        "Year & Month",
                        "Payment Status",
                        "Payment Date",
                        "ClearDate",
                        "Bank",
                        "Customer",
                        "Payment Type",
                        "Order No",
                        "PP No",
                        "Account No",
                        "Account Name",
                        "Bank Branch",
                        "Amount"
                    );

                    AddObjects(
                        sheet, 4, input.listData.listBody,
                        _ => _.year,
                        _ => _.yearMonth,
                        _ => _.paymentStatus,
                        _ => _.paymentDate,
                        _ => _.clearDate,
                        _ => _.bankName,
                        _ => _.customerName,
                        _ => _.paymentTypeName,
                        _ => _.orderCode,
                        _ => _.PPNo,
                        _ => _.accNo,
                        _ => _.accName,
                        _ => _.bankBranch,
                        _ => _.amount
                        );

                    var x = input.listData.listBody.Count + 5;

                    var grandTotal = sheet.Cells[x, 1];

                    grandTotal.Value = "Grand Total";

                    grandTotal.Merge = true;

                    var gtFont = grandTotal.Style.Font;
                    gtFont.Size = 14;
                    gtFont.Bold = true;

                    var grandTotalAmt = sheet.Cells[x, 14];

                    var amt = input.listData.listBody.Sum(a => a.amount);

                    grandTotalAmt.Value = amt;


                    for (var i = 1; i <= 10; i++)
                    {
                        if (i.IsIn(5, 10)) //Don't AutoFit Parameters and Exception
                        {
                            continue;
                        }

                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}
