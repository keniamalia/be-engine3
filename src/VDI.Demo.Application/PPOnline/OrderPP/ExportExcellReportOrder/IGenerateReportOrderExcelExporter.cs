﻿using System;
using System.Collections.Generic;
using System.Text;
using VDI.Demo.Dto;
using VDI.Demo.PPOnline.OrderPP.Dto;
using VDI.Demo.PPOnline.PaymentPP.Dto;

namespace VDI.Demo.PPOnline.OrderPP.ExportExcellReportOrder
{
    public interface IGenerateReportOrderExcelExporter
    {
        FileDto GenerateExcelReportOrder(GetReportOrderPdfInputDto input);

        FileDto GenerateExcelReportPayment(GetPaymentReportPdfInputDto input);
    }
}
